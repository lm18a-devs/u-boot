#ifndef _EPK_H_
#define _EPK_H_

#define RELEASE_MODE		0
#define DEBUG_MODE 			1
#define TEST_MODE 			2
#define UNKNOWN_MODE		3

#define PAK_TYPE_ID_LEN		4
#define FILE_PATH_LEN		1024

#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_YELLOW  "\x1b[33m"
#define ANSI_COLOR_BLUE    "\x1b[34m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_CYAN    "\x1b[36m"
#define ANSI_COLOR_RESET   "\x1b[0m"

typedef unsigned char	UINT8;
typedef unsigned int	UINT32;

/* FILE TYPE */
typedef enum
{
	FILE_TYPE_PAK	= 0,
	FILE_TYPE_EPK,
	FILE_TYPE_UNKNOWN
} FILE_TYPE_T;

typedef struct
{
	UINT32				imageOffset;
	UINT32				imageSize;
} PAK_LOCATION_T;

// EPK�� ���� �� PAK Header 128 Bytes
typedef struct
{
	char				imageType[PAK_TYPE_ID_LEN];
	UINT32				imageSize;
	char				modelName[64];
	UINT32				SWVersion;
	UINT32				SWDate;
	UINT32				DevMode; 		/* 0 : Release Mode , 1 : Debug Mode */
	UINT32              loadAddr;		/*< 0x24 : laod Address                      */
	UINT32				magic_number;	/* Magic Number */
	UINT8				Reserved[36];
} PAK_HEADER_T;

typedef struct
{
	char				fileType[PAK_TYPE_ID_LEN];
	UINT32				fileSize;
	UINT32				fileNum;
	UINT32				epkVersion;
	char				ota_id[32];
	PAK_LOCATION_T		imageLocation[];
} EPK_HEADER_T;

typedef enum
{
	CREATE_PAK,
	CREATE_EPK,
	UNCOMP_EPK,
} CREATE_OPT_T;

//#ifdef CFG_LG_CHG
typedef enum
{
	LOAD_TYPE_TFTP			=0,
	LOAD_TYPE_FAT32,
	LOAD_TYPE_ZMODEM,
	LOAD_TYPE_HZMODEM,
	LOAD_TYPE_KERMIT,
	LOAD_TYPE_SERIAL,
	LOAD_TYPE_UNKNOWN,
} RECV_TYPE_T;
//#endif /* CFG_LG_CHG */

/**********************************************************/
#define RAWIMAGE_MAGIC			"LGRAWIMG"
#define SECTOR_SIZE_BOOT		(0x200)

typedef struct
{
	unsigned long long image_offset;	// 8Byte
	unsigned long long image_size;		// 8Byte
	unsigned long long flash_offset;	// 8Byte
	unsigned char dummy[8];				// 8Byte
}PART_LOCATION_T;

typedef struct
{
	char magic[8];							// 8Byte
	unsigned char partition_count;			// 1Byte
	unsigned char dummy1[3];				// 3Byte
	unsigned char check_boot_area;			// 1Byte
	unsigned char dummy2[3];				// 3Byte
	PART_LOCATION_T part_location[];		// 32Byte * partition_count
}RAWIMG_HEADER_T;

typedef enum
{
	GANG_TYPE_NONE = 0,
	GANG_TYPE_ORIGIANL,
	GANG_TYPE_HEAD,
} GANG_IMAGE_T;

/**********************************************************/

static FILE_TYPE_T _get_file_type(char *type)
{
	if(strncmp(type, "epak", PAK_TYPE_ID_LEN) == 0)
		return FILE_TYPE_EPK;
	else
		return FILE_TYPE_PAK;
}

static void get_imgtype(PAK_HEADER_T * pak, char* imgtype)
{
	strncpy(imgtype, pak->imageType, PAK_TYPE_ID_LEN);
	imgtype[PAK_TYPE_ID_LEN] = '\0';
//	printf("\t PAK type : %s\n", imgtype);
	#ifdef CC_EMMC_BOOT
	if(!strcmp(imgtype, "mtdinfo"))
		sprintf(imgtype, "partinfo");
	#endif
}
#ifdef CONFIG_LOAD_TFTP_PARTIAL
enum PARTIAL_MODE
{
	NORMAL,
	PARTIAL,
	PARTIAL_BIN
};

extern enum PARTIAL_MODE swup_mode;
extern enum PARTIAL_MODE swump_mode;


enum PARTIAL_ERR
{
	PARTIAL_NO_ERR,
	PARTIAL_STATUS_ERR,
	PARTIAL_SET_THRESHOLD_ERR,
	PARTIAL_WRONG_BUF_SZ,
	PARTIAL_CRC_ERR,
	PARTIAL_EMMC_WRITE_ERR,
	PARTIAL_EMMC_SIZE_ERR,
	PARTIAL_NO_PARTINFO
};

extern enum PARTIAL_ERR partial_err;

enum PARTIAL_DOWNLOAD_STATUS
{
	GET_EPK_HEADER,
	GET_PAK_LOCATION,
	GET_PAK_HEADER,
	GET_PAK_DATA,
	SKIP_PAK_DATA,
	OVER_BUF_SZ,
	DOWNLOAD_DONE
};

int partial_download(ulong offset, uchar *src, unsigned len);
#endif

#ifdef CONFIG_MULTICORES_PLATFORM
/*
 * Limited data buffer management for updating epk bigger than memory buffer size
 */
typedef struct {
	u8		*copy_buf;	/* copy buffer addr */
	u8		*data_buf;	/* buffer addr */
	size_t	size;		/* buffer size */
	volatile int front;
	volatile int rear;
	volatile int status;
	size_t	receive_size;	/* receive data size by tftp */
	size_t	write_size;		/* write data size to eMMC */
} swue_buf_t;

#define SWUE_BUF_SIZE			(64 * 1024 * 1024)	// 64M
#define SWUE_COPY_BUF_SIZE		(16 * 1024 * 1024)	// 16M
#define SWUE_BUF_STATUS_NORMAL	1
#define SWUE_BUF_STATUS_DONE	2
#define SWUE_TIME_DELAY			(10000)				// 10000ms
#define SWUE_FLAG_DONOT_UPDATE_INFO		0x01 /* do not update tail & write_size */

#define swue_buf_capa(b)	((b)->size - 1)
#define swue_buf_empty(b)	((b)->rear == (b)->front)
#define swue_buf_full(b)	(((b)->rear+1)%(b)->size == (b)->front)

/* compare remained buffer size and size */
#define swue_buf_receivable(b,s)	\
	(swue_buf_empty(b) || ((s) < ((swue_buf_capa(b) - (b)->rear + (b)->front)%(b)->size)))

/* compare downloaded buffer size and size */
#define swue_buf_writable(b,s)	\
	((s) <= (((b)->size - (b)->front + (b)->rear)%(b)->size))
#endif /* CONFIG_MULTICORES_PLATFORM */

#endif /* _EPK_H_ */
