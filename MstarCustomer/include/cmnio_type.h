/******************************************************************************
 *   DTV LABORATORY, LG ELECTRONICS INC., SEOUL, KOREA
 *   Copyright(c) 1999 by LG Electronics Inc.
 *
 *   All rights reserved. No part of this work may be reproduced, stored in a
 *   retrieval system, or transmitted by any means without prior written
 *   Permission of LG Electronics Inc.
 *****************************************************************************/

/** @file 	cmnio_type.h
 *
 *  Definitions for global configurations
 *  DESCRIPTION : In this file, all global configurations, enumerations, data
			   or structures should be shared between modules positioned in
			   different layers.
			   For example, enumeration for external video supported in the system,
			   should be referenced in User Interface module (APP layer) and,
			   at the same time, referenced Video module(Driver Layer).
			   So, these kind of definition or declaration should be stated
			   here.
 *  @author	Baekwon Choi (ÃÖ¹è±Ç, bk1472@lge.com)
 *  @version 	1.1
 *  @date	2008.06.20
 *  @see
 */

#ifndef _CMNIO_TYPE_H_

#define _CMNIO_TYPE_H_

/*-----------------------------------------------------------------------------
	Á¦¾î »ó¼ö
	(Control Constants)
------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------
	#include ÆÄÀÏµé
	(File Inclusions)
------------------------------------------------------------------------------*/
//#include "branches.h"

#include <common.h>
#include <command.h>
#include <x_typedef.h>
#include <linux/types.h>
#include <linux/string.h>
#include <linux/ctype.h>
#include <exports.h>


#ifdef	__cplusplus
extern "C"
{
#endif /* __cplusplus */

/*-----------------------------------------------------------------------------
	¸ÅÅ©·Î ÇÔ¼ö  Á¤ÀÇ
	(Macros Definitions)
------------------------------------------------------------------------------*/
#ifdef _CMN_VAR_CONF_C_
#define CMN_VAR(vType, vName, vValue)	vType vName = (vType) vValue
#define CMN_EXT
#else
#define CMN_VAR(vType, vName, vValue)	extern vType vName
#define CMN_EXT                         extern
#endif

/*-----------------------------------------------------------------------------
	»ó¼ö Á¤ÀÇ
	(Constant Definitions)
------------------------------------------------------------------------------*/


#define OK	1
#define NOT_OK 0

/*********************************************************************
	¸ÅÅ©·Î Á¤ÀÇ(Macro Definitions)
**********************************************************************/

/******************************************************************************
	»ó¼ö Á¤ÀÇ(Constant Definitions)
******************************************************************************/
#define EEPROM_SIZE						0x8000 // 32K


/* NVM Header */
#define NVM_HEADER_SIZE					16

/* NVM Magic */
#define TNVM_MAGIC_SIZE					4

/* NVM DB */
#define SYS_DB_SIZE						640		/** System DB, should be first DB			**/	// 400->640
#define FACTORY_DB_SIZE					592		/** Factory DB 							**/	// 256->288->592
#define TOOL_OPTION_DB_SIZE				128		/** Tool Option DB							**/	// 128
#define ANA_DB_SIZE						832		/** Analog Driver + Calibration DB			**/	// 832
#define SUMODE_10POINTWB_DB_SIZE		128		/** Sevice Mode ÀÇ 10 Point White Balance DB	**/	// 128
#define MODEL_INFO_DB_SIZE				96		/** Model Info DB 							**/	// 48->96
#define PRESERVE_DB_SIZE				32		/** Preserve DB							**/	// 16->32
#define EMP_DB_SIZE 					128		/** DivX DRM Information					**/	// 128
#define SWU_DB_SIZE						4096	/** SWU									**/	// 4096
#define MICOM_DB_SIZE					256		/** Internal MICOM DB						**/	// 128
#define THX_DB_SIZE						68		/** THX ÀÇ 10 Point White Balance DB			**/	// 68
#define NEW_FACTORY_DB_SIZE				64		/** New Factory DB						**/	// 64
#define	MAC_ADDRESS_SIZE				32		/** MAC Address¸¦ eeprom¿¡ backup			**/	// 32
#define SOC_VENDOR_SIZE					64		/** Vendor data							**/	// 64
#define LAST_INPUT_INFO_DB_SIZE			140		/** (sizeof(LAST_INPUT_INFO_DB_T))			**/	// 60->140
#define CONTINENT_OPTION_DB_SIZE		60		/** Continent option						**/ // 60
#define TWIN_ANA_DB_SIZE				64		/** White Balance for TWIN					**/
#define	TMP_UI_DB_SIZE					32		/** MP ÀÌÈÄ Ãß°¡µÇ´Â registry¸¦ À§ÇÔ.			**/	// 32


#define	BASIC_DB_SIZE					( NVM_HEADER_SIZE				\
										+ TNVM_MAGIC_SIZE				\
										+ SYS_DB_SIZE 					\
										+ FACTORY_DB_SIZE				\
										+ TOOL_OPTION_DB_SIZE			\
										+ ANA_DB_SIZE 					\
										+ SUMODE_10POINTWB_DB_SIZE		\
										+ MODEL_INFO_DB_SIZE			\
										+ PRESERVE_DB_SIZE				\
										+ EMP_DB_SIZE					\
										+ SWU_DB_SIZE					\
										+ MICOM_DB_SIZE					\
										+ THX_DB_SIZE					\
										+ NEW_FACTORY_DB_SIZE			\
										+ MAC_ADDRESS_SIZE				\
										+ LAST_INPUT_INFO_DB_SIZE		\
										+ CONTINENT_OPTION_DB_SIZE		\
										+ TWIN_ANA_DB_SIZE				\
										+ TMP_UI_DB_SIZE)


/*
 * ANA_DB¿Í FACTORY_DB´Â NVM MAPÀÌ º¯°æµÇ´õ¶óµµ, °ªÀ» À¯ÁöÇØ¾ß ÇÕ´Ï´Ù.
 * NVM MAP º¯°æ½Ã ANA_DB¿Í FACTORY_DB°¡ ResetµÇÁö ¾Êµµ·Ï ½ÅÁßÈ÷ °í·ÁÇØ ÁÖ½Ê½Ã¿À.
 */
/* BASE OFFSET */
#define NVM_HEADER_BASE   			( 0                                     )
#define TNVM_MAGIC_BASE				( NVM_HEADER_BASE   + NVM_HEADER_SIZE 	)
#define SYS_DB_BASE       			( TNVM_MAGIC_BASE 	+ TNVM_MAGIC_SIZE 	)
#define FACTORY_DB_BASE				( SYS_DB_BASE		+ SYS_DB_SIZE		)
#define TOOL_OPTION_DB_BASE			( FACTORY_DB_BASE   + FACTORY_DB_SIZE   )
#define ANA_DB_BASE					( TOOL_OPTION_DB_BASE + TOOL_OPTION_DB_SIZE )
#define	SUMODE_10POINTWB_DB_BASE	( ANA_DB_BASE		+ ANA_DB_SIZE		)
#define	MODEL_INFO_DB_BASE			( SUMODE_10POINTWB_DB_BASE	+ SUMODE_10POINTWB_DB_SIZE	)
#define	PRESERVE_DB_BASE			( MODEL_INFO_DB_BASE	+ MODEL_INFO_DB_SIZE	)
#define	EMP_DB_BASE     			( PRESERVE_DB_BASE  + PRESERVE_DB_SIZE 	)
#define	SWU_DB_BASE	 				( EMP_DB_BASE		+ EMP_DB_SIZE 		)
#define	MICOM_DB_BASE     			( SWU_DB_BASE		+ SWU_DB_SIZE		)
#define	THX_DB_BASE					( MICOM_DB_BASE		+ MICOM_DB_SIZE		)
#define NEW_FACTORY_DB_BASE   		( THX_DB_BASE		+ THX_DB_SIZE 		)
#define	MAC_ADDRESS_BASE			( NEW_FACTORY_DB_BASE  + NEW_FACTORY_DB_SIZE )
#define LAST_INPUT_INFO_DB_BASE		( MAC_ADDRESS_BASE	  + MAC_ADDRESS_SIZE)
#define CONTINENT_OPTION_DB_BASE	( LAST_INPUT_INFO_DB_BASE + LAST_INPUT_INFO_DB_SIZE)
#define TWIN_ANA_DB_BASE			( CONTINENT_OPTION_DB_BASE + CONTINENT_OPTION_DB_SIZE)
#define	TMP_UI_DB_BASE				( TWIN_ANA_DB_BASE + TWIN_ANA_DB_SIZE)




#define	SOC_VENDOR_BASE				( EEPROM_SIZE			- SOC_VENDOR_SIZE - 10)

#if ( BASIC_DB_SIZE > SOC_VENDOR_BASE)
#error "We need more EEPROM NVM size !!!!!"
#endif

/**
 *  This type defines system print mask.
    (from common/include/osa_debug.h)
 */
#define NUM_OF_PRNT_EACH	32
#define NUM_OF_PRNT_MODULE	10
#define BAR_CODE_SIZE_MAX	17 // same in dil_nvm.h
#define MODELNAME_SIZE_MAX	24
/*-----------------------------------------------------------------------------
	Çü Á¤ÀÇ
	(Type Definitions)
------------------------------------------------------------------------------*/
/**
 *  This type defines system print mask.
    (from common/include/osa_debug.h)
 */
typedef struct {
    UINT8   color[NUM_OF_PRNT_EACH];
    UINT32  bitmask;
} OSA_PRINT_ALLOC_T;

typedef struct
{
    UINT32              msk;
    OSA_PRINT_ALLOC_T   mod[NUM_OF_PRNT_MODULE];
} OSA_PRINT_MASK_T;

/**
 * PWM configuration
 * (from os/common_linux/dil/include/dil_lcdport.h)
 */
#define PWM_OUT_EN	0x01	/* pwm out enable */
#define PWM_DB_EN	0x02	/* pwm double buffer enable */
#define PWM_VRST_EN	0x04	/* pwm v-sync reset enable */
#define PWM_VDB_EN	0x08	/* pwm v-sync double buffer enable */
#define PWM_VAR_EN	0x10	/* pwm variable freq mode enable */
#define PWM_CLEAR_PLUST 0x20 /* PWM Clear Plus Support by LED Current */
#define PWM_CLEAR_PLUST_ON_OFF 0x40  /* PWM Clear Plus On/off by UI Menu */
#define PWM_PHASE_SHIFT 0x80 /* PWM Phase shift for Backlgith scanning */


/**
 * panel power seq.
    (from drivers/include/paneldb_ddi.h)
 *
 * @see
*/
typedef struct
{
	UINT16 panelPowOnToData;	/* Panel Power 12V On to Tx Data On*/
	UINT16 dataToLampOn;		/* Tx Data On to DRV(Inverter/ModeSelect)/EVDD(OLED) */
	UINT16 lampOffToData;		/* DRV(Inverter/ModeSelect)/EVDD(OLED) Off to Tx Data Off */
	UINT16 dataToPanelPowOff;	/* Tx Data Off to Panel Power 12V Off*/
	UINT16 powSeqReserved1;		/* Power Sequence Timing reserved data1 */
	UINT16 powSeqReserved2;		/* Power Sequence Timing reserved data2 */
}	PANEL_POWER_SEQ_T;



/**
 * panel pwm info.
 * (from os/common_linux/dil/include/dil_vbe_disp.h)
 * @see
*/
typedef struct
{
	UINT8 reserved1;
	UINT8 reserved2;
	UINT8 vbrBBootlogo;
	UINT8 vbrBMinDuty;	/** refer to CAS*/
	UINT8 vbrBMaxDuty;	/** refer to CAS*/
	UINT8 vbrBMaxDutyStore; /** Store Mode duty*/
	UINT8 vbrBDCRDuty;/** refer to CAS*/
	UINT8 vbrBNoSignalDuty;	/** refer to CAS*/
	UINT8 vbrBFreq48hz;
	UINT8 vbrBFreq60hz; /** refer to CAS*/
	UINT8 vbrBFreq50hz; /** refer to CAS*/
	UINT8 vbrCLedCurrent;
	UINT8 vbrCFreq;
	UINT8 config;
}PANEL_PWM_T;



/**
  *  System DB ¸ðµâÀ» À§ÇÑ ½ÇÁ¦ »ç¿ë type Á¤ÀÇ
  * (from os/common_linux/dil/include/dil_nvm.h)
*/
typedef struct
{
    UINT32          	validMark;
    OSA_PRINT_MASK_T	printMask;
    UINT32          	sys_utt;			/* PDP panel use time */
    UINT8          	    frcDownloadMode;	/* FRC auto download or not */
    PANEL_POWER_SEQ_T	panelpowerseq;		/* For panel power sequence timing */
    PANEL_PWM_T			panelpwm;			/* For panel pwm */
    UINT8               systemtype;         /*system type 0:atsc, 1: dvb*/
    UINT8	            ColorDepth;			/**COLOR_DEPTH_T */
    UINT8	            LVDSPixel;			/**LVDS_PIXEL_T */
	UINT8				vcomPgammaChecksum;	/*Vcom Pgamma value checksum */
	UINT8				nDebugStatus;		/* to apply encypted debugStatus feature (nEncyptedDebugStatus[32]), this param will be used no more. but it will be leaved for compatibility */
	UINT8				swuMode;			/* swu mode */
	UINT8				make_hib;			/* for snapshot boot (make building) */
	UINT8				snapshot_onoff;		/* for snapshot support or not*/
	UINT32				adcMode;			/* for adc Mode ÇöÀç ADC ¼³Á¤ °ªÀ» ÀúÀå  */
	UINT32				swum_magic;			/* swu mode magic */
	UINT8				soundout_mode;		/* soundout mode save 0:tv speaker, 1:external speaker 2:optical sound bar, 3:headphone,4:lineout,5;bluetooth soundbar */
	UINT32				zram;				/* zram */
	UINT8				eMMCUsage;			/* for eMMC Usage */
	UINT8				bDPortEnable;
	UINT8				fullVerify;			/* Secureboot full verify flag */
	UINT8				pwm_mode;			/* PWM mode */
	int				timeDifference;
	UINT8				bLKCDEnable;		/* LKCD enable */
	UINT8				bCRIUEnable;		/* for criu support or not*/
	UINT8                           nEncryptedDebugStatus[32]; /* do not refer to this param by "typeof". this should be use internally in dil layer only */
	UINT8				instant_onoff;		/* for instant boot */
	UINT8				snapshot_art;		/* for snapshot boot art*/

} SYS_DB_T;



/******************************************************
 			TOOL_OPTION1 ¿¡ ÇÊ¿äÇÑ ±¸Á¶Ã¼ Á¤ÀÇ
*****************************************************/
// ¾Æ·¡ ±¸Á¶Ã¼ º¯°æ ½Ã ¹Ýµå½Ã °¢ Ç×¸ñÀÇ MAX °ªµµ °°ÀÌ º¯°æÇÒ °Í
#define MAX_MODULE_VER		4


/* This type should be same with ePanel_SPI_Type in drv_display.h */
typedef enum
{
	LED_BAR_6	= 0,
	LED_BAR_12	= 1,
	LED_BAR_36	= 2,
	LED_BAR_40	= 3,
	LED_BAR_48	= 4,
	LED_BAR_50	= 5,
	LED_BAR_96	= 6,
	LED_BAR_MAX,
} LED_BAR_TYPE_T;


typedef enum
{
	LD_10_16_BLK	 = 0, //V_16BLK, V10BLK, RES_I, RES_II  (for 2011 modules)
	LD_6_12_BLK		 = 1, //V_12BLK, V_6BLK, H_12BLK, H_6BLK (for 2012 modules)
	LD_BLK_MAX	 	 = 2,
} LOCAL_DIM_BLOCK_TYPE_T;



/**
 * Named size of panel, comes from src/common/include/osa_modeldef.h
 */
typedef enum
{
	INCH_22 = 0,
	INCH_23,
	INCH_24,
	INCH_26,
	INCH_27,
	INCH_28,
	INCH_32,
	INCH_39,
	INCH_40,
	INCH_42,
	INCH_43,
	INCH_47,
	INCH_49,
	INCH_50,
	INCH_55,
	INCH_58,
	INCH_60,
	INCH_65,
	INCH_70,

	//16Y Inch addition
	INCH_75,

	INCH_77,
	INCH_79,
	INCH_84,
	INCH_86,
	INCH_98,
	INCH_105,

	//17Y Inch addition
	INCH_48,
	INCH_BASE
} INCH_TYPE_T;


/**
 * not used inch type
 */
typedef enum
{
	INCH_15= INCH_BASE,
	INCH_19,
	INCH_29,
	INCH_37,
   	INCH_46,
	INCH_52,
	INCH_72,
	INCH_95,
	INCH_100, // TV model

} OLD_INCH_TYPE_T;


typedef enum
{
	TOOL_W8 = 0, 	// 1
	TOOL_C8, 	// 2
	TOOL_E8, 	// 3
	TOOL_G8, 	// 4
	TOOL_B8, 	// 5
	TOOL_G7, 	// 6
	TOOL_SK95, 	// 7
	TOOL_SK85, 	// 8
	TOOL_SK80, 	// 9
	TOOL_UK75, 	// 10
	TOOL_UK65, 	// 11
	TOOL_UK63, 	// 12
	TOOL_UK62, 	// 13
	TOOL_LK61, 	// 14
	TOOL_LK57, 	// 15
	TOOL_LK54, 	// 16
	TOOL_LK619,		// 17
	TOOL_LK616,		// 18
	TOOL_LCD_END,
	TOOL_PDP_END = TOOL_LCD_END+1,
	TOOL_BASE	= TOOL_PDP_END,

} TOOL_TYPE_T;



//not used tool type (old type)
typedef enum
{
	TOOL_EA88=TOOL_BASE, 	// 1
	TOOL_EA98,		// 2
	TOOL_G3A, 		// 3
	TOOL_G3S, 		// 4
	TOOL_LA58, 		// 5
	TOOL_LA62, 		// 6
	TOOL_LA64, 		// 7
	TOOL_LA65, 		// 8
	TOOL_LA66,		// 9
	TOOL_LA68,		// 10
	TOOL_LA69,		// 11
	TOOL_LA74,		// 12
	TOOL_LA79,		// 13
	TOOL_LA86,		// 14
	TOOL_LA96,		// 15
	TOOL_LN51,		// 16
	TOOL_LN54,		// 17
	TOOL_LN56,		// 18
	TOOL_LN57,		// 19
	TOOL_MT93,		// 20
	TOOL_MS73,		// 21
	TOOL_MS53,		// 22
	TOOL_LN46,		// 23
	TOOL_LA625,		// 24
	TOOL_LA6205,	// 25
	TOOL_LN615,		// 26
	TOOL_LN575,		// 27
	//TOOL_LA97,		// 28
	TOOL_LA623,		// 29
	TOOL_LA71,		// 30
	TOOL_LN61,		// 31
	TOOL_LA88,		// 32
	TOOL_LA63,		// 33
	TOOL_GA68,		// 34
	TOOL_LA697, 	// 35
	TOOL_LN571,		// 36

	TOOL_LM63,
	TOOL_LM96,
	TOOL_LM95,
	TOOL_LM86,

	TOOL_LCD_END_OLD,

	//PDP Tool
	TOOL_PN670= TOOL_LCD_END_OLD,	// 1
	TOOL_PN470,					// 2
	TOOL_PM470,
	TOOL_PH670,					// 3
	TOOL_PH470,					// 4
	TOOL_PN570,					// 5
	TOOL_PH660,					// 6
	TOOL_PDP_END_OLD,
	TOOL_BASE_OLD	= TOOL_PDP_END_OLD,	// LCD ToolÀÌ Ãß°¡µÇ¾î PDP ToolÀÇ ¼öº¸´Ù´Â ¸¹À»°ÍÀ¸·Î ¿¹»óµÊ.
} OLD_TOOL_TYPE_T;


typedef enum
{
	MODULE_LGD = 0,
	MODULE_AUO,
	MODULE_SHARP,
	MODULE_BOE,
	MODULE_CSOT,
	MODULE_INNOLUX,
	MODULE_LGD_M,
	MODULE_ODM_B,
	MODULE_BOE_TPV,
	MODULE_HKC,
	MODULE_LCD_END,
	MODULE_BASE	= MODULE_LCD_END,
} MODULE_MAKER_TYPE_T;


//balup_090626
typedef enum
{
	MODULE_VER0		=0,		//not support error out, not support scanning backlight
	MODULE_VER1		=1,     //support error out, not support scanning backlight
	MODULE_VER2		=2,     //support scanning backlight, not support error out
	MODULE_VER3	 	=3		//support error out, support scanning backlight
} MODULE_VERSION_TYPE_T;


/******************************************************
 			TOOL_OPTION3 ¿¡ ÇÊ¿äÇÑ ±¸Á¶Ã¼ Á¤ÀÇ
******************************************************/
#define AMP_CHIP_TYPE_MAX			5
#define BACKLIGHT_TYPE_MAX			7

/* AMP type */
typedef enum
{
	AMP_NTP6AMP = 0,	/* NTP7514 6AMP	*/
	AMP_TAS5733,		/* TAS5733	*/
	AMP_STA380,			/* ST380	*/
	AMP_NTP7514,
	AMP_NTP2AMP,		/* NTP7514 2AMP */
	AMP_NTP3AMP,		/* NTP7514 3AMP */
	AMP_NTP4AMP,		/* NTP7514 4AMP */
	AMP_NTP5AMP,		/* NTP7514 5AMP */

	// not use
	AMP_NTP7513,	/* NTP7513	*/
	AMP_STA2AMP,		/* STA2AMP	*/
	AMP_TAS2AMP,		/* TAS5733 2AMP */
	AMP_TAS3AMP,		/* TAS5733 3AMP */
	AMP_NTP7400,	/* NTP7400  */
	AMP_NTP7500,		/* NTP7500	*/
}AMP_CHIP_TYPE_T;



/* Backlight type*/
typedef enum
{
	BL_DIRECT_L = 0,
	BL_EDGE_LED,
	BL_OLED,
	BL_DIRECT_VI,
	BL_DIRECT_SKY,
	BL_END,
}BACKLIGHT_TYPE_T;


/* Backlight type*/
typedef enum
{
	BL_ROW = BL_END,
	BL_NOR_LED,
	BL_IOL_LED,
	BL_POLA,
	BL_CCFL,

	BL_IOP_LED,
	BL_ALEF_LGD,
	BL_ALEF_FPR,
}OLD_BACKLIGHT_TYPE_T;


typedef enum
{
	PANEL_GAMUT_NORMAL = 0,
	PANEL_GAMUT_WCG_LCD,
	PANEL_GAMUT_OLED_CW16,
	PANEL_GAMUT_MAX,
}PANEL_GAMUT_TYPE_T;

/* LVDS bit */
typedef enum
{
	DISP_8BIT_LVDS = 0,
	DISP_10BIT_LVDS,
}DISPLAY_LVDS_BIT_T;


/* EDID type*/
typedef enum
{
	EDID_PCM = 0,
	EDID_AC3,
#ifdef INCLUDE_SUPPORT_HDMI_DTS
	EDID_DTS,
#else
	EDID_RESERVED,
#endif
	SUPPORT_EDID_TYPE_MAX,
}EDID_DATA_TYPE_T;


typedef enum
{
	PWM_FREQ_50_60_HZ				= 0,
	PWM_FREQ_100_120_HZ				= 1,
	PWM_FREQ_LVDS_LINK_MAX			= 2,
} PWM_FREQ_T;


/**
* sw.byeon - 100910 : Åø¿É¼Ç Union °ª ¼ø¼­ ÀçÁ¤·ÄÇÔ.
* °¢ Tool Option¿¡ Ç×¸ñ Ãß°¡½Ã, ¸Ç ¾Æ·¡¿¡ Ãß°¡ÇÒ°Í.
* À§¿¡ Ãß°¡ÇÒ °æ¿ì Bit°¡ ¹Ð¸®±â ¶§¹®¿¡ ToolOption Valid mark¸¦ º¯°æÇÏ¿© ÃÊ±âÈ­ÇÏÁö ¾ÊÀ¸¸é °ªÀÌ Æ²¾îÁö°ÔµÊ.
* ¶ÇÇÑ ¼ø¼­´ë·Î Ãß°¡ÇØ¾ß OSD ¹× Åø¿É¼Ç enumÀÇ ¼ø¼­¿Í µ¿ÀÏÇÏ°Ô µÊ.
**/

/* Ext input Adjust */
typedef struct
{
	UINT8					nHDMI1;
	UINT8					nHDMI2;
	UINT8					nHDMI3;
	UINT8					nHDMI4;
	UINT8					nCOMP1;
	UINT8					nCOMP2;
	UINT8					nCOMP3;
	UINT8					nRGB1;
	UINT8 					nVideoScartFullportA;
	UINT8					nRCA_AV1;
	UINT8					nRCA_AV2;
	UINT8					nRCA_AV3;
	UINT8					nReserved1;
	UINT8					nReserved2;
	UINT8					nReserved3;
} EXT_VIDEO_INPUT_ADJ_T;

typedef struct
{
	UINT8					nCOMP1;
	UINT8					nCOMP2;
	UINT8					nCOMP3;
	UINT8					nRGB1;
	UINT8 					nAudioScartFullportA;
	UINT8					nRCA_AV1;
	UINT8					nRCA_AV2;
	UINT8					nRCA_AV3;
	UINT8 					nReserved1;
	UINT8					nReserved2;
	UINT8					nReserved3;
} EXT_AUDIO_INPUT_ADJ_T;


/**
 *  Tool Option1 of Ez Adjust Menu(Service Menu).
 */
typedef union
{
	UINT32 all;
	struct
	{
		UINT32 					eModelInchType  		: 5;
		UINT32					eModelToolType			: 6;
		UINT32					eModelModuleType		: 5;
		UINT32					nLVDSBit				: 1;
		UINT32					bToolOpt32bit			: 1;
		UINT32					nReserved			: 14;
	} flags;

} TOOL_OPTION1_T;



/**
 *  Tool Option2 of Ez Adjust Menu(Service Menu).
 */
typedef union
{
	UINT32 all;
	struct
	{
		UINT32 					nExtVideoInputIndex		: 5;
		UINT32					nExtAudioInputIndex		: 5;
		UINT32					eBackLight			: 5;
		UINT32					nNumOfUSBInput			: 2;
		UINT32					eHDMISwitchIC			: 3;
		UINT32					bEmmcRecord				: 1;
		UINT32					eUSB1					: 2;
		UINT32					eUSB2					: 2;
		UINT32					eUSB3					: 2;
		UINT32					nReserved			: 5;
	} flags;
} TOOL_OPTION2_T;


/**
 *  Tool Option3 of Ez Adjust Menu(Service Menu).
 */
typedef union
{
	UINT32 all;
	struct
	{
		UINT32					eAudioAmp				: 4;
		UINT32					bHeadphone				: 1;
		UINT32					bLogoLight				: 1;
		UINT32					bDVRReady				: 1;
		UINT32 					bTHX					: 1;
		UINT32					bISF					: 1;
		UINT32 					bSupportEPA				: 1;
		UINT32					bDualView				: 1;
		UINT32					bPip					: 1;
		UINT32					bSupportFanControl		: 1;
		UINT32 					bLocalDimming			: 1;
		UINT32					bLocalDimMenu			: 1;
		UINT32					bLuminanceUp			: 1;
		UINT32					bsupportAudioLineOut	: 1;
		UINT32					bsupportAtvAvDvr		: 1;
		UINT32					eLocalKeyType			: 2;
		UINT32					nReserved				: 12;
	} flags;
} TOOL_OPTION3_T;


/**
 *  Tool Option4 of Ez Adjust Menu(Service Menu).
 */

typedef union
{
	UINT32 all;
	struct
 	{

		UINT32					eDigitalDemod_S 		: 2;
		UINT32					eDigitalDemod			: 4;
		UINT32					eAnalogDemod			: 3;
		UINT32					eDigitalDemod_Sub		: 4;
		UINT32					eAnalogDemod_Sub		: 3;
		UINT32					nReserved				: 16;
	} flags;
} TOOL_OPTION4_T;

/**
 *  Tool Option5 of Ez Adjust Menu(Service Menu).
 */
typedef union
{
	UINT32 all;
	struct
	{
		UINT32					eSupportWiFi			: 3;
		UINT32					eSupportCameraReady		: 2;
		UINT32					eSupportMouseRC			: 3;
		UINT32					bTVLink 				: 1;
		UINT32					bMirrorMode 			: 1;
		UINT32					bSupportCompRCACommon	: 1;
		UINT32					eSoundMode				: 2;
		UINT32					bSupportHDMI2ExtEdid	: 1;
		UINT32					bScanningBL				: 1;
		UINT32					bSupportWOL				: 1;
		UINT32					bClearPlus				: 1;
		UINT32					nReserved				: 15;
	} flags;

} TOOL_OPTION5_T;


/**
 *  Tool Option6 of Ez Adjust Menu(Service Menu).
 */
typedef union
{
	UINT32 all;
	struct
	{
		UINT32					nDefaultStdBacklight	: 4;
		UINT32					nMotionEyeCare			: 4;
		UINT32 					nAudioPowerVoltage		: 3;
		UINT32 					nSpeakerWattage			: 2;
		UINT32 					eMaxPwmType				: 3;
		UINT32 					ePwmDuty				: 3;
		UINT32					nReserved				: 13;
	} flags;
} TOOL_OPTION6_T;


/**
 *  Tool Option7 of Ez Adjust Menu(Service Menu).
 */
typedef union
{
	UINT32 all;
	struct
	{
		UINT32					bMAC_AP					: 1;
		UINT32					bESN_AP					: 1;
		UINT32					bCI_AP					: 1;
		UINT32					bWIDEVINE_AP			: 1;
		UINT32 					bMARLIN_AP				: 1;
		UINT32					bSupportDTCPKey			: 1;
		UINT32 					eEDID_Type				: 2;
		UINT32					bSupportMHL				: 1;
		UINT32					eDigitalEye				: 2;
		UINT32					bPowerBoardType			: 1;
		UINT32					eLEDBarType				: 3;
		UINT32					eInstantBoot			: 2;
		UINT32					bPWMFreq				: 1;
		UINT32					bMagicSpaceSound		: 1;
		UINT32					bInstantBootDefaultValue	: 1;
		UINT32					eEyeCurveDerivation		: 3;
		UINT32					nReserved				: 9;
	} flags;
} TOOL_OPTION7_T;



/**
 *  Tool Option8 of Ez Adjust Menu(Service Menu).
 */
typedef union
{
	UINT32 all;
	struct
	{
		UINT32					nReserved				: 32;
	} flags;
} TOOL_OPTION8_T;

/**
 *  Tool Option9 of Ez Adjust Menu(Service Menu). - Tool Option8 is Commercial TV ToolOption
 */
typedef union
{
	UINT32 all;
	struct
	{
		UINT32					eBPLCurrentType			: 3;
		UINT32					eModeSelect				: 2;
		UINT32					bExternalSpeaker		: 1;
		UINT32					bMovingSpeaker			: 1;
		UINT32					ePanelGamutType			: 3;
		UINT32					bTunerSleepMode			: 1;
		UINT32					eCellType				: 1;
		UINT32					bTwinTV					: 1;
		UINT32					eTconPmicType			: 3;
		UINT32					bAdjustVCOM				: 1;
		UINT32					bHDR					: 1;
		UINT32					bOledJB					: 1;
		UINT32					bOledOffRsQuickStart	: 1;
		UINT32					bOledTconOrbit			: 1;
		UINT32					bSupportDolbyVisionHDR	: 1;
		UINT32					bSupportBNO				: 1;	//17Y RMS_5680
		UINT32					nReserved				: 9;
	} flags;
} TOOL_OPTION9_T;


// stonedef - 090717
// Area option ±¸Á¶Ã¼ Á¤ÀÇ
typedef union
{
	UINT16 all;
	struct
	{
		UINT16					eWiFiFreq		: 5;		/* Wi-Fi Frequency */
		UINT16					eLocation		: 3;		/* ¾ÆÁÖ ±¹°¡ Áß Æ¯º°ÇÑ settingÀÌ ÇÊ¿äÇÒ °æ¿ì optionÀ¸·Î »ç¿ë, 1 : India(for audio setting), 2 ~ 7 : reserved */
		UINT16					bNordicUI		: 1;		/* 00:Non-Nordic(UK/Ireland+Others), 01:Nordic */
		UINT16					bHwOption		: 1;
		UINT16					bIsolator		: 1;		/* Isolator Áö¿ø À¯¹« */
		UINT16					bAJJAArea		: 1;		/* ¾ÆÁÖÁß¾Æ option */
		UINT16					eTTXLangGroup	: 3;		/* Teletext Language */
		UINT16					bC2				: 1;		/* C2 Áö¿ø ¸ðµ¨ Check */
	}flags;
}AREA_OPTION_T;



typedef union
{
	UINT16 all;
	struct
	{
		UINT16					nNordicUI			: 1;			/* 00:Non-Nordic(UK/Ireland+Others), 01:Nordic */
		UINT16 					bHwOption			: 1;
		UINT16					bT2					: 1;			/* T2 Áö¿ø ¸ðµ¨ Check_Hardware_Option */
		UINT16					bC2					: 1;			/* C2 Áö¿ø ¸ðµ¨ Check_Hardware_Option */
		UINT16					bSatellite			: 1;			/* À§¼ºÁö¿ø ¸ðµ¨ Check_Hardware_Option */
		UINT16					bMHP				: 1;			/* MHP Áö¿ø ¸ðµ¨ */
		UINT16					nHbbtv				: 2;			/* 00:Hbbtv Disable, 01:¹ÌÁ¤, 10:Enable, 11:reserve */
		UINT16					bIsolator			: 1;			/* isolator Áö¿ø À¯¹« */
		UINT16					eEastWest_EU		: 2;			/* µ¿/¼­ À¯·´ ±¸ºÐ */
		UINT16					bArabicLang			: 1;			/* ArabicLang support */
		UINT16					bAJJAArea			: 1;			/* ¾ÆÁÖÁß¾Æ option */
		UINT16					nTTXLang			: 3;			/* ¾ÆÁÖÁß¾Æ Teletext Language */
	}flags;
}AREA_OPTION_EU_T;





/**
 *	TOOL OPTION struct type
 */
typedef struct
{
	UINT32              	validMark;
	UINT32              	version;
	TOOL_OPTION1_T			nToolOption1;
	TOOL_OPTION2_T			nToolOption2;
	TOOL_OPTION3_T			nToolOption3;
	TOOL_OPTION4_T			nToolOption4;
	TOOL_OPTION5_T			nToolOption5;
	TOOL_OPTION6_T			nToolOption6;
	TOOL_OPTION7_T			nToolOption7;
	TOOL_OPTION8_T			nToolOption8;
	TOOL_OPTION9_T			nToolOption9;
	AREA_OPTION_T			stAreaOptions;		/* Area Option for ¾ÆÁÖ/Áß¾Æ/Áß³²¹Ì */
	AREA_OPTION_EU_T		stEuAreaOptions;	/* Area Option for ±¸ÁÖ */
	EXT_VIDEO_INPUT_ADJ_T	stExtVideoInputADJ;
	EXT_AUDIO_INPUT_ADJ_T	stExtAudioInputADJ;

}	TOOLOPTION_DB_T;



/**
*	MODEL INFO DB ( ¸ðµ¨ Á¤º¸ °ü·Ã DB )
*/
typedef struct MODEL_INFO_DB
{
	UINT32		validMark;
	UINT8		aModelName[MODELNAME_SIZE_MAX];
	UINT8		aSerialNum[BAR_CODE_SIZE_MAX];
	UINT8		group_code;      /* country group code which is defined in "common/include/country.h" */
	UINT32		country_code;    /* country code which is defined in "common/include/country_codes.h" */
	UINT32		city_code;    /* city code which is defined in "common/include/country_codes.h" *//*zhangze 0718*/
}MODEL_INFO_DB_T;



/**
 *	BAUDRATE struct.
 */
typedef enum
{
	BAUDRATE_2400		= 0,
	BAUDRATE_4800		= 1,
	BAUDRATE_9600		= 2,
	BAUDRATE_14400		= 3,
	BAUDRATE_19200		= 4,
	BAUDRATE_38400		= 5,
	BAUDRATE_57600		= 6,
	BAUDRATE_115200 	= 7,
	BAUDRATE_460800 	= 8

}	SYS_BAUDRATE_T;



/**
* PRESERVE DB ( ¾ç»ê ÀÌÈÄ¿¡ Reset µÇ°Å³ª ÀÓÀÇ·Î º¯°æµÇÁö ¸»¾Æ¾ß ÇÏ´Â DB )
*/
typedef struct PRESERVE_DB
{
	UINT32			validMark;
	UINT8 			bInstopReserved;
	UINT8 			bInstopCompleted;
	SYS_BAUDRATE_T	eBaudrate;
	UINT8			b1stBootAfterInstop;
	UINT8			b1stBootAfterFactoryReset;
}	PRESERVE_DB_T;


/**
*	MAC ADDRESS back-up data
*	by ieeum, zaewon.lee.
*/
typedef struct MAC_ADDRESS
{
	UINT32		validMark;
	UINT8		macAddr[17];
}MAC_ADDRESS_T;

extern MAC_ADDRESS_T gMacAddress;

typedef enum
{
	SOUNDOUT_TVSPEAKER = 0,
	SOUNDOUT_EXTERNALSPEAKER,
	SOUNDOUT_OPTICAL_SOUNDBAR,
	SOUNDOUT_HEADPHONE,
	SOUNDOUT_LINEOUT,
	SOUNDOUT_BLUETOOTH_SOUNDBAR,
} SOUNDOUT_MODE_T;

/****************************************************
*** MICOM DB ***************************************/
typedef struct
{
		UINT8				PWMCount;
		UINT8				UartBaudRate;			//enum:UART_BAUDRATE_TYPE
		UINT8				ModelID;
		UINT8				LastPowerSaveStatus ;	//enum:POWER_STATUS_SAVE
		UINT8				PowerOnlyStatus;		//enum:POWER_ONLY_STATUS
		UINT8				Reserve1;
		UINT8				Reserve2;
		UINT8				DataCheckFlag;
}PM_SAVE_DATA_SYSTEM_T; // 8Bytes

typedef struct
{
	UINT8	DemoModeEnable;
	UINT8	PowerLightEnable;
	UINT8	StandByLightEnable;
	UINT8	LogoLightEnable;
	UINT8	ChildLockEnable;
	UINT8	CECEnable;
	UINT8	CECSyncPower;
	UINT8	OffOnKeyLockTime;
	UINT8	InstopFlag;
	UINT8	WOLOnOff;
	UINT8	WOWOnOff;
	UINT8	WOLActiveMode;
	UINT8	WOWActiveMode;
	UINT8	UserInstopFlag;
	UINT8	BDPCheckCnt;
	UINT8	IsJpMicomFlag;
	UINT8	InstantBootMode;
	UINT8 	PMUpdateFlag;
	UINT8 	WOBLEOnOff;
	UINT8	DataCheckFlag;
}PM_SAVE_DATA_OPTION_T;

typedef struct
{
	UINT8	PowerOnStatus;		//enum:LAST_POWER_STATUS_T
	UINT8	KeyManagement;		//enum:KEY_MANAGEMENT_T
	UINT8	SetID;
	UINT8	DataCheckFlag;
}PM_SAVE_DATA_COMMERCIAL_T; // 4Byte

typedef struct
{
	UINT8	PwrOnOffHistCurPoint;
	UINT8	PwrOnOffHistCount;
	UINT8	Reserve1;
	UINT8	DataCheckFlag;
}PM_SAVE_DATA_HISTORY_T; // 4Bytes

typedef struct
{
	UINT8 stdLogoDuty;
	UINT8 pwrLogoDuty;
	UINT8 ReserveStatus;
	UINT8 DataCheckFlag;
}PM_SAVE_DATA_LOGO_T; // 4Bytes

typedef struct
{
	UINT16   panel_ctrl;
	UINT16   eth_enable;
	UINT16   wifi_enable;
	UINT8    soc_init;
	UINT8    rl_on;
	UINT8    led_ctrl;
	UINT8    ac_stable_check;
	UINT8    soc_run;
	UINT8    power_on_21;
	UINT8    power_on_22;
	UINT8    power_on_23;
	UINT8    frc_on;
	UINT8    active;
	UINT16	 combo_reset;
	UINT8	 ontime_reserve1;
	UINT8	 ontime_reserved2;
}PM_SAVE_DATA_POW_ON_TIME_T; // 16Bytes

typedef struct
{
	UINT16   soc_off;
	UINT16   save_mode;
	UINT8    relay_off;
	UINT8    standby_off;
	UINT8    abn_rl_off;
	UINT8    abn_power_off_21;
	UINT8    abn_power_off_22;
	UINT8    abn_power_off_23;
	UINT8    power_off_21;
	UINT8    power_off_22;
	UINT8    power_off_23;
	UINT8    eth_enable;
	UINT8    panel_ctrl;
	UINT8    oneshot_rl_off;
	UINT16	 combo_reset;
	UINT8	 offtime_reserve1;
	UINT8	 offtime_reserved2;
}PM_SAVE_DATA_POW_OFF_TIME_T; // 16Bytes

typedef struct
{
	UINT8 Reserve1;
	UINT8 Reserve2;
	UINT8 Reserve3;
	UINT8 Reserve4;
	UINT8 Reserve5;
	UINT8 Reserve6;
	UINT8 Reserve7;
	UINT8 Reserve8;
	UINT8 Reserve9;
	UINT8 Reserve10;
	UINT8 Reserve11;
	UINT8 Reserve12;
	UINT8 Reserve13;
	UINT8 Reserve14;
	UINT8 Reserve15;
	UINT8 Reserve16;
	UINT8 Reserve17;
	UINT8 Reserve18;
	UINT8 Reserve19;
	UINT8 Reserve20;
}PM_SAVE_DATA_NEW_OPTION_T; // 20Bytes

typedef struct
{
	UINT32						validMark;
    PM_SAVE_DATA_SYSTEM_T		stPMSaveDatasystem;
	PM_SAVE_DATA_OPTION_T		stPMSaveDataOption;
	PM_SAVE_DATA_COMMERCIAL_T	stPMSaveDataCommercial;
	PM_SAVE_DATA_HISTORY_T		stPMSaveDataHistory;
	PM_SAVE_DATA_LOGO_T			stPMSaveDataLogo;
	PM_SAVE_DATA_POW_ON_TIME_T	stPMSaveDataPowOnTime;
	PM_SAVE_DATA_POW_OFF_TIME_T	stPMSaveDataPowOffTime;
	PM_SAVE_DATA_NEW_OPTION_T	stPMSaveDataNewOption;
	UINT8 						PwrOffHistory[128]; //128Bytes
} MICOM_DB_T;

/**	1st Boot After Instop State enum */
enum
{
	BOOT_STATE_NOT_1ST_BOOT = 0,	// Not First boot after instop!!!
	//BOOT_STATE_AFTER_SYSTEM_INIT,		// not use
	//BOOT_STATE_20S_AFTER_UI_INIT,		// not use
	BOOT_STATE_AFTER_INSTOP,		// First boo after instop!!!
};


#ifdef	__cplusplus
}
#endif /* __cplusplus */

#endif /*_CMNIO_TYPE_H_*/




