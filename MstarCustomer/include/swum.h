#include <command.h>
#include <partinfo.h>
#include <environment.h>
#include <cmd_resume.h>

#define MAGIC(a,b,c,d)	((a) | (b) << 8 | (c) << 16 | (d) << 24)
#define SWUM_MAGIC		MAGIC('S', 'W', 'U', 'M')
#define BACK_MAGIC		MAGIC('B','A','C','K')
#define LICE_MAGIC		MAGIC('L','I','C','E')

extern int get_swumode(void);
extern int get_backupmode(void);
