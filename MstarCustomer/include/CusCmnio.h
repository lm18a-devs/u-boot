#include <common.h>
#include <MsTypes.h>

//-----------------------------------------------------------------------------
// Configurations
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// Constant definitionss
//-----------------------------------------------------------------------------
#define GPIO_NA             0 /* GPIO0 is nothing */
#define GPIO_MODEL_OPT0_INT     170
#define GPIO_MODEL_OPT1_INT     169
#define GPIO_MODEL_OPT0     52
#define GPIO_MODEL_OPT1     53
#define GPIO_MODEL_OPT2     54
#define GPIO_MODEL_OPT3     55
#define GPIO_MODEL_OPT4     56
#define GPIO_MODEL_OPT5     57
#define GPIO_MODEL_OPT6     31
#define GPIO_MODEL_OPT7     32
#define GPIO_MODEL_OPT8     33
#if 0 // not used
#define GPIO_MODEL_OPT9     73
#define GPIO_MODEL_OPT10    74
#define GPIO_MODEL_OPT11    75
#define GPIO_MODEL_OPT12    76
#define GPIO_MODEL_OPT13    77
#define GPIO_MODEL_OPT14    43
#endif

#define NUM_MODEL_OPT		8 //6 -> 8
#define NUM_HW_OPT			18
#define NUM_MODELOPT_BITCOMB	4
#define NUM_BACKEND_OPT_LEV		4
#define NUM_BACKEND_OPT_BITCOMB	16


#define GPIO_PORT_WR_OTP_WR_CTRL                GPIO_NA
#define GPIO_PORT_WR_LOCAL_DIM_OS_PANEL_CTRL    GPIO_NA
#define GPIO_PORT_WR_DATA_FORMAT_0              GPIO_NA
#define GPIO_PORT_WR_DATA_FORMAT_1              GPIO_NA
#define GPIO_PORT_WR_CVBS_OUT_SEL               GPIO_NA
#define GPIO_PORT_WR_MN864778_RESET             GPIO_NA
#define GPIO_PORT_WR_TCON_I2C_EN                GPIO_NA
#define GPIO_PORT_WR_LOCAL_DIMMING_EN           GPIO_NA
#define GPIO_PORT_WR_3D_EN                      GPIO_NA
//#define GPIO_PORT_WR_USB_CTL1	                (gModelOpt.micom_type == 1)? (85):(18)
#define GPIO_PORT_WR_USB_CTL2	                87//(gModelOpt.micom_type == 1)? (87):(87)
#define GPIO_PORT_WR_USB_CTL3	                79//(gModelOpt.micom_type == 1)? (79):(79)

#define GPIO_PORT_WR_FLASH_WP	                (47)
#define NVM_I2C_CH                  0
#define MICOM_I2C_CH                1
#define MAX_I2C_CH                  2

#define ADC_MODEL_OPT_LV_0    55
#define ADC_MODEL_OPT_LV_1    118
#define ADC_MODEL_OPT_LV_2    175
#define ADC_MODEL_OPT_LV_3    231
#define ADC_MODEL_OPT_LV_MAX  255

#define ADC_MODEL_OPT_0   ( (ADC_MODEL_OPT_LV_1   - ADC_MODEL_OPT_LV_0) / 2 + ADC_MODEL_OPT_LV_0 )
#define ADC_MODEL_OPT_1   ( (ADC_MODEL_OPT_LV_2   - ADC_MODEL_OPT_LV_1) / 2 + ADC_MODEL_OPT_LV_1 )
#define ADC_MODEL_OPT_2   ( (ADC_MODEL_OPT_LV_3   - ADC_MODEL_OPT_LV_2) / 2 + ADC_MODEL_OPT_LV_2 )
#define ADC_MODEL_OPT_3   ( (ADC_MODEL_OPT_LV_MAX - ADC_MODEL_OPT_LV_3) / 2 + ADC_MODEL_OPT_LV_3 )

//-----------------------------------------------------------------------------
// Macros
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// Type definitions
//-----------------------------------------------------------------------------
/**
 *	Type of PANEL (HW OPTION)
 */
typedef enum
{
    MODELOPT_PANEL_TYPE_NONE        = 0,
    MODELOPT_PANEL_V12              = 1,
    MODELOPT_PANEL_V13              = 2,
    MODELOPT_PANEL_V14              = 3,
    MODELOPT_PANEL_V15				= 4,
    MODELOPT_PANEL_V16				= 5,
    MODELOPT_PANEL_V17				= 6,
    MODELOPT_PANEL_V18				= 7,
    MODELOPT_PANEL_MAX,
}MODELOPT_PANEL_T;

typedef enum  
{
    MODELOPT_MODULE_TYPE_NONE		= 0,
    MODELOPT_MODULE_TYPE_RGB		= 1,
    MODELOPT_MODULE_TYPE_RGBW		= 2,
    MODELOPT_MODULE_TYPE_MPLUS		= 3,
    MODELOPT_MODULE_TYPE_MAX,
}MODELOPT_MODULE_T;


/**
 *	Type of PANEL INTERFACE (HW OPTION)
 */
typedef enum
{
	MODELOPT_PANEL_INTERFACE_EPI	= 0,
	MODELOPT_PANEL_INTERFACE_LVDS	= 1,
	MODELOPT_PANEL_INTERFACE_VBYONE	= 2,
	MODELOPT_PANEL_INTERFACE_CEDS   = 3,
	MODELOPT_PANEL_INTERFACE_EPI_QSAC = 4,
	MODELOPT_PANEL_INTERFACE_MAX
}MODELOPT_PANEL_INTERFACE_T;

/**
 *	Type of PANEL Bandwidth (HW OPTION)
 */
typedef enum
{
	MODELOPT_PANEL_BW_NONE = 0,
	MODELOPT_PANEL_BW_1_5G = 1,
	MODELOPT_PANEL_BW_2_1G = 2,
	MODELOPT_PANEL_BW_3_0G = 3,
	MODELOPT_PANEL_BW_MAX
}MODELOPT_PANEL_BW_T;

/**
 *	Type of PANEL RESOLUTION (HW OPTION)
 */
typedef enum
{
	MODELOPT_PANEL_RESOLUTION_HD	= 0,
	MODELOPT_PANEL_RESOLUTION_FHD	= 1,
	MODELOPT_PANEL_RESOLUTION_UD	= 2,
	MODELOPT_PANEL_RESOLUTION_MAX
}MODELOPT_PANEL_RESOLUTION_T;

/**
 *	Type of DDR SIZE (HW OPTION)
 */
typedef enum
{
	DDR_SIZE_768M       = 0,
        DDR_SIZE_1G         = 1,
        DDR_SIZE_1_25G      = 2,
        DDR_SIZE_1_5G       = 3,
        DDR_SIZE_2G         = 4,
	DDR_SIZE_2_5G       = 5,
        DDR_SIZE_3G         = 6,
        DDR_SIZE_2G_ADV     = 7,
        DDR_SIZE_2G_STD     = 8,
	DDR_SIZE_MAX
}MODELOPT_DDR_SIZE_T;

typedef enum
{
	MODELOPT_DISPLAY_LCD	= 0,
	MODELOPT_DISPLAY_OLED	= 1,
	MODELOPT_DISPLAY_PDP	= 2,
	MODELOPT_DISPLAY_MAX	= 3
}MODELOPT_DISPLAY_T;

typedef enum
{
	MODELOPT_FRC_URSA_NONE = 0,
	MODELOPT_FRC_URSA7,
	MODELOPT_FRC_URSA9,
	MODELOPT_FRC_URSA9_P,
	MODELOPT_FRC_URSA11,
	MODELOPT_FRC_URSA11_P,
	MODELOPT_FRC_F16,
	MODELOPT_FRC_MAX
}MODELOPT_FRC_T;

typedef enum
{
	MODELOPT_PMIC_FALSE	= 0,
	MODELOPT_PMIC_TRUE	= 1,
	MODELOPT_PMIC_PMIC_MAX_OPT,
}PMIC_MODEL_OPT_T;


typedef enum
{
	MODELOPT_GRAPHIC_1024X768	= 0,
	MODELOPT_GRAPHIC_1366X768	= 1,	// panel resolution == HD
	MODELOPT_GRAPHIC_1920X1080	= 2,	// panel resolution == FHD or panel resolution == UHD
	MODELOPT_GRAPHIC_2560X1080	= 3,	// panel resolution == WUHD
	MODELOPT_GRAPHIC_3840X2160	= 4,	// panel resolution == UHD
	MODELOPT_GRAPHIC_5120X2160	= 5,	// panel resolution == WUHD
	MODELOPT_GRAPHIC_1280X720	= 6
}MODELOPT_GRAPHIC_RESOLUTION_T;

typedef enum
{
	MODELOPT_MICOM_EXTERNAL	= 0,
	MODELOPT_MICOM_INTERNAL,
	MODELOPT_MICOM_TYPE_MAX
}MODELOPT_MICOM_TYPE_T;

typedef enum
{
	MODELOPT_MICOM_VENDOR_NONE	= 0,
	MODELOPT_MICOM_VENDOR_RENESAS,
	MODELOPT_MICOM_VENDOR_ABOV,
	MODELOPT_MICOM_VENDOR_MAX
}MODELOPT_MICOM_VENDOR_T;

typedef struct modelopt_bitcomb
{
	MODELOPT_PANEL_INTERFACE_T	panel_interface;
	MODELOPT_PANEL_RESOLUTION_T	panel_resolution;
	uint8_t						bSupport_frc;
	MODELOPT_PANEL_T			panel_type;
	uint8_t						bSupport_cp_box;
	uint8_t						reserved;
	uint8_t						bSupportOptic;
}MODELOPT_BITCOMB_T;

/**
 *	HW OPTION
 */
typedef struct modelopt_t
{
	MODELOPT_PANEL_INTERFACE_T	panel_interface;
	MODELOPT_PANEL_RESOLUTION_T	panel_resolution;
	MODELOPT_PANEL_T			panel_type;
	MODELOPT_PANEL_BW_T			panel_bandwidth;
	MODELOPT_DDR_SIZE_T			ddr_size;
	MODELOPT_FRC_T              frc_type;
	MODELOPT_MICOM_TYPE_T		micom_type;
    PMIC_MODEL_OPT_T			pmic_type;
	MODELOPT_MODULE_T			module_type;
	uint8_t						country_type;
	uint8_t						bSupport_frc;
	uint8_t						bSupport_cp_box;
	uint8_t						tuner_type;
	uint8_t						bSupportOptic;
	uint8_t						graphic_resolution;
	uint8_t						bSupport_external_EDID;
	uint8_t						bSupport_URSA;
	uint8_t						isSupportEWBS;
	MODELOPT_MICOM_VENDOR_T		micom_vendor;
}MODELOPT_T;

/*
 *  ADC H/W model opt
 */
typedef struct adc_backend_opt_t{
	MODELOPT_PANEL_RESOLUTION_T	panel_resolution;
	MODELOPT_PANEL_INTERFACE_T	panel_interface;
	MODELOPT_PANEL_T			panel_type;
	MODELOPT_MODULE_T			module_type;
	MODELOPT_PANEL_BW_T			panel_bandwidth;
}ADC_BACKEND_OPT_T;

/**
 *	PWM Driving mode
 *	(from os/common_linux/dil/include/dil_lcdport.h)
 */
typedef enum{
	PWM_DRIVING_LED_CURRENT		= 0,	/* PWM1(global dimming )+ PWM2(led current) */
	PWM_DRIVING_2CH_PHASE_SAME	= 1,	/* PWM1(global dimming) + PWM2(global dimming) : same phase */
	PWM_DRIVING_2CH_PHASE_DIFF	= 2,	/* PWM1(global dimming) + PWM2(global dimming) : diffrent phase */
	PWM_DRIVING_MODE_MAX		= 3
}PWM_DRIVING_MODE_T;

/**
 * PWM INDEX FOR SCANNING
 *  (from os/common_linux/dil/include/dil_lcdport.h)
 */
typedef enum
{
	PWM_SCANNING_IDX_0		= 3,	// main
	PWM_SCANNING_IDX_1		= 2,	// sub
	PWM_SCANNING_IDX_MAX	= 2
} PWM_SCANNING_IDX_T;

#define H_MIRROR_PATCH_EPI 1 //Temp EPI patch
#define TCON_TEMP_PATCH 0//Tcon_Patch
#define TCON_NUM_MAX 16
#define TCON_MPLUS_PATCH 1 //Tcon_MPLUS patch
typedef enum
{
	E_TCON_NONE = 0,/*0*/ 
	E_TCON_V18_EPI_49INCH_6LANE_MPLUSOFF,/*1*/
	E_TCON_V18_EPI_49INCH_6LANE_MPLUSON,/*2*/
	E_TCON_V18_EPI_55INCH_6LANE_MPLUSOFF,/*3*/
	E_TCON_V18_EPI_55INCH_6LANE_MPLUSON,/*4*/
	E_TCON_V18_EPI_65INCH_8LANE_MPLUSOFF,/*5*/
	E_TCON_V18_EPI_65INCH_8LANE_MPLUSON,/*6*/
	E_TCON_V18_CEDS_65INCH_12LANE_MPLUSOFF,/*7*/
	E_TCON_V18_CEDS_55INCH_12LANE_MPLUSON,/*8*/
	E_TCON_V18_EPI_43INCH_6LANE_MPLUSOFF,/*9*/
	E_TCON_V18_EPI_43INCH_6LANE_MPLUSON,/*10*/
	E_TCON_V18_CEDS_43INCH_12LANE_MPLUSON,/*11*/
	E_TCON_V18_CEDS_49INCH_12LANE_MPLUSON,/*12*/
	E_TCON_V17_EPI_55INCH_6LANE_UK6500PVA_MPLUSON,/*13*//*/LMTASKWBS-72838*/
	E_TCON_MAX,

} EN_TCON_TYPE_IDX_T;


extern MODELOPT_T gModelOpt;
extern char aHW_model_opt[];
extern char strModelOpt[];

//extern int msI2C_init(void);
extern MS_BOOL MApi_SWI2C_WriteBytes(MS_U16 u16BusNumSlaveID, MS_U8 u8addrcount, MS_U8* pu8addr, MS_U16 u16size, MS_U8* pu8data);
extern MS_BOOL MApi_SWI2C_ReadBytes(MS_U16 u16BusNumSlaveID, MS_U8 u8AddrNum, MS_U8* paddr, MS_U16 u16size, MS_U8* pu8data);

extern int DDI_CMNIO_GetMicomType(void);
extern int DDI_CMNIO_I2C_Init(void);
extern int DDI_CMNIO_MBX_Init(void);
extern int DDI_CMNIO_MBX_Write(MS_U8 u8Cmd, MS_U8 u8Cnt, MS_U8 *pu8Parameters);
extern int DDI_CMNIO_MBX_Read(MS_U8 u8Cmd, MS_U8 *data, MS_U8 len);

extern int CMNIO_GPIO_SetOutputPort(uint portIndex, uchar data);
