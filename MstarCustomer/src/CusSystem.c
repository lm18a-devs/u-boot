//<MStar Software>
//******************************************************************************
// MStar Software
// Copyright (c) 2010 - 2012 MStar Semiconductor, Inc. All rights reserved.
// All software, firmware and related documentation herein ("MStar Software") are
// intellectual property of MStar Semiconductor, Inc. ("MStar") and protected by
// law, including, but not limited to, copyright law and international treaties.
// Any use, modification, reproduction, retransmission, or republication of all
// or part of MStar Software is expressly prohibited, unless prior written
// permission has been granted by MStar.
//
// By accessing, browsing and/or using MStar Software, you acknowledge that you
// have read, understood, and agree, to be bound by below terms ("Terms") and to
// comply with all applicable laws and regulations:
//
// 1. MStar shall retain any and all right, ownership and interest to MStar
//    Software and any modification/derivatives thereof.
//    No right, ownership, or interest to MStar Software and any
//    modification/derivatives thereof is transferred to you under Terms.
//
// 2. You understand that MStar Software might include, incorporate or be
//    supplied together with third party`s software and the use of MStar
//    Software may require additional licenses from third parties.
//    Therefore, you hereby agree it is your sole responsibility to separately
//    obtain any and all third party right and license necessary for your use of
//    such third party`s software.
//
// 3. MStar Software and any modification/derivatives thereof shall be deemed as
//    MStar`s confidential information and you agree to keep MStar`s
//    confidential information in strictest confidence and not disclose to any
//    third party.
//
// 4. MStar Software is provided on an "AS IS" basis without warranties of any
//    kind. Any warranties are hereby expressly disclaimed by MStar, including
//    without limitation, any warranties of merchantability, non-infringement of
//    intellectual property rights, fitness for a particular purpose, error free
//    and in conformity with any international standard.  You agree to waive any
//    claim against MStar for any loss, damage, cost or expense that you may
//    incur related to your use of MStar Software.
//    In no event shall MStar be liable for any direct, indirect, incidental or
//    consequential damages, including without limitation, lost of profit or
//    revenues, lost or damage of data, and unauthorized system use.
//    You agree that this Section 4 shall still apply without being affected
//    even if MStar Software has been modified by MStar in accordance with your
//    request or instruction for your use, except otherwise agreed by both
//    parties in writing.
//
// 5. If requested, MStar may from time to time provide technical supports or
//    services in relation with MStar Software to you for your use of
//    MStar Software in conjunction with your or your customer`s product
//    ("Services").
//    You understand and agree that, except otherwise agreed by both parties in
//    writing, Services are provided on an "AS IS" basis and the warranty
//    disclaimer set forth in Section 4 above shall apply.
//
// 6. Nothing contained herein shall be construed as by implication, estoppels
//    or otherwise:
//    (a) conferring any license or right to use MStar name, trademark, service
//        mark, symbol or any other identification;
//    (b) obligating MStar or any of its affiliates to furnish any person,
//        including without limitation, you and your customers, any assistance
//        of any kind whatsoever, or any information; or
//    (c) conferring any license or right under any intellectual property right.
//
// 7. These terms shall be governed by and construed in accordance with the laws
//    of Taiwan, R.O.C., excluding its conflict of law rules.
//    Any and all dispute arising out hereof or related hereto shall be finally
//    settled by arbitration referred to the Chinese Arbitration Association,
//    Taipei in accordance with the ROC Arbitration Law and the Arbitration
//    Rules of the Association by three (3) arbitrators appointed in accordance
//    with the said Rules.
//    The place of arbitration shall be in Taipei, Taiwan and the language shall
//    be English.
//    The arbitration award shall be final and binding to both parties.
//
//******************************************************************************
//<MStar Software>

#include <exports.h>
#include <config.h>
#include <CusSystem.h>
#include <uboot_mmap.h>
#include <ShareType.h>
#include <MsVfs.h>
#include <MsUtility.h>
#include <MsApiMiu.h>
#include <MsDebug.h>
#if defined (CONFIG_SET_4K2K_MODE)
#include <apiPNL.h>
#include <apiGOP.h>
#include <MsBoot.h>
#endif
#if defined (CONFIG_URSA_6M40)
#include <ursa/ursa_6m40.h>
#endif
#if defined (CONFIG_URSA_8)
#include <ursa/ursa_8.h>
#endif
#include <MsOS.h>
#include <CusConfig.h>
#include <CusDevice.h>
#include <partinfo.h>
#include <x_typedef.h>
#include <lg_modeldef.h>

#ifdef CONFIG_HIBERNATION
#include <cmd_resume.h>
#endif

#if defined (CONFIG_INX_VB1)
#include <panel/panel_INX_vb1.h>
#elif defined (CONFIG_INX_NOVA_VB1)
#include <panel/panel_INX_NOVA_vb1.h>
#endif

#if defined (CONFIG_URSA6_VB1)
#include <apiPNL.h>
#include <MsMmap.h>
#include <MsSystem.h>
#include <mstarstr.h>
#include <ursa/ursa_6m38.h>
#include <bootlogo/MsPoolDB.h>
#endif
#include <MsSecureBoot.h>

#include <thread.h>
#include "cmd_snaplog.h"
#include <CusCmnio.h>
#include <asm/mmapger.h>
#include <swum.h>
#include <common.h>
MMapInfo_t MMAPGer[MMAP_ID_MAX];
MiuInfo_t  MiuInfo;

extern MODELOPT_T   gModelOpt;
extern MMapInfo_t __mmap_256mb_256mb[MMAP_ID_MAX];
extern MMapInfo_t __mmap_512mb_256mb[MMAP_ID_MAX];
extern MMapInfo_t __mmap_512mb_512mb[MMAP_ID_MAX];
extern MMapInfo_t __mmap_1024mb_0mb[MMAP_ID_MAX];
extern MMapInfo_t __mmap_512mb_1024mb[MMAP_ID_MAX];
extern MMapInfo_t __mmap_1024mb_1024mb[MMAP_ID_MAX];

extern MiuInfo_t __miu_256mb_256mb;
extern MiuInfo_t __miu_512mb_256mb;
extern MiuInfo_t __miu_512mb_512mb;
extern MiuInfo_t __miu_1024mb_0mb;
extern MiuInfo_t __miu_512mb_1024mb;
extern MiuInfo_t __miu_1024mb_1024mb;

extern char* update_list[UPDATE_PARTITION_MAX];

extern int snprintf(char *str, size_t size, const char *fmt, ...);

#ifdef CONFIG_MSTAR_CMA
/* extenal structures defined in MMAP_xx.c */
extern CMA_MMapInfo_t __cma_mmap_1024mb_0mb[CMA_MMAP_ID_MAX];
extern CMA_MMapInfo_t __cma_mmap_512mb_1024mb[CMA_MMAP_ID_MAX];
CMA_MMapInfo_t CMAInfo[CMA_MMAP_ID_MAX];
#endif /* CONFIG_MSTAR_CMA */

#if CONFIG_RESCUE_ENV
char *CUS_BOOT_RECOVER_ENV_LIST[] =
{
    NULL
};
#endif

extern int megic_number_cleaned ;

#if defined(CONFIG_MULTICORES_PLATFORM)
#define SMP_WAIT_NON_BOOT_CPU_RELEASE_COUNT 10000
extern thread_t *thread[];
#endif
#if defined(USE_TEE_THREAD)
thread_t *thread_tee = NULL;
#endif


#if CONFIG_MSTAR_STR_ENABLE
#ifdef CONFIG_MSTAR_STR_CRC //calculate CRC for STR dbg
#define BIT0 0x01
#define BIT3 0x08
#define BDMA_BANK 0x1f201200
#define MEM_REGION1_ST 0x00200000
#define MEM_REGION1_SZ 0x9F00000
#define MEM_REGION2_ST 0x10E00000
#define MEM_REGION2_SZ 0xC200000

#define MB_BANK 0x1F206600
#define PM_SLEEP 0x1F001C00

#define Check_STR_CRC 0x1F002090 // 0x1048
extern BOOLEAN get_poweroff_flag(void);

void HAL_Write2Byte( unsigned long u32Reg, unsigned short u16Val )
{
    *((volatile unsigned long *)u32Reg)=u16Val;
}

unsigned short HAL_Read2Byte( unsigned long u32Reg )
{
    return (unsigned short)(*(volatile unsigned long *)u32Reg);
}

unsigned long BDMACRCCalc(int miu, unsigned long u32Addr, unsigned long u32Len)
{
    UBOOT_TRACE("IN\n");
    unsigned long u32CRC=0;
    unsigned long u32Timeout=0;
    HAL_Write2Byte(BDMA_BANK+0x0c*4,0);
    HAL_Write2Byte(BDMA_BANK+0x0d*4,0);
    if(miu)
    {
        HAL_Write2Byte(BDMA_BANK+0x02*4,0x0341);    //miu 1
    }else{
        HAL_Write2Byte(BDMA_BANK+0x02*4,0x0340);    //miu 0
    }
    HAL_Write2Byte(BDMA_BANK+0x04*4,u32Addr&0x0FFFF);
    HAL_Write2Byte(BDMA_BANK+0x05*4,(u32Addr>>16)&0x0FFFF);
    HAL_Write2Byte(BDMA_BANK+0x06*4,u32Addr&0x0FFFF);
    HAL_Write2Byte(BDMA_BANK+0x07*4,(u32Addr>>16)&0x0FFFF);
    HAL_Write2Byte(BDMA_BANK+0x08*4,u32Len&0x0FFFF);
    HAL_Write2Byte(BDMA_BANK+0x09*4,(u32Len>>16)&0x0FFFF);
    HAL_Write2Byte(BDMA_BANK+0x0a*4, 0x1DB7);
    HAL_Write2Byte(BDMA_BANK+0x0b*4, 0x04c1);
    HAL_Write2Byte(BDMA_BANK, HAL_Read2Byte(BDMA_BANK)|BIT0);
    //while(0==(HAL_Read2Byte(BDMA_BANK+0x01*4)&BIT3));
    while(1)
    {
        if((HAL_Read2Byte(BDMA_BANK+0x01*4)&BIT3)== 0x08 )
            break;
        if(u32Timeout>0xA00000)
        {
                printf("TIME OUT BDAM 0x%x \n",HAL_Read2Byte(BDMA_BANK+(0x01*4)));
                printf("----CRC TIME OUT ----\n");

                break;
        }
        udelay(10000);
        u32Timeout++;
    }
    u32CRC=HAL_Read2Byte(BDMA_BANK+0x0d*4);
    u32CRC=((u32CRC<<16) | HAL_Read2Byte(BDMA_BANK+0x0c*4));
    UBOOT_TRACE("OK\n");
    return u32CRC;
}
void getTheMemInfo(char* info, int LxMem, unsigned long *addr, unsigned long *size)
{
    char *strLxMem = NULL;
    char *endp = NULL;
    char buf[256]={0};
    switch(LxMem)
    {
        case 1:
            if ((strLxMem = strstr(info, "LX_MEM=")) != NULL)
            {
                *addr = CONFIG_KERNEL_START_ADDRESS;
                *addr = ((*addr) & (0x40000000 - 1));

                strncpy(buf, strchr(strLxMem, '0') , 10);
                buf[10] = 0;
                *size = simple_strtoul(buf, NULL, NULL);
            }
        break;
        case 2:
            if ((strLxMem = strstr(info, "LX_MEM2=")) != NULL)
            {
                endp = strchr(strLxMem, ',');
                strncpy(buf, strchr(strLxMem, '0') , endp-info+1);
                buf[endp-info+1] = 0;
                *addr = simple_strtoul(buf, NULL, NULL);
                if((*addr)>=0xA0000000)
                {
                    *addr = ((*addr) - (0xA0000000));
                }
                else
                {
                    *addr = ((*addr) & (0x40000000 - 1));
                }
                strncpy(buf, strchr(strLxMem, ',') + 1 , (endp+1)-info+1);
                buf[(endp+1)-info+1] = 0;
                *size = simple_strtoul(buf, NULL, NULL);
            }
        break;
        case 3:
            if ((strLxMem = strstr(info, "LX_MEM3=")) != NULL)
            {
                endp = strchr(strLxMem, ',');
                strncpy(buf, strchr(strLxMem, '0') ,  endp-info+1);
                buf[ endp-info+1] = 0;
                *addr = simple_strtoul(buf, NULL, NULL);
                *addr = ((*addr) & 0xFFFFFFF);
                strncpy(buf, strchr(strLxMem, ',') + 1 , (endp+1)-info+1);
                buf[(endp+1)-info+1] = 0;
                *size = simple_strtoul(buf, NULL, NULL);
            }
        break;
        default:
            UBOOT_DEBUG("exception lxmem number %d\n",LxMem);
        break;
    }
}
void CRCCheck(void)
{
    char crclevel[20];
    char *startp=NULL, *endp = NULL;
    U16 STR_CRC_num = 0;
    int len=0;
    unsigned long lx_mem_addr = 0, lx_mem_size = 0 , lx_mem2_addr = 0, lx_mem2_size = 0 , lx_mem3_addr = 0, lx_mem3_size = 0;
    unsigned long crc_result_1 = 0, crc_result_2 = 0, crc_result_3 = 0, pm_crc_1 = 0, pm_crc_2 = 0, pm_crc_3 = 0;
    UBOOT_TRACE("IN\n");

    char *bootargs = getenv("bootargs");

    if (bootargs)
    {
        if ((startp = strstr(bootargs, "str_crc=")) != NULL)
        {
            endp = strchr(startp, ' ');
            if(NULL==endp)len=strlen(startp);
            else len=endp-startp;
            if(len>19)len=19;
            memcpy(crclevel, startp, len);
            crclevel[len]=0;
            if ((strchr(crclevel, '1') != NULL) || (strchr(crclevel, '2') != NULL) || (strchr(crclevel, '3') != NULL))
            {
                // check pattern first
        /*        if ((0x9987 != HAL_Read2Byte(MB_BANK + 0xA0*2)) || (0x8887 != HAL_Read2Byte(MB_BANK + 0xA2*2)))
                {
                    UBOOT_INFO("[WARNING]pattern failed, 99878887 != %04lx%04lx\n",(long unsigned int)HAL_Read2Byte(MB_BANK + 0xA0*2),
                                                                                (long unsigned int)HAL_Read2Byte(MB_BANK + 0xA2*2));
                    setenv("dc_poweroff", "0");
                    saveenv();
                    run_command("reset", 0);
                }
        */

                // get lx_mem  address and size
                getTheMemInfo(bootargs, 1, &lx_mem_addr, &lx_mem_size);
                getTheMemInfo(bootargs, 2, &lx_mem2_addr, &lx_mem2_size);
                getTheMemInfo(bootargs, 3, &lx_mem3_addr, &lx_mem3_size);

                // calculate crc
                //crc_result_1 = BDMACRCCalc(0,lx_mem_addr + 0x10000, lx_mem_size - 0x10000);
                //pm_crc_1 = HAL_Read2Byte(MB_BANK + 0xA6*2) << 16 | HAL_Read2Byte(MB_BANK + 0xA4*2);

                if (lx_mem_size != 0 && lx_mem2_size != 0)
                {
                    if(CONFIG_MSTAR_STR_CRC_SIZE != 0x0)
                    {
                        lx_mem_size = CONFIG_MSTAR_STR_CRC_SIZE;
                        lx_mem2_size = CONFIG_MSTAR_STR_CRC_SIZE;
                    }
                    crc_result_1 = BDMACRCCalc(0,lx_mem_addr+ 0x10000, lx_mem_size- 0x10000);
                    pm_crc_1 = HAL_Read2Byte(MB_BANK + 0xA6*2) << 16 | HAL_Read2Byte(MB_BANK + 0xA4*2);
                    crc_result_2 = BDMACRCCalc(1,lx_mem2_addr, lx_mem2_size);
                    pm_crc_2 = HAL_Read2Byte(MB_BANK + 0xAA*2) << 16 | HAL_Read2Byte(MB_BANK + 0xA8*2);

                    STR_CRC_num = (U16)(pm_crc_1) + (U16)(pm_crc_1>>16) + (U16)(pm_crc_2) + (U16)(pm_crc_2>>16);
                    if(HAL_Read2Byte(Check_STR_CRC)!=STR_CRC_num)
                    {
                        printf("CRC Check Fail !!!!\n");
                    }
                }



                /*
                else if(lx_mem3_size == 0 && lx_mem2_size != 0)
                {
                    crc_result_2 = BDMACRCCalc(0,lx_mem2_addr, lx_mem2_size);
                    pm_crc_2 = HAL_Read2Byte(MB_BANK + 0xAA*2) << 16 | HAL_Read2Byte(MB_BANK + 0xA8*2);
                }
                else if(lx_mem3_size != 0 && lx_mem2_size == 0)
                {
                    crc_result_3 = BDMACRCCalc(1,lx_mem3_addr, lx_mem3_size);
                    pm_crc_3 = HAL_Read2Byte(MB_BANK + 0xAA*2) << 16 | HAL_Read2Byte(MB_BANK + 0xA8*2);
                }*/

                UBOOT_ERROR("MemRegion[%08lx,%08lx] CRC is %08lx, pmCRC is %08lx\n",lx_mem_addr,lx_mem_size,crc_result_1, pm_crc_1);
                UBOOT_ERROR("MemRegion[%08lx,%08lx] CRC is %08lx, pmCRC is %08lx\n",lx_mem2_addr,lx_mem2_size,crc_result_2, pm_crc_2);
        /*
                if ((crc_result_1 != pm_crc_1) ||
                    (crc_result_2 != pm_crc_2) ||
                    (crc_result_3 != pm_crc_3))
                {
                    UBOOT_ERROR("Check CRC fail :\n");
                    UBOOT_ERROR("MemRegion[%08lx,%08lx] CRC is %08lx, pmCRC is %08lx\n",lx_mem_addr,lx_mem_size,crc_result_1, pm_crc_1);
                    UBOOT_ERROR("MemRegion[%08lx,%08lx] CRC is %08lx, pmCRC is %08lx\n",lx_mem2_addr,lx_mem2_size,crc_result_2, pm_crc_2);
                    UBOOT_ERROR("MemRegion[%08lx,%08lx] CRC is %08lx, pmCRC is %08lx\n",lx_mem3_addr,lx_mem3_size,crc_result_3, pm_crc_3);

                    if (strchr(crclevel, '2') != NULL)
                    {
                        // system reset
                        setenv("dc_poweroff", "0");
                        saveenv();
                        UBOOT_ERROR("system reset due to crc error!!!!!\n");
                        run_command("reset", 0);
                    }
                    else if (strchr(crclevel, '3') != NULL)
                    {
                        // system hang
                        setenv("dc_poweroff", "0");
                        saveenv();
                        UBOOT_ERROR("system hang due to crc error!!!!!\n");
                        while(1);
                    }
                }*/
            }
            else
            {
                // do nothing
            }
        }
        else
        {
            UBOOT_INFO("warning: can not get crclevel !!!!!!\n");
        }
    }
    else
    {
        UBOOT_ERROR("error: can not get bootargs !!!!!!\n");
    }
     UBOOT_TRACE("OK\n");
}

#endif

#define MSTAR_SLEEP_MAGIC         0x4D535452

#if ENABLE_MSTAR_EAGLE
#define PMU_WAKEUP_ADDR_REGL     0x1F001CE0
#define PMU_WAKEUP_ADDR_REGH     0x1F001CE4
#elif ENABLE_MSTAR_KENYA
#define PMU_WAKEUP_ADDR_REGL     0xBF001CF0
#define PMU_WAKEUP_ADDR_REGH     0xBF001CF0
#else
#define PMU_WAKEUP_ADDR_REGL     0x1F001D48
#define PMU_WAKEUP_ADDR_REGH     0x1F001D48
#endif
#define PMU_WAKEUP_ADDR_LMASK    0x000000FF
#define PMU_WAKEUP_ADDR_HMASK    0x0000FF00



#define WAKEUP_ADDR_MASK          0x0000FFF0
#define WAKEUP_FLAG_MASK          0x0000000F
#define WAKEUP_FLAG_INVALID       0
#define WAKEUP_FLAG_SLEPT         1
#define WAKEUP_FLAG_WKUP          2

#if CONFIG_MCU_MIPS32
#define MIU0_VIRTBASE1   0x80000000
#define MIU1_BASE_ADDR1  0x60000000
#define MIU1_BASE_ADDR2  0x70000000
#define MIU1_VIRTBASE1   0x00000000
#define MIU1_VIRTBASE2   0x20000000
#define _PHY_TO_VIRT(x) ((x<MIU1_BASE_ADDR1)?(x+MIU0_VIRTBASE1):    \
                          ((x<MIU1_BASE_ADDR2)?(x-MIU1_BASE_ADDR1+MIU1_VIRTBASE1):(x-MIU1_BASE_ADDR2+MIU1_VIRTBASE2)) )
#elif CONFIG_MCU_ARM
#define M512M (512*1024*1024)
#define _PHY_TO_VIRT(x) ( (((x)>=(CONFIG_MIU0_BUSADDR)&&(x)<(CONFIG_MIU0_BUSADDR+M512M)) \
                            ||((x)>=(CONFIG_MIU1_BUSADDR)&&(x)<(CONFIG_MIU1_BUSADDR+M512M)))? \
                            (x):(0) )
#else
#define _PHY_TO_VIRT(x) (x)
#endif
#define PHY_TO_VIRT(x) _PHY_TO_VIRT((unsigned long)(x))

unsigned long read_wkup_pmu(void)
{
    volatile unsigned long *preg=0;
    unsigned long val=0;

    preg=(volatile unsigned long *)PMU_WAKEUP_ADDR_REGL;
    val = ((*preg)&PMU_WAKEUP_ADDR_LMASK);
    preg=(volatile unsigned long *)PMU_WAKEUP_ADDR_REGH;
    val |= ((*preg)&PMU_WAKEUP_ADDR_HMASK);

    return val;
}
void write_wkup_pmu(unsigned long val)
{
    volatile unsigned long *preg=0;
    unsigned long oldval;

    preg=(volatile unsigned long *)PMU_WAKEUP_ADDR_REGL;
    oldval = ((*preg) & ~PMU_WAKEUP_ADDR_LMASK);
    oldval |= (val&PMU_WAKEUP_ADDR_LMASK);
    (*preg) = oldval;

    preg=(volatile unsigned long *)PMU_WAKEUP_ADDR_REGH;
    oldval = ((*preg) & ~PMU_WAKEUP_ADDR_HMASK);
    oldval |= (val&PMU_WAKEUP_ADDR_HMASK);
    (*preg) = oldval;

}

int _is_str_resume(void)
{
    unsigned long pmu_str_reg=0,*pwkupsave=0;
    pmu_str_reg=read_wkup_pmu();
    pwkupsave = (unsigned long*)((pmu_str_reg&WAKEUP_ADDR_MASK)<<16);
#if CONFIG_MCU_MIPS32
    pwkupsave = (unsigned long*)PHY_TO_VIRT(pwkupsave);
#endif
    if((pmu_str_reg & WAKEUP_FLAG_MASK) == WAKEUP_FLAG_SLEPT && pwkupsave
        && pwkupsave[0]==MSTAR_SLEEP_MAGIC && pwkupsave[1])
    {
        return 1;
    }
    return 0;
}

int check_str_resume(void)
{
#ifdef CONFIG_MSTAR_STR_CRC
    if(!((HAL_Read2Byte(PM_SLEEP + 0x6E*2) & 0xFF00) == 0xFF00) &&
        (get_poweroff_flag()))
    {
        CRCCheck();
    }
#endif
    return _is_str_resume();
}

int do_check_str_resume(cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[]){

    unsigned long pmu_str_reg=0,*pwkupsave=0;
    char* s = NULL;
#ifdef CONFIG_MSTAR_STR_CRC
    if(!((HAL_Read2Byte(PM_SLEEP + 0x6E*2) & 0xFF00) == 0xFF00) &&
        (get_poweroff_flag()))
    {
        CRCCheck();
    }
#endif

    pmu_str_reg=read_wkup_pmu();
    pwkupsave = (unsigned long*)((pmu_str_reg&WAKEUP_ADDR_MASK)<<16);
#if CONFIG_MCU_MIPS32
    pwkupsave = (unsigned long*)PHY_TO_VIRT(pwkupsave);
#endif
    if((pmu_str_reg & WAKEUP_FLAG_MASK) == WAKEUP_FLAG_SLEPT && pwkupsave
    && pwkupsave[0]==MSTAR_SLEEP_MAGIC && pwkupsave[1])
    {
        s = getenv("KERNEL_PROTECT");
        if (s != NULL)
            MsApi_kernelProtect();
        s = getenv("kernelProtectBist");
        if (s != NULL)
            MsApi_kernelProtectBist();

        write_wkup_pmu((pmu_str_reg&~WAKEUP_FLAG_MASK)|WAKEUP_FLAG_WKUP);
#if CONFIG_MCU_MIPS32
        {
              void (*resume)(void);
              resume = (void *)PHY_TO_VIRT(pwkupsave[1]);
              if(resume)
                  resume();
        }
#elif CONFIG_MCU_ARM
        __asm__ volatile (
            "ldr r1, %0\n"
            "ldr r1, [r1, #4]\n"
            "cpsid if, #0x13\n"
            //; Disable MMU
            "MRC   p15, 0, r0, c1, c0, 0\n"
            "BIC   r0, r0, #0x1\n"
            "MCR   p15, 0, r0, c1, c0, 0\n"
            //; jump to wakeup address
            "mov pc, r1\n"
            ::"m"(pwkupsave):"r0","r1","memory","cc");
#endif
    }
    else
    {
        write_wkup_pmu(0);
    }
    return 0;
}
#endif

int do_envload (cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[])
{
    uchar bReloadEnv = 0;
    char *envbuffer;
    envbuffer=NULL;

#if defined(CONFIG_ENV_IS_IN_SPIFLASH)
    extern int env_isempty(void);
    if(env_isempty())
    {
        printf("\n <env reload for CRC>\n");
        bReloadEnv = 1;
    }
#endif

#if defined(CONFIG_LOAD_ENV_FROM_SN)
    envbuffer = getenv ("mboot_default_env");
    if(envbuffer)
    {
        if(simple_strtoul (envbuffer, NULL, 10))
        {
            printf("\n <env reload for CHECK_IF_MBOOT_DEFAULT_ENV>\n");
            bReloadEnv = 1;
        }
    }

    if (getenv("bootargs") == NULL || getenv("recoverycmd") == NULL)
    {
        UBOOT_DEBUG("get env configs from SN\n");
        bReloadEnv = 1;
    }
#endif

    if(bReloadEnv)
    {
        char cmd[128]="\0";
        snprintf(cmd,sizeof(cmd)-1,"loadenv %s %s/set_env",CONFIG,CONFIG_PATH);
        run_command(cmd,0);
    }
    else
    {
        // reload env after reboot in recovery mode.
        extern int get_boot_status_from_mtd0(void);
        get_boot_status_from_mtd0();
    }

     return 0;
}

#if defined (CONFIG_MBOOT_VERSION)
int do_setmbootver (cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[]){
    char *s;
    if((s = getenv (USB_MBOOT_VERSION))!= NULL)
    {
        if(strcmp(s,USB_MBOOT_CURRENT_VERSION))
        {
            setenv(USB_MBOOT_VERSION, USB_MBOOT_CURRENT_VERSION);
            saveenv();
        }
    }
    else
    {
        setenv(USB_MBOOT_VERSION, USB_MBOOT_CURRENT_VERSION);
        saveenv();
    }
    return 0;
}
#endif



#define BOOT_IF_ACTION(c, b)                ((c) & (b))

int get_boot_status_from_mtd0(void)
{
    int ret = 0;
    struct bootloader_message *p_msg = NULL;
    p_msg = (struct bootloader_message*)BOOT_MODE_TMP_BUF_ADDR;
    char cmd[128];

    memset(cmd, 0, sizeof(cmd));
#if (ENABLE_MODULE_MMC == 1)
    snprintf(cmd, sizeof(cmd)-1, "mmc read.p 0x%08lX misc 40", (unsigned long)BOOT_MODE_TMP_BUF_ADDR);
#else
    snprintf(cmd, sizeof(cmd)-1, "nand read.e 0x%08lX misc 40", (unsigned long)BOOT_MODE_TMP_BUF_ADDR);
#endif
    if(-1 != run_command(cmd, 0))
    {
        UBOOT_DEBUG("read the misc partion data\n");
        memset(cmd, 0, sizeof(cmd));
        if ((strlen(p_msg->command) == strlen(BOOT_STATUS_CUSTOMER_ACTIONS_STR)) && (0==strncmp(p_msg->command, BOOT_STATUS_CUSTOMER_ACTIONS_STR, strlen(BOOT_STATUS_CUSTOMER_ACTIONS_STR))))
        {
            int reloadEnv   = 0;
            int reloadPanel = 0;
            char actionByte = p_msg->status[0];
            if (BOOT_IF_ACTION(actionByte, BOOT_STATUS_ACTION_RELOADPANEL_BIT))
            {
                reloadPanel = 1;
            }
            if (BOOT_IF_ACTION(actionByte, BOOT_STATUS_ACTION_RELOADENV_BIT))
            {
                reloadEnv = 1;
            }

            memset(p_msg->command, 0, sizeof(p_msg->command));
            memset(p_msg->status, 0, sizeof(p_msg->status));
            memset(cmd, 0, sizeof(cmd));
#if (ENABLE_MODULE_MMC == 1)
            snprintf(cmd, sizeof(cmd)-1, "mmc write.p 0x%08lX misc 40", (unsigned long)BOOT_MODE_TMP_BUF_ADDR);
#else
            snprintf(cmd, sizeof(cmd)-1, "nand write.e 0x%08lX misc 40", (unsigned long)BOOT_MODE_TMP_BUF_ADDR);
#endif
            run_command(cmd, 0);

            if (reloadPanel)
            {
                setenv("db_table","0");
                saveenv();
            }

            if (reloadEnv)
            {
                // reload env
                char cmd[128]="\0";
                snprintf(cmd,sizeof(cmd)-1,"loadenv %s %s/set_env",CONFIG,CONFIG_PATH);
                run_command(cmd,0);
            }
        }
    }
    else
    {
        UBOOT_ERROR("read misc partition data failed\n");
        ret = -1;
    }
    return ret;
}


int do_loadenv(cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[])
{
#if ENABLE_MODULE_LOAD_ENV_FROM_SN
    #ifdef CONFIG_UBI
    int ret=0;
    #endif
#endif
    UBOOT_TRACE("IN\n");
    if(argc < 3)
    {
        cmd_usage(cmdtp);
        return -1;
    }

    if(argv[1] == NULL)
    {
        cmd_usage(cmdtp);
        return -1;
    }

    if(argv[2] == NULL)
    {
        cmd_usage(cmdtp);
        return -1;
    }
    UBOOT_DEBUG("load env from partition -> %s path -> %s\n",argv[1],argv[2]);
#if ENABLE_MODULE_LOAD_ENV_FROM_SN
    #ifdef CONFIG_UBI
    ret = run_command("dynpart edb64M-nand:0x00100000(misc),0x00600000(recovery),0x00400000(boot),0x1ee00000(ubi),3m(CTRL),3m(TBL),-(reserved)", 0);
    if(ret==-1)
    {
        UBOOT_ERROR("dynpart fail\n");
        return -1;
    }
    #endif
    vfs_mount(argv[1]);
    runscript_linebyline(argv[2]);
    vfs_umount();
#endif
    UBOOT_TRACE("OK\n");

    return 0;

}

#if defined (CONFIG_SET_4K2K_MODE)
extern U8 g_logoGopIdx;

MS_BOOL __Sc_is_interlace(void)
{
    return 0;
}

MS_U16 __Sc_get_h_cap_start(void)
{
    return 0x60;
}

void __Sys_PQ_ReduceBW_ForOSD(MS_U8 PqWin, MS_BOOL bOSD_On)
{

}

int do_setFRC(cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[])
{
    GOP_InitInfo pGopInit;

    if(get_poweroff_flag())
    {
        memset((void *)&pGopInit, 0, sizeof(pGopInit));
        MApi_GOP_RegisterXCIsInterlaceCB(__Sc_is_interlace);
        MApi_GOP_RegisterXCGetCapHStartCB(__Sc_get_h_cap_start);
        MApi_GOP_RegisterXCReduceBWForOSDCB(__Sys_PQ_ReduceBW_ForOSD);
        pGopInit.u16PanelWidth = g_IPanel.Width();
        pGopInit.u16PanelHeight = g_IPanel.Height();
        pGopInit.u16PanelHStr = g_IPanel.HStart();
        pGopInit.u32GOPRBAdr = 0x0;
        pGopInit.u32GOPRBLen = 0x0;

        pGopInit.u32GOPRegdmaAdr = 0;
        pGopInit.u32GOPRegdmaLen = 0;
        pGopInit.bEnableVsyncIntFlip = FALSE;

        MApi_GOP_InitByGOP(&pGopInit, g_logoGopIdx);
        MApi_GOP_GWIN_SetGOPDst(g_logoGopIdx, E_GOP_DST_FRC);
    }
    return 0;
}

int Set_4K2K_OP0(void)
{
    GOP_InitInfo pGopInit;

    //close lvds
    MDrv_Ursa_6M40_Set_Lvds_Off();

    //disable osd
    MDrv_Ursa_6M40_Set_Osd_Off();

    //set OP0
    memset((void *)&pGopInit, 0, sizeof(pGopInit));
    MApi_GOP_RegisterXCIsInterlaceCB(__Sc_is_interlace);
    MApi_GOP_RegisterXCGetCapHStartCB(__Sc_get_h_cap_start);
    MApi_GOP_RegisterXCReduceBWForOSDCB(__Sys_PQ_ReduceBW_ForOSD);
    pGopInit.u16PanelWidth = g_IPanel.Width();
    pGopInit.u16PanelHeight = g_IPanel.Height();
    pGopInit.u16PanelHStr = g_IPanel.HStart();
    pGopInit.u32GOPRBAdr = 0x0;
    pGopInit.u32GOPRBLen = 0x0;

    pGopInit.u32GOPRegdmaAdr = 0;
    pGopInit.u32GOPRegdmaLen = 0;
    pGopInit.bEnableVsyncIntFlip = FALSE;

    MApi_GOP_InitByGOP(&pGopInit, g_logoGopIdx);
    MApi_GOP_GWIN_SetGOPDst(g_logoGopIdx, E_GOP_DST_OP0);

    //open lvds
    MDrv_Ursa_6M40_Set_Lvds_On();

    return 0;
}
#endif

#if defined (CONFIG_URSA_6M40)
int do_ursa_lvds_on (cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[])
{
    MS_BOOL bRet = FALSE;
    bRet = MDrv_Ursa_6M40_Set_Lvds_On();
    return 0;
}
int do_ursa_lvds_off(cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[])
{
    MS_BOOL bRet = FALSE;
    bRet = MDrv_Ursa_6M40_Set_Lvds_Off();
    return 0;
}

int do_ursa_osd_unmute (cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[])
{
    MS_BOOL bRet = FALSE;
    bRet = MDrv_Ursa_6M40_Set_Osd_Unmute();
    return 0;
}
int do_ursa_2k_mode_on (cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[])
{
    MS_BOOL bRet = FALSE;
    bRet = MDrv_Ursa_6M40_Set_2K_Mode_On();
    return 0;
}

int do_ursa_set_osd_mode(cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[])
{
    u16 protect_mode =0;
    int ret =0;

    protect_mode = g_UrsaCMDGenSetting.g_OsdMode.protect_mode;
    if(protect_mode>=0 && protect_mode < 3)
    {

        printf("osd_protect_mode = %d\n",protect_mode);
            switch(protect_mode)
                {
                case URSA_6M40_OSD_PROTECT_OFF:
                    {
                        MDrv_Ursa_6M40_SendCmd(CMD_6M40_OSD_PROTECT_OFF);
                    }
                    break;
                case URSA_6M40_OSD_PROTECT_ON:
                    {
                        MDrv_Ursa_6M40_SendCmd(CMD_6M40_OSD_PROTECT_ON);
                    }
                    break;
                case URSA_6M40_OSD_PROTECT_ON_EMMC:
                    {
                        MDrv_Ursa_6M40_SendCmd(CMD_6M40_OSD_PROTECT_ON_EMMC);
                    }
                    break;
                default:
                         break;
                }
        }
        else
        {
            UBOOT_ERROR("OSD protect Mode setting not correct\n");
            ret = -1;
        }
    return ret;
}

#endif

#if defined (CONFIG_ENABLE_4K2K_PANEL)
int do_inx_panel_set_init (cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[])
{
#if defined (CONFIG_INX_VB1)
    MDrv_Panel_INX_VB1_Set_Pre_Init();
#elif defined(CONFIG_INX_NOVA_VB1)
    MDrv_Panel_INX_NOVA_VB1_Unlock_AHB();
#endif
    return 0;
}

int do_inx_panel_set_fhd (cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[])
{
#if defined (CONFIG_INX_VB1)
    MDrv_Panel_INX_VB1_Set_FHD();
#elif defined (CONFIG_INX_LVDS)
    MDrv_PANEL_INX_LVDS_Set_FHD();
#endif
    return 0;
}

int do_inx_panel_set_4k2k (cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[])
{
#if defined (CONFIG_INX_VB1)
    MDrv_Panel_INX_VB1_Set_4K2K();
#elif defined (CONFIG_INX_LVDS)
    MDrv_PANEL_INX_LVDS_Set_4K2K();
#elif defined (CONFIG_INX_NOVA_VB1)
    MDrv_Panel_INX_NOVA_VB1_Set_UHD_DIVISION(UHD_2_DIVISION);
#endif
    return 0;
}

#if defined (CONFIG_INX_NOVA_VB1)
int do_inx_nova_set_4k2k_2division (cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[])
{
    MDrv_Panel_INX_NOVA_VB1_Set_UHD_DIVISION(UHD_2_DIVISION);
    return 0;
}
#endif

#if defined (CONFIG_INX_VB1)
int do_panel_inx_vb1_init (cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[])
{
#if defined(CONFIG_BOOTLOGO_4K2K)
    MDrv_Panel_INX_VB1_RX_INIT();
#endif
    return 0;
}
#endif
#endif

#if defined (CONFIG_URSA_8)
int do_ursa8_lvds_on (cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[])
{
    MS_BOOL bRet = FALSE;
    bRet = MDrv_Ursa_8_Set_Lvds_On();
    return 0;
}


int do_ursa8_set_osd_mode(cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[])
{
    u16 protect_mode =0;
    int ret =0;

    protect_mode = g_UrsaCMDGenSetting.g_OsdMode.protect_mode;
    if(protect_mode>=0 && protect_mode < 3)
    {

        printf("osd_protect_mode = %d\n",protect_mode);
            switch(protect_mode)
                {
                case URSA_8_OSD_PROTECT_OFF:
                    {
                        MDrv_Ursa_8_SendCmd(URSA_8_CMD_OSD_PROTECT_OFF);
                    }
                    break;
                case URSA_8_OSD_PROTECT_ON:
                    {
                        MDrv_Ursa_8_SendCmd(URSA_8_CMD_OSD_PROTECT_ON);
                    }
                    break;
                case URSA_8_OSD_PROTECT_ON_EMMC:
                    {
                        MDrv_Ursa_8_SendCmd(URSA_8_CMD_OSD_PROTECT_ON_EMMC);
                    }
                    break;
                default:
                         break;
                }
        }
        else{
            UBOOT_ERROR("OSD protect Mode setting not correct\n");
            ret = -1;
            }


    return ret;

}

#endif
#if (ENABLE_MODULE_BOOT_IR == 1)
int do_ir_delay(cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[])
{
    mdelay(300); //sleep 300ms
    return 0;
}
#endif

#if defined (CONFIG_URSA6_VB1)
int do_send_I2C_cmd_to_ursa6(cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[])
{
    PanelType panel_data;
    memset(&panel_data, 0, sizeof(panel_data));

    if(is_str_resume())
    {
        U32 u32PanelConfigsAddr;
        if(get_addr_from_mmap("E_MMAP_ID_VDEC_CPU", &u32PanelConfigsAddr)!=0)
        {
            UBOOT_ERROR("get E_MMAP_ID_VDEC_CPU mmap fail\n");
            return -1;
        }
        UBOOT_DEBUG("E_MMAP_ID_VDEC_CPU = 0x%x\n", u32PanelConfigsAddr);
        UBOOT_DEBUG("(U32)(PA2NVA(u32PanelConfigsAddr)) = 0x%x\n", (U32)(PA2NVA(u32PanelConfigsAddr)));
        memcpy(&panel_data, (U32*)(PA2NVA(u32PanelConfigsAddr)), sizeof(PanelType));
    }
    else
    {
        if(Read_PanelParaFromflash(&panel_data)!=0)
        {
            UBOOT_ERROR("%s: Read_PanelParaFromflash() failed, at %d\n", __func__, __LINE__);
            return -1;
        }
    }

    if(panel_data.m_wPanelWidth == 3840 && panel_data.m_wPanelHeight == 2160)
        MDrv_Ursa_6M38_4K2K_init();
    else
        MDrv_Ursa_6M38_Set_2_lane_VB1_per_init();

    if(panel_data.m_wPanelWidth == 1920 && panel_data.m_wPanelHeight == 1080)
        MDrv_Ursa_6M38_Set_2_lane_VB1();

    return 0;
}
#endif


#if defined(CONFIG_MULTICORES_PLATFORM)
//#include <smp.h>

volatile unsigned int CoreHeartBeat[5] = {0, 0, 0, 0, 0};
extern unsigned int get_cpu_id(void);
extern void delayms(u32 TimeMs);
extern volatile MS_U32 gsystem_time_ms_cores[5];

void Core1_HeartBeat(void)
{
    u32 cpuID;
    cpuID = get_cpu_id();
    while(1)
    {
        delayms(1000);
        CoreHeartBeat[cpuID]++;
    }
}

void Core2_HeartBeat(void)
{
    u32 cpuID;
    cpuID = get_cpu_id();
    while(1)
    {
        delayms(1000);
        CoreHeartBeat[cpuID]++;
    }
}

void Core3_HeartBeat(void)
{
    u32 cpuID;
    cpuID = get_cpu_id();
    while(1)
    {
        delayms(1000);
        CoreHeartBeat[cpuID]++;
    }
}

void test_cb(unsigned int ipi_num)
{
    printf("ipi call back received number = %d\n", ipi_num);
}

void testHandler(InterruptNum eIntNum)
{
    printf("InterruptNum = %d\n", eIntNum);

    /*
      Implementation of isr here
    */

    /*Enable interrupt again*/
    MsOS_EnableInterrupt(eIntNum);
}

int do_ipi_test(cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[])
{
    int mod, arg1, arg2;
    if(argc <= 2)
    {
        printf("Usage: ipi_test [mod] ([cpu_mask]) [ipi_num] \n");
        printf("[ipi_send]        = 1 [cpu_mask] [ipi_num]\n");
        printf("[ipi_broadcast]   = 2 [ipi_num]\n");
        printf("[ipi_send_target] = 3 [cpu_id] [ipi_num]\n");
        printf("[ipi_send_self]   = 4 [ipi_num]\n");
        return -1;
    }

    mod = (int)simple_strtoul(argv[1], NULL, 16);
    arg1 = (int)simple_strtoul(argv[2], NULL, 16);

    switch(mod)
    {
        case 1:
        if(argc == 4)
        {
            arg2 = (int)simple_strtoul(argv[3], NULL, 16);
            Interrupt_request((E_INT_IPI_0x130_START+arg2), test_cb, NULL);
            ipi_send(arg1, (InterruptNum)(E_INT_IPI_0x130_START+arg2));
        }
        else
        {
            printf("Wrong args:\n");
            printf("Usage of [ipi_send] = 1 [cpu_mask] [ipi_num]\n");
            return -1;
        }
        break;

        case 2:
        if(argc == 3)
        {
            Interrupt_request((E_INT_IPI_0x130_START+arg1), test_cb, NULL);
            ipi_broadcast((InterruptNum)(E_INT_IPI_0x130_START+arg1));
        }
        else
        {
            printf("Wrong args:\n");
            printf("Usage of [ipi_broadcast] = 2 [ipi_num]\n");
            return -1;
        }
        break;

        case 3:
        if(argc == 4)
        {
            arg2 = (int)simple_strtoul(argv[3], NULL, 16);
            Interrupt_request((E_INT_IPI_0x130_START+arg2), test_cb, NULL);
            ipi_send_target(arg1, (InterruptNum)(E_INT_IPI_0x130_START+arg2));
        }
        else
        {
            printf("Wrong args:\n");
            printf("Usage of [ipi_send_target] = 3 [cpu_id] [ipi_num]\n");
            return -1;
        }
        break;

        case 4:
        if(argc == 3)
        {
            Interrupt_request((E_INT_IPI_0x130_START+arg1), test_cb, NULL);
            ipi_send_self((InterruptNum)(E_INT_IPI_0x130_START+arg1));
        }
        else
        {
            printf("Wrong args:\n");
            printf("Usage of [ipi_send_self] = 4 [ipi_num]\n");
            return -1;
        }
        break;

        case 5:
        {
             arg2 = (int)simple_strtoul(argv[3], NULL, 16);
             switch(arg1)
             {
                 unsigned int cpsr_b, cpsr_a;
                 case 1:
                 {
                     printf("interrupt disable test\n");
                     cpsr_b = Interrupt_enabled(); // 0 is disable, 1 is enable
                     Interrupt_disable();
                     cpsr_a = Interrupt_enabled(); // 0 is disable, 1 is enable
                     printf("cpsr before setup = %x\n", cpsr_b);
                     printf("cpsr after setup = %x\n", cpsr_a);
                 }
                 break;
                 case 2:
                 {
                     printf("interrupt ensable test\n");
                     cpsr_b = Interrupt_enabled(); // 0 is disable, 1 is enable
                     Interrupt_enable();
                     cpsr_a = Interrupt_enabled(); // 0 is disable, 1 is enable
                     printf("cpsr before setup = %x\n", cpsr_b);
                     printf("cpsr after setup = %x\n", cpsr_a);
                }
                 break;
                 case 3:
                 {
                     cpsr_b = Interrupt_save();
                     printf("interrupt save test: get the cpsr value : %x\n", cpsr_b);

                 }
                 break;
                 case 4:
                 {
                     printf("interrupt restore test: put the value %x to cpsr\n", arg2);
                     cpsr_b = Interrupt_save();
                     Interrupt_restore(arg2);
                     cpsr_a = Interrupt_save();
                     printf("the value of cpsr before restore = %x, after = %x\n", cpsr_b, cpsr_a);

                 }
                 break;
                 default:
                     printf("UnKnown sub command\n");
                 break;
             }
             printf("disable");
        }
        break;

        case 6:
        {
            printf("attach_irq test, will use E_INT_IRQ_MVD to testify\n");
            MsOS_AttachInterrupt(E_INT_IRQ_MVD, (InterruptCb)testHandler);
            MsOS_EnableInterrupt(E_INT_IRQ_MVD);
        }
        break;

        case 7:
        {
            u32 CountForWake = 0;
            while(1)
            {
                delayms(2000);
                CoreHeartBeat[0] = CoreHeartBeat[0] + 2;
                CountForWake++;
                printf("HeartBeat Checker Core 0 - Core 3 [%d %d %d %d]\n", CoreHeartBeat[0], CoreHeartBeat[1], CoreHeartBeat[2], CoreHeartBeat[3]);
                if(CountForWake == 4)
                {
                    Core_Wakeup(Core1_HeartBeat, 1);
                }

                if(CountForWake == 8)
                {
                    Core_Wakeup(Core2_HeartBeat, 2);
                }

                if(CountForWake == 12)
                {
                    Core_Wakeup(Core3_HeartBeat, 3);
                }
            }
        }

        default:
            printf("UnKnown mod %d\n", mod);
        break;
    }

    return 0;
}

extern unsigned int sched_cnt;
int do_sched_test(cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[])
{
    if (argc == 1)
    {
        sched_cnt=1;
        return 0;
    }

    sched_cnt = (int)simple_strtoul(argv[1], NULL, 16);
    return 0;
}
#endif

int do_set_ssc(cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[])
{
    MS_BOOL bIsEnable = false;
    int u16Fmodulation = 0;
    int u16Rdeviation = 0;
    char buf[12];

    if (argc == 1)
    {
        printf("unknown args:\n");
        return 0;
    }

    bIsEnable = (MS_BOOL)simple_strtoul(argv[1], NULL, 16);

    if  (true == bIsEnable)
    {
        switch(argc)
        {
            default:
            case 2:
                u16Fmodulation = 300;
                u16Rdeviation = 300;
                break;

            case 3:
                u16Fmodulation = (int)simple_strtoul(argv[2], NULL, 16);
                u16Rdeviation = 300;
                break;

            case 4:
                u16Fmodulation = (int)simple_strtoul(argv[2], NULL, 16);
                u16Rdeviation = (int)simple_strtoul(argv[3], NULL, 16);
                break;
        }

        if (u16Fmodulation < 200 || u16Fmodulation > 400)
        {
            u16Fmodulation = 300;
        }

        if (u16Rdeviation > 300)
        {
            u16Rdeviation = 300;
        }

        sprintf(buf, "%d", u16Fmodulation);
        setenv("ssc_modulation", buf);

        sprintf(buf, "%d", u16Rdeviation);
        setenv("ssc_deviation", buf);
    }

    MsDrv_Set_SSC(bIsEnable, u16Fmodulation, u16Rdeviation);

    sprintf(buf, "%d", bIsEnable);
    setenv("ssc_enable", buf);

    saveenv();

    return 0;
}

#if defined(LG_CHG)
extern MS_U32 MsOS_GetSystemTime (void);
MS_U32 readMsTicks(void)
{
    return MsOS_GetSystemTime();
}

void setup_mmap(void)
{
    int u8IDCount = 0;
    MMapInfo_t *temp;
#ifdef CONFIG_MSTAR_CMA
    CMA_MMapInfo_t *pCMA_temp;
#endif

    printf("gModelOpt.ddr_size = %d \n",gModelOpt.ddr_size);

//#if LG_BRING_UP
#if (ENABLE_MSTAR_M7621)
    switch (DDR_SIZE_1_5G) {
#else
    switch (gModelOpt.ddr_size) {
#endif
#if 0
        case DDR_SIZE_512M:
            temp = __mmap_256mb_256mb;
            MiuInfo = __miu_256mb_256mb;
            break;
#endif
        case DDR_SIZE_768M:
            temp = __mmap_512mb_256mb;
            MiuInfo = __miu_512mb_256mb;
            break;
        case DDR_SIZE_1G:
#if 0
            temp = __mmap_512mb_512mb;
            MiuInfo = __miu_512mb_512mb;
#else
            temp = __mmap_1024mb_0mb;
            MiuInfo = __miu_1024mb_0mb;
#ifdef CONFIG_MSTAR_CMA
            pCMA_temp = __cma_mmap_1024mb_0mb;
#endif /* CONFIG_MSTAR_CMA */
#endif
            break;
        case DDR_SIZE_1_5G:
            temp = __mmap_512mb_1024mb;
            MiuInfo = __miu_512mb_1024mb;
#ifdef CONFIG_MSTAR_CMA
            pCMA_temp = __cma_mmap_512mb_1024mb;
#endif /* CONFIG_MSTAR_CMA */
            break;
        case DDR_SIZE_2G:
            temp = __mmap_1024mb_1024mb;
            MiuInfo = __miu_1024mb_1024mb;
            break;
        default:
            temp = __mmap_512mb_512mb;
            MiuInfo = __miu_512mb_512mb;
            break;
    }

    for(u8IDCount = 0; u8IDCount < MMAP_ID_MAX; u8IDCount++)
    {
        MMAPGer[u8IDCount] = temp[u8IDCount];
    }
#ifdef CONFIG_MSTAR_CMA
    for(u8IDCount = 0; u8IDCount < CMA_MMAP_ID_MAX; u8IDCount++) {
        CMAInfo[u8IDCount] = pCMA_temp[u8IDCount];
    }
#endif /* CONFIG_MSTAR_CMA */

}
void lg_boot_splash(void)
{
#ifdef CFG_LG_CHG
        //verify_mac_address(); zzindda move to eth.c
        printf("\033[0;32m[%d]device preinit \033[0m\n", readMsTicks());
        devices_preinit();

        /*
         * call devices_init in DEBUG_LEVEL for using usb2serial.
         * if not , call it later.
         * */
        // zzindda
        #if 0
        if (DDI_NVM_GetDebugStatus() == DEBUG_LEVEL)
        {
            devices_init(); /* get the devices list going. */
        }

        src = get_tzfw_base();
        if (src != 0)
        {
            get_kernel_size(src);
#ifdef CC_TRUSTZONE_SUPPORT
            move_tz_binary(src);
#endif
        }
        #endif
        setup_mmap();
        fast_boot();
#endif
}

#define FLASH_MAC_DATA_SIZE     140
#define FLASH_MAC_HEADER_SIZE   12
#define FLASH_MAC_CHECKSUM_SIZE 2
#define FLASH_MAC_TOTAL_LEN     (FLASH_MAC_DATA_SIZE + FLASH_MAC_CHECKSUM_SIZE)
#define FLASH_MAC_START_INDEX   (12 + 8)    /* 2nd mac adddress offset */
#define MAC_STR_LEN             17

void verify_mac_address(void)
{
    int rc, idx;
    struct partition_info * partinfo;
    uint8_t macbuf[FLASH_MAC_TOTAL_LEN];
    char mac_str[MAC_STR_LEN + 1];
    uint8_t valid_flash = 0;
    uint8_t update_env  = 0;
    char *eth_addr      =getenv("ethaddr");

    /* read mac address in partition 'macadr' */
    if((partinfo = get_partition_by_name("macadr")) != NULL)
    {
        rc = storage_read((ulong)partinfo->offset, FLASH_MAC_TOTAL_LEN, macbuf);
        if(rc >= 0)
        {
            if(verify_mac_checksum(macbuf) == 0)
            {
                idx = FLASH_MAC_START_INDEX;
                sprintf(mac_str, "%02X:%02X:%02X:%02X:%02X:%02X",
                    macbuf[idx+0], macbuf[idx+1], macbuf[idx+2],
                    macbuf[idx+3], macbuf[idx+4], macbuf[idx+5]);
                printf("macaddr:%02X:%02X:%02X:%02X:%02X:%02X\n", macbuf[idx+0], macbuf[idx+1], macbuf[idx+2], macbuf[idx+3], macbuf[idx+4], macbuf[idx+5]);
                mac_str[MAC_STR_LEN] = '\0';
                valid_flash = 1;
                if (strncmp(eth_addr,mac_str,MAC_STR_LEN))
                {
                    /* update env variable */
                    update_env = 1;
                }
            }
        }
    }
    else
    {
        printf("macadr partition not found.\n");
    }

    if (valid_flash == 1)
    {
        setenv("macaddr", mac_str);
        setenv("ethaddr", mac_str);
        if (update_env == 1)
            saveenv();
    }
    else
        printf("not valid mac in macadr partition. use default ethaddr.\n");
}

int verify_mac_checksum(uint8_t* data)
{
    uint8_t checksum = 0;
    uint32_t idx;

    if(data[FLASH_MAC_DATA_SIZE] != data[FLASH_MAC_DATA_SIZE+1])
        goto invalid;

    for(idx=0; idx<FLASH_MAC_DATA_SIZE; idx++)
    {
        checksum += data[idx];
    }

    if(checksum == data[FLASH_MAC_DATA_SIZE])
        return 0;

invalid:
    printf("invalid checksum. calculated=%02x, data[n]=%02x, data[n+1]=%02x\n",
        checksum, data[FLASH_MAC_DATA_SIZE], data[FLASH_MAC_DATA_SIZE+1]);

    return -1;
}


void secure_init(void)
{
    printf("\033[0;32m[%d] %s ~~~ \033[0m\n", readMsTicks(), __FUNCTION__);

    run_command("SecureInit", 0);
}

void kernel_read(void)
{
    struct partition_info *mpi = NULL;
    mpi = get_used_partition("kernel");
    char read_cmd[128] = {0,};
    unsigned long long kernel_offset = 0;
    unsigned long long kernel_size = 0;

    printf("\033[0;32m[%d] %s \033[0m\n", readMsTicks(), __FUNCTION__);

    kernel_offset = mpi->offset;
    kernel_size = mpi->size;

    sprintf(read_cmd, "mmc read 0x25000000");
    sprintf(read_cmd, "%s 0x%x", read_cmd, ((ulong)kernel_offset/512));
    sprintf(read_cmd, "%s 0x%x", read_cmd, (ulong)kernel_size);

    printf("%s\n", read_cmd);

    run_command(read_cmd, 0);
}

void secure_verify_kernel(void)
{
#if(BYPASS_VERIFICATION == 0)
    struct partition_info *mpi = NULL;
    unsigned long long kernel_size = 0;
    unsigned long long kernel_offset = 0;
    int ret = 0;
    int retry_cnt = 0;
    int check_update_kernel = check_updated_partition("kernel");

    printf("\033[0;32m[%d] %s start \n", readMsTicks(), __FUNCTION__);
    mpi = get_used_partition("kernel");

    do {
        ret = sb_verify_image((unsigned int)mpi->offset, (unsigned int)mpi->filesize,1);
        retry_cnt++;
        printf("\033[0;32msb_verify_application check %d time \033[0m\n",retry_cnt);
        if (ret < 0)
            udelay(200000);
    } while (ret < 0 && retry_cnt < VERIFY_RETRY_MAX);

    if (ret<0)
    {
        printf("\033[0;32m %s verification failed  ... System will stop \033[0m\n",retry_cnt);
        if(check_update_kernel)
        {
            printf("\033[0;32m try backup partition booting \033[0m\n");
            write_emergency_partition(BACK_MAGIC, 0);
            swap_backup_partition(update_list);
            do_reset(NULL, 0, 0, NULL);
        }
        else
            while(1);
        return -1;
    }

    verify_done |= VERIFY_KERNEL_DONE;
    printf("\033[0;32m[%d] %s end \n", readMsTicks(), __FUNCTION__);

#endif
}

void secure_read_tee(void)
{
    printf("\033[0;32m[%d] %s \033[0m\n", readMsTicks(), __FUNCTION__);

    run_command("readNuttx", 0);
}

void secure_boot_tee(void)
{
    printf("\033[0;32m[%d] %s \033[0m\n", readMsTicks(), __FUNCTION__);

    run_command("bootNuttx", 0);
}

void secure_verify_tee(void)
{
#if defined (CONFIG_SECURITY_BOOT)
#if(BYPASS_VERIFICATION == 0)
        if(0!=verify_tzfw()){
        UBOOT_ERROR("TEE Authentication fail\n");
        return;
    }
#endif
#endif
}

static void verify_image(int verify)
{
    int snapshot_mode = check_snapshot_mode();
    int ret = 0;
    char logStr[MAX_LOG_STR_LEN] = {0};
    if(TRUE == verify)
    {
        if(snapshot_mode != LGSNAP_RESUME)
        {
            secure_verify_kernel();
            if(MICOM_IsPowerOnly() || !DDI_NVM_GetInstopStatus())
            {
                printf("Factory mode.. skip verify_apps..\n");
            }else
            {
                printf("Veirfy APP : BOOT_COLD..\n");
                verify_apps(BOOT_COLD);
            }
        }else if(snapshot_mode == LGSNAP_RESUME)
        {
            printf("Veirfy APP : BOOT_SNAPSHOT..\n");
            ret = verify_apps(BOOT_SNAPSHOT);
            if ( 0 > ret)
            {
                snprintf(logStr, MAX_LOG_STR_LEN, "ret:%d", ret);
                SNAPSHOT_Log(SNAPSHOT_LOG_ERROR, SNAPSHOT_VERIFY_APP_ERROR, logStr, 0);
            }
        }
        secure_verify_tee();
    }else
    {
        printf("Skip Veirfy Image\n");
    }
}

#if defined(USE_TEE_THREAD)
static void thread_secure_tee(void *arg)
{
    verify_image(TRUE);
    secure_read_tee();
    secure_boot_tee();
}
#endif

void board_check_ddr_size()
{
    int result = 0;
    int mountedDDRSize = 0;
    mountedDDRSize = get_ddr_size_in_mb();

//#if LG_BRING_UP
#if (ENABLE_MSTAR_M7621)
    return;
#endif

    if(MICOM_IsPowerOnly())
    {
        printf("\033[0;32m[%s] skip to check ddr size \033[0m\n", __FUNCTION__);
        return;
    }

    printf("\033[0;32m[%s] ddr_size = %d(MB), hwopt = %d \033[0m\n", __FUNCTION__, mountedDDRSize, gModelOpt.ddr_size);

    switch(gModelOpt.ddr_size)
    {
#if 0
    case DDR_SIZE_512M: // 512MB
        if(mountedDDRSize == 512)
            result = 1;
        else
            result = 0;
        break;
#endif
    case DDR_SIZE_1G:   // 1024MB
        if(mountedDDRSize == 1024)
            result = 1;
        else
            result = 0;
        break;

    default:
        printf("Undefined HW option for DDR size\n");
        result = 0;
        break;
    }

    if(result)
    {
        printf("\033[0;32m[%s] Mounted DDR is ok \033[0m\n", __FUNCTION__);
    }
    else
    {
        printf("\033[0;32m[%s] Mounted DDR is wrong \033[0m\n", __FUNCTION__);

        // apply stall policy
        MICOM_forceShutDown();
    }

}

#if defined(CONFIG_MULTICORES_PLATFORM)
void release_non_boot_core(void)
{
    unsigned int waitCount=0;
    unsigned int perCPU;

    if(__smp_cpu_init_done())
    {
        secondary_start_uboot_cleanup();
        megic_number_cleaned=1;

        for(perCPU=1; perCPU<NR_CPUS; perCPU++)
            thread_cond_signal(&g_sub_cond[perCPU]);
    }
    else
    {
        printf("Should not be here!!!\n");
    }

    while(!__smp_cpu_released())
    {
        ++waitCount;

        if (SMP_WAIT_NON_BOOT_CPU_RELEASE_COUNT < waitCount)
            do_reset(NULL, 0, 0, NULL);  //just in case, should never be here!
    }
}
#endif

void kernel_jump(void)
{
    tlog("\033[0;32m[%d] %s \033[0m\n", readMsTicks(), __FUNCTION__);

#if defined(CONFIG_MULTICORES_PLATFORM)
    release_non_boot_core();
#endif

    run_command("bootm 0x25000000", 0);
}

extern void wait_tee_ready(void);

void tputs(const char *s)
{
    if(s)
        tlog("%s", s);
}


/*
 * xtal: fixed 12M
 * mpl: fixed 216M
 * pll, variable,
 * ex: 1.6G --> 2717908.8 / 1.6 = 0x19EB85
 * bank 0x110C, reg 0x60 = 0xEB85
 *              reg 0x61 = 0x19
 * reverse: 2717908.8 / (reg 0x61 << 16 + reg 0x60) * 1000 = 1600(MHz)
 * --> 2717908800 / (reg 0x61 << 16 + reg 0x60) = 1600(MHz)
 *
 * return unit: MHz
 */
#define XTAL 0x0000 /* 12M */
#define MPL  0x0400 /* 216M */
#define PLL  0x8000 /* freq. depends on setting */
int query_frequency(void)
{
#if defined(CONFIG_MSTAR_DVFS_ENABLE)
    return 12;
#endif
    unsigned short freq_type =  *(volatile unsigned short*)(0x1F000000 + 0x100B00 * 2 + 0x11 * 4);
    unsigned short freq_low, freq_high;

    if(freq_type==XTAL)
    {
        return 12;
    }
    else if(freq_type==MPL)
    {
        return 216;
    }
    else if((freq_type&&PLL)==1)
    {
        freq_low = *(volatile unsigned short*)(0x1F000000 + 0x110C00 * 2 + 0x60 * 4);
        freq_high = *(volatile unsigned short*)(0x1F000000 + 0x110C00 * 2 + 0x61 * 4);
        return ( 0xA2000000/ ((freq_high << 16) + freq_low));
    }
    else
    {
        printf("without clock source\n");
    }
}


//typedef void ( *TimerCb ) (MS_U32 u32StTimer, MS_U32 u32TimerID);  ///< Timer callback function  u32StTimer: not used; u32TimerID: Timer ID
u64 arch_counter_get_ticks(void);

u64 Tick2MS = 0;
void arch_counter_calibration(void)
{
    unsigned int clock_freq;  //Hz
    unsigned int SysClock;
    u64          PrevTick, PostTick;

#if defined(CONFIG_MSTAR_MUNICH) || defined(CONFIG_MSTAR_MUSTANG)
    clock_freq = (query_frequency()/2)*1000*1000;
#elif defined(CONFIG_MSTAR_MAXIM) || defined(CONFIG_MSTAR_M7621)
    asm volatile("mrc p15, 0, %0, c14, c0, 0" : "=r" (clock_freq));
#endif

    printf("CNTFRQ: %lu Hz\n", clock_freq);
    PrevTick = arch_counter_get_ticks();
    SysClock = MsOS_GetSystemTime();
    // 10 ms calibration
    while(1)
    {
        if(SysClock+10 <= MsOS_GetSystemTime())
            break;
    }
    PostTick = arch_counter_get_ticks();
    Tick2MS = (PostTick-PrevTick)/10;

    printf("Counter Calibration: %llu ticks/ms\n", Tick2MS);
}

u64 arch_counter_get_ticks(void)
{
    u32 cvall, cvalh;

#if defined(CONFIG_MSTAR_MUNICH) || defined(CONFIG_MSTAR_MUSTANG)
    cvall = *(volatile u32*)(chip_TWD);
    cvalh = *(volatile u32*)(chip_TWD+0x4);
#elif defined(CONFIG_MSTAR_MAXIM) || defined(CONFIG_MSTAR_M7621)
    asm volatile("mrrc p15, 0, %0, %1, c14" : "=r" (cvall), "=r" (cvalh));
#endif
    return ((u64) cvalh << 32) | cvall;
}

/* clock source */
u64 arch_counter_get_ms(void)
{
    u32 cvall, cvalh;
    u64 Time_ms;

    if(Tick2MS == 0)
        arch_counter_calibration();

    Time_ms = arch_counter_get_ticks();

    return Time_ms / Tick2MS;
}


#define PERIOD_TIMER            1
#define ONE_SHOT_TIMER         2


/* clock event */
u32 arch_create_timer(TimerCb pTimerCb, u32 ElapseTimeMs, u32 Mode, char*pName, u32 CoreID)
{
    if(Mode == ONE_SHOT_TIMER)
        if(CoreID == 0)
            return MsOS_CreateTimer(pTimerCb, ElapseTimeMs, 0xF5F5F5F5, TRUE, pName);
        else
            return MsOS_CreateTimer_core(pTimerCb, ElapseTimeMs, 0xF5F5F5F5, TRUE, pName, CoreID);
    else
        if(CoreID == 0)
            return MsOS_CreateTimer(pTimerCb, ElapseTimeMs, ElapseTimeMs, TRUE, pName);
        else
            return MsOS_CreateTimer_core(pTimerCb, ElapseTimeMs, ElapseTimeMs, TRUE, pName, CoreID);
}

MS_BOOL arch_reset_timer(u32 u32TimerId, u32 ElapseTimeMs, u32 Mode)
{
    u32 CoreID;
    CoreID = u32TimerId/100;

    if(Mode == ONE_SHOT_TIMER)
        if(CoreID == 0)
            return MsOS_ResetTimer(u32TimerId, ElapseTimeMs, 0xF5F5F5F5, TRUE);
        else
            return MsOS_ResetTimer_core(u32TimerId, ElapseTimeMs, 0xF5F5F5F5, TRUE, CoreID);
    else
        if(CoreID == 0)
            return MsOS_ResetTimer(u32TimerId, ElapseTimeMs, ElapseTimeMs, TRUE);
        else
            return MsOS_ResetTimer_core(u32TimerId, ElapseTimeMs, ElapseTimeMs, TRUE, CoreID);
}

MS_BOOL arch_stop_timer(u32 u32TimerId)
{
    u32 CoreID;
    CoreID = u32TimerId/100;

    if(CoreID == 0)
        return MsOS_StopTimer(u32TimerId);
    else
        return MsOS_StopTimer_core(u32TimerId, CoreID);
}

MS_BOOL arch_start_timer(u32 u32TimerId)
{
    u32 CoreID;
    CoreID = u32TimerId/100;

    if(CoreID == 0)
        return MsOS_StartTimer(u32TimerId);
    else
        return MsOS_StartTimer_core(u32TimerId, CoreID);
}

MS_BOOL arch_del_timer(u32 u32TimerId)
{
    u32 CoreID;
    CoreID = u32TimerId/100;

    if(CoreID == 0)
        return MsOS_DeleteTimer(u32TimerId);
    else
        return MsOS_DeleteTimer_core(u32TimerId, CoreID);
}

MS_U32 arch_thread_create(const char *name,void *(*start)(void *arg), void *arg, int stk_size,int cpu_id,int priority,int join)
{
#if defined(CONFIG_MULTICORES_PLATFORM)
    return (MS_U32) thread_create_ex(name, start, arg, stk_size, cpu_id, priority, join);
#else
    return 0;
#endif
}

MS_U32 arch_mutex_new(void)
{
#if defined(CONFIG_MULTICORES_PLATFORM)
    mutex_t  *pMutex;
    pMutex = (mutex_t*) MsOS_AllocateMemory(sizeof(mutex_t), 0);
    mutex_new(pMutex);
    return pMutex;
#else
    return 0;
#endif
}

MS_BOOL arch_mutex_lock(MS_U32 mutexAdr)
{
#if defined(CONFIG_MULTICORES_PLATFORM)
    return mutex_lock((mutex_t*)mutexAdr);
#else
    return 0;
#endif
}

MS_BOOL arch_mutex_unlock(MS_U32 mutexAdr)
{
#if defined(CONFIG_MULTICORES_PLATFORM)
    return mutex_unlock((mutex_t*)mutexAdr);
#else
    return 0;
#endif
}

#if defined(CONFIG_MULTICORES_PLATFORM)
u32 Time_Core[NR_CPUS] = {0,};
#endif
static void arch_period_timer_callback(MS_U32 stTimer, MS_U32 u32Data)
{
#if defined(CONFIG_MULTICORES_PLATFORM)
    static int count = 0;
    static int sec = 0;
    unsigned int CPUID;

    CPUID = get_cpu_id();
    Time_Core[CPUID]++;
#endif
}

int fast_boot(void)
{
    char *s = NULL, key = 0;
    int i;
// zzindda
#if 0
    char *cmd1 = BOOTCOMMAND;
#endif
    char *cmd2[1024];

// zzindda
#ifdef USING_SIMPLE_MMAP

#else
#ifdef CONFIG_SECURITY_BOOT
  #if (BUILD_RESCUE_BOOT == 0)
    if ((DDI_NVM_GetDebugStatus() != RELEASE_LEVEL))
  #endif
#endif
#endif
    {
        for (i=0; i<20; ++i) {
            if (tstc()) {
                key = getc();
                if (key == 0x60)
                    break;
            }
            udelay (10);
        }
    }

   eMMC_RPMB_program_auth_key();

#if defined (CONFIG_SECURITY_BOOT)
   secure_init();
#endif
#if BUILD_RESCUE_BOOT
    key = 0x60;
#endif
    if(key != 0x60)
    {

        printf("[%d] %s Mstar Handshaking\n", readMsTicks(), __FUNCTION__);
                if(FALSE==MstarProcess())
                    printf("Error : MstarProcess()\n");
                if(FALSE==MstarToKernel())
                    printf("Error : MstarToKernel()\n");

#if defined (CONFIG_DISPLAY_LOGO)
    display_logo();
#endif

    // check wrong mounted ddr
    if(is_factory_mode())
        board_check_ddr_size();

printf("Enable kernel prottect\n");
MsApi_kernelProtect();

#if defined (CONFIG_HIBERNATION)
        int snapshot_mode = check_snapshot_mode();
        if(snapshot_mode == LGSNAP_RESUME)
        {
        #if defined (CONFIG_SECURITY_BOOT)
        #if defined(USE_TEE_THREAD)
            thread_tee = thread_create_ex("secure_tee", thread_secure_tee, NULL, 0, 3, THREAD_MAX_PRIORITY, 1);
        #if defined(CONFIG_TIMELOG_CHECKPOINT) && (CONFIG_TIMELOG_CHECKPOINT == TRUE)
        TIME_CHECK(0xDC);
       #endif
        #else
            verify_image(TRUE);
            secure_read_tee();
            secure_boot_tee();
        #endif //defined(USE_TEE_THREAD)
        #endif// defined (CONFIG_SECURITY_BOOT)

        #if 0 // kyungmok
            disable_wp(0); // eMMC W/P disable before snapshot booting
        #endif
            if(do_hib() == -1)
            {
                printf("boot state is snapshot, but snapshot image is not valid\n");
                printf("check the snapshot image\n");

                if(DDI_NVM_GetDebugStatus() == RELEASE_LEVEL)
                    remake_hib();
            }

        }
        else if (snapshot_mode == LGSNAP_MAKING_IMAGE)
        {
            unsigned int ptr = 0;
            const unsigned int WORDSIZE = sizeof(unsigned int);
            const unsigned int LX0_MEM_ADDR = (CONFIG_SYS_MIU0_CACHE + MMAPGer[LINUX_MEM].u32MemAddr);
            const unsigned int LX1_MEM_ADDR = (((MMAPGer[LX_MEM1].u32MemType & 0x1) ? CONFIG_SYS_MIU1_CACHE : CONFIG_SYS_MIU0_CACHE) + MMAPGer[LX_MEM1].u32MemAddr);
            const unsigned int LX0_MEM_LEN  = MMAPGer[LINUX_MEM].u32MemLength;
            const unsigned int LX1_MEM_LEN  = MMAPGer[LX_MEM1].u32MemLength;

            printf("Snapshot Makeing mode...\n");
            printf("Clear up LX0 memory: [0x%08x~0x%08x] WORDSIZE: %d...\n", LX0_MEM_ADDR, (LX0_MEM_ADDR+LX0_MEM_LEN), WORDSIZE);
            for(ptr = LX0_MEM_ADDR; ptr < (LX0_MEM_ADDR+LX0_MEM_LEN); ptr+=WORDSIZE)
                *((unsigned int volatile *) ptr) = 0;

            printf("Clear up LX1 memory: [0x%08x~0x%08x] WORDSIZE: %d...\n", LX1_MEM_ADDR, (LX1_MEM_ADDR+LX1_MEM_LEN), WORDSIZE);
            for(ptr = LX1_MEM_ADDR; ptr < (LX1_MEM_ADDR+LX1_MEM_LEN); ptr+=WORDSIZE)
                *((unsigned int volatile *) ptr) = 0;
        }
#endif

    printf("Factory mode.. skip verify_apps..\n");
    kernel_read();
#ifdef USING_SIMPLE_MMAP
//zzindda
#else

#if defined (CONFIG_SECURITY_BOOT)
#if defined(USE_TEE_THREAD)
    thread_tee = thread_create_ex("secure_tee", thread_secure_tee, NULL, 0, 3, THREAD_MAX_PRIORITY, 1);
#else
    verify_image(TRUE);
    secure_read_tee();
    secure_boot_tee();
#endif //defined(USE_TEE_THREAD)
#endif// defined (CONFIG_SECURITY_BOOT)

#if defined (CONFIG_SECURITY_BOOT)
#if defined(USE_TEE_THREAD)
    if( thread_tee ) thread_join(thread_tee, NULL);
#endif
    printf("Check TEE ready\n");
    wait_tee_ready();
    printf("TEE BringUp Done\n");
#endif
#endif
/* =================================================
    ursa vbyone lock ~ inverter t3 time spec A!M!PI AICI?c A!¡?A! AI¢Gg?
    snapshot resume mode : cmd_resume.c - snapshot jump AIAuA!M!PI AI¢Gg?
    snapshot making mode : CusSystem.c - kernel jump AIAuA!M!PI AI¢Gg?
================================================= */

#if defined(CONFIG_MULTICORES_PLATFORM)
        if( thread[1] ) thread_join(thread[1], NULL);
#endif
        kernel_jump();

    // zzindda
#if 0
        if(DDI_NVM_GetSWUMode() != 0) //swum on case
        {
            sprintf(cmd2,"%s;%s","cp2ram swue 0x04000000;verification 0 swue 0x04000000",cmd1);
            printf("bootcmd cmd = %s \n",cmd2);
            run_command(cmd2, 0);
        }
        //SHOWLOG();
        polling_timer();
#endif
    }

#if 0
        disable_wp(0); // eMMC W/P disable for user debugging in boot prompt
#endif

    return 0;
}

void Splash_DrawImage(unsigned int x, unsigned int y, unsigned int u4Width, unsigned int u4Height, unsigned int u4BmpAddr, int panelRes)
{
    return;

// zzindda
#if 0
    UINT32 u4RegionList, u4Region, u4FBPitch = 0, u4BmpPitch = 0;
    UINT32 u4PanelWidth, u4PanelHeight;
    INT32 ret;
    UINT32 u4ColorMode = 14; // ARGB8888
    UINT32 u4FBBuffer = 0, u4HeaderBuffer = 0, u4FirstRegionAddr = 0;
    UINT8 *pu1TmpBuf = NULL;

    u4FBBuffer = TOTAL_MEM_SIZE - FBM_MEM_CFG_SIZE - DIRECT_FB_MEM_SIZE - 1920*1080*4;
    u4HeaderBuffer = TOTAL_MEM_SIZE - FBM_MEM_CFG_SIZE - DIRECT_FB_MEM_SIZE - 1920*1080*8;


    Printf("Color:%d BmpAddr:0x%08x Width:%d Height:%d\n", (int)u4ColorMode, u4BmpAddr, (int)u4Width, (int)u4Height);
#if 1
    ret = decode_bmp(u4BmpAddr, &u4Width, &u4Height, &pu1TmpBuf);
    if (ret != BMPR_OK)
    {
    return 1;
    }
    u4BmpAddr = pu1TmpBuf;
#endif

  //printf("test debug Splash_DrawImage 2222222222222 u4FBBuffer =0x%x  x= %d, y=%d\n",u4FBBuffer, x, y);

    if(!fgBootLogoInit)
    {
        fgBootLogoInit = 1;

        OSD_Init();

        _OSD_BASE_SetScrnHStartOsd2(wDrvGetHsyncBp() - 2);
        _OSD_BASE_UpdateHwReg();
        vDrvFireImportPortection();

        //GFX_Reset();
        u4PanelWidth = PANEL_GetPanelWidth();
        u4PanelHeight = PANEL_GetPanelHeight();

        Printf("Panel %d x %d \n", (int)u4PanelWidth, (int)u4PanelHeight);
        memset((void *)u4FBBuffer, 0 , 1920*1080*4);
    memset((void *)u4HeaderBuffer, 0 , 32);

        ret = OSD_RGN_LIST_Create(&u4RegionList);
        if (ret != OSD_RET_OK) return 1;
        // to set u4BmpPitch by u4ColorMode and u4Width.
        OSD_GET_PITCH_SIZE(u4ColorMode, UBOOT_LOGO_WIDTH, u4FBPitch);
        if(GetCurrentPanelIndex() == PANEL_PDP_HD_60HZ)
        {
#if 1
            ret = OSD_RGN_Create(&u4Region, 1920, 450, (void *)u4FBBuffer,
                                u4ColorMode, u4FBPitch, 0, 206, 1024, 320);
#else
            ret = OSD_RGN_Create(&u4Region, 1920, 1080, (void *)u4FBBuffer,
                                u4ColorMode, u4FBPitch, 0, 0, 1024, 768);
#endif
        }
        else
        {
#if 1
            if(panelRes == 1)//FHD
            {
                ret = OSD_RGN_Create(&u4Region, 1920, 450, (void *)u4FBBuffer,
                                u4ColorMode, u4FBPitch, 0, 290, 1920, 450);
            }
            else
            {
                ret = OSD_RGN_Create(&u4Region, 1920, 450, (void *)u4FBBuffer,
                                u4ColorMode, u4FBPitch, 0, 206, 1366, 320);
            }
#else
            ret = OSD_RGN_Create(&u4Region, UBOOT_LOGO_WIDTH, UBOOT_LOGO_HEIGHT, (void *)u4FBBuffer,
                                u4ColorMode, u4FBPitch, 0, 0, UBOOT_LOGO_WIDTH, UBOOT_LOGO_HEIGHT);
#endif

        }
        if (ret != OSD_RET_OK) return 2;
        ret = OSD_RGN_Insert(u4Region, u4RegionList);
        if (ret != OSD_RET_OK) return 3;
        ret = OSD_PLA_FlipTo(OSD_PLANE_2, u4RegionList);
        if (ret != OSD_RET_OK) return 4;
/*
        ret = OSD_RGN_Set(u4Region, OSD_RGN_POS_X, x);
        if (ret != OSD_RET_OK) return 5;
        ret = OSD_RGN_Set(u4Region, OSD_RGN_POS_Y, y);
        if (ret != OSD_RET_OK) return 6;
*/
        ret = OSD_RGN_SetBigEndian(u4Region, TRUE);
        if (ret != OSD_RET_OK) return 8;

    _OSD_RGN_GetAddress((UINT32)u4Region, &u4FirstRegionAddr);

    memcpy((void*)u4HeaderBuffer, (void*)u4FirstRegionAddr, sizeof(OSD_RGN_UNION_T));

    _OSD_PLA_SetHeaderAddr(OSD_PLANE_2, u4HeaderBuffer);

    u4FBBuffer += u4FBPitch*y + x*4;

    //printf("test debug Splash_DrawImage 44444444444444444 u4FBBuffer =0x%x    x= %d, y=%d\n",u4FBBuffer, x, y);
    OSD_GET_PITCH_SIZE(u4ColorMode, u4Width, u4BmpPitch);
        Loader_SwBitBlt((UINT8 *)u4BmpAddr, (UINT8 *)u4FBBuffer, u4BmpPitch, u4FBPitch, u4ColorMode, u4ColorMode, u4Width,
    u4Height, 0, 0, 0, 0, 0, 0, 0);

        HalFlushDCache();

        ret = OSD_PLA_Enable(OSD_PLANE_2, TRUE);
        if (ret != OSD_RET_OK) return 9;
    }
    else
    {
        OSD_GET_PITCH_SIZE(u4ColorMode, u4Width, u4BmpPitch);
        OSD_GET_PITCH_SIZE(u4ColorMode, UBOOT_LOGO_WIDTH, u4FBPitch);

        u4FBBuffer += u4FBPitch*y + x*4;

        //printf("test debug Splash_DrawImage 555555555 u4FBBuffer =0x%x    x= %d, y=%d\n",u4FBBuffer, x, y);
        Loader_SwBitBlt((UINT8 *)u4BmpAddr, (UINT8 *)u4FBBuffer, u4BmpPitch, u4FBPitch, u4ColorMode, u4ColorMode, u4Width,
                     u4Height, 0, 0, 0, 0, 0, 0, 0);
        HalFlushDCache();

    }
    return 0;
#endif
}


int is_factory_mode(void)
{
    if( MICOM_IsPowerOnly() || !DDI_NVM_GetInstopStatus() )
        return 1;

    return 0;
}

#endif
