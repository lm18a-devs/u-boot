#include <common.h>
#include <CusCmnio.h>
#include <x_typedef.h>
#include <cmnio_type.h>
#include <drvGPIO.h>
#include <drvPWM.h>
#include <drvMBX.h>
#include <spinlock.h>
#include <drvSAR.h>
#include <MsApiPanel.h>
//-----------------------------------------------------------------------------
// Configurations
//-----------------------------------------------------------------------------

//---------------------------------------------------------------------------
// Constant definitions
//---------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// Macros
//-----------------------------------------------------------------------------
#define CHECK_ZERO_ADD_ONE(Z)	if( Z == 0 ){ Z = 1; }

//---------------------------------------------------------------------------
// Type definitions
//---------------------------------------------------------------------------
#define PWM_PORT_A	0// 0
#define PWM_PORT_B	1// 1
#define PWM_PORT_C	2

#define PWM_BASE_XTAL 4000000UL
#define PWM_CALIBRATE 255UL
#define MSTAR_BOARD_BRING_UP_2015_04_21 1

typedef enum
{
    E_HWOPT_LEVEL0  = 0,  
    E_HWOPT_LEVEL1  = 1,
    E_HWOPT_LEVEL2  = 2, 
    E_HWOPT_LEVEL3  = 3,   
    E_HWOPT_LEVEL_MAX = 99,
}LGE_HWOPT_LV;

//-----------------------------------------------------------------------------
// Local variables
//-----------------------------------------------------------------------------
#define MICOM_TYPE  DDI_CMNIO_GetMicomType()
#define IIC_HWSEM_LOCK 1
#if IIC_HWSEM_LOCK
#include <drvSEM.h>
#define _HWI2C_ENTRY()       do{                \
   while (MDrv_SEM_Get_Resource((MS_U8)E_SEM_PM, (MS_U16)SEM_RESOURCE_ID) == FALSE); \
}while(0)

#define _HWI2C_RETURN()     do{    \
     MDrv_SEM_Free_Resource((MS_U8)E_SEM_PM, (MS_U16)SEM_RESOURCE_ID); \
}while(0)
#else
#define _HWI2C_ENTRY()      do{ }while(0)
#define _HWI2C_RETURN()     do{ }while(0)
#endif

//---------------------------------------------------------------------------
// Static variables
//---------------------------------------------------------------------------
static UINT8 u8InitPWM = 0;

static unsigned int gEndPosition = 0;
static unsigned int gPhaseShift = 0;
static spin_lock_t g_i2c_lock[MAX_I2C_CH] = {INIT_SPIN_LOCK, INIT_SPIN_LOCK};

char aHW_model_opt[NUM_MODEL_OPT+1] = {0,};
char strModelOpt[NUM_MODEL_OPT+1] = {0,};
MODELOPT_T	gModelOpt = {0,};
char strHWOption[NUM_HW_OPT+1] = {0,};

// panel_interface,	resolution,	panel_type,	cp_box, smalm smart, optic
static MODELOPT_BITCOMB_T gmodelopt_bitcomb[NUM_MODELOPT_BITCOMB] =
{
	{MODELOPT_PANEL_INTERFACE_LVDS,		MODELOPT_PANEL_RESOLUTION_FHD,	0,	MODELOPT_PANEL_TYPE_NONE,	0,	0,	0},
	{MODELOPT_PANEL_INTERFACE_LVDS,		MODELOPT_PANEL_RESOLUTION_FHD,	0,	MODELOPT_PANEL_TYPE_NONE,	1,	0,	0},
	{0, 0, 0, 0, 0, 0, 0},
	{MODELOPT_PANEL_INTERFACE_LVDS,     MODELOPT_PANEL_RESOLUTION_HD,   0,  MODELOPT_PANEL_TYPE_NONE,   0,  0,  0},
};

// panel_resolution, panel_interface, panel_type, module_type
static ADC_BACKEND_OPT_T g_adc_backend_opt_table[NUM_BACKEND_OPT_BITCOMB] =
{
	{MODELOPT_PANEL_RESOLUTION_UD,	MODELOPT_PANEL_INTERFACE_VBYONE, 	MODELOPT_PANEL_TYPE_NONE, 	MODELOPT_MODULE_TYPE_RGB, 	MODELOPT_PANEL_BW_NONE},
	{0,								0,									0,							0,							MODELOPT_PANEL_BW_NONE},
	{0,								0,									0,							0,							MODELOPT_PANEL_BW_NONE},
	{0,								0,									0,							0,							MODELOPT_PANEL_BW_NONE},

	{MODELOPT_PANEL_RESOLUTION_UD,	MODELOPT_PANEL_INTERFACE_EPI, 		MODELOPT_PANEL_V18, 		MODELOPT_MODULE_TYPE_RGB,	MODELOPT_PANEL_BW_3_0G},
	{MODELOPT_PANEL_RESOLUTION_UD,	MODELOPT_PANEL_INTERFACE_EPI, 		MODELOPT_PANEL_V18, 		MODELOPT_MODULE_TYPE_MPLUS,	MODELOPT_PANEL_BW_3_0G},
	{MODELOPT_PANEL_RESOLUTION_UD,	MODELOPT_PANEL_INTERFACE_EPI, 		MODELOPT_PANEL_V17,	 		MODELOPT_MODULE_TYPE_MPLUS,	MODELOPT_PANEL_BW_3_0G},
	{0,								0,									0,							0,							MODELOPT_PANEL_BW_NONE},

	{MODELOPT_PANEL_RESOLUTION_UD,	MODELOPT_PANEL_INTERFACE_CEDS, 		MODELOPT_PANEL_V18, 		MODELOPT_MODULE_TYPE_RGB,	MODELOPT_PANEL_BW_1_5G},
	{MODELOPT_PANEL_RESOLUTION_UD,	MODELOPT_PANEL_INTERFACE_CEDS, 		MODELOPT_PANEL_V18, 		MODELOPT_MODULE_TYPE_RGBW,	MODELOPT_PANEL_BW_1_5G},
	{0,								0,									0,							0,							MODELOPT_PANEL_BW_NONE},
	{0,								0,									0,							0,							MODELOPT_PANEL_BW_NONE},

	{MODELOPT_PANEL_RESOLUTION_UD,	MODELOPT_PANEL_INTERFACE_EPI, 		MODELOPT_PANEL_V18, 		MODELOPT_MODULE_TYPE_RGB,	MODELOPT_PANEL_BW_2_1G},
	{MODELOPT_PANEL_RESOLUTION_UD,	MODELOPT_PANEL_INTERFACE_EPI, 		MODELOPT_PANEL_V18, 		MODELOPT_MODULE_TYPE_MPLUS,	MODELOPT_PANEL_BW_2_1G},
	{MODELOPT_PANEL_RESOLUTION_UD,	MODELOPT_PANEL_INTERFACE_EPI_QSAC, 	MODELOPT_PANEL_V18, 		MODELOPT_MODULE_TYPE_RGB,	MODELOPT_PANEL_BW_3_0G},
	{MODELOPT_PANEL_RESOLUTION_UD,	MODELOPT_PANEL_INTERFACE_EPI_QSAC, 	MODELOPT_PANEL_V18, 		MODELOPT_MODULE_TYPE_MPLUS,	MODELOPT_PANEL_BW_3_0G},
};

//-----------------------------------------------------------------------------
// Local Functions
//-----------------------------------------------------------------------------
extern UINT8 DDI_NVM_GetPWM_mode( void );

//-------------------------------------------------------------------------
//
// U-BOOT API Implementation (declared in uboot-xxx/include)
//
//-------------------------------------------------------------------------
LGE_HWOPT_LV CUS_MSTAR_ADC_VOLTAGE_READ(UINT8 ch)
{
     LGE_HWOPT_LV   hw_type_lv  = E_HWOPT_LEVEL_MAX;
     unsigned int          u32ADC_VALUE = 0;
      
    //u8ADC_VALUE = MDrv_SAR_Adc_GetValue(ch);
    *(volatile unsigned int*)(0x1f002800) = *(volatile unsigned int*)(0x1f002800) | 0x4000;

    u32ADC_VALUE = (*(volatile unsigned int*)(0x1f002800 + ((0x40 + ch)*4)))/4;

    if (u32ADC_VALUE <= ADC_MODEL_OPT_0)
        hw_type_lv = E_HWOPT_LEVEL0;
    else if(u32ADC_VALUE <= ADC_MODEL_OPT_1)
        hw_type_lv = E_HWOPT_LEVEL1;
    else if(u32ADC_VALUE <= ADC_MODEL_OPT_2)
        hw_type_lv = E_HWOPT_LEVEL2;
    else if(u32ADC_VALUE <= ADC_MODEL_OPT_3)
        hw_type_lv = E_HWOPT_LEVEL3;

    printf("[CH %d][ADC = %d][LV %d] \n", ch, u32ADC_VALUE, hw_type_lv);
     
    return hw_type_lv  ; 
}

#if 0
int DDI_GPIO_SetDefault(void)
{
	// zzindda
	//CMNIO_GPIO_SetOutputPort(GPIO_PORT_WR_LOCAL_DIM_OS_PANEL_CTRL, 0);

	return 0;
}
#endif

void modelOpt_parsing()
{
    uint8_t bitcomb = (aHW_model_opt[2] * NUM_BACKEND_OPT_LEV) + aHW_model_opt[3];
    
    // BIT0 - country
    gModelOpt.country_type = (uint8_t)aHW_model_opt[0];
    
    // BIT1 - tuner
    gModelOpt.tuner_type   = (uint8_t)aHW_model_opt[1];


    // BIT2/3 - backend
    if (bitcomb < NUM_BACKEND_OPT_BITCOMB)
	{
        gModelOpt.panel_resolution  = g_adc_backend_opt_table[bitcomb].panel_resolution;
        gModelOpt.panel_interface   = g_adc_backend_opt_table[bitcomb].panel_interface;
        gModelOpt.panel_type        = g_adc_backend_opt_table[bitcomb].panel_type;
        gModelOpt.module_type       = g_adc_backend_opt_table[bitcomb].module_type;
		gModelOpt.panel_bandwidth	= g_adc_backend_opt_table[bitcomb].panel_bandwidth;
	}
    
    // BIT4 - PMIC/MICOM
	gModelOpt.micom_type = MODELOPT_MICOM_EXTERNAL;

    switch (aHW_model_opt[4])
    {
        case 0:
            {
				gModelOpt.micom_vendor = MODELOPT_MICOM_VENDOR_RENESAS;
				gModelOpt.pmic_type = MODELOPT_PMIC_FALSE;
            }
			break;
        case 1:
            {
				gModelOpt.micom_vendor = MODELOPT_MICOM_VENDOR_ABOV;
				gModelOpt.pmic_type = MODELOPT_PMIC_FALSE;
            }
            break;
        case 2:
            {
				gModelOpt.micom_vendor = MODELOPT_MICOM_VENDOR_RENESAS;
                if (gModelOpt.panel_interface == MODELOPT_PANEL_INTERFACE_EPI || gModelOpt.panel_interface == MODELOPT_PANEL_INTERFACE_EPI_QSAC)
                    gModelOpt.pmic_type = MODELOPT_PMIC_TRUE;
                else
                    gModelOpt.pmic_type = MODELOPT_PMIC_FALSE;
            }
            break;
        case 3:
            {
				gModelOpt.micom_vendor = MODELOPT_MICOM_VENDOR_ABOV;
                if (gModelOpt.panel_interface == MODELOPT_PANEL_INTERFACE_EPI || gModelOpt.panel_interface == MODELOPT_PANEL_INTERFACE_EPI_QSAC)
                    gModelOpt.pmic_type = MODELOPT_PMIC_TRUE;
                else
                    gModelOpt.pmic_type = MODELOPT_PMIC_FALSE;
            }
            break;
    }
    
    // BIT5 - reserved

	// BIT6 - EWBS
	if(aHW_model_opt[6]) // Low : EWBS Tuner, High : Non-EWBS Tuner
		gModelOpt.isSupportEWBS = FALSE;
	else	
		gModelOpt.isSupportEWBS = TRUE;

	// BIT7 - reserved
	
    // BIT8 - reserved
    
    // Fixed Model OPT
    gModelOpt.ddr_size = DDR_SIZE_1_5G;
    gModelOpt.graphic_resolution = MODELOPT_GRAPHIC_1920X1080;
    gModelOpt.bSupport_external_EDID = 1;
    gModelOpt.bSupportOptic = 0;
    gModelOpt.bSupport_cp_box = FALSE;
    gModelOpt.bSupport_frc = 0;
   

    sprintf(strModelOpt, "%x%x%x%x%x%x%x%x%x",
        aHW_model_opt[0],
        aHW_model_opt[1],
        aHW_model_opt[2],
        aHW_model_opt[3],
        aHW_model_opt[4],
        aHW_model_opt[5],
        aHW_model_opt[6],
        aHW_model_opt[7],
        aHW_model_opt[8]
        );
 
    sprintf(strHWOption, "%x%x%x%x%x%x%x%x%x%x%x%x%x%x%x%x%x%x",
            gModelOpt.country_type,             // 1. HW_OPT_COUNTRY
            gModelOpt.pmic_type,                // 2. HW_OPT_PMIC_TYPE
            gModelOpt.panel_interface,          // 3. HW_OPT_PANEL_INTERFACE
            gModelOpt.panel_resolution,         // 4. HW_OPT_PANEL_RESOLUTION
            gModelOpt.bSupport_frc,             // 5. HW_OPT_FRC
            gModelOpt.panel_type,               // 6. HW_OPT_PANEL_TYPE
            gModelOpt.bSupport_cp_box,          // 7. HW_OPT_CP_BOX
            gModelOpt.micom_vendor,             // 8. HW_OPT_MICOM_VENDOR
            gModelOpt.tuner_type,               // 9. HW_OPT_TUNER
            gModelOpt.panel_bandwidth,			// 10. HW_OPT_PANEL_BANDWIDTH
            gModelOpt.isSupportEWBS,            // 11. HW_OPT_EWBS
            gModelOpt.ddr_size,                 // 12. HW_OPT_DDR_SIZE
            gModelOpt.bSupportOptic,            // 13. HW_OPT_OPTIC
            0,                                  // 14. reserved
            gModelOpt.graphic_resolution,       // 15. HW_OPT_GRAPHIC_RESOLUTION
            gModelOpt.micom_type,               // 16. HW_OPT_MICOM_TYPE
            gModelOpt.module_type,              // 17. HW_OPT_MODULE_TYPE
            gModelOpt.frc_type                  // 18. HW_OPT_FRC_TYPE
           );
    
    printf("HW_OPT_COUNTRY: %d\n", gModelOpt.country_type);                 // 1. HW_OPT_COUNTRY
    printf("HW_OPT_PMIC_TYPE: %d\n", gModelOpt.pmic_type);                  // 2. HW_OPT_PMIC_TYPE
    printf("HW_OPT_PANEL_INTERFACE: %d\n", gModelOpt.panel_interface);      // 3. HW_OPT_PANEL_INTERFACE
    printf("HW_OPT_PANEL_RESOLUTION: %d\n", gModelOpt.panel_resolution);    // 4. HW_OPT_PANEL_RESOLUTION
    printf("HW_OPT_FRC: %d\n", gModelOpt.bSupport_frc);                     // 5. HW_OPT_FRC
    printf("HW_OPT_PANEL_TYPE: %d\n", gModelOpt.panel_type);                // 6. HW_OPT_PANEL_TYPE
    printf("HW_OPT_CP_BOX: %d\n", gModelOpt.bSupport_cp_box);               // 7. HW_OPT_CP_BOX
    printf("HW_OPT_MICOM_VENDOR: %d\n", gModelOpt.micom_vendor);            // 8. HW_OPT_MICOM_VENDOR
    printf("HW_OPT_TUNER: %d\n", gModelOpt.tuner_type);                     // 9. HW_OPT_TUNER
    printf("HW_OPT_PANEL_BANDWIDTH: %d\n", gModelOpt.panel_bandwidth);		// 10. HW_OPT_PANEL_BANDWIDTH
    printf("HW_OPT_EWBS: %d\n", gModelOpt.isSupportEWBS);                   // 11. HW_OPT_EWBS
    printf("HW_OPT_DDR_SIZE: %d\n", gModelOpt.ddr_size);                    // 12. HW_OPT_DDR_SIZE
    printf("HW_OPT_OPTIC: %d\n", gModelOpt.bSupportOptic);                  // 13. HW_OPT_OPTIC
    printf("HW_OPT_GRAPHIC_RESOLUTION: %d\n", gModelOpt.graphic_resolution);// 15. HW_OPT_GRAPHIC_RESOLUTION
    printf("HW_OPT_MICOM_TYPE: %d\n", gModelOpt.micom_type);                // 16. HW_OPT_MICOM_TYPE
    printf("HW_OPT_MODULE_TYPE: %d\n", gModelOpt.module_type);              // 17. HW_OPT_MODULE_TYPE
    printf("HW_OPT_FRC_TYPE: %d\n", gModelOpt.frc_type);                    // 18. HW_OPT_FRC_TYPE
    
    printf("[HW_OPT] = %s\n", strHWOption);

}

void initHW_model_option()
{
	int i = 0;
	uchar temp = 0;
    for(i = 0; i < 6; i++) // 0 ~ 5 ADC
        aHW_model_opt[i] = (char)CUS_MSTAR_ADC_VOLTAGE_READ(i);

	// 6 ~ 8 High/Low
	if(CMNIO_GPIO_GetInputPort(GPIO_MODEL_OPT6, &temp)>=0)
	{
		printf("[CH 6][Value = %d] \n", temp);
		aHW_model_opt[6] = temp;
	}
	else
		printf("[CH 6]Read Failed \n");

	if(CMNIO_GPIO_GetInputPort(GPIO_MODEL_OPT7, &temp)>=0)
	{
		printf("[CH 7][Value = %d] \n", temp);
		aHW_model_opt[7] = temp;
	}
	else
		printf("[CH 7]Read Failed \n");

	if(CMNIO_GPIO_GetInputPort(GPIO_MODEL_OPT8, &temp)>=0)
	{
		printf("[CH 8][Value = %d] \n", temp);
		aHW_model_opt[8] = temp;
	}
	else
		printf("[CH 8]Read Failed \n");
		
	
	// parsing model option
	modelOpt_parsing();

	printf("[HW_OPTION] = %s \n", strModelOpt);
}


unsigned int DDI_PWM_GetStartPosition(PWM_SCANNING_IDX_T eScanningIdx, UINT32 data)
{
	UINT32 startposition = 0;

	if(eScanningIdx == PWM_SCANNING_IDX_0)
	{
		if( (gEndPosition + gPhaseShift) <= 255 )
		{
			if( (gEndPosition + gPhaseShift) < data)
				startposition = 0;
			else
				startposition = (gEndPosition + gPhaseShift) - data;
		}
		else
			startposition = 255 - data;
	}
	else
	{
		if(gEndPosition < data)
			startposition = 0;
		else
			startposition = gEndPosition - data;
	}

	return startposition;

}


int DDI_PWM_Init(PANEL_PWM_T pnlpw)
{
	UINT8	pwm_mode = PWM_DRIVING_LED_CURRENT;

	UINT8	u_hex_LEDCur, u_vbr_duty;

	u_hex_LEDCur = (pnlpw.vbrCLedCurrent*255)/ 100;

	if(pnlpw.vbrBBootlogo > pnlpw.vbrBMaxDuty)
		u_vbr_duty	 = (pnlpw.vbrBMaxDuty*255)/ 100;
	else
		u_vbr_duty	 = (pnlpw.vbrBBootlogo*255)/ 100;

	DDI_CMNIO_PWM_PreInit();

	pwm_mode = DDI_NVM_GetPWM_mode();

	if( (pwm_mode != PWM_DRIVING_LED_CURRENT) &&
		(pwm_mode != PWM_DRIVING_2CH_PHASE_DIFF) &&
		(pwm_mode != PWM_DRIVING_2CH_PHASE_SAME) )
		pwm_mode = PWM_DRIVING_LED_CURRENT;	// default

	printf("PWM Mode %d\n", pwm_mode);

	if(pwm_mode == PWM_DRIVING_2CH_PHASE_DIFF)
	{
		// initialize driver for phase shift mode
		gEndPosition = 200;
		gPhaseShift = 50;
	}
	else
	{
		gEndPosition = 0;
		gPhaseShift = 0;
	}

	if(pwm_mode == PWM_DRIVING_LED_CURRENT)
	{
		printf("[PWM] --- VbrB Init duty = %d\n", u_vbr_duty);
		printf("[PWM][LED Current] PWM Freq = %d\n",pnlpw.vbrCFreq);
		printf("[PWM][LED Current] Duty = %d\n", u_hex_LEDCur);

		DDI_CMNIO_PWM_ApplyParamSet(PWM_PORT_A, 1, u_vbr_duty, pnlpw.vbrBFreq60hz, 1, DDI_PWM_GetStartPosition(PWM_SCANNING_IDX_0, u_vbr_duty));
		DDI_CMNIO_PWM_ApplyParamSet(PWM_PORT_B, 1, u_hex_LEDCur, pnlpw.vbrCFreq, 0, DDI_PWM_GetStartPosition(PWM_SCANNING_IDX_0, u_hex_LEDCur));
	}
	else if(pwm_mode == PWM_DRIVING_2CH_PHASE_DIFF)
	{
		printf("[PWM] --- VbrB Init duty = %d\n", u_vbr_duty);
		DDI_CMNIO_PWM_ApplyParamSet(PWM_PORT_A, 1, u_vbr_duty, pnlpw.vbrBFreq60hz, 1, DDI_PWM_GetStartPosition(PWM_SCANNING_IDX_0, u_vbr_duty));
		DDI_CMNIO_PWM_ApplyParamSet(PWM_PORT_B, 1, u_vbr_duty, pnlpw.vbrBFreq60hz, 1, DDI_PWM_GetStartPosition(PWM_SCANNING_IDX_1, u_vbr_duty));
	}
	else if(pwm_mode == PWM_DRIVING_2CH_PHASE_SAME)
	{
		printf("[PWM] --- VbrB Init duty = %d\n", u_vbr_duty);
		DDI_CMNIO_PWM_ApplyParamSet(PWM_PORT_A, 1, u_vbr_duty, pnlpw.vbrBFreq60hz, 1, DDI_PWM_GetStartPosition(PWM_SCANNING_IDX_0, u_vbr_duty));
		DDI_CMNIO_PWM_ApplyParamSet(PWM_PORT_B, 1, u_vbr_duty, pnlpw.vbrBFreq60hz, 1, DDI_PWM_GetStartPosition(PWM_SCANNING_IDX_1, u_vbr_duty));
	}
	else
		printf("Invalid PWM Mode %d", pwm_mode);


	return 0;
}


void DDI_CMNIO_PWM_PreInit(void)//UINT32 bEnableSCANPWM, UINT32 bDoubleFreqSCANPWM, UINT32 bPhaseShift)
{

    int retVal = MDrv_PWM_Init(E_PWM_DBGLV_ERR_ONLY);

    if(retVal == E_PWM_OK)
    {
        u8InitPWM = 1;
    }
    else
    {
        printf("Fail to Init PWM\n");
        return;
    }

    MDrv_PWM_Oen(0, 0); //enable PWM_DIM
    MDrv_PWM_Oen(1, 0); //enable PWM_DIM2

    return;


}


void DDI_CMNIO_PWM_ApplyParamSet(UINT8 pwmIndex, UINT8 m_pwm_enable, UINT32 m_pwm_duty,
                                              UINT32 m_pwm_frequency, UINT32 m_pwm_lock, UINT32 m_pwm_pos_start)
{
    UINT32 u32Period, u32Duty, u32Shift;
    UINT32 temp_pwm_frequency;

    if(u8InitPWM == 0)
        DDI_CMNIO_PWM_PreInit();

    if(m_pwm_frequency == 0)
        temp_pwm_frequency = 1;
    else
        temp_pwm_frequency = m_pwm_frequency;

    if(m_pwm_enable)
        MDrv_PWM_Oen(pwmIndex, 0);
    else
        MDrv_PWM_Oen(pwmIndex, 1);


    MDrv_PWM_Div(pwmIndex, 2);// (12/4)-1 = 2
    MDrv_PWM_Polarity(pwmIndex, 0);

    u32Period = ( PWM_BASE_XTAL / temp_pwm_frequency );


    u32Shift = u32Period * m_pwm_pos_start / PWM_CALIBRATE;
    u32Duty = u32Period * m_pwm_duty / PWM_CALIBRATE;

    MDrv_PWM_Period(pwmIndex, (u32Period - 1));

    if(u32Shift == 0)
        u32Shift = 1;

    if(u32Duty > 0)
    {
        MDrv_PWM_Shift(pwmIndex, u32Shift);
        MDrv_PWM_DutyCycle(pwmIndex, u32Shift + (u32Duty - 1));
    }
    else
    {
        MDrv_PWM_Shift(pwmIndex, 0);
        MDrv_PWM_DutyCycle(pwmIndex, 0);
    }

    if(m_pwm_lock )
    {
        MDrv_PWM_Dben(pwmIndex, 0);
        MDrv_PWM_Vdben(pwmIndex, 1);
        MDrv_PWM_ResetEn(pwmIndex, 1);
        MDrv_PWM_RstMux(pwmIndex, 0);
    }
    else
    {
        MDrv_PWM_Dben(pwmIndex, 1);
        MDrv_PWM_Vdben(pwmIndex, 0);
        MDrv_PWM_ResetEn(pwmIndex, 0);
        MDrv_PWM_RstMux(pwmIndex, 0);
    }
}


int DDI_CMNIO_PWM_SetFrequency(uint pwmIndex, uint data)
{
    //printf("[DDI_CMNIO_PWM_SetFrequency] %d, %d\n", pwmIndex, data);

    if(u8InitPWM)
    {
        MDrv_PWM_Div(pwmIndex, data);
    }

    return 0;
}

int DDI_CMNIO_PWM_SetDutyCycle(uint pwmIndex, uint data)
{
    //printf("[DDI_CMNIO_PWM_SetDutyCycle] %d, %d\n", pwmIndex, data);

    if(u8InitPWM)
    {
        MDrv_PWM_DutyCycle(pwmIndex, data);
    }
    // zzindda

    return 0;
}

int DDI_CMNIO_PWM_SetEnable(uint pwmIndex, uint data)
{
// zzindda
#if 0
    if(pwmIndex > 2) return -1;
    _pwm[pwmIndex].enable = data;
    _PwmSet(pwmIndex, &_pwm[pwmIndex]);
#endif
    return 0;
}

int DDI_CMNIO_PWM_SetPulseWidth(uint pwmIndex, uint data)
{
	//printf("[DDI_CMNIO_PWM_SetPulseWidth] %d, %x\n", pwmIndex, data);

    if(u8InitPWM)
    {
        MDrv_PWM_Period(pwmIndex, data);
    }

    return 0;
}

int DDI_CMNIO_PWM_SetVsyncAlign(uint pwmIndex, uint data)
{
// zzindda
#if 0
    if(pwmIndex > 2) return -1;

	vDrvSetLock(pwmIndex, data);
#endif
    return 0;
}

int CMNIO_GPIO_Init()
{
    mdrv_gpio_init();

#if 0
    CMNIO_GPIO_SetOutputPort(GPIO_PORT_WR_DATA_FORMAT_0,0);
    CMNIO_GPIO_SetOutputPort(GPIO_PORT_WR_DATA_FORMAT_1,0);
    CMNIO_GPIO_SetOutputPort(GPIO_PORT_WR_CVBS_OUT_SEL,0);
    CMNIO_GPIO_SetOutputPort(GPIO_PORT_WR_MN864778_RESET,0);
#endif

    return 0;
}

int CMNIO_GPIO_SetPortDirection(uint portIndex, char direction)
{
// zzindda
#if 0
	GPIO_Enable(portIndex, &direction);
#endif
	return 0;
}

extern void mdrv_gpio_set_high(MS_GPIO_NUM gpio);
extern void mdrv_gpio_set_low(MS_GPIO_NUM gpio);
extern void mdrv_gpio_set_input(MS_GPIO_NUM gpio);
extern int mdrv_gpio_get_inout(MS_GPIO_NUM gpio);
extern int mdrv_gpio_get_level(MS_GPIO_NUM gpio);

// mdrv_gpio_get_inout() : get gpio type (input or output)
// mdrv_gpio_get_level() : get gpio input value

int CMNIO_GPIO_SetOutputPort(uint portIndex, uchar data)
{
    if(portIndex == 0xffffffff || portIndex == 0) return -1;

	if (data == 0)
		mdrv_gpio_set_low(portIndex);
	else
		mdrv_gpio_set_high(portIndex);

    return 0;
}

int CMNIO_GPIO_GetInputPort(uint portIndex, uchar *pData)
{
    uint i4Mode=0;
    int i4Val;
    if(pData == NULL || portIndex == 0xffffffff || portIndex == 0) return -1;

	i4Val = mdrv_gpio_get_level(portIndex);
	if(i4Val >=0)
	{
		*pData = i4Val;
	}
	else
	{
		return -1;
	}
    return 0;
}

int CMNIO_GPIO_SetOutputPortArray(uint numArray, uint portArray[], uchar dataArray[])
{
    uint i = 0;

    for(i=0;i<numArray;i++)
		CMNIO_GPIO_SetOutputPort(portArray[i], dataArray[i]);

    return 0;
}

int CMNIO_GPIO_GetInputPortArray(uint numArray, uint portArray[], uchar dataArray[])
{
    uint i = 0;
    int i4Val;
    for(i=0;i<numArray;i++)
    {
		i4Val = mdrv_gpio_get_level(portArray[i]);
		if(i4Val>=0)
            dataArray[i] = i4Val;
        else
            return -1;
    }
    return 0;
}

int DDI_CMNIO_GetMicomType(void) //1 for PM51
{
    uchar val = 0;

    mdrv_gpio_init(); //_avoid gpio used before its init
//#if LG_BRING_UP
#if (ENABLE_MSTAR_M7621)
    return 0; //For LM18A external micom
#else
    CMNIO_GPIO_GetInputPort(GPIO_MODEL_OPT14, &val);
    return (val == 0)? 1: 0;
#endif
}

int DDI_CMNIO_I2C_Init(void)
{
	return msI2C_init();
}

UINT8 DDI_CMNIO_I2C_Write(uchar chNum, uchar transMode, UINT8 slaveAddr, UINT32 subAddrMode, UINT8 *subAddr, UINT16 nBytes, UINT8 *txBuf, UINT32 retry)
{
	UINT32 idx = 0;
	UINT8 ret = -1;
	UINT16 u16SlaveCfg;
        UINT16 flags = 0;

	MApi_SWI2C_Speed_Setting(chNum, transMode);
	u16SlaveCfg = (chNum << 8) | slaveAddr;

    if ( MICOM_TYPE )
         _HWI2C_ENTRY();

	for(idx = 0; idx <= retry; idx++)
	{
		spin_lock_save(&g_i2c_lock[chNum], flags);
		ret = MApi_SWI2C_WriteBytes(u16SlaveCfg, subAddrMode, subAddr, nBytes, txBuf);
		spin_unlock_restore(&g_i2c_lock[chNum], flags);
		//printf("%s[%d] :: u16SlaveCfg = 0x%x, subAddrMode = 0x%x, subAddr = 0x%x, nBytes = 0x%x, txBuf = 0x%x\n", __FUNCTION__, ret, u16SlaveCfg, subAddrMode, subAddr, nBytes, txBuf);
		if(ret == TRUE)
		{
		    if ( MICOM_TYPE )
                        _HWI2C_RETURN();

			return 0;
		}
	}

    if ( MICOM_TYPE )
        _HWI2C_RETURN();

	return -1;
}


UINT8 DDI_CMNIO_I2C_Read(uchar chNum, uchar transMode, UINT8 slaveAddr, UINT32 subAddrMode, UINT8 *subAddr, UINT16 nBytes, UINT8 *rxBuf, UINT32 retry)
{
	UINT32 idx = 0;
	UINT8 ret = -1;
	UINT16 u16SlaveCfg;
	UINT16 flags = 0;

	MApi_SWI2C_Speed_Setting(chNum, transMode);
	u16SlaveCfg = (chNum << 8) | slaveAddr;

    if ( MICOM_TYPE )
        _HWI2C_ENTRY();

	for(idx = 0; idx <= retry; idx++)
	{
		spin_lock_save(&g_i2c_lock[chNum], flags);
		ret = MApi_SWI2C_ReadBytes(u16SlaveCfg, subAddrMode, subAddr, nBytes, rxBuf);
		spin_unlock_restore(&g_i2c_lock[chNum], flags);
		//printf("%s[%d] :: u16SlaveCfg = 0x%x, subAddrMode = 0x%x, subAddr = 0x%x, nBytes = 0x%x, txBuf = 0x%x\n", __FUNCTION__, ret, u16SlaveCfg, subAddrMode, subAddr, nBytes, txBuf);
		if(ret == TRUE)
		{
            if ( MICOM_TYPE )
                        _HWI2C_RETURN();

			return 0;
		}
	}

    if ( MICOM_TYPE )
        _HWI2C_RETURN();

	return -1;
}

int DDI_CMNIO_MBX_Init(void)
{
    return msAPI_PM_MBX_Init();
}

int DDI_CMNIO_MBX_Write(MS_U8 u8Cmd, MS_U8 u8Cnt, MS_U8 *pu8Parameters)
{
    MBX_Msg MB_Command;
    MBX_Result MB_Result;

    memset((void*)&MB_Command, 0, sizeof(MBX_Msg));

    MB_Command.eRoleID      = E_MBX_ROLE_PM;
    MB_Command.eMsgType     = E_MBX_MSG_TYPE_INSTANT;
    MB_Command.u8Ctrl       = 0;
    MB_Command.u8MsgClass   = E_MBX_CLASS_PM_NOWAIT;

    MB_Command.u8Index = u8Cmd;
    MB_Command.u8ParameterCount = u8Cnt;

    memcpy(MB_Command.u8Parameters, pu8Parameters, u8Cnt);

    if((MB_Result = MDrv_MBX_SendMsg(&MB_Command)) != E_MBX_SUCCESS)
    {
        printf("[MBX]%s:FAIL(%d)\n",__func__, (int)MB_Result);
        return -1;
    }

    return 0;
}

int DDI_CMNIO_MBX_Read(MS_U8 u8Cmd, MS_U8 *data, MS_U8 len)
{
    MBX_Msg MB_Command;
    MBX_Result MB_Result;

    memset((void*)&MB_Command, 0, sizeof(MBX_Msg));

    MB_Command.eRoleID      = E_MBX_ROLE_PM;
    MB_Command.eMsgType     = E_MBX_MSG_TYPE_INSTANT;
    MB_Command.u8Ctrl       = 0;
    MB_Command.u8MsgClass   = E_MBX_CLASS_PM_NOWAIT;

    MB_Command.u8Index = u8Cmd;
    MB_Command.u8ParameterCount = 1;
    MB_Command.u8Parameters[0] = len;

    if((MB_Result = MDrv_MBX_SendMsg(&MB_Command)) != E_MBX_SUCCESS)
    {
        printf("[MBX]%s:FAIL(%d)\n",__func__, (int)MB_Result);
        return -1;
    }

    //(2) Waiting for message done
    memset((void*)&MB_Command, 0, sizeof(MBX_Msg));

    //do{
    MB_Result = MDrv_MBX_RecvMsg(E_MBX_CLASS_PM_NOWAIT, &MB_Command, 1000, MBX_CHECK_NORMAL_MSG);
    //}while((MB_Result  !=  E_MBX_SUCCESS) && (MB_Result  !=  E_MBX_ERR_TIME_OUT));

    //(3) check result
    if(MB_Result == E_MBX_SUCCESS)
    {
        memcpy((void *)data, (void *)&(MB_Command.u8Parameters), MB_Command.u8ParameterCount);
        return 0;
    }
    else if(MB_Result == E_MBX_ERR_TIME_OUT)
    {
        printf("[MBX]%s:TIMEOUT!!!\n",__func__);
    }
    else
    {
        printf("[MBX]%s:FAIL(%d)\n",__func__, (int)MB_Result);
    }

    return -1;
}

#if (CONFIG_LOCAL_DIMMING)
int DDI_LD_Init(void)
{
	UINT8	ld_mode = PWM_DRIVING_LED_CURRENT;

	ld_mode = DDI_NVM_GetPWM_mode();

	if(ld_mode == PWM_DRIVING_LED_CURRENT)
	{
        msAPI_LD_Init();
	}


	return 0;
}
#endif


