//<MStar Software>
//******************************************************************************
// MStar Software
// Copyright (c) 2010 - 2012 MStar Semiconductor, Inc. All rights reserved.
// All software, firmware and related documentation herein ("MStar Software") are
// intellectual property of MStar Semiconductor, Inc. ("MStar") and protected by
// law, including, but not limited to, copyright law and international treaties.
// Any use, modification, reproduction, retransmission, or republication of all
// or part of MStar Software is expressly prohibited, unless prior written
// permission has been granted by MStar.
//
// By accessing, browsing and/or using MStar Software, you acknowledge that you
// have read, understood, and agree, to be bound by below terms ("Terms") and to
// comply with all applicable laws and regulations:
//
// 1. MStar shall retain any and all right, ownership and interest to MStar
//    Software and any modification/derivatives thereof.
//    No right, ownership, or interest to MStar Software and any
//    modification/derivatives thereof is transferred to you under Terms.
//
// 2. You understand that MStar Software might include, incorporate or be
//    supplied together with third party`s software and the use of MStar
//    Software may require additional licenses from third parties.
//    Therefore, you hereby agree it is your sole responsibility to separately
//    obtain any and all third party right and license necessary for your use of
//    such third party`s software.
//
// 3. MStar Software and any modification/derivatives thereof shall be deemed as
//    MStar`s confidential information and you agree to keep MStar`s
//    confidential information in strictest confidence and not disclose to any
//    third party.
//
// 4. MStar Software is provided on an "AS IS" basis without warranties of any
//    kind. Any warranties are hereby expressly disclaimed by MStar, including
//    without limitation, any warranties of merchantability, non-infringement of
//    intellectual property rights, fitness for a particular purpose, error free
//    and in conformity with any international standard.  You agree to waive any
//    claim against MStar for any loss, damage, cost or expense that you may
//    incur related to your use of MStar Software.
//    In no event shall MStar be liable for any direct, indirect, incidental or
//    consequential damages, including without limitation, lost of profit or
//    revenues, lost or damage of data, and unauthorized system use.
//    You agree that this Section 4 shall still apply without being affected
//    even if MStar Software has been modified by MStar in accordance with your
//    request or instruction for your use, except otherwise agreed by both
//    parties in writing.
//
// 5. If requested, MStar may from time to time provide technical supports or
//    services in relation with MStar Software to you for your use of
//    MStar Software in conjunction with your or your customer`s product
//    ("Services").
//    You understand and agree that, except otherwise agreed by both parties in
//    writing, Services are provided on an "AS IS" basis and the warranty
//    disclaimer set forth in Section 4 above shall apply.
//
// 6. Nothing contained herein shall be construed as by implication, estoppels
//    or otherwise:
//    (a) conferring any license or right to use MStar name, trademark, service
//        mark, symbol or any other identification;
//    (b) obligating MStar or any of its affiliates to furnish any person,
//        including without limitation, you and your customers, any assistance
//        of any kind whatsoever, or any information; or
//    (c) conferring any license or right under any intellectual property right.
//
// 7. These terms shall be governed by and construed in accordance with the laws
//    of Taiwan, R.O.C., excluding its conflict of law rules.
//    Any and all dispute arising out hereof or related hereto shall be finally
//    settled by arbitration referred to the Chinese Arbitration Association,
//    Taipei in accordance with the ROC Arbitration Law and the Arbitration
//    Rules of the Association by three (3) arbitrators appointed in accordance
//    with the said Rules.
//    The place of arbitration shall be in Taipei, Taiwan and the language shall
//    be English.
//    The arbitration award shall be final and binding to both parties.
//
//******************************************************************************
//<MStar Software>

//-------------------------------------------------------------------------------------------------
// Includes
//-------------------------------------------------------------------------------------------------
#include <command.h>
#include <common.h>
#include <ShareType.h>
#include <msAPI_Power.h>
#include <drvWDT.h>
#include <MsSystem.h>
#include <CusConfig.h>
#include <MsVfs.h>
#include <MsDebug.h>
#include <drvBDMA.h>
#if (ENABLE_MSTAR_NUGGET==1)
#include <apiCEC.h>
#endif
#include <drvPM.h>
#include <MsUboot.h>
#include <MsMmap.h>
#include <MsDrvCache.h>
#if (ENABLE_MSTAR_NIKON==1)
#include <IR_MSTAR_DTV.h>
#endif

#include <asm/types.h>
#include <cmd_micom.h>
#include <CusCmnio.h>
#include <malloc.h>
#include <CusPM.h>
#include <partinfo.h>
#include <MsOS.h>
//-------------------------------------------------------------------------------------------------
//  Debug Functions
//-------------------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------------
//  Local Defines
//-------------------------------------------------------------------------------------------------
#define PM_INFO_MAGIC	0x20160905

#define PM_SHA_ALIGN		0x20
#define	PM_BIN_SIZE		0x10000
#define PM_SERIAL_SIZE	        0x100000
#define PM_SERIAL_ERASE_SIZE	0x1000
#define PM_HASH_SIZE		0x20
#define PM_EMMC_SIZE		(PM_SHA_ALIGN+PM_VER_SIZE+PM_BIN_SIZE+PM_HASH_SIZE)

#define	PM_INVALID		0xFF
#define	PM_VALID		0x55
#define PM_UPDATE       0x55

#define PM_READY_TIMEOUT_MS	5000
#define IMICOM_PART_NAME	"intmicom"

#define	CP_READ_MICOM_VERSION	0xA1
enum {
	PM_RUN_SUCCESS = 0,
	PM_RUN_TIMEOUT = 1,
	PM_RUN_FAIL = -1
};
//-------------------------------------------------------------------------------------------------
//  Global Variables
//-------------------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------------
//  Local Variables
//-------------------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------------
//  Extern Functions
//-------------------------------------------------------------------------------------------------
extern int snprintf(char *str, size_t size, const char *fmt, ...);
extern unsigned char  DDI_NVM_GetPMUpdate(void);
extern void DDI_NVM_SetPMUpdate(unsigned char update_flag);
extern int  emmc_read(off_t ofs, size_t len, u_char *buf);
//-------------------------------------------------------------------------------------------------
//  Private Functions
//-------------------------------------------------------------------------------------------------

#if (ENABLE_MSTAR_NIKON==1)
static int SetPm2L1(void);
#else
int SetPm2Sram(void);
#endif

void SetPM51Disable(void);

BOOLEAN get_poweroff_flag(void);
#if (CONFIG_WDT_RESET_BY_ESD)
static void set_poweroff_flag(BOOLEAN bEnable);
#endif
static int If_Boot_To_PM(void);
static BOOLEAN check_pm_standby(void);
static int PM51_PowerDown(void);
static unsigned int get_pm51_program_counter(void);
static BOOLEAN check_pm_alive(void);

//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------

static int PM51_PowerDown(void)
{
    char *s = NULL;
    int iStrCrc = 0;

    UBOOT_TRACE("IN\n");
    if(IsHouseKeepingBootingMode()==TRUE)
    {
        // bring up 51
        if( IsBootingFromMaskRom() == TRUE )
        {
            #if (ENABLE_MSTAR_EDISON==1)
                UBOOT_DEBUG("=== PATCH FOR EDISON GTV ROMBOOT BUT PM IN SPI ===\n");
                msAPI_SetSPIOffsetForMCU();
            #elif (ENABLE_MSTAR_NIKON==1)
                UBOOT_DEBUG("=== NiKon ROMBOOT BUT PM IN L1 ===\n");
                return SetPm2L1();
            #else
            UBOOT_DEBUG("== SetPm2Sram ==\n");
            if(SetPm2Sram()!=0)
            {
                UBOOT_ERROR("SetPm2Sram Fail!!\n");
                return -1;
            }
            #endif
        }
        else
        {
            msAPI_SetSPIOffsetForMCU();
        }
    }
    #if (CONFIG_MSTAR_RT_PM_IN_SPI)
    MDrv_PM_STR_CheckFactoryPowerOnModePassword();
    #endif

    s = getenv ("str_crc");
    if(NULL != s)
        iStrCrc = (int) simple_strtoul (s, NULL, 10);
    msAPI_Power_SetStrConfig( iStrCrc );
    //msAPI_Power_SetStrConfig( (strcmp(getenv("str_crc"), "0")==0) ? FALSE : TRUE ) ;
    msAPI_Power_PowerDown_EXEC();
    UBOOT_TRACE("OK\n");
    return 0;
}

#if (ENABLE_MSTAR_NIKON==1)
extern BOOLEAN msAPI_KeyPad_Initialize(void);

static int SetPm2L1(void)
{
    U32 u32Addr=0;
    U32 u32AddrVA=0;
    U32 u32AddrNVA=0;
    char PMPath[CMD_BUF]="\0";
    UBOOT_TRACE("IN\n");
    //*(volatile U32*)(0xBF203DC4) = 0x0002;
    msAPI_KeyPad_Initialize();
    static PM_WakeCfg PmWakeCfg =
        {
            .bPmWakeEnableIR = TRUE,
            .bPmWakeEnableSAR = TRUE,
            .bPmWakeEnableGPIO0 = FALSE,
            .bPmWakeEnableGPIO1 = FALSE,
            .bPmWakeEnableUART1 = FALSE,
            .bPmWakeEnableSYNC = FALSE,
            .bPmWakeEnableESYNC = FALSE,

            .bPmWakeEnableRTC0 = TRUE,
            .bPmWakeEnableRTC1 = TRUE,
            .bPmWakeEnableDVI0 = FALSE,
            .bPmWakeEnableDVI2 = FALSE,
            .bPmWakeEnableCEC = FALSE,
            .bPmWakeEnableAVLINK = FALSE,

            .u8PmWakeIR =
            {   //IR wake-up key define
                IRKEY_POWER, 0xFF, 0xFF, 0xFF,
                0xFF, 0xFF, 0xFF, 0xFF,
                0xFF, 0xFF, 0xFF, 0xFF,
                0xFF, 0xFF, 0xFF, 0xFF,
                0xFF, 0xFF, 0xFF, 0xFF,
                0xFF, 0xFF, 0xFF, 0xFF,
                0xFF, 0xFF, 0xFF, 0xFF,
                0xFF, 0xFF, 0xFF, 0xFF
            },

            .u8PmWakeIR2 =
            {   //IR wake-up key define
                IRKEY_POWER, 0xFF, 0xFF, 0xFF,
                0xFF, 0xFF, 0xFF, 0xFF,
                0xFF, 0xFF, 0xFF, 0xFF,
                0xFF, 0xFF, 0xFF, 0xFF
            }
        };
    char* s = getenv(POWER_KEY_NAME);
    if (s)
    {
        PmWakeCfg.u8PmWakeIR[0] = simple_strtoul(s, NULL, 16);
    }

    flush_cache((MS_U32)&PmWakeCfg, sizeof(PM_WakeCfg));
    if(MDrv_PM_Init((PM_WakeCfg*)VA2PA((MS_U32) &PmWakeCfg))==0)
    {
        UBOOT_ERROR("MDrv_PM_Init fail !!!\n");
    }

    if(get_addr_from_mmap("E_MMAP_ID_PM51_CODE_MEM", &u32Addr)!=0)
    {
        UBOOT_ERROR("get E_MMAP_ID_PM51_CODE_MEM_ADR mmap fail !!!\n");
        return -1;
    }

    if(u32Addr==0xFFFF)
    {
        UBOOT_ERROR("Get Mmap for PM51 Failed Skip DC PM !!! \n");
        return -1;
    }
    if(vfs_mount(CONFIG)!= 0)
        {
            UBOOT_ERROR("vfs_mount fail\n");
            return -1;
        }
    snprintf(PMPath,sizeof(PMPath),"%s/PM.bin",CONFIG_PATH);
    u32AddrVA=PA2VA(u32Addr);
    u32AddrNVA=PA2NVA(u32Addr);

    if(vfs_read((void*)u32AddrVA,PMPath,0,0x10000) != 0)
    {
        UBOOT_ERROR("vfs_read fail !!! \n");
        return -1;
    }
    udelay(50);


    UBOOT_DEBUG("Load PM To DRAM PA[0x%x]VA[0x%x]NVA[0x%x] \n",u32Addr,u32AddrVA,u32AddrNVA);

    //MsOS_DisableAllInterrupts();
    flush_cache(u32AddrVA,0x10000);
    Chip_Flush_Memory();

    enable_cache(0);

    ric_fill_icache((void *)(u32AddrNVA), 0x8000);
    ric_fill_dcache((void *)(u32AddrNVA+0x8000), 0x8000);
    enable_cache(1);

    printf("[%s][%s] Start PM at :0x%x \n\n",__FILE__,__FUNCTION__, (u32AddrVA|0x400));
    console_disable();
    udelay(50);

    asm volatile (
            "move $9, %[i0]\n\t" \
            "j $9\n\t" \
            "nop\n\t" \
            : \
            : [i0] "r"(u32AddrVA|0x400) \
            : "memory" \
    );
    while(1);

    UBOOT_TRACE("OK\n");
    return 0;
}

#else

#if defined(LG_CHG)
static U8 _pm_dat[] =
{
    #include "pmlite.dat"
};
#endif

extern void Chip_Flush_Memory(void);

void SetPM51Disable(void)
{
    MDrv_PM_Disable51();
    *(volatile U32*)(MS_RIU_MAP+(0x0e68<<1)) = *(volatile U32*)(MS_RIU_MAP+(0x0e68<<1)) & 0xFFFF00FF;
    //value = (U8)((*(volatile U32*)(MS_RIU_MAP+(0x0e68<<1)))>>8);
}

int SetPm2SPI(char u8Bank)
{
    int pc = get_pm51_program_counter();
    int ret = TRUE;

    if( pc == 0x00 ){
        MDrv_PM_SetSPIOffsetForMCU(u8Bank);
        ret = check_pm_alive();

        if(ret)
            printf("%s PC=%x, new PC=%x, PM51 booting!\n", __func__, pc, get_pm51_program_counter());
        else
            printf("%s PC=%x, new PC=%x, TIMEOUTS!!!\n", __func__, pc, get_pm51_program_counter());
    }
    //else
    //    printf("%s PM51 already boot, PC=0x%x\n", __func__, get_pm51_program_counter());

    return ret;
}

int SetPm2Sram(void)
{

#define PM_SIZE 0x5FFF

    U32 u32Addr=0;
    char PMPath[CMD_BUF]="\0";
    UBOOT_TRACE("IN\n");

#if defined(LG_CHG)
    u32Addr = PM_SRAM_ADDRESS;

    memset(PA2NVA(u32Addr), 0, sizeof(_pm_dat));
    memcpy(PA2NVA(u32Addr), _pm_dat, sizeof(_pm_dat));

    Chip_Flush_Memory();
#else
    if(get_addr_from_mmap("E_MMAP_ID_PM51_USAGE_MEM", &u32Addr)!=0)
    {
        UBOOT_ERROR("get E_MMAP_ID_PM51_USAGE_MEM mmap fail\n");
    }

    if(u32Addr==0xFFFF)
    {
        UBOOT_ERROR("Get Mmap for PM51 Failed Skip DC PM !!! \n");
        return -1;
    }
    if(vfs_mount(CONFIG)!= 0)
    {
        UBOOT_ERROR("vfs_mount fail\n");
        return -1;
    }

    snprintf(PMPath,sizeof(PMPath),"%s/PM.bin",CONFIG_PATH);
    if(vfs_read((void*)PA2NVA(u32Addr),PMPath,0,PM_SIZE) != 0)
    {
        UBOOT_ERROR("vfs_read fail\n");
        return -1;
    }
#endif

    if(MDrv_BDMA_CopyHnd(u32Addr, 0x0, PM_SIZE, E_BDMA_SDRAM2SRAM1K_HK51, 0) != TRUE )
    {
        UBOOT_ERROR("MDrv_BDMA_CopyHnd fail\n");
        return -1;
    }
    MDrv_PM_SetSRAMOffsetForMCU();
    UBOOT_TRACE("OK\n");
    return 0;
}
#endif

BOOLEAN get_poweroff_flag(void)///ac power off also use this flag -> dc_poweroff
{
    BOOLEAN ret = FALSE;
    UBOOT_TRACE("IN\n");

#if (ENABLE_ENV_IN_NAND == 1)
    if(vfs_mount(CUSTOMER)!=-1)
    {
        char PathBuf[64] = "\0";
        snprintf(PathBuf, sizeof(PathBuf), "%s/dc_poweroff", CUSTOMER_PATH);
        if(vfs_getsize(PathBuf) > 0)
        {
            UBOOT_DEBUG("dc_poweroff is Ture\n");;
            ret = TRUE;
        }
        else
        {
            UBOOT_DEBUG("dc_poweroff is False\n");;
            ret = FALSE;
        }
    }
#else
    if(strcmp(getenv("dc_poweroff"), "1") == 0)
    {
        ret = TRUE;
    }
#endif

    UBOOT_TRACE("OK\n");
    return ret;
}

#if (CONFIG_WDT_RESET_BY_ESD)
static void set_poweroff_flag(BOOLEAN bEnable)///ac power off also use this flag -> dc_poweroff
{
    UBOOT_TRACE("IN\n");

#if (ENABLE_ENV_IN_NAND == 1)
    ////Do nothing because vfs_write is not implement now
#else
    BOOLEAN bOrgValue = FALSE;
    if (strcmp(getenv("dc_poweroff"), "1") == 0)
    {
        bOrgValue = TRUE;
    }

    if (bEnable != bOrgValue)
    {
        if(bEnable)
        {
            setenv("dc_poweroff", "1");
        }
        else
        {
            setenv("dc_poweroff", "0");
        }
        saveenv();
    }
#endif
    UBOOT_TRACE("OK\n");
}
#endif

static BOOLEAN check_pm_standby(void)
{
    BOOLEAN ret = FALSE;
    UBOOT_TRACE("IN\n");

    if(strcmp(getenv("factory_poweron_mode"), "secondary") == 0) //Secondary mode, AC on will always enter standby mode.
    {
        #if (ENABLE_MSTAR_NUGGET==1)
        MApi_CEC_ConfigWakeUp();
        #endif
        ret = TRUE;
    }
    else if(strcmp(getenv("factory_poweron_mode"), "memory") == 0) //Memory, DC off -> AC off -> AC on -> Standby
    {
        #if (CONFIG_MSTAR_RT_PM_IN_SPI)
        if(MDrv_PM_STR_CheckFactoryPowerOnMode_Second(1)!= 1)
        {
            return FALSE;
        }
        #endif

        ret = get_poweroff_flag();
    }

    UBOOT_TRACE("OK\n");
    return ret;
}

static unsigned int get_pm51_program_counter(void)
{
    unsigned int u32ProgramCounter = 0;
    UBOOT_TRACE("IN\n");
    u32ProgramCounter = *(volatile U32*)(MS_RIU_MAP+(0x10fe<<1));
    u32ProgramCounter = (u32ProgramCounter&0xff)<<16;
    u32ProgramCounter |= *(volatile U32*)(MS_RIU_MAP+(0x10fc<<1));
    UBOOT_TRACE("OK\n");
    return u32ProgramCounter;
}

static BOOLEAN check_pm_alive(void)
{
    BOOLEAN ret = FALSE;
    int timeout = 200;

    do{
        if(*(volatile U32*)(MS_RIU_MAP+(0x0ea4<<1))){
            UBOOT_INFO("Bring-up PM51: done!\n");
            ret = TRUE;
            break;
        }
        udelay(5000); // 5ms
        timeout--;
    }while(timeout);
    //UBOOT_INFO("%s: remained timeouts %d!\n",__func__, timeout);
    return ret;
}

static int If_Boot_To_PM(void)
{
    EN_POWER_ON_MODE ePowerMode=EN_POWER_DC_BOOT;
    UBOOT_TRACE("IN\n");

#if (WDT_STANDBY_MODE==1)
    MDrv_WDT_Init(E_WDT_DBGLV_ALL);

    if(MDrv_WDT_IsReset() && get_poweroff_flag())
    {

        MDrv_WDT_ClearRstFlag();
        return 0;
    }
#endif
    ePowerMode=msAPI_Power_QueryPowerOnMode();

    if ( EN_POWER_AC_BOOT == ePowerMode)
    {
        if(getenv("factory_poweron_mode") == NULL)  //first boot on has no data
        {
            setenv("factory_poweron_mode", "direct");//default is direct mode (never standby)
            saveenv();
        }
        if(getenv("dc_poweroff") == NULL)
        {
            setenv("dc_poweroff", "0");
            saveenv();
        }

#if (CONFIG_WDT_RESET_BY_ESD)
        MS_BOOL bWDTResetToPowerOff = FALSE;
    #if (WDT_STANDBY_MODE == 0)
        MDrv_WDT_Init(E_WDT_DBGLV_ALL);
    #endif
        if(MDrv_WDT_IsReset())      // when we running ESD test, system will reset by wdt
        {
            UBOOT_DEBUG("System is reset by watchdog !!!\n");
            if(!get_poweroff_flag())
            {
                UBOOT_DEBUG("The last status is power on, system will not enter in standby mode\n");
                return 0;
            }
            else
            {
                UBOOT_DEBUG("The last status is power down, system will enter in standby mode\n");
                bWDTResetToPowerOff = TRUE;
            }
        }
#endif

#if (CONFIG_WDT_RESET_BY_ESD)
        if(bWDTResetToPowerOff || check_pm_standby())
#else
        if(check_pm_standby())
#endif
        {
#if (CONFIG_WDT_RESET_BY_ESD)
            if (!bWDTResetToPowerOff)
            {
                set_poweroff_flag(TRUE);
            }
#endif
            //PM51_PowerDown();
            run_command("pm51 standby",0);
        }
    }

    UBOOT_TRACE("OK\n");
    return 0;
}

int do_if_boot_to_pm( cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[])
{
    If_Boot_To_PM();
    return 0;
}

int do_Pm_Power_Test( cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[])
{
    int ret =0;
    if(getenv("factory_poweron_mode") != NULL)
    {
        if(strcmp(getenv("factory_poweron_mode"), "power_test") == 0) //Secondary mode, AC on will always enter standby mode.
       {
            ret = run_command("pm51 standby",0);
       }
    }
    return ret;
}

int do_pm51( cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[])
{
    int ret =0;
    volatile unsigned int *readyp;
    char * u8Command = NULL;
    char pattern1[16]={0xff,0x00,0xaa,0x55,0xa5,0x5a,0xf0,0x0f,0x0f,0xf0,0x5a,0xa5,0x55,0xaa,0x00,0xff};
    char pattern2[16]={0x0f,0xf0,0x5a,0xa5,0x55,0xaa,0x00,0xff,0xff,0x00,0xaa,0x55,0xa5,0x5a,0xf0,0x0f};
    char tpattern1[16];
    char tpattern2[16];
    UBOOT_TRACE("IN\n");

    if (argc < 2 || argv[1]==NULL)
    {
        cmd_usage(cmdtp);
        return 0;
    }

    u8Command = argv[1];

    if(strncmp(u8Command, "stop", 4) == 0)
    {
        UBOOT_INFO("Stop PM51!\n");
        pm_stop();
    }
    else if(strncmp(u8Command, "start0", 6) == 0)
    {
        UBOOT_INFO("Start PM51:");
        pm_start(0);
        UBOOT_INFO("Done!\n");
    }
     else if(strncmp(u8Command, "start1", 6) == 0)
    {
        UBOOT_INFO("Start PM51:");
        pm_start(1);
        UBOOT_INFO("Done!\n");
    }
    else if(strncmp(u8Command, "start2", 6) == 0)
    {
        UBOOT_INFO("Start PM51:");
        pm_start(2);
        UBOOT_INFO("Done!\n");
    }
    else if(strncmp(u8Command, "spistart", 8) == 0)
    {
        UBOOT_INFO("SPI PM51!\n");
        SetPm2SPI(0);
    }
    else if(strncmp(u8Command, "readpc", 6) == 0)
    {
        UBOOT_INFO("PM51[PC]=0x%x\n",get_pm51_program_counter());
    }
    else if(strncmp(u8Command, "standby", 7) == 0)
    {
         ret = PM51_PowerDown();
    }
    else if (strncmp(u8Command, "pminfo_clear", 10)==0)
    {
        UBOOT_INFO("check pm info\n");
        PM_INFO_T pm_info;
        memset(&pm_info,0xff,sizeof(PM_INFO_T));
        pm_write_info(&pm_info);
    }
    else if (strncmp(u8Command, "pminfo", 6)==0)
    {
        UBOOT_INFO("check pm info\n");
        PM_INFO_T pm_info;
        memset(&pm_info,0x00,sizeof(PM_INFO_T));
        pm_read_info(&pm_info);
        UBOOT_INFO("current bank num is %d,\n",pm_info.cur_bank_num);
        UBOOT_INFO("=========================\n");
        UBOOT_INFO("0 bank's version is %c%c%c%c\n\n",pm_info.bank[0].version[0],pm_info.bank[0].version[1],pm_info.bank[0].version[2],pm_info.bank[0].version[3]);
        UBOOT_INFO("0 bank's valid mark is %x\n",pm_info.bank[0].valid);
        UBOOT_INFO("=========================\n");
        UBOOT_INFO("1 bank's version is %c%c%c%c\n\n",pm_info.bank[1].version[0],pm_info.bank[1].version[1],pm_info.bank[1].version[2],pm_info.bank[1].version[3]);
        UBOOT_INFO("1 bank's valid mark is %x\n",pm_info.bank[1].valid);
        UBOOT_INFO("=========================\n");
        UBOOT_INFO("2 bank's version is %c%c%c%c\n\n",pm_info.bank[2].version[0],pm_info.bank[2].version[1],pm_info.bank[2].version[2],pm_info.bank[2].version[3]);
        UBOOT_INFO("2 bank's valid mark is %x\n",pm_info.bank[2].valid);
    }
    else if (!strncmp(u8Command, "ready", 5))
    {
        UBOOT_INFO("Check PM Ready\n");
        readyp = (MS_RIU_MAP+(0x0ea4<<1));
        printf("[%08x]:%08x\n", readyp, *readyp);
    }
    else if (!strncmp(u8Command, "clear", 5))
    {
        UBOOT_INFO("Clear PM Ready\n");
        readyp = (MS_RIU_MAP+(0x0ea4<<1));
        *readyp = 0;
    }
    else if(!strncmp(u8Command, "merong1",7))
    {		
        int i;

        pm_start(0);

        while(1)
        {
            DDI_NVM_Read(0x1a90, 16, tpattern1);
            DDI_NVM_Read(0x1aa0, 16, tpattern2);
            if(strncmp(pattern1,tpattern1,16)) {
                printf("pattern1 error : ",0);
                for(i=0;i<16;i++)  printf("0x%x,",tpattern1[i]);
            }
            if(strncmp(pattern2,tpattern2,16)) {
                printf("pattern2 error : ",0);
                for(i=0;i<16;i++)  printf("0x%x,",tpattern2[i]);
            }
        }
    }
    else if(!strncmp(u8Command, "merong2",7))
    {		
        DDI_NVM_Write(0x1a90, 16, (U8*)pattern1);
        DDI_NVM_Write(0x1aa0, 16, (U8*)pattern2);
    }

    UBOOT_TRACE("OK\n");
    return ret;
}

int pm_read_info(PM_INFO_T *pm_infop)
{
    size_t len = sizeof(PM_INFO_T);
    size_t start = PM_SERIAL_SIZE - PM_SERIAL_ERASE_SIZE;
    if(spi_ReadPMAddress((unsigned char *)pm_infop, len ,start) != 0)
    {
        printf("spi_ReadPMAddress fail!!\n");
        return -1;
    }

    return 0;
}

extern unsigned char MDrv_SERFLASH_WriteProtect(unsigned char bEnable);
extern unsigned char MDrv_SERFLASH_SectorErase(unsigned int u32StartAddr, unsigned int u32EndAddr);
extern unsigned char MDrv_SERFLASH_Write(unsigned int u32FlashAddr, unsigned int u32FlashSize, unsigned char *user_buffer);
extern unsigned char MDrv_SERFLASH_Read(unsigned int u32FlashAddr, unsigned int u32FlashSize, unsigned char *user_buffer);

int pm_write_info(const PM_INFO_T *pm_infop)
{
    PM_INFO_T tpm_info;
    size_t len = sizeof(PM_INFO_T);
    size_t start = PM_SERIAL_SIZE - PM_SERIAL_ERASE_SIZE;

    Chip_Flush_Memory();

    CMNIO_GPIO_SetOutputPort(GPIO_PORT_WR_FLASH_WP, TRUE);

    MDrv_SERFLASH_WriteProtect(FALSE);
    MDrv_SERFLASH_SectorErase(start, start + PM_SERIAL_ERASE_SIZE-1);
    MDrv_SERFLASH_Write(start, len, (unsigned char *)pm_infop);
    MDrv_SERFLASH_WriteProtect(TRUE);

    CMNIO_GPIO_SetOutputPort(GPIO_PORT_WR_FLASH_WP, FALSE);

    if (!MDrv_SERFLASH_Read(start, len, (unsigned char *)&tpm_info)) {
        printf("%s fail at line:%d\n", __func__, __LINE__);
        return -1;
    }

    if (memcmp (pm_infop, &tpm_info, len)) {
        printf("%s fail at line:%d\n", __func__, __LINE__);
        return -1;
    }

    printf("%s success\n", __func__);
    return 0;
}


int pm_check_update(void)
{
    unsigned char pm_update =  DDI_NVM_GetPMUpdate();

    if(pm_update == PM_UPDATE)
        return 1;
    return 0;
}

int pm_clear_update(void)
{
    DDI_NVM_SetPMUpdate(0);
}

int pm_read_emmc(unsigned char *buf)
{
    int ret;
    struct partition_info *pm_part_emmc;

    pm_part_emmc = get_partition_by_name(IMICOM_PART_NAME);

    if (!pm_part_emmc || !buf) {
        printf("%s fail\n", __func__);
        return -1;
    }

    ret = emmc_read (pm_part_emmc->offset, PM_EMMC_SIZE, buf);

    if (!ret)
        printf("%s success\n", __func__);
    else
        printf("%s fail\n", __func__);

    return ret;
}

int pm_hash_read_emmc(unsigned char *buf)
{
    int ret;
    struct partition_info *pm_part_emmc;
	
    pm_part_emmc = get_partition_by_name(IMICOM_PART_NAME);

    if (!pm_part_emmc || !buf) {
        printf("%s fail\n", __func__);
        return -1;
    }

    ret = emmc_read (pm_part_emmc->offset+PM_BIN_SIZE+PM_VER_SIZE, PM_HASH_SIZE, buf);

    if (!ret)
        printf("%s success\n", __func__);
    else
        printf("%s fail\n", __func__);

	return ret;
}

int pm_read_spi(unsigned char *buf, unsigned int addr, size_t size)
{
    int retry = 0;

    do {

        if(spi_ReadPMAddress(buf, size ,addr) == 0)
            return 0;

        printf("spi_ReadPMAddress fail!!\n");
        retry++;
    } while (retry<3);
    return -1;
}

int pm_write_spi(unsigned char *buf, unsigned int addr, size_t size)
{
    int retry = 0;

    do{

        if(spi_WritePMAddress(buf, size, addr) == 0)
            return 0;

        printf("spi_ReadPMAddress fail!!\n");
        retry++;
    }while(retry<3);
    return -1;
}

int pm_check_ready(void)
{
    U8 value = 0;

    value = (U8)((*(volatile U32*)(MS_RIU_MAP+(0x0e68<<1)))>>8);
    //if(*(volatile U32*)(MS_RIU_MAP+(0x0ea4<<1)))
    if(value == 0x11)	
        return 1;

    return 0;
}

int pm_recover( PM_INFO_T *pm_infop)
{
    int cur_nbank = pm_infop->cur_bank_num;
    int alt_nbank;

    if(cur_nbank == 1)
        alt_nbank = 2;
    else if(cur_nbank == 2)
        alt_nbank = 1;
    else
        goto default_bank;

    if(pm_infop->bank[alt_nbank].valid == PM_INVALID)
        goto default_bank;

    pm_infop->cur_bank_num = alt_nbank;
    return alt_nbank;

default_bank:
    pm_infop->cur_bank_num = 0;
    pm_infop->bank[0].valid=PM_VALID;
    return 0;
}


int pm_mb_get_version(char version[PM_VER_SIZE])
{
    unsigned char	cmd = 0;
    unsigned char	data[7];
    int loop_cnt	=0;

    cmd = CP_READ_MICOM_VERSION;
    while(1)
    {
        if(DDI_CMNIO_MBX_Read(cmd, data, 7) == 0)
        {
            printf("get version success!!\n");
            printf("%c",data[0]);
            printf("%c",data[1]);
            printf("%c",data[2]);
            printf("%c",data[3]);
            printf("%c",data[4]);
            printf("%c",data[5]);
            printf("%c",data[6]);
            printf("%c",data[7]);
            break;
        }
        mdelay(100);
        loop_cnt++;
        printf("mbx_read fail!!\n");

        if(loop_cnt>9)
            return -1;
    }

    version[0] = data[1];
    version[1] = data[3];
    version[2] = data[4];
    version[3] = data[6];
    printf("version = %c%c%c%c\n",version[0],version[1],version[2],version[3]);

    return 0;
}

int pm_is_alive(void)
{
    int pc = get_pm51_program_counter();

    if( pc == 0x00 )
        return 0;

    return 1;
}
void pm_stop(void)
{
    SetPM51Disable();
}

void pm_start (char bank)
{
    MDrv_PM_SetSPIOffsetForMCU(bank);
}

void pm_run(PM_INFO_T *pm_infop)
{
    PM_INFO_T tpm_info;
    int ret = 0;

    if (!pm_infop) {
        pm_infop = &tpm_info;
        ret = pm_read_info(pm_infop);
    }

    if(ret || pm_infop->magic != PM_INFO_MAGIC || pm_infop->cur_bank_num >= PM_BANK_MAX || pm_infop->cur_bank_num < 0 )
    {
        printf("PM's bank or magic value is invalid!!\n");
        pm_infop->magic = PM_INFO_MAGIC;
        pm_infop->cur_bank_num = 0;
        pm_infop->bank[0].valid=PM_VALID;
        pm_write_info (pm_infop);
    }

    printf ("pm_run bank %d\n", pm_infop->cur_bank_num);
    pm_start(pm_infop->cur_bank_num);
}

int pm_wait_ready(unsigned int timeout)
{
    int pm_wait_ms  = 0;
    char pm_version[PM_VER_SIZE];
    int retry_cnt=5;

    printf("wait pm ready!!\n");
    /* if timeout is zero, wait infinitively until pm is ready */
    while(pm_wait_ms < timeout || timeout == 0)
    {
        if(pm_check_ready())
        {
            return PM_RUN_SUCCESS;
        }
        mdelay(10);
        pm_wait_ms+=10;
    }
    printf("pm_wait_ready time out\n");
    return PM_RUN_TIMEOUT;
}

int pm_verify_run(PM_INFO_T *pm_infop)
{
	int ret;
	char pm_version[PM_VER_SIZE];

	if (!pm_infop)
		return PM_RUN_FAIL;

	pm_run(pm_infop);

	ret = pm_wait_ready(PM_READY_TIMEOUT_MS);

	if (ret == PM_RUN_SUCCESS) {
		if (pm_mb_get_version(pm_version))
			return PM_RUN_FAIL;
		UBOOT_DUMP((U32)pm_version,4);
		UBOOT_DUMP((U32)pm_infop->bank[pm_infop->cur_bank_num].version,4);
		if (strncmp(pm_version, pm_infop->bank[pm_infop->cur_bank_num].version, PM_VER_SIZE))
			return PM_RUN_FAIL;

		return PM_RUN_SUCCESS;
	}

	return ret;
}

static void pm_hash_inverse(U8 *dst, U8 *src, U32 len)
{
    int i=0;
    for(i=0;i<len;i++)
    {
        *(dst+i)=*(src+len-1-i);
    }
}

int pm_sha_verify(unsigned char *buf, unsigned char *hash_buf)
{
#if defined (CONFIG_SECURITY_BOOT)
    unsigned char hash_out[32+PM_SHA_ALIGN] = {0,};
    unsigned char hash_out_inv[32] = {0,};
    unsigned char* hash_out_align;
	
    hash_out_align = (unsigned char *)(((unsigned int)hash_out + PM_SHA_ALIGN - 1) & ~(PM_SHA_ALIGN -1));
    UBOOT_DUMP((U32)hash_buf,32);

    Chip_Flush_Memory();

    if(CommonSHA((unsigned int)buf, (unsigned int)hash_out_align, PM_BIN_SIZE+PM_VER_SIZE))
    {
        printf("failed making sha hash data\n");
        return -1;
    }
    UBOOT_DUMP((U32)hash_out_align,32);
	
    pm_hash_inverse(hash_out_inv,hash_out_align,PM_HASH_SIZE);
    UBOOT_DUMP((U32)hash_out_inv,32);
    UBOOT_DUMP((U32)hash_buf,32);
    if(memcmp(hash_out_inv,hash_buf,PM_HASH_SIZE)!=0)
    {
        printf("pm_sha_verify fail!!\n");
        return -1;
    }
    printf("pm_sha_verify success!!\n");
    //success
#endif
    return 0;
}

unsigned char pm_buf[PM_EMMC_SIZE];

void pm_update(void)
{
    PM_INFO_T pm_info;
    PM_INFO_T pm_info_backup;
    FW_INFO_T *pbank;
    FW_INFO_T *pbank_alt;
    int  ret = 0;
    int  alt_nbank;
	
    unsigned char *pm_buf_align;
    unsigned char *pm_version;
    unsigned char *pm_bin;
    unsigned char *pm_hashp;
    unsigned char pm_hash[32];


    if( pm_is_alive() ) {
        printf("pm is alive, stop pm\n");
        pm_stop();
    }

    ret = pm_read_info(&pm_info);
    printf("pm_info.bank = %d\n",pm_info.cur_bank_num);
    printf("pm_info.magic = %x\n",pm_info.magic);
    if(ret || pm_info.magic != PM_INFO_MAGIC || pm_info.cur_bank_num>=PM_BANK_MAX || pm_info.cur_bank_num < 0)
    {
        printf("pm magic is not correct, initialize pm info and choose bank 0\n");

        pm_info.magic = PM_INFO_MAGIC;
        pm_info.cur_bank_num = 0;
        pm_info.bank[0].valid=PM_VALID;
        pm_write_info(&pm_info);
        return pm_run(&pm_info);
    }

    memcpy(&pm_info_backup, &pm_info, sizeof(pm_info));

    memset (pm_buf, 0xff, sizeof(pm_buf));
    pm_buf_align = (unsigned char *)(((unsigned int)pm_buf + PM_SHA_ALIGN - 1) & ~(PM_SHA_ALIGN -1));
    if(pm_read_emmc(pm_buf_align) == -1)
        goto clear_update_and_run;
	
    pm_version = pm_buf_align;
    pm_bin = pm_buf_align + PM_VER_SIZE;
    pm_hashp = pm_bin + PM_BIN_SIZE;

    // when use comsha func, need 16Byte to use SACTTERGATHER_TABLE_SIZE.
    // so, we move pm_hashp to pm_hash and we use pm_hashp for SACTTERGATHER_TABLE_SIZE.
    memcpy(pm_hash, pm_hashp, PM_HASH_SIZE);	
    if( pm_sha_verify(pm_buf_align, pm_hash) == -1)
        goto clear_update_and_run;
	
    pbank = &pm_info.bank[pm_info.cur_bank_num];
    if(!strncmp(pbank->version,pm_version,PM_VER_SIZE))
    {
        if(pbank->valid == PM_VALID)
        {
            printf("pm is already valid\n");
            goto clear_update_and_run;
        }
    }

    if(pm_info.cur_bank_num!=1)
        alt_nbank = 1;
    else
        alt_nbank = 2;

    if(pm_write_spi(pm_bin, PM_BIN_SIZE*alt_nbank, PM_BIN_SIZE) == -1)
    {
        printf("pm_write_spi fail\n");
        goto clear_update_and_run;
    }

    pbank_alt=&pm_info.bank[alt_nbank];

    pbank_alt->valid = PM_INVALID;
    strncpy(pbank_alt->version,pm_version,PM_VER_SIZE);
    pm_info.cur_bank_num = alt_nbank;

    if (PM_RUN_SUCCESS == pm_verify_run(&pm_info))
        pbank_alt ->valid = PM_VALID;
    else {
        printf("Verify fail, then recover pm\n");
        memcpy(&pm_info, &pm_info_backup, sizeof(PM_INFO_T));
        printf("pm stop\n");
        pm_stop();
        goto clear_update_and_run;
    }

    printf("pm stop\n");
    pm_stop();

    printf("pm write info\n");
    pm_write_info(&pm_info);

clear_update_and_run:
    printf("clear pm update\n");
    pm_clear_update();
    printf("pm run\n");
    pm_run(&pm_info);
    pm_wait_ready(1000);
}

