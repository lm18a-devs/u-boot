#include <common.h>
#include <command.h>
#include <linux/types.h>
#include <linux/string.h>
#include <linux/ctype.h>
#include <exports.h>

// zzindda
#if 1//def CC_EMMC_BOOT
#include <mmc.h>
#endif

#include <xyzModem.h>
#include <net.h>
#include <partinfo.h>
#include <swu.h>
#include <cmd_resume.h>
#include <swum.h>
#ifdef CONFIG_MULTICORES_PLATFORM
#include <thread.h>
#endif
#include <CusCmnio.h>

#define	FLASH_BASE			0
#define SECUREBOOT_NAME		"secureboot"

#define SKIP_TYPE1_BOOT		// zzindda lm18a - skip to update boot, secureboot with type1 board
#ifdef SKIP_TYPE1_BOOT
#define GPIO_LM18A_BOARD_TYPE 57
typedef enum
{
	BOARD_TYPE1,
	BOARD_TYPE2,
	BOARD_TYPE_END
}	LM18A_BOARD_TYPE_T;
LM18A_BOARD_TYPE_T _get_boardtype_lm18a (void);
#endif

// zzindda
#if 0//def MTDINFO_IN_FLASH
extern int write_flash_data(uchar *src, ulong addr, ulong cnt);
#else
extern int storage_write(uint32_t offset, size_t len, const void* buf);
#endif

#undef SWU_DEBUG
#ifdef SWU_DEBUG
#define dprintf(fmt, args...)		printf("%s: " fmt, __FUNCTION__, ## args)
#else
#define dprintf(fmt, args...)
#endif	/* DEBUG */

extern ulong default_offset;
extern ulong default_offset_bin;
extern MODELOPT_T gModelOpt;

// zzindda
#if 0//def CONFIG_LOAD_SERIAL
extern ulong get_serial_data(ulong offset);
#endif
#ifdef CONFIG_LOAD_TFTP
extern int tftp_get (char *filename);
#endif
#if defined(CONFIG_LOAD_FAT32) || defined(CONFIG_LOAD_FAT32_PARTIAL)
extern int fat_fsload(char *filename);
#endif
extern u8 SWU_MICOM_BinUpdate(char* pImg_start, u32 size);
extern u8 SWU_ABOV_MICOM_BinUpdate(char* pImg_start, u32 size);
extern u8 SWU_SPI_Update(unsigned char* pImg_start, u32 size);

#define CRC32_POLY	0x04c11db7		/* AUTODIN II, Ethernet, & FDDI */

u32	crc32_table[256];
#ifdef CONFIG_MULTICORES_PLATFORM
static swue_buf_t _swue_buf;
static swue_buf_t *swue_buf = &_swue_buf;
#endif

static void init_crc32(void)
{
	int	i, j;
	u32	c;

	for (i = 0; i < 256; ++i) {
		for (c = i << 24, j = 8; j > 0; --j)
			c = c & 0x80000000 ? (c << 1) ^ CRC32_POLY : (c << 1);
		crc32_table[i] = c;
	}
}

u32 calc_crc32(unsigned char *buf, u32 len)
{
	unsigned char	*p;
	u32				crc;
	static	u32		fTableOk=0;

	if (fTableOk == 0)
	{
		init_crc32();   	/* build table */
		fTableOk = 1;
	}

	crc = 0xffffffff;       /* preload shift register, per CRC-32 spec */

	for (p = buf; len > 0; ++p, --len)
		crc = (crc << 8) ^ crc32_table[(crc >> 24) ^ *p];

	return(crc);
}

#ifdef CONFIG_MULTICORES_PLATFORM
void swue_init_buffer(void *buf, size_t size)
{
	memset(swue_buf, 0, sizeof(swue_buf_t));
	swue_buf->data_buf = (u8*)buf;
	swue_buf->size = size;
	swue_buf->copy_buf = (u8*)((u32)buf+(u32)size);
	swue_buf->status = SWUE_BUF_STATUS_NORMAL;
}

static void swue_close_buffer(void)
{
	swue_buf->status = SWUE_BUF_STATUS_DONE;
}

/*
  * buf:	tftp buffer address
  * size:	copy data size from tftp buffer to swue buffer
*/
int swue_push_data(const void *buf, int size)
{
	int cnt = 0;

	do {
		if(swue_buf->status != SWUE_BUF_STATUS_NORMAL)
			return -1;

		if(swue_buf_receivable(swue_buf, size)) {
			if(swue_buf->rear >= swue_buf->front) {
				int wsize = min(swue_buf->size - swue_buf->rear, size);
				memcpy(swue_buf->data_buf + swue_buf->rear, buf, wsize);

				swue_buf->rear = (swue_buf->rear + wsize) % swue_buf->size;
				swue_buf->receive_size += wsize;
				if(size == wsize) return 0; // done

				size -= wsize;
				buf += wsize;
			}
			memcpy(swue_buf->data_buf + swue_buf->rear, buf, size);
			swue_buf->rear += size;
			swue_buf->receive_size += size;
			return 0;
		}

		if(cnt > SWUE_TIME_DELAY) {
			printf("[%s] timeout !!!\n", __FUNCTION__);
			return -1;
		}
		mdelay(1);
		cnt++;
	} while(1);
}

/*
  * buf:	write buffer address to copy data from swue buffer
  * size:	copy data size
*/
static int swue_get_data(void *buf, int size, int timeout, uint32_t flag)
{
	int cnt = 0;

	if(!size)
		return 0;

	do {
		/* if timeout is 0 then returned avaiable data */;
		if(swue_buf_writable(swue_buf, size) || timeout == 0) {
			int tsize = 0;	// written size to buf
			int wsize = 0;	// writable size from swue buffer
			int front = swue_buf->front;
			int rear = swue_buf->rear;	/* save last rear position */

			if(rear < front) {
				wsize = min(swue_buf->size - front, size);
				memcpy(buf, swue_buf->data_buf + front, wsize);
				front = (front + wsize) % swue_buf->size;

				if(!(flag&SWUE_FLAG_DONOT_UPDATE_INFO)) {
					swue_buf->front = front;
					swue_buf->write_size += wsize;
				}
				if(size == wsize) return wsize;

				size -= wsize;
				tsize += wsize;
			}
			wsize = min(rear - front, size);
			memcpy(buf + tsize, swue_buf->data_buf + front, wsize);
			tsize += wsize;
			if(!(flag&SWUE_FLAG_DONOT_UPDATE_INFO)) {
				swue_buf->front += wsize;
				swue_buf->write_size += wsize;
			}

			return tsize;
		}

		if(swue_buf->status == SWUE_BUF_STATUS_DONE) {
			if(swue_buf->rear + 1 != swue_buf->front) {
				int tsize = 0;	// written size to buf
				int wsize = 0;	// writable size from swue buffer
				int front = swue_buf->front;
				int rear = swue_buf->rear;	/* save last rear position */

				if(rear < front) {
					wsize = min(swue_buf->size - front, size);
					memcpy(buf, swue_buf->data_buf + front, wsize);
					front = (front + wsize) % swue_buf->size;
					if(!(flag&SWUE_FLAG_DONOT_UPDATE_INFO)) {
						swue_buf->front = front;
						swue_buf->write_size += wsize;
					}
					if(size == wsize) return wsize;

					size -= wsize;
					tsize += wsize;
				}
				wsize = min(rear - front, size);
				memcpy(buf + tsize, swue_buf->data_buf + front, wsize);
				tsize += wsize;
				if(!(flag&SWUE_FLAG_DONOT_UPDATE_INFO)) {
					swue_buf->front += wsize;
					swue_buf->write_size += wsize;
				}
				return tsize;
			}
			else {
				printf("Download completed but no data left\n");
				return 0;
			}
		}

		cnt++;
		if(timeout >= 0 && cnt > timeout) {
			printf("[%s] timeout !!!\n", __FUNCTION__);
			return -1;
		}
		mdelay(10);
	} while(1);

}

/* Can't support rewind, only support forward seeking */
static int swue_seek_data(u32 offset, int timeout)
{
	int size = 0;
	int cnt = 0;

	if(offset < swue_buf->write_size) {
		printf("Can't support rewind. request:%d, current:%d\n", offset, swue_buf->write_size);
		return -1;
	}

	if(offset == swue_buf->write_size) return 0;

	cnt = 0;
	size = offset - swue_buf->write_size;
	do {
		if(swue_buf_writable(swue_buf, size)) {
			int rsize;
			int rear = swue_buf->rear;	/* save last rear position */
			if(rear < swue_buf->front) {
				rsize = min(swue_buf->size - swue_buf->front, size);
				swue_buf->front = (swue_buf->front + rsize) % swue_buf->size;
				swue_buf->write_size += rsize;
				if(size == rsize) return rsize;
				size -= rsize;
			}
			rsize = min(rear - swue_buf->front, size);
			swue_buf->front += rsize;
			swue_buf->write_size += rsize;
			return 0;
		}

		cnt++;
		if(timeout >= 0 && cnt > timeout) {
			printf("[%s] timeout !!!\n", __FUNCTION__);
			return -1;
		}
		mdelay(1);
	} while(1);
}
#endif

static int check_storage_capacity(struct partmap_info * pmi)
{
// zzindda
#if 1 //defined(CC_EMMC_BOOT)
	extern struct mmc emmc_info[];
	printf("dev: %llx vs part.dev: %llx\n", emmc_info[0].capacity, pmi->dev.size);
	return (int)((u32)emmc_info[0].capacity - pmi->dev.size);
#else
	return 0;
#endif
}

static int check_capacity(struct partmap_info * pmi)
{
	printf("dev: %llx vs %llx\n", GET_DEVICE_INFO()->size, pmi->dev.size);
	if(GET_DEVICE_INFO()->size != pmi->dev.size) {
		return 1;
	}
	return 0;
}

static int check_idkey_change(struct partition_info * pi, struct partition_info *npi)
{
	if( (npi->offset != pi->offset) || (npi->size != pi->size) )
		return 1;
	else
		return 0;
}

#if 1
static int backup_idkey(struct partmap_info * pmi)
{
	struct partition_info *pi, *npi;
	int i, j;

	for(i = (GET_PART_NUM()); i > 0; i--) {
		pi = GET_PART_INFO(i);

		if(pi->mask_flags & PART_FLG_IDKEY || !strncmp(pi->name, "nvram", 4)) {
			printf("%s ] part_name = %s\n", __func__, pi->name);
			for(j = (pmi->npartition); j > 0; j--) {
				npi = &(pmi->partition[j]);

				if( !strncmp(pi->name, npi->name, 4) && check_idkey_change(pi,npi))
				{
					printf("%s has been changed offset or size(0x%x:0x%x)\n", pi->name, (ulong)pi->offset, (ulong)npi->offset);
					backup_partition(pi, npi);
				}
			}
		}
	}
	return 0;
}

#else
static int backup_idkey(struct partmap_info * pmi)
{
	struct partition_info *pi, *npi;
	int i, j;

	for(i=(GET_PART_NUM()-2); i>0; i--) {
		pi = GET_PART_INFO(i);

		if(pi->mask_flags & PART_FLG_IDKEY || !strncmp(pi->name, "nvram", 4)) {
			printf("%s ] part_name = %s\n", __func__, pi->name);
			for(j=(pmi->npartition-2); j>0; j--) {
				npi = &(pmi->partition[j]);

				if(strncmp(pi->name, npi->name, 4) == 0)
					backup_partition(pi, npi);
			}
		}
	}
	return 0;
}
#endif

static void print_pak_info(PAK_HEADER_T * pak)
{
	char imgtype[PAK_TYPE_ID_LEN + 1];

	dprintf("pak = 0x%x\n", pak);

	// print pak header info
	get_imgtype(pak, imgtype);
	printf("\n[ " ANSI_COLOR_CYAN "%s" ANSI_COLOR_RESET" ]\n", imgtype);
	printf("\tPAK Model : %s\n", pak->modelName);
	printf("\timageSize = %u\n", pak->imageSize);
	printf("\tmodelName = %s\n", pak->modelName);
	printf("\tSWVersion = %u\n", pak->SWVersion);
	printf("\t Dev mode : %s\n", pak->DevMode ? "Debug Mode" : "Release Mode");
	printf("\tloadAddr = %u\n", pak->loadAddr);
	printf("\tmagic_number = %u\n\n", pak->magic_number);
}

static int check_crc(PAK_HEADER_T * pak, u32 offset, u32 size)
{
	u32 crc = 0, calcrc = 0;

	//get the crc value in end of pak
	memcpy((void*)&crc, (void*)(offset+size), 4);

	//temp
	printf("test size:0x%x offset:0x%x offset+size:0x%p \n", size, offset, ((u32 *)(offset+size)));

	//check the crc value
	if((calcrc = calc_crc32((unsigned char *)offset, size)) != crc) {
		printf("\t" ANSI_COLOR_RED "CRC check failed [%x/%x]" ANSI_COLOR_RESET "\n", calcrc, crc);
		return -1;
	}

	printf("\t CRC check [calcrc:%x/crc:%x]\n", calcrc, crc);
	return 0;
}

static int check_partinfo(struct partition_info* mpi, u32 offset)
{
	struct partition_info *mtdmpi = NULL;
	struct partmap_info *pmi = NULL;
	u32 idx = 0;
	int retVal = 1;

	if(!strcmp(mpi->name, "partinfo")) {
		pmi = (struct partmap_info *)(offset);

		/* check storage capacity */
#if 0	//no  need because of run-time emmc size branch
		if(check_storage_capacity(pmi) < 0)
		{	/* pmi->dev.size > emmc->chip_size */
			char in_char = 'N';
			printf("warning!!, plz check storeage capacity(%x)\n", pmi->dev.size);
			printf("Do you want download ? <y/N> ");
			in_char = (char)getc();
			printf("%c\n\n", isprint(in_char) ? in_char : ' ');

			if(in_char != 'y' && in_char != 'Y' )
				return -1;
		}

		/* check partmap  capacity */
		if(check_capacity(pmi))
		{
			mpi->sw_ver = 0;
			backup_idkey(pmi);
		}


#endif
		do {
			mtdmpi = &(pmi->partition[idx]);
			if(mtdmpi->used && (strncmp(mpi->name, mtdmpi->name, 4) == 0))
				break;
		}while(++idx < PARTITION_MAX);

		if (mpi->sw_ver == mtdmpi->sw_ver) {
			printf("partinfo version is same case!! 0x%x -> 0x%x\n", mpi->sw_ver, mtdmpi->sw_ver);
			retVal = 0;
		}
		else
		{
			printf(ANSI_COLOR_GREEN "partinfo version is different case!! 0x%x -> 0x%x" ANSI_COLOR_RESET "\n", mpi->sw_ver, mtdmpi->sw_ver);
			backup_idkey(pmi);
			erase_datapart(pmi);
			retVal = 1;
		}
	}
	return retVal;
}

static void update_partinfo(PAK_HEADER_T *pak, struct partition_info* unmpi, struct partition_info* mpi, u32 size)
{
	u32 swver = 0;

	/* to do version check */
	printf("\t SW Version : %x\n", pak->SWVersion);
	printf("\t SW Date : %x\n", pak->SWDate);
	swver = pak->SWVersion;

	/* adjust mtdinfo with new info */
	if(unmpi) {
		unmpi->filesize = size;
		unmpi->sw_ver 	= swver;
		unmpi->used 	= YES;
		unmpi->valid	= PART_VALID_FLG_VALID;
		mpi->used 		= NO;

		swap_partition(mpi, unmpi);

	} else {
		mpi->filesize	= size;
		mpi->sw_ver		= swver;
	}


	/* save mtdinfo */
	if (strcmp(mpi->name, "partinfo"))
		save_partinfo();
	else {	/* load mtdinfo */
		load_partinfo();
		erase_cachepart();
	}

}

static int storage_partition(PAK_HEADER_T *pak, struct partition_info* unmpi, struct partition_info* mpi, u32 offset, u32 size)
{
	char imgtype[PAK_TYPE_ID_LEN + 1];
	u32 floffset = 0, flsize = 0;
	int ret = 0;

	get_imgtype(pak, imgtype);

	if(unmpi) {
		floffset	= (ulong)(unmpi->offset + CFG_FLASH_BASE);
		flsize		= (ulong)(unmpi->size);
	} else {
		floffset	= (ulong)(mpi->offset + CFG_FLASH_BASE);
		flsize		= (ulong)(mpi->size);
	}

	if( (size > 0) && (size <= flsize) ) {

#if 0//def MTDINFO_IN_FLASH
		flash_protect(FLAG_PROTECT_CLEAR, floffset, floffset+size-1, &flash_info[0]);
		write_flash_data((uchar*)offset, floffset, size);
#else
		if (!strcmp(mpi->name, "partinfo"))
		{
			ret = storage_write(DEFAULT_MAPBAK__BASE, size+4, (const void *)offset);
			if(ret) {
				printf(ANSI_COLOR_RED "ERROR: [" ANSI_COLOR_CYAN "%s" ANSI_COLOR_RED " ] emmc write failed.." ANSI_COLOR_RESET "\n", imgtype);
				return ret;
			}

			ret= storage_write((uint32_t)floffset, (size_t)size+4, (const void *)offset);
			if(ret) {
				printf(ANSI_COLOR_RED "ERROR: [" ANSI_COLOR_CYAN "%s" ANSI_COLOR_RED " ] emmc write failed.." ANSI_COLOR_RESET "\n", imgtype);
				return ret;
			}
		}
		else if (!strcmp(mpi->name, "boot"))
		{
			printf("boot pak download!!\n");

			printf("sw ver = %x\n", pak->SWVersion);
			if(pak->SWVersion <= 1)
			{
				if(get_partition_idx(SECUREBOOT_NAME) < 0)
				{
					printf(ANSI_COLOR_RED "ERROR: [" ANSI_COLOR_CYAN "%s" ANSI_COLOR_RED " ] Fail to get %s partition idx" ANSI_COLOR_RESET "\n", imgtype, SECUREBOOT_NAME);
					ret= storage_write((uint32_t)floffset+0x200000, (size_t)size, (const void *)offset);
				}
				else
				{
					printf("Success to get %s partition idx\n", SECUREBOOT_NAME);
					ret= storage_write((uint32_t)floffset, (size_t)size, (const void *)offset);
				}
			}
			else
			{
				char *bootCmd="mmc write.boot 1 0x";
				char *offsetCmd[128];
				char bWriteCmd[128]={'\0',};

				sprintf(offsetCmd,"%x",offset);
				strcat(bWriteCmd, bootCmd);
				strcat(bWriteCmd, offsetCmd);
				strcat(bWriteCmd, " 0 0x28000");

				printf("bootcmd = %s\n",bWriteCmd);
				run_command(bWriteCmd,0);

				printf("finished write rom_boot\n");
				if(get_partition_idx(SECUREBOOT_NAME) < 0)
				{
					printf(ANSI_COLOR_RED "ERROR: [" ANSI_COLOR_CYAN "%s" ANSI_COLOR_RED " ] Fail to get %s partition idx" ANSI_COLOR_RESET "\n", imgtype, SECUREBOOT_NAME);
					ret= storage_write((uint32_t)floffset+0x200000, (size_t)size-0x28000, (const void *)offset+0x28000);
				}
				else
				{
					printf("Success to get %s partition idx\n", SECUREBOOT_NAME);
					ret= storage_write((uint32_t)floffset, (size_t)size-0x28000, (const void *)offset+0x28000);
				}
			}
			if(ret)
			{
				printf(ANSI_COLOR_RED "ERROR: [" ANSI_COLOR_CYAN "%s" ANSI_COLOR_RED " ] emmc write failed.." ANSI_COLOR_RESET "\n", imgtype);
				return ret;
			}
		}
		else
		{
			ret= storage_write((uint32_t)floffset, (size_t)size, (const void *)offset);
			if(ret) {
				printf(ANSI_COLOR_RED "ERROR: [" ANSI_COLOR_CYAN "%s" ANSI_COLOR_RED " ] emmc write failed.." ANSI_COLOR_RESET "\n", imgtype);
				return ret;
			}
			if(!strncmp(mpi->name, "license", 4))
				write_emergency_partition(LICE_MAGIC, 8);
		}
#endif
		update_partinfo(pak, unmpi, mpi, size);
	} else {
		printf("\t" ANSI_COLOR_RED "Wrong image size [%x/%x]" ANSI_COLOR_RESET "\n", size, flsize);
		return -1;
	}

	return ret;
}

static int _do_single_pak(PAK_HEADER_T * pak)
{
extern int erase_datapart(struct partmap_info *pmi);
extern int erase_cachepart(void);

	struct partition_info *mpi = NULL, *unmpi = NULL;
	char imgtype[PAK_TYPE_ID_LEN + 1];
	u32 offset = 0, size = 0;
	int ret = 0;

	print_pak_info(pak);
	offset = (ulong)(pak) + sizeof(PAK_HEADER_T);
	size = pak->imageSize;

	if(check_crc(pak, offset, size))
		return -1;

	get_imgtype(pak, imgtype);

	if(!strncmp(imgtype, "secureboot", 4))
	{
		printf("rom_boot pak download!!\n");

		char *bootCmd="mmc write.boot 1 0x";
		char *offsetCmd[128];
		char bWriteCmd[128]={'\0',};

		sprintf(offsetCmd,"%x",offset);
		strcat(bWriteCmd, bootCmd);
		strcat(bWriteCmd, offsetCmd);
		strcat(bWriteCmd, " 0 0x28000");

		printf("bootcmd = %s\n",bWriteCmd);
		ret = run_command(bWriteCmd,0);
		if(!ret)
			printf("finished write rom_boot\n");
		else
			printf(ANSI_COLOR_RED "ERROR: [" ANSI_COLOR_CYAN "%s" ANSI_COLOR_RED " ] rom_boot write failed..." ANSI_COLOR_RESET "\n", imgtype);

		return ret;
	}

#if 0
	if(!strncmp(imgtype, "spiboot", 4)) {
		SWU_SPI_Update((unsigned char *)offset, size);
		return 0;
	}

	if(!strncmp(imgtype, "micom", 4)) {
		SWU_MICOM_BinUpdate((char *)offset, size);
		return 0;
	}
#endif

	/* get partition info & handle */
	mpi		= get_used_partition(imgtype);
	unmpi	= get_unused_partition(imgtype);
	if(!mpi) {
		printf(ANSI_COLOR_RED "mpi = NULL" ANSI_COLOR_RESET "\n");
		return -1;
	}

	if(!check_partinfo(mpi, offset)) // Do not change partinfo version. skip partinfo partition
		return 0;

	if(ret = storage_partition(pak, unmpi, mpi, offset, size))
		return ret;

	// To make new hibernation image
	if(-1 != get_partition_idx(SNAP_PART_NAME))
		remake_hib();

	if(get_swumode() && (write_emergency_partition(0,0) < 0))
		printf("can not disable emergency mode\n");

	return 0;
}

#ifdef CONFIG_LOAD_TFTP_PARTIAL

// the partial buffer size must be bigger than parttion map size
#define PARTIAL_BUF_SZ                         1024*1024*128
// download status
enum PARTIAL_DOWNLOAD_STATUS partial_download_status = GET_EPK_HEADER;
// for store error status in partial download
enum PARTIAL_ERR partial_err;
// image type (epk/pak)
u32 partial_image_type = FILE_TYPE_UNKNOWN;
// pre_threshold : previous pak/header offset
// threshold : current threshold offset
// load_offset : remained offset for loaded buffer
ulong pre_threshold, threshold, load_offset;
// current downloaded pak index
unsigned short pak_index = 0;
// pak_loaded_size : currently loaded size to tftp loaded buffer(load_addr) in pak data block
// pak_writed_size : currently writed size to emmc in pak data block
ulong pak_loaded_size, pak_writed_size;
// current dowloaded pak crc value
u32 crc;
// pak file offset in epk file
PAK_LOCATION_T* imageLocation = NULL;
// current pak information
PAK_HEADER_T* pak = NULL;
// epk information
EPK_HEADER_T* epk = NULL;

// pak file can be partialized. so crc value must be calculated by the cumulative.
void calc_crc32_partial(unsigned char *buf, u32 len)
{
	unsigned char	*p;
	u32		fTableOk=0;

	// when new pak file
	if(pak_writed_size == 0)
	{
		if (fTableOk == 0)
		{
			init_crc32();   	/* build table */
			fTableOk = 1;
		}

		crc = 0xffffffff;       /* preload shift register, per CRC-32 spec */
	}

	// it's calcuated by the cumulative.
	for (p = buf; len > 0; ++p, --len)
		crc = (crc << 8) ^ crc32_table[(crc >> 24) ^ *p];

	return;
}

// compare the crc valen between the one of pak end and another one calculated by image file
static int check_crc_partial(u32 offset, u32 size)
{
	u32 calcrc = 0;

	calc_crc32_partial(offset, size);
	if(pak_writed_size + size >= pak->imageSize)
	{
		print_pak_info(pak);

		// get the crc value in end of pak file
		memcpy((void*)&calcrc, (void*)(offset + size), 4);

		//temp
		printf("test size:0x%x offset:0x%x offset+size:0x%p \n", size, offset, ((u32 *)(offset+size)));

		if(calcrc != crc) {
			printf("\t" ANSI_COLOR_RED "CRC check failed [%x/%x]" ANSI_COLOR_RESET "\n", calcrc, crc);
			return -1;
		}

		printf("\t CRC check [calcrc:%x/crc:%x]\n", calcrc, crc);
	}
	return 0;
}

// write to emmc from tftp buffer(load_addr) with already written offset
static int storage_partition_partial(PAK_HEADER_T *pak, struct partition_info* unmpi, struct partition_info* mpi, u32 size)
{
	char imgtype[PAK_TYPE_ID_LEN + 1];
	u32 floffset = 0, flsize = 0;
	int ret = 0;

	get_imgtype(pak, imgtype);

	// calcuate the emmc offset with partition map and already written offset
	if(unmpi) {
		floffset	= (ulong)(unmpi->offset + CFG_FLASH_BASE + pak_writed_size);
		flsize		= (ulong)(unmpi->size);
	} else {
		floffset	= (ulong)(mpi->offset + CFG_FLASH_BASE + pak_writed_size);
		flsize		= (ulong)(mpi->size);
	}

	if( (size > 0) && (size <= flsize) ) {

#if 0//def MTDINFO_IN_FLASH
		flash_protect(FLAG_PROTECT_CLEAR, floffset, floffset+size-1, &flash_info[0]);
		write_flash_data((uchar*)offset, floffset, size);
#else
	#ifdef USING_SIMPLE_MMAP
	#else
		#if 0//def CC_EMMC_BOOT
		if(!strcmp(mpi->name, "kernel") || !strcmp(mpi->name, "lgapp")) {
			ret= storage_write_image_fastboot((uint32_t)floffset, (size_t)size, (const void *)load_addr);
		}
		else
		#endif
	#endif
		if (!strcmp(mpi->name, "partinfo"))
		{
			ret = storage_write(DEFAULT_MAPBAK__BASE, size+4, (const void *)load_addr);
			if(ret) {
				partial_err = PARTIAL_EMMC_WRITE_ERR;
				printf(ANSI_COLOR_RED "ERROR: [" ANSI_COLOR_CYAN "%s" ANSI_COLOR_RED " ] emmc write failed.." ANSI_COLOR_RESET "\n", imgtype);
				return ret;
			}

			ret= storage_write((uint32_t)floffset, (size_t)size+4, (const void *)load_addr);
			if(ret) {
				partial_err = PARTIAL_EMMC_WRITE_ERR;
				printf(ANSI_COLOR_RED "ERROR: [" ANSI_COLOR_CYAN "%s" ANSI_COLOR_RED " ] emmc write failed.." ANSI_COLOR_RESET "\n", imgtype);
				return ret;
			}
		}
		else if (!strcmp(mpi->name, "boot"))
		{
			printf("boot pak download!!\n");

			printf("sw ver = %x\n", pak->SWVersion);
			if(pak->SWVersion <= 1)
			{
				if(get_partition_idx(SECUREBOOT_NAME) < 0)
				{
					printf(ANSI_COLOR_RED "ERROR: [" ANSI_COLOR_CYAN "%s" ANSI_COLOR_RED " ] Fail to get %s partition idx" ANSI_COLOR_RESET "\n", imgtype, SECUREBOOT_NAME);
					ret= storage_write((uint32_t)floffset+0x200000, (size_t)size, (const void *)load_addr);
				}
				else
				{
					printf("Success to get %s partition idx\n", SECUREBOOT_NAME);
					ret= storage_write((uint32_t)floffset, (size_t)size, (const void *)load_addr);
				}
			}
			else
			{
				char *bootCmd="mmc write.boot 1 0x";
				char *offsetCmd[128];
				char bWriteCmd[128]={'\0',};

				sprintf(offsetCmd,"%x",load_addr);
				strcat(bWriteCmd, bootCmd);
				strcat(bWriteCmd, offsetCmd);
				strcat(bWriteCmd, " 0 0x28000");

				printf("bootcmd = %s\n",bWriteCmd);
				run_command(bWriteCmd,0);

				printf("finished write rom_boot\n");
				if(get_partition_idx(SECUREBOOT_NAME) < 0)
				{
					printf(ANSI_COLOR_RED "ERROR: [" ANSI_COLOR_CYAN "%s" ANSI_COLOR_RED " ] Fail to get %s partition idx" ANSI_COLOR_RESET "\n", imgtype, SECUREBOOT_NAME);
					ret= storage_write((uint32_t)floffset+0x200000, (size_t)size-0x28000, (const void *)load_addr+0x28000);
				}
				else
				{
					printf("Success to get %s partition idx\n", SECUREBOOT_NAME);
					ret= storage_write((uint32_t)floffset, (size_t)size-0x28000, (const void *)load_addr+0x28000);
				}
			}
			if(ret)
			{
				printf(ANSI_COLOR_RED "ERROR: [" ANSI_COLOR_CYAN "%s" ANSI_COLOR_RED " ] emmc write failed.." ANSI_COLOR_RESET "\n", imgtype);
				return ret;
			}
		}
		else
		{
			ret= storage_write((uint32_t)floffset, (size_t)size, (const void *)load_addr);
			if(ret) {
				partial_err = PARTIAL_EMMC_WRITE_ERR;
				printf(ANSI_COLOR_RED "ERROR: [" ANSI_COLOR_CYAN "%s" ANSI_COLOR_RED " ] emmc write failed.." ANSI_COLOR_RESET "\n", imgtype);
				return ret;
			}
		}
#endif
		// update partinfo about download completed partition
		if(pak_writed_size + size == pak->imageSize)
		{
			if(!strncmp(mpi->name, "license", 4))
		        	write_emergency_partition(LICE_MAGIC, 8);
			update_partinfo(pak, unmpi, mpi, pak->imageSize);
		}
	} else {
		printf("\t" ANSI_COLOR_RED "Wrong image size [%x/%x]" ANSI_COLOR_RESET "\n", size, flsize);
		return -1;
	}

	return ret;
}

// download to emmc when pak load completed to the tftp buffer or almost full the tftp buffer
static int _do_single_pak_partial(PAK_HEADER_T * pak, ulong size)
{
extern int erase_datapart(struct partmap_info *pmi);
extern int erase_cachepart(void);

	struct partition_info *mpi = NULL, *unmpi = NULL;
	char imgtype[PAK_TYPE_ID_LEN + 1];
	int ret = 0;

	// check crc32
	if(check_crc_partial(load_addr, size))
	{
		partial_err = PARTIAL_CRC_ERR;
		return -1;
	}

	get_imgtype(pak, imgtype);

#ifdef SKIP_TYPE1_BOOT
	if ((!strncmp(imgtype, "secureboot", 4)) /*|| (!strncmp(imgtype, "boot", 4))*/)
	{
		printf("[SKIP_TYPE1_BOOT] %s:%d imgtype:%s, partial_image_type:%d\n", __func__, __LINE__, imgtype, partial_image_type);
		if ((partial_image_type == FILE_TYPE_EPK) && (_get_boardtype_lm18a() == BOARD_TYPE1))
		{
			printf("[SKIP_TYPE1_BOOT] %s:%d skip download\n", __func__, __LINE__);
			return ret;
		}
	}
#endif

	if(!strncmp(imgtype, "secureboot", 4))
	{
		printf("rom_boot pak download!!\n");

		char *bootCmd="mmc write.boot 1 0x";
		char *offsetCmd[128];
		char bWriteCmd[128]={'\0',};

		sprintf(offsetCmd,"%x",load_addr);
		strcat(bWriteCmd, bootCmd);
		strcat(bWriteCmd, offsetCmd);
		strcat(bWriteCmd, " 0 0x28000");

		printf("bootcmd = %s\n",bWriteCmd);
		ret = run_command(bWriteCmd,0);
		if(!ret)
			printf("finished write rom_boot\n");
		else
			printf(ANSI_COLOR_RED "ERROR: [" ANSI_COLOR_CYAN "%s" ANSI_COLOR_RED " ] rom_boot write failed..." ANSI_COLOR_RESET "\n", imgtype);

		return ret;
	}

#if 0
	if(!strncmp(imgtype, "spiboot", 4)) {
		SWU_SPI_Update((unsigned char *)offset, size);
		return 0;
	}

	if(!strncmp(imgtype, "micom", 4)) {
		SWU_MICOM_BinUpdate((char *)offset, size);
		return 0;
	}
#endif

	/* get partition info & handle */
	mpi		= get_used_partition(imgtype);
	unmpi	= get_unused_partition(imgtype);
	if(!mpi) {
		partial_err = PARTIAL_NO_PARTINFO;
		printf(ANSI_COLOR_RED "mpi = NULL" ANSI_COLOR_RESET "\n");
		return -1;
	}

	// whether current downloded pak is partinfo partition or not
	if(!check_partinfo(mpi, (ulong)load_addr)) // Do not change partinfo version. skip partinfo partition
		return 0;

	// write emmc
	if(ret = storage_partition_partial(pak, unmpi, mpi, size))
		return ret;

	// To make new hibernation image
	if(pak_writed_size + size >= pak->imageSize)
	{
	if(-1 != get_partition_idx(SNAP_PART_NAME))
		remake_hib();
	}

	if(get_swumode() && (write_emergency_partition(0,0) < 0))
		printf("can not disable emergency mode\n");

	return 0;
}

// get the pak offsets in download image
void set_pak_location()
{
	int i;
	EPK_HEADER_T* epk_buffer = (EPK_HEADER_T*)load_addr;
	PAK_LOCATION_T* pakloc;

	printf("\n [ " ANSI_COLOR_BLUE "PAK location info" ANSI_COLOR_RESET " ]\n");
	for(i=0; i<epk_buffer->fileNum; i++)
	{
		imageLocation[i].imageOffset = epk_buffer->imageLocation[i].imageOffset;
		imageLocation[i].imageSize = epk_buffer->imageLocation[i].imageSize;
		printf("\tfilenum = %d [%d] imageOffset = %lu imageSize = %lu\n", epk_buffer->fileNum, i, imageLocation[i].imageOffset, imageLocation[i].imageSize);
	}
}

// set pak header information
void set_pak_header()
{
	PAK_HEADER_T* pak_buffer = (PAK_HEADER_T*)load_addr;
	char imgtype[PAK_TYPE_ID_LEN + 1];

	// for maintain the pak header info, copy another area
	if(pak == NULL)
		pak = malloc(sizeof(PAK_HEADER_T));
	strncpy(pak->imageType, pak_buffer->imageType, PAK_TYPE_ID_LEN);
	pak->imageSize = pak_buffer->imageSize;
	strncpy(pak->modelName, pak_buffer->modelName, 64);
	pak->SWVersion = pak_buffer->SWVersion;
	pak->DevMode = pak_buffer->DevMode;
	pak->loadAddr = pak_buffer->loadAddr;
	pak->magic_number = pak_buffer->magic_number;

	// print pak header info
	get_imgtype(pak, imgtype);
	printf("\n[ " ANSI_COLOR_CYAN "%s" ANSI_COLOR_RESET" start ]\n", imgtype);
}

void print_epk_header(EPK_HEADER_T* epk)
{
	printf("\n[ " ANSI_COLOR_BLUE "EPK HEADER info" ANSI_COLOR_RESET " ]\n");
	printf("\tfileSize = %u\n", epk->fileSize);
	printf("\tfileNum = %u\n", epk->fileNum);
	printf("\tepkVersion = %u\n", epk->epkVersion);
	printf("\tota_id = %s\n", epk->ota_id);
}

// set epk_header information
void set_epk_header()
{
	EPK_HEADER_T* epk_buffer = (EPK_HEADER_T*)load_addr;

	// for maintain the epk header info, copy another area
	if(epk == NULL)
		epk = (EPK_HEADER_T*)malloc(sizeof(EPK_HEADER_T));
	(void)memcpy(epk->fileType, epk_buffer->fileType, PAK_TYPE_ID_LEN);
	epk->fileSize = epk_buffer->fileSize;

	// set first pak location data for get the pak offset location
	if(imageLocation == NULL)
		imageLocation = (PAK_LOCATION_T*)malloc(epk_buffer->fileNum * sizeof(PAK_LOCATION_T));
	imageLocation[0].imageOffset = epk_buffer->imageLocation[0].imageOffset;

	partial_image_type = _get_file_type((char*)epk);
	if(partial_image_type == FILE_TYPE_EPK)
	{
		epk->fileNum = epk_buffer->fileNum;
		epk->epkVersion = epk_buffer->epkVersion;
		(void)memcpy(epk->ota_id, epk_buffer->ota_id, 32);
		print_epk_header(epk);
	}
	else
		init_epk_info();
}

void init_pak_info()
{
	PAK_HEADER_T* tmp_pak;

	pak_loaded_size = 0;
	pak_writed_size = 0;
	load_offset = 0;

	if(pak != NULL)
	{
		tmp_pak = pak;
		pak = NULL;
		free(tmp_pak);
	}
}

void init_epk_info()
{
	EPK_HEADER_T* tmp_epk;

	if(epk != NULL)
	{
		tmp_epk = epk;
		epk = NULL;
		free(tmp_epk);
	}
}

void init_imageLocation()
{
	PAK_LOCATION_T* pakloc;

	if(imageLocation != NULL)
	{
		pakloc = imageLocation;
		imageLocation = NULL;
		free(pakloc);
	}
}

void init_partial_gval()
{
	pak_index = 0;
	partial_image_type = FILE_TYPE_UNKNOWN;
	partial_download_status = GET_EPK_HEADER;
	partial_err = PARTIAL_NO_ERR;
	threshold = 0;
	pre_threshold = 0;
	load_offset = 0;

	init_pak_info();
	init_imageLocation();
	init_epk_info();
}

void set_threshold(ulong offset)
{
	switch(partial_download_status)
	{
		case GET_EPK_HEADER :
			init_partial_gval();
			// for get the first pak start location
			threshold = sizeof(EPK_HEADER_T) + sizeof(PAK_LOCATION_T);
			break;
		case GET_PAK_LOCATION :
			pre_threshold = 0;
			threshold = imageLocation[0].imageOffset;
			load_offset = sizeof(EPK_HEADER_T) + sizeof(PAK_LOCATION_T);
			break;
		case GET_PAK_HEADER :
			init_pak_info();
			pre_threshold = (partial_image_type == FILE_TYPE_EPK) ? imageLocation[pak_index].imageOffset : 0;
			threshold = (partial_image_type == FILE_TYPE_EPK) ? imageLocation[pak_index].imageOffset + sizeof(PAK_HEADER_T) : sizeof(PAK_HEADER_T);
			load_offset = (partial_image_type == FILE_TYPE_EPK)? 0: sizeof(EPK_HEADER_T) + sizeof(PAK_LOCATION_T);
			break;
		case SKIP_PAK_DATA :
		case GET_PAK_DATA :
			pre_threshold = ((partial_image_type == FILE_TYPE_EPK) ? imageLocation[pak_index].imageOffset + sizeof(PAK_HEADER_T) : sizeof(PAK_HEADER_T));
			threshold = (partial_image_type == FILE_TYPE_EPK) ? imageLocation[pak_index].imageOffset + imageLocation[pak_index].imageSize : sizeof(PAK_HEADER_T) + pak->imageSize + 4;
			break;
		case DOWNLOAD_DONE :
			init_partial_gval();
			break;
		default :
			partial_err = PARTIAL_SET_THRESHOLD_ERR;
			printf(ANSI_COLOR_RED "error : %s" ANSI_COLOR_RESET "\n", __FUNCTION__);
			break;
	}
	// loaded size about pak data block
	pak_loaded_size = offset - pre_threshold;
	// gap of written offset to emmc and loaded offset to tftp buffer
	load_offset = pak_loaded_size - pak_writed_size;
}

// process function about loaded tftp buffer data
void partial_pak_download(ulong len)
{
	static int sboot_count = 0;
	static int boot_count = 0;

	switch(partial_download_status)
	{
		case GET_EPK_HEADER :
			set_epk_header();
			partial_download_status = ((partial_image_type == FILE_TYPE_EPK) ? GET_PAK_LOCATION : GET_PAK_HEADER);
			break;
		case GET_PAK_LOCATION :
			set_pak_location();
			partial_download_status = GET_PAK_HEADER;
			break;
		case GET_PAK_HEADER :
			set_pak_header();
			if(((!strncmp(pak->imageType, "secureboot", 4) && ((sboot_count++ % 2) == 1)) ||
				(!strncmp(pak->imageType, "boot", 4) && ((boot_count++ % 2) == 1))) &&
				(gModelOpt.micom_type == MODELOPT_MICOM_INTERNAL)) {
					printf("[%4s] count:[%d], MICOM type is internal. So skip to download\n", pak->imageType, (!strncmp(pak->imageType, "boot", 4))? boot_count:sboot_count);
					partial_download_status = SKIP_PAK_DATA;
					break;
			}
			partial_download_status = GET_PAK_DATA;
			break;
		case GET_PAK_DATA :
			if(pak_writed_size + len >= pak->imageSize + 4)
			{
				// pak download complete
				_do_single_pak_partial(pak, pak->imageSize - pak_writed_size);
				if(partial_image_type == FILE_TYPE_PAK)
					partial_download_status = DOWNLOAD_DONE;
				else
				{
					pak_index++;
					if(pak_index == epk->fileNum)
						partial_download_status = DOWNLOAD_DONE;
					else
						partial_download_status = GET_PAK_HEADER;
				}
				if(pak_writed_size + len > pak->imageSize + 4)
					printf("\n" ANSI_COLOR_YELLOW "warning : pak->imageSize is not suitable" ANSI_COLOR_RESET "\n");
			}
			else
			{
				// tftp buffer full
				_do_single_pak_partial(pak, len);
				pak_writed_size = pak_loaded_size;
				load_offset = 0;
			}
			break;
		case SKIP_PAK_DATA :
			if(pak_writed_size + len >= pak->imageSize + 4) {
				// pak download complete
				pak_index++;
				if(pak_index == epk->fileNum)
					partial_download_status = DOWNLOAD_DONE;
				else
					partial_download_status = GET_PAK_HEADER;
			} else {
				// tftp buffer full
				pak_writed_size = pak_loaded_size;
				load_offset = 0;
			}
			break;
		case DOWNLOAD_DONE :
			break;
		default :
			partial_err = PARTIAL_STATUS_ERR;
			printf(ANSI_COLOR_RED "error : %s" ANSI_COLOR_RESET "\n", __FUNCTION__);
			break;
	}
}

// partial download core function
int partial_download(ulong offset, uchar *src, unsigned len)
{
	int ret = 0;
	unsigned short i;
	ulong loaded_len = 0, current_len = 0;

	while(len != 0)
	{
	    if(partial_download_status == DOWNLOAD_DONE || partial_err != PARTIAL_NO_ERR)
		    break;

		// pak start and end offset setting
		set_threshold(offset);

		// buffer over case
		if((pak_loaded_size - pak_writed_size + len) > PARTIAL_BUF_SZ)
		{
			current_len = pak_loaded_size - pak_writed_size;
			partial_pak_download(current_len);
			continue;
		}

		// load to tftp buffer
		if(offset + len > threshold)
		{
			// status change case
			current_len = threshold - offset;
			memcpy(load_addr + load_offset, src + loaded_len, current_len);
			offset += current_len;
			loaded_len += current_len;
			len -= current_len;
			// process the tftp buffer data
			partial_pak_download(pak_loaded_size - pak_writed_size + current_len);
			load_offset = 0;
		}
		else if(offset + len == threshold) {
			// end of pak or end of pak/epk header case
			current_len = len;
			memcpy(load_addr + load_offset, src + loaded_len, current_len);
			loaded_len = 0;
			len -= current_len;
			// process the tftp buffer data
			partial_pak_download(pak_loaded_size - pak_writed_size + current_len);
            break;
		}
		else {
			// middle of pak or pak/epk header case
			current_len = len;
			memcpy(load_addr + load_offset, src + loaded_len, current_len);
			loaded_len = 0;
			len -= current_len;
			break;
		}
	};

	return ret;
}
#endif

extern void set_uart_baudrate(int baudrate);

#if CONFIG_USB_PL2303
extern ushort gu4PausePL2303;
#endif
#ifdef CONFIG_LOAD_TFTP_PARTIAL

int check_swup_err()
{
	int ret = -1;
	switch(partial_err)
	{
		case PARTIAL_STATUS_ERR :
			printf(ANSI_COLOR_RED "ERROR : wrong partial_download_status" ANSI_COLOR_RESET "\n");
			break;
		case PARTIAL_SET_THRESHOLD_ERR :
			printf(ANSI_COLOR_RED "ERROR : wrong threshold set" ANSI_COLOR_RESET "\n");
			break;
		case PARTIAL_CRC_ERR :
			printf(ANSI_COLOR_RED "ERROR : crc check fail" ANSI_COLOR_RESET "\n");
			// print crc status
			break;
		case PARTIAL_EMMC_WRITE_ERR :
			printf(ANSI_COLOR_RED "ERROR : emmc write error" ANSI_COLOR_RESET "\n");
			// found the error partition
			break;
		case PARTIAL_EMMC_SIZE_ERR :
			printf(ANSI_COLOR_RED "ERROR : emmc size error" ANSI_COLOR_RESET "\n");
			// found the error partition
			break;
		case PARTIAL_NO_PARTINFO :
			printf(ANSI_COLOR_RED "ERROR : mpi = NULL" ANSI_COLOR_RESET "\n");
			break;
		case PARTIAL_WRONG_BUF_SZ :
			break;
		case PARTIAL_NO_ERR :
			ret = 0;
			break;
		default :
			printf(ANSI_COLOR_RED "ERROR : wrong error status" ANSI_COLOR_RESET "\n");
			break;
	}
	return ret;
}

static int _do_epk_partial(int type, char *filename)
{
	EPK_HEADER_T	*epk = NULL;
	PAK_HEADER_T	*pak = NULL;
	PAK_LOCATION_T	*pakloc = NULL;
	u32 size = 0, filenum = 0, i = 0, count = 0;
	u32 filetype = 0;
	char dnfilename[128]={0};
	char *userpath;
	char *prj_name;
	int ret = 0;

	userpath = getenv("userpath");
	prj_name = getenv("prj_name");
	if(userpath && prj_name) {
		sprintf(dnfilename, "%s/pak-%s/%s", userpath, prj_name, filename);
	}
	else if (prj_name) {
		sprintf(dnfilename, "pak-%s/%s", prj_name, filename);
	}
	else {
		sprintf(dnfilename, "%s", filename);
	}

	init_partial_gval();

	if(sizeof(struct partmap_info) > PARTIAL_BUF_SZ)
	{
		printf(ANSI_COLOR_RED "ERROR : PARTIAL_BUF_SZ must be bigger than partmap_info struct size...!" ANSI_COLOR_RESET "\n");
		printf("\tsizeof(struct partmap_info) = %d\n", sizeof(struct partmap_info));
		printf("\tPARTIAL_BUF_SZ = %d\n", PARTIAL_BUF_SZ);
		partial_err = PARTIAL_WRONG_BUF_SZ;
		return -1;
	}
	else
	{

		switch(type) {
#ifdef CONFIG_LOAD_TFTP
			case LOAD_TYPE_TFTP:
#if CONFIG_USB_PL2303
				gu4PausePL2303 = 1;
#endif
				size = tftp_get(dnfilename);
#if CONFIG_USB_PL2303
				gu4PausePL2303 = 0;
#endif
				break;
#endif
#ifdef CONFIG_LOAD_FAT32_PARTIAL
			case LOAD_TYPE_FAT32:
				size = fat_fsload(filename);
				break;
#endif
#ifdef CONFIG_LOAD_ZMODEM
			case LOAD_TYPE_ZMODEM:
				size = rz("test.txt", default_offset);
				break;
#endif
#ifdef CONFIG_LOAD_HZMODEM
			case LOAD_TYPE_HZMODEM:
				set_uart_baudrate(CONFIG_RZ_BAUDRATE);
				size = rz("test.txt", default_offset);
				break;
#endif
#ifdef CONFIG_LOAD_SERIAL
			case LOAD_TYPE_SERIAL:
				size = get_serial_data(default_offset);
				break;
#endif
			default:
				size = 0;
				break;
		}
	}

	if(size == 0) {
		dprintf(ANSI_COLOR_RED "file receive error...(type:%d, size:%d)" ANSI_COLOR_RESET "\n", type, size);
#ifdef CONFIG_LOAD_HZMODEM
		if(type == LOAD_TYPE_HZMODEM) {
			printf("\n\n");
			set_uart_baudrate(CONFIG_BAUDRATE);
		}
#endif
	}

	init_partial_gval();
	return check_swup_err();
}

#endif

static int _do_epk(int type, char *filename)
{
	EPK_HEADER_T	*epk = NULL;
	PAK_HEADER_T	*pak = NULL;
	PAK_LOCATION_T	*pakloc = NULL;
	u32 size = 0, filenum = 0, i = 0, count = 0;
	u32 filetype = 0;
	char dnfilename[128]={0};
	char *userpath;
	char *prj_name;
	int ret = 0;
	int sboot_count_swu = 0;
	int boot_count_swu = 0;

	userpath = getenv("userpath");
	prj_name = getenv("prj_name");
	if(userpath && prj_name) {
		sprintf(dnfilename, "%s/pak-%s/%s", userpath, prj_name, filename);
	}
	else if (prj_name) {
		sprintf(dnfilename, "pak-%s/%s", prj_name, filename);
	}
	else {
		sprintf(dnfilename, "%s", filename);
	}

	switch(type) {
#ifdef CONFIG_LOAD_TFTP
		case LOAD_TYPE_TFTP:
#if CONFIG_USB_PL2303
			gu4PausePL2303 = 1;
#endif
			size = tftp_get(dnfilename);
#if CONFIG_USB_PL2303
			gu4PausePL2303 = 0;
#endif
			break;
#endif
#ifdef CONFIG_LOAD_FAT32
		case LOAD_TYPE_FAT32:
			size = fat_fsload(filename);
			break;
#endif
#ifdef CONFIG_LOAD_ZMODEM
		case LOAD_TYPE_ZMODEM:
			size = rz("test.txt", default_offset);
			break;
#endif
#ifdef CONFIG_LOAD_HZMODEM
		case LOAD_TYPE_HZMODEM:
			set_uart_baudrate(CONFIG_RZ_BAUDRATE);
			size = rz("test.txt", default_offset);
			break;
#endif
#ifdef CONFIG_LOAD_SERIAL
		case LOAD_TYPE_SERIAL:
			size = get_serial_data(default_offset);
			break;
#endif
		default:
			size = 0;
			break;
	}

	if(size == 0) {
		dprintf(ANSI_COLOR_RED "file receive error...(type:%d, size:%d)" ANSI_COLOR_RESET "\n", type, size);
		goto _epk_end;
	}

	if(strstr(filename, ".bin") != NULL)
	{
		goto _bin_start;
	}

	filetype = _get_file_type((char*)default_offset);

	switch (filetype) {
		case FILE_TYPE_EPK:
			goto _epk_start;
			break;

		case FILE_TYPE_PAK:
			pak = (PAK_HEADER_T*)default_offset;
			_do_single_pak(pak);
			#if 0
			goto _save_mtdinfo;
			#else
			goto _epk_end;
			#endif
			break;

		default:
			printf("unknown file type (0x%x)\n", filetype);
			return -1;
	}

_bin_start:
	printf("binary file size : %d KB\n", size);
	printf("binary file copy start. wait a minute plz.\n");

	if(strstr(filename, "boot") == NULL)
	{
		run_command("mmc write.boot 1 0x22000000 0 0x28000",0);

		printf("binary test!\n");
		ret = storage_write((uint32_t)(default_offset_bin+0x200000), (size_t)(size-0x28000), (const void *)(default_offset+0x28000));
	}
	else
	{
		printf("FAIL!! if you want download bin file, please use [load] command\n");
		return ret;
	}
	if(ret) {
		printf("emmc bin write failed..--;;\n");
		return ret;
	}
	printf("binary file copy completed.\n");
	goto _epk_end;

_epk_start:
	epk = (EPK_HEADER_T*)(default_offset);

	/* check epk version */
	dprintf("check epk version : %lx\n", epk->epkVersion);
	GET_PARTMAP_INFO()->old_epk_ver = GET_PARTMAP_INFO()->cur_epk_ver;
	GET_PARTMAP_INFO()->cur_epk_ver = epk->epkVersion;

	/* handle epk package */
	filenum = epk->fileNum;

	for(i=0; i<filenum;i++) {

		dprintf("filenum = %d [%d]\n", filenum, i);

#ifdef SKIP_TYPE1_BOOT
		if ((!strncmp(pak->imageType, "secureboot", 4)) /*|| (!strncmp(pak->imageType, "boot", 4))*/)
		{
			printf("[SKIP_TYPE1_BOOT] %s:%d imgtype:%s, filetype:%d\n", __func__, __LINE__, pak->imageType, filetype);
			if ((filetype == FILE_TYPE_EPK) && (_get_boardtype_lm18a() == BOARD_TYPE1))
			{
				printf("[SKIP_TYPE1_BOOT] %s:%d skip download\n", __func__, __LINE__);
				return ret;
			}
		}
#endif

		pakloc = &(epk->imageLocation[i]);

		if(pakloc->imageOffset == 0)
			continue;

		pak = (PAK_HEADER_T *)(default_offset + pakloc->imageOffset);

		if(((!strncmp(pak->imageType, "secureboot", 4) && ((sboot_count_swu++ % 2) == 1)) ||
			(!strncmp(pak->imageType, "boot", 4) && ((boot_count_swu++ % 2) == 1))) &&
			(gModelOpt.micom_type == MODELOPT_MICOM_INTERNAL)) {
				printf("[%4s] count:[%d], MICOM type is internal. So skip to download\n", pak->imageType, (!strncmp(pak->imageType, "boot", 4))? boot_count_swu:sboot_count_swu);
				continue;
		}

		if(_do_single_pak(pak)) {
			printf(ANSI_COLOR_RESET "Huk --;;" ANSI_COLOR_RESET "\n");
		}

		count++;
		if(count==filenum)
			break;
	}

#if 0
_save_mtdinfo:
	/* save mtdinfo */
	save_mtdinfo();
#endif

_epk_end:
#ifdef CONFIG_LOAD_HZMODEM
	if(type == LOAD_TYPE_HZMODEM) {
		printf("\n\n");
		set_uart_baudrate(CONFIG_BAUDRATE);
	}
#endif
	return 0;
}

#ifdef CONFIG_LOAD_TFTP
int do_swu (cmd_tbl_t *cmdtp, int flag, int argc, char *argv[])
{
	if(argc != 2) {
		printf ("Usage:\n%s\n", cmdtp->usage);
		printf ("         :\n%s\n", cmdtp->help);
		return -1;
	}
	return _do_epk(LOAD_TYPE_TFTP, argv[1]);
}

#ifdef jjaem // tmp force mapping swu->swup
U_BOOT_CMD(
		swu,	2,	0,	do_swu,
		"swu\t- downlaod epk image file from tftp, and write on flash \n",
		"[filename]\n"
		);
#endif // jjaem
#endif

static int _do_epk_ex(int type, char *filename1, char *filename2)
{
	u32 size[2] = {0,}, i = 0;
	char dnfilename[128]={0,};
	int ret = 0;

	for(i=0; i<2; i++)
	{
		if((strstr(filename1, ".part1") != NULL) && (strstr(filename2, ".part2") != NULL))
			sprintf(dnfilename, "%s", (i==0)? filename1 : filename2);
		else if((strstr(filename1, ".part2") != NULL) && (strstr(filename2, ".part1") != NULL))
			sprintf(dnfilename, "%s", (i==0)? filename2 : filename1);
		else {
			printf("File name is wrong. Please check file name(foramt: filename.bin1 filename.bin2)\n");
			goto _bin_end;
		}

		switch(type) {
#ifdef CONFIG_LOAD_TFTP
			case LOAD_TYPE_TFTP:
#if CONFIG_USB_PL2303
				gu4PausePL2303 = 1;
#endif
				size[i] = tftp_get(dnfilename);
#if CONFIG_USB_PL2303
				gu4PausePL2303 = 0;
#endif
				break;
#endif
#ifdef CONFIG_LOAD_FAT32
			case LOAD_TYPE_FAT32:
				size[i] = fat_fsload(dnfilename);
				break;
#endif
			default:
				size[i] = 0;
				break;
		}

		if(size[i] <= 1024) {
			dprintf("file receive error...(type:%d, size:%d)\n", type, size[i]);
			goto _bin_end;
		}

		printf("binary[%d] file size : %d KB\n", i, size[i]);
		printf("binary[%d] file copy start. wait a minute plz.\n", i);

		if(i == 0) {
			char *bootCmd="mmc write.boot 1 0x";
			char offsetCmd[128];
			char bWriteCmd[128]={'\0',};

			sprintf(offsetCmd,"%x",default_offset);
			strcpy(bWriteCmd, bootCmd);
			strcat(bWriteCmd, offsetCmd);
			strcat(bWriteCmd, " 0 0x28000");

			printf("bootcmd = %s\n",bWriteCmd);
			ret = run_command(bWriteCmd,0);
			if(!ret)
				printf("finished write rom_boot\n");
			else
			{
				printf("rom_boot write failed..--;;\n");
				return ret;
			}

			ret = storage_write((uint32_t)(default_offset_bin+0x200000), (size_t)(size[i]-0x28000), (const void *)(default_offset+0x28000));
			printf("Download completed 1st part image. file offset: %u, file size: %u\n", default_offset_bin+0x200000, size[i]-0x28000);
		}
		else
		{
			ret = storage_write((uint32_t)(default_offset_bin+0x200000+size[0]-0x28000), (size_t)(size[i]), (const void *)default_offset);
			printf("Download completed 2nd part image. file offset: %u, file size: %u\n", default_offset_bin+0x200000+size[0]-0x28000, size[i]);
		}

		if(ret) {
			printf("emmc bin%d write failed..--;;\n", i);
			return ret;
		}
	}

_bin_end:
#ifdef CONFIG_LOAD_HZMODEM
	if(type == LOAD_TYPE_HZMODEM) {
		printf("\n\n");
		set_uart_baudrate(CONFIG_BAUDRATE);
	}
#endif
	return 0;
}

#ifdef CONFIG_MULTICORES_PLATFORM
static int swue_download_from_tftp(void *param)
{
	int size;
	char *fileName = (char*)param;

	printf("[tftp]Start SWUE download thread...\n");

	size = tftp_get(fileName);

	if(size <= 1) printf("SWUE downlaod failure. rc=%d\n", size);
	else {
		printf("SWUE downlaod success.\n");
		swue_close_buffer();
	}

	return size;
}

static int swue_download_from_usb(void *param)
{
   int size;
   char *fileName = (char*)param;

   printf("[usb]Start SWUE download thread...\n");

   size = fat_fsload(fileName);

   if(size <= 1) printf("SWUE downlaod failure. rc=%d\n", size);
   else {
       printf("SWUE downlaod success.\n");
       swue_close_buffer();
   }

   return size;
}

static int percentage(uint32_t numerator, uint32_t denominator)
{
	int p;

	if(numerator < (0xFFFFFFFF/100))
		p = numerator * 100 / denominator;
	else
		p = numerator / (denominator/100);

	return p;
}

static int swub_download_boot(const u8* data_buf)
{
	char *bootCmd="mmc write.boot 1 0x";
	char offsetCmd[16]= {0,};
	char bWriteCmd[40]={0,};
	int ret = -1;

	sprintf(offsetCmd,"%x",data_buf);
	strcpy(bWriteCmd, bootCmd);
	strcat(bWriteCmd, offsetCmd);
	strcat(bWriteCmd, " 0 0x28000");

	ret = run_command(bWriteCmd,0);
	if(!ret)
		printf("finished write rom_boot\n");
	else
		printf("rom_boot write failed..--;;\n");

	return ret;
}

static int swub_download_image(u32 image_offset, size_t image_size, uint32_t flash_offset)
{
	u8		*copy_buf = swue_buf->copy_buf;
	size_t 	wsize = 0;
	int		downloaded_data = 0;
	int		print_ctrl = 0;

	if(swue_seek_data(image_offset, SWUE_TIME_DELAY) < 0) {
		printf("[%s:%d]swue_seek_data error!\n", __FUNCTION__, __LINE__);
		return -1;
	}

	memset(copy_buf, 0x0, SWUE_COPY_BUF_SIZE);

	if(image_size) {	// If image_size is not 0: It means that there is HEAD data
		size_t	remained = image_size;
		do {
			if (ctrlc()) {
				puts("\nAbort\n");
				return -1;
			}
			wsize = min(remained, SWUE_COPY_BUF_SIZE);
			if(!flash_offset) {
				if(swue_get_data(copy_buf, wsize, SWUE_TIME_DELAY, 0) < 0x28000) return -1;
				if(swub_download_boot((const u8*)copy_buf) < 0) return -1;

				remained -= wsize;
				flash_offset += wsize;
			} else {
				if(swue_get_data(copy_buf, wsize, SWUE_TIME_DELAY, 0) < 0) return -1;
				if(storage_write(flash_offset, wsize, (const void *)copy_buf) < 0) return -1;

				remained -= wsize;
				flash_offset += wsize;

				downloaded_data = percentage((image_size-remained), image_size);
				if(downloaded_data > print_ctrl) {
					printf("\r\t\t\t\t\t%5d%% Completed.", downloaded_data);
					print_ctrl++;
				}
			}
		}while(remained != 0);
		printf("\r\t\t\t\t\t%5d%% Completed.", downloaded_data);
	} else {			// If image_size is 0: It means that there is not HEAD data
		int	current_status = 0;
		do {
			if (ctrlc()) {
				puts("\nAbort\n");
				return -1;
			}
			current_status = swue_buf->status;
			if(!flash_offset) {
				if((wsize = swue_get_data(copy_buf, SWUE_COPY_BUF_SIZE, SWUE_TIME_DELAY, 0)) < 0x28000) return -1;
				if(swub_download_boot((const u8*)copy_buf) < 0) return -1;

				wsize -= 0x28000;
				flash_offset += 0x200000;

				if(storage_write(flash_offset, wsize, (const void *)(copy_buf+0x28000)) < 0) return -1;

				flash_offset += wsize;
				downloaded_data += 0x28000 + wsize;
				continue;
			} else {
				if((wsize=swue_get_data(copy_buf, SWUE_COPY_BUF_SIZE, 0, 0)) < 0) return -1;
				if(storage_write(flash_offset, wsize, (const void *)copy_buf) < 0) return -1;

				flash_offset += wsize;
				downloaded_data = flash_offset-0x200000+0x28000;

				if(downloaded_data > print_ctrl) {
					printf("\r\t\t\t\t\t%15d Bytes Completed.", downloaded_data);
					print_ctrl += 1024;
				}
			}
		}while(current_status != SWUE_BUF_STATUS_DONE || wsize == SWUE_COPY_BUF_SIZE);
		printf("\r\t\t\t\t\t%15d Bytes Completed.", downloaded_data);
	}

	return 0;
}


static int _do_bin(int type, char *filename)
{
	char dnfilename[128]={0,};
	int ret = -1;
	char *userpath = getenv("userpath");
	char *prj_name = getenv("prj_name");
	thread_t *swue_dn_thread = NULL;
	RAWIMG_HEADER_T *check_head = (RAWIMG_HEADER_T*)malloc(SECTOR_SIZE_BOOT);	// Head info
	if(check_head == NULL) {
		printf("[%s:%d] malloc fail\n", __FUNCTION__, __LINE__);
		goto _bin_end;
	}

	swue_init_buffer((void*)load_addr, SWUE_BUF_SIZE);

	if(userpath && prj_name)
		sprintf(dnfilename, "%s/pak-%s/%s", userpath, prj_name, filename);
	else if (prj_name)
		sprintf(dnfilename, "pak-%s/%s", prj_name, filename);
	else
		sprintf(dnfilename, "%s", filename);

	switch(type) {
#ifdef CONFIG_LOAD_TFTP
		case LOAD_TYPE_TFTP:
#if CONFIG_USB_PL2303
			gu4PausePL2303 = 1;
#endif
			swue_dn_thread = thread_create_ex("swue_download_tftp", swue_download_from_tftp, dnfilename, 0, 1, 255, 1);
			if(swue_dn_thread == NULL) goto _bin_end;
#if CONFIG_USB_PL2303
			gu4PausePL2303 = 0;
#endif
			break;
#endif

#ifdef CONFIG_LOAD_FAT32
		case LOAD_TYPE_FAT32:
#if CONFIG_USB_PL2303
			gu4PausePL2303 = 1;
#endif
			swue_dn_thread = thread_create_ex("swue_download_usb", swue_download_from_usb, filename, 0, 1, 255, 1);
			if(swue_dn_thread == NULL) goto _bin_end;
#if CONFIG_USB_PL2303
			gu4PausePL2303 = 0;
#endif
			break;
#endif

		default:
			break;
	}

	// Check HEAD data
	if(swue_get_data((void*)check_head, SECTOR_SIZE_BOOT, SWUE_TIME_DELAY, SWUE_FLAG_DONOT_UPDATE_INFO) != SECTOR_SIZE_BOOT) {
		goto _bin_end;
	}

	if(!strncmp(check_head->magic, RAWIMAGE_MAGIC, strlen(RAWIMAGE_MAGIC))) {
		PART_LOCATION_T	*partloc;					// each partition info
		uint32_t flash_offset = 0;					// eMMC offset to download
		int part_index = 0;							// part_index : current partition index
		int partnum = check_head->partition_count;	// Total partition number

		printf("Binary type is HEAD\n");
		printf("binary magic: %8s\n", check_head->magic);
		printf("partition number: %d\n", check_head->partition_count);

		for (part_index=0; part_index<partnum; part_index++) {
			partloc = &(check_head->part_location[part_index]);
			printf("\npartition location [%d/%d] info - image_offset: 0x%llx, image_size: 0x%llx, flash_offset: 0x%llx\n",
				part_index+1, partnum, partloc->image_offset, partloc->image_size, partloc->flash_offset);

			if(partloc->image_size == 0)
				continue;

			ret = swub_download_image((u32)(partloc->image_offset), (size_t)(partloc->image_size), (uint32_t)(partloc->flash_offset));
			if(ret < 0) {
				printf("[%s:%d]: storage write error..--;;\n", __FUNCTION__, __LINE__);
				goto _bin_end;
			}
		}
	}
	else {
		printf("Binary type is ORIGINAL\n");
		ret = swub_download_image(0, 0, 0);
		if(ret < 0)
			printf("[%s:%d]: storage write error..--;;\n", __FUNCTION__, __LINE__);
	}

_bin_end:
	if(ret < 0)
		printf("Failure!!\n");
	else
		printf("Success!!\n");
	if(swue_dn_thread) thread_join(swue_dn_thread, NULL);
	if(check_head) free(check_head);
	return ret;
}
#endif


#ifdef CONFIG_LOAD_TFTP_PARTIAL
enum PARTIAL_MODE swup_mode;

int do_swup (cmd_tbl_t *cmdtp, int flag, int argc, char *argv[])
{
	int ret = 0;
	if(argc != 2) {
		printf ("Usage:\n%s\n", cmdtp->usage);
		printf ("         :\n%s\n", cmdtp->help);
		return -1;
	}
	swup_mode = PARTIAL;
	printf("swup_mode = %d", swup_mode);
	ret = _do_epk_partial(LOAD_TYPE_TFTP, argv[1]);
	swup_mode = NORMAL;
	return ret;
}

U_BOOT_CMD(
		swup,	2,	0,	do_swup,
		"swup\t- partial downlaod epk image file from tftp, and write on flash \n",
		"[filename]\n"
		);
// tmp force mapping swu->swup by jjaem
U_BOOT_CMD(
		swu,	2,	0,	do_swup,
		"swu\t- downlaod epk image file from tftp, and write on flash \n",
		"[filename]\n"
		);
#endif

#ifdef CONFIG_LOAD_TFTP
int do_swub (cmd_tbl_t *cmdtp, int flag, int argc, char *argv[])
{
#ifdef CONFIG_MULTICORES_PLATFORM
	int ret = 0;
	if(argc != 2) {
		printf ("Usage:\n%s\n", cmdtp->usage);
		printf ("		  :\n%s\n", cmdtp->help);
		return -1;
	}
	swup_mode = PARTIAL_BIN;
	printf("swup_mode = PARTIAL_BIN\n");
	ret = _do_bin(LOAD_TYPE_TFTP, argv[1]);
	swup_mode = NORMAL;
	return ret;
#else
	if(argc == 3) return _do_epk_ex(LOAD_TYPE_TFTP, argv[1], argv[2]);
	else {
		printf ("Usage:\n%s\n", cmdtp->usage);
		printf ("         :\n%s\n", cmdtp->help);
		return -1;
	}
#endif
}

U_BOOT_CMD(
		swub,	3,	0,	do_swub,
		"swub\t- downlaod raw image file from tftp, and write on flash \n",
		"swub [filename] || swub [filename1] [filename2]\n"
		);
#endif

#ifdef CONFIG_LOAD_FAT32_PARTIAL

enum PARTIAL_MODE swump_mode;

int do_swump (cmd_tbl_t *cmdtp, int flag, int argc, char *argv[])
{
	int ret = 0;
	if(argc != 2) {
		printf ("Usage:\n%s\n", cmdtp->usage);
		printf ("         :\n%s\n", cmdtp->help);
		return -1;
	}
	swump_mode = PARTIAL;
	//printf("swump_mode = %d\n", swump_mode);
	printf("\n" ANSI_COLOR_RED "Partial USB Download Start ... " ANSI_COLOR_RESET "\n");
	ret = _do_epk_partial(LOAD_TYPE_FAT32, argv[1]);
	swump_mode = NORMAL;
	return ret;
}

U_BOOT_CMD(
		swuup,	2,	0,	do_swump,
		"swuup\t- partial downlaod epk image file from fat32, and write on flash \n",
		"[filename]\n"
		);
#endif


#ifdef CONFIG_LOAD_FAT32
int do_swum (cmd_tbl_t *cmdtp, int flag, int argc, char *argv[])
{
	if(argc != 2) {
		printf ("Usage:\n%s\n", cmdtp->usage);
		printf ("         :\n%s\n", cmdtp->help);
		return -1;
	}
	return _do_epk(LOAD_TYPE_FAT32, argv[1]);
}

U_BOOT_CMD(
		swuu,	2,	0,	do_swum,
		"swum\t- downlaod epk image file from fat32, and write on flash \n",
		"[filename]\n"
		);
#endif

#ifdef CONFIG_LOAD_FAT32
int do_swumb (cmd_tbl_t *cmdtp, int flag, int argc, char *argv[])
{
	if(argc == 3) return _do_epk_ex(LOAD_TYPE_FAT32, argv[1], argv[2]);
	else {
		printf ("Usage:\n%s\n", cmdtp->usage);
		printf ("         :\n%s\n", cmdtp->help);
		return -1;
	}
}

int do_swuub (cmd_tbl_t *cmdtp, int flag, int argc, char *argv[])
{
   int ret = 0;
   if(argc != 2) {
       printf ("Usage:\n%s\n", cmdtp->usage);
       printf ("         :\n%s\n", cmdtp->help);
       return -1;
   }
   swup_mode = PARTIAL_BIN; // swue
   printf("swup_mode = PARTIAL_BIN\n");
   ret = _do_bin(LOAD_TYPE_FAT32, argv[1]);
   swup_mode = NORMAL;
   return ret;
}

U_BOOT_CMD(
		swuub,	3,	0,	do_swuub,
		"swuub\t- downlaod raw image file from fat32, and write on flash \n",
		"swuub [filename] \n"
		);
#endif


#ifdef CONFIG_LOAD_ZMODEM
int do_swuz (cmd_tbl_t *cmdtp, int flag, int argc, char *argv[])
{
	if(argc != 1) {
		printf ("Usage:\n%s\n", cmdtp->usage);
		printf ("		  :\n%s\n", cmdtp->help);
		return -1;
	}
	return _do_epk(LOAD_TYPE_ZMODEM, (void*)0x0);
}

U_BOOT_CMD(
		swuz,	1,	0,	do_swuz,
		"swuz\t- downlaod epk image file from zmodem, and write on flash \n",
		" \n"
		);
#endif

#ifdef CONFIG_LOAD_HZMODEM
int do_swuhz (cmd_tbl_t *cmdtp, int flag, int argc, char *argv[])
{
	if(argc != 1) {
		printf ("Usage:\n%s\n", cmdtp->usage);
		printf ("		  :\n%s\n", cmdtp->help);
		return -1;
	}
	return _do_epk(LOAD_TYPE_HZMODEM, (void*)0x0);
}

U_BOOT_CMD(
		swuhz,	1,	0,	do_swuhz,
		"swuhz\t- downlaod epk image file from high speed zmodem, and write on flash \n",
		" \n"
		);
#endif

#ifdef CONFIG_LOAD_SERIAL
int do_swus (cmd_tbl_t *cmdtp, int flag, int argc, char *argv[])
{
	if(argc != 1) {
		printf ("Usage:\n%s\n", cmdtp->usage);
		printf ("		  :\n%s\n", cmdtp->help);
		return -1;
	}
	return _do_epk(LOAD_TYPE_SERIAL, (void*)0x0);
}

U_BOOT_CMD(
		swus,	1,	0,	do_swus,
		"swus\t- downlaod epk image file from serial, and write on flash \n",
		" \n"
		);
#endif

#if 0
/*
 * for swu mode test by junorion
 */
#ifndef TRUE
#define TRUE  1
#endif

#ifndef FALSE
#define FALSE 0
#endif

extern UINT8 DDI_NVM_GetSWUMode( void );
extern void DDI_NVM_SetSWUMode( UINT8 mode );

int do_swu_mode(cmd_tbl_t *cmdtp, int flag,int argc, char *argv[])
{
	UINT8 swu_mode = DDI_NVM_GetSWUMode();

	int i;
//	for(i=0; i<argc; i++)
//		printf("arg[%d] : %s\n", i, argv[i]);

	printf("[swu mode status]\n");
	printf("current : %s", swu_mode ? "on" : "off");


	if(argc == 1) {
		printf("\n");
		return 0;
	}

	if (argc == 2) {
		UINT8 new_mode = swu_mode;

			new_mode = (strcmp(argv[1], "on")) ? (UINT8)FALSE : (UINT8)TRUE;

		if(swu_mode != new_mode) {
			printf(" => %s\n", argv[1]);
			DDI_NVM_SetSWUMode((UINT8)(new_mode));
		}
	}
	else
		return -1;
	return 0;
}

U_BOOT_CMD(
		swumode,	2,	0,	do_swu_mode,
		"swumode\t- set swu mode\n",
		"swumode [on/off]\n"
		);

/*
 * for snapshot boot by junorion
 */
extern UINT8 DDI_NVM_GetMakeHib(void);
extern void DDI_NVM_SetMakeHib( UINT8 mode );

static int make_hib_cmd(cmd_tbl_t *cmdtp, int flag, int argc, char *argv[])
{
	UINT8 make_hib = DDI_NVM_GetMakeHib();

	printf("[make hib status]\n");
	printf("current : %s", make_hib ? "on" : "off");

	if(argc == 1) {
		printf("\n");
		return 0;
	}

	if (argc == 2) {
		UINT8 new_make_hib = make_hib;

		new_make_hib = (strcmp(argv[1], "on")) ? (UINT8)FALSE : (UINT8)TRUE;

		if(make_hib != new_make_hib) {
			printf(" => %s\n", argv[1]);
			DDI_NVM_SetMakeHib((UINT8)(new_make_hib));
		}
	}
	else
		return -1;
	return 0;
}

U_BOOT_CMD(
		make_hib,	2,	0,	make_hib_cmd,
		"make_hib\t- set make hib\n",
		"make_hib [on/off]\n"
		);

#endif

#ifdef SKIP_TYPE1_BOOT
LM18A_BOARD_TYPE_T _get_boardtype_lm18a (void)
{
	int getVal = 0;

	// type1 - return 0, type2 - return 1
	getVal = mdrv_gpio_get_level(GPIO_LM18A_BOARD_TYPE);

	printf("[SKIP_TYPE1_BOOT] %s:%d getVal:%d\n", __func__, __LINE__, getVal);

	if (getVal == 0)
		return BOARD_TYPE1;
	else
		return BOARD_TYPE2;
}
#endif
