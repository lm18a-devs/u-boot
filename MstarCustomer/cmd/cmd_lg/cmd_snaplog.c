#include <stdio.h>
#include <linux/types.h>
#include <linux/string.h>
#include <cmd_snaplog.h>
#include <cmd_resume.h>

#define SNAPSHOT_LOG_WRITE_POS(x) (((x)-1) % MAX_LOG_CNT)

// dump start address + 10076160(0x99C000)Bytes
#define SNAPSHOT_LOG_START_POS (10076160)

char *typeToStr[SNAPSHOT_LOG_TYPE_END] =
{ "unknown", "remake reason", "error"};

char *remakeReasonToStr[SNAPSHOT_REMAKE_REASON_END] =
{ "unknown", "poweronly", "factorydefault", "mac change",
	"widevine change", "tooloption change", "countrygroup change", "modelname change",
	"serial change", "debuglevel change", "sbproperty change", "videodelay change"};

char *errorReasonToStr[SNAPSHOT_ERROR_REASON_END] =
{ "unknown", "verify app", "invalid partition", "emmc read", "deppart mismatch",
	"mem alloc", "uncomp restore", "comp restore", "verify", "tz load"};

extern int storage_read(uint32_t offset,size_t len,void * buf);
extern int storage_write(uint32_t offset,size_t len,const void * buf);
extern int storage_get_partition(const char* name, storage_partition_t* info);

int _SNAPSHOT_GetLogStartPos(uint32_t *start_offset)
{
	storage_partition_t partition;

	/* Get partition which has snapshot boot image(hibernation image) */
	if (storage_get_partition("dump", &partition) < 0) {
		printf("Invalid snapshot image partition\n");
		return -1;
	}

	*start_offset = partition.offset + SNAPSHOT_LOG_START_POS;
	return 0;
}

static int _SNAPSHOT_GetLogData(uint32_t start_offset, int offset, void *buf, int bufSize)
{
	uint32_t read_offset;

	if (NULL == buf)
	{
		return -1;
	}

	memset(buf, 0, bufSize);
	read_offset = start_offset + offset;
	if (0 != storage_read(read_offset, bufSize, buf))
	{
		printf(" %s read error %d\n", __FUNCTION__, __LINE__);
		return -1;
	}

	return 0;
}

static int _SNAPSHOT_SetLogData(uint32_t start_offset, int offset, void *buf, int bufSize)
{
	uint32_t write_offset;

	if (NULL == buf)
	{
		return -1;
	}

	write_offset = start_offset + offset;
	if (0 != storage_write(write_offset, bufSize, buf))
	{
		printf(" %s write error %d\n", __FUNCTION__, __LINE__);
		return -1;
	}

	return 0;
}

static int _SNAPSHOT_GetLogHeader(uint32_t start_offset, SNAPSHOT_LOG_HEADER_T *header)
{
	return _SNAPSHOT_GetLogData(start_offset, 0, header, sizeof(SNAPSHOT_LOG_HEADER_T));
}

static int _SNAPSHOT_GetLogInfo(uint32_t start_offset, SNAPSHOT_LOG_T *log, int idx)
{
	int offset;

	if (0 > idx)
	{
		printf(" %s error %d\n", __FUNCTION__, __LINE__);
		return -1;
	}

	offset = idx * sizeof(SNAPSHOT_LOG_T) + sizeof(SNAPSHOT_LOG_HEADER_T);
	return _SNAPSHOT_GetLogData(start_offset, offset, log, sizeof(SNAPSHOT_LOG_T));
}

static int _SNAPSHOT_SetLogHeader(uint32_t start_offset, SNAPSHOT_LOG_HEADER_T *header)
{
	return _SNAPSHOT_SetLogData(start_offset, 0, header, sizeof(SNAPSHOT_LOG_HEADER_T));
}

static int _SNAPSHOT_SetLogInfo(uint32_t start_offset, SNAPSHOT_LOG_T *log, int idx)
{
	int offset;

	if (0 > idx)
	{
		printf(" %s error %d\n", __FUNCTION__, __LINE__);
		return -1;
	}

	offset = idx * sizeof(SNAPSHOT_LOG_T) + sizeof(SNAPSHOT_LOG_HEADER_T);
	return _SNAPSHOT_SetLogData(start_offset, offset, log, sizeof(SNAPSHOT_LOG_T));
}

static int _SNAPSHOT_MakeLogHeader(SNAPSHOT_LOG_HEADER_T *header)
{
	if (NULL == header)
	{
		return -1;
	}

	if (LOG_MAGIC_NUM != header->magic)
	{ /* first logging */
		header->magic = LOG_MAGIC_NUM;
		header->logCnt = 1;
		header->curSeq = 1;
	}
	else
	{
		header->logCnt = (header->logCnt < MAX_LOG_CNT ? header->logCnt+1 : MAX_LOG_CNT);
		if (0xffff != header->curSeq)
		{
			header->curSeq++;
		}
		else
		{
			header->curSeq = 1;
		}
	}
	return 0;
}

static int _SNAPSHOT_MakeLog(SNAPSHOT_LOG_HEADER_T *header, SNAPSHOT_LOG_T *log,
		SNAPSHOT_LOG_TYPE_T type, unsigned short reason, char *detail, int userMode)
{
	if (NULL == header || NULL == log)
	{
		return -1;
	}

	_SNAPSHOT_MakeLogHeader(header);
	log->type = type;
	log->seqId = header->curSeq;
	log->reason = reason;
	memset(log->logDetail, 0, MAX_LOG_STR_LEN);
	if (NULL != detail)
	{
		strncpy((char *)log->logDetail, (char *)detail, MAX_LOG_STR_LEN);
	}
	return 0;
}

static int _SNAPSHOT_PrintLogHeader(SNAPSHOT_LOG_HEADER_T *header)
{
	printf("****************** HEADER *******************\n");
	printf(" magic:%08x logCnt:%d, curSeq:%d\n", header->magic, header->logCnt, header->curSeq);
	printf("*********************************************\n");

	return 0;
}

static int _SNAPSHOT_PrintLogInfo(SNAPSHOT_LOG_T *info)
{
	if (NULL == info)
		return -1;

	if (SNAPSHOT_LOG_REMAKE_REASON == info->type)
	{
		printf(" %04d:[%20s] %10s:%15s:%s\n", info->seqId, "no time info", typeToStr[info->type],
				remakeReasonToStr[info->reason], info->logDetail);
	}
	else if (SNAPSHOT_LOG_ERROR == info->type)
	{
		printf(" %04d:[%20s] %10s:%15s:%s\n", info->seqId, "no time info", typeToStr[info->type],
				errorReasonToStr[info->reason], info->logDetail);
	}
	else
	{
		printf(" %04d:[%20s] %10s:%04d:%s\n", info->seqId, "no time info", typeToStr[info->type],
				info->reason, info->logDetail);
	}
	printf("*********************************************\n");

	return 0;
}

int SNAPSHOT_Log(SNAPSHOT_LOG_TYPE_T type, unsigned short reason, char *detail, int userMode)
{
	SNAPSHOT_LOG_HEADER_T header;
	SNAPSHOT_LOG_T log;
	uint32_t start_offset;
	memset(&log, 0, sizeof(SNAPSHOT_LOG_T));
	memset(&header, 0, sizeof(SNAPSHOT_LOG_HEADER_T));

	_SNAPSHOT_GetLogStartPos(&start_offset);
	_SNAPSHOT_GetLogHeader(start_offset, &header);
    _SNAPSHOT_MakeLog(&header, &log, type, reason, detail, 0);
    _SNAPSHOT_SetLogHeader(start_offset, &header);
    _SNAPSHOT_SetLogInfo(start_offset, &log, SNAPSHOT_LOG_WRITE_POS(header.curSeq));
	return 0;
}

int SNAPSHOT_PrintLog(void)
{
	SNAPSHOT_LOG_HEADER_T header;
	SNAPSHOT_LOG_T log;
	int i;
	uint32_t start_offset;

    if (0 > _SNAPSHOT_GetLogStartPos(&start_offset))
	{
		return -1;
	}
	if (0 > _SNAPSHOT_GetLogHeader(start_offset, &header))
	{
		return -1;
	}
	_SNAPSHOT_PrintLogHeader(&header);

	for (i = 0; i < header.logCnt; i++)
	{
		if (0 > _SNAPSHOT_GetLogInfo(start_offset, &log, i))
		{
			return -1;
		}
		_SNAPSHOT_PrintLogInfo(&log);
	}

	return 0;
}

int SNAPSHOT_RemoveLogHeader(void)
{
	SNAPSHOT_LOG_HEADER_T header;
	uint32_t start_offset;

	memset(&header, 0, sizeof(SNAPSHOT_LOG_HEADER_T));
	_SNAPSHOT_GetLogStartPos(&start_offset);
	_SNAPSHOT_SetLogHeader(start_offset, &header);

	return 0;
}
