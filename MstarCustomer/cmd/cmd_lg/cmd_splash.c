/******************************************************************************
 *   DTV LABORATORY, LG ELECTRONICS INC., SEOUL, KOREA
 *   Copyright(c) 2009 by LG Electronics Inc.
 *
 *   All rights reserved. No part of this work may be reproduced, stored in a
 *   retrieval system, or transmitted by any means without prior written
 *   permission of LG Electronics Inc.
 *****************************************************************************/

/** @file cmd_splash.c
 *
 *  This c file defines the functions to display splash window(logo window)
 *
 *  @author     haeyong.pyun@lge.com
 *  @version    1.0
 *  @date       2011.06.28
 *  @note
 *  @see        cmd_splash.c
 */

/*-------------------------------------------------------------
 * include files
 *------------------------------------------------------------*/
#include <common.h>
#include <x_typedef.h>
#include <splash_auxinfo_bmp.h>
#include <cmd_splash.h>
#include <partinfo.h>
#include <cmd_polltimer.h>
#include <cmd_micom.h>
#include <cmnio_type.h>
#include <CusCmnio.h>
#include <drvGPIO.h>
#include <thread.h>
//#include "x_ckgen.h"

#include <asm/mmapger.h>
#include <gop/MsDrvGop.h>


/******************************************************************************
  전역 형 정의 (Global Type Definitions)
 ******************************************************************************/
#define     _RETRY_URSA3_WRITE  3
#define     SYS_ATSC    (0)
#define     SYS_DVB     (1)
#define     _RETRY_MICOM    3

#define NUM_TOOL_OPTION (9)
#define USER_DATA_DELAY (10)

#if NR_CPUS > 2
#define USE_LOGO_THREAD
#endif

#define MICOM_TYPE  DDI_CMNIO_GetMicomType()
#define GPIO_PORT_PANEL_CTL         13
#define GPIO_PORT_INV_CTL           168

/******************************************************************************
  함수 prototype 선언
  (Function Prototypes Declarations)
 ******************************************************************************/
#ifndef NEW_BOOT_LOGO
extern void Splash_GetImageInfo(SPLASH_BMP_TYPE_T bmpType, SPLASH_BMP_INFO_T *pSplashBmpInfo, void *argv, unsigned char systype );
#endif
extern void Splash_MICOM_GetRTC( TIME_T *pTime );
extern void Splash_DrawImage(unsigned int x, unsigned int y, unsigned int u4Width, unsigned int u4Height, unsigned int u4BmpAddr, int panelRes);
extern int read_blocks_with_bg_task (off_t ofs, size_t *len, u_char *buf, void (*bg_task(BGTASK_POWSEQ_FLAG)));
extern UINT8 DDI_NVM_GetSoundOutMode( void );
thread_t *thread[2];

/******************************************************************************
  변수 선언
  (Variables)
 ******************************************************************************/
UINT32 gToolOpt[NUM_TOOL_OPTION] = {0, };
extern MODELOPT_T   gModelOpt;
extern MMapInfo_t MMAPGer[MMAP_ID_MAX];
extern MiuInfo_t  MiuInfo;

/*
   URSA3 Configure of each LCD model size(inch)
 */
typedef struct
{
    UINT8   inch;
    UINT8   data[3];

} URSA3_CONF_INCH_T;

typedef enum
{
    COUNTRY_TW      =0,
    COUNTRY_COL     =1,
    COUNTRY_US      =2,
    COUNTRY_CN      =3,
    COUNTRY_HK      =4,
    COUNTRY_KR      =5,
    COUNTRY_JP      =6,
    COUNTRY_EU      =7,
    COUNTRY_BR      =8,
    COUNTRY_AJJA    =9,
    COUNTR_CI       =10,
} MODEL_COUNTRY_T;


typedef enum
{
    TUNER_TC                =0,
    TUNER_ATV_INT_DTV_EXT   =1,
    TUNER_ATSC_PIP          =2,
    TUNER_ATV_INT_DTV_INT   =3,
    TUNER_T2CS2_ATV_EXT     =4,
    TUNER_T2C_PIP           =5,
    TUNER_ATC_SOC           =6,
    TUNER_T2C               =7,
    TUNER_ATV_EXT           =8,
    TUNER_T2CS2_AT          =9,
    TUNER_DEFAULT           =11,
} MODEL_TUNER_T;


typedef struct
{
    MODEL_COUNTRY_T opt_country;
    MODEL_TUNER_T opt_tuner;
    UINT8 vx1_type; //
    UINT8 panel_res;//FHD/UHD
    UINT8 ursa9_support; // URSA9 Support/ URSA9 Not Support
    UINT8 res2;
    UINT8 res3;
} MODEL_OPT_RES_T;


uint8_t gDispType = 0xff;
unsigned short __InvertOn = 0; //for checking whether Invert is on
static UINT32   _gModelOptions;

extern char strModelOpt[];

int Get_modelOpt(HW_OPT_T option_mask)
{
    /***************************************
     ************** HW OPTION ***************
     ****************************************/

    // pModelOpt[0],pModelOpt[1]
    // DVB              ATSC        JP
    // 0,0 : TW/COL         US
    // 0,1 : CN/HK      KR          JP
    // 1,0 : EU             BR
    // 1,1 : AJJA           CI

    // pModelOpt[2],pModelOpt[3]
    // EU/CIS                   AJJA            TW/COL          CN/HK       KR                  North.AM            BR          JP
    // 0,0 : T/C                    T/C             T/C                 Default         ATSC_PIP            ATSC_PIP        ISDB_PIP    Default
    // 0,1 : T2/C/S2/ATV_EXT        T2/C_PIP    T2/C_PIP                    ATC_SOC ATV_SOC     ISDB
    // 1,0 : T2/C                   T2/C        T2                          ATV_EXT             ATV_EXT
    // 1,1 : T2/C/S2/AT             T2/C/S2

    // pModelOpt[4]
    // 0 : Display LCD
    // 1 : Display OLED

    // pModelOpt[5]
    // 0 : Resolution FHD
    // 1 : Resolution UHD

    // pModelOpt[6] : Reserved
    // pModelOpt[7] : Reserved
    // pModelOpt[8] : Reserved
    switch(option_mask)
    {
        case PANEL_RES_OPT_SEL:
            {
                if(gModelOpt.panel_resolution == MODELOPT_PANEL_RESOLUTION_UD)
                {
                    return 1;
                }
                else
                {
                    return 0;
                }
            }
            break;

        case DISPLAY_TYPE_OPT_SEL:
            {
                if(gModelOpt.bSupportOptic == '1')
                {
                    return 1; //OLED
                }
                else
                {
                    return 0; // LCD
                }
            }
            break;
        default:
            return -1;
            break;
    }
}

int is_OLED_mode(void)
{
    if(Get_modelOpt(DISPLAY_TYPE_OPT_SEL) == MODELOPT_DISPLAY_OLED)
    {
        return 1;
    }
    else
    {
        return 0;
    }
}

void Splash_MICOM_GetRTC( TIME_T *pTime )
{
    UINT8   i;
    TIME_T  tTime;
    const TIME_T    tInvalidTime = { 0, };

    *pTime = tInvalidTime;

    for( i = 0; i < _RETRY_MICOM; i ++ )
    {
        if( MICOM_GetRTStime( &tTime ) == 0 )
        {
            if(     (tTime.month  < 13)
                    &&  (tTime.day    < 32)
                    &&  (tTime.hour   < 24)
                    &&  (tTime.minute < 60)
                    &&  (tTime.second < 61) )
            {
                *pTime = tTime;
                break;
            }
        }
        udelay(10000);
    }

    //printf("[%d,%d,%d] [%d,%d,%d] \n",pTime->year,pTime->month,pTime->day,pTime->hour,pTime->minute,pTime->second);
}

void Splash_MICOM_PanelOn( void )
{
    if(MICOM_TYPE)
    {
        CMNIO_GPIO_SetOutputPort(GPIO_PORT_PANEL_CTL, 1);
    }
    else
    {
        UINT8   i;
        UINT8   panel_on = 0;

        for( i = 0; i < _RETRY_MICOM; i ++ )
        {
            MICOM_TurnOnPanel();
            udelay(10000);
            MICOM_VerifyPanelOn( &panel_on );
            if( panel_on == 1)  break;

            udelay(50000);
        }

        if( ! panel_on )
        {
            printf("turn on PANEL: FAILED\n");
        }
    }
}

void Splash_MICOM_InvOn( void )
{

    UINT8   i;
    UINT8   inv_on = 0;

    for( i = 0; i < _RETRY_MICOM; i ++ )
    {
        MICOM_TurnOnInv();
        MICOM_SetMSModeDuty(60);
        udelay(15000);
        MICOM_VerifyInvertOn( &inv_on );

        if( inv_on == 1)    break;

        udelay(50000);
    }

    if( ! inv_on )
    {
        printf("turn on INV: FAILED\n");
    }
}

void Splash_MICOM_Delayed_InvOn( unsigned char delay )
{
    UINT8 i;
    int   check = -1;

    for( i = 0; i < _RETRY_MICOM; i++ )
    {
        check = MICOM_Delayed_InvertOn(delay);

        if( check == 0 )    break;
        else                udelay(50000);
    }

    if( check != 0 )
        printf("turn on Delayed_INV: FAILED\n");
}

void Splash_MICOM_LongDelayed_InvOn( unsigned char delay1, unsigned char delay2 )
{
    UINT8 i;
    int   check = -1;

    for( i = 0; i < _RETRY_MICOM; i++ )
    {
        MICOM_SetDelayedMSModeDuty(60, delay1, delay2);
        udelay(15000);
        check = MICOM_LongDelayed_InvertOn(delay1, delay2);

        if( check == 0 )    break;
        else                udelay(50000);
    }
    if( check != 0 )
        printf("turn on LongDelayed_INV: FAILED\n");
}

void Splash_DrawLogoImage(unsigned int loadaddr, unsigned char systype)
{
    SPLASH_BMP_INFO_T   splash_bmp_info;
    TIME_T              curRTCTime;

    TIME_T *pTimeSt;
    int time;
    unsigned char bIsTimeInvalid = FALSE;
    unsigned char hour;

#ifdef NEW_BOOT_LOGO
    Splash_DrawImage(0, 0, 0, 0, loadaddr, Get_modelOpt(PANEL_RES_OPT_SEL));
#else
    printf("DRAW OLD BOOT LOGO \n");
    /*-----------------------------------------------
     * Get RTC time & Display time
     *-----------------------------------------------*/
    Splash_MICOM_GetRTC( &curRTCTime );

    pTimeSt = (TIME_T*)&curRTCTime;
    hour = pTimeSt->hour;

    if( (pTimeSt != NULL) &&
            (pTimeSt->year == 0 || pTimeSt->month == 0 || pTimeSt->day == 0) )
    {
        bIsTimeInvalid = TRUE;
    }

    Splash_GetImageInfo(SPLASH_LG_LOG_BMP, &splash_bmp_info, (void *)&curRTCTime, systype);
    Splash_DrawImage(splash_bmp_info.xPos, splash_bmp_info.yPos, splash_bmp_info.w, splash_bmp_info.h, (loadaddr + splash_bmp_info.offset) );

    Splash_GetImageInfo(SPLASH_BG_GRAPHIC_BMP, &splash_bmp_info, (void *)&curRTCTime, systype);
    Splash_DrawImage(splash_bmp_info.xPos, splash_bmp_info.yPos, splash_bmp_info.w, splash_bmp_info.h, (loadaddr + splash_bmp_info.offset) );

    Splash_GetImageInfo(SPLASH_CLOCK_ICON_BMP, &splash_bmp_info, (void *)&curRTCTime, systype);
    if (systype == SYS_ATSC && !bIsTimeInvalid)
        splash_bmp_info.xPos = splash_bmp_info.xPos + 394;
    Splash_DrawImage(splash_bmp_info.xPos, splash_bmp_info.yPos, splash_bmp_info.w, splash_bmp_info.h, (loadaddr + splash_bmp_info.offset) );

    Splash_GetImageInfo(SPLASH_TIME_COLON_BMP, &splash_bmp_info, (void *)&curRTCTime, systype);
    if(systype == SYS_ATSC && !bIsTimeInvalid)
        splash_bmp_info.xPos = splash_bmp_info.xPos - 166;
    Splash_DrawImage(splash_bmp_info.xPos, splash_bmp_info.yPos, splash_bmp_info.w, splash_bmp_info.h, (loadaddr + splash_bmp_info.offset) );

    Splash_GetImageInfo(SPLASH_TIME_HH_BMP, &splash_bmp_info, (void *)&curRTCTime, systype);
    if(systype == SYS_ATSC && !bIsTimeInvalid)
        splash_bmp_info.xPos = splash_bmp_info.xPos - 166;
    Splash_DrawImage(splash_bmp_info.xPos, splash_bmp_info.yPos, splash_bmp_info.w, splash_bmp_info.h, (loadaddr + splash_bmp_info.offset) );

    Splash_GetImageInfo(SPLASH_TIME_H_BMP, &splash_bmp_info, (void *)&curRTCTime, systype);
    if(systype == SYS_ATSC && !bIsTimeInvalid)
        splash_bmp_info.xPos = splash_bmp_info.xPos - 166;
    Splash_DrawImage(splash_bmp_info.xPos, splash_bmp_info.yPos, splash_bmp_info.w, splash_bmp_info.h, (loadaddr + splash_bmp_info.offset) );

    Splash_GetImageInfo(SPLASH_TIME_MM_BMP, &splash_bmp_info, (void *)&curRTCTime, systype);
    if(systype == SYS_ATSC && !bIsTimeInvalid)
        splash_bmp_info.xPos = splash_bmp_info.xPos - 166;
    Splash_DrawImage(splash_bmp_info.xPos, splash_bmp_info.yPos, splash_bmp_info.w, splash_bmp_info.h, (loadaddr + splash_bmp_info.offset) );

    Splash_GetImageInfo(SPLASH_TIME_M_BMP, &splash_bmp_info, (void *)&curRTCTime, systype);
    if(systype == SYS_ATSC && !bIsTimeInvalid)
        splash_bmp_info.xPos = splash_bmp_info.xPos - 166;
    Splash_DrawImage(splash_bmp_info.xPos, splash_bmp_info.yPos, splash_bmp_info.w, splash_bmp_info.h, (loadaddr + splash_bmp_info.offset) );
#endif
}

#ifndef NEW_BOOT_LOGO
void Splash_GetImageInfo(SPLASH_BMP_TYPE_T bmpType, SPLASH_BMP_INFO_T *pSplashBmpInfo, void *argv, unsigned char systype )
{
    TIME_T *pTimeSt;
    int time;
    unsigned char bIsTimeInvalid = FALSE;
    unsigned char bIsPM = FALSE;
    unsigned char hour;

    pTimeSt = (TIME_T*)argv;
    hour = pTimeSt->hour;

    if( (pTimeSt != NULL) &&
            (pTimeSt->year == 0 || pTimeSt->month == 0 || pTimeSt->day == 0) )
    {
        bIsTimeInvalid = TRUE;
    }
    else
    {
        if(systype == SYS_ATSC)
        {
            if( hour >= 12 )
                bIsPM = TRUE;

            hour %= 12;
            if( hour == 0 )
                hour+=12;
        }
    }

    switch ( bmpType )
    {
        case SPLASH_BG_BOOTING_BMP:
            pSplashBmpInfo->offset  = BG_LOGO_OFFSET;
            pSplashBmpInfo->size    = BG_LOGO_SIZE;
            pSplashBmpInfo->xPos    = BG_LOGO_X_POS;
            pSplashBmpInfo->yPos    = BG_LOGO_Y_POS;
            pSplashBmpInfo->w       = BG_LOGO_WIDTH;
            pSplashBmpInfo->h       = BG_LOGO_HEIGHT;
            break;

        case SPLASH_BG_GRAPHIC_BMP:
            pSplashBmpInfo->offset  = BG_GRAPHIC_OFFSET;
            pSplashBmpInfo->size    = BG_GRAPHIC_SIZE;
            pSplashBmpInfo->xPos    = BG_GRAPHIC_X_POS;
            pSplashBmpInfo->yPos    = BG_GRAPHIC_Y_POS;
            pSplashBmpInfo->w       = BG_GRAPHIC_WIDTH;
            pSplashBmpInfo->h       = BG_GRAPHIC_HEIGHT;
            break;

        case SPLASH_LG_LOG_BMP:
            pSplashBmpInfo->offset  = BG_LOGO_OFFSET;
            pSplashBmpInfo->size    = BG_LOGO_SIZE;
            pSplashBmpInfo->xPos    = BG_LOGO_X_POS;
            pSplashBmpInfo->yPos    = BG_LOGO_Y_POS;
            pSplashBmpInfo->w       = BG_LOGO_WIDTH;
            pSplashBmpInfo->h       = BG_LOGO_HEIGHT;
            break;

        case SPLASH_CLOCK_ICON_BMP:
            if(systype == SYS_ATSC)
            {
                if(!bIsTimeInvalid)
                {
                    if(!bIsPM)
                    {
                        pSplashBmpInfo->offset  = TIME_AM_OFFSET;
                        pSplashBmpInfo->size    = TIME_AM_SIZE;
                        pSplashBmpInfo->xPos    = TIME_AM_X_POS;
                        pSplashBmpInfo->yPos    = TIME_AM_Y_POS;
                        pSplashBmpInfo->w       = TIME_AM_WIDTH;
                        pSplashBmpInfo->h       = TIME_AM_HEIGHT;
                    }
                    else
                    {
                        pSplashBmpInfo->offset  = TIME_PM_OFFSET;
                        pSplashBmpInfo->size    = TIME_PM_SIZE;
                        pSplashBmpInfo->xPos    = TIME_PM_X_POS;
                        pSplashBmpInfo->yPos    = TIME_PM_Y_POS;
                        pSplashBmpInfo->w       = TIME_PM_WIDTH;
                        pSplashBmpInfo->h       = TIME_PM_HEIGHT;
                    }
                }
                else
                {
                    pSplashBmpInfo->offset  = CLOCK_ICON_OFFSET;
                    pSplashBmpInfo->size    = CLOCK_ICON_SIZE;
                    pSplashBmpInfo->xPos    = CLOCK_ICON_X_POS;
                    pSplashBmpInfo->yPos    = CLOCK_ICON_Y_POS;
                    pSplashBmpInfo->w       = CLOCK_ICON_WIDTH;
                    pSplashBmpInfo->h       = CLOCK_ICON_HEIGHT;
                }
            }
            else
            {
                pSplashBmpInfo->offset  = CLOCK_ICON_OFFSET;
                pSplashBmpInfo->size    = CLOCK_ICON_SIZE;
                pSplashBmpInfo->xPos    = CLOCK_ICON_X_POS;
                pSplashBmpInfo->yPos    = CLOCK_ICON_Y_POS;
                pSplashBmpInfo->w       = CLOCK_ICON_WIDTH;
                pSplashBmpInfo->h       = CLOCK_ICON_HEIGHT;
            }
            break;

        case SPLASH_TIME_HH_BMP:
            if( bIsTimeInvalid == TRUE )
            {
                pSplashBmpInfo->offset  = INVALID_01_OFFSET;
                pSplashBmpInfo->size    = INVALID_01_SIZE;
                pSplashBmpInfo->xPos    = INVALID_01_X_POS;
                pSplashBmpInfo->yPos    = INVALID_01_Y_POS;
                pSplashBmpInfo->w       = INVALID_01_WIDTH;
                pSplashBmpInfo->h       = INVALID_01_HEIGHT;
                break;
            }

            time = hour/10; //pTimeSt->hour/10;
            if( time == 0 )
            {
                pSplashBmpInfo->offset  = TIME_HH_00_OFFSET;
                pSplashBmpInfo->size    = TIME_HH_00_SIZE;
                pSplashBmpInfo->xPos    = TIME_HH_00_X_POS;
                pSplashBmpInfo->yPos    = TIME_HH_00_Y_POS;
                pSplashBmpInfo->w       = TIME_HH_00_WIDTH;
                pSplashBmpInfo->h       = TIME_HH_00_HEIGHT;
            }
            else if( time == 1 )
            {
                pSplashBmpInfo->offset  = TIME_HH_10_OFFSET;
                pSplashBmpInfo->size    = TIME_HH_10_SIZE;
                pSplashBmpInfo->xPos    = TIME_HH_10_X_POS;
                pSplashBmpInfo->yPos    = TIME_HH_10_Y_POS;
                pSplashBmpInfo->w       = TIME_HH_10_WIDTH;
                pSplashBmpInfo->h       = TIME_HH_10_HEIGHT;
            }
            else if( time == 2 )
            {
                pSplashBmpInfo->offset  = TIME_HH_20_OFFSET;
                pSplashBmpInfo->size    = TIME_HH_20_SIZE;
                pSplashBmpInfo->xPos    = TIME_HH_20_X_POS;
                pSplashBmpInfo->yPos    = TIME_HH_20_Y_POS;
                pSplashBmpInfo->w       = TIME_HH_20_WIDTH;
                pSplashBmpInfo->h       = TIME_HH_20_HEIGHT;
            }
            else
            {
                pSplashBmpInfo->offset  = TIME_HH_00_OFFSET;
                pSplashBmpInfo->size    = TIME_HH_00_SIZE;
                pSplashBmpInfo->xPos    = TIME_HH_00_X_POS;
                pSplashBmpInfo->yPos    = TIME_HH_00_Y_POS;
                pSplashBmpInfo->w       = TIME_HH_00_WIDTH;
                pSplashBmpInfo->h       = TIME_HH_00_HEIGHT;
            }
            break;

        case SPLASH_TIME_H_BMP:
            if( bIsTimeInvalid == TRUE )
            {
                pSplashBmpInfo->offset  = INVALID_02_OFFSET;
                pSplashBmpInfo->size    = INVALID_02_SIZE;
                pSplashBmpInfo->xPos    = INVALID_02_X_POS;
                pSplashBmpInfo->yPos    = INVALID_02_Y_POS;
                pSplashBmpInfo->w       = INVALID_02_WIDTH;
                pSplashBmpInfo->h       = INVALID_02_HEIGHT;
                break;
            }

            time = hour%10; //pTimeSt->hour%10;
            if( time == 0 )
            {
                pSplashBmpInfo->offset  = TIME_HH_0_OFFSET;
                pSplashBmpInfo->size    = TIME_HH_0_SIZE;
                pSplashBmpInfo->xPos    = TIME_HH_0_X_POS;
                pSplashBmpInfo->yPos    = TIME_HH_0_Y_POS;
                pSplashBmpInfo->w       = TIME_HH_0_WIDTH;
                pSplashBmpInfo->h       = TIME_HH_0_HEIGHT;
            }
            else if( time == 1 )
            {
                pSplashBmpInfo->offset  = TIME_HH_1_OFFSET;
                pSplashBmpInfo->size    = TIME_HH_1_SIZE;
                pSplashBmpInfo->xPos    = TIME_HH_1_X_POS;
                pSplashBmpInfo->yPos    = TIME_HH_1_Y_POS;
                pSplashBmpInfo->w       = TIME_HH_1_WIDTH;
                pSplashBmpInfo->h       = TIME_HH_1_HEIGHT;
            }
            else if( time == 2 )
            {
                pSplashBmpInfo->offset  = TIME_HH_2_OFFSET;
                pSplashBmpInfo->size    = TIME_HH_2_SIZE;
                pSplashBmpInfo->xPos    = TIME_HH_2_X_POS;
                pSplashBmpInfo->yPos    = TIME_HH_2_Y_POS;
                pSplashBmpInfo->w       = TIME_HH_2_WIDTH;
                pSplashBmpInfo->h       = TIME_HH_2_HEIGHT;
            }
            else if( time == 3 )
            {
                pSplashBmpInfo->offset  = TIME_HH_3_OFFSET;
                pSplashBmpInfo->size    = TIME_HH_3_SIZE;
                pSplashBmpInfo->xPos    = TIME_HH_3_X_POS;
                pSplashBmpInfo->yPos    = TIME_HH_3_Y_POS;
                pSplashBmpInfo->w       = TIME_HH_3_WIDTH;
                pSplashBmpInfo->h       = TIME_HH_3_HEIGHT;
            }
            else if( time == 4 )
            {
                pSplashBmpInfo->offset  = TIME_HH_4_OFFSET;
                pSplashBmpInfo->size    = TIME_HH_4_SIZE;
                pSplashBmpInfo->xPos    = TIME_HH_4_X_POS;
                pSplashBmpInfo->yPos    = TIME_HH_4_Y_POS;
                pSplashBmpInfo->w       = TIME_HH_4_WIDTH;
                pSplashBmpInfo->h       = TIME_HH_4_HEIGHT;
            }
            else if( time == 5 )
            {
                pSplashBmpInfo->offset  = TIME_HH_5_OFFSET;
                pSplashBmpInfo->size    = TIME_HH_5_SIZE;
                pSplashBmpInfo->xPos    = TIME_HH_5_X_POS;
                pSplashBmpInfo->yPos    = TIME_HH_5_Y_POS;
                pSplashBmpInfo->w       = TIME_HH_5_WIDTH;
                pSplashBmpInfo->h       = TIME_HH_5_HEIGHT;
            }
            else if( time == 6 )
            {
                pSplashBmpInfo->offset  = TIME_HH_6_OFFSET;
                pSplashBmpInfo->size    = TIME_HH_6_SIZE;
                pSplashBmpInfo->xPos    = TIME_HH_6_X_POS;
                pSplashBmpInfo->yPos    = TIME_HH_6_Y_POS;
                pSplashBmpInfo->w       = TIME_HH_6_WIDTH;
                pSplashBmpInfo->h       = TIME_HH_6_HEIGHT;
            }
            else if( time == 7 )
            {
                pSplashBmpInfo->offset  = TIME_HH_7_OFFSET;
                pSplashBmpInfo->size    = TIME_HH_7_SIZE;
                pSplashBmpInfo->xPos    = TIME_HH_7_X_POS;
                pSplashBmpInfo->yPos    = TIME_HH_7_Y_POS;
                pSplashBmpInfo->w       = TIME_HH_7_WIDTH;
                pSplashBmpInfo->h       = TIME_HH_7_HEIGHT;
            }
            else if( time == 8 )
            {
                pSplashBmpInfo->offset  = TIME_HH_8_OFFSET;
                pSplashBmpInfo->size    = TIME_HH_8_SIZE;
                pSplashBmpInfo->xPos    = TIME_HH_8_X_POS;
                pSplashBmpInfo->yPos    = TIME_HH_8_Y_POS;
                pSplashBmpInfo->w       = TIME_HH_8_WIDTH;
                pSplashBmpInfo->h       = TIME_HH_8_HEIGHT;
            }
            else if( time == 9 )
            {
                pSplashBmpInfo->offset  = TIME_HH_9_OFFSET;
                pSplashBmpInfo->size    = TIME_HH_9_SIZE;
                pSplashBmpInfo->xPos    = TIME_HH_9_X_POS;
                pSplashBmpInfo->yPos    = TIME_HH_9_Y_POS;
                pSplashBmpInfo->w       = TIME_HH_9_WIDTH;
                pSplashBmpInfo->h       = TIME_HH_9_HEIGHT;
            }
            else
            {
                pSplashBmpInfo->offset  = TIME_HH_0_OFFSET;
                pSplashBmpInfo->size    = TIME_HH_0_SIZE;
                pSplashBmpInfo->xPos    = TIME_HH_0_X_POS;
                pSplashBmpInfo->yPos    = TIME_HH_0_Y_POS;
                pSplashBmpInfo->w       = TIME_HH_0_WIDTH;
                pSplashBmpInfo->h       = TIME_HH_0_HEIGHT;
            }
            break;

        case SPLASH_TIME_COLON_BMP:
            pSplashBmpInfo->offset  = TIME_COLON_OFFSET;
            pSplashBmpInfo->size    = TIME_COLON_SIZE;
            pSplashBmpInfo->xPos    = TIME_COLON_X_POS;
            pSplashBmpInfo->yPos    = TIME_COLON_Y_POS;
            pSplashBmpInfo->w   = TIME_COLON_WIDTH;
            pSplashBmpInfo->h   = TIME_COLON_HEIGHT;
            break;

        case SPLASH_TIME_MM_BMP:
            if( bIsTimeInvalid == TRUE )
            {
                pSplashBmpInfo->offset  = INVALID_03_OFFSET;
                pSplashBmpInfo->size    = INVALID_03_SIZE;
                pSplashBmpInfo->xPos    = INVALID_03_X_POS;
                pSplashBmpInfo->yPos    = INVALID_03_Y_POS;
                pSplashBmpInfo->w   = INVALID_03_WIDTH;
                pSplashBmpInfo->h   = INVALID_03_HEIGHT;
                break;
            }

            time = pTimeSt->minute/10;

            if( time == 0 )
            {
                pSplashBmpInfo->offset  = TIME_MM_00_OFFSET;
                pSplashBmpInfo->size    = TIME_MM_00_SIZE;
                pSplashBmpInfo->xPos    = TIME_MM_00_X_POS;
                pSplashBmpInfo->yPos    = TIME_MM_00_Y_POS;
                pSplashBmpInfo->w   = TIME_MM_00_WIDTH;
                pSplashBmpInfo->h   = TIME_MM_00_HEIGHT;
            }
            else if( time == 1 )
            {
                pSplashBmpInfo->offset  = TIME_MM_10_OFFSET;
                pSplashBmpInfo->size    = TIME_MM_10_SIZE;
                pSplashBmpInfo->xPos    = TIME_MM_10_X_POS;
                pSplashBmpInfo->yPos    = TIME_MM_10_Y_POS;
                pSplashBmpInfo->w   = TIME_MM_10_WIDTH;
                pSplashBmpInfo->h   = TIME_MM_10_HEIGHT;
            }
            else if( time == 2 )
            {
                pSplashBmpInfo->offset  = TIME_MM_20_OFFSET;
                pSplashBmpInfo->size    = TIME_MM_20_SIZE;
                pSplashBmpInfo->xPos    = TIME_MM_20_X_POS;
                pSplashBmpInfo->yPos    = TIME_MM_20_Y_POS;
                pSplashBmpInfo->w   = TIME_MM_20_WIDTH;
                pSplashBmpInfo->h   = TIME_MM_20_HEIGHT;
            }
            else if( time == 3 )
            {
                pSplashBmpInfo->offset  = TIME_MM_30_OFFSET;
                pSplashBmpInfo->size    = TIME_MM_30_SIZE;
                pSplashBmpInfo->xPos    = TIME_MM_30_X_POS;
                pSplashBmpInfo->yPos    = TIME_MM_30_Y_POS;
                pSplashBmpInfo->w   = TIME_MM_30_WIDTH;
                pSplashBmpInfo->h   = TIME_MM_30_HEIGHT;
            }
            else if( time == 4 )
            {
                pSplashBmpInfo->offset  = TIME_MM_40_OFFSET;
                pSplashBmpInfo->size    = TIME_MM_40_SIZE;
                pSplashBmpInfo->xPos    = TIME_MM_40_X_POS;
                pSplashBmpInfo->yPos    = TIME_MM_40_Y_POS;
                pSplashBmpInfo->w   = TIME_MM_40_WIDTH;
                pSplashBmpInfo->h   = TIME_MM_40_HEIGHT;
            }
            else if( time == 5 )
            {
                pSplashBmpInfo->offset  = TIME_MM_50_OFFSET;
                pSplashBmpInfo->size    = TIME_MM_50_SIZE;
                pSplashBmpInfo->xPos    = TIME_MM_50_X_POS;
                pSplashBmpInfo->yPos    = TIME_MM_50_Y_POS;
                pSplashBmpInfo->w   = TIME_MM_50_WIDTH;
                pSplashBmpInfo->h   = TIME_MM_50_HEIGHT;
            }
            else
            {
                pSplashBmpInfo->offset  = TIME_MM_00_OFFSET;
                pSplashBmpInfo->size    = TIME_MM_00_SIZE;
                pSplashBmpInfo->xPos    = TIME_MM_00_X_POS;
                pSplashBmpInfo->yPos    = TIME_MM_00_Y_POS;
                pSplashBmpInfo->w   = TIME_MM_00_WIDTH;
                pSplashBmpInfo->h   = TIME_MM_00_HEIGHT;
            }
            break;

        case SPLASH_TIME_M_BMP:
            if( bIsTimeInvalid == TRUE )
            {
                pSplashBmpInfo->offset  = INVALID_04_OFFSET;
                pSplashBmpInfo->size    = INVALID_04_SIZE;
                pSplashBmpInfo->xPos    = INVALID_04_X_POS;
                pSplashBmpInfo->yPos    = INVALID_04_Y_POS;
                pSplashBmpInfo->w   = INVALID_04_WIDTH;
                pSplashBmpInfo->h   = INVALID_04_HEIGHT;
                break;
            }

            time = pTimeSt->minute%10;

            if( time == 0 )
            {
                pSplashBmpInfo->offset  = TIME_MM_0_OFFSET;
                pSplashBmpInfo->size    = TIME_MM_0_SIZE;
                pSplashBmpInfo->xPos    = TIME_MM_0_X_POS;
                pSplashBmpInfo->yPos    = TIME_MM_0_Y_POS;
                pSplashBmpInfo->w   = TIME_MM_0_WIDTH;
                pSplashBmpInfo->h   = TIME_MM_0_HEIGHT;

            }
            else if( time == 1 )
            {
                pSplashBmpInfo->offset  = TIME_MM_1_OFFSET;
                pSplashBmpInfo->size    = TIME_MM_1_SIZE;
                pSplashBmpInfo->xPos    = TIME_MM_1_X_POS;
                pSplashBmpInfo->yPos    = TIME_MM_1_Y_POS;
                pSplashBmpInfo->w   = TIME_MM_1_WIDTH;
                pSplashBmpInfo->h   = TIME_MM_1_HEIGHT;
            }
            else if( time == 2 )
            {
                pSplashBmpInfo->offset  = TIME_MM_2_OFFSET;
                pSplashBmpInfo->size    = TIME_MM_2_SIZE;
                pSplashBmpInfo->xPos    = TIME_MM_2_X_POS;
                pSplashBmpInfo->yPos    = TIME_MM_2_Y_POS;
                pSplashBmpInfo->w   = TIME_MM_2_WIDTH;
                pSplashBmpInfo->h   = TIME_MM_2_HEIGHT;
            }
            else if( time == 3 )
            {
                pSplashBmpInfo->offset  = TIME_MM_3_OFFSET;
                pSplashBmpInfo->size    = TIME_MM_3_SIZE;
                pSplashBmpInfo->xPos    = TIME_MM_3_X_POS;
                pSplashBmpInfo->yPos    = TIME_MM_3_Y_POS;
                pSplashBmpInfo->w   = TIME_MM_3_WIDTH;
                pSplashBmpInfo->h   = TIME_MM_3_HEIGHT;
            }
            else if( time == 4 )
            {
                pSplashBmpInfo->offset  = TIME_MM_4_OFFSET;
                pSplashBmpInfo->size    = TIME_MM_4_SIZE;
                pSplashBmpInfo->xPos    = TIME_MM_4_X_POS;
                pSplashBmpInfo->yPos    = TIME_MM_4_Y_POS;
                pSplashBmpInfo->w   = TIME_MM_4_WIDTH;
                pSplashBmpInfo->h   = TIME_MM_4_HEIGHT;
            }
            else if( time == 5 )
            {
                pSplashBmpInfo->offset  = TIME_MM_5_OFFSET;
                pSplashBmpInfo->size    = TIME_MM_5_SIZE;
                pSplashBmpInfo->xPos    = TIME_MM_5_X_POS;
                pSplashBmpInfo->yPos    = TIME_MM_5_Y_POS;
                pSplashBmpInfo->w   = TIME_MM_5_WIDTH;
                pSplashBmpInfo->h   = TIME_MM_5_HEIGHT;
            }
            else if( time == 6 )
            {
                pSplashBmpInfo->offset  = TIME_MM_6_OFFSET;
                pSplashBmpInfo->size    = TIME_MM_6_SIZE;
                pSplashBmpInfo->xPos    = TIME_MM_6_X_POS;
                pSplashBmpInfo->yPos    = TIME_MM_6_Y_POS;
                pSplashBmpInfo->w   = TIME_MM_6_WIDTH;
                pSplashBmpInfo->h   = TIME_MM_6_HEIGHT;
            }
            else if( time == 7 )
            {
                pSplashBmpInfo->offset  = TIME_MM_7_OFFSET;
                pSplashBmpInfo->size    = TIME_MM_7_SIZE;
                pSplashBmpInfo->xPos    = TIME_MM_7_X_POS;
                pSplashBmpInfo->yPos    = TIME_MM_7_Y_POS;
                pSplashBmpInfo->w   = TIME_MM_7_WIDTH;
                pSplashBmpInfo->h   = TIME_MM_7_HEIGHT;
            }
            else if( time == 8 )
            {
                pSplashBmpInfo->offset  = TIME_MM_8_OFFSET;
                pSplashBmpInfo->size    = TIME_MM_8_SIZE;
                pSplashBmpInfo->xPos    = TIME_MM_8_X_POS;
                pSplashBmpInfo->yPos    = TIME_MM_8_Y_POS;
                pSplashBmpInfo->w   = TIME_MM_8_WIDTH;
                pSplashBmpInfo->h   = TIME_MM_8_HEIGHT;
            }
            else if( time == 9 )
            {
                pSplashBmpInfo->offset  = TIME_MM_9_OFFSET;
                pSplashBmpInfo->size    = TIME_MM_9_SIZE;
                pSplashBmpInfo->xPos    = TIME_MM_9_X_POS;
                pSplashBmpInfo->yPos    = TIME_MM_9_Y_POS;
                pSplashBmpInfo->w   = TIME_MM_9_WIDTH;
                pSplashBmpInfo->h   = TIME_MM_9_HEIGHT;
            }
            else
            {
                pSplashBmpInfo->offset  = TIME_MM_0_OFFSET;
                pSplashBmpInfo->size    = TIME_MM_0_SIZE;
                pSplashBmpInfo->xPos    = TIME_MM_0_X_POS;
                pSplashBmpInfo->yPos    = TIME_MM_0_Y_POS;
                pSplashBmpInfo->w   = TIME_MM_0_WIDTH;
                pSplashBmpInfo->h   = TIME_MM_0_HEIGHT;
            }
            break;

        default :
            pSplashBmpInfo->offset  = 0;
            pSplashBmpInfo->size    = 0;
            pSplashBmpInfo->xPos    = 0;
            pSplashBmpInfo->yPos    = 0;
    }

    return;
}
#endif

void Splash_set_frc_reset( FRC_RESET_T mode, uint8_t isHighActive )
{
    ;
}

void Splash_configure_gpio( void )
{
    uint32_t    lval, lval_dbg;
    int         i;
    uint32_t    lTmp;
    uint32_t    lOpt;
    uint8_t     u8HwOpt=0;
}

extern unsigned int unlz4_get_decompsize(unsigned char * buf);
extern int lz4_do_decomp(unsigned char * pDecomp, unsigned char * pComp, unsigned long * pDecompSize);
int Splash_copyimage( unsigned int loadaddress, unsigned int uncompaddress)
{
#define MAX_CPY_SZ              (7*1024*1024)
    struct partition_info   *mpi      = NULL;
    unsigned int                read_size;
    unsigned int                hdr_img_len;
    unsigned long               decomp_size;
    unsigned int                tick1, tick2;
    unsigned char               *pCompData;
    int                         ret;
    ulong   decomp_size1 = 0, decomp_size2 = 0;

    mpi = get_used_partition("logo");

    if (mpi == NULL)
    {
        printf("^R^failed to get partition free\n");
        return -1;
    }

    if (mpi->valid == NO) {
        printf("^R^Partition is Not valid! => Skip!");
        return -1;
    }

    if(!mpi->filesize) {
        printf("^g^File image size is Zero, Using partition size!!");
        read_size = mpi->size;
    }
    else
        read_size = mpi->filesize;

    emmc_read((ulong)mpi->offset, read_size, loadaddress);
    printf("[%d] read image done\n", readMsTicks());

    if (ret) {
        printf("block read failed..\n");
        return 1;
    }

    decomp_size1 = unlz4_get_decompsize((unsigned char *)loadaddress);
    if (decomp_size1 == 0) {
        printf("decompressed size is zero at LZO header\n");
        return 1;
    }

    printf("[%d] decomp_size1 = 0x%x, copy \"%s (%ld bytes)\" from 0x%08x to 0x%08x\n", readMsTicks(), decomp_size1, mpi->name, read_size, (unsigned int)loadaddress, (unsigned int)uncompaddress);
    lz4_do_decomp((u_char *)uncompaddress, (u_char *)loadaddress, &decomp_size2);
    if (decomp_size2 != decomp_size1) {
        printf("decompressed size=%d is different with %d\n", (int)decomp_size2, (int)decomp_size1);
        return 1;
    }
    flush_cache((ulong)uncompaddress,(ulong)decomp_size2);
    printf("[%d] decomp done\n", readMsTicks());
    return 0;
}

#define UNCOMPRESS_HEADER_SIZE (0x36)
void Splash_load_logo(void)
{
    //Use the last 16MB of GOP buffer
    MS_U32 u32BootLogoFB = MMAPGer[GOP_FRAMEBUFFER].u32MemAddr + MMAPGer[GOP_FRAMEBUFFER].u32MemLength - CONFIG_BOOTLOGO_BUFFER_SIZE*2 + MiuInfo.u32MiuInterval;

    //Copy compressed data from the last 8MB and decompress to the last 16MB of GOP buffer
    UINT8 *_loadAddr    = MsOS_PA2KSEG0(u32BootLogoFB + CONFIG_BOOTLOGO_BUFFER_SIZE);
    UINT8 *_uncompAddr  = MsOS_PA2KSEG0(u32BootLogoFB - UNCOMPRESS_HEADER_SIZE);

    printf("\033[0;32m[%d] copy logo image _loadAddr = 0x%x, _uncompAddr = 0x%x \033[0m\n", readMsTicks(), _loadAddr, _uncompAddr);
    Splash_copyimage(_loadAddr, _uncompAddr);
}

void Splash_PWM_Init( uint32_t WidthPortB,  PWM2_LVL_MODE_T mode)
{
    ;
}

UINT8 Splash_FRC_Reset( FRC_RESET_T mode )
{
    return 0;
}

/**
 * Set swing level
 *
 * @return void
 * @author Hong, Young Ho(longsky@lge.com)
 * @note    (from src/drvers/frc/ursa3/frc_ursa3.c)
 */

UINT8 Splash_FRC_Config(void)
{
    return 0;
}

int Splash_FRC_Freeze(void)
{
    return 0;
}

void Splash_FRC_Init( void )
{
    ;
}

UINT32 Splash_SetModelOption( UINT32 tModelOption )
{
    _gModelOptions = tModelOption | 0x80000000;
    return _gModelOptions;
}

UINT32 Splash_GetHWoption(HW_OPT_T option_mask)
{
    UINT32 ret=0;
    static int init = 0;

    return ret;
}


#define FRC_RESET_START 0
#define FRC_RSSET_END   1
#define IMAGE_OFFSET    0x500000


extern void DDI_PWM_Init(PANEL_PWM_T pnlpw);
extern int DDI_LD_Init(void);

extern const SYS_DB_T gSysNvmDB;
int CurrentMirrorMode = 0;

void BootSplash(void)
{
    static UINT32       _gTimeLvdsOut;
    static UINT32       _gTimeFRCreset;
    static UINT32       _gTimePwrOn;
    static UINT32       _gTimePwmInit;
    static UINT32       _gTimePanelOn;
    static UINT32       _gTimeTCONI2CEN;

    static PANEL_POWER_SEQ_T    pnlpwrseq;
    static PANEL_PWM_T          pnlpwm;
    static UINT8                systype = 0;
    static UINT8                bPWM_VSync_Enable;
    static UINT32               nvmData;
    static UINT32               msDelay;
    static UINT8                supportB0LVDSMAP;
    int                         color_mode;
    int                         led_bar_type;
    UINT8                       inch;
    int                         OsdPath = 0; //LM15U


    TOOL_OPTION1_T      toolOpt1;
    TOOL_OPTION2_T      toolOpt2;
    TOOL_OPTION3_T      toolOpt3;
    TOOL_OPTION4_T      toolOpt4;
    TOOL_OPTION5_T      toolOpt5;
    TOOL_OPTION6_T      toolOpt6;
    TOOL_OPTION7_T      toolOpt7;
    TOOL_OPTION8_T      toolOpt8;
    TOOL_OPTION9_T      toolOpt9;

    UINT8 tempAddr[2]   = {0,0};
    UINT8 tconAddr      = 0xAD;
    UINT8 tconValue     = 0x70;

    UINT8 long_delay[2] = {0, };
    UINT32 delay        = 0x0;


    DDI_NVM_GetToolOpt1(&toolOpt1);
    DDI_NVM_GetToolOpt2(&toolOpt2);
    DDI_NVM_GetToolOpt3(&toolOpt3);
    DDI_NVM_GetToolOpt4(&toolOpt4);
    DDI_NVM_GetToolOpt5(&toolOpt5);
    DDI_NVM_GetToolOpt6(&toolOpt6);
    DDI_NVM_GetToolOpt7(&toolOpt7);
    DDI_NVM_GetToolOpt8(&toolOpt8);
    DDI_NVM_GetToolOpt9(&toolOpt9);

    gToolOpt[0] = toolOpt1.all;
    gToolOpt[1] = toolOpt2.all;
    gToolOpt[2] = toolOpt3.all;
    gToolOpt[3] = toolOpt4.all;
    gToolOpt[4] = toolOpt5.all;
    gToolOpt[5] = toolOpt6.all;
    gToolOpt[6] = toolOpt7.all;
    gToolOpt[7] = toolOpt8.all;
    gToolOpt[8] = toolOpt9.all;

#define OSA_MD_GetModuleInch()			(toolOpt1.flags.eModelInchType)
#define OSA_MD_GetToolType()			(toolOpt1.flags.eModelToolType)
#define OSA_MD_GetModuleMakerType()		(toolOpt1.flags.eModelModuleType)
#define OSA_MD_GetLvdsBit()				(toolOpt1.flags.nLVDSBit)
#define OSA_MD_GetBacklightType()		(toolOpt2.flags.eBackLight)
#define OSA_MD_IsSupportMirrorMode()	(toolOpt5.flags.bMirrorMode)
#define OSA_MD_GetLedBarType()			(toolOpt7.flags.eLEDBarType)
#define OSA_MD_GetPanelGamutType()		(toolOpt9.flags.ePanelGamutType)

    inch = OSA_MD_GetModuleInch();

    /*-----------------------------------------------
     * Read NVM data FOR power sequence data
     *-----------------------------------------------*/
    DDI_NVM_Read(SYS_DB_BASE + (UINT32)&(gSysNvmDB.validMark) - (UINT32)&gSysNvmDB, \
            sizeof(gSysNvmDB.validMark), (UINT8 *)&(nvmData));

    if( OSA_MD_IsSupportMirrorMode())
    {
        CurrentMirrorMode = 1;
    }
    if(nvmData != 0xffffffff)
    {
        DDI_NVM_Read(SYS_DB_BASE + (UINT32)&(gSysNvmDB.panelpowerseq) - (UINT32)&gSysNvmDB, \
                sizeof(gSysNvmDB.panelpowerseq), (UINT8 *)&(pnlpwrseq));
        DDI_NVM_Read(SYS_DB_BASE + (UINT32)&(gSysNvmDB.panelpwm) - (UINT32)&gSysNvmDB,  \
                sizeof(gSysNvmDB.panelpwm), (UINT8 *)&(pnlpwm));
        DDI_NVM_Read(SYS_DB_BASE + (UINT32)&(gSysNvmDB.systemtype) - (UINT32)&gSysNvmDB,    \
                sizeof(gSysNvmDB.systemtype), (UINT8 *)&(systype));
        DDI_NVM_Read(SYS_DB_BASE + (UINT32)&(gSysNvmDB.ColorDepth) - (UINT32)&gSysNvmDB,    \
                sizeof(gSysNvmDB.ColorDepth), (UINT8 *)&(gSysNvmDB.ColorDepth));
    }
    else
    {
        pnlpwrseq = gSysNvmDB.panelpowerseq;
        pnlpwm = gSysNvmDB.panelpwm;
    }

    printf("panel pwm = 48hz=%d, 50hz=%d, 60hz=%d, vbrB duty=%d \n", pnlpwm.vbrBFreq48hz, pnlpwm.vbrBFreq50hz, pnlpwm.vbrBFreq60hz, pnlpwm.vbrBMaxDuty);
    printf("gSysNvmDB.ColorDepth = %d\n", gSysNvmDB.ColorDepth);
    printf("gSysNvmDB.systemtype = %d\n", systype);
    printf("bPWM_VSync_Enable = %d\n",pnlpwm.config&PWM_VRST_EN);
    printf("bPWM led current = %d, freq=%d\n",pnlpwm.vbrCLedCurrent, pnlpwm.vbrCFreq);
    printf("pnlpwrseq.panelPowOnToData = %d\n", pnlpwrseq.panelPowOnToData);
    printf("pnlpwrseq.dataToLampOn = %d\n", pnlpwrseq.dataToLampOn);
    printf("pnlpwrseq.lampOffToData = %d\n", pnlpwrseq.lampOffToData);
    printf("pnlpwrseq.dataToPanelPowOff = %d\n", pnlpwrseq.dataToPanelPowOff);

    printf("[ToolOpt1] = %d\n",toolOpt1.all);
    printf("[ToolOpt2] = %d\n",toolOpt2.all);
    printf("[ToolOpt3] = %d\n",toolOpt3.all);
    printf("[ToolOpt4] = %d\n",toolOpt4.all);
    printf("[ToolOpt5] = %d\n",toolOpt5.all);
    printf("[ToolOpt6] = %d\n",toolOpt6.all);
    printf("[ToolOpt7] = %d\n",toolOpt7.all);
    printf("[ToolOpt8] = %d\n",toolOpt8.all);
    printf("[ToolOpt9] = %d\n",toolOpt9.all);

    /* Panel On */
    printf("\033[0;32m[%d] PANEL ON \033[0m\n", readMsTicks());
    Splash_MICOM_PanelOn();
    mdelay(USER_DATA_DELAY);

    printf("\033[0;32m[%d] Dimming ON \033[0m\n",readMsTicks());
    CMNIO_GPIO_SetOutputPort(GPIO_PORT_WR_LOCAL_DIMMING_EN,1);

    if(pnlpwrseq.panelPowOnToData < USER_DATA_DELAY)
        delay = 0;
    else
        delay = pnlpwrseq.panelPowOnToData - USER_DATA_DELAY;

    mdelay(delay);
#if (CONFIG_PANEL_INIT)
    run_command("panel_pre_init" , 0);
#endif
    run_command("panel_init",  0);
    printf("\033[0;32m[%d] PWM Init \033[0m\n",readMsTicks());
    DDI_PWM_Init(pnlpwm);

#if (CONFIG_LOCAL_DIMMING)
    DDI_LD_Init();
#endif

    if(pnlpwrseq.dataToLampOn > 0)
        delay = pnlpwrseq.dataToLampOn;
    else
        delay = 10;

#if defined(CONFIG_MULTICORES_PLATFORM)
    #ifdef USE_LOGO_THREAD
    if( thread[0] ) thread_join(thread[0], NULL);
    #endif
#endif

    printf("\033[0;32m[%d] Run bootlogo \033[0m\n",readMsTicks());
    run_command("bootlogo", 0); //draw_logo
    CMNIO_GPIO_SetOutputPort(GPIO_PORT_WR_TCON_I2C_EN,1);

    // Inv On
    long_delay[0] = (UINT8)(delay / 100);
    long_delay[1] = (UINT8)(delay - (long_delay[0] * 100));
    printf("[%d] Long Delayed INV on(delay[0]: %u * 100 ms + delay[1]: %u ms)\n", readMsTicks(), long_delay[0], long_delay[1]);
    Splash_MICOM_LongDelayed_InvOn(long_delay[0], long_delay[1]);

    // tcon 초기값 임시 설정
    *((volatile unsigned int *)(0x1F000000+ 0x0E*0x200+0x12*4)) = 0xBABE;
}

void display_logo(void)
{
    printf("Bootlogo Initializing...\n");
#if defined(CONFIG_MULTICORES_PLATFORM)

#if defined(CONFIG_MSTAR_MUNICH) || defined(CONFIG_MSTAR_MUSTANG) || defined(CONFIG_MSTAR_MAXIM)
    int cpu_id = 1;
#elif defined(CONFIG_MSTAR_M7621)
    int cpu_id = 3;
#else
    int cpu_id = 2;
#endif
    int priority = THREAD_MAX_PRIORITY;

    #ifdef USE_LOGO_THREAD
    printf("Creating thread=%s, pri=%d, cpu= %d\n", "load_logo", priority, cpu_id);
    thread[0] = thread_create_ex("load_logo", Splash_load_logo, NULL, 0, cpu_id--, priority--, 1);
    if( thread[0] == NULL )
    {
        printf("load_logo thread create fail...\n");
        return;
    }

    printf("Creating thread=%s, pri=%d, cpu= %d\n", "logo", priority, cpu_id);
    thread[1] = thread_create_ex("logo", BootSplash, NULL, 0, cpu_id, priority, 1);
    if( thread[1] == NULL ) printf("logo thread create fail...\n");
    #else
    Splash_load_logo();
    BootSplash();
    #endif
#else
    Splash_load_logo();
    BootSplash();
#endif
}
