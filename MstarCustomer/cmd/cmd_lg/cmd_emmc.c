
#include <malloc.h>
#include <common.h>
#include <command.h>
#include <string.h>
#include <partinfo.h>
#include <mmc.h>
#include <linux/ctype.h>
#include <eMMC.h>

typedef int (*SUB_FUNC_T)(int argc, char *argv[]);

static int subcmd_emmc_erase(int argc, char *argv[])
{
	u64			size, offset;
	int			rc;
	static int	curr_device = -1;

	if(curr_device < 0) {
		if(get_mmc_num() > 0)
			curr_device = 0;
		else {
			puts("No MMC device available\n");
			return 1;
		}
	}
	struct mmc	*mmc = find_mmc_device(curr_device);

	if(!mmc)
	{
		printf("No mmc device at slot %x\n", curr_device);
		return -1;
	}

	if(argc == 2)
	{
		int in_char;
		printf("Really erase all the data in emmc ? <y/N> ");
		in_char = getc();
		printf("%c\n\n", isprint(in_char)? in_char : ' ');

		if(in_char != 'y')
			return 0;

		offset = 0;
		size = g_eMMCDrv.u32_BOOT_SEC_COUNT;
		if(size == 0)
		{
			printf("ERR:emmc no boot partition size !\n");
			return -1;
		}

		printf("Erase boot area..\n");
		eMMC_EraseBootPart(offset, offset + size - 1, 1);
		eMMC_EraseBootPart(offset, offset + size - 1, 2);
		printf("Erase boot area..Done\n");
		offset = 0;
		size = mmc->capacity;
	}
	else
	{
		if(get_offset_size(argc - 2, argv + 2, &offset, &size) < 0)
			return -1;

		if(size == 0)
		{
			printf("invalid size\n");
			return -1;
		}
	}		

	printf("emmc erase (0x%012llx ~ 0x%012llx) : ", offset, offset + size);
	rc = storage_erase(offset, size);

	printf("%s\n", (rc == 0)? "OK" : "Fail");

	return 0;
}

static int subcmd_emmc_dump(int argc, char *argv[])
{
	ulong	size = 0, offset = 0, dst = CFG_LOAD_ADDR;
	int		ret = 0;
	char	cmd[20] = {0, };

	if(argc != 3 && argc != 4)
		return -1;

	if(get_offset_size(argc - 2, argv + 2, &offset, &size) < 0)
		return -2;

	if(size == 0)
		size = 512;

	ret = emmc_read(offset, size, dst);
	if(ret) {
		printf("block read failed..\n");
		return -1;
	}

	printf("Offset 0x%x, size: 0x%x, dump:0x%x\n", offset, size, offset);

	sprintf(cmd, "md.b 0x%x 0x%x\n", dst, size);
	ret = run_command(cmd, 0);

	return ret;
}

static SUB_FUNC_T cmp_subcmd(char *str)
{
	typedef struct
	{
		char		*cmd;
		SUB_FUNC_T	func;
	} SUBCMD_T;

	SUBCMD_T cmd_tbl[] = {
		{ "erase", subcmd_emmc_erase   },
		{ "dump", subcmd_emmc_dump   },
		{  NULL }
	};

	int		index;

	if (str == NULL)
		return NULL;

	for (index = 0; ; index++)
	{
		if (cmd_tbl[index].cmd == NULL)
			return NULL;

		if (!strcmp(cmd_tbl[index].cmd, str))
			break;
	}

	return (SUB_FUNC_T)cmd_tbl[index].func;
}


int cmd_emmc(cmd_tbl_t *cmdtp, int flag, int argc, char *argv[]) // should be used integer type function
{
	SUB_FUNC_T	func;

	if (argc < 2)
		goto usage;

	func = cmp_subcmd(argv[1]);

	if (func == NULL)
		goto usage;

	if(func(argc, argv) == 0)
		return 0;

usage:
	printf ("Usage in cmd_emmc :\n%s\n", cmdtp->usage);
	return -1;
	
}

U_BOOT_CMD(
     emmc,   6,  0,  cmd_emmc,
     "emmc dump, erase\n",
	 "emmc erase offset|partition size\n"
	 "emmc dump offset|partition size\n"
);



