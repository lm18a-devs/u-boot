/*
 * (C) Copyright 2003
 * Kyle Harris, kharris@nexus-tech.net
 *
 * See file CREDITS for list of people who contributed to this
 * project.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */

#ifndef __CMD_MS_MMC__
#define __CMD_MS_MMC__
#include <common.h>

struct emmc_write_options {
	ulong *buffer;		/* memory block containing image to write */
       char *fatfile_name; /* only be used for emmc binary restore, transfer the binary file name */
	u64 length;		/* number of bytes to write */
	u64 offset;		/* start address in EMMC */
	int quiet;		/* don't display progress messages */
	int pad;		/* pad to page size */
};

typedef struct emmc_write_options emmc_write_options_t;

struct emmc_read_options {
    u_char *buffer;     /* memory block in which read image is written*/
    char *fatfile_name; /* only be used for emmc binary making, transfer the binary file name */
    u64 length;       /* number of bytes to read */
    u64 offset;       /* start address in EMMC */
    int quiet;      /* don't display progress messages */
	int bootno;     /* boot partition number, 1 or 2 */
};

struct sd_mmc_ext_csd
{
	unsigned char       buffer[512];
	unsigned char       s_cmd_set;             // 1bit 504
	unsigned char       power_off_long_time;        // 1byte 247
	unsigned char       ini_timeout_ap;        // 1byte 241
	unsigned char       pwr_cl_ddr_52_360;     // 1byte 239
	unsigned char       pwr_cl_ddr_52_195;     // 1byte 238
	unsigned char       min_perf_ddr_w8_52;    // 1byte 235
	unsigned char       min_perf_ddr_r8_52;    // 1byte 234
	unsigned char       trim_mult;    		   // 1byte 232
	unsigned char       sec_support;   		   // 1byte 231
	unsigned char       sec_erase_mult;    	   // 1byte 230
	unsigned char       sec_trim_mult;    	   // 1byte 229
	unsigned char       boot_info;    		   // 1byte 228
	unsigned char 		boot_size_mult;		   //1byte 226
	unsigned char 		access_size;		   //1byte 225
	unsigned char 		hc_erase_grp_size;	   //1byte 224
	unsigned char 		erase_timeout_multi;   //1byte 223
	unsigned char 		rel_wr_sec_cnt;   	   //1byte 222
	unsigned char 		hc_wp_grp_size;        //1byte 221
	unsigned char 		sleep_cur_vcc;   	   //1byte 220
	unsigned char 		sleep_cur_vccq;   	   //1byte 219
	unsigned char 		slp_awk_timeout;   	   //1byte 217
    unsigned int		sec_count;             // 4bit 215-212
	unsigned char       min_pref_w_8_52;       // 1bit 210
	unsigned char       min_pref_r_8_52;       // 1bit 209
	unsigned char       min_pref_w_8_26_4_52;  // 1bit 208
	unsigned char       min_pref_r_8_26_4_52;  // 1bit 207
	unsigned char       min_pref_w_4_26;       // 1bit 206
	unsigned char       min_pref_r_4_26;       // 1bit 205
	unsigned char       pwr_cl_26_360;         // 1bit 203
	unsigned char       pwr_cl_52_360;         // 1bit 202
	unsigned char       pwr_cl_26_195;         // 1bit 201
	unsigned char       pwr_cl_52_195;         // 1bit 200
	unsigned char       card_type;             // 1bit 196
	unsigned char       csd_structure;         // 1bit 194
	unsigned char       ext_csd_rev;           // 1bit 192

	//Modes segment

	unsigned char       cmd_Set;               //1bit 191  R/W
	unsigned char       cmd_set_rev;           //bit1 189
	unsigned char       power_class;           //bit1 187
	unsigned char       hs_timing;             //bit1 185
	unsigned char       bus_width;             //bit1 183
	unsigned char       erased_mem_cont;       //bit1 181
	unsigned char       boot_config;       	   //bit1 179
	unsigned char       boot_config_prot;  	   //1byte 178
	unsigned char       boot_bus_width;        //bit1 177
	unsigned char       erase_grp_defn;        //bit1 175
	unsigned char       boot_wp;        //1byte 173
	unsigned char       user_wp;        //1byte 171
	unsigned char       fw_config;      //1byte 169
	unsigned char       rpmb_size_mult; //1byte 168
	unsigned char       rst_n_func;     //1byte 162
	unsigned char       part_support;   //1byte 160
	unsigned int        max_enh_size_mult;  //3bytes 157
	unsigned char       part_attrb;  //1byte 156
	unsigned char       part_set_complete;  //1byte 155
	unsigned int       GP_size_mult0;  //3byte 143
	unsigned int       GP_size_mult1;  //3byte 146
	unsigned int       GP_size_mult2;  //3byte 149
	unsigned int       GP_size_mult3;  //3byte 152
	unsigned int       enh_size_mult;  //3byte 140
	unsigned int       enh_start_addr;  //4byte 136
	unsigned char      sec_bad_blk_mngt;  //1byte 134
	unsigned char       power_off_noti;        // 1byte 34
}__attribute__((packed));
static struct sd_mmc_ext_csd ext_csd;

struct sd_mmc_csd_v4
{
    unsigned char       csd_struct         :2;         //127-126 bits=4
    unsigned int        specs_ver          :4;         //125-122 bits=4
    unsigned int        reserve3           :2;         //121-120 bits=2
    //121-120 reserved
    unsigned char       taac;                          //119-112 bits=8
    unsigned char       nsac;                          //111-104 bits=8
    unsigned char       tran_speed;                    //103-96  bits-8
    unsigned short      ccc;                           //95-84 bits=12
    unsigned char       read_bl_len;                   //83-80 bits=4
    unsigned int        read_partial       :1,         //79-79 bits=1
                        write_misalign     :1,         //78-78 bits=1
                        read_misalign      :1;         //77-77 bits=1
    unsigned int        dsr_imp            :1;         //75-75 bits=1
    unsigned int        reserve2           :2;         //75-74 bits=2
    //75-74 bits=3  reserved
    unsigned int        c_size             :12;         //73-62 bits=12

    unsigned int        vdd_r_curr_min     :3;         //61-59 bits=3
    unsigned int        vdd_r_curr_max     :3;         //58-56 bits=3
    unsigned int        vdd_w_curr_min     :3;         //55-53 bits=3
    unsigned int        vdd_w_curr_max     :3;         //52-50 bits=3
    //50-61 bits=12 reserved previously
    unsigned            c_size_mult        :3;         //49-47 bits=3

    unsigned int        erase_grp_size     :5;         //46-42 bits=5
    unsigned int        erase_grp_mult     :5;         //41-32 bits=5
    unsigned int        wp_grp_size        :5;         //36-32 bits=5
    unsigned int        wp_grp_enable      :1;         //31-31 bits=2
    unsigned int        default_ecc        :2;         //30-29 bits=2
    //29-46 bits=18 reserved prevoiously
    unsigned int        r2w_factor         :3;         //28-26 bits=3
    unsigned int        write_bl_len       :4;         //25-22 bits=4
    unsigned            write_partial      :1;         //21-21 bits=1

    unsigned int        reserve1           :4;         //20-17 reserved
    unsigned int        content_prot_app   :1,         //16-16 bits=1
                        file_format_grp    :1,         //15-15 bits=1
                        copy               :1,         //14-14 bits=1
                        perm_write_protect :1,         //13-13 bits=1
                        tmp_write_prot     :1;         //12-12 bits=1
    unsigned int        file_format        :2;         //11-10 bits=2
    unsigned int        ecc                :2;         //09-08 bits=2
};
static struct sd_mmc_csd_v4 mmc_csd_v4;

struct mmc_bininfo{
	ulong valid_block;
	disk_partition_t dpt; 
};

typedef struct emmc_read_options emmc_read_options_t;

#ifndef CONFIG_GENERIC_MMC
int do_mmc (cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[]);
#else
int do_mmcinfo (cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[]);
int do_mmcreg (cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[]);

#if defined(CONFIG_USB) && defined(CONFIG_FAT)
int do_emmc_bin_restore(cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[]);
int do_emmcboot_mkbin(cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[]);
int do_emmc_mkbin(cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[]);
#endif

int do_mmc_bininfo(cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[]);
int do_mmcops(cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[]);
int do_eMMCops(cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[]);
#endif

int get_mmc_partsize (char *puPartName,unsigned int *u32PartSize);

#endif
