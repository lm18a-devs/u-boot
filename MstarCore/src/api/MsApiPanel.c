//<MStar Software>
//******************************************************************************
// MStar Software
// Copyright (c) 2010 - 2012 MStar Semiconductor, Inc. All rights reserved.
// All software, firmware and related documentation herein ("MStar Software") are
// intellectual property of MStar Semiconductor, Inc. ("MStar") and protected by
// law, including, but not limited to, copyright law and international treaties.
// Any use, modification, reproduction, retransmission, or republication of all
// or part of MStar Software is expressly prohibited, unless prior written
// permission has been granted by MStar.
//
// By accessing, browsing and/or using MStar Software, you acknowledge that you
// have read, understood, and agree, to be bound by below terms ("Terms") and to
// comply with all applicable laws and regulations:
//
// 1. MStar shall retain any and all right, ownership and interest to MStar
//    Software and any modification/derivatives thereof.
//    No right, ownership, or interest to MStar Software and any
//    modification/derivatives thereof is transferred to you under Terms.
//
// 2. You understand that MStar Software might include, incorporate or be
//    supplied together with third party`s software and the use of MStar
//    Software may require additional licenses from third parties.
//    Therefore, you hereby agree it is your sole responsibility to separately
//    obtain any and all third party right and license necessary for your use of
//    such third party`s software.
//
// 3. MStar Software and any modification/derivatives thereof shall be deemed as
//    MStar`s confidential information and you agree to keep MStar`s
//    confidential information in strictest confidence and not disclose to any
//    third party.
//
// 4. MStar Software is provided on an "AS IS" basis without warranties of any
//    kind. Any warranties are hereby expressly disclaimed by MStar, including
//    without limitation, any warranties of merchantability, non-infringement of
//    intellectual property rights, fitness for a particular purpose, error free
//    and in conformity with any international standard.  You agree to waive any
//    claim against MStar for any loss, damage, cost or expense that you may
//    incur related to your use of MStar Software.
//    In no event shall MStar be liable for any direct, indirect, incidental or
//    consequential damages, including without limitation, lost of profit or
//    revenues, lost or damage of data, and unauthorized system use.
//    You agree that this Section 4 shall still apply without being affected
//    even if MStar Software has been modified by MStar in accordance with your
//    request or instruction for your use, except otherwise agreed by both
//    parties in writing.
//
// 5. If requested, MStar may from time to time provide technical supports or
//    services in relation with MStar Software to you for your use of
//    MStar Software in conjunction with your or your customer`s product
//    ("Services").
//    You understand and agree that, except otherwise agreed by both parties in
//    writing, Services are provided on an "AS IS" basis and the warranty
//    disclaimer set forth in Section 4 above shall apply.
//
// 6. Nothing contained herein shall be construed as by implication, estoppels
//    or otherwise:
//    (a) conferring any license or right to use MStar name, trademark, service
//        mark, symbol or any other identification;
//    (b) obligating MStar or any of its affiliates to furnish any person,
//        including without limitation, you and your customers, any assistance
//        of any kind whatsoever, or any information; or
//    (c) conferring any license or right under any intellectual property right.
//
// 7. These terms shall be governed by and construed in accordance with the laws
//    of Taiwan, R.O.C., excluding its conflict of law rules.
//    Any and all dispute arising out hereof or related hereto shall be finally
//    settled by arbitration referred to the Chinese Arbitration Association,
//    Taipei in accordance with the ROC Arbitration Law and the Arbitration
//    Rules of the Association by three (3) arbitrators appointed in accordance
//    with the said Rules.
//    The place of arbitration shall be in Taipei, Taiwan and the language shall
//    be English.
//    The arbitration award shall be final and binding to both parties.
//
//******************************************************************************
//<MStar Software>
//-------------------------------------------------------------------------------------------------
// Includes
//-------------------------------------------------------------------------------------------------
#include <common.h>
#include <command.h>
#include <MsTypes.h>
#include <apiPNL.h>
#include <MsDebug.h>
#include <bootlogo/MsPoolDB.h>
#include <panel/MsDrvPanel.h>
#include <MsApiPanel.h>
#if(ENABLE_URSA_6M30 == 1)
#include <ursa/ursa_6m30.h>
#endif
#if (ENABLE_MSTAR_CLEANBOOT) ||(ENABLE_MSTAR_PUMABOOT)
#include <MsSystem.h>
#include <drvMBX.h>
#include <drvMBXMsgPool.h>
#include <MsOS.h>
#endif

#if(ENABLE_URSA_6M40 == 1)
#include <ursa/ursa_6m40.h>
#endif

#if(ENABLE_URSA_8 == 1)
#include <ursa/ursa_8.h>
#endif

#if defined(CONFIG_URSA6_VB1)
#include <ursa/ursa_6m38.h>
#endif

#include <MsMmap.h>
#include <MsSystem.h>
#include <mstarstr.h>

#include <CusCmnio.h>
//-------------------------------------------------------------------------------------------------
//  Debug Functions
//-------------------------------------------------------------------------------------------------
void cmp(PanelType *p1, PanelType *p2);

//-------------------------------------------------------------------------------------------------
//  Local Defines
//-------------------------------------------------------------------------------------------------
#define PANEL_DEBUG 1

#if defined(LG_CHG)
#define REG_SCALER_BK10 0x1310
#define REG_LPLL 0x1031
#define REG_MOD_BK00 0x1032
#define REG_CLKGEN0 0x100B
#define REG_CLKGEN1 0x1033
#define MS_PANEL__WRITE2BYTE(bank, addr16,value) (*((volatile unsigned int *)(0x1F000000+ (bank)*0x200+(addr16)*4)) = (value))
#endif

//-------------------------------------------------------------------------------------------------
//  External Functions
//-------------------------------------------------------------------------------------------------
#if (ENABLE_MSTAR_PUMABOOT)
extern MS_BOOL MApi_PBMBX_SendMsgPanel(U32 u32Addr, U16 u16Level, U16 u16Type);
#endif

//-------------------------------------------------------------------------------------------------
//  Global Variables
//-------------------------------------------------------------------------------------------------

MS_U16 gPanelWidth;
MS_U16 gPanelHeight;

MS_BOOL bPreinit = false;

//-------------------------------------------------------------------------------------------------
//  Local Variables
//-------------------------------------------------------------------------------------------------
static MS_BOOL bPanleReady=FALSE;
static GetPnlTypeSet_cb fpCusGetPnlTypeSet = NULL;

#if (ENABLE_HDMI_RESOLUTION_RESET==1)
#if (ENABLE_HDMI_TX_RESOLUTION == 0)
static char* DacPanelIndexTbl[] = {
    "DACOUT_480P_60",
    "DACOUT_576P_50",
    "DACOUT_720P_50",
    "DACOUT_720P_60",
    "DACOUT_1080P_50",
    "DACOUT_1080P_60",
    "DACOUT_480I_60",
    "DACOUT_576I_50",
    "DACOUT_1080I_50",
    "DACOUT_1080I_60",
};

typedef enum{
    DACOUT_RES_480P_60              = 0,
    DACOUT_RES_576P_50              = 1,
    DACOUT_RES_720P_50              = 2,
    DACOUT_RES_720P_60              = 3,
    DACOUT_RES_1080P_50             = 4,
    DACOUT_RES_1080P_60             = 5,
    DACOUT_RES_480I_60              = 6,
    DACOUT_RES_576I_50              = 7,
    DACOUT_RES_1080I_50             = 8,
    DACOUT_RES_1080I_60             = 9,
}DACOUT_VIDEO_TIMING;

#else
#include <apiHDMITx.h>
static char* HdmiTxPanelIndexTable[] = {
    "",
    "HDMITX_480_60I",
    "HDMITX_576_50I",
    "HDMITX_480_60P",
    "HDMITX_576_50P",
    "HDMITX_720_50P",
    "HDMITX_720_60P",
    "HDMITX_1080_50I",
    "HDMITX_1080_60I",
    "HDMITX_1080_24P",
    "HDMITX_1080_25P",
    "HDMITX_1080_30P",
    "HDMITX_1080_50P",
    "HDMITX_1080_60P",
    "HDMITX_4K2K_30P",
    "HDMITX_1470_50P",
    "HDMITX_1470_60P",
    "HDMITX_1470_24P",
    "HDMITX_1470_30P",
    "HDMITX_2205_24P",
    "HDMITX_2205_30P",
    "HDMITX_4K2K_25P",
};

static char* HdmiTxTimingIndexTable[] = {
    "HDMITX_RES_640x480p",
    "HDMITX_RES_720x480i",
    "HDMITX_RES_720x576i",
    "HDMITX_RES_720x480p",
    "HDMITX_RES_720x576p",
    "HDMITX_RES_1280x720p_50Hz",
    "HDMITX_RES_1280x720p_60Hz",
    "HDMITX_RES_1920x1080i_50Hz",
    "HDMITX_RES_1920x1080i_60Hz",
    "HDMITX_RES_1920x1080p_24Hz",
    "HDMITX_RES_1920x1080p_25Hz",
    "HDMITX_RES_1920x1080p_30Hz",
    "HDMITX_RES_1920x1080p_50Hz",
    "HDMITX_RES_1920x1080p_60Hz",
    "HDMITX_RES_4K2Kp_30Hz",
    "HDMITX_RES_1280x1470p_50Hz",
    "HDMITX_RES_1280x1470p_60Hz",
    "HDMITX_RES_1280x1470p_24Hz",
    "HDMITX_RES_1280x1470p_30Hz",
    "HDMITX_RES_1920x2205p_24Hz",
    "HDMITX_RES_1920x2205p_30Hz",
    "HDMITX_RES_4K2Kp_25Hz",
};
#endif
#endif

#if (ENABLE_MSTAR_KENYA==1)
typedef struct
{
    MS_U8                   u8ResolutionEnv;
    PANEL_RESOLUTION_TYPE   enResolutionType;
}RESOLUTION_DAC_MAP;

static RESOLUTION_DAC_MAP stMapTypeIndex[] = {
    {0, DACOUT_480P},
    {1, DACOUT_576P},
    {2, DACOUT_720P_50},
    {3, DACOUT_720P_60},
    {4, DACOUT_1080P_50},
    {5, DACOUT_1080P_60},
    {6, DACOUT_480I},
    {7, DACOUT_576I},
    {8, DACOUT_1080I_50},
    {9, DACOUT_1080I_60}
};
#endif

//-------------------------------------------------------------------------------------------------
//  Extern Functions
//-------------------------------------------------------------------------------------------------


//-------------------------------------------------------------------------------------------------
//  Private Functions
//-------------------------------------------------------------------------------------------------
int panel_sinit(void);
int panel_dinit(void);
int do_panel_vby1_setting(void);
static PANEL_RESOLUTION_TYPE _GetPnlTypeSetting(void);

//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------
int IsPanelReady(void)
{
    if(bPanleReady==TRUE)
    {
        return 0;
    }
    else
    {
        return -1;
    }
}

APIPNL_LINK_EXT_TYPE GetPanelLinkExtType(void)
{
    APIPNL_LINK_EXT_TYPE eType = LINK_EPI34_8P;
    char *s = getenv("panel_ext_type");
    if(s != NULL)
    {
        eType = (APIPNL_LINK_EXT_TYPE)simple_strtol(s, NULL, 10);
    }
    return eType;
}

#if (ENABLE_MSTAR_CLEANBOOT==1)
#define PNL_MBX_QUEUESIZE      8
#define PNL_MBX_TIMEOUT          5000

MS_BOOL MApi_PNL_MailBox_Init(void)
{
    if( E_MBX_SUCCESS != MDrv_MBX_RegisterMSG(E_MBX_CLASS_CB_PNL_NOWAIT, PNL_MBX_QUEUESIZE))
    {
        printf("MApi_PNL_MailBox_Init - MBX register msg error\n");
        return FALSE;
    }
    return TRUE;
}

MS_BOOL MApi_PNL_MailBox_Deinit(void)
{
    if( E_MBX_SUCCESS != MDrv_MBX_UnRegisterMSG(E_MBX_CLASS_CB_PNL_NOWAIT, TRUE))
    {
        printf("@@ E_MBX_CLASS_SECURE_WAIT - MBX unregister msg error\n");
    }
    return TRUE;
}

MS_BOOL MApi_PNL_MailBox_SendParameters(U32 u32Addr, U16 u16Level, U16 u16Type)
{
    MBX_Msg stMB_Command;
    MBX_Result enMbxResult = 0;

    memset((void*)&stMB_Command, 0x00, sizeof(MBX_Msg));
//    printf("MApi_PNL_MailBox_SendParameters - MDrv_MBX_SendMsg u32Addr:0x%x,u16Level:0x%x,u16Type:0x%x\n",u32Addr,u16Level,u16Type);

    //(1) send to CB
    stMB_Command.eRoleID = E_MBX_ROLE_CP;
    stMB_Command.eMsgType = E_MBX_MSG_TYPE_INSTANT;
    stMB_Command.u8Ctrl = 0;
    stMB_Command.u8MsgClass = E_MBX_CLASS_CB_PNL_NOWAIT;
    stMB_Command.u8Index = 0;
    stMB_Command.u8ParameterCount = 9;
    stMB_Command.u8Parameters[0] =  (U8)(u32Addr>>24);
    stMB_Command.u8Parameters[1] =  (U8)(u32Addr>>16);
    stMB_Command.u8Parameters[2] =  (U8)(u32Addr>>8);
    stMB_Command.u8Parameters[3] =  (U8)(u32Addr&0xFF);
    stMB_Command.u8Parameters[4] =  (U8)(u16Level>>8);
    stMB_Command.u8Parameters[5] =  (U8)(u16Level&0xFF);
    stMB_Command.u8Parameters[6] =  (U8)(u16Type>>8);
    stMB_Command.u8Parameters[7] =  (U8)(u16Type&0xFF);
    stMB_Command.u8Parameters[8] =  0;//panel init order
//    printf(" -MDrv_MBX_SendMsg u8Parameters[0]:0x%x, MDrv_MBX_SendMsg u8Parameters[1]:0x%x\n",stMB_Command.u8Parameters[0],stMB_Command.u8Parameters[1]);
//    printf(" -MDrv_MBX_SendMsg u8Parameters[2]:0x%x, MDrv_MBX_SendMsg u8Parameters[3]:0x%x\n",stMB_Command.u8Parameters[2],stMB_Command.u8Parameters[3]);
//    printf(" -MDrv_MBX_SendMsg u8Parameters[4]:0x%x, MDrv_MBX_SendMsg u8Parameters[5]:0x%x\n",stMB_Command.u8Parameters[4],stMB_Command.u8Parameters[5]);
//    printf(" -MDrv_MBX_SendMsg u8Parameters[6]:0x%x, MDrv_MBX_SendMsg u8Parameters[7]:0x%x\n",stMB_Command.u8Parameters[6],stMB_Command.u8Parameters[7]);
    MDrv_MBX_SendMsg(&stMB_Command);
//    printf("MApi_PNL_MailBox_SendParameters - MDrv_MBX_SendMsg \n");

    //(2) Waiting for message done
    do
    {
        memset((void*)&stMB_Command, 0x00, sizeof(MBX_Msg));
        enMbxResult = MDrv_MBX_RecvMsg(E_MBX_CLASS_CB_PNL_NOWAIT, &stMB_Command, PNL_MBX_TIMEOUT, MBX_CHECK_INSTANT_MSG);
    }while((enMbxResult  !=  E_MBX_SUCCESS) && (enMbxResult  !=  E_MBX_ERR_TIME_OUT));

    //(3) check result
    if(enMbxResult == E_MBX_ERR_TIME_OUT)
    {
        printf("MApi_PNL_MailBox_SendParameters Fail\n");
    }
    return enMbxResult;
}

#endif //(ENABLE_MSTAR_CLEANBOOT==1)


void RegisterCBGetPnlTypeSetting(GetPnlTypeSet_cb cb)
{
    UBOOT_TRACE("IN\n");
    fpCusGetPnlTypeSet=cb;
    UBOOT_TRACE("OK\n");
}

#if (ENABLE_MSTAR_KENYA==1)
static PANEL_RESOLUTION_TYPE _GetPnlTypeSettingFromEnv(void)
{
    int resolution_index = 0;
    PANEL_RESOLUTION_TYPE PanelType = DACOUT_1080I_50;
    char * p_str;
    p_str = getenv ("resolution");

    if(p_str != NULL)
    {
        resolution_index = (int)simple_strtol(p_str, NULL, 10);
        if(resolution_index < sizeof(stMapTypeIndex)/sizeof(RESOLUTION_DAC_MAP))
        {
            return stMapTypeIndex[resolution_index].enResolutionType;
        }
    }

    return PanelType;
}
#endif
static PANEL_RESOLUTION_TYPE _GetPnlTypeSetting(void)
{
    PANEL_RESOLUTION_TYPE PanelType = 0;
    UBOOT_TRACE("IN\n");
    if(fpCusGetPnlTypeSet!=NULL)
    {
        PanelType = fpCusGetPnlTypeSet();
    }
    else
    {
        PanelType = MApi_PNL_GetPnlTypeSetting();
    }
    UBOOT_TRACE("OK\n");
    return PanelType;
}

extern EN_TCON_TYPE_IDX_T e_tcontype;

int panel_sinit(void)
{
    PanelType* panel_data = NULL;
    PANEL_RESOLUTION_TYPE enPanelType;
    UBOOT_TRACE("IN\n");

    if(bPreinit == false)
    {
        if(e_tcontype == E_TCON_NONE)
        {
            MApi_PNL_SetOutput(E_PNL_CLK_DATA);
        }
        return 0;
    }

#if (ENABLE_MSTAR_KENYA==1)
    RegisterCBGetPnlTypeSetting(_GetPnlTypeSettingFromEnv);
#endif

    enPanelType = _GetPnlTypeSetting();
    if(MApi_PNL_PreInit(E_PNL_NO_OUTPUT)!=TRUE)
    {
        bPanleReady=FALSE;
        return -1;
    }

    panel_data=MApi_XC_GetPanelSpec(enPanelType);

#if 1 // [LM17A_PNL_BRINGUP]
    panel_data=MApi_XC_GetPanelSpec(5);
#endif

    if(panel_data==NULL)
    {
        bPanleReady=FALSE;
        return -1;
    }
    gPanelWidth = panel_data->m_wPanelWidth;
    gPanelHeight = panel_data->m_wPanelHeight;

#if 1 //(ENABLE_MSTAR_MONACO)
    if(panel_data->m_wPanelWidth == 3840 && panel_data->m_wPanelHeight == 2160)
    {
        UBOOT_DEBUG(">> m_ePanelLinkExtType = 51 <<<\n");
        setLinkExtType(51);
    }
    else if(panel_data->m_wPanelWidth == 1920 && panel_data->m_wPanelHeight == 1080)
    {
        UBOOT_DEBUG(">> m_ePanelLinkExtType = 51 <<<\n");
        if(panel_data->m_ePanelLinkType == LINK_EXT)
        {
            setLinkExtType(46);
        }
        else if(panel_data->m_ePanelLinkType == LINK_LVDS)
        {
            //do nothing
        }
        else
        {
            //do nothing
        }
    }
#endif
    //pane init
    #ifdef BD_LVDS_CONNECT_TYPE
	MApi_BD_LVDS_Output_Type(BD_LVDS_CONNECT_TYPE);
    #endif
    if(MsDrv_PNL_Init(panel_data)==FALSE)
    {
        bPanleReady=FALSE;
        return -1;
    }
    bPanleReady=TRUE;
    UBOOT_TRACE("OK\n");
    return 0;
}

#if (ENABLE_HDMI_RESOLUTION_RESET==1)
static int BootArgs_reset(void)
{
    int resolution_index = 5;
    char p_str[] = "\0";
    char *p;
    char * bootmode = getenv("bootmode");
    char as8PanelFilePath[50] = "\0";
    #if (ENABLE_HDMI_TX_RESOLUTION == 1)
    HDMITX_VIDEO_TIMING eHDMITimgType = HDMITX_RES_1280x720p_50Hz;
    #else
    DACOUT_VIDEO_TIMING ePANELType = DACOUT_RES_720P_50;
    #endif
    if ((bootmode != NULL) && (0 == strcmp(bootmode,"boot-recovery")))
    {
        if(getenv("resolution") != NULL)
        {
            #if (ENABLE_HDMI_TX_RESOLUTION == 1)
            snprintf(p_str,sizeof(p_str),"%ld",eHDMITimgType);
            #else
            snprintf(p_str,sizeof(p_str),"%ld",ePANELType);
            #endif
            if (0 == strcmp(getenv("resolution"), p_str))
                return 0;
            setenv("resolution", p_str);
        }
    }
    else
    {
        if (getenv ("resolution_reset") == NULL)
        {
            setenv("resolution_reset", getenv ("resolution"));
        }
        else
        {
            if (0 == strcmp(getenv("resolution"), getenv ("resolution_reset")))
                return 0;
            setenv("resolution", getenv ("resolution_reset"));
        }
    }

    p = getenv ("resolution");
    if(p != NULL)
    {
        resolution_index = (int)simple_strtol(p, NULL, 10);
        #if (ENABLE_HDMI_TX_RESOLUTION == 1)
        MApi_SetEnv2BootArgs("resolution=", HdmiTxTimingIndexTable[resolution_index]);
        snprintf(as8PanelFilePath, sizeof(as8PanelFilePath),"/config/panel/%s.ini", HdmiTxPanelIndexTable[resolution_index]);
        setenv("panel_path", as8PanelFilePath);
        setenv("panel_name", HdmiTxPanelIndexTable[resolution_index]);
        #else
        MApi_SetEnv2BootArgs("resolution=", DacPanelIndexTbl[resolution_index]);
        snprintf(as8PanelFilePath, sizeof(as8PanelFilePath),"/config/panel/%s.ini", DacPanelIndexTbl[resolution_index]);
        setenv("panel_path", as8PanelFilePath);
        setenv("panel_name", DacPanelIndexTbl[resolution_index]);
        #endif
        setenv("db_table", "0");
        run_command("dbtable_init", 0);
    }
    else
    {
        //set env normal resolution.
        setenv("resolution", "5");
    }
    saveenv();
    return 0;
}
#endif

int panel_dinit(void)
{
    char *pSwingLevel;
    MS_U16 u16Panel_SwingLevel;
    #if (ENABLE_MSTAR_CLEANBOOT==1)
    static PanelType panelpara;
    #else
    PanelType panelpara;
    #endif //(ENABLE_MSTAR_CLEANBOOT==1)
    st_board_para boardpara;
    UBOOT_TRACE("IN\n");

    memset(&panelpara, 0, sizeof(panelpara));
    memset(&boardpara, 0, sizeof(boardpara));

    #if (ENABLE_HDMI_RESOLUTION_RESET==1)
    BootArgs_reset();
    #endif
    //load panel para
#if defined(CONFIG_URSA6_VB1) || defined(CONFIG_NOVA_KS2)
	if(is_str_resume())
	{
		U32 u32PanelConfigsAddr;
		if(get_addr_from_mmap("E_MMAP_ID_VDEC_CPU", &u32PanelConfigsAddr)!=0)
		{
			UBOOT_ERROR("get E_MMAP_ID_VDEC_CPU mmap fail\n");
			bPanleReady=FALSE;
			return -1;
		}
		UBOOT_DEBUG("E_MMAP_ID_VDEC_CPU = 0x%x\n", u32PanelConfigsAddr);
		UBOOT_DEBUG("(U32)(PA2NVA(u32PanelConfigsAddr)) = 0x%x\n", (U32)(PA2NVA(u32PanelConfigsAddr)));
		memcpy(&panelpara, (U32*)(PA2NVA(u32PanelConfigsAddr)), sizeof(PanelType));
	}
	else
	{
		if(Read_PanelParaFromflash(&panelpara)!=0)
		{
			bPanleReady=FALSE;
			UBOOT_ERROR("%s: Read_PanelParaFromflash() failed, at %d\n", __func__, __LINE__);
			return -1;
		}
	}
#else
    if(Read_PanelParaFromflash(&panelpara)!=0)
    {
        bPanleReady=FALSE;
        UBOOT_ERROR("%s: Read_PanelParaFromflash() failed, at %d\n", __func__, __LINE__);
        return -1;
    }
#endif

    //load board para
    if(Read_BoardParaFromflash(&boardpara)!=0)
    {
        bPanleReady=FALSE;
        UBOOT_ERROR("%s: Read_BoardParaFromflash() failed, at %d\n", __func__, __LINE__);
        return -1;
    }

    //panel setting by each board
    panelpara.m_bPanelPDP10BIT = boardpara.m_bPANEL_PDP_10BIT;
    panelpara.m_bPanelSwapLVDS_POL = boardpara.m_bPANEL_SWAP_LVDS_POL;
    panelpara.m_bPanelSwapLVDS_CH = boardpara.m_bPANEL_SWAP_LVDS_CH;
    panelpara.m_bPanelSwapPort ^= boardpara.m_bPANEL_CONNECTOR_SWAP_PORT;
    panelpara.m_u16LVDSTxSwapValue = (boardpara.m_u16LVDS_PN_SWAP_H << 8) + boardpara.m_u16LVDS_PN_SWAP_L;
#if defined(CONFIG_URSA6_VB1)
	if(is_str_resume())
	{
        MDrv_Ursa_6M38_SWI2C_Init();
		if(panelpara.m_wPanelWidth == 3840 && panelpara.m_wPanelHeight == 2160)
		{
			UBOOT_DEBUG(">> m_ePanelLinkExtType = 45 <<<\n");
			setLinkExtType(45);
		}
		else if(panelpara.m_wPanelWidth == 1920 && panelpara.m_wPanelHeight == 1080)
		{
			UBOOT_DEBUG(">>> m_ePanelLinkExtType = 46 <<<\n");
			setLinkExtType(46);
		}
		else
		{
			UBOOT_DEBUG(">>> get m_ePanelLinkExtType from env <<<\n");
			setLinkExtType(GetPanelLinkExtType());
		}
	}
	else
	{
		UBOOT_DEBUG(">>> get m_ePanelLinkExtType from env <<<\n");
        MDrv_Ursa_6M38_SWI2C_Init();
		setLinkExtType(GetPanelLinkExtType());
	}
#else
#if 1 //(ENABLE_MSTAR_MONACO)
    if(panelpara.m_wPanelWidth == 3840 && panelpara.m_wPanelHeight == 2160)
    {
        UBOOT_DEBUG(">> m_ePanelLinkExtType = 51 <<<\n");
        setLinkExtType(51);
    }
    else
    {
        UBOOT_DEBUG(">>@ m_ePanelLinkExtType = %u <<<\n", GetPanelLinkExtType());
        setLinkExtType(GetPanelLinkExtType());
    }
#else
    setLinkExtType(GetPanelLinkExtType());
#endif
#endif
#if (ENABLE_ENABLE_URSA == 1)
#if (ENABLE_URSA_6M40 == 1)
    MDrv_Ursa_6M40_Set_VB1_Init(GetPanelLinkExtType());
#endif
#if (ENABLE_URSA_8 ==1 )||(ENABLE_URSA_6M40 == 1)
       ursa_cmd_table cmd_table={0};
	   if(Read_Ursa_Para(&cmd_table)==0)
	   {
	   	#if (ENABLE_URSA_8 == 1)
		   Ursa_8_Setting(&cmd_table);
		#else
			Ursa_6M40_Syetting(&cmd_table);
		#endif
	   }
	   else
	   {
		  UBOOT_ERROR("read ursa_8 data fail ...>>>\n");
	   }

#endif

#if (ENABLE_URSA_6M30 == 1)
    MDrv_Ursa_6M30_Initialize();
    if(bMst6m30Installed)
    {
        ursa_6m30_cmd_table cmd_table={0};
        if(Read_Ursa_6m30_Para(&cmd_table)==0)
        {
            Ursa_6M30_Setting(&cmd_table);
        }
        else
        {
           UBOOT_ERROR("read ursa_6m30 data fail ...>>>\n");
        }
    }
    else
    {
           UBOOT_ERROR("ursa_6m30 installed fail ...>>>\n");
    }
#endif
#endif
#if PANEL_DEBUG
    PanelType* panel_data = NULL;
    PANEL_RESOLUTION_TYPE enPanelType;
    enPanelType = MApi_PNL_GetPnlTypeSetting();
    MApi_PNL_PreInit(E_PNL_NO_OUTPUT);
    panel_data=MApi_XC_GetPanelSpec(enPanelType);
    cmp(panel_data,&panelpara);
#endif

    //pane init
#if (ENABLE_MSTAR_CLEANBOOT==1)
    //set swing level
    pSwingLevel = getenv("swing_level");
    u16Panel_SwingLevel = simple_strtoul(pSwingLevel, NULL, 10);
    //printf("do_panel_init  SwingLevel:%d, CONNECT_TYPE:%d\n",u16Panel_SwingLevel,boardpara.m_u16BOARD_LVDS_CONNECT_TYPE);
    MApi_PNL_MailBox_Init();
    flush_cache((MS_U32)(&panelpara), sizeof(panelpara));
    MApi_PNL_MailBox_SendParameters(MsOS_VA2PA((MS_U32)(&panelpara)), u16Panel_SwingLevel,boardpara.m_u16BOARD_LVDS_CONNECT_TYPE);
    //MApi_PNL_MailBox_SendParameters(PANEL_To_R2_ADDRESS, u16Panel_SwingLevel,boardpara.m_u16BOARD_LVDS_CONNECT_TYPE);
#else //(ENABLE_MSTAR_CLEANBOOT==1)
    char* pPanel_LVDS_Connect_Type = getenv("PANEL_LVDS_CONNECT_TYPE");
    if((pPanel_LVDS_Connect_Type != NULL) && (strncmp(pPanel_LVDS_Connect_Type,"0x04",4)==0))
    {
        MApi_BD_LVDS_Output_Type(0x04);//LVDS_OUTPUT_User , use to set MOD channels
        //MApi_PNL_MOD_OutputConfig_User(0x1550, 0x0155, 0x0000); //close Channel7 Channel13
    }
	else
	{
        MApi_BD_LVDS_Output_Type(boardpara.m_u16BOARD_LVDS_CONNECT_TYPE);
	}
    if(MsDrv_PNL_Init(&panelpara)==FALSE)
    {
        bPanleReady=FALSE;
        return -1;
    }

    //set swing level
    pSwingLevel = getenv("swing_level");
    if(pSwingLevel==NULL)
    {
        bPanleReady=FALSE;
        return -1;
    }
    u16Panel_SwingLevel = simple_strtoul(pSwingLevel, NULL, 10);
	#if (ENABLE_MSTAR_KAISER==0 && ENABLE_MSTAR_KAISERIN ==0)
    if(MApi_PNL_Control_Out_Swing(u16Panel_SwingLevel)!=TRUE)
    {
        bPanleReady=FALSE;
        return -1;
    }
	#endif
#endif
    bPanleReady=TRUE;

    UBOOT_TRACE("OK\n");
    return 0;
}

#if defined(LG_CHG)

int do_panel_vby1_setting(void)
{
	///////////////////////////////
	// VBY1 PATCH
	// *((volatile unsigned int *)0x1F000000+ bank *0x200+addr *4) = value;

	printf(" ~~~~ VBY1 TRAINING ~~~\n");
	   unsigned int vby1loop[6]={0x0f56,0x0fd6, 0x0f96, 0x0fb6, 0x0fbe, 0x0fae};
	 int i=0, j=0,k = 0, lockstatus;
	 for (k=0;k<5;k++)
	 {
		 for (i=0;i<6;i++)
		 {
			 for (j=0;j<6;j++)
			{
			   MS_PANEL__WRITE2BYTE(REG_MOD_BK00, 0x60,vby1loop[j]);
				udelay(5000);
			   }
			 udelay(10000);
		 }
		   udelay(100000);
	 }
		printf(" ~~~~ PANEL SETTING ~~~\n");

		*((volatile unsigned int *)(0x1F000000+ 0x1031*0x200+0xdc*2)) = 0x0100;
		// napoli Atop LDO open
		*((volatile unsigned int *)(0x1F000000+ 0x1032*0x200+0xf6*2)) = 0x0080;

		//-------------------------------------
		//				clkgen
		//-------------------------------------
		*((volatile unsigned int *)(0x1F000000+ 0x100b*0x200+0xae*2)) = 0x0008;  //[3:0] reg_ckg_fifo_mini
		*((volatile unsigned int *)(0x1F000000+ 0x100b*0x200+0xb0*2)) = 0x0000; //[3:0]ckg_tx_mod
		*((volatile unsigned int *)(0x1F000000+ 0x1033*0x200+0x62*2)) = 0x0000;  //[11:8]ckg_odclk_frc
		*((volatile unsigned int *)(0x1F000000+ 0x100A*0x200+0x12*2)) = 0x1800; //[12:8]ckg_vby1_fifo_osd [3:0]clk_vby1_fifo
		*((volatile unsigned int *)(0x1F000000+ 0x100B*0x200+0xa6*2)) = 0x000C; //[11:8]clk_bt656 [3:0]ckg_odclk --> clk_lpll_buf
		*((volatile unsigned int *)(0x1F000000+ 0x100B*0x200+0xc6*2)) = 0x0410;  //[11:8]ckg_tx_mod_osd[4:0]osd2mod

		  //---------------------------------------
		  //	   SCALER BK10 SETTING
		  //---------------------------------------
		*((volatile unsigned int *)(0x1F000000+ 0x1310*0x200+0x04*4)) = 0x0070;
		*((volatile unsigned int *)(0x1F000000+ 0x1310*0x200+0x05*4)) = 0x07EF;
  		*((volatile unsigned int *)(0x1F000000+ 0x1310*0x200+0x06*4)) = 0x0000;
  		*((volatile unsigned int *)(0x1F000000+ 0x1310*0x200+0x07*4)) = 0x0437;

		*((volatile unsigned int *)(0x1F000000+ 0x1310*0x200+0x08*4)) = 0x0070;
		*((volatile unsigned int *)(0x1F000000+ 0x1310*0x200+0x09*4)) = 0x07EF;
		*((volatile unsigned int *)(0x1F000000+ 0x1310*0x200+0x0A*4)) = 0x0000;
		*((volatile unsigned int *)(0x1F000000+ 0x1310*0x200+0x0B*4)) = 0x0437;

	//	*((volatile unsigned int *)(0x1F000000+ 0x1310*0x200+0x10*2)) = 0x0070;
	//	*((volatile unsigned int *)(0x1F000000+ 0x1310*0x200+0x12*2)) = 0x07EF;

		*((volatile unsigned int *)(0x1F000000+ 0x1310*0x200+0x18*2)) = 0x0897;  //output htotal
		*((volatile unsigned int *)(0x1F000000+ 0x1310*0x200+0x1A*2)) = 0x054B;//output Vtotal

		*((volatile unsigned int *)(0x1F000000+ 0x1031*0x200+0x04*2)) = 0x2202;  ///[11:8]ckg_tx_mod_osd[4:0]osd2mod
		*((volatile unsigned int *)(0x1F000000+ 0x1031*0x200+0x06*2)) = 0x0C07;  //[11:8]ckg_tx_mod_osd[4:0]osd2mod
		*((volatile unsigned int *)(0x1F000000+ 0x1031*0x200+0x1E*2)) = 0x892A; //[11:8]ckg_tx_mod_osd[4:0]osd2mod
		*((volatile unsigned int *)(0x1F000000+ 0x1031*0x200+0x20*2)) = 0x0012; //[11:8]ckg_tx_mod_osd[4:0]osd2mod
		*((volatile unsigned int *)(0x1F000000+ 0x1031*0x200+0x5C*2)) = 0x87D0; //[11:8]ckg_tx_mod_osd[4:0]osd2mod
		*((volatile unsigned int *)(0x1F000000+ 0x1031*0x200+0x6A*2)) = 0x0F10;  //[11:8]ckg_tx_mod_osd[4:0]osd2mod

		//---------------------------------------
		//		 MOD SETTING
		//---------------------------------------

		*((volatile unsigned int *)(0x1F000000+ 0x1032*0x200+0x3E*2)) = 0x0400;  //[11:8]ckg_tx_mod_osd[4:0]osd2mod
		*((volatile unsigned int *)(0x1F000000+ 0x1032*0x200+0x40*2)) = 0x0044;  //[11:8]ckg_tx_mod_osd[4:0]osd2mod

		*((volatile unsigned int *)(0x1F000000+ 0x1032*0x200+0x50*2)) = 0x0000;
		*((volatile unsigned int *)(0x1F000000+ 0x1032*0x200+0x52*2)) = 0x0000;
		*((volatile unsigned int *)(0x1F000000+ 0x1032*0x200+0x54*2)) = 0x7F7F;
		*((volatile unsigned int *)(0x1F000000+ 0x1032*0x200+0x56*2)) = 0x7F7F;
		*((volatile unsigned int *)(0x1F000000+ 0x1032*0x200+0x58*2)) = 0x7F7F;
		*((volatile unsigned int *)(0x1F000000+ 0x1032*0x200+0x5A*2)) = 0x7F7F;
		*((volatile unsigned int *)(0x1F000000+ 0x1032*0x200+0x5C*2)) = 0x0000;

		*((volatile unsigned int *)(0x1F000000+ 0x1032*0x200+0x66*2)) = 0x8000;
		*((volatile unsigned int *)(0x1F000000+ 0x1032*0x200+0x68*2)) = 0xF000;
		*((volatile unsigned int *)(0x1F000000+ 0x1032*0x200+0x76*2)) = 0xFFFF;
		*((volatile unsigned int *)(0x1F000000+ 0x1032*0x200+0x78*2)) = 0x0814;
		*((volatile unsigned int *)(0x1F000000+ 0x1032*0x200+0x7A*2)) = 0x6001;

		*((volatile unsigned int *)(0x1F000000+ 0x1032*0x200+0x80*2)) = 0x0000;
		*((volatile unsigned int *)(0x1F000000+ 0x1032*0x200+0x84*2)) = 0x1008;
		*((volatile unsigned int *)(0x1F000000+ 0x1032*0x200+0x96*2)) = 0x0002;

		*((volatile unsigned int *)(0x1F000000+ 0x1032*0x200+0xC2*2)) = 0x8F3F;
		*((volatile unsigned int *)(0x1F000000+ 0x1032*0x200+0xC4*2)) = 0xAA40;

		*((volatile unsigned int *)(0x1F000000+ 0x1032*0x200+0xDA*2)) = 0x0500;
		*((volatile unsigned int *)(0x1F000000+ 0x1032*0x200+0xDC*2)) = 0x0005;
		*((volatile unsigned int *)(0x1F000000+ 0x1032*0x200+0xDE*2)) = 0x3F00;

		*((volatile unsigned int *)(0x1F000000+ 0x1032*0x200+0xE2*2)) = 0x0FF0;
		*((volatile unsigned int *)(0x1F000000+ 0x1032*0x200+0xE6*2)) = 0x0FF0;
		*((volatile unsigned int *)(0x1F000000+ 0x1032*0x200+0xEE*2)) = 0xC01F;
		*((volatile unsigned int *)(0x1F000000+ 0x1032*0x200+0xF0*2)) = 0xC000;




		//---------------------------------------
		//		 MOD Test Pattern
		//---------------------------------------

		printf(" ~~~~DO MOD TEST PATTERN BY USER~~~\n");
		//*((volatile unsigned int *)(0x1F000000+ 0x1032*0x200+0x02*2)) = 0x8000; // Test Pattern Enable
		//*((volatile unsigned int *)(0x1F000000+ 0x1032*0x200+0x02*2)) = 0x03ff; // Red

		//---------------------------------------
		//		 VBY1 TRAINING
		//---------------------------------------
		printf(" ~~~~ VBY1 TRAINING ~~~\n");
		 for (k=0;k<5;k++)
		 {
			 for (i=0;i<6;i++)
			 {
				 for (j=0;j<6;j++)
				{
					 MS_PANEL__WRITE2BYTE(REG_MOD_BK00, 0x60,vby1loop[j]);
				    udelay(5000);
				   }
				 udelay(10000);
			 }
		       udelay(100000);
		 }

		j = 1;
		 for (k=0;k<50;k++)
		 {
		    lockstatus = *((volatile unsigned int *)(0x1F000000+ 0x1032*0x200+0x56*2));
		    if (lockstatus == 0x0000)
		    	{
		    	      j = 0;
				break;
		    	}
		   udelay(1000);
		 }

		 if (j)
		 {
			 for (k=0;k<5;k++)
			 {
				 for (i=0;i<6;i++)
				 {
					 for (j=0;j<6;j++)
					{
						 MS_PANEL__WRITE2BYTE(REG_MOD_BK00, 0x60,vby1loop[j]);
						udelay(5000);
					   }
					 udelay(10000);
				 }
				   udelay(100000);
			 }
		 }


	return 0;
}

#endif

int do_panel_pre_init(cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[])
{
    int ret = 0;
    UBOOT_TRACE("IN\n");
    bPreinit = true;
    if (argc < 2)
    {
#if (CONFIG_STATIC_PANEL_PARA)
        ret = panel_sinit();
#else
        ret = panel_dinit();
#endif
    }
    else
    {
#if CONFIG_MINIUBOOT
#else
       if(strncmp(argv[1], "-d", 2) == 0)
       {
            ret = panel_dinit();
       }
       else if (strncmp(argv[1], "-s", 2) == 0)
       {
            ret = panel_sinit();
       }
       else
#endif
       {
           cmd_usage(cmdtp);
       }
    }
    UBOOT_TRACE("OK\n");
    return ret;
}
int do_panel_init(cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[])
{
    int ret = 0;
    UBOOT_TRACE("IN\n");
    bPreinit = false;

    if (argc < 2)
    {
#if (CONFIG_STATIC_PANEL_PARA)
        ret = panel_sinit();
#else
        ret = panel_dinit();
#endif
    }
    else
    {
       if(strncmp(argv[1], "-d", 2) == 0)
       {
            ret = panel_dinit();
       }
       else if (strncmp(argv[1], "-s", 2) == 0)
       {
            ret = panel_sinit();
       }
       else
       {
           cmd_usage(cmdtp);
		   return ret;
       }
    }
	if(bPanleReady)
	{
   		MsDrv_PNL_BackLigth_On();
	}
    UBOOT_TRACE("OK\n");
    return ret;
}

int do_backLigth_on(cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[])
{
    int ret = 0;
    UBOOT_TRACE("IN\n");
	if(bPanleReady)
	{
   		MsDrv_PNL_BackLigth_On();
	}
	else
	{
		UBOOT_ERROR("do Panel init first!!\n");
	}
    UBOOT_TRACE("OK\n");
    return ret;
}

#if (ENABLE_MSTAR_PUMABOOT)
int r2_pbpnlinit(void)
{
    char *pSwingLevel;
    MS_U16 u16Panel_SwingLevel;
    static PanelType panelpara;
    st_board_para boardpara;

    UBOOT_TRACE("IN\n");
    memset(&panelpara, 0, sizeof(panelpara));
    memset(&boardpara, 0, sizeof(boardpara));

#if 1//
{
    PanelType* panel_data = NULL;
    printf("\n ============= do_panel_init : start =============\n");
    panel_data=MApi_XC_GetPanelSpec(PNL_FULLHD_CMO216_H1L01);
    memcpy(&panelpara, panel_data, sizeof(PanelType));
    panelpara.m_bPanelPDP10BIT = 1;
    panelpara.m_bPanelSwapLVDS_POL = 0;
    panelpara.m_bPanelSwapLVDS_CH = 0;
    panelpara.m_bPanelSwapPort ^= 0;
    panelpara.m_u16LVDSTxSwapValue = 0;
}
#else
    //load panel para
    if(Read_PanelParaFromflash(&panelpara)!=0)
    {
        bPanleReady=FALSE;
        UBOOT_ERROR("%s: Read_PanelParaFromflash() failed, at %d\n", __func__, __LINE__);
        return -1;
    }

    //load board para
    if(Read_BoardParaFromflash(&boardpara)!=0)
    {
        bPanleReady=FALSE;
        UBOOT_ERROR("%s: Read_BoardParaFromflash() failed, at %d\n", __func__, __LINE__);
        return -1;
    }

    //panel setting by each board
    panelpara.m_bPanelPDP10BIT = boardpara.m_bPANEL_PDP_10BIT;
    panelpara.m_bPanelSwapLVDS_POL = boardpara.m_bPANEL_SWAP_LVDS_POL;
    panelpara.m_bPanelSwapLVDS_CH = boardpara.m_bPANEL_SWAP_LVDS_CH;
    panelpara.m_bPanelSwapPort ^= boardpara.m_bPANEL_CONNECTOR_SWAP_PORT;
    panelpara.m_u16LVDSTxSwapValue = (boardpara.m_u16LVDS_PN_SWAP_H << 8) + boardpara.m_u16LVDS_PN_SWAP_L;
#endif

    pSwingLevel = 0;
    u16Panel_SwingLevel = 250;
    boardpara.m_u16BOARD_LVDS_CONNECT_TYPE = 3;
    flush_cache((MS_U32)(&panelpara), sizeof(panelpara));
    //printf("\n====================================  send mbx to R2 for panel init: ==================================== \n");
    MApi_PBMBX_SendMsgPanel(MsOS_VA2PA((MS_U32)(&panelpara)), u16Panel_SwingLevel,boardpara.m_u16BOARD_LVDS_CONNECT_TYPE);
    printf("\n ============= do_panel_init : finish =============\n");
    UBOOT_TRACE("OK\n");
    return 0;
}
#endif

#if (CONFIG_LOCAL_DIMMING)
#include <drvMSPI.h>
#include <drvMSPI_v2.h>
#include <drvLDMA.h>
#include <drvLDM.h>
#include <bootlogo/MsPoolDB.h>
#define MHAL_LD_PACKLENGTH    (32)
#define LDFALIGN              (8)          //align bits
#define LD_BIN_LENGTH         (0x80000)    //bin buffer
#define BRIGHT_LEVEL_DEFAULT  (0xff)
#define LD_MAX_BLOCK          (0)//(10*1024)    //follow kernel setting in file linaro\mstar2\drv\ldm\include\Mdrv_ldm_algorithm.h
#define LD_BUFFER_ADR                  "E_MMAP_ID_LD_BUFFER_ADR"
extern MMapInfo_t MMAPGer[MMAP_ID_MAX];
extern MiuInfo_t MiuInfo;

#define REG_SET_2BYTE(_reg_, _val_)    \
do{ (*((volatile MS_U16*)(_reg_))) = ((0xFFFF) & (_val_)); }while(0)

#define REG_SET_WL(_reg_, _val_)    \
do{ (*((volatile MS_U16*)(_reg_))) = (*((volatile MS_U16*)(_reg_))) | (1<<_val_); }while(0)

#define REG_CLEAR_WL(_reg_, _val_)    \
do{ (*((volatile MS_U16*)(_reg_))) = (*((volatile MS_U16*)(_reg_))) & (~(1<<_val_)); }while(0)

#define PM_RIU_BASE 0x1F000000UL
#define REG_BK132E_7B_L 0x132EF6
#define REG_BK000E_72_L 0x000EE4
#define REG_BK13F4_11_L 0x13F422 //PWM5 Period
#define REG_BK13F4_12_L 0x13F424 //PWM5 Duty
#define REG_BK13F4_13_L 0x13F426 //PWM5 LockEn and Reset
#define REG_BK13F4_32_L 0x13F464 //PWM5 Shift

int msAPI_LD_Init(void)
{
    ST_DRV_MSPI_INFO mspi_info;
    ST_DRV_LD_DMA_INFO dma_info;
    ST_LDM_CFG_MMAP_ADDR stLdmCfgAddr;
    MS_U8 * pLEDVirBuffer;
    MS_U32 LEDVirBufferSize;
    unsigned int u32BaseAddr = 0;
    unsigned int u32BaseAddr2 = 0;
    unsigned int u32Size = 0;
    MS_U8 u8Bright = BRIGHT_LEVEL_DEFAULT;
    MS_U32 u32LDFAddr_L0 = 0;
    MS_U32 u32LDFAddr_L1 = 0;
    MS_U32 u32LDFAddr_R0 = 0;
    MS_U32 u32LDFAddr_R1 = 0;
    MS_U32 u32LDBAddr_L0 = 0;
    MS_U32 u32LDBAddr_L1 = 0;
    MS_U32 u32LDBAddr_R0 = 0;
    MS_U32 u32LDBAddr_R1 = 0;
    MS_U32 LDF_pack_per_row = 0;
    MS_U32 LDF_mem_size = 0;
    MS_U8 u8LDBWidth = 0;
    MS_U8 u8LDBHeight = 0;
    MS_U32 LDB_pack_per_row = 0;
    MS_U32 LDB_mem_size = LD_MAX_BLOCK;
    MS_U32 u32LDBBaseAddr = 0;
    MS_U32 u32Edge2DBaseAddr = 0;
    MS_U32 u32DMABaseOffset = 0;

    UBOOT_TRACE("IN");
    /*if (argc >= 2) {
        u8Bright = simple_strtoul(argv[1], NULL, 16);
    }*/

    memset(&mspi_info,0,sizeof(ST_DRV_MSPI_INFO));
    memset(&dma_info,0,sizeof(ST_DRV_LD_DMA_INFO));

    /*if(0!=Read_MSPIPara_FromFash(&mspi_info) || 0!=Read_DMAPara_FromFash(&dma_info))
    {
        UBOOT_ERROR("Read_MSPIPara_FromFash fail\n");
    }*/

    mspi_info.u8MspiChanel = 0x00;
    mspi_info.u8MspiMode = 0x00;
    mspi_info.u32MspiClk = 1200000;

    mspi_info.u8WBitConfig[0] = 0x07;
    mspi_info.u8WBitConfig[1] = 0x07;
    mspi_info.u8WBitConfig[2] = 0x07;
    mspi_info.u8WBitConfig[3] = 0x04;
    mspi_info.u8WBitConfig[4] = 0x03;
    mspi_info.u8WBitConfig[5] = 0x02;
    mspi_info.u8WBitConfig[6] = 0x01;
    mspi_info.u8WBitConfig[7] = 0x00;

    mspi_info.u8RBitConfig[0] = 0x07;
    mspi_info.u8RBitConfig[1] = 0x07;
    mspi_info.u8RBitConfig[2] = 0x07;
    mspi_info.u8RBitConfig[3] = 0x04;
    mspi_info.u8RBitConfig[4] = 0x03;
    mspi_info.u8RBitConfig[5] = 0x02;
    mspi_info.u8RBitConfig[6] = 0x01;
    mspi_info.u8RBitConfig[7] = 0x00;

    mspi_info.u8TrStart = 0x0;
    mspi_info.u8TrEnd = 0x0;
    mspi_info.u8TB = 0x0;
    mspi_info.u8TRW = 0x0;
    

    UBOOT_DEBUG("spi_info.u8MspiChanel=: 0x%x\n",mspi_info.u8MspiChanel);
    UBOOT_DEBUG("spi_info.u8MspiMode=: 0x%x\n",mspi_info.u8MspiMode);
    UBOOT_DEBUG("spi_info.u32MspiClk=: %ld\n",mspi_info.u32MspiClk);
    UBOOT_DEBUG("spi_info.u8WBitConfig=: 0x%x,0x%x,0x%x,0x%x,0x%x,0x%x,0x%x,0x%x\n",mspi_info.u8WBitConfig[0],mspi_info.u8WBitConfig[1] \
                     ,mspi_info.u8WBitConfig[2],mspi_info.u8WBitConfig[3],mspi_info.u8WBitConfig[4],mspi_info.u8WBitConfig[5]
                     ,mspi_info.u8WBitConfig[6],mspi_info.u8WBitConfig[7]);
    UBOOT_DEBUG("spi_info.u8RBitConfig=: 0x%x,0x%x,0x%x,0x%x,0x%x,0x%x,0x%x,0x%x\n",mspi_info.u8RBitConfig[0],mspi_info.u8RBitConfig[1] \
                    ,mspi_info.u8RBitConfig[2],mspi_info.u8RBitConfig[3],mspi_info.u8RBitConfig[4],mspi_info.u8RBitConfig[5]
                    ,mspi_info.u8RBitConfig[6],mspi_info.u8RBitConfig[7]);
    UBOOT_DEBUG("spi_info.u8TrStart=: 0x%x\n",mspi_info.u8TrStart);
    UBOOT_DEBUG("spi_info.u8TrEnd=: 0x%x\n",mspi_info.u8TrEnd);
    UBOOT_DEBUG("spi_info.u8TB=: 0x%x\n",mspi_info.u8TB);
    UBOOT_DEBUG("spi_info.u8TRW=: 0x%x\n",mspi_info.u8TRW);

    dma_info.eLEDType = 0;
    dma_info.u8LEDWidth = 12;
    dma_info.u8LEDHeight = 1;
    dma_info.u8LDFWidth = 6;
    dma_info.u8LDFHeight = 1;
    dma_info.u8LSFWidth = 128;
    dma_info.u8LSFHeight = 72;
    dma_info.bEdge2DEn = 1; 

    dma_info.u8LDMAchanel = 0x0;
    dma_info.u8LDMATrimode = 0x02;
    dma_info.u8LDMACheckSumMode = 0x01;

    dma_info.u16DMADelay[0] = 0x1A00;
    dma_info.u16DMADelay[1] = 0x00;
    dma_info.u16DMADelay[2] = 0x1DFF;
    dma_info.u16DMADelay[3] = 0x00;

    dma_info.u16MspiHead[0] = 0x00AA;
    dma_info.u16MspiHead[1] = 0x0001;
    dma_info.u16MspiHead[2] = 0x0000;
    dma_info.u16MspiHead[3] = 0x0000;
    dma_info.u16MspiHead[4] = 0x0000;
    dma_info.u16MspiHead[5] = 0x0000;
    dma_info.u16MspiHead[6] = 0x0000;
    dma_info.u16MspiHead[7] = 0x0000;

    dma_info.u16LedNum = 6;
    dma_info.u8ClkHz = 60;

    UBOOT_DEBUG("dma_info.eLEDType=: 0x%x\n", dma_info.eLEDType);
    UBOOT_DEBUG("dma_info.u8LEDWidth=: %d\n", dma_info.u8LEDWidth);
    UBOOT_DEBUG("dma_info.u8LEDHeight=: %d\n", dma_info.u8LEDHeight);
    UBOOT_DEBUG("dma_info.u8LDFWidth=: %d\n", dma_info.u8LDFWidth);
    UBOOT_DEBUG("dma_info.u8LDFHeight=: %d\n", dma_info.u8LDFHeight);
    UBOOT_DEBUG("dma_info.u8LSFWidth=: %d\n", dma_info.u8LSFWidth);
    UBOOT_DEBUG("dma_info.u8LSFHeight=: %d\n", dma_info.u8LSFHeight);
    UBOOT_DEBUG("dma_info.bEdge2DEn=: 0x%x\n", dma_info.bEdge2DEn);

    UBOOT_DEBUG("dma_info.u8LDMAchanel=: 0x%x\n", dma_info.u8LDMAchanel);
    UBOOT_DEBUG("dma_info.u8LDMATrimode=: 0x%x\n", dma_info.u8LDMATrimode);
    UBOOT_DEBUG("dma_info.u8LDMACheckSumMode=: 0x%x\n", dma_info.u8LDMACheckSumMode);
    UBOOT_DEBUG("dma_info.u16DMADelay=: 0x%x, 0x%x, 0x%x, 0x%x\n", dma_info.u16DMADelay[0],dma_info.u16DMADelay[1]  \
                , dma_info.u16DMADelay[2],dma_info.u16DMADelay[3]);
    UBOOT_DEBUG("dma_info.u8cmdlength=: 0x%x\n", dma_info.u8cmdlength);
    UBOOT_DEBUG("dma_info.u16MspiHead=: 0x%x, 0x%x, 0x%x, 0x%x, 0x%x, 0x%x, 0x%x, 0x%x\n", dma_info.u16MspiHead[0],dma_info.u16MspiHead[1] \
      , dma_info.u16MspiHead[2],dma_info.u16MspiHead[3], dma_info.u16MspiHead[4],dma_info.u16MspiHead[5]
      , dma_info.u16MspiHead[6],dma_info.u16MspiHead[7]);
    UBOOT_DEBUG("dma_info.u16LedNum=: 0x%x\n", dma_info.u16LedNum);
    UBOOT_DEBUG("dma_info.u8ClkHz=: %d\n", dma_info.u8ClkHz);


    u32BaseAddr=MMAPGer[LD_BUFFER2].u32MemAddr;
    u32Size=MMAPGer[LD_BUFFER2].u32MemLength;

    //Init MSPI
    MDrv_MasterSPI_CsPadConfig(0,0xFF);//default cs pin use mspi config3
    MDrv_MSPI_Info_Config(&mspi_info);
    MDrv_MSPI_RWBytes(0,0x00);//set DMA read bytes
    MDrv_MSPI_RWBytes(1,0x01);//set DMA write bytes
    MDrv_MSPI_SlaveEnable(false);

    //LDF addr calc
    if(120 == dma_info.u8ClkHz)  //120Hz panel
    {
        LDF_pack_per_row = ((dma_info.u8LDFWidth/2 *4 - 1) / MHAL_LD_PACKLENGTH) + 1;
    }
    else
    {
        LDF_pack_per_row = ((dma_info.u8LDFWidth *4 - 1) / MHAL_LD_PACKLENGTH) + 1;
    }
    LDF_mem_size = LDF_pack_per_row * MHAL_LD_PACKLENGTH * MHAL_LD_PACKLENGTH;
    LDF_mem_size = ((LDF_mem_size + 0xFF) >> LDFALIGN) << LDFALIGN; // align at 0x100

    stLdmCfgAddr.u32LDFAddr0_l = u32BaseAddr/MHAL_LD_PACKLENGTH;
    stLdmCfgAddr.u32LDFAddr1_l = (u32BaseAddr + LDF_mem_size)/MHAL_LD_PACKLENGTH;

    if (120 == dma_info.u8ClkHz)
    {
        stLdmCfgAddr.u32LDFAddr0_r = (u32BaseAddr + 2*LDF_mem_size)/MHAL_LD_PACKLENGTH;
        stLdmCfgAddr.u32LDFAddr1_r = (u32BaseAddr + 3*LDF_mem_size)/MHAL_LD_PACKLENGTH;
    }
    else
    {
        stLdmCfgAddr.u32LDFAddr0_r = stLdmCfgAddr.u32LDFAddr0_l;
        stLdmCfgAddr.u32LDFAddr1_r = stLdmCfgAddr.u32LDFAddr1_l;
    }

    //LDB addr calc
    if(120 == dma_info.u8ClkHz)  //120Hz panel
    {
        u8LDBWidth = dma_info.bEdge2DEn
                      ? (dma_info.eLEDType == E_LD_EDGE_LR_TYPE ? dma_info.u8LEDHeight: dma_info.u8LEDWidth)
                      : dma_info.u8LEDWidth/2 + 1;
    }
    else
    {
        u8LDBWidth = dma_info.bEdge2DEn
                      ? (dma_info.eLEDType == E_LD_EDGE_LR_TYPE ? dma_info.u8LEDHeight: dma_info.u8LEDWidth)
                      : dma_info.u8LEDWidth;
    }

    u8LDBHeight = dma_info.bEdge2DEn
                   ? (dma_info.eLEDType == E_LD_EDGE_LR_TYPE ? dma_info.u8LEDWidth : dma_info.u8LEDHeight)
                   : dma_info.u8LEDHeight;
    LDB_pack_per_row = (dma_info.bEdge2DEn && dma_info.eLEDType == E_LD_LOCAL_TYPE) ? 2 :
                                ((u8LDBWidth - 1) / MHAL_LD_PACKLENGTH) + 1;

    if(120 == dma_info.u8ClkHz)
    {
        u32LDBBaseAddr = u32BaseAddr + 4*LDF_mem_size;
        stLdmCfgAddr.u32Edge2DAddr = u32LDBBaseAddr + 2*LDB_mem_size;
    }
    else
    {
        u32LDBBaseAddr = u32BaseAddr + 2*LDF_mem_size;
        stLdmCfgAddr.u32Edge2DAddr = u32LDBBaseAddr + 2*LDB_mem_size;
    }

    stLdmCfgAddr.u32LDBAddr0_l = u32LDBBaseAddr / MHAL_LD_PACKLENGTH;
    stLdmCfgAddr.u32LDBAddr1_l = (u32LDBBaseAddr + LDB_mem_size)/ MHAL_LD_PACKLENGTH;

    if (120 == dma_info.u8ClkHz)
    {
        stLdmCfgAddr.u32LDBAddr0_r = (u32LDBBaseAddr + 2*LDB_mem_size) / MHAL_LD_PACKLENGTH;
        stLdmCfgAddr.u32LDBAddr1_r = (u32LDBBaseAddr + 3*LDB_mem_size) / MHAL_LD_PACKLENGTH;
    }
    else
    {
        stLdmCfgAddr.u32LDBAddr0_r = stLdmCfgAddr.u32LDBAddr0_l;
        stLdmCfgAddr.u32LDBAddr1_r = stLdmCfgAddr.u32LDBAddr1_l;
    }
    stLdmCfgAddr.u32DataOffset = ((stLdmCfgAddr.u32LDFAddr0_l*0x20) + LD_BIN_LENGTH - (stLdmCfgAddr.u32LDBAddr0_l*0x20)) / MHAL_LD_PACKLENGTH;

    pLEDVirBuffer = (MS_U8 *)MS_PA2KSEG1(u32BaseAddr + LD_BIN_LENGTH);
    LEDVirBufferSize = LD_BIN_LENGTH;
    memset(pLEDVirBuffer, u8Bright, LEDVirBufferSize);    //set Bright value to LED buffer

    UBOOT_ERROR("u32LDFAddr_L0 = 0x%lx\n", stLdmCfgAddr.u32LDFAddr0_l);
    UBOOT_ERROR("u32LDBAddr_L0 = 0x%lx\n", stLdmCfgAddr.u32LDBAddr0_l);

    //UBOOT_ERROR("u32DataOffset addr = 0x%lx , 0x%lx ,0x%lx\n", stLdmCfgAddr.u32DataOffset, stLdmCfgAddr.u32LDFAddr0_l*0x20, stLdmCfgAddr.u32LDBAddr0_l*0x20);
    //UBOOT_ERROR("pLEDVirBuffer addr = 0x%lx , 0x%lx ,0x%lx\n", (MS_U32)pLEDVirBuffer, u32BaseAddr, u32BaseAddr2);
    UBOOT_ERROR("pLEDVirBuffer addr = 0x%lx\n", (MS_U32)pLEDVirBuffer);
    UBOOT_ERROR("LEDVirBufferSize = 0x%lx\n", LEDVirBufferSize);
    UBOOT_ERROR("u8Bright = 0x%x\n", u8Bright);

    MDrv_LDMA_Init(dma_info.u8LDMAchanel, dma_info.u8ClkHz);
    MDrv_LDMA_SetMenuloadNumber(dma_info.u8LDMAchanel, dma_info.u16LedNum);
    MDrv_LDMA_SetSPICommandFormat(dma_info.u8LDMAchanel, dma_info.u8cmdlength, dma_info.u16MspiHead);
    MDrv_LDMA_SetCheckSumMode(dma_info.u8LDMAchanel, dma_info.u8LDMACheckSumMode);
    MDrv_LDMA_SetSpiTriggerMode(dma_info.u8LDMAchanel, dma_info.u8LDMATrimode);
    MDrv_LDMA_SetTrigDelay(dma_info.u8LDMAchanel, dma_info.u16DMADelay);
    MDrv_LDMA_EnableCS(dma_info.u8LDMAchanel, true);

    MDrv_LDM_SetIOMapBase(dma_info.u8ClkHz);
    MDrv_LDM_Config_MmapAddr(&stLdmCfgAddr);

    MDrv_LDM_SetMIUPackFormat(dma_info.u8LDMAchanel,0, dma_info.u8LEDWidth -1);

    MDrv_LDM_SetYoffEnd(dma_info.u8LDMAchanel,dma_info.u8LEDHeight -1);

    MDrv_LDM_SetBlLdmaSize(dma_info.u8LEDHeight -1,dma_info.u8LEDWidth -1);

#if 1//STELLAR
    //ENABLE for PWM trigger LD to gen Vsync
    MS_U32 u32reg_vsync, u32reg_pm_padmux_sel_protn, u32reg_pwm5_period, u32reg_pwm5_duty, u32reg_pwm5_locken, u32reg_pwm5_shift;
    u32reg_vsync = (REG_BK132E_7B_L)<<1; //ENABLE for PWM trigger LD to gen Vsync
    u32reg_vsync += PM_RIU_BASE;
    REG_SET_WL(u32reg_vsync, 15); //0x132E[15] ENABLE

    //config reg_ld_spi3_config reg_tee_pm_padmux_sel_protn path select
    u32reg_pm_padmux_sel_protn = (REG_BK000E_72_L)<<1; // EPI M+ DoubleBuffer
    u32reg_pm_padmux_sel_protn += PM_RIU_BASE;
    REG_SET_WL(u32reg_pm_padmux_sel_protn, 15);

    //Set PWM5 to 120Hz
    //Set Period = 120Hz
    u32reg_pwm5_period = (REG_BK13F4_11_L)<<1;
    u32reg_pwm5_period += PM_RIU_BASE;
    REG_SET_2BYTE(u32reg_pwm5_period, 0x8334);

    //Set Duty Like Vsync
    u32reg_pwm5_duty = (REG_BK13F4_12_L)<<1;
    u32reg_pwm5_duty += PM_RIU_BASE;
    REG_SET_2BYTE(u32reg_pwm5_duty, 0x003A);//0x003A

    //Set Locken
    u32reg_pwm5_locken = (REG_BK13F4_13_L)<<1;
    u32reg_pwm5_locken += PM_RIU_BASE;
    REG_SET_2BYTE(u32reg_pwm5_locken, 0x4602);

    //Set Shift = 1
    u32reg_pwm5_shift = (REG_BK13F4_32_L)<<1;
    u32reg_pwm5_shift += PM_RIU_BASE;
    REG_SET_2BYTE(u32reg_pwm5_shift, 0x0001);

#endif 

    ///MDrv_LDMA_LD_SetDmaEnable(dma_info.u8LDMAchanel, true);
    MDrv_LDM_SetDmaEnable(dma_info.u8LDMAchanel,true);

    MDrv_LDM_Enable();

    UBOOT_TRACE("OK\n");
    return 0;
}

#endif

#if PANEL_DEBUG
void cmp(PanelType *p1, PanelType *p2)
{
    if((p1 ==NULL) || (p2==NULL))
    {
        printf("null return\n");
        return;
    }
//    printf("compare: '%s', '%s'\n", p1->m_pPanelName, p2->m_pPanelName);
    if(p1->m_bPanelDither != p2->m_bPanelDither)
    {
        printf("diff: '%u', '%u', at %u\n", p1->m_bPanelDither, p2->m_bPanelDither, __LINE__);
    }
    if(p1->m_ePanelLinkType != p2->m_ePanelLinkType)
    {
        printf("diff: '%u', '%u', at %u\n", p1->m_ePanelLinkType, p2->m_ePanelLinkType, __LINE__);
    }
    if(p1->m_bPanelDualPort != p2->m_bPanelDualPort)
    {
        printf("diff: '%u', '%u', at %u\n", p1->m_bPanelDualPort, p2->m_bPanelDualPort, __LINE__);
    }
    if(p1->m_bPanelSwapPort != p2->m_bPanelSwapPort)
    {
        printf("diff: '%u', '%u', at %u\n", p1->m_bPanelSwapPort, p2->m_bPanelSwapPort, __LINE__);
    }
    if(p1->m_bPanelSwapOdd_ML != p2->m_bPanelSwapOdd_ML)
    {
        printf("diff: '%u', '%u', at %u\n", p1->m_bPanelSwapOdd_ML, p2->m_bPanelSwapOdd_ML, __LINE__);
    }
    if(p1->m_bPanelSwapEven_ML != p2->m_bPanelSwapEven_ML)
    {
        printf("diff: '%u', '%u', at %u\n", p1->m_bPanelSwapEven_ML, p2->m_bPanelSwapEven_ML, __LINE__);
    }
    if(p1->m_bPanelSwapOdd_RB != p2->m_bPanelSwapOdd_RB)
    {
        printf("diff: '%u', '%u', at %u\n", p1->m_bPanelSwapOdd_RB, p2->m_bPanelSwapOdd_RB, __LINE__);
    }
    if(p1->m_bPanelSwapEven_RB != p2->m_bPanelSwapEven_RB)
    {
        printf("diff: '%u', '%lu', at %u\n", p1->m_bPanelSwapEven_RB, p2->m_bPanelSwapEven_RB, __LINE__);
    }
    if(p1->m_bPanelSwapLVDS_POL != p2->m_bPanelSwapLVDS_POL)
    {
        printf("diff: '%u', '%lu', at %u\n", p1->m_bPanelSwapLVDS_POL, p2->m_bPanelSwapLVDS_POL, __LINE__);
    }
    if(p1->m_bPanelSwapLVDS_CH != p2->m_bPanelSwapLVDS_CH)
    {
        printf("diff: '%u', '%u', at %u\n", p1->m_bPanelSwapLVDS_CH, p2->m_bPanelSwapLVDS_CH, __LINE__);
    }
    if(p1->m_bPanelPDP10BIT != p2->m_bPanelPDP10BIT)
    {
        printf("diff: '%u', '%u', at %u\n", p1->m_bPanelPDP10BIT, p2->m_bPanelPDP10BIT, __LINE__);
    }
    if(p1->m_bPanelLVDS_TI_MODE != p2->m_bPanelLVDS_TI_MODE)
    {
        printf("diff: '%u', '%u', at %u\n", p1->m_bPanelLVDS_TI_MODE, p2->m_bPanelLVDS_TI_MODE, __LINE__);
    }
    if(p1->m_ucPanelDCLKDelay != p2->m_ucPanelDCLKDelay)
    {
        printf("diff: '%u', '%u', at %u\n", p1->m_ucPanelDCLKDelay, p2->m_ucPanelDCLKDelay, __LINE__);
    }
    if(p1->m_bPanelInvDCLK != p2->m_bPanelInvDCLK)
    {
        printf("diff: '%u', '%u', at %u\n", p1->m_bPanelInvDCLK, p2->m_bPanelInvDCLK, __LINE__);
    }
    if(p1->m_bPanelInvDE != p2->m_bPanelInvDE)
    {
        printf("diff: '%u', '%u', at %u\n", p1->m_bPanelInvDE, p2->m_bPanelInvDE, __LINE__);
    }
    if(p1->m_bPanelInvHSync != p2->m_bPanelInvHSync)
    {
        printf("diff: '%u', '%u', at %u\n", p1->m_bPanelInvHSync, p2->m_bPanelInvHSync, __LINE__);
    }
    if(p1->m_bPanelInvVSync != p2->m_bPanelInvVSync)
    {
        printf("diff: '%u', '%u', at %u\n", p1->m_bPanelInvVSync, p2->m_bPanelInvVSync, __LINE__);
    }
    if(p1->m_ucPanelDCKLCurrent != p2->m_ucPanelDCKLCurrent)
    {
        printf("diff: '%u', '%u', at %u\n", p1->m_ucPanelDCKLCurrent, p2->m_ucPanelDCKLCurrent, __LINE__);
    }
    if(p1->m_ucPanelDECurrent != p2->m_ucPanelDECurrent)
    {
        printf("diff: '%u', '%u', at %u\n", p1->m_ucPanelDECurrent, p2->m_ucPanelDECurrent, __LINE__);
    }
    if(p1->m_ucPanelODDDataCurrent != p2->m_ucPanelODDDataCurrent)
    {
        printf("diff: '%u', '%u', at %u\n", p1->m_ucPanelODDDataCurrent, p2->m_ucPanelODDDataCurrent, __LINE__);
    }
    if(p1->m_ucPanelEvenDataCurrent != p2->m_ucPanelEvenDataCurrent)
    {
        printf("diff: '%u', '%u', at %u\n", p1->m_ucPanelEvenDataCurrent, p2->m_ucPanelEvenDataCurrent, __LINE__);
    }
    if(p1->m_wPanelOnTiming1 != p2->m_wPanelOnTiming1)
    {
        printf("diff: '%u', '%u', at %u\n", p1->m_wPanelOnTiming1, p2->m_wPanelOnTiming1, __LINE__);
    }
    if(p1->m_wPanelOnTiming2 != p2->m_wPanelOnTiming2)
    {
        printf("diff: '%u', '%u', at %u\n", p1->m_wPanelOnTiming2, p2->m_wPanelOnTiming2, __LINE__);
    }
    if(p1->m_wPanelOffTiming1 != p2->m_wPanelOffTiming1)
    {
        printf("diff: '%u', '%u', at %u\n", p1->m_wPanelOffTiming1, p2->m_wPanelOffTiming1, __LINE__);
    }
    if(p1->m_wPanelOffTiming2 != p2->m_wPanelOffTiming2)
    {
        printf("diff: '%u', '%u', at %u\n", p1->m_wPanelOffTiming2, p2->m_wPanelOffTiming2, __LINE__);
    }
    if(p1->m_ucPanelHSyncWidth != p2->m_ucPanelHSyncWidth)
    {
        printf("diff: '%u', '%u', at %u\n", p1->m_ucPanelHSyncWidth, p2->m_ucPanelHSyncWidth, __LINE__);
    }
    if(p1->m_ucPanelHSyncBackPorch != p2->m_ucPanelHSyncBackPorch)
    {
        printf("diff: '%u', '%u', at %u\n", p1->m_ucPanelHSyncBackPorch, p2->m_ucPanelHSyncBackPorch, __LINE__);
    }
    if(p1->m_ucPanelVSyncWidth != p2->m_ucPanelVSyncWidth)
    {
        printf("diff: '%u', '%u', at %u\n", p1->m_ucPanelVSyncWidth, p2->m_ucPanelVSyncWidth, __LINE__);
    }
    if(p1->m_ucPanelVBackPorch != p2->m_ucPanelVBackPorch)
    {
        printf("diff: '%u', '%u', at %u\n", p1->m_ucPanelVBackPorch, p2->m_ucPanelVBackPorch, __LINE__);
    }
    if(p1->m_wPanelHStart != p2->m_wPanelHStart)
    {
        printf("diff: '%u', '%u', at %u\n", p1->m_wPanelHStart, p2->m_wPanelHStart, __LINE__);
    }
    if(p1->m_wPanelVStart != p2->m_wPanelVStart)
    {
        printf("diff: '%u', '%u', at %u\n", p1->m_wPanelVStart, p2->m_wPanelVStart, __LINE__);
    }
    if(p1->m_wPanelWidth != p2->m_wPanelWidth)
    {
        printf("diff: '%u', '%u', at %u\n", p1->m_wPanelWidth, p2->m_wPanelWidth, __LINE__);
    }
    if(p1->m_wPanelHeight != p2->m_wPanelHeight)
    {
        printf("diff: '%u', '%u', at %u\n", p1->m_wPanelHeight, p2->m_wPanelHeight, __LINE__);
    }
    if(p1->m_wPanelMaxHTotal != p2->m_wPanelMaxHTotal)
    {
        printf("diff: '%u', '%u', at %u\n", p1->m_wPanelMaxHTotal, p2->m_wPanelMaxHTotal, __LINE__);
    }
    if(p1->m_wPanelHTotal != p2->m_wPanelHTotal)
    {
        printf("diff: '%u', '%u', at %u\n", p1->m_wPanelHTotal, p2->m_wPanelHTotal, __LINE__);
    }
    if(p1->m_wPanelMinHTotal != p2->m_wPanelMinHTotal)
    {
        printf("diff: '%u', '%u', at %u\n", p1->m_wPanelMinHTotal, p2->m_wPanelMinHTotal, __LINE__);
    }
    if(p1->m_wPanelMaxVTotal != p2->m_wPanelMaxVTotal)
    {
        printf("diff: '%u', '%u', at %u\n", p1->m_wPanelMaxVTotal, p2->m_wPanelMaxVTotal, __LINE__);
    }
    if(p1->m_wPanelVTotal != p2->m_wPanelVTotal)
    {
        printf("diff: '%u', '%u', at %u\n", p1->m_wPanelVTotal, p2->m_wPanelVTotal, __LINE__);
    }
    if(p1->m_wPanelMinVTotal != p2->m_wPanelMinVTotal)
    {
        printf("diff: '%u', '%u', at %u\n", p1->m_wPanelMinVTotal, p2->m_wPanelMinVTotal, __LINE__);
    }
    if(p1->m_dwPanelMaxDCLK != p2->m_dwPanelMaxDCLK)
    {
        printf("diff: '%u', '%u', at %u\n", p1->m_dwPanelMaxDCLK, p2->m_dwPanelMaxDCLK, __LINE__);
    }
    if(p1->m_dwPanelDCLK != p2->m_dwPanelDCLK)
    {
        printf("diff: '%u', '%u', at %u\n", p1->m_dwPanelDCLK, p2->m_dwPanelDCLK, __LINE__);
    }
    if(p1->m_dwPanelMinDCLK != p2->m_dwPanelMinDCLK)
    {
        printf("diff: '%u', '%u', at %u\n", p1->m_dwPanelMinDCLK, p2->m_dwPanelMinDCLK, __LINE__);
    }
    if(p1->m_wSpreadSpectrumStep != p2->m_wSpreadSpectrumStep)
    {
        printf("diff: '%u', '%u', at %u\n", p1->m_wSpreadSpectrumStep, p2->m_wSpreadSpectrumStep, __LINE__);
    }
    if(p1->m_wSpreadSpectrumSpan != p2->m_wSpreadSpectrumSpan)
    {
        printf("diff: '%u', '%u', at %u\n", p1->m_wSpreadSpectrumSpan, p2->m_wSpreadSpectrumSpan, __LINE__);
    }
    if(p1->m_ucDimmingCtl != p2->m_ucDimmingCtl)
    {
        printf("diff: '%u', '%u', at %u\n", p1->m_ucDimmingCtl, p2->m_ucDimmingCtl, __LINE__);
    }
    if(p1->m_ucMaxPWMVal != p2->m_ucMaxPWMVal)
    {
        printf("diff: '%u', '%u', at %u\n", p1->m_ucMaxPWMVal, p2->m_ucMaxPWMVal, __LINE__);
    }
    if(p1->m_ucMinPWMVal != p2->m_ucMinPWMVal)
    {
        printf("diff: '%u', '%u', at %u\n", p1->m_ucMinPWMVal, p2->m_ucMinPWMVal, __LINE__);
    }
    if(p1->m_bPanelDeinterMode != p2->m_bPanelDeinterMode)
    {
        printf("diff: '%u', '%u', at %u\n", p1->m_bPanelDeinterMode, p2->m_bPanelDeinterMode, __LINE__);
    }
    if(p1->m_ucPanelAspectRatio != p2->m_ucPanelAspectRatio)
    {
        printf("diff: '%u', '%u', at %u\n", p1->m_ucPanelAspectRatio, p2->m_ucPanelAspectRatio, __LINE__);
    }
    if(p1->m_u16LVDSTxSwapValue != p2->m_u16LVDSTxSwapValue)
    {
        printf("diff: '%u', '%u', at %u\n", p1->m_u16LVDSTxSwapValue, p2->m_u16LVDSTxSwapValue, __LINE__);
    }
    if(p1->m_ucTiBitMode != p2->m_ucTiBitMode)
    {
        printf("diff: '%u', '%u', at %u\n", p1->m_ucTiBitMode, p2->m_ucTiBitMode, __LINE__);
    }
    if(p1->m_ucOutputFormatBitMode != p2->m_ucOutputFormatBitMode)
    {
        printf("diff: '%u', '%u', at %u\n", p1->m_ucOutputFormatBitMode, p2->m_ucOutputFormatBitMode, __LINE__);
    }
    if(p1->m_bPanelSwapOdd_RG != p2->m_bPanelSwapOdd_RG)
    {
        printf("diff: '%u', '%u', at %u\n", p1->m_bPanelSwapOdd_RG, p2->m_bPanelSwapOdd_RG, __LINE__);
    }
    if(p1->m_bPanelSwapEven_RG != p2->m_bPanelSwapEven_RG)
    {
        printf("diff: '%u', '%u', at %u\n", p1->m_bPanelSwapEven_RG, p2->m_bPanelSwapEven_RG, __LINE__);
    }
    if(p1->m_bPanelSwapOdd_GB != p2->m_bPanelSwapOdd_GB)
    {
        printf("diff: '%u', '%u', at %u\n", p1->m_bPanelSwapOdd_GB, p2->m_bPanelSwapOdd_GB, __LINE__);
    }
    if(p1->m_bPanelSwapEven_GB != p2->m_bPanelSwapEven_GB)
    {
        printf("diff: '%u', '%u', at %u\n", p1->m_bPanelSwapEven_GB, p2->m_bPanelSwapEven_GB, __LINE__);
    }
    if(p1->m_bPanelDoubleClk != p2->m_bPanelDoubleClk)
    {
        printf("diff: '%u', '%u', at %u\n", p1->m_bPanelDoubleClk, p2->m_bPanelDoubleClk, __LINE__);
    }
    if(p1->m_dwPanelMaxSET != p2->m_dwPanelMaxSET)
    {
        printf("diff: '%lu', '%lu', at %u\n", p1->m_dwPanelMaxSET, p2->m_dwPanelMaxSET, __LINE__);
    }
    if(p1->m_dwPanelMinSET != p2->m_dwPanelMinSET)
    {
        printf("diff: '%lu', '%lu', at %u\n", p1->m_dwPanelMinSET, p2->m_dwPanelMinSET, __LINE__);
    }
    if(p1->m_ucOutTimingMode != p2->m_ucOutTimingMode)
    {
        printf("diff: '%u', '%u', at %u\n", p1->m_ucOutTimingMode, p2->m_ucOutTimingMode, __LINE__);
    }
    if(p1->m_bPanelNoiseDith != p2->m_bPanelNoiseDith)
    {
        printf("diff: '%u', '%u', at %u\n", p1->m_bPanelNoiseDith, p2->m_bPanelNoiseDith, __LINE__);
    }
}
#endif //PANEL_DEBUG
