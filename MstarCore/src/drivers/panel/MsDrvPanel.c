//<MStar Software>
//******************************************************************************
// MStar Software
// Copyright (c) 2010 - 2012 MStar Semiconductor, Inc. All rights reserved.
// All software, firmware and related documentation herein ("MStar Software") are
// intellectual property of MStar Semiconductor, Inc. ("MStar") and protected by
// law, including, but not limited to, copyright law and international treaties.
// Any use, modification, reproduction, retransmission, or republication of all
// or part of MStar Software is expressly prohibited, unless prior written
// permission has been granted by MStar.
//
// By accessing, browsing and/or using MStar Software, you acknowledge that you
// have read, understood, and agree, to be bound by below terms ("Terms") and to
// comply with all applicable laws and regulations:
//
// 1. MStar shall retain any and all right, ownership and interest to MStar
//    Software and any modification/derivatives thereof.
//    No right, ownership, or interest to MStar Software and any
//    modification/derivatives thereof is transferred to you under Terms.
//
// 2. You understand that MStar Software might include, incorporate or be
//    supplied together with third party`s software and the use of MStar
//    Software may require additional licenses from third parties.
//    Therefore, you hereby agree it is your sole responsibility to separately
//    obtain any and all third party right and license necessary for your use of
//    such third party`s software.
//
// 3. MStar Software and any modification/derivatives thereof shall be deemed as
//    MStar`s confidential information and you agree to keep MStar`s
//    confidential information in strictest confidence and not disclose to any
//    third party.
//
// 4. MStar Software is provided on an "AS IS" basis without warranties of any
//    kind. Any warranties are hereby expressly disclaimed by MStar, including
//    without limitation, any warranties of merchantability, non-infringement of
//    intellectual property rights, fitness for a particular purpose, error free
//    and in conformity with any international standard.  You agree to waive any
//    claim against MStar for any loss, damage, cost or expense that you may
//    incur related to your use of MStar Software.
//    In no event shall MStar be liable for any direct, indirect, incidental or
//    consequential damages, including without limitation, lost of profit or
//    revenues, lost or damage of data, and unauthorized system use.
//    You agree that this Section 4 shall still apply without being affected
//    even if MStar Software has been modified by MStar in accordance with your
//    request or instruction for your use, except otherwise agreed by both
//    parties in writing.
//
// 5. If requested, MStar may from time to time provide technical supports or
//    services in relation with MStar Software to you for your use of
//    MStar Software in conjunction with your or your customer`s product
//    ("Services").
//    You understand and agree that, except otherwise agreed by both parties in
//    writing, Services are provided on an "AS IS" basis and the warranty
//    disclaimer set forth in Section 4 above shall apply.
//
// 6. Nothing contained herein shall be construed as by implication, estoppels
//    or otherwise:
//    (a) conferring any license or right to use MStar name, trademark, service
//        mark, symbol or any other identification;
//    (b) obligating MStar or any of its affiliates to furnish any person,
//        including without limitation, you and your customers, any assistance
//        of any kind whatsoever, or any information; or
//    (c) conferring any license or right under any intellectual property right.
//
// 7. These terms shall be governed by and construed in accordance with the laws
//    of Taiwan, R.O.C., excluding its conflict of law rules.
//    Any and all dispute arising out hereof or related hereto shall be finally
//    settled by arbitration referred to the Chinese Arbitration Association,
//    Taipei in accordance with the ROC Arbitration Law and the Arbitration
//    Rules of the Association by three (3) arbitrators appointed in accordance
//    with the said Rules.
//    The place of arbitration shall be in Taipei, Taiwan and the language shall
//    be English.
//    The arbitration award shall be final and binding to both parties.
//
//******************************************************************************
//<MStar Software>

#include <common.h>
#include <command.h>
#include <config.h>
#include <configs/uboot_board_config.h>
#include <MsTypes.h>

#include <MsCommon.h>
#include <MsVersion.h>
#include <MsUboot.h>
#if (ENABLE_MSTAR_KAISER==1) //K3
#include <apiPNL_EX.h>
#endif
#include <apiPNL.h>
#include <drvPWM.h>

#include <panel/MsDrvPanel.h>
#include <panel/Gamma12BIT_256.inc>
#include <MsApiPM.h>
#include <MsSystem.h>

#include <MsMmap.h>
#if(CONFIG_HDMITX_MSTAR_ROCKET ==1)
#include <apiHDMITx.h>
#endif

#if(ENABLE_URSA_6M30 == 1)
#include <ursa/ursa_6m30.h>
#endif
#if(ENABLE_URSA_8 == 1)
#include <ursa/ursa_8.h>
#endif
#if(ENABLE_URSA6_VB1 == 1 || ENABLE_NOVA_KS2 == 1)
#include <ursa/ursa_6m38.h>
#endif
#if(ENABLE_URSA7_VB1 == 1)
#include <ursa/ursa_7.h>
#endif
#include <mstarstr.h>

//for LG EPI Setting
#include <CusCmnio.h>
extern MODELOPT_T gModelOpt;
extern char aHW_model_opt[NUM_MODEL_OPT+1];

#include <cmnio_type.h>


#define PWM_DUTY   "u32PWMDuty"
#define PWM_PERIOD "u32PWMPeriod"
#define PWM_DIVPWM "u16DivPWM"
#define MIRROR_ON "MIRROR_ON"
#define PWM_POLPWM "bPolPWM"
#define PWM_MAXPWM "u16MaxPWMvalue"
#define PWM_MINPWM "u16MinPWMvalue"
#define PWM_CH "pnl_pwmch"
#include <bootlogo/MsPoolDB.h>
#include <MsDebug.h>

//-------------------------------------------------------------------------------------------------
//  Global Variables
//-------------------------------------------------------------------------------------------------
unsigned long lPanleTimer =0;
unsigned long lPanelOnTiming=0;
//--------------------------------------------------------------------------------------------------
// Local Variables
//--------------------------------------------------------------------------------------------------
static unsigned char PanelLinkExtType=10;

#define SHARP_VBY1 1

#if (CONFIG_XC_FRC_VB1)

#include <apiXC.h>

#endif

#if defined(CONFIG_A3_STB)
#include <drvTVEncoder.h>
#include <drvXC_IOPort.h>
#include <apiXC.h>
#include <apiXC_Adc.h>
#include <drvGPIO.h>
#include <apiGOP.h>



MS_BOOL mvideo_sc_is_interlace(void)
{
    return 0;
}

void verDispCvbs_Main(void)
{
    MS_Switch_VE_Dest_Info SwitchOutputDest;
    MS_VE_Output_Ctrl OutputCtrl;
    MS_VE_Set_Mode_Type SetModeType;
    //XC_INITDATA XC_InitData;
	MS_U16 u16OutputVfreq;
    U32 u32Addr;
	XC_INITDATA XC_InitData;
	GOP_InitInfo pGopInit;
	//xc init
	UBOOT_TRACE("IN\n");
	memset((void *)&XC_InitData, 0, sizeof(XC_InitData));
    MApi_XC_Init(&XC_InitData, 0);
	MApi_XC_SetDispWindowColor(0x82,MAIN_WINDOW);
	memset((void *)&pGopInit, 0, sizeof(pGopInit));
	MApi_GOP_RegisterXCIsInterlaceCB(mvideo_sc_is_interlace);
	MApi_GOP_Init(&pGopInit);
	MApi_GOP_GWIN_OutputColor(GOPOUT_YUV);

    MApi_GOP_SetGOPHStart(0, g_IPanel.HStart());
    MApi_GOP_SetGOPHStart(1, g_IPanel.HStart());
    if(get_addr_from_mmap("E_MMAP_ID_VE", &u32Addr)!=0)
    {
        UBOOT_ERROR("get E_MMAP_ID_VE fail\n");
        return -1;
    }
	MDrv_VE_Init(u32Addr);//MDrv_VE_Init(VE_BUFFER_ADDR);
	MApi_XC_SetOutputCapture(ENABLE, E_XC_OP2);     // Enable op2 to ve path

	u16OutputVfreq = MApi_XC_GetOutputVFreqX100()/10;
    if(u16OutputVfreq > 550)
    {
        //60 hz for NTSC
        MDrv_VE_SetOutputVideoStd(MS_VE_NTSC);
		SetModeType.u16InputVFreq = (3000 * 2) / 10;
    }
    else
    {
        //50 hz for PAL
        MDrv_VE_SetOutputVideoStd(MS_VE_PAL);
		SetModeType.u16InputVFreq = (2500 * 2) / 10;
    }


    MS_Switch_VE_Src_Info SwitchInputSrc;
    SwitchInputSrc.InputSrcType = MS_VE_SRC_SCALER;
    MDrv_VE_SwitchInputSource(&SwitchInputSrc);
	MDrv_VE_SetRGBIn(false);

    SwitchOutputDest.OutputDstType = MS_VE_DEST_CVBS;
    MDrv_VE_SwitchOuputDest(&SwitchOutputDest);

    SetModeType.u16H_CapSize     = g_IPanel.Width();
    SetModeType.u16V_CapSize     = g_IPanel.Height();
    SetModeType.u16H_CapStart    = g_IPanel.HStart();
    SetModeType.u16V_CapStart    = g_IPanel.VStart();
    SetModeType.u16H_SC_CapSize  = g_IPanel.Width();
    SetModeType.u16V_SC_CapSize  = g_IPanel.Height();
    SetModeType.u16H_SC_CapStart = g_IPanel.HStart();
    SetModeType.u16V_SC_CapStart = g_IPanel.VStart();

	SetModeType.bHDuplicate = FALSE;
	SetModeType.bSrcInterlace = FALSE;

    MDrv_VE_SetMode(&SetModeType);

    OutputCtrl.bEnable = TRUE;
    OutputCtrl.OutputType = MS_VE_OUT_TVENCODER;

    MDrv_VE_SetOutputCtrl(&OutputCtrl);
	MDrv_VE_SetBlackScreen(FALSE);
	MApi_XC_ADC_SetCVBSOut(ENABLE, OUTPUT_CVBS1, INPUT_SOURCE_DTV, TRUE);

    UBOOT_TRACE("OUT\n");

}
#endif

#define LVDS_OUTPUT_USER          4
#if(ENABLE_URSA_6M30 == 1)
#define PANEL_4K2K_ENABLE         0
#endif
#if (CONFIG_PANEL_INIT)

void SetPWM(U16 Period,U16 Duty,U16 PwmDiv,PWM_ChNum PWMPort)
{
    PWM_Result result = E_PWM_FAIL;

    U16 bPolPWM = 0;
    char *p_str=NULL;


    result = MDrv_PWM_Init(E_PWM_DBGLV_NONE);
    if(result == E_PWM_FAIL)
    {
        UBOOT_DEBUG("Sorry , init failed!~\n");
        return ;
    }
	p_str = getenv(PWM_POLPWM);
		if(p_str == NULL)
		{
			UBOOT_DEBUG("PWM period in env is NULL,so use default value 0xC350\n");
			setenv(PWM_POLPWM,"0x1");
		}
		else
		{
			bPolPWM = simple_strtol(p_str, NULL, 16);
		}
		UBOOT_DEBUG("\nbPolPWM_T = %d\n",bPolPWM);

    //MDrv_PWM_UnitDiv(0); //duplicate  MDrv_PWM_Div
    MDrv_PWM_Oen(PWMPort, 0);    /* Set 0 for output enable */
    MDrv_PWM_Period(PWMPort, Period);
    MDrv_PWM_DutyCycle(PWMPort, Duty);
    MDrv_PWM_Div(PWMPort, PwmDiv);
	if(bPolPWM==1)
    MDrv_PWM_Polarity(PWMPort, TRUE);
	else
    MDrv_PWM_Polarity(PWMPort, FALSE);
    MDrv_PWM_Vdben(PWMPort, FALSE);
    MDrv_PWM_Dben(PWMPort,FALSE);
}

int PWM_init(void)
{
    U16 u32PWMDuty = 0;
    U16 u32PWMPeriod = 0;
    U16 u32PWMDIV = 0;
	U16 u16maxPWM =0;
	U16 u16minPWM =0;
	PWM_ChNum PWMPort = 0;
    char *p_str=NULL;
    // printf("getenv duty[%s]\n",getenv(PWM_DUTY));
    // printf("getenv period[%s]\n",getenv(PWM_PERIOD));
    p_str = getenv(PWM_DIVPWM);
    if(p_str == NULL)
    {
        UBOOT_DEBUG("PWM period in env is NULL,so use default value 0xC350\n");
        setenv(PWM_DIVPWM,"0x1FF");
    }
    else
    {
        u32PWMDIV = simple_strtol(p_str, NULL, 16);
    }

    p_str = getenv(PWM_PERIOD);
    if(p_str == NULL)
    {
        UBOOT_DEBUG("PWM period in env is NULL,so use default value 0xC350\n");
        setenv(PWM_PERIOD,"0xC350");
    }
    else
    {
        u32PWMPeriod = simple_strtol(p_str, NULL, 16);
    }

	p_str = getenv(PWM_MAXPWM);
	if(p_str == NULL)
	{
		UBOOT_DEBUG("PWM PWM_MAXPWM in env is NULL,so use default value 0xC350\n");
		setenv(PWM_PERIOD,"0xC350");
	}
	else
	{
		u16maxPWM = simple_strtol(p_str, NULL, 16);
	}
	p_str = getenv(PWM_MINPWM);
	if(p_str == NULL)
	{
		UBOOT_DEBUG("PWM PWM_MAXPWM in env is NULL,so use default value 0xC350\n");
		setenv(PWM_PERIOD,"0xC350");
	}
	else
	{
		u16minPWM = simple_strtol(p_str, NULL, 16);
	}

	p_str = getenv(PWM_CH);
	if( p_str == NULL )
	{
      UBOOT_DEBUG("PWM CH not specified ,So use default CH2\n");
      PWMPort = (PWM_ChNum)E_PWM_CH2;
	}
	else
	{
      PWMPort = (PWM_ChNum)simple_strtol(p_str, NULL, 16);
      if(PWMPort < 0 || PWMPort >9)
      {
        UBOOT_DEBUG("PWM CH out of range ,So use default CH2\n");
        PWMPort = (PWM_ChNum)E_PWM_CH2;
      }
	}
    u32PWMDuty = (u16maxPWM+u16minPWM)/2;

    UBOOT_DEBUG("u32PWMPeriod_T = %d\n",u32PWMPeriod);
    UBOOT_DEBUG("u32PWMDuty_T = %d\n",u32PWMDuty);
    UBOOT_DEBUG("u32PWMDIV_T = %d\n",u32PWMDIV);
    UBOOT_DEBUG("u16maxPWM_T = %d\n",u16maxPWM);
    UBOOT_DEBUG("u16minPWM_T = %d\n",u16minPWM);

    //[LMTASKWBS-3760][M2][PWM] 2 ports of PWM should has the same phase
    //SetPWM(u32PWMPeriod,u32PWMDuty, u32PWMDIV,PWMPort);
    return 0;
}

//#if (ENABLE_TCON_PANEL == 1)
#include <MsOS.h>
#include <drvXC_IOPort.h>
#include <apiXC.h>
#include <panel/pnl_tcon_tbl.h>
#include <panel/pnl_tcon_tbl.c>

#define TABLE_HEADER_OFFSET      0x00
#define TABLE_SUB_HEADER_OFFSET  0x20

#define SUB_HEADER_START_OFFSET       0x08
#define SUB_HEADER_TYPE_OFFSET        0x00
#define SUB_HEADER_REG_COUNT_OFFSET   0x01
#define SUB_HEADER_REG_LIST_OFFSET    0x04
#define PER_SUB_HEADER                0x08

#define PER_NUM_REG_COUNT             0x06

#define TABLE_ADDR_OFFSET_LOW    0x00
#define TABLE_ADDR_OFFSET_LOW_2  0x01
#define TABLE_ADDR_OFFSET_HIGH   0x02
#define TABLE_ADDR_OFFSET_HIGH_2 0x03
#define TABLE_MASK_OFFSET        0x04
#define TABLE_VALUE_OFFSET       0x05
#define TABLE_NEED_DELAY_OFFSET  0x06
#define TABLE_DELAY_TIME_OFFSET  0x07

#define POWER_IC_I2C_CHANNEL     0x05
#define POWER_IC_I2C_TRANS_MODE   100

#define POWER_IC_I2C_DATA_LENGTH 0x1a
#define POWER_IC_I2C_SLAVE_ID    0x40
#define POWER_IC_I2C_SUBADR_MODE    0
#define POWER_IC_I2C_RETRY       0x03

static U8 _tcon_dat_43inch[] =
{
    #include "tcon_43inch.dat"
};
static U8 _tcon_dat_49inch[] =
{
    #include "tcon_49inch.dat"
};
static U8 _tcon_dat_55inch[] =
{
    #include "tcon_55inch.dat"
};
static U8 _tcon_dat_65inch[] =
{
    #include "tcon_65inch.dat"
};


static U8 _tcon20_43inch_6lane_off[] =
{
    #include "TCON20_EPI_43In_6L_Off.dat"
};

static U8 _tcon20_43inch_6lane_on[] =
{
    #include "TCON20_EPI_43In_6L_On.dat"
};

static U8 _tcon20_49inch_6lane_off[] =
{
    #include "TCON20_EPI_49In_6L_Off.dat"
};

static U8 _tcon20_49inch_6lane_on[] =
{
    #include "TCON20_EPI_49In_6L_On.dat"
};
static U8 _tcon20_55inch_6lane_off[] =
{
    #include "TCON20_EPI_55In_6L_Off.dat"
};
static U8 _tcon20_55inch_6lane_on[] =
{
    #include "TCON20_EPI_55In_6L_On.dat"
};
static U8 _tcon20_55inch_6lane_uk65_on[] =
{
    #include "TCON20_EPI_55In_6L_UK65_On.dat"
};
static U8 _tcon20_65inch_8lane_off[] =
{
    #include "TCON20_EPI_65In_8L_Off.dat"
};
static U8 _tcon20_65inch_8lane_on[] =
{
    #include "TCON20_EPI_65In_8L_On.dat"
};
static U8 _tcon20_65inch_12lane_off[] =
{
    #include "TCON20_CEDS_65In_12L_Off.dat"
};
static U8 _tcon20_55inch_12lane_on[] =
{
    #include "TCON20_CEDS_55In_12L_On.dat"
};
static U8 _tcon20_43inch_12lane_on[] =
{
    #include "TCON20_CEDS_43In_12L_On.dat"
};
static U8 _tcon20_49inch_12lane_on[] =
{
    #include "TCON20_CEDS_49In_12L_On.dat"
};

#if 0
static U8 _tcon20_49inch_6lane_off_A0[] =
{
    #include "../../../include//A0/TCON20_EPI_49In_6L_Off.dat"
};

static U8 _tcon20_49inch_6lane_on_A0[] =
{
    #include "../../../include//A0/TCON20_EPI_49In_6L_On.dat"
};
static U8 _tcon20_55inch_6lane_off_A0[] =
{
    #include "../../../include//A0/TCON20_EPI_55In_6L_Off.dat"
};
static U8 _tcon20_55inch_6lane_on_A0[] =
{
    #include "../../../include//A0/TCON20_EPI_55In_6L_On.dat"
};
static U8 _tcon20_65inch_8lane_off_A0[] =
{
    #include "../../../include//A0/TCON20_EPI_65In_8L_Off.dat"
};
static U8 _tcon20_65inch_8lane_on_A0[] =
{
    #include "../../../include//A0/TCON20_EPI_65In_8L_On.dat"
};
static U8 _tcon20_49inch_12lane_off_A0[] =
{
    #include "../../../include/A0/TCON20_CEDS_49In_12L_Off.dat"
};
static U8 _tcon20_49inch_12lane_on_A0[] =
{
    #include "../../../include//A0/TCON20_CEDS_49In_12L_On.dat"
};
#endif


static U8 *ptcon_table = NULL;

EN_TCON_TYPE_IDX_T e_tcontype = E_TCON_NONE;
EN_TCON_TYPE_IDX_T g_panel_list[TCON_NUM_MAX]=
{
	E_TCON_NONE, //0
	E_TCON_NONE, //1
	E_TCON_NONE, //2
	E_TCON_NONE, //3
	E_TCON_V18_EPI_55INCH_6LANE_MPLUSOFF, //4
	E_TCON_V18_EPI_55INCH_6LANE_MPLUSON, //5
	E_TCON_V17_EPI_55INCH_6LANE_UK6500PVA_MPLUSON, //6
	E_TCON_NONE, //7
	E_TCON_V18_CEDS_65INCH_12LANE_MPLUSOFF, //8
	E_TCON_V18_CEDS_55INCH_12LANE_MPLUSON, //9
	E_TCON_NONE, //10
	E_TCON_NONE, //11
	E_TCON_V18_EPI_65INCH_8LANE_MPLUSOFF, //12
	E_TCON_V18_EPI_65INCH_8LANE_MPLUSON, //13
	E_TCON_V18_EPI_49INCH_6LANE_MPLUSOFF, //14
	E_TCON_V18_EPI_49INCH_6LANE_MPLUSON, //15	
};


U8 *ptr = NULL;
U8 *pTable = NULL;

//------------Bin Header information--------------
MS_U16 u16BinID = 0;
MS_U16 u16CustomerID = 0;
MS_U16 u16BinCheckSum = 0;
MS_U32 u32DataLength = 0;
char DescriptionBuffer[18];

//------------Bin Sub-Header Information----------
MS_U8 u8TwoDTableNum = 0;
MS_U8 u8ThreeDTableNum = 0;
MS_U8 u8TconSubTableType = 0;
MS_U16 u16RegCount = 0;
MS_U8 u8RegType = 0;
MS_U32 u32RegListOffset = 0; //data offset
//---------------------------------------------

int Init_Power_IC(void)
{
    UBOOT_TRACE("IN\n");

    uint8_t data[POWER_IC_I2C_DATA_LENGTH] =  {0x61,0x99,0x09,0x09,0x1F,0x00,0x01,0x03,0x00,
 	        0x03,0xF6,0x03,0x15,0x02,0x1E,0x01,0xF4,0x01,0x0B,0x00,0x45,0x00,0x9B,0x04,0xB0,0x08};

	mdrv_gpio_init();
	DDI_CMNIO_I2C_Init();
    if( DDI_CMNIO_I2C_Write(POWER_IC_I2C_CHANNEL, POWER_IC_I2C_TRANS_MODE, POWER_IC_I2C_SLAVE_ID, POWER_IC_I2C_SUBADR_MODE, 0, POWER_IC_I2C_DATA_LENGTH, data, POWER_IC_I2C_RETRY) == -1)
    {
 	    UBOOT_TRACE("Init_Power_IC Init Failed\n");
        return -1;
    }
	UBOOT_TRACE("Init_Power_IC Init OK\n");
    UBOOT_TRACE("OK\n");
    return 0;

}

void Judge_Sub_Table(TCON_TAB_INFO* pTcontab)
{
   UBOOT_TRACE("IN\n");
   U8* data_ptr = ptr;
   u8TconSubTableType = *(pTable + SUB_HEADER_START_OFFSET + SUB_HEADER_TYPE_OFFSET);

   u16RegCount =(pTable[SUB_HEADER_START_OFFSET + SUB_HEADER_REG_COUNT_OFFSET] |
			    (pTable[SUB_HEADER_START_OFFSET + SUB_HEADER_REG_COUNT_OFFSET+0x01] << 8))*PER_NUM_REG_COUNT;

   u32RegListOffset = (pTable[SUB_HEADER_START_OFFSET + SUB_HEADER_REG_LIST_OFFSET] |
   	                  (pTable[SUB_HEADER_START_OFFSET + SUB_HEADER_REG_LIST_OFFSET+0x01] << 8) |
                      (pTable[SUB_HEADER_START_OFFSET + SUB_HEADER_REG_LIST_OFFSET+0x02] << 16)|
                      (pTable[SUB_HEADER_START_OFFSET + SUB_HEADER_REG_LIST_OFFSET+0x03] << 24));

   data_ptr = data_ptr + (u32RegListOffset);

   pTable = pTable + PER_SUB_HEADER;

   switch(u8TconSubTableType)
   {
	 case 0x0:
	    pTcontab->pTConInitTab = data_ptr;
		break;
	 case 0x1:
	    pTcontab->pTConInit_GPIOTab = data_ptr;
		break;
	 case 0x2:
	    pTcontab->pTConInit_SCTab = data_ptr;
		break;
	 case 0x3:
	    pTcontab->pTConInit_MODTab = data_ptr;
		break;
	 case 0x4:
	    pTcontab->pTConGammaTab = data_ptr;
		break;
	 case 0x5:
	    pTcontab->pTConPower_Sequence_OnTab = data_ptr;
		break;
	 case 0x6:
	    pTcontab->pTConPower_Sequence_OffTab = data_ptr;
		break;

	 default:
	 	pTcontab->pTConInitTab = data_ptr;
  }

  UBOOT_TRACE("OK\n");

}

void SetColumnInv(MS_BOOL bEnable)
{
    if (bEnable) { // Column inversion
        // 0x103072[4:3] (8bit address)  = 0
        MDrv_WriteByte(0x103072, (MDrv_ReadByte(0x103072) & ~BIT4) & ~BIT3);
        // 0x10301a(8bit address) = 0x0000  //Vst
        MDrv_WriteByte(0x10301a, 0x00);
        MDrv_WriteByte(0x10301b, 0x00);
    } else { // V2dot inversion
        // 0x103072[4:3] (8bit address)  = 2
        MDrv_WriteByte(0x103072, (MDrv_ReadByte(0x103072) | BIT4) & ~BIT3);
        // 0x10301a(8bit address) = 0x0001  //Vst
        MDrv_WriteByte(0x10301a, 0x01);
        MDrv_WriteByte(0x10301b, 0x00);
    }
}

extern int DDI_NVM_GetToolOpt1(TOOL_OPTION1_T *pOpt1);
void Load_TCON_Table(TCON_TAB_INFO* pTcontab)
{

  UBOOT_TRACE("IN\n");

  char buffer[CMD_BUF] = "\0";
  MS_U32 u32tabIdx = 0;
  MS_U32 u32Addr = 0;
  MS_U32 u32DataAddr = 0;
  MS_U8 u8Mask = 0;
  MS_U8 u8Value = 0, u8ReadValue = 0;
  MS_U8  u8signal_type = 0;
  MS_U8  u8SubHeader_Count = 0;
  int ret = 0;
  UBOOT_TRACE("IN\n");

  TOOL_OPTION1_T      toolOpt1;

  mdrv_gpio_init();
  DDI_CMNIO_I2C_Init();
  DDI_NVM_GetToolOpt1(&toolOpt1);

  switch(toolOpt1.flags.eModelInchType)
  {
	 case 23:
		ptr = _tcon_dat_43inch;
		pTable = _tcon_dat_43inch + TABLE_SUB_HEADER_OFFSET;
		break;
	 case 4:
        ptr = _tcon_dat_49inch;
        pTable = _tcon_dat_49inch + TABLE_SUB_HEADER_OFFSET;
		break;
	 case 6:
	    ptr = _tcon_dat_55inch;
		pTable = _tcon_dat_55inch + TABLE_SUB_HEADER_OFFSET;
		break;
	 case 9:
	    ptr = _tcon_dat_65inch;
		pTable = _tcon_dat_65inch + TABLE_SUB_HEADER_OFFSET;
		break;
	 default:
	 	ptr = _tcon_dat_43inch;
		pTable = _tcon_dat_43inch + TABLE_SUB_HEADER_OFFSET;
  }

  u32DataLength = ptr[TABLE_HEADER_OFFSET+0x06] | (ptr[TABLE_HEADER_OFFSET+0x07] << 8) |
                  (ptr[TABLE_HEADER_OFFSET+0x08] << 16) | (ptr[TABLE_HEADER_OFFSET+0x09] << 24);

  u32DataAddr = u32RegListOffset = (pTable[SUB_HEADER_START_OFFSET + SUB_HEADER_REG_LIST_OFFSET] |
   	                   (pTable[SUB_HEADER_START_OFFSET + SUB_HEADER_REG_LIST_OFFSET+0x01] << 8) |
                       (pTable[SUB_HEADER_START_OFFSET + SUB_HEADER_REG_LIST_OFFSET+0x02] << 16)|
                       (pTable[SUB_HEADER_START_OFFSET + SUB_HEADER_REG_LIST_OFFSET+0x03] << 24));

  while(1)
  {
	 u32Addr  = (ptr[u32tabIdx+TABLE_ADDR_OFFSET_LOW]<<24) | (ptr[u32tabIdx+TABLE_ADDR_OFFSET_LOW_2]<<16) | (ptr[u32tabIdx+TABLE_ADDR_OFFSET_HIGH]<<8) | ptr[u32tabIdx+TABLE_ADDR_OFFSET_HIGH_2];
	 u8Mask	  = ptr[(u32tabIdx + TABLE_MASK_OFFSET)];
	 u8Value  = ptr[(u32tabIdx + TABLE_VALUE_OFFSET)];

     if(u32tabIdx >= u32DataAddr)
     {
		 u8ReadValue = MDrv_ReadByte(u32Addr);
         u8ReadValue &= ~(u8Mask);
		 u8Value |= u8ReadValue;
         MDrv_WriteByte(u32Addr,u8Value);
     }
     if( u32tabIdx == 0 || u32tabIdx == u32RegListOffset + u16RegCount || u16RegCount == 0)
     {
         Judge_Sub_Table(pTcontab);
	 }

	  if(u8TconSubTableType == 5 || u8TconSubTableType == 6)break;  //power on/off temp

	 if (u32tabIdx == u32DataLength) // check end of table
	 {
		 ptr[u32tabIdx+1] = 0xff;
		 ptr[u32tabIdx+2] = 0xff;
		 break;
	 }
	 u32tabIdx += PER_NUM_REG_COUNT;

  }
//Delay
  MDrv_WriteByte(0x103285, 0x00);
  MDrv_WriteByte(0x103285, 0x10);

  SetColumnInv(0); // set V2dot inversion as default

  UBOOT_TRACE("OK\n");

}


E_TCON_TAB_TYPE eTCONPNL_TypeSel = TCON_TABTYPE_GENERAL;
static void _MApi_TCon_Tab_DumpPSRegTab(MS_U8* pu8TconTab)
{
    #define TCON_DUMP_TIMEOUT_TIME  1000
    MS_U32 u32tabIdx = 0;
    MS_U32 u32Addr;
    MS_U16 u16Mask;
    MS_U16 u16Value;
    MS_U8 u8NeedDelay;
    MS_U8 u8DelayTime;
    MS_U32 u32StartTime = MsOS_GetSystemTime();
    UBOOT_TRACE("IN\n");


    if (pu8TconTab == NULL)
    {
        UBOOT_ERROR("pu8TconTab is NULL! at %s %u \n", __FUNCTION__, __LINE__);
        return;
    }

    while(1)
    {
        if( (MsOS_GetSystemTime() - u32StartTime) > TCON_DUMP_TIMEOUT_TIME )
        {
            UBOOT_ERROR("Dump tcon tab timeout! at %s %u \n", __FUNCTION__, __LINE__);
            return;
        }

        u32Addr     = ((pu8TconTab[u32tabIdx+ TABLE_ADDR_OFFSET_LOW]<<8) + pu8TconTab[(u32tabIdx + TABLE_ADDR_OFFSET_HIGH)]) & 0xFFFF;
        u16Mask     = pu8TconTab[(u32tabIdx + TABLE_MASK_OFFSET)] & 0xFF;
        u16Value    = pu8TconTab[(u32tabIdx + TABLE_VALUE_OFFSET)] & 0xFF;
        u8NeedDelay = pu8TconTab[(u32tabIdx + TABLE_NEED_DELAY_OFFSET)] & 0xFF;
        u8DelayTime = pu8TconTab[(u32tabIdx + TABLE_DELAY_TIME_OFFSET)] & 0xFF;

        if (u32Addr == REG_TABLE_END) // check end of table
            break;

        u32Addr = (u32Addr | 0x100000);

        if( u32Addr == REG_TCON_BASE )
        {
            UBOOT_DEBUG("Wait V sync \n");
            MApi_XC_WaitOutputVSync(1, 50, MAIN_WINDOW);
        }

        if (u32Addr & 0x1)
        {
            u32Addr --;
            //temp_mask, need check!! MApi_PNL_Write2ByteMask(u32Addr, (u16Value << 8), (u16Mask << 8));//MApi_XC_Write2ByteMask
            MApi_XC_Write2ByteMask(u32Addr, (u16Value << 8), (u16Mask << 8));
//            UBOOT_DEBUG("[Odd .addr=%04lx, msk=%02x, val=%02x] \n", u32Addr, (u16Mask << 8), (u16Value << 8));
        }
        else
        {
            MApi_XC_Write2ByteMask(u32Addr, u16Value, u16Mask);
            //temp_mask, need check!! MApi_PNL_Write2ByteMask(u32Addr, u16Value, u16Mask);//MApi_XC_Write2ByteMask
//            UBOOT_DEBUG("[Even.addr=%04lx, msk=%02x, val=%02x] \n", u32Addr, u16Mask, u16Value);
        }
        // Check need delay?
        if( u8NeedDelay && u8DelayTime )
        {
            mdelay(u8DelayTime);
        }
        u32tabIdx = u32tabIdx + 7;
    }
    UBOOT_TRACE("OK\n");
}


static void _MApi_XC_Sys_Init_TCON_Panel(TCON_TAB_INFO* pTcontab)
{
    UBOOT_TRACE("IN\n");
	// TCON init
    MApi_PNL_TCON_Init();

    // dump TCON general register tab
    MApi_PNL_TCONMAP_DumpTable(pTcontab->pTConInitTab, E_APIPNL_TCON_TAB_TYPE_GENERAL);

    // dump TCON mod register tab
    MApi_PNL_TCONMAP_DumpTable(pTcontab->pTConInit_MODTab, E_APIPNL_TCON_TAB_TYPE_MOD);

    // dump TCON GPIO register tab
    MApi_PNL_TCONMAP_DumpTable(pTcontab->pTConInit_GPIOTab, E_APIPNL_TCON_TAB_TYPE_GPIO);

    // dump TCON gamma register tab
    #if 0
    {
        _MApi_XC_Sys_Init_TCON_GAMMA(pTcontab->pTConGammaTab);
    }
    #endif

    _MApi_TCon_Tab_DumpPSRegTab(pTcontab->pTConPower_Sequence_OnTab);

	UBOOT_TRACE("OK\n");
}

// TCon register type
typedef enum
{
    EN_TCON_REGISTERTYPE_6_BYTES = 0,       // 32-bit address, 4+1+1, address+mask+value
    EN_TCON_REGISTERTYPE_4_BYTES,       // 16-bit address, 2+1+1, address+mask+value
    EN_TCON_REGISTERTYPE_POWERSEQENCE_9_BYTES,      // 32-bit address, 4+1+1+1+1+1, address+mask+value+delayReady+delayTime+signalType
    EN_TCON_REGISTERTYPE_POWERSEQENCE_7_BYTES,      // 16-bit address, 2+1+1+1+1+1, address+mask+value+delayReady+delayTime+signalType
    EN_TCON_REGISTERTYPE_POWERSEQENCE_WITHSUB_15_BYTES, // 32-bit address, 4+1+1+4+1+1+1+1+1, subAddress+subMask+subValue+address+mask+value+delayReady+delayTime+signalType
    EN_TCON_REGISTERTYPE_POWERSEQENCE_WITHSUB_11_BYTES, // 16-bit address, 2+1+1+2+1+1+1+1+1, subAddress+subMask+subValue+address+mask+value+delayReady+delayTime+signalType
    EN_TCON_REGISTERTYPE_POWERGAMMA_4_BYTES, // Register type 6: P-Gamma (Power Gamma) 4-byte register format (16-bit address size and 16-bit value size)
    EN_TCON_REGISTERTYPE_3_BYTES, // Register type 7: TCON 3-byte register format (12-bit address size)
    EN_TCON_REGISTERTYPE_PANEL_1_BYTES, // Register type 8: panel
    EN_TCON_REGISTERTYPE_END,
} EN_TCON_REGISTERTYPE;

#define TCON_REGISTERTYPE_1_BYTES   1
#define TCON_REGISTERTYPE_6_BYTES   6
#define TCON_REGISTERTYPE_4_BYTES   4
#define TCON_REGISTERTYPE_POWERSEQENCE_9_BYTES   9
#define TCON_REGISTERTYPE_POWERSEQENCE_7_BYTES   7
#define TCON_REGISTERTYPE_POWERSEQENCE_WITHSUB_15_BYTES   15
#define TCON_REGISTERTYPE_POWERSEQENCE_WITHSUB_11_BYTES   11

unsigned int _registerTypeToSize(unsigned char registerType)
{
    EN_TCON_REGISTERTYPE en = (EN_TCON_REGISTERTYPE)registerType;
    switch (en)
    {
        case EN_TCON_REGISTERTYPE_6_BYTES:
            return TCON_REGISTERTYPE_6_BYTES;
        case EN_TCON_REGISTERTYPE_4_BYTES:
            return TCON_REGISTERTYPE_4_BYTES;
        case EN_TCON_REGISTERTYPE_POWERSEQENCE_9_BYTES:
            return TCON_REGISTERTYPE_POWERSEQENCE_9_BYTES;
        case EN_TCON_REGISTERTYPE_POWERSEQENCE_7_BYTES:
            return TCON_REGISTERTYPE_POWERSEQENCE_7_BYTES;
        case EN_TCON_REGISTERTYPE_POWERSEQENCE_WITHSUB_15_BYTES:
            return TCON_REGISTERTYPE_POWERSEQENCE_WITHSUB_15_BYTES;
        case EN_TCON_REGISTERTYPE_POWERSEQENCE_WITHSUB_11_BYTES:
            return TCON_REGISTERTYPE_POWERSEQENCE_WITHSUB_11_BYTES;
        case EN_TCON_REGISTERTYPE_PANEL_1_BYTES:
            return TCON_REGISTERTYPE_1_BYTES;
        default:
            UBOOT_ERROR("register type out of range: %d", en);
            return 0;
    }
}



MS_U8 TCON_VERSION= 3;

/***** Tcon  Major header:32 ********
    1. BinaryID:2
    2. CustomerID:2
    3. Checksum:2
    4. DataLength:4
    5. Description:18
    6.  Reserved:4
  ***** Tcon  sub header: 8 *****
    1. 2D Table:1
    2. 3D Table:1
    3. reserver: 6

  ***** each sub table information : 8 *******
  1. Sub-Table Type:1
  2. Register count:2
  3. Register type:1
  4. Register list offset:4
*********************************/
static void _parseAndDump_TCON_bin(MS_U8* Dram_Addr,APIPNL_TCON_TAB_TYPE TconType)
{
#if(CONFIG_ENABLE_TCON20 == 1)
    MApi_PNL_TCONMAP_DumpTable(Dram_Addr,TconType);
#else
    unsigned char  u8TCon2DTableNum;
    unsigned char  u8TableType=0;
    unsigned short u16RegisterCount=0;
    unsigned char  u8RegisterType=0;
    unsigned int   u32RegisterlistOffset=0;
    unsigned int   u32RegisterlistSize=0;
    unsigned char * targetDramBuf = NULL;
    UBOOT_TRACE("IN\n");
    UBOOT_DEBUG("===== TconType = %d =====\n",TconType);
    // Version 1 skip major header 32 byte:
    Dram_Addr += 32;

    u8TCon2DTableNum= *Dram_Addr;
    if(u8TCon2DTableNum == 0)
    {
        UBOOT_ERROR("u8TCon2DTableNum can not be 0\n");
        return;
    }
    // skip sub header 8byte:
    Dram_Addr += 8;
    if(TCON_VERSION==3)
    {
        // Version 2 skip verison infor 40 byte:
        Dram_Addr += 40;
    }

    // get specific Register list offset (each item is 8byte)
    u8TableType           = *((unsigned char*) (Dram_Addr + (TconType * 8) + 0));
    u16RegisterCount      = *((unsigned short*)(Dram_Addr + (TconType * 8) + 1));
    u8RegisterType        = *((unsigned char*) (Dram_Addr + (TconType * 8) + 3));
    u32RegisterlistOffset = *((unsigned int*)  (Dram_Addr + (TconType * 8) + 4));
    u32RegisterlistSize   = u16RegisterCount * _registerTypeToSize(u8RegisterType);
    UBOOT_DEBUG("u8TableType           = %d\n",u8TableType);
    UBOOT_DEBUG("u16RegisterCount      = %d\n",u16RegisterCount);
    UBOOT_DEBUG("u8RegisterType        = %d\n",u8RegisterType);
    UBOOT_DEBUG("u32RegisterlistOffset = 0x%x\n",u32RegisterlistOffset);
    UBOOT_DEBUG("u32RegisterlistSize   = 0x%x\n",u32RegisterlistSize);
    if(TCON_VERSION==3)
    {
        Dram_Addr+=(u32RegisterlistOffset-80);//minus major header 32 & sub header 8 & verison 40
    }
    else if (TCON_VERSION ==1)
    {
        Dram_Addr+=(u32RegisterlistOffset-40);//minus major header 32 & sub header 8
    }
    UBOOT_DEBUG("Dram_Addr+pRegisterlistOffset = %p\n",Dram_Addr);

    //copy the data section to target dram buffer
    //and extra 4byte:0xFFFFFFFF, it's for driver detects the register list end.
    targetDramBuf = malloc(u32RegisterlistSize+4);
    if(targetDramBuf==NULL)
    {
        UBOOT_ERROR("malloc fail!!\n");
        return;
    }
    memset(targetDramBuf,0xFF,u32RegisterlistSize+4);
    memcpy(targetDramBuf,Dram_Addr,u32RegisterlistSize);
    UBOOT_DUMP((unsigned int)targetDramBuf,u32RegisterlistSize+4);
    //dump table to PNL driver
    MApi_PNL_TCONMAP_DumpTable(ptcon_table,TconType);

    free(targetDramBuf);
#endif
    UBOOT_TRACE("OK\n");
    return;
}

#if TCON_TEMP_PATCH
static void init_TCON_Patch(MS_U8 data)
{
#define PM_RIU_BASE 0x1F000000UL
#define SC_BK01_42_L 0x130184
#define DUMMY_REG SC_BK01_42_L
    MS_U32 u32reg = (DUMMY_REG)<<1;
    u32reg += PM_RIU_BASE;
    (*((volatile MS_U16*)(u32reg))) = (MS_U16)(data);
}
#endif

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

void get_TCON_Type(void)
{
    MS_U8 bitcomb = (aHW_model_opt[2] * NUM_BACKEND_OPT_LEV) + aHW_model_opt[3];
    char  *pTconPanelEnv = NULL;
    char  ptcontype[3];
    MS_U8 tcontype = 0;
    char sar_hw_opt[3];
    TOOL_OPTION1_T      toolOpt1;
    DDI_NVM_GetToolOpt1(&toolOpt1);
    
//First check PCB HW option to select tcon type
    if (bitcomb < TCON_NUM_MAX)
    {
        e_tcontype = g_panel_list[bitcomb];      
        
        switch (bitcomb)
        {
              case 4: //[Level 1 : Level 0]
              {
                    if(toolOpt1.flags.eModelInchType == INCH_43)//toolOpt1: 43 inch
                    {
                        e_tcontype = E_TCON_V18_EPI_43INCH_6LANE_MPLUSOFF;
                    }
                    else if(toolOpt1.flags.eModelInchType == INCH_55)
                    {
                        e_tcontype = E_TCON_V18_EPI_55INCH_6LANE_MPLUSOFF;
                    }
              }
              break;
              case 5: //[Level 1 : Level 1]
              {
                    if(toolOpt1.flags.eModelInchType == INCH_43)//toolOpt1: 43 inch
                    {
                        e_tcontype = E_TCON_V18_EPI_43INCH_6LANE_MPLUSON;
                    }
                    else if(toolOpt1.flags.eModelInchType == INCH_55)
                    {
                        e_tcontype = E_TCON_V18_EPI_55INCH_6LANE_MPLUSON;
                    }
              }
              break;
              case 6:/*[Level 1 : Level 2]*/
              {
                    if(toolOpt1.flags.eModelInchType == INCH_55)//toolOpt1: 55 inch
                    {
                        e_tcontype = E_TCON_V17_EPI_55INCH_6LANE_UK6500PVA_MPLUSON;
                    }
              }
              break;  
              case 9: //[Level 2 : Level 1]
              {
                    if(toolOpt1.flags.eModelInchType == INCH_43)//toolOpt1: 43 inch
                    {
                        e_tcontype = E_TCON_V18_CEDS_43INCH_12LANE_MPLUSON;
                    }
                    else if(toolOpt1.flags.eModelInchType == INCH_49)//toolOpt1: 49 inch
                    {
                        e_tcontype = E_TCON_V18_CEDS_49INCH_12LANE_MPLUSON;
                    }
                    else if(toolOpt1.flags.eModelInchType == INCH_55)//toolOpt1: 55 inch
                    {
                        e_tcontype = E_TCON_V18_CEDS_55INCH_12LANE_MPLUSON;
                    }
              }
              break;
            	default:
		break;
        }
        
    }
    else
    {
        e_tcontype = E_TCON_NONE;    
	}
    snprintf(sar_hw_opt, sizeof(sar_hw_opt),"%d", bitcomb);

    printf("\33[0;32m [TCON] %s %d: bitcomb = %u, e_tcontype = %u,sar_hw_opt = %s, toolOpt1.flags.eModelInchType = %u	\33[m \n", __FUNCTION__,  __LINE__,bitcomb,e_tcontype, sar_hw_opt,toolOpt1.flags.eModelInchType);
	UBOOT_DEBUG("%s %d: bitcomb = %u, e_tcontype = %u,sar_hw_opt = %s, toolOpt1.flags.eModelInchType = %u	 \n",  __FUNCTION__,  __LINE__,bitcomb,e_tcontype, sar_hw_opt,toolOpt1.flags.eModelInchType);
	setenv("sar_hw_opt", sar_hw_opt);
	saveenv;
    

// Second. Check bootarg env if PCB HW option is not ready, SW option
    if ((pTconPanelEnv = getenv("enable_tcon_panel")) != NULL)
    {
		if (strcmp("1", pTconPanelEnv) == 0)
		{
		    e_tcontype = E_TCON_V18_EPI_49INCH_6LANE_MPLUSOFF;
		}
		else if(strcmp("2", pTconPanelEnv) == 0)
		{
		    e_tcontype = E_TCON_V18_EPI_49INCH_6LANE_MPLUSON;
		}
		else if(strcmp("3", pTconPanelEnv) == 0)
		{
		    e_tcontype = E_TCON_V18_EPI_55INCH_6LANE_MPLUSOFF;
		}
		else if(strcmp("4", pTconPanelEnv) == 0)
		{
		    e_tcontype = E_TCON_V18_EPI_55INCH_6LANE_MPLUSON;
		}
		else if(strcmp("5", pTconPanelEnv) == 0)
		{
		    e_tcontype = E_TCON_V18_EPI_65INCH_8LANE_MPLUSOFF;
		}
		else if(strcmp("6", pTconPanelEnv) == 0)
		{
		    e_tcontype = E_TCON_V18_EPI_65INCH_8LANE_MPLUSON;
		}
		else if(strcmp("7", pTconPanelEnv) == 0)
		{
		    e_tcontype = E_TCON_V18_CEDS_65INCH_12LANE_MPLUSOFF;
		}
		else if(strcmp("8", pTconPanelEnv) == 0)
		{
		    e_tcontype = E_TCON_V18_CEDS_55INCH_12LANE_MPLUSON;
		}
        	else if(strcmp("9", pTconPanelEnv) == 0)
		{
		    e_tcontype = E_TCON_V18_EPI_43INCH_6LANE_MPLUSOFF;
		}
              else if(strcmp("10", pTconPanelEnv) == 0)
		{
		    e_tcontype = E_TCON_V18_EPI_43INCH_6LANE_MPLUSON;
		}
              else if(strcmp("11", pTconPanelEnv) == 0)
		{
		    e_tcontype = E_TCON_V18_CEDS_43INCH_12LANE_MPLUSON;
		}
              else if(strcmp("12", pTconPanelEnv) == 0)
		{
		    e_tcontype = E_TCON_V18_CEDS_49INCH_12LANE_MPLUSON;
		}
               else if(strcmp("13", pTconPanelEnv) == 0)
		{
		    e_tcontype = E_TCON_V17_EPI_55INCH_6LANE_UK6500PVA_MPLUSON;
		}
		else
		{
	            e_tcontype = E_TCON_NONE;
		}
#if TCON_TEMP_PATCH
       init_TCON_Patch(e_tcontype);
#endif
       printf("\33[0;32m [TCON] %s %d: pTconPanelEnv = %s, e_tcontype = %u	\33[m \n", __FUNCTION__,  __LINE__,pTconPanelEnv,e_tcontype);
	   UBOOT_DEBUG("%s %d: pTconPanelEnv = %s, e_tcontype = %u\n",  __FUNCTION__,  __LINE__,pTconPanelEnv,e_tcontype);
	}
    
    tcontype = e_tcontype;
    snprintf(ptcontype, sizeof(ptcontype),"%d", tcontype);
	setenv("tcontype", ptcontype);
	saveenv;
}

#if(TCON_MPLUS_PATCH)

#define REG_SET_WL(_reg_, _val_)    \
do{ (*((volatile MS_U16*)(_reg_))) = (*((volatile MS_U16*)(_reg_))) | (1<<_val_); }while(0)

#define REG_CLEAR_WL(_reg_, _val_)    \
do{ (*((volatile MS_U16*)(_reg_))) = (*((volatile MS_U16*)(_reg_))) & (~(1<<_val_)); }while(0)

#define PM_RIU_BASE 0x1F000000UL
#define REG_BK3326_01_L 0x332602
#define REG_BK3303_01_L 0x330302

#endif

static void Init_TCON_Panel(void)
{
    UBOOT_TRACE("IN\n");
    if (ptcon_table == NULL)
    {
        UBOOT_TRACE("OK\n");
        return;
    }
    printf("[TCON] %s %d: dump tcon bin\n", __FUNCTION__, __LINE__);
    UBOOT_DEBUG("%s %d: dump tcon bin\n", __FUNCTION__, __LINE__);

    //MApi_PNL_SetSSC_Fmodulation(1);
    //MApi_PNL_SetSSC_Rdeviation(1);
    //MApi_PNL_SetSSC_En(ENABLE);

#if(TCON_MPLUS_PATCH)
    MS_U32 u32reg_ceds, u32reg_epi;
    u32reg_ceds = (REG_BK3326_01_L)<<1; // CEDS M+ DoubleBuffer
    u32reg_ceds += PM_RIU_BASE;
    REG_SET_WL(u32reg_ceds, 1);

    u32reg_epi = (REG_BK3303_01_L)<<1; // EPI M+ DoubleBuffer
    u32reg_epi += PM_RIU_BASE;
    REG_SET_WL(u32reg_epi, 6);
#endif

    _parseAndDump_TCON_bin(ptcon_table,E_APIPNL_TCON_TAB_TYPE_GENERAL);
    _parseAndDump_TCON_bin(ptcon_table,E_APIPNL_TCON_TAB_TYPE_GPIO);
    _parseAndDump_TCON_bin(ptcon_table,E_APIPNL_TCON_TAB_TYPE_SCALER);
    _parseAndDump_TCON_bin(ptcon_table,E_APIPNL_TCON_TAB_TYPE_MOD);

#if(TCON_MPLUS_PATCH)
    u32reg_ceds = (REG_BK3326_01_L)<<1; // CEDS M+ DoubleBuffer
    u32reg_ceds += PM_RIU_BASE;
    REG_CLEAR_WL(u32reg_ceds, 1);

    u32reg_epi = (REG_BK3303_01_L)<<1; // EPI M+ DoubleBuffer
    u32reg_epi += PM_RIU_BASE;
    REG_CLEAR_WL(u32reg_epi, 6);
#endif



    if(TCON_VERSION==3)
    {
        _parseAndDump_TCON_bin(ptcon_table,7); // verison 2 new panel table
    }
	/*
    UBOOT_DUMP(pTcontab->pTConInitTab, 0xB94);
	UBOOT_DUMP(pTcontab->pTConInit_GPIOTab, 0xE4C);
	UBOOT_DUMP(pTcontab->pTConInit_SCTab, 0x36);
	UBOOT_DUMP(pTcontab->pTConInit_MODTab, 0x61E);
	UBOOT_DUMP(pTcontab->pTConGammaTab, 0);
	UBOOT_DUMP(pTcontab->pTConPower_Sequence_OnTab, 0x18);
	UBOOT_DUMP(pTcontab->pTConPower_Sequence_OffTab, 0x18);
    */
    //_MApi_XC_Sys_Init_TCON_Panel(pTcontab);
    UBOOT_TRACE("OK\n");
}
//#endif

#if (CONFIG_3D_HWLVDSLRFLAG)
static void PNL_Set3D_HWLVDSLRFlag(void)
{
    MS_PNL_HW_LVDSResInfo lvdsresinfo;
    lvdsresinfo.bEnable = 1;
    lvdsresinfo.u16channel = 0x03; // channel A: BIT0, channel B: BIT1,
    lvdsresinfo.u32pair = 0x18; // pair 0: BIT0, pair 1: BIT1, pair 2: BIT2, pair 3: BIT3, pair 4: BIT4, etc ...

    MApi_PNL_HWLVDSReservedtoLRFlag(lvdsresinfo);
}
#endif

#if SHARP_VBY1
MS_BOOL MsDrv_PNL_IsVby1Sharp70(void)
{
    TOOL_OPTION1_T toolOpt1;
    DDI_NVM_GetToolOpt1(&toolOpt1);
#if 1
    if(toolOpt1.flags.eModelInchType == INCH_70 \ 
    && toolOpt1.flags.eModelModuleType == MODULE_SHARP)
#else
    if(toolOpt1.flags.eModelInchType == INCH_70 \
    && toolOpt1.flags.eModelToolType == TOOL_UK65 \ 
    && toolOpt1.flags.eModelModuleType == MODULE_SHARP)
#endif    
    {  
        UBOOT_DEBUG("\r\n @Vby1IsSharp70@");
        MApi_PNL_SetToolOption1(toolOpt1.flags.eModelInchType,\
        toolOpt1.flags.eModelToolType,\
        toolOpt1.flags.eModelModuleType);
        return TRUE;
    }
    else
    {
         UBOOT_DEBUG("\r\n @Vby1IsNotSharp70@");
        // MApi_PNL_SetToolOption1(0xFFFF,0xFFFF,0xFFFF);        
    }
    return FALSE;
}
#endif

MS_BOOL MsDrv_PNL_Init(PanelType*  panel_data)
{
    MS_BOOL ret=TRUE;
    UBOOT_TRACE("IN\n");
#if SHARP_VBY1    
    MS_BOOL bSharyVBY1 = FALSE;
#endif    
    MApi_PNL_PreInit(E_PNL_NO_OUTPUT);
    MApi_PNL_SkipTimingChange(FALSE);

    if (panel_data->m_ePanelLinkType == LINK_TTL)
    {
        UBOOT_DEBUG("Panle Link Type=LINK_TTL \n");
        MApi_BD_LVDS_Output_Type(LVDS_OUTPUT_USER);
        MApi_PNL_MOD_OutputConfig_User(0, 0, 0);
    }
    else if(panel_data->m_ePanelLinkType == LINK_LVDS)
    {
        UBOOT_DEBUG("Panle Link Type=LINK_LVDS \n");
    }
    else if(panel_data->m_ePanelLinkType == LINK_EXT)
    {
        UBOOT_DEBUG("Panle Link Type=LINK_EXT \n");
        UBOOT_DEBUG("Link Ext Type=%u \n", getLinkExtType());

        MApi_PNL_SetLPLLTypeExt(getLinkExtType());
#if(ENABLE_MSTAR_NAPOLI==1 || ENABLE_MSTAR_NIKE==1)
        if((LINK_VBY1_8BIT_8LANE<getLinkExtType()) || (LINK_VBY1_10BIT_4LANE>getLinkExtType()))
        {
            MApi_PNL_PreInit(E_PNL_CLK_DATA);
        }
#endif
#if (CONFIG_XC_FRC_VB1==1||CONFIG_URSA6_VB1 ==1 || CONFIG_URSA9_VB1 == 1 || CONFIG_URSA7_VB1 == 1)
        if(LINK_VBY1_10BIT_8LANE==getLinkExtType())
        {
            MApi_PNL_MOD_OutputConfig_User(0x5500, 0x0055, 0x0000);
            MApi_BD_LVDS_Output_Type(4); // LVDS_SINGLE_OUTPUT_B
            MApi_PNL_ForceSetPanelDCLK((panel_data->m_dwPanelDCLK *2),TRUE); //4k2k
            MApi_PNL_Init_MISC(E_APIPNL_MISC_4K2K_ENABLE_60HZ);
            UBOOT_DEBUG("VB1 Link Type=LINK_VBY1_10BIT_8LANE \n");

        }
        else if(LINK_VBY1_10BIT_2LANE==getLinkExtType())
        {
            MApi_PNL_MOD_OutputConfig_User(0x0500, 0x0000, 0x0000);
            MApi_BD_LVDS_Output_Type(4); // LVDS_SINGLE_OUTPUT_B
            UBOOT_DEBUG("VB1 Link Type=LINK_VBY1_10BIT_2LANE \n");

        }
        else if(LINK_VBY1_10BIT_4LANE==getLinkExtType())
        {
            MApi_PNL_MOD_OutputConfig_User(0x5500, 0x0000, 0x0000);
            MApi_BD_LVDS_Output_Type(4); // LVDS_SINGLE_OUTPUT_B
            MApi_PNL_ForceSetPanelDCLK((panel_data->m_dwPanelDCLK *2),TRUE); //4k2k

            UBOOT_DEBUG("VB1 Link Type=LINK_VBY1_10BIT_4LANE \n");
        }
#endif
#if 1 //s(ENABLE_MSTAR_MONACO)
        if(LINK_VBY1_10BIT_8LANE==getLinkExtType())
        {
            MApi_PNL_MOD_OutputConfig_User(0x5555, 0x0055, 0x0000);
            MApi_BD_LVDS_Output_Type(4); // LVDS_SINGLE_OUTPUT_B
            MApi_PNL_ForceSetPanelDCLK((panel_data->m_dwPanelDCLK *4),TRUE); //4k2k
            //MApi_PNL_Init_MISC(E_APIPNL_MISC_4K2K_ENABLE_60HZ);
            UBOOT_DEBUG("VB1 Link Type=LINK_VBY1_10BIT_8LANE \n");
        }
        else if(LINK_VBY1_10BIT_2LANE==getLinkExtType())
        {
            MApi_PNL_MOD_OutputConfig_User(0x5555, 0x0055, 0x0000);
            MApi_BD_LVDS_Output_Type(4); // LVDS_SINGLE_OUTPUT_B
            //MApi_PNL_ForceSetPanelDCLK((panel_data->m_dwPanelDCLK *4),TRUE); //4k2k
            //MApi_PNL_Init_MISC(E_APIPNL_MISC_4K2K_ENABLE_60HZ);
            UBOOT_DEBUG("VB1 Link Type=LINK_VBY1_10BIT_2LANE \n");
        }
#endif
    }

#if (ENABLE_HDMITX_MSTAR_ROCKET==1)
    char * p_str = NULL;
    int u8ResolutionEnv = -1;
    MApi_BD_LVDS_Output_Type(4); // LVDS_SINGLE_OUTPUT_B
    MApi_PNL_MOD_OutputConfig_User(0x1550, 0x0155, 0x0000);
    MApi_PNL_Init_MISC(E_APIPNL_MISC_SKIP_T3D_CONTROL|E_APIPNL_MISC_ENABLE_MANUAL_VSYNC_CTRL);
    p_str = getenv ("resolution");
    if(NULL != p_str)
    {
        u8ResolutionEnv = (int)simple_strtol(p_str, NULL, 10);
        UBOOT_DEBUG("ROCKET Link Type=%d \n",u8ResolutionEnv);

        if(u8ResolutionEnv==HDMITX_RES_4K2Kp_30Hz || u8ResolutionEnv==HDMITX_RES_4K2Kp_25Hz)
        {
            MApi_PNL_SetLPLLTypeExt(getLinkExtType());
            MApi_PNL_PreInit(E_PNL_CLK_DATA);
            MApi_PNL_ForceSetPanelDCLK((panel_data->m_dwPanelDCLK *2),TRUE);
            UBOOT_DEBUG("ROCKET Link Type=E_HAL_LTH_OUTPUT_4K2K \n");
        }
    }

#endif

#if (ENABLE_MSTAR_KAISER==0 && ENABLE_MSTAR_KAISERIN ==0 && ENABLE_MSTAR_KENYA == 0)
    MApi_PNL_On(panel_data->m_wPanelOnTiming1);//Power on TCON and Delay Timming1
#endif

    TOOL_OPTION1_T toolOpt1;
    DDI_NVM_GetToolOpt1(&toolOpt1);
        MApi_PNL_SetToolOption1(toolOpt1.flags.eModelInchType,\
        toolOpt1.flags.eModelToolType,\
        toolOpt1.flags.eModelModuleType);
#if 1 // [LM17A_PNL_BRINGUP]
#if SHARP_VBY1
#define LM18A_OUTPUT_ORDER_0_3_SHARP 0x6420
#define LM18A_OUTPUT_ORDER_4_7_SHARP 0x7531
#endif
#define LM18A_OUTPUT_ORDER_0_3 0x3210
#define LM18A_OUTPUT_ORDER_4_7 0x7654
#define LM18A_OUTPUT_ORDER_8_11 0xBA98
#define LM18A_OUTPUT_ORDER_12_13 0x0000
  
#if SHARP_VBY1   
    if(MsDrv_PNL_IsVby1Sharp70())bSharyVBY1 = TRUE;   
     
    if(bSharyVBY1)/*Shary VBY1 10BIT 8 LANE*/
    {
        MApi_PNL_MOD_OutputChannelOrder(APIPNL_OUTPUT_CHANNEL_ORDER_USER, 
        LM18A_OUTPUT_ORDER_0_3_SHARP, LM18A_OUTPUT_ORDER_4_7_SHARP,\
        LM18A_OUTPUT_ORDER_8_11, LM18A_OUTPUT_ORDER_12_13);
    }
    else
#endif        
    {
        MApi_PNL_MOD_OutputChannelOrder(APIPNL_OUTPUT_CHANNEL_ORDER_USER, 
        LM18A_OUTPUT_ORDER_0_3, LM18A_OUTPUT_ORDER_4_7,\
        LM18A_OUTPUT_ORDER_8_11, LM18A_OUTPUT_ORDER_12_13);
    }
#if(CONFIG_ENABLE_TCON20 == 1)
        get_TCON_Type();
        if(MDrv_SYS_GetChipRev() != 0)/*For New*/
        {
         switch (e_tcontype)
         {
            case E_TCON_V18_EPI_55INCH_6LANE_MPLUSOFF:
			ptcon_table = _tcon20_55inch_6lane_off;
			MApi_PNL_TCONMAP_DumpTable(ptcon_table,E_APIPNL_TCON_TAB_TYPE_PANEL_INFO);
			break;
            case E_TCON_V18_EPI_55INCH_6LANE_MPLUSON:
			ptcon_table = _tcon20_55inch_6lane_on;
			MApi_PNL_TCONMAP_DumpTable(ptcon_table,E_APIPNL_TCON_TAB_TYPE_PANEL_INFO);
			break;
            case E_TCON_V18_CEDS_65INCH_12LANE_MPLUSOFF:
			ptcon_table = _tcon20_65inch_12lane_off;
			MApi_PNL_TCONMAP_DumpTable(ptcon_table,E_APIPNL_TCON_TAB_TYPE_PANEL_INFO);
			break;
            case E_TCON_V18_CEDS_55INCH_12LANE_MPLUSON:
			ptcon_table = _tcon20_55inch_12lane_on;
			MApi_PNL_TCONMAP_DumpTable(ptcon_table,E_APIPNL_TCON_TAB_TYPE_PANEL_INFO);
			break;
            case E_TCON_V18_EPI_65INCH_8LANE_MPLUSOFF:
			ptcon_table = _tcon20_65inch_8lane_off;
			MApi_PNL_TCONMAP_DumpTable(ptcon_table,E_APIPNL_TCON_TAB_TYPE_PANEL_INFO);
			break;
            case E_TCON_V18_EPI_65INCH_8LANE_MPLUSON:
			ptcon_table = _tcon20_65inch_8lane_on;
			MApi_PNL_TCONMAP_DumpTable(ptcon_table,E_APIPNL_TCON_TAB_TYPE_PANEL_INFO);
			break;
            case E_TCON_V18_EPI_49INCH_6LANE_MPLUSOFF:
			ptcon_table = _tcon20_49inch_6lane_off;
			MApi_PNL_TCONMAP_DumpTable(ptcon_table,E_APIPNL_TCON_TAB_TYPE_PANEL_INFO);
			break;
            case E_TCON_V18_EPI_49INCH_6LANE_MPLUSON:
			ptcon_table = _tcon20_49inch_6lane_on;
			MApi_PNL_TCONMAP_DumpTable(ptcon_table,E_APIPNL_TCON_TAB_TYPE_PANEL_INFO);
			break;
            case E_TCON_V18_EPI_43INCH_6LANE_MPLUSOFF:
			ptcon_table = _tcon20_43inch_6lane_off;
			MApi_PNL_TCONMAP_DumpTable(ptcon_table,E_APIPNL_TCON_TAB_TYPE_PANEL_INFO);
			break;
            case E_TCON_V18_EPI_43INCH_6LANE_MPLUSON:
			ptcon_table = _tcon20_43inch_6lane_on;
			MApi_PNL_TCONMAP_DumpTable(ptcon_table,E_APIPNL_TCON_TAB_TYPE_PANEL_INFO);
			break;
            case E_TCON_V18_CEDS_43INCH_12LANE_MPLUSON:
			ptcon_table = _tcon20_43inch_12lane_on;
			MApi_PNL_TCONMAP_DumpTable(ptcon_table,E_APIPNL_TCON_TAB_TYPE_PANEL_INFO);
			break;
            case E_TCON_V18_CEDS_49INCH_12LANE_MPLUSON:
			ptcon_table = _tcon20_49inch_12lane_on;
			MApi_PNL_TCONMAP_DumpTable(ptcon_table,E_APIPNL_TCON_TAB_TYPE_PANEL_INFO);
			break;
            case E_TCON_V17_EPI_55INCH_6LANE_UK6500PVA_MPLUSON:
			ptcon_table = _tcon20_55inch_6lane_uk65_on;
			MApi_PNL_TCONMAP_DumpTable(ptcon_table,E_APIPNL_TCON_TAB_TYPE_PANEL_INFO);
			break; 
		default:
			ptcon_table = NULL;
			break;
         }
         }
         else
         {
         switch (e_tcontype)
         {
            case E_TCON_V18_EPI_55INCH_6LANE_MPLUSOFF:
			ptcon_table = _tcon20_55inch_6lane_off;
			MApi_PNL_TCONMAP_DumpTable(ptcon_table,E_APIPNL_TCON_TAB_TYPE_PANEL_INFO);
			break;
            case E_TCON_V18_EPI_55INCH_6LANE_MPLUSON:
			ptcon_table = _tcon20_55inch_6lane_on;
			MApi_PNL_TCONMAP_DumpTable(ptcon_table,E_APIPNL_TCON_TAB_TYPE_PANEL_INFO);
			break;
            case E_TCON_V18_CEDS_65INCH_12LANE_MPLUSOFF:
			ptcon_table = _tcon20_65inch_12lane_off;
			MApi_PNL_TCONMAP_DumpTable(ptcon_table,E_APIPNL_TCON_TAB_TYPE_PANEL_INFO);
			break;
            case E_TCON_V18_CEDS_55INCH_12LANE_MPLUSON:
			ptcon_table = _tcon20_55inch_12lane_on;
			MApi_PNL_TCONMAP_DumpTable(ptcon_table,E_APIPNL_TCON_TAB_TYPE_PANEL_INFO);
			break;
            case E_TCON_V18_EPI_65INCH_8LANE_MPLUSOFF:
			ptcon_table = _tcon20_65inch_8lane_off;
			MApi_PNL_TCONMAP_DumpTable(ptcon_table,E_APIPNL_TCON_TAB_TYPE_PANEL_INFO);
			break;
            case E_TCON_V18_EPI_65INCH_8LANE_MPLUSON:
			ptcon_table = _tcon20_65inch_8lane_on;
			MApi_PNL_TCONMAP_DumpTable(ptcon_table,E_APIPNL_TCON_TAB_TYPE_PANEL_INFO);
			break;
            case E_TCON_V18_EPI_49INCH_6LANE_MPLUSOFF:
			ptcon_table = _tcon20_49inch_6lane_off;
			MApi_PNL_TCONMAP_DumpTable(ptcon_table,E_APIPNL_TCON_TAB_TYPE_PANEL_INFO);
			break;
            case E_TCON_V18_EPI_49INCH_6LANE_MPLUSON:
			ptcon_table = _tcon20_49inch_6lane_on;
			MApi_PNL_TCONMAP_DumpTable(ptcon_table,E_APIPNL_TCON_TAB_TYPE_PANEL_INFO);
			break;
		default:
			ptcon_table = NULL;
			break;
         }
         }
#endif

    MApi_PNL_Init(panel_data);
#if SHARP_VBY1
    if(bSharyVBY1)/*Shary VBY1 10BIT 8 LANE*/
    {
        MApi_PNL_MOD_OutputChannelOrder(APIPNL_OUTPUT_CHANNEL_ORDER_DEFAULT, 			
        LM18A_OUTPUT_ORDER_0_3_SHARP, LM18A_OUTPUT_ORDER_4_7_SHARP, \
        LM18A_OUTPUT_ORDER_8_11, LM18A_OUTPUT_ORDER_12_13);		
    }
    else
#endif     
    {
        MApi_PNL_MOD_OutputChannelOrder(APIPNL_OUTPUT_CHANNEL_ORDER_DEFAULT, 			
        LM18A_OUTPUT_ORDER_0_3, LM18A_OUTPUT_ORDER_4_7, \
        LM18A_OUTPUT_ORDER_8_11, LM18A_OUTPUT_ORDER_12_13);			
    }
#else
	MApi_PNL_Init(panel_data);
#endif


#if VX1_SW_MODE
	MApi_PNL_SetVideoHWTraining(FALSE);
	MApi_PNL_SetOSDHWTraining(FALSE);
#endif

#if (CONFIG_XC_FRC_VB1==1||CONFIG_URSA6_VB1 ==1 || CONFIG_URSA9_VB1 == 1 || CONFIG_URSA7_VB1 == 1)
     MApi_PNL_Control_Out_Swing(600);
#endif

        g_IPanel.Dump();
#if 1 // [LM17A_PNL_BRINGUP]
        //g_IPanel.SetGammaTbl(E_APIPNL_GAMMA_12BIT, tAllGammaTab, GAMMA_MAPPING_MODE);
#else
        g_IPanel.SetGammaTbl(E_APIPNL_GAMMA_12BIT, tAllGammaTab, GAMMA_MAPPING_MODE);
#endif
#if(ENABLE_URSA_6M30 == 1)
        if(PANEL_4K2K_ENABLE == 1)
        {
            MDrv_WriteByte(0x1032B5, 0xF0);  //LR Flag Send From LVDS Dummy Pixel
        }

        if(bMst6m30Installed)
        {
            ret=MApi_PNL_PreInit(E_PNL_CLK_DATA);
            MApi_PNL_SetOutput(E_APIPNL_OUTPUT_CLK_DATA);
            g_IPanel.Enable(TRUE);
            MApi_PNL_On(panel_data->m_wPanelOnTiming1);//Power on TCON and Delay Timming1
            MDrv_Ursa_6M30_LVDS_Enalbe(TRUE);//run_command("ursa_lvds_on", 0);
            PWM_init();
            MApi_PNL_On(panel_data->m_wPanelOnTiming2);//Delay Timming2
            lPanelOnTiming=panel_data->m_wPanelOnTiming2;
            lPanleTimer=MsSystemGetBootTime();
        }
        else
#endif
        {
            #if (ENABLE_MSTAR_KAISER==0 && ENABLE_MSTAR_KAISERIN ==0 && ENABLE_MSTAR_KENYA == 0)

            #if defined (CONFIG_URSA9_VB1) && (CONFIG_URSA9_VB1 == 1)
                if (LINK_VBY1_10BIT_2LANE == getLinkExtType())
                    MDrv_Ursa_9_Set_Lane_VB1_per_init(2, 2);
                else if (LINK_VBY1_10BIT_4LANE == getLinkExtType())
                    MDrv_Ursa_9_Set_Lane_VB1_per_init(4, 4);
            #elif defined (CONFIG_URSA7_VB1) && (CONFIG_URSA7_VB1 == 1)
                MDrv_Ursa_7_Set_2_lane_VB1_per_init();
            #endif

            ret=MApi_PNL_PreInit(E_PNL_CLK_DATA);

            #if defined(CONFIG_NOVA_KS2)
                MDrv_KS2_Panel_Unlock();
                if(panel_data->m_wPanelWidth == 3840 && panel_data->m_wPanelHeight == 2160)
				{
                    MDrv_KS2_Panel_Bootlogo(1);
				}
                else
				{
                    MDrv_KS2_Panel_Bootlogo(0);
					MDrv_KS2_Panel_AutoMuteMode();
				}
            #endif

            #if (ENABLE_MSTAR_AMBER1 == 1) || (ENABLE_MSTAR_MACAW12 == 1)
            MApi_PNL_SetOutput(E_APIPNL_OUTPUT_CLK_DATA);
            #endif
            PWM_init();
            MApi_PNL_En(TRUE, panel_data->m_wPanelOnTiming2);//Delay Timming2
            lPanelOnTiming=panel_data->m_wPanelOnTiming2;
            lPanleTimer=MsSystemGetBootTime();
		#endif

        // fixed me.
        // m2r bring up the tool option have some problem, make patch to bring up first
        gModelOpt.panel_interface = MODELOPT_PANEL_INTERFACE_LVDS;
        if( ((gModelOpt.panel_interface == MODELOPT_PANEL_INTERFACE_EPI) || (e_tcontype != E_TCON_NONE)) )
        {
            if(MApi_XC_Init(NULL, 0) == TRUE)
            {
                //Fix back porch for TCON
                XC_PANEL_INFO_EX stPanelInfoEx;
                memset(&stPanelInfoEx, 0, sizeof(XC_PANEL_INFO_EX));
                stPanelInfoEx.u32PanelInfoEx_Version = PANEL_INFO_EX_VERSION;
                stPanelInfoEx.u16PanelInfoEX_Length = sizeof(XC_PANEL_INFO_EX);
                stPanelInfoEx.bVSyncBackPorchValid = TRUE;
                stPanelInfoEx.u16VSyncBackPorch = panel_data->m_ucPanelVBackPorch; //set back porch value
                stPanelInfoEx.u16VFreq = 500; //this step setting info is only for 50hz
                if(MApi_XC_SetExPanelInfo(TRUE, &stPanelInfoEx))//Check the set is accepted or not
                    UBOOT_DEBUG("---%s:%d Set ExPanel Info OK\n", __FUNCTION__, __LINE__);
                else
                    UBOOT_ERROR("---%s:%d Set ExPanel Info Fail\n", __FUNCTION__, __LINE__);

                stPanelInfoEx.u16VFreq = 600; //set same setting for 60hz
                if(MApi_XC_SetExPanelInfo(TRUE, &stPanelInfoEx))//Check the set is accepted or not
                    UBOOT_DEBUG("---%s:%d Set ExPanel Info OK\n", __FUNCTION__, __LINE__);
                else
                    UBOOT_ERROR("---%s:%d Set ExPanel Info Fail\n", __FUNCTION__, __LINE__);

                Init_TCON_Panel();
            }
            else
            {
                UBOOT_DEBUG("xc Init failed....\n");
            }
       	 }


        }

#if (defined(CONFIG_A3_STB))
    verDispCvbs_Main();
#endif

    #if (CONFIG_3D_HWLVDSLRFLAG)
    PNL_Set3D_HWLVDSLRFlag();
    #endif
    UBOOT_TRACE("OK\n");

#if (ENABLE_MSTAR_KAISER==1) //K3
	PNL_DeviceId stPNL_DeviceId = {0, 1}; // SC1 PNL device ID
	MApi_PNL_EX_SkipTimingChange(&stPNL_DeviceId, FALSE);
	MApi_PNL_EX_Init(&stPNL_DeviceId, (PNL_EX_PanelType*)panel_data);
#endif
#if (CONFIG_XC_FRC_VB1==1)
        MDrv_XC_Sys_Init_XC();
#endif
#if defined (CONFIG_URSA9_VB1) && (CONFIG_URSA9_VB1 == 1)
    if (LINK_VBY1_10BIT_2LANE == getLinkExtType())
        MDrv_Ursa_9_Set_Lane_VB1(1920, panel_data->m_wPanelWidth);
    else if (LINK_VBY1_10BIT_4LANE == getLinkExtType())
        MDrv_Ursa_9_Set_Lane_VB1(3840, panel_data->m_wPanelWidth);
#elif defined (CONFIG_URSA7_VB1) && (CONFIG_URSA7_VB1 == 1)
    MDrv_Ursa_7_Set_2_lane_VB1();
#endif

#if 1 //LM14A  patch to fix the flicker issue
	MDrv_WriteByte(0x111eC1, 0x0);
	MDrv_WriteByte(0x111eC0, 0x0);
	MDrv_WriteByte(0x111eC3, 0x0);
	MDrv_WriteByte(0x111eC2, 0x0);
#endif

	return ret;

}

void MsDrv_PNL_BackLigth_On(void)
{
	UBOOT_TRACE("IN\n");
	unsigned long lpanelDelayTime=0;
	unsigned long ltotalTime=MsSystemGetBootTime();

	if(lPanelOnTiming>(ltotalTime-lPanleTimer))
	{
		lpanelDelayTime=(lPanelOnTiming-(ltotalTime-lPanleTimer));

		mdelay(lpanelDelayTime);
		printf("\n---%s:%d Set MsDrv_PNL_BackLigth_On DelayTask %lu \n", __FUNCTION__, __LINE__,lpanelDelayTime);
	}
	else
	{
		UBOOT_DEBUG("\n---%s:%d Set MsDrv_PNL_BackLigth_On No Delay \n", __FUNCTION__, __LINE__);

	}
	if(FALSE == pm_check_back_ground_active())
	{
		MApi_PNL_SetBackLight(BACKLITE_INIT_SETTING);//Power on backlight
	}
	else
	{
		MApi_PNL_SetBackLight(DISABLE);
	}

	UBOOT_TRACE("OK\n");

}

void setLinkExtType(APIPNL_LINK_EXT_TYPE linkExtType)
{
    PanelLinkExtType=linkExtType;
}
APIPNL_LINK_EXT_TYPE getLinkExtType(void)
{
    return PanelLinkExtType;
}

#define LVDS_MPLL_CLOCK_MHZ     432     // For crystal 24Mhz
#define LVDS_SPAN_FACTOR        131072
void MsDrv_Set_SSC(MS_BOOL bEnable, MS_U16 u16Fmodulation, MS_U16 u16Rdeviation)
{
    int u16Span = 0;
    int u16Step = 0;

    u32 u32PLL_SET = 0;
    u32 u32Temp = 0;

    if (true == bEnable)
    {
        u32PLL_SET = MsDrv_Read2Byte(LPLL_BANK + 0x0F*4) | (MsDrv_Read2Byte(LPLL_BANK + 0x10*4) << 16);
        //printf("u32PLL_SET = 0x%lx !\n", u32PLL_SET);

        // Set SPAN
        u32Temp = (LVDS_MPLL_CLOCK_MHZ * LVDS_SPAN_FACTOR) / (u16Fmodulation);
        u32Temp = u32Temp * 1000;
        u32Temp = u32Temp / u32PLL_SET;
        u16Span = (int)u32Temp;

        // Set STEP
        u16Step = ((MS_U32)u32PLL_SET*u16Rdeviation) / ((MS_U32)u16Span*10000);

        MsDrv_Write2Byte(LPLL_BANK + 0x17*4, u16Step & 0x03FF);   // LPLL_STEP
        MsDrv_Write2Byte(LPLL_BANK + 0x18*4, u16Span & 0x3FFF);   // LPLL_SPAN
    }

    MsDrv_Write2ByteMask(LPLL_BANK + 0x0D*4, (bEnable << 11), BIT(11));  // Enable ssc
    printf("MsDrv_Set_SSC done!\n");

}

#if (CONFIG_XC_FRC_VB1==1)

int MDrv_XC_Sys_Init_XC(void)
{
    XC_INITDATA sXC_InitData;
    XC_INITDATA *pstXC_InitData= &sXC_InitData;
    XC_INITMISC sXC_Init_Misc;
    U32 u32Addr=0,u32Size=0;


    // reset to zero

    memset(&sXC_InitData, 0, sizeof(sXC_InitData));
    memset(&sXC_Init_Misc, 0, sizeof(XC_INITMISC));

    // Init XC
#if 1// (OBA2!=1) // remove when XC driver update
    // Check library version. Do not modify this statement please.
    pstXC_InitData->u32XC_version = XC_INITDATA_VERSION;
#endif
    pstXC_InitData->u32XTAL_Clock = MST_XTAL_CLOCK_HZ;
#if 1


	if(get_addr_from_mmap("E_MMAP_ID_XC_MAIN_FB", &u32Addr)!=0)
	{
		UBOOT_ERROR("get E_MMAP_ID_XC_MAIN_FB mmap fail\n");
	}
	if(u32Addr==0xFFFF)
	{
		UBOOT_ERROR("get E_MMAP_ID_XC_MAIN_FB mmap fail\n");
		return -1;
	}
    get_length_from_mmap("E_MMAP_ID_XC_MAIN_FB", (U32 *)&u32Size);

#if 0

    /* E_MMAP_ID_XC_MAIN_FB   */
    //co_buffer L0
#define E_MMAP_ID_XC_MAIN_FB_AVAILABLE                         0x000C380000
#define E_MMAP_ID_XC_MAIN_FB_ADR                               0x000C380000  //Alignment 0
#define E_MMAP_ID_XC_MAIN_FB_GAP_CHK                           0x0000000000
#define E_MMAP_ID_XC_MAIN_FB_LEN                               0x0003000000
#define E_MMAP_ID_XC_MAIN_FB_MEMORY_TYPE                       (MIU1 | TYPE_NONE | UNCACHE

#endif
    pstXC_InitData->u32Main_FB_Size = u32Size;
    pstXC_InitData->u32Main_FB_Start_Addr = u32Addr;
    // Init DNR Address in Main & Sub channel. Keep the same. If project support FB PIP mode, set Sub DNR Address in AP layer (eg. mapp_init).
    pstXC_InitData->u32Sub_FB_Size = pstXC_InitData->u32Main_FB_Size;
    pstXC_InitData->u32Sub_FB_Start_Addr = pstXC_InitData->u32Main_FB_Start_Addr;

    // Chip related.
    pstXC_InitData->bIsShareGround = ENABLE;
#endif

#if 1 // (OBA2!=1) // remove when XC driver update
    // Board related
    pstXC_InitData->eScartIDPort_Sel = SCART_ID_SEL;//SCART_ID_SEL | SCART2_ID_SEL ;
#endif
    pstXC_InitData->bCEC_Use_Interrupt = FALSE;

    pstXC_InitData->bEnableIPAutoCoast = 0;

    pstXC_InitData->bMirror = FALSE;


    // panel info
    pstXC_InitData->stPanelInfo.u16HStart = g_IPanel.HStart();      // DE H start
    pstXC_InitData->stPanelInfo.u16VStart = g_IPanel.VStart();
    pstXC_InitData->stPanelInfo.u16Width  = g_IPanel.Width();
    pstXC_InitData->stPanelInfo.u16Height = g_IPanel.Height();
    pstXC_InitData->stPanelInfo.u16HTotal = g_IPanel.HTotal();
    pstXC_InitData->stPanelInfo.u16VTotal = g_IPanel.VTotal();

    pstXC_InitData->stPanelInfo.u16DefaultVFreq = g_IPanel.DefaultVFreq();

    pstXC_InitData->stPanelInfo.u8LPLL_Mode = g_IPanel.LPLL_Mode();
    pstXC_InitData->stPanelInfo.enPnl_Out_Timing_Mode = (E_XC_PNL_OUT_TIMING_MODE)(g_IPanel.OutTimingMode());

    pstXC_InitData->stPanelInfo.u16DefaultHTotal = g_IPanel.HTotal();
    pstXC_InitData->stPanelInfo.u16DefaultVTotal = g_IPanel.VTotal();
    pstXC_InitData->stPanelInfo.u32MinSET = g_IPanel.MinSET();
    pstXC_InitData->stPanelInfo.u32MaxSET = g_IPanel.MaxSET();
    pstXC_InitData->stPanelInfo.eLPLL_Type = (E_XC_PNL_LPLL_TYPE) g_IPanel.LPLL_Type();
    //printf("%s, %d, pstXC_InitData->stPanelInfo.eLPLL_Type=%u\n", __FUNCTION__, __LINE__, pstXC_InitData->stPanelInfo.eLPLL_Type);
    pstXC_InitData->bDLC_Histogram_From_VBlank = FALSE;

    if (  MApi_XC_GetCapability(E_XC_SUPPORT_IMMESWITCH)  )
    {
        sXC_Init_Misc.u32MISC_A |= E_XC_INIT_MISC_A_IMMESWITCH;
    }

    if ( MApi_XC_GetCapability(E_XC_SUPPORT_FRC_INSIDE) && 1 )
    {
        sXC_Init_Misc.u32MISC_A |= E_XC_INIT_MISC_A_FRC_INSIDE;
    }
// TODO: =====Start Temp function=====//After Interface/Struct definition are fixed, remove these!!

    if(sXC_Init_Misc.u32MISC_A & E_XC_INIT_MISC_A_FRC_INSIDE)
    {
        XC_PREINIT_INFO_t stXC_PANEL_INFO_ADV;
        memset(&stXC_PANEL_INFO_ADV, 0, sizeof(XC_PREINIT_INFO_t));

        stXC_PANEL_INFO_ADV.u8PanelHSyncWidth      = g_IPanel.HSynWidth();
        stXC_PANEL_INFO_ADV.u8PanelHSyncBackPorch  = g_IPanel.HSynBackPorch();
        stXC_PANEL_INFO_ADV.u8PanelVSyncWidth      = MApi_XC_GetPanelSpec(MApi_PNL_GetPnlTypeSetting())->m_ucPanelVSyncWidth;
        stXC_PANEL_INFO_ADV.u8PanelVSyncBackPorch  = g_IPanel.VSynBackPorch();
        stXC_PANEL_INFO_ADV.u16VTrigX              = 0x82F;
            stXC_PANEL_INFO_ADV.u16VTrigY              = (g_IPanel.VStart()+g_IPanel.Height()+12)%(MApi_XC_GetPanelSpec(MApi_PNL_GetPnlTypeSetting())->m_wPanelVTotal);//0x45B;

        // ???Memory???????a?code????, ?s?????u??n?@??memory
        #if 0

        /* E_MMAP_ID_FRC_4K2K   */
        //co_buffer L0
        #define E_MMAP_ID_FRC_4K2K_AVAILABLE                           0x000FE00000
        #define E_MMAP_ID_FRC_4K2K_ADR                                 0x000FE00000  //Alignment 0x100000
        #define E_MMAP_ID_FRC_4K2K_GAP_CHK                             0x0000000000
        #define E_MMAP_ID_FRC_4K2K_LEN                                 0x0002800000
        #define E_MMAP_ID_FRC_4K2K_MEMORY_TYPE                         (MIU1 | TYPE_NONE | UNCACHE)


        #endif
        if(get_addr_from_mmap("E_MMAP_ID_FRC_4K2K", &u32Addr)!=0)
	    {
		    UBOOT_ERROR("get E_MMAP_ID_FRC_4K2K mmap fail\n");
	    }
	    if(u32Addr==0xFFFF)
	    {
		    UBOOT_ERROR("get E_MMAP_ID_FRC_4K2K mmap fail\n");
		    return -1;
	    }
        get_length_from_mmap("E_MMAP_ID_FRC_4K2K", (U32 *)&u32Size);
        stXC_PANEL_INFO_ADV.FRCInfo.u32FRC_MEMC_MemAddr    = u32Addr;  //??o?O??FRC????memory
        stXC_PANEL_INFO_ADV.FRCInfo.u32FRC_MEMC_MemSize    = u32Size;


        stXC_PANEL_INFO_ADV.FRCInfo.u16FB_YcountLinePitch  = 0x00;
        stXC_PANEL_INFO_ADV.FRCInfo.u16PanelWidth          = g_IPanel.Width();
        stXC_PANEL_INFO_ADV.FRCInfo.u16PanelHeigh          = g_IPanel.Height();
        stXC_PANEL_INFO_ADV.FRCInfo.u8FRC3DPanelType       = E_XC_3D_PANEL_NONE;

        stXC_PANEL_INFO_ADV.FRCInfo.bFRC                   = FALSE; // TRUE: Normal; FALSE: Bypass
        stXC_PANEL_INFO_ADV.FRCInfo.u83Dmode               = 0x00;
        stXC_PANEL_INFO_ADV.FRCInfo.u8IpMode               = 0x00;
        stXC_PANEL_INFO_ADV.FRCInfo.u8MirrorMode           = 0x00;
        stXC_PANEL_INFO_ADV.FRCInfo.u32FRC_FrameSize       = 0;
        stXC_PANEL_INFO_ADV.FRCInfo.u83D_FI_out            = 0;

        MApi_XC_PreInit( E_XC_PREINIT_FRC, &stXC_PANEL_INFO_ADV, sizeof(XC_PREINIT_INFO_t) );
        pstXC_InitData->stPanelInfo.u8LPLL_Mode = 2; // Quad mode

    }
// TODO: =====End of Temp Function=====


        if(MApi_XC_Init(pstXC_InitData, sizeof(XC_INITDATA)) == FALSE)
    {
        printf("L:%d, XC_Init failed because of InitData wrong, please update header file and compile again\n", __LINE__);
    }

#if 1

    if (is_str_resume() == 1)
    {
       MApi_XC_FRC_Mute(TRUE);
    }

        if(MApi_XC_Init_MISC(&sXC_Init_Misc, sizeof(XC_INITMISC)) == FALSE)
    {
        printf("L:%d, XC Init MISC failed because of InitData wrong, please update header file and compile again\n", __LINE__);
    }

    // Init Gamma table
#endif
    return 1;

}
#endif

#endif
