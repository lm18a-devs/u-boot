//<MStar Software>
//******************************************************************************
// MStar Software
// Copyright (c) 2010 - 2012 MStar Semiconductor, Inc. All rights reserved.
// All software, firmware and related documentation herein ("MStar Software") are
// intellectual property of MStar Semiconductor, Inc. ("MStar") and protected by
// law, including, but not limited to, copyright law and international treaties.
// Any use, modification, reproduction, retransmission, or republication of all 
// or part of MStar Software is expressly prohibited, unless prior written 
// permission has been granted by MStar. 
//
// By accessing, browsing and/or using MStar Software, you acknowledge that you
// have read, understood, and agree, to be bound by below terms ("Terms") and to
// comply with all applicable laws and regulations:
//
// 1. MStar shall retain any and all right, ownership and interest to MStar
//    Software and any modification/derivatives thereof.
//    No right, ownership, or interest to MStar Software and any
//    modification/derivatives thereof is transferred to you under Terms.
//
// 2. You understand that MStar Software might include, incorporate or be
//    supplied together with third party`s software and the use of MStar
//    Software may require additional licenses from third parties.  
//    Therefore, you hereby agree it is your sole responsibility to separately
//    obtain any and all third party right and license necessary for your use of
//    such third party`s software. 
//
// 3. MStar Software and any modification/derivatives thereof shall be deemed as
//    MStar`s confidential information and you agree to keep MStar`s 
//    confidential information in strictest confidence and not disclose to any
//    third party.  
//
// 4. MStar Software is provided on an "AS IS" basis without warranties of any
//    kind. Any warranties are hereby expressly disclaimed by MStar, including
//    without limitation, any warranties of merchantability, non-infringement of
//    intellectual property rights, fitness for a particular purpose, error free
//    and in conformity with any international standard.  You agree to waive any
//    claim against MStar for any loss, damage, cost or expense that you may
//    incur related to your use of MStar Software.
//    In no event shall MStar be liable for any direct, indirect, incidental or
//    consequential damages, including without limitation, lost of profit or
//    revenues, lost or damage of data, and unauthorized system use.
//    You agree that this Section 4 shall still apply without being affected
//    even if MStar Software has been modified by MStar in accordance with your
//    request or instruction for your use, except otherwise agreed by both
//    parties in writing.
//
// 5. If requested, MStar may from time to time provide technical supports or
//    services in relation with MStar Software to you for your use of
//    MStar Software in conjunction with your or your customer`s product
//    ("Services").
//    You understand and agree that, except otherwise agreed by both parties in
//    writing, Services are provided on an "AS IS" basis and the warranty
//    disclaimer set forth in Section 4 above shall apply.  
//
// 6. Nothing contained herein shall be construed as by implication, estoppels
//    or otherwise:
//    (a) conferring any license or right to use MStar name, trademark, service
//        mark, symbol or any other identification;
//    (b) obligating MStar or any of its affiliates to furnish any person,
//        including without limitation, you and your customers, any assistance
//        of any kind whatsoever, or any information; or 
//    (c) conferring any license or right under any intellectual property right.
//
// 7. These terms shall be governed by and construed in accordance with the laws
//    of Taiwan, R.O.C., excluding its conflict of law rules.
//    Any and all dispute arising out hereof or related hereto shall be finally
//    settled by arbitration referred to the Chinese Arbitration Association,
//    Taipei in accordance with the ROC Arbitration Law and the Arbitration
//    Rules of the Association by three (3) arbitrators appointed in accordance
//    with the said Rules.
//    The place of arbitration shall be in Taipei, Taiwan and the language shall
//    be English.  
//    The arbitration award shall be final and binding to both parties.
//
//******************************************************************************
//<MStar Software>

#include "ursa_common.h"

/*
 * creating the global URSA command table here.
 */
unsigned char ursa_common_cmd_table[URSA_COMMON_COMMAND_MAX_SIZE][URSA_COMMON_CMD_DATA_SIZE];

static unsigned char  i2cbus_cfg_size      = URSA_COMMON_I2C_BUS_DEFAULT_SIZE;
static unsigned short ursa_common_bus_num  = URSA_COMMON_I2C_BUS_DEFAULT_NUM; 
static unsigned short ursa_common_slave_id = URSA_COMMON_I2C_DEFAULT_SLAVE_ID;
static unsigned int   ursa_common_cmd_table_cur_index = 0;    // the current pointer to URSA command table

static ursa_common_init_operations ursa_common_init_ops = {NULL, NULL, NULL};

static SWI2C_BusCfg ursa_common_i2cbus_cfg[URSA_COMMON_I2C_BUS_MAX_SIZE] =
{
    { PAD_DDCR_CK, PAD_DDCR_DA, 400 },    // bus-0, default bus
    { PAD_TGPIO2,  PAD_TGPIO3,  400 },    // bus-1, default bus
};

URSA_COMMON_STATUS MDrv_URSA_COM_SWI2C_Init(unsigned short i2c_bus_num, unsigned short i2c_slave_id)
{
    static int is_first_init = 1;

    UBOOT_TRACE("IN\n");
    if (is_first_init)
    {
        UBOOT_DEBUG("[URSA_COMMON] Intialize SW I2C.\n");
        MApi_SWI2C_Init(ursa_common_i2cbus_cfg, i2cbus_cfg_size);
        is_first_init = 0;
        ursa_common_bus_num = i2c_bus_num;
        ursa_common_slave_id = i2c_slave_id;
    }
    else
    {
        UBOOT_ERROR("[URSA_COMMON] SW I2C is already initialized !\n");
        UBOOT_TRACE("OK\n");
        return URSA_FAILED;
    }

    UBOOT_TRACE("OK\n");
    return URSA_SUCCED;
}

void MDrv_URSA_COM_I2C_Add_Bus(unsigned short SCL_PAD, unsigned short SDA_PAD, unsigned short delay)
{
    i2cbus_cfg_size++;
    ursa_common_i2cbus_cfg[i2cbus_cfg_size].padSCL   = SCL_PAD;
    ursa_common_i2cbus_cfg[i2cbus_cfg_size].padSDA   = SDA_PAD;
    ursa_common_i2cbus_cfg[i2cbus_cfg_size].defDelay = delay;
}

unsigned short MDrv_URSA_COM_Get_Bus_Num_Slave_ID(void)
{
    unsigned short bus_num_slave_id = (unsigned short)((ursa_common_bus_num << 8) | ursa_common_slave_id); 
    UBOOT_TRACE("IN\n");
    UBOOT_DEBUG("[URSA_COMMON] The bus_num_slave_id is 0x%x.\n", bus_num_slave_id);
    UBOOT_TRACE("OK\n");
    return bus_num_slave_id; 
}

URSA_COMMON_STATUS MDrv_URSA_COM_SWI2C_WriteBytes(unsigned short bus_num_slave_id, unsigned char reg_offset, 
                            unsigned char *reg_addr, unsigned short data_size, unsigned char *data)
{
    unsigned short cmd_index;
    
    UBOOT_TRACE("IN\n");
    UBOOT_DEBUG("[URSA_COMOM] Sending I2C command:\n");
    for (cmd_index = 0; cmd_index < data_size; cmd_index++)
    {
        UBOOT_DEBUG("[URSA_COMMON][0x%X]\n", data[cmd_index]);
    }

    if (MApi_SWI2C_WriteBytes(bus_num_slave_id, reg_offset, reg_addr, data_size, data) == TRUE)
    {
        UBOOT_DEBUG("[URSA_COMMON] Sending I2C command succeed.\n");
        UBOOT_TRACE("OK\n");
        return URSA_SUCCED;
    }
    else
    {
        UBOOT_ERROR("[URSA_COMMON] Sending I2C command FAILED !\n");
        UBOOT_TRACE("OK\n");
        return URSA_FAILED;
    }
}

URSA_COMMON_STATUS MDrv_URSA_COM_SWI2C_ReadBytes(unsigned short bus_num_slave_id, unsigned char reg_offset, 
                            unsigned char *reg_addr, unsigned short data_size, unsigned char *data)
{
    unsigned short cmd_index;

    UBOOT_TRACE("IN\n");
    if (MApi_SWI2C_ReadBytes(bus_num_slave_id, reg_offset, reg_addr, data_size, data) == TRUE)
    {
        UBOOT_DEBUG("[URSA_COMMON] The received I2C data:\n");
        for (cmd_index = 0; cmd_index < data_size; cmd_index++)
        {
            UBOOT_DEBUG("[URSA_COMMON][0x%X]\n", data[cmd_index]);
        }

        UBOOT_TRACE("OK\n");
        return URSA_SUCCED;
    }
    else
    {
        UBOOT_ERROR("[URSA_COMMON] Receiving I2C data FAILED !\n");
        UBOOT_TRACE("OK\n");
        return URSA_FAILED;
    }
}

void MDrv_URSA_COM_Cmd_Table_Init(void)
{
    UBOOT_TRACE("IN\n");
    memset(ursa_common_cmd_table, 0x00, URSA_COMMON_COMMAND_MAX_SIZE * URSA_COMMON_CMD_DATA_SIZE);
    ursa_common_cmd_table_cur_index = 0;
    UBOOT_TRACE("OK\n");
}

int MDrv_URSA_COM_Get_Next_Cmd_Index(int ursa_command_default_size)
{
    if (ursa_common_cmd_table_cur_index < ursa_command_default_size)
        return ursa_command_default_size;
    else
        return ursa_common_cmd_table_cur_index;
}

URSA_COMMON_STATUS MDrv_URSA_COM_Creat_Cmd(int cmd_index, unsigned char ursa_cmd_0, unsigned char ursa_cmd_1, unsigned char ursa_cmd_2,
                            unsigned char ursa_cmd_3, unsigned char ursa_cmd_4, unsigned char ursa_cmd_5, unsigned char ursa_cmd_6)
{
    int i;

    UBOOT_TRACE("IN\n");
    if (cmd_index > URSA_COMMON_COMMAND_MAX_SIZE || cmd_index < 0)
    {
        UBOOT_ERROR("\n[URSA_COMMON] ---> cmd_index is illeagal !\n");
        return URSA_FAILED;
    }

    if (ursa_common_cmd_table_cur_index >= URSA_COMMON_COMMAND_MAX_SIZE)
    {
        UBOOT_ERROR("\n[URSA_COMMON] ---> ursa_common_cmd_table is FULL !\n");
        return URSA_FAILED;
    }

    ursa_common_cmd_table[cmd_index][0] = ursa_cmd_0;
    ursa_common_cmd_table[cmd_index][1] = ursa_cmd_1;
    ursa_common_cmd_table[cmd_index][2] = ursa_cmd_2;
    ursa_common_cmd_table[cmd_index][3] = ursa_cmd_3;
    ursa_common_cmd_table[cmd_index][4] = ursa_cmd_4;
    ursa_common_cmd_table[cmd_index][5] = ursa_cmd_5;
    ursa_common_cmd_table[cmd_index][6] = ursa_cmd_6;

    ursa_common_cmd_table_cur_index++;

    UBOOT_DEBUG("\n[URSA_COMMON] Creating ursa_common_cmd_table[%d] ===>\n", cmd_index);
    for (i = 0; i < URSA_COMMON_CMD_DATA_SIZE; i++)
    {
        UBOOT_DEBUG("[URSA_COMMON][0x%x]\n", ursa_common_cmd_table[cmd_index][i]);
    }
    UBOOT_TRACE("OK\n");
    return URSA_SUCCED;
}

URSA_COMMON_STATUS MDrv_URSA_COM_init(void)
{
    UBOOT_TRACE("IN\n");
    if (ursa_common_init_ops.ursa_init != NULL)
    {
        UBOOT_TRACE("OK\n");
        return (*ursa_common_init_ops.ursa_init)();
    }
    else
    {
        UBOOT_ERROR("ursa_init is NULL !\n");
        UBOOT_TRACE("OK\n");
        return URSA_FAILED;
    }
}

URSA_COMMON_STATUS MDrv_URSA_COM_pre_init(PanelType *panel_para, APIPNL_LINK_EXT_TYPE panel_link_ext_type)
{
    UBOOT_TRACE("IN\n");
    if (ursa_common_init_ops.ursa_common_pre_init != NULL)
    {
        UBOOT_TRACE("OK\n");
        return (*ursa_common_init_ops.ursa_common_pre_init)(panel_para, panel_link_ext_type);
    }
    else
    {
        // no operations don't mean error !
        UBOOT_DEBUG("ursa_common_ac_on_pre_init operation is NULL, do nothing at this stage !\n");
        UBOOT_TRACE("OK\n");
        return URSA_SUCCED;
    }
}

URSA_COMMON_STATUS MDrv_URSA_COM_post_init(PanelType *panel_para, APIPNL_LINK_EXT_TYPE panel_link_ext_type)
{
    UBOOT_TRACE("IN\n");
    if (ursa_common_init_ops.ursa_common_post_init != NULL)
    {
        UBOOT_TRACE("OK\n");
        return (*ursa_common_init_ops.ursa_common_post_init)(panel_para, panel_link_ext_type);
    }
    else
    {
		if (MDrv_URSA_COM_Get_URSA_Type() == URSA_COMMON_FRC)
		{
			UBOOT_DEBUG("[URSA_COMMON] Executing XC_FRC init process --->\n");
			MDrv_XC_Sys_Init_XC();
		}
		else
		{
		    // no operations don't mean error !
			UBOOT_DEBUG("ursa_common_ac_on_post_init operation is NULL, do nothing at this stage !\n");
		}
		
        UBOOT_TRACE("OK\n");
        return URSA_SUCCED;
    }
}

URSA_COMMON_STATUS MDrv_URSA_COM_RegisterURSAInitOP(ursa_common_init_operations *specific_ursa_init_ops)
{
    UBOOT_TRACE("IN\n");
    if (specific_ursa_init_ops == NULL)
    {
        UBOOT_ERROR("ursa_init_ops is NULL !\n");
        return URSA_FAILED;
    }
    
    // Attension: the init operations function pointer could be NULL !
    ursa_common_init_ops.ursa_init = specific_ursa_init_ops->ursa_init;
    ursa_common_init_ops.ursa_common_pre_init = specific_ursa_init_ops->ursa_common_pre_init;
    ursa_common_init_ops.ursa_common_post_init = specific_ursa_init_ops->ursa_common_post_init;

    UBOOT_TRACE("OK\n");

    return URSA_SUCCED;
}

URSA_COMMON_STATUS MDrv_URSA_COM_CreateURSAInitFlow(void)
{
   UBOOT_TRACE("IN\n");

    URSA_COMMON_TYPE ursa_type = MDrv_URSA_COM_Get_URSA_Type();

    switch (ursa_type)
    {
        case URSA_COMMON_U6:
			UBOOT_DEBUG("[URSA_COMMON] We don't support U6 driver in URSA_COMMON, maybe later.");
            UBOOT_TRACE("OK\n");
            return URSA_SUCCED;

        case URSA_COMMON_U7:        // do nothing now
			UBOOT_DEBUG("[URSA_COMMON] We don't support U7 driver in URSA_COMMON, maybe later.");
            UBOOT_TRACE("OK\n");
            return URSA_SUCCED;

        case URSA_COMMON_U8:        // do nothing now
			UBOOT_DEBUG("[URSA_COMMON] We don't support U8 driver in URSA_COMMON, maybe later.");
            UBOOT_TRACE("OK\n");
            return URSA_SUCCED;

        case URSA_COMMON_U9:
			UBOOT_DEBUG("[URSA_COMMON] We don't support U9 driver in URSA_COMMON, maybe later.");
            UBOOT_TRACE("OK\n");
            return URSA_SUCCED;

        case URSA_COMMON_KS2:
			UBOOT_DEBUG("[URSA_COMMON] We don't support KS2 driver in URSA_COMMON, maybe later.");
            UBOOT_TRACE("OK\n");
            return URSA_SUCCED;

        case URSA_COMMON_FRC:
            UBOOT_TRACE("OK\n");
            return URSA_SUCCED;

        case URSA_COMMON_UNKNOWN:
            UBOOT_ERROR("[URSA_COMMON] Unknown URSA type !\n");
            return URSA_FAILED;

        default:
            UBOOT_ERROR("[URSA_COMMON] ursa_type is a illeagal value !\n");
            return URSA_FAILED;
    }
}

URSA_COMMON_STATUS MDrv_URSA_COM_Write_URSA_HandshakeType(void)
{
    UBOOT_TRACE("IN\n");

    URSA_COMMON_TYPE ursa_type = MDrv_URSA_COM_Get_URSA_Type();

    switch (ursa_type)
    {
        case URSA_COMMON_U6:
            Write2Byte(URSA_COMMON_HANDSHAKE_TYPE_DUMMY_REG, URSA_COMMON_URSA6_HANDSHAKE);
            UBOOT_TRACE("OK\n");
            return URSA_SUCCED;

        case URSA_COMMON_U7:
            Write2Byte(URSA_COMMON_HANDSHAKE_TYPE_DUMMY_REG, URSA_COMMON_URSA7_HANDSHAKE);
            UBOOT_TRACE("OK\n");
            return URSA_SUCCED;

        case URSA_COMMON_U8:        // it don't need handshake
            UBOOT_TRACE("OK\n");
            return URSA_SUCCED;

        case URSA_COMMON_U9:
            Write2Byte(URSA_COMMON_HANDSHAKE_TYPE_DUMMY_REG, URSA_COMMON_URSA7_HANDSHAKE);
            UBOOT_TRACE("OK\n");
            return URSA_SUCCED;

        case URSA_COMMON_KS2:
            Write2Byte(URSA_COMMON_HANDSHAKE_TYPE_DUMMY_REG, URSA_COMMON_URSA6_HANDSHAKE);
            UBOOT_TRACE("OK\n");
            return URSA_SUCCED;

        case URSA_COMMON_FRC:
            Write2Byte(URSA_COMMON_HANDSHAKE_TYPE_DUMMY_REG, URSA_COMMON_URSA6_HANDSHAKE);
            UBOOT_TRACE("OK\n");
            return URSA_SUCCED;

        case URSA_COMMON_UNKNOWN:
            UBOOT_ERROR("[URSA_COMMON] Unknown URSA type !\n");
            UBOOT_TRACE("OK\n");
            return URSA_FAILED;

        default:
            UBOOT_ERROR("[URSA_COMMON] ursa_type is a illeagal value !\n");
            UBOOT_TRACE("OK\n");
            return URSA_FAILED;
    }
}

URSA_COMMON_TYPE MDrv_URSA_COM_Get_URSA_Type(void)
{
    UBOOT_TRACE("IN\n");

    int ursa_type_num = URSA_COMMON_UNKNOWN;
    char *ursa_type = getenv(URSA_COMMON_TYPE_IN_ENV);

    if (ursa_type != NULL)
    {
        ursa_type_num = simple_strtoul(ursa_type, NULL, 10);
    }
    switch (ursa_type_num)
    {
        case URSA_COMMON_U6:
            UBOOT_DEBUG("[URSA_COMMON] ursa type is U6.\n");
            UBOOT_TRACE("OK\n");
            return URSA_COMMON_U6;
    
        case URSA_COMMON_U7:
            UBOOT_DEBUG("[URSA_COMMON] ursa type is U7.\n");
            UBOOT_TRACE("OK\n");
            return URSA_COMMON_U7;
    
        case URSA_COMMON_U8:        // it don't need handshake
            UBOOT_DEBUG("[URSA_COMMON] ursa type is U8.\n");
            UBOOT_TRACE("OK\n");
            return URSA_COMMON_U8;
    
        case URSA_COMMON_U9:
            UBOOT_DEBUG("[URSA_COMMON] ursa type is U9.\n");
            UBOOT_TRACE("OK\n");
            return URSA_COMMON_U9;
    
        case URSA_COMMON_KS2:
            UBOOT_DEBUG("[URSA_COMMON] ursa type is KS2.\n");
            UBOOT_TRACE("OK\n");
            return URSA_COMMON_KS2;
            
        case URSA_COMMON_FRC:
            UBOOT_DEBUG("[URSA_COMMON] ursa type is FRC.\n");
            UBOOT_TRACE("OK\n");
            return URSA_COMMON_FRC;
    
        case URSA_COMMON_UNKNOWN:
            UBOOT_ERROR("[URSA_COMMON] ursa_type is an illeagal value !\n");
            UBOOT_TRACE("OK\n");
            return URSA_COMMON_UNKNOWN;
    
        default:
            UBOOT_ERROR("[URSA_COMMON] ursa_type is a illeagal value !\n");
            return URSA_FAILED;
    }

}

int MDrv_URSA_COM_Delay(unsigned int delay_time)   // delay a little more time before sending I2C command to host
{
    char cmd[32] = { 0x00 };
    char *delay_time_from_env = NULL;
    unsigned int real_delay_time;

    UBOOT_TRACE("IN\n");
    delay_time_from_env = getenv("ursa_delay_time"); // you can also get ursa delay time from mboot env
    if (delay_time_from_env != NULL)
        real_delay_time = simple_strtoul(delay_time_from_env, NULL, 10);
    else
        real_delay_time = delay_time;

    sprintf(cmd, "delay %d", real_delay_time);
    UBOOT_DEBUG("[URSA_COMMON] DELAY %d\n", real_delay_time);

    UBOOT_TRACE("OK\n");
    return run_command(cmd, 0);
}
