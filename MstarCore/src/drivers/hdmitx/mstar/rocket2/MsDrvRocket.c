//<MStar Software>
//******************************************************************************
// MStar Software
// Copyright (c) 2010 - 2012 MStar Semiconductor, Inc. All rights reserved.
// All software, firmware and related documentation herein ("MStar Software") are
// intellectual property of MStar Semiconductor, Inc. ("MStar") and protected by
// law, including, but not limited to, copyright law and international treaties.
// Any use, modification, reproduction, retransmission, or republication of all
// or part of MStar Software is expressly prohibited, unless prior written
// permission has been granted by MStar.
//
// By accessing, browsing and/or using MStar Software, you acknowledge that you
// have read, understood, and agree, to be bound by below terms ("Terms") and to
// comply with all applicable laws and regulations:
//
// 1. MStar shall retain any and all right, ownership and interest to MStar
//    Software and any modification/derivatives thereof.
//    No right, ownership, or interest to MStar Software and any
//    modification/derivatives thereof is transferred to you under Terms.
//
// 2. You understand that MStar Software might include, incorporate or be
//    supplied together with third party`s software and the use of MStar
//    Software may require additional licenses from third parties.
//    Therefore, you hereby agree it is your sole responsibility to separately
//    obtain any and all third party right and license necessary for your use of
//    such third party`s software.
//
// 3. MStar Software and any modification/derivatives thereof shall be deemed as
//    MStar`s confidential information and you agree to keep MStar`s
//    confidential information in strictest confidence and not disclose to any
//    third party.
//
// 4. MStar Software is provided on an "AS IS" basis without warranties of any
//    kind. Any warranties are hereby expressly disclaimed by MStar, including
//    without limitation, any warranties of merchantability, non-infringement of
//    intellectual property rights, fitness for a particular purpose, error free
//    and in conformity with any international standard.  You agree to waive any
//    claim against MStar for any loss, damage, cost or expense that you may
//    incur related to your use of MStar Software.
//    In no event shall MStar be liable for any direct, indirect, incidental or
//    consequential damages, including without limitation, lost of profit or
//    revenues, lost or damage of data, and unauthorized system use.
//    You agree that this Section 4 shall still apply without being affected
//    even if MStar Software has been modified by MStar in accordance with your
//    request or instruction for your use, except otherwise agreed by both
//    parties in writing.
//
// 5. If requested, MStar may from time to time provide technical supports or
//    services in relation with MStar Software to you for your use of
//    MStar Software in conjunction with your or your customer`s product
//    ("Services").
//    You understand and agree that, except otherwise agreed by both parties in
//    writing, Services are provided on an "AS IS" basis and the warranty
//    disclaimer set forth in Section 4 above shall apply.
//
// 6. Nothing contained herein shall be construed as by implication, estoppels
//    or otherwise:
//    (a) conferring any license or right to use MStar name, trademark, service
//        mark, symbol or any other identification;
//    (b) obligating MStar or any of its affiliates to furnish any person,
//        including without limitation, you and your customers, any assistance
//        of any kind whatsoever, or any information; or
//    (c) conferring any license or right under any intellectual property right.
//
// 7. These terms shall be governed by and construed in accordance with the laws
//    of Taiwan, R.O.C., excluding its conflict of law rules.
//    Any and all dispute arising out hereof or related hereto shall be finally
//    settled by arbitration referred to the Chinese Arbitration Association,
//    Taipei in accordance with the ROC Arbitration Law and the Arbitration
//    Rules of the Association by three (3) arbitrators appointed in accordance
//    with the said Rules.
//    The place of arbitration shall be in Taipei, Taiwan and the language shall
//    be English.
//    The arbitration award shall be final and binding to both parties.
//
//******************************************************************************
//<MStar Software>
////////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2006-2009 MStar Semiconductor, Inc.
// All rights reserved.
//
// Unless otherwise stipulated in writing, any and all information contained
// herein regardless in any format shall remain the sole proprietary of
// MStar Semiconductor Inc. and be kept in strict confidence
// (��MStar Confidential Information��) by the recipient.
// Any unauthorized act including without limitation unauthorized disclosure,
// copying, use, reproduction, sale, distribution, modification, disassembling,
// reverse engineering and compiling of the contents of MStar Confidential
// Information is unlawful and strictly prohibited. MStar hereby reserves the
// rights to any and all damages, losses, costs and expenses resulting therefrom.
//
////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////////////////////////
///
/// file    mhal_lth.cpp
/// @author MStar Semiconductor Inc.
/// @brief  HDMITx HDCP HAL
///////////////////////////////////////////////////////////////////////////////////////////////////
#define _MHAL_LTH_C_

//-------------------------------------------------------------------------------------------------
//  Include Files
//-------------------------------------------------------------------------------------------------

#include <common.h>
#include <command.h>
#include "MsCommon.h"
#include <MsDebug.h>
#include <apiSWI2C.h>
#include <apiPNL.h>
#include "MsDrvRocket.h"
#include "MsBoot.h"
#include <drvGPIO.h>

#define MAILBOX_REG_BASE           (0x1200)




#define HDMITX_INTERRUPT HDMITX_INT

MS_BOOL _bSuspend = FALSE;
MS_BOOL _bHDMITx_Running = FALSE;

#if (HDMITX_IDAC_ENABLE == 1)
MS_BOOL _bIDAC_En = TRUE;
#else
MS_BOOL _bIDAC_En = FALSE;
#endif
#define SWI2C_CMD_DATA_SUM          (7)
#define SWI2C_I2C_BUS_NUM           (3)
#define SWI2C_DDC2BI_MODE_ADDR      0xA2
#if(ENABLE_MSTAR_MUJI==1)
#define SWI2C_I2C_SUB_ADDR          (2)
#define ROCKET2_GPIO_ACK            PAD_GPIO_PM5

#else
#define SWI2C_I2C_SUB_ADDR          (0)
#define ROCKET2_GPIO_ACK            PAD_GPIO_PM2

#endif

#define SWI2C_PANEL_INIT_AC_DELAY   750
#define SWI2C_PANEL_INIT_DC_DELAY   800


#define ROCKET_I2C_WR_SLAVE_ID (SWI2C_I2C_SUB_ADDR<<8 | SWI2C_DDC2BI_MODE_ADDR)


static SWI2C_BusCfg Rocket2_I2cBusCfg[SWI2C_I2C_BUS_NUM]=
{
    
    // Bus-0
    {PAD_GPIO28, PAD_GPIO29, 100}, //IS_SW_I2C  /SCL_PAD /SDA_PAD /Delay
    // Bus-1
    {PAD_TGPIO2, PAD_TGPIO3, 400},   //IS_SW_I2C  /SCL_PAD /SDA_PAD /Delay
    // Bus-2
    {PAD_DDCR_CK, PAD_DDCR_DA, 100}, //IS_SW_I2C  /SCL_PAD /SDA_PAD /Delay
    // Others, add other bus here
};

#define PARSING_IN_COLOR(x)    (\
    x == E_MAPI_RAPTORS_HDMITX_COLORS_INPUT_RGB ? "RGB" : \
    x == E_MAPI_RAPTORS_HDMITX_COLORS_INPUT_YUV ? "YUV" : \
                                     "unknown")

#define PARSING_OUT_COLOR(x)    (\
    x == E_MAPI_RAPTORS_HDMITX_COLORS_OUTPUT_RGB_444 ? "RGB444" : \
    x == E_MAPI_RAPTORS_HDMITX_COLORS_OUTPUT_YUV_422 ? "YUV422" : \
    x == E_MAPI_RAPTORS_HDMITX_COLORS_OUTPUT_YUV_444 ? "YUV444" : \
    x == E_MAPI_RAPTORS_HDMITX_COLORS_OUTPUT_YUV_420? "YUV444" : \
                                     "unknown")


/// Mailbox Command Type Index
typedef enum
{
    E_MAPI_MAILBOX_CONFIQ_VIDEO        = 0x01,
    E_MAPI_MAILBOX_CONFIQ_AUDIO        = 0x02,
    E_MAPI_MAILBOX_AVMUTE_TIMING_RESET = 0x03,
    E_MAPI_MAILBOX_QUERY_EDID_INFO     = 0x04,
    E_MAPI_MAILBOX_REPORT_EDID_INFO    = 0x05,
    E_MAPI_MAILBOX_ACK_STATUS_RESPOND  = 0x06,
    E_MAPI_MAILBOX_DEBUG               = 0xFF,
} EN_MAPI_RAPTORS_MAILBOX_COMMAND_TYPE; // mailbox command type

/// Mailbox Timing Type
typedef enum
{
    E_MAPI_RAPTORS_HDMITX_TIMING_720x576p_50Hz   = 0x00,
    E_MAPI_RAPTORS_HDMITX_TIMING_720x480p_60Hz   = 0x01,
    E_MAPI_RAPTORS_HDMITX_TIMING_1280x720p_50Hz  = 0x02,
    E_MAPI_RAPTORS_HDMITX_TIMING_1280x720p_60Hz  = 0x03,
    E_MAPI_RAPTORS_HDMITX_TIMING_1920x1080p_24Hz = 0x04,
    E_MAPI_RAPTORS_HDMITX_TIMING_1920x1080i_50Hz = 0x05,
    E_MAPI_RAPTORS_HDMITX_TIMING_1920x1080i_60Hz = 0x06,
    E_MAPI_RAPTORS_HDMITX_TIMING_1920x1080p_50Hz = 0x07,
    E_MAPI_RAPTORS_HDMITX_TIMING_1920x1080p_60Hz = 0x08,
    E_MAPI_RAPTORS_HDMITX_TIMING_4K2Kp_25Hz      = 0x09,
    E_MAPI_RAPTORS_HDMITX_TIMING_4K2Kp_30Hz      = 0x0A,
    E_MAPI_RAPTORS_HDMITX_TIMING_4K2Kp_50Hz      = 0x0B,
    E_MAPI_RAPTORS_HDMITX_TIMING_4K2Kp_60Hz      = 0x0C,
    E_MAPI_RAPTORS_HDMITX_TIMING_MAX             = 0x0D,
} EN_MAPI_RAPTORS_HDMITX_VIDEO_TIMING; // raptors timing type index

/// Mailbox Color Depth Type
typedef enum
{
    E_MAPI_RAPTORS_HDMITX_COLORS_NOID   = 0,
    E_MAPI_RAPTORS_HDMITX_COLORS_8bits  = 1,
    E_MAPI_RAPTORS_HDMITX_COLORS_10bits = 2,
    E_MAPI_RAPTORS_HDMITX_COLORS_12bits = 3,
} EN_MAPI_RAPTORS_HDMITX_COLOR_DEPTH; // raptors color type index

/// Mailbox Video Output Mode
typedef enum
{
    E_MAPI_RAPTORS_HDMITX_VIDEO_OUTPUT_DVI       = 0,
    E_MAPI_RAPTORS_HDMITX_VIDEO_OUTPUT_DVI_HDCP  = 1,
    E_MAPI_RAPTORS_HDMITX_VIDEO_OUTPUT_HDMI      = 2,
    E_MAPI_RAPTORS_HDMITX_VIDEO_OUTPUT_HDMI_HDCP = 3,
} EN_MAPI_RAPTORS_HDMITX_VIDEO_OUTPUT_TYPE; // raptors video output type index

/// Mailbox Input Color Type
typedef enum
{
    E_MAPI_RAPTORS_HDMITX_COLORS_INPUT_RGB = 0,
    E_MAPI_RAPTORS_HDMITX_COLORS_INPUT_YUV = 1,
} EN_MAPI_RAPTORS_HDMITX_INPUT_COLOR_TYPE; // raptors input color type index

/// Mailbox Output Color Type
typedef enum
{
    E_MAPI_RAPTORS_HDMITX_COLORS_OUTPUT_RGB_444 = 0,
    E_MAPI_RAPTORS_HDMITX_COLORS_OUTPUT_YUV_422 = 1,
    E_MAPI_RAPTORS_HDMITX_COLORS_OUTPUT_YUV_444 = 2,
    E_MAPI_RAPTORS_HDMITX_COLORS_OUTPUT_YUV_420 = 3,
} EN_MAPI_RAPTORS_HDMITX_OUTPUT_COLOR_TYPE; // raptors output color type index

/// Mailbox 3D VS Information Type
typedef enum
{
    E_MAPI_RAPTORS_HDMITX_VIDEO_3D_FramePacking     = 0,
    E_MAPI_RAPTORS_HDMITX_VIDEO_3D_FieldAlternative = 1,
    E_MAPI_RAPTORS_HDMITX_VIDEO_3D_LineAlternative  = 2,
    E_MAPI_RAPTORS_HDMITX_VIDEO_3D_SidebySide_FULL  = 3,
    E_MAPI_RAPTORS_HDMITX_VIDEO_3D_L_Dep            = 4,
    E_MAPI_RAPTORS_HDMITX_VIDEO_3D_L_Dep_Graphic_Dep= 5,
    E_MAPI_RAPTORS_HDMITX_VIDEO_3D_TopandBottom     = 6,
    E_MAPI_RAPTORS_HDMITX_VIDEO_3D_SidebySide_Half  = 8,
    E_MAPI_RAPTORS_HDMITX_VIDEO_3D_Not_in_Use       = 15,
} EN_MAPI_RAPTORS_HDMITX_VIDEO_3D_STRUCTURE_TYPE; // raptors 3D VS info type index

/// Mailbox 4K2K VS Information Type
typedef enum
{
    E_MAPI_RAPTORS_HDMITX_VIDEO_4K2K_Reserved    = 0, // 0x00
    E_MAPI_RAPTORS_HDMITX_VIDEO_4K2K_30Hz        = 1, // 0x01
    E_MAPI_RAPTORS_HDMITX_VIDEO_4K2K_25Hz        = 2, // 0x02
    E_MAPI_RAPTORS_HDMITX_VIDEO_4K2K_24Hz        = 3, // 0x03
    E_MAPI_RAPTORS_HDMITX_VIDEO_4K2K_24Hz_SMPTE  = 4, // 0x04
} EN_MAPI_RAPTORS_HDMITX_VIDEO_4K2K_VIC;// raptors 4K2K VS info type index


/// Mailbox Audio Frequency Type
typedef enum
{
    E_MAPI_RAPTORS_HDMITX_AUDIO_32KHz   = 0, // 0x00
    E_MAPI_RAPTORS_HDMITX_AUDIO_44KHz   = 1, // 0x01
    E_MAPI_RAPTORS_HDMITX_AUDIO_48KHz   = 2, // 0x02
    E_MAPI_RAPTORS_HDMITX_AUDIO_88KHz   = 3, // 0x03
    E_MAPI_RAPTORS_HDMITX_AUDIO_96KHz   = 4, // 0x04
    E_MAPI_RAPTORS_HDMITX_AUDIO_176KHz  = 5, // 0x05
    E_MAPI_RAPTORS_HDMITX_AUDIO_192KHz  = 6, // 0x06
} EN_MAPI_RAPTORS_HDMITX_AUDIO_FREQUENCY_TYPE;// raptors audio frequency index

/// Mailbox Audio Channel Number
typedef enum
{
    E_MAPI_RAPTORS_HDMITX_AUDIO_CH_2    = 2, // 2 channels
    E_MAPI_RAPTORS_HDMITX_AUDIO_CH_8    = 8, // 8 channels
} EN_MAPI_RAPTORS_HDMITX_AUDIO_CHANNEL_COUNT;// raptors audio channel number index

/// Mailbox Audio Code Type
typedef enum
{
    E_MAPI_RAPTORS_HDMITX_AUDIO_PCM     = 0, // PCM
    E_MAPI_RAPTORS_HDMITX_AUDIO_NONPCM  = 1, // non-PCM
} EN_MAPI_RAPTORS_HDMITX_AUDIO_CODING_TYPE;// raptors audio code type index

/// Mailbox Audio Channel Number
typedef enum
{
    E_MAPI_RAPTORS_HDMITX_AUDIO_SPDIF   = 0, // SPDIF
    E_MAPI_RAPTORS_HDMITX_AUDIO_I2S     = 1, // I2S
} EN_MAPI_RAPTORS_HDMITX_AUDIO_SOURCE_TYPE;// raptors audio source type index

/// Mailbox Audio Channel Number
typedef enum
{
    E_MAPI_RAPTORS_HDMITX_AUDIO_FORMAT_PCM   = 0, // PCM
    E_MAPI_RAPTORS_HDMITX_AUDIO_FORMAT_DSD   = 1, // DSD
    E_MAPI_RAPTORS_HDMITX_AUDIO_FORMAT_HBR   = 2, // HBR
} EN_MAPI_RAPTORS_HDMITX_AUDIO_SOURCE_FORMAT;// raptors audio source format index

/// Mailbox Video Mute
typedef enum
{
    E_MAPI_RAPTORS_HDMITX_VIDEO_MUTE_DISABLE   = 0, // disable
    E_MAPI_RAPTORS_HDMITX_VIDEO_MUTE_ENABLE    = 1, // enable
} EN_MAPI_RAPTORS_HDMITX_VIDEO_MUTE;// raptors video mute index

/// Mailbox Audio Mute
typedef enum
{
    E_MAPI_RAPTORS_HDMITX_AUDIO_MUTE_DISABLE   = 0, // disable
    E_MAPI_RAPTORS_HDMITX_AUDIO_MUTE_ENABLE    = 1, // enable
} EN_MAPI_RAPTORS_HDMITX_AUDIO_MUTE;// raptors audio mute index

/// Mailbox Audio Mute
typedef enum
{
    E_MAPI_RAPTORS_HDMITX_AVMUTE_DISABLE   = 0, // disable
    E_MAPI_RAPTORS_HDMITX_AVMUTE_ENABLE    = 1, // enable
} EN_MAPI_RAPTORS_HDMITX_AVMUTE;// raptors audio and video mute index

/// Mailbox Timing Change
typedef enum
{
    E_MAPI_RAPTORS_HDMITX_TIMING_CHANGE_DISABLE   = 0, // disable
    E_MAPI_RAPTORS_HDMITX_TIMING_CHANGE_ENABLE    = 1, // enable
} EN_MAPI_RAPTORS_HDMITX_TIMING_CHANGE;// raptors timing reset index

/// Mailbox Query Type
typedef enum
{
    E_MAPI_RAPTORS_QUERY_4K2K_VIC             = 1, // 0x01
    E_MAPI_RAPTORS_QUERY_3D_STRUCTURE         = 2, // 0x02
    E_MAPI_RAPTORS_QUERY_AUDIO_DESCRIPTOR_NUM = 3, // 0x03
    E_MAPI_RAPTORS_QUERY_AUDIO_DESCRIPTOR     = 4, // 0x04
    E_MAPI_RAPTORS_QUERY_VIDEO_DESCRIPTOR_NUM = 5, // 0x05
    E_MAPI_RAPTORS_QUERY_VIDEO_DESCRIPTOR     = 6, // 0x06
    E_MAPI_RAPTORS_QUERY_EDID_DATA            = 7, // 0x07
} EN_MAPI_RAPTORS_QUERY_TYPE;// raptors command 0x04 0x05 query type index


/// Mailbox 3D Timing Type for Command 0x04 Query
typedef enum
{
    E_MAPI_RAPTORS_HDMITX_QUERY_3DTIMING_1280x720p_50Hz  = 0x00,
    E_MAPI_RAPTORS_HDMITX_QUERY_3DTIMING_1280x720p_60Hz  = 0x01,
    E_MAPI_RAPTORS_HDMITX_QUERY_3DTIMING_1920x1080p_24Hz = 0x02,
    E_MAPI_RAPTORS_HDMITX_QUERY_3DTIMING_1920x1080i_50Hz = 0x03,
    E_MAPI_RAPTORS_HDMITX_QUERY_3DTIMING_1920x1080i_60Hz = 0x04,
    E_MAPI_RAPTORS_HDMITX_QUERY_3DTIMING_1920x1080p_25Hz = 0x05,
    E_MAPI_RAPTORS_HDMITX_QUERY_3DTIMING_1920x1080p_50Hz = 0x06,
    E_MAPI_RAPTORS_HDMITX_QUERY_3DTIMING_1920x1080p_60Hz = 0x07,
    E_MAPI_RAPTORS_HDMITX_QUERY_3DTIMING_4K2Kp_25Hz      = 0x08,
    E_MAPI_RAPTORS_HDMITX_QUERY_3DTIMING_4K2Kp_30Hz      = 0x09,
    E_MAPI_RAPTORS_HDMITX_QUERY_3DTIMING_4K2Kp_50Hz      = 0x0A,
    E_MAPI_RAPTORS_HDMITX_QUERY_3DTIMING_4K2Kp_60Hz      = 0x0B,
    E_MAPI_RAPTORS_HDMITX_QUERY_3DTIMING_MAX             = 0x0C,
} EN_MAPI_RAPTORS_HDMITX_QUERY_3DTIMING; // raptors 3D timing type index for command 0x04 query & 0x05 report

/// Mailbox 3D VS Information Type for Command 0x05 Report
typedef enum
{
    E_MAPI_RAPTORS_HDMITX_VIDEO_REPORT_3D_FramePacking               = 0x0001,
    E_MAPI_RAPTORS_HDMITX_VIDEO_REPORT_3D_FieldAlternative           = 0x0002,
    E_MAPI_RAPTORS_HDMITX_VIDEO_REPORT_3D_LineAlternative            = 0x0004,
    E_MAPI_RAPTORS_HDMITX_VIDEO_REPORT_3D_SidebySide_FULL            = 0x0008,
    E_MAPI_RAPTORS_HDMITX_VIDEO_REPORT_3D_L_Dep                      = 0x0010,
    E_MAPI_RAPTORS_HDMITX_VIDEO_REPORT_3D_L_Dep_Graphic_Dep          = 0x0020,
    E_MAPI_RAPTORS_HDMITX_VIDEO_REPORT_3D_TopandBottom               = 0x0040,
    E_MAPI_RAPTORS_HDMITX_VIDEO_REPORT_3D_SidebySide_Half_Horizontal = 0x0080,
    E_MAPI_RAPTORS_HDMITX_VIDEO_REPORT_3D_SidebySide_Half_Quincunx   = 0x8000,
} EN_MAPI_RAPTORS_HDMITX_VIDEO_REPORT_3D_STRUCTURE_TYPE; // raptors 3D VS info type index

/// Mailbox Status Response
typedef enum
{
    E_MAPI_MAILBOX_REPORT_CONFIQ_VIDEO        = 0x01,
    E_MAPI_MAILBOX_REPORT_CONFIQ_AUDIO        = 0x02,
    E_MAPI_MAILBOX_REPORT_AVMUTE_TIMING_RESET = 0x03,
    E_MAPI_MAILBOX_REPORT_REPORT_EDID_INFO    = 0x04,
    E_MAPI_MAILBOX_REPORT_DEBUG               = 0x07,
} EN_MAPI_RAPTORS_MAILBOX_RECIEVED_COMMAND_TYPE; // mailbox received command type

typedef enum
{
    E_MAPI_MAILBOX_REPORT_SUCCESS           = 0x00,
    E_MAPI_MAILBOX_REPORT_FAIL              = 0x01,
    E_MAPI_MAILBOX_REPORT_PARAMETER_INVALID = 0x02,
    E_MAPI_MAILBOX_REPORT_COMMAND_INVALID   = 0x03,
} EN_MAPI_RAPTORS_MAILBOX_STATUS_RESPONSE_TYPE; // mailbox received status response type

/// Mailbox Debug Type
typedef enum
{
    E_MAPI_MAILBOX_DEBUG_TEST_PATTERN     = 0x00,
    E_MAPI_MAILBOX_DEBUG_DEBUG_MESSAGE    = 0x01,
    E_MAPI_MAILBOX_DEBUG_FREE_RUN         = 0x02,
    E_MAPI_MAILBOX_DEBUG_LOCK_STATUS_LPLL = 0x03,
    E_MAPI_MAILBOX_DEBUG_STATUS_HTT_VTT   = 0x04,
} EN_MAPI_RAPTORS_MAILBOX_DEBUG_TYPE; // mailbox received command type

typedef enum
{
    E_MAPI_HDMITX_COLOR_RGB444,
    E_MAPI_HDMITX_COLOR_YUV422,
    E_MAPI_HDMITX_COLOR_YUV444,
    E_MAPI_HDMITX_COLOR_MAX,
} EN_MAPI_HDMITX_COLOR_TYPE;

typedef enum
{
    E_MAPI_HDMITX_COLOR_DEPTH_8BITS = 0,
    E_MAPI_HDMITX_COLOR_DEPTH_10BITS,
    E_MAPI_HDMITX_COLOR_DEPTH_12BITS,
    E_MAPI_HDMITX_COLOR_DEPTH_16BITS,
    E_MAPI_HDMITX_COLOR_DEPTH_AUTO,
    E_MAPI_HDMITX_COLOR_DEPTH_MAX,
} EN_MAPI_HDMITX_COLOR_DEPTH_TYPE;

typedef enum
{
    E_MAPI_HDMITX_VIDEO_HDMI = 0,
    E_MAPI_HDMITX_VIDEO_HDMI_HDCP,
    E_MAPI_HDMITX_VIDEO_DVI,
    E_MAPI_HDMITX_VIDEO_DVI_HDCP,
    E_MAPI_HDMITX_VIDEO_MAX,
} EN_MAPI_HDMITX_OUTPUT_MODE;

//audio
typedef enum
{
    E_MAPI_HDMITX_AUDIO_FREQ_NO_SIG  = 0,
    E_MAPI_HDMITX_AUDIO_32K          = 1,
    E_MAPI_HDMITX_AUDIO_44K          = 2,
    E_MAPI_HDMITX_AUDIO_48K          = 3,
    E_MAPI_HDMITX_AUDIO_88K          = 4,
    E_MAPI_HDMITX_AUDIO_96K          = 5,
    E_MAPI_HDMITX_AUDIO_176K         = 6,
    E_MAPI_HDMITX_AUDIO_192K         = 7,
    E_MAPI_HDMITX_AUDIO_FREQ_MAX_NUM = 8,
} EN_MAPI_HDMITX_AUDIO_FREQUENCY;


typedef enum
{
    E_MAPI_HDMITX_AUDIO_CH_2  = 2, // 2 channels
    E_MAPI_HDMITX_AUDIO_CH_8  = 8, // 8 channels
} EN_MAPI_HDMITX_AUDIO_CHANNEL_COUNT;


typedef enum
{
    E_MAPI_HDMITX_AUDIO_PCM        = 0, // PCM
    E_MAPI_HDMITX_AUDIO_NONPCM     = 1, // non-PCM
} EN_MAPI_HDMITX_AUDIO_CODING_TYPE;


typedef enum
{
    E_MAPI_HDMITX_AUDIO_SPDIF,
    E_MAPI_HDMITX_AUDIO_I2S,
    E_MAPI_HDMITX_AUDIO_NONE,
} EN_MAPI_HDMITX_AUDIO_TYPE;

typedef enum
{
    E_MAPI_HDMITX_AUDIO_FORMAT_PCM   = 0,
    E_MAPI_HDMITX_AUDIO_FORMAT_DSD   = 1,
    E_MAPI_HDMITX_AUDIO_FORMAT_HBR   = 2,
    E_MAPI_HDMITX_AUDIO_FORMAT_NA    = 3,
} EN_MAPI_HDMITX_AUDIO_SOURCE_FORMAT;
//end audio

//Global Variables
EN_MAPI_HDMITX_TIMING_TYPE genMAPI_HDMITxTming = E_MAPI_HDMITX_TIMING_1080_60P;

EN_MAPI_RAPTORS_HDMITX_VIDEO_OUTPUT_TYPE          genHDMITX_Mode = E_MAPI_RAPTORS_HDMITX_VIDEO_OUTPUT_HDMI;
EN_MAPI_RAPTORS_HDMITX_INPUT_COLOR_TYPE   genHDMITx_InColor = E_MAPI_RAPTORS_HDMITX_COLORS_INPUT_YUV;
EN_MAPI_RAPTORS_HDMITX_OUTPUT_COLOR_TYPE   genHDMITx_OutColor = E_MAPI_RAPTORS_HDMITX_COLORS_OUTPUT_YUV_444;
EN_MAPI_RAPTORS_HDMITX_COLOR_DEPTH genHDMITx_ColorDepth = E_MAPI_RAPTORS_HDMITX_COLORS_NOID;

EN_MAPI_RAPTORS_HDMITX_AUDIO_FREQUENCY_TYPE      genAudio_Freq = E_MAPI_RAPTORS_HDMITX_AUDIO_48KHz;
EN_MAPI_RAPTORS_HDMITX_AUDIO_CHANNEL_COUNT  genAudio_Ch_Cnt = E_MAPI_RAPTORS_HDMITX_AUDIO_CH_2;
EN_MAPI_RAPTORS_HDMITX_AUDIO_CODING_TYPE    genAudio_CodeType = E_MAPI_RAPTORS_HDMITX_AUDIO_PCM;
EN_MAPI_RAPTORS_HDMITX_AUDIO_SOURCE_FORMAT  genAudio_SrcFmt = E_MAPI_RAPTORS_HDMITX_AUDIO_FORMAT_PCM;
EN_MAPI_RAPTORS_HDMITX_AUDIO_SOURCE_TYPE    genAudio_SrcType =  E_MAPI_RAPTORS_HDMITX_AUDIO_I2S;
EN_MAPI_RAPTORS_HDMITX_VIDEO_TIMING         genHDMITx_VideoTiming = E_MAPI_RAPTORS_HDMITX_TIMING_1920x1080p_60Hz;
EN_MAPI_RAPTORS_HDMITX_VIDEO_3D_STRUCTURE_TYPE   genHDMITx_3D = E_MAPI_RAPTORS_HDMITX_VIDEO_3D_Not_in_Use;
EN_MAPI_RAPTORS_HDMITX_VIDEO_4K2K_VIC       genHDMITx_4K2K_VIC = E_MAPI_RAPTORS_HDMITX_VIDEO_4K2K_Reserved;



/// Mailbox Data Struct For Command 0x01 Configure Video
typedef struct ST_MAILBOX_COMMAND_CONFIQ_VIDEO
{
    EN_MAPI_RAPTORS_MAILBOX_COMMAND_TYPE command_idx; /// val = E_MAPI_MAILBOX_CONFIQ_VIDEO

    /// length of the following data
    MS_U8 data_len; /// Bytes

    /// flags for actions
    MS_BOOL timing_present;       /// flag of setting timing
    MS_BOOL color_present;        /// flag of setting color flag
    MS_BOOL VSinfo_3D_present;    /// flag of setting 3D_VS flag
    MS_BOOL analog_present;       /// flag of setting current, pren2, precon, tenpre and ten

    /// data
    EN_MAPI_RAPTORS_HDMITX_VIDEO_TIMING timing_idx;        /// timing type
    EN_MAPI_RAPTORS_HDMITX_COLOR_DEPTH color_depth;               /// color depth, 8bit, 10bit, 12bit
    EN_MAPI_RAPTORS_HDMITX_VIDEO_OUTPUT_TYPE output_mode;         /// video output mode, DVI or HDMI
    EN_MAPI_RAPTORS_HDMITX_INPUT_COLOR_TYPE in_color;             /// input color format, RGB or YUV
    EN_MAPI_RAPTORS_HDMITX_OUTPUT_COLOR_TYPE out_color;           /// output color format, RGB or YUV
    EN_MAPI_RAPTORS_HDMITX_VIDEO_3D_STRUCTURE_TYPE vs_3d;  /// 3d vs information
    MS_U8 current;
    MS_BOOL pren2;
    MS_U8 precon;
    MS_U8 tenpre;
    MS_U8 ten;
}ST_MAILBOX_COMMAND_CONFIQ_VIDEO;// struct of configure video

/// Mailbox Data Struct For Command 0x02 Configure Audio
typedef struct ST_MAILBOX_COMMAND_CONFIQ_AUDIO
{

    EN_MAPI_RAPTORS_MAILBOX_COMMAND_TYPE command_idx; /// val = E_MAPI_MAILBOX_CONFIQ_AUDIO

    /// length of the following data
    MS_U8 data_len; /// Bytes

    /// flags for actions
    MS_BOOL inform_present;   /// flag of timing id, frequency, channel number and code type
    MS_BOOL source_present;   /// flag of timing id, source type
    MS_BOOL fmt_present;      /// flag of timing id, source format

    /// data
    EN_MAPI_RAPTORS_HDMITX_AUDIO_FREQUENCY_TYPE frequency;  /// audio frequency
    EN_MAPI_RAPTORS_HDMITX_AUDIO_CHANNEL_COUNT channel_num; /// audio channel number
    EN_MAPI_RAPTORS_HDMITX_AUDIO_CODING_TYPE code_type;     /// audio coding type
    EN_MAPI_RAPTORS_HDMITX_AUDIO_SOURCE_TYPE source_type;   /// audio source type
    EN_MAPI_RAPTORS_HDMITX_AUDIO_SOURCE_FORMAT source_fmt;  /// audio source format
}ST_MAILBOX_COMMAND_CONFIQ_AUDIO;// struct of configure audio

/// Mailbox Data Struct For Command 0x03 Video/Audio Mute and Timing Reset
typedef struct ST_MAILBOX_COMMAND_TIMING_CHANGE_AVMUTE
{

    EN_MAPI_RAPTORS_MAILBOX_COMMAND_TYPE command_idx; /// val = E_MAPI_MAILBOX_AVMUTE_TIMING_RESET

    /// length of the following data
    MS_U8 data_len; /// Bytes

    /// flags for actions
    MS_BOOL video_flag;   /// flag of video mute
    MS_BOOL audio_flag;   /// flag of audio mute
    MS_BOOL avmute_flag;  /// flag of audio and video mute
    MS_BOOL timing_flag;  /// flag of changing timing

    /// data
    EN_MAPI_RAPTORS_HDMITX_VIDEO_MUTE enVideo_mute;
    EN_MAPI_RAPTORS_HDMITX_AUDIO_MUTE enAudio_mute;
    EN_MAPI_RAPTORS_HDMITX_AVMUTE enAV_mute;
    EN_MAPI_RAPTORS_HDMITX_TIMING_CHANGE enTiming;
}ST_MAILBOX_COMMAND_TIMING_CHANGE_AVMUTE;// struct of Video/Audio Mute and Timing Reset

/// Mailbox Data Struct For Command 0x04 Query EDID Information
typedef struct ST_MAILBOX_COMMAND_QUERY_EDID_INFO
{

    EN_MAPI_RAPTORS_MAILBOX_COMMAND_TYPE command_idx; /// val = E_MAPI_MAILBOX_QUERY_EDID_INFO

    /// length of the following data
    MS_U8 data_len; /// Bytes

    /// query type
    EN_MAPI_RAPTORS_QUERY_TYPE query_type;

    /// data
    EN_MAPI_RAPTORS_HDMITX_QUERY_3DTIMING timing_3d;
    MS_U8 des_startIdx;  // audio/video descriptor start index
    MS_U8 des_endIdx;    // audio/video descriptor end index
    MS_U8 bytes64_Idx;   // 64 bytes index
    MS_U8 edid_Idx;      // EDID block index
}ST_MAILBOX_COMMAND_QUERY_EDID_INFO;// struct of query edid info

/// Mailbox Data Struct For Command 0x05 Report EDID Information (Different Query Type Has Different Struct)
/// Query Type = 4K2K Vic, Query_Type = E_MAPI_RAPTORS_QUERY_4K2K_VIC 0x01
typedef struct ST_MAILBOX_COMMAND_REPORT_EDID_INFO_4K2KVIC
{

    EN_MAPI_RAPTORS_MAILBOX_COMMAND_TYPE command_idx; /// val = E_MAPI_MAILBOX_REPORT_EDID_INFO

    /// length of the following data
    MS_U8 data_len; /// Bytes

    /// query type
    EN_MAPI_RAPTORS_QUERY_TYPE query_type; /// val = E_MAPI_RAPTORS_QUERY_4K2K_VIC

    /// data
    MS_U8 vic_num; /// number of 4k2k_vic
    EN_MAPI_RAPTORS_HDMITX_VIDEO_4K2K_VIC vic4k2k[8]; /// 4k2k_vic
}ST_MAILBOX_COMMAND_REPORT_EDID_INFO_4K2KVIC;// struct of report edid info 4k2k_vic

/// Mailbox Data Struct For Command 0x05 Report EDID Information (Different Query Type Has Different Struct)
/// Query Type = 3D Structure, Query_Type = E_MAPI_RAPTORS_QUERY_3D_STRUCTURE 0x02
typedef struct ST_MAILBOX_COMMAND_REPORT_EDID_INFO_3DSTRUCT
{

    EN_MAPI_RAPTORS_MAILBOX_COMMAND_TYPE command_idx; /// val = E_MAPI_MAILBOX_REPORT_EDID_INFO

    /// length of the following data
    MS_U8 data_len; /// Bytes

    /// query type
    EN_MAPI_RAPTORS_QUERY_TYPE query_type; /// val = E_MAPI_RAPTORS_QUERY_3D_STRUCTURE

    /// data
    EN_MAPI_RAPTORS_HDMITX_QUERY_3DTIMING repTiming_3d; /// 3D timing
    EN_MAPI_RAPTORS_HDMITX_VIDEO_REPORT_3D_STRUCTURE_TYPE edid_3d; /// 3D edid structure, combination of EN_MAPI_RAPTORS_HDMITX_VIDEO_REPORT_3D_STRUCTURE_TYPE
}ST_MAILBOX_COMMAND_REPORT_EDID_INFO_3DSTRUCT;// struct of report edid info 3D_structure

/// Mailbox Data Struct For Command 0x05 Report EDID Information (Different Query Type Has Different Struct)
/// Query Type = Number of Audio Descriptor, Query_Type = E_MAPI_RAPTORS_QUERY_AUDIO_DESCRIPTOR_NUM 0x03
typedef struct ST_MAILBOX_COMMAND_REPORT_EDID_INFO_AUDIODES_NUM
{

    EN_MAPI_RAPTORS_MAILBOX_COMMAND_TYPE command_idx; /// val = E_MAPI_MAILBOX_REPORT_EDID_INFO

    /// length of the following data
    MS_U8 data_len; /// Bytes

    /// query type
    EN_MAPI_RAPTORS_QUERY_TYPE query_type; /// val = E_MAPI_RAPTORS_QUERY_AUDIO_DESCRIPTOR_NUM

    /// data
    MS_U8 auddes_num; /// number of audio descriptor
}ST_MAILBOX_COMMAND_REPORT_EDID_INFO_AUDIODES_NUM;// struct of number of audio descriptor

/// Mailbox Data Struct For Command 0x05 Report EDID Information (Different Query Type Has Different Struct)
/// Query Type = Audio Descriptor, Query_Type = E_MAPI_RAPTORS_QUERY_AUDIO_DESCRIPTOR 0x04
typedef struct ST_MAILBOX_COMMAND_REPORT_EDID_INFO_AUDIODES
{

    EN_MAPI_RAPTORS_MAILBOX_COMMAND_TYPE command_idx; /// val = E_MAPI_MAILBOX_REPORT_EDID_INFO

    /// length of the following data
    MS_U8 data_len; /// Bytes

    /// query type
    EN_MAPI_RAPTORS_QUERY_TYPE query_type; /// val = E_MAPI_RAPTORS_QUERY_AUDIO_DESCRIPTOR

    /// data
    MS_U8 audstr_Idx; /// start index of audio descriptor
    MS_U8 audend_Idx; /// end index of audio descriptor
    MS_U8 aud_des[33];   /// audio descriptors
}ST_MAILBOX_COMMAND_REPORT_EDID_INFO_AUDIODES;// struct of audio descriptor

/// Mailbox Data Struct For Command 0x05 Report EDID Information (Different Query Type Has Different Struct)
/// Query Type = Number of Video Descriptor, Query_Type = E_MAPI_RAPTORS_QUERY_VIDEO_DESCRIPTOR_NUM 0x05
typedef struct ST_MAILBOX_COMMAND_REPORT_EDID_INFO_VIDEODES_NUM
{

    EN_MAPI_RAPTORS_MAILBOX_COMMAND_TYPE command_idx; /// val = E_MAPI_MAILBOX_REPORT_EDID_INFO

    /// length of the following data
    MS_U8 data_len; /// Bytes

    /// query type
    EN_MAPI_RAPTORS_QUERY_TYPE query_type; /// val = E_MAPI_RAPTORS_QUERY_VIDEO_DESCRIPTOR_NUM

    /// data
    MS_U8 viddes_num; /// number of audio descriptor
}ST_MAILBOX_COMMAND_REPORT_EDID_INFO_VIDEODES_NUM;// struct of number of video descriptor

/// Mailbox Data Struct For Command 0x05 Report EDID Information (Different Query Type Has Different Struct)
/// Query Type = Video Descriptor, Query_Type = E_MAPI_RAPTORS_QUERY_VIDEO_DESCRIPTOR 0x06
typedef struct ST_MAILBOX_COMMAND_REPORT_EDID_INFO_VIDEODES
{

    EN_MAPI_RAPTORS_MAILBOX_COMMAND_TYPE command_idx; /// val = E_MAPI_MAILBOX_REPORT_EDID_INFO

    /// length of the following data
    MS_U8 data_len; /// Bytes

    /// query type
    EN_MAPI_RAPTORS_QUERY_TYPE query_type; /// val = E_MAPI_RAPTORS_QUERY_VIDEO_DESCRIPTOR

    /// data
    MS_U8 vidstr_Idx; /// start index of video descriptor
    MS_U8 vidend_Idx; /// end index of video descriptor
    MS_U8 vid_des[33];   /// video descriptors
}ST_MAILBOX_COMMAND_REPORT_EDID_INFO_VIDEODES;// struct of video descriptor

/// Mailbox Data Struct For Command 0x05 Report EDID Information (Different Query Type Has Different Struct)
/// Query Type = EDID Raw Data, Query_Type = E_MAPI_RAPTORS_QUERY_EDID_DATA 0x07
typedef struct ST_MAILBOX_COMMAND_REPORT_EDID_INFO_EDIDRAWDATA
{

    EN_MAPI_RAPTORS_MAILBOX_COMMAND_TYPE command_idx; /// val = E_MAPI_MAILBOX_REPORT_EDID_INFO

    /// length of the following data
    MS_U8 data_len; /// Bytes

    /// query type
    EN_MAPI_RAPTORS_QUERY_TYPE query_type; /// val = E_MAPI_RAPTORS_QUERY_EDID_DATA

    /// data
    MS_U8 Idx_edid;    /// index of edid
    MS_U8 Idx_64bytes; /// index of 64bytes
    MS_U8 raw_data[68];   /// raw data
}ST_MAILBOX_COMMAND_REPORT_EDID_INFO_EDIDRAWDATA;// struct of edid raw data

/// Mailbox Data Struct For Command 0x06 Status Response / ACK-Command
typedef struct ST_MAILBOX_COMMAND_ACK_STATUS_RESPONSE
{

    EN_MAPI_RAPTORS_MAILBOX_COMMAND_TYPE command_idx; /// val = E_MAPI_MAILBOX_ACK_STATUS_RESPOND

    /// length of the following data
    MS_U8 data_len; /// Bytes

    /// received command
    EN_MAPI_RAPTORS_MAILBOX_RECIEVED_COMMAND_TYPE mbx_reccom; /// received command type

    /// status
    EN_MAPI_RAPTORS_MAILBOX_STATUS_RESPONSE_TYPE mbx_status;  /// received status type

}ST_MAILBOX_COMMAND_ACK_STATUS_RESPONSE;// struct of status response or ack-command



void MDrv_Rocker2_SWI2C_Init(void)
{
    static MS_U8 is_first_init = FALSE;
    UBOOT_TRACE("IN\n");
    if(is_first_init == FALSE)
    {
        UBOOT_DEBUG("[Rocker2] Initialize SW I2C\n");
        MApi_SWI2C_Init(Rocket2_I2cBusCfg, SWI2C_I2C_BUS_NUM);
        is_first_init = TRUE;
    }
    else
    {
        UBOOT_ERROR("[Rocker2] SW I2C is already initialized\n");
        return;
    }
    UBOOT_TRACE("OK\n");
}

static MS_BOOL Raptors_IIC_WriteBytes( MS_U32 uAddrCnt, MS_U8 *pRegAddr, MS_U32 uSize, MS_U8 *pData)
{

    if(MApi_SWI2C_WriteBytes(ROCKET_I2C_WR_SLAVE_ID,uAddrCnt, pRegAddr, uSize, pData) == FALSE)
    {
        UBOOT_ERROR("%s, fail\n", __func__);
        return FALSE;
    }
    else
        return TRUE;

}

static MS_BOOL Raptors_IIC_ReadBytes( MS_U32 uAddrCnt, MS_U8 *pRegAddr, MS_U32 uSize, MS_U8 *pData)
{

    if(MApi_SWI2C_ReadBytes(ROCKET_I2C_WR_SLAVE_ID,uAddrCnt, pRegAddr, uSize, pData) == FALSE)
        {
        UBOOT_ERROR("%s, fail\n", __func__);
        return FALSE;
        }
    else
        return TRUE;
}


MS_BOOL Device_Hdmitx_IIC_WriteBytes(MS_U16 u16Addr, MS_U8 u8Buf)
{
        MS_U8 u8Databuf[5] = {0, 0, 0, 0, 0};
        MS_BOOL bResult = TRUE;

        u8Databuf[0] = 0x10;
        u8Databuf[3] = u8Buf;

        u8Databuf[1] = (MS_U8)(u16Addr >> 8);          //the high byte need not move left 1 bit
        u8Databuf[2] = (MS_U8)((u16Addr & 0xFF));        //the low byte moves left 1 bit, reset bit0 means data low byte
        #if(ENABLE_MSTAR_MUJI==1)
            mdelay(10);
        #endif
        if(Raptors_IIC_WriteBytes( 0, NULL, 4, u8Databuf) == FALSE)
        {
            UBOOT_ERROR("IIC Write fail\n");
            bResult = Raptors_IIC_WriteBytes( 0, NULL, 4, u8Databuf);
        }
        return bResult;

}

MS_BOOL Device_Hdmitx_IIC_ReadBytes(MS_U16 u16Addr, MS_U8* pBuf)
{

        MS_BOOL bResult = TRUE;
        MS_U8 u8Databuf[6]={0, 0, 0, 0, 0, 0};

        u8Databuf[0] = 0x10;

        u8Databuf[1] = (MS_U8)(u16Addr >> 8);          //the high byte need not move left 1 bit
        u8Databuf[2] = (MS_U8)((u16Addr & 0xFF));        //the low byte moves left 1 bit, reset bit0 means data low byte

        #if(ENABLE_MSTAR_MUJI==1)
            mdelay(10);
        #endif
        if(Raptors_IIC_WriteBytes( 0, NULL, 3, u8Databuf) == FALSE)
        {
            UBOOT_ERROR("IIC Read fail 1\n");
            bResult &= (Raptors_IIC_WriteBytes( 0, NULL, 3, u8Databuf));
        }

        if(Raptors_IIC_ReadBytes(0, NULL, 1, pBuf) == FALSE)
        {
            UBOOT_ERROR("IIC Read fail 2\n");
            bResult &= (Raptors_IIC_ReadBytes( 0, NULL, 1, pBuf));
        }

        return bResult;

}

void Raptors_w2byte(MS_U16 u16Addr, MS_U16 u16val)
{
    MS_U8 u8regval;


    UBOOT_DEBUG("w2byte: %04x, %04x\n", u16Addr, u16val);


    u8regval = (MS_U8)(u16val & 0xFF);
    Device_Hdmitx_IIC_WriteBytes(u16Addr, u8regval);

    u8regval = (MS_U8)(u16val >> 8);
    Device_Hdmitx_IIC_WriteBytes(u16Addr+1, u8regval);

}


MS_U16 Raptors_r2byte(MS_U16 u16Addr)
{
    MS_U8 u8regval;
    MS_U16 u16regval;

    UBOOT_DEBUG("Raptors_r2byte: 0x%04x\n", u16Addr);


    Device_Hdmitx_IIC_ReadBytes(u16Addr, &u8regval);
    u16regval = u8regval;

    Device_Hdmitx_IIC_ReadBytes(u16Addr+1, &u8regval);
    u16regval |= (((MS_U16)u8regval)<<8);


    return u16regval;
}


MS_BOOL Mailbox_WriteCommand(EN_MAPI_RAPTORS_MAILBOX_COMMAND_TYPE command_idx, MS_U8 data_len, MS_U8 *pBuf)
{
    int i=0;
    Device_Hdmitx_IIC_WriteBytes(MAILBOX_REG_BASE | 0x00,command_idx);
    Device_Hdmitx_IIC_WriteBytes(MAILBOX_REG_BASE | 0x01,data_len);
    for(i=0;i<data_len;i++)
        {
         Device_Hdmitx_IIC_WriteBytes(MAILBOX_REG_BASE + 0x02 + i,pBuf[i]);
         //printf("Data = %x\n",pBuf[i]);
        }
    return TRUE;
}

MS_BOOL Mailbox_ReadCommand(EN_MAPI_RAPTORS_MAILBOX_COMMAND_TYPE command_idx, MS_U8 data_len, MS_U8 *pBuf)
{
    MS_U8 read_command;
    int i=0;
    Device_Hdmitx_IIC_ReadBytes(MAILBOX_REG_BASE | 0x00,&read_command);
    switch(read_command)
        {
        case 0x01:
            command_idx=E_MAPI_MAILBOX_CONFIQ_VIDEO;
            break;
        case 0x02:
            command_idx=E_MAPI_MAILBOX_CONFIQ_AUDIO;
            break;
        case 0x03:
            command_idx=E_MAPI_MAILBOX_AVMUTE_TIMING_RESET;
            break;
        case 0x04:
            command_idx=E_MAPI_MAILBOX_QUERY_EDID_INFO;
            break;
        case 0x05:
            command_idx=E_MAPI_MAILBOX_REPORT_EDID_INFO;
            break;
        case 0x06:
            command_idx=E_MAPI_MAILBOX_ACK_STATUS_RESPOND;
            break;
        case 0xFF:
            command_idx=E_MAPI_MAILBOX_DEBUG;
            break;
        default:
            UBOOT_ERROR("Mailbox_ReadCommand Fail!!! Wrong Command Type!!!\n");
            return FALSE;
        }
    Device_Hdmitx_IIC_ReadBytes(MAILBOX_REG_BASE | 0x01,&data_len);


        for(i=0;i<data_len;i++)
            {
             Device_Hdmitx_IIC_ReadBytes(MAILBOX_REG_BASE + 0x02 + i,&pBuf[i]);
            }
        return TRUE;

}

MS_BOOL Mailbox_WriteCommand_Config_Video(ST_MAILBOX_COMMAND_CONFIQ_VIDEO *pBuf)
{
    if (pBuf->command_idx!=E_MAPI_MAILBOX_CONFIQ_VIDEO)
        {
        UBOOT_ERROR("Wrong Command Type !!!\n");
        return FALSE;
        }
    if (pBuf->data_len>7)
        {
        UBOOT_ERROR("Wrong Data Length for Mailbox Write Command Configure Video, Maximum Data Length is 7 !!!\n");
        return FALSE;
        }
    MS_U8 pData[7];
    pData[0]=(pBuf->analog_present<<4)|(pBuf->VSinfo_3D_present<<2)|(pBuf->color_present<<1)|pBuf->timing_present;
    int dataIdx=1;
    if(pBuf->timing_present && (pBuf->data_len>dataIdx))
        {
        pData[dataIdx]=pBuf->timing_idx;
        dataIdx++;
        pData[dataIdx]=(pBuf->output_mode<<3) | pBuf->color_depth;
        if(!pBuf->color_present)
            dataIdx++;
        }
    if(pBuf->color_present && (pBuf->data_len>dataIdx))
        {
        if(pBuf->timing_present)
            pData[dataIdx]=(pBuf->out_color<<6) | (pBuf->in_color<<5) | pData[dataIdx];
        else
            pData[dataIdx]=(pBuf->out_color<<6) | (pBuf->in_color<<5);
        dataIdx++;
        }
    if(pBuf->VSinfo_3D_present && (pBuf->data_len>dataIdx))
        {
        pData[dataIdx]=pBuf->vs_3d;
        dataIdx++;
        }
    if(pBuf->analog_present && (pBuf->data_len>dataIdx))
        {
        pData[dataIdx]=(pBuf->pren2<<4) | pBuf->current;
        pData[dataIdx+1]=(pBuf->tenpre<<4) | pBuf->precon;
        }
    return Mailbox_WriteCommand(pBuf->command_idx,pBuf->data_len,pData);

}

MS_BOOL Mailbox_WriteCommand_Config_Audio(ST_MAILBOX_COMMAND_CONFIQ_AUDIO *pBuf)
{
    if (pBuf->command_idx!=E_MAPI_MAILBOX_CONFIQ_AUDIO)
        {
        UBOOT_ERROR("Wrong Command Type !!!\n");
        return FALSE;
        }
    if (pBuf->data_len>5)
        {
        UBOOT_ERROR("Wrong Data Length for Mailbox Write Command Configure Audio, Maximum Data Length is 5 !!!\n");
        return FALSE;
        }
    MS_U8 pData[5];
    pData[0]=(pBuf->fmt_present<<2)|(pBuf->source_present<<1)|pBuf->inform_present;
    int dataIdx=1;
    if(pBuf->inform_present && (pBuf->data_len>dataIdx))
        {
        pData[dataIdx]=(pBuf->channel_num<<4)|pBuf->frequency;
        pData[dataIdx+1]=pBuf->code_type;
        dataIdx=dataIdx+2;
        }
    if(pBuf->source_present && (pBuf->data_len>dataIdx))
        {
        pData[dataIdx]=pBuf->source_type;
        dataIdx++;
        }
    if(pBuf->fmt_present && (pBuf->data_len>dataIdx))
        {
        pData[dataIdx]=pBuf->source_fmt;
        }

    return Mailbox_WriteCommand(pBuf->command_idx,pBuf->data_len,pData);

}

MS_BOOL Mailbox_WriteCommand_TimingChange_AVmute(ST_MAILBOX_COMMAND_TIMING_CHANGE_AVMUTE *pBuf)
{
    if (pBuf->command_idx!=E_MAPI_MAILBOX_AVMUTE_TIMING_RESET)
        {
        UBOOT_ERROR("Wrong Command Type !!!\n");
        return FALSE;
        }
    if (pBuf->data_len>2)
        {
        UBOOT_ERROR("Wrong Data Wrong for Mailbox Write Command Timing Change & AudioVideo Mute, Maximum Data Length is 2 !!!\n");
        return FALSE;
        }
    MS_U8 pData[2];
    pData[0]=(pBuf->timing_flag<<3)|(pBuf->avmute_flag<<2)|(pBuf->audio_flag<<1)|pBuf->video_flag;
    pData[1]=(pBuf->enTiming<<3)|(pBuf->enAV_mute<<2)|(pBuf->enAudio_mute<<1)|pBuf->enVideo_mute;

    return Mailbox_WriteCommand(pBuf->command_idx,pBuf->data_len,pData);

}

MS_BOOL Mailbox_WriteCommand_Query_EDID(ST_MAILBOX_COMMAND_QUERY_EDID_INFO *pBuf)
{
    if (pBuf->command_idx!=E_MAPI_MAILBOX_QUERY_EDID_INFO)
        {
        UBOOT_ERROR("Wrong Command Type !!!\n");
        return FALSE;
        }
    if (pBuf->data_len>3)
        {
        UBOOT_ERROR("Wrong Data Length for Mailbox Write Command Query EDID Information, Maximum Data Length is 2 !!!\n");
        return FALSE;
        }
    MS_U8 pData[3];
    pData[0]=pBuf->query_type;
    if(pBuf->data_len>1)
        {

        switch(pBuf->query_type)
            {
                case E_MAPI_RAPTORS_QUERY_3D_STRUCTURE:
                    pData[1]=pBuf->timing_3d;
                    pData[2]=0;
                    break;
                case E_MAPI_RAPTORS_QUERY_AUDIO_DESCRIPTOR:
                    pData[1]=pBuf->des_startIdx;
                    pData[2]=pBuf->des_endIdx;
                    break;
                case E_MAPI_RAPTORS_QUERY_VIDEO_DESCRIPTOR:
                    pData[1]=pBuf->des_startIdx;
                    pData[2]=pBuf->des_endIdx;
                    break;
                case E_MAPI_RAPTORS_QUERY_EDID_DATA:
                    pData[1]=pBuf->edid_Idx;
                    pData[2]=pBuf->bytes64_Idx;
                    break;
                default:
                    UBOOT_ERROR("Wrong Data Length for this Query Type in Mailbox Write Command EDID Information !!!\n");
                    return FALSE;
            }

        }


    return Mailbox_WriteCommand(pBuf->command_idx,pBuf->data_len,pData);

}

MS_BOOL Mailbox_ReadCommand_Report_4K2KVic (ST_MAILBOX_COMMAND_REPORT_EDID_INFO_4K2KVIC *pBuf)
{
    MS_U8 pData[10];
    int i=0;
    if(Mailbox_ReadCommand(pBuf->command_idx,pBuf->data_len,pData))
        {
        if(pBuf->command_idx!=E_MAPI_MAILBOX_REPORT_EDID_INFO)
            {
            UBOOT_ERROR("Mailbox_ReadCommand_Report_4K2KVic Fail!!! Wrong Command Type!!!");
            return FALSE;
            }
        if(pData[0]!=0x01)
            {
            UBOOT_ERROR("Mailbox_ReadCommand_Report_4K2KVic Fail!!! Wrong Query Type!!!");
            return FALSE;
            }
        pBuf->vic_num=pData[1]&0x0F;
        for(i=0;i<(pBuf->data_len - 2);i++)
            {
            switch(pData[i+2])
                {
                    case 0x00:
                        pBuf->vic4k2k[i]=E_MAPI_RAPTORS_HDMITX_VIDEO_4K2K_Reserved;
                        break;
                    case 0x01:
                        pBuf->vic4k2k[i]=E_MAPI_RAPTORS_HDMITX_VIDEO_4K2K_30Hz;
                        break;
                    case 0x02:
                        pBuf->vic4k2k[i]=E_MAPI_RAPTORS_HDMITX_VIDEO_4K2K_25Hz;
                        break;
                    case 0x03:
                        pBuf->vic4k2k[i]=E_MAPI_RAPTORS_HDMITX_VIDEO_4K2K_24Hz;
                        break;
                    case 0x04:
                        pBuf->vic4k2k[i]=E_MAPI_RAPTORS_HDMITX_VIDEO_4K2K_24Hz_SMPTE;
                        break;
                    default:
                        UBOOT_ERROR("Mailbox_ReadCommand_Report_4K2KVic Fail !!! Wrong Type of 4K2KVic !!!\n");
                        pBuf->vic4k2k[i]=E_MAPI_RAPTORS_HDMITX_VIDEO_4K2K_Reserved;
                }
            }
        return TRUE;
        }
    else
        {
        UBOOT_ERROR("Mailbox_ReadCommand_Report_4K2KVic Read Fail !!!\n");
        return FALSE;
        }
}


MS_BOOL Mailbox_ReadCommand_Report_3D_Structure(ST_MAILBOX_COMMAND_REPORT_EDID_INFO_3DSTRUCT *pBuf)
{
    MS_U8 data_len=0x04;
    MS_U8 pData[4];
    if(Mailbox_ReadCommand(pBuf->command_idx,pBuf->data_len,pData))
        {
        if(pBuf->data_len!=data_len)
            {
            UBOOT_ERROR("Mailbox_ReadCommand_Report_3D_Structure Fail !!! Wrong Data Length!!!");
            return FALSE;
            }
        if(pData[0]==0x02)
            {
            switch(pData[1])
                {
                    case 0x00:
                        pBuf->repTiming_3d=E_MAPI_RAPTORS_HDMITX_QUERY_3DTIMING_1280x720p_50Hz;
                        break;
                    case 0x01:
                        pBuf->repTiming_3d=E_MAPI_RAPTORS_HDMITX_QUERY_3DTIMING_1280x720p_60Hz;
                        break;
                    case 0x02:
                        pBuf->repTiming_3d=E_MAPI_RAPTORS_HDMITX_QUERY_3DTIMING_1920x1080i_50Hz;
                        break;
                    case 0x03:
                        pBuf->repTiming_3d=E_MAPI_RAPTORS_HDMITX_QUERY_3DTIMING_1920x1080i_60Hz;
                        break;
                    case 0x04:
                        pBuf->repTiming_3d=E_MAPI_RAPTORS_HDMITX_QUERY_3DTIMING_1920x1080p_24Hz;
                        break;
                    case 0x05:
                        pBuf->repTiming_3d=E_MAPI_RAPTORS_HDMITX_QUERY_3DTIMING_1920x1080p_25Hz;
                        break;
                    case 0x06:
                        pBuf->repTiming_3d=E_MAPI_RAPTORS_HDMITX_QUERY_3DTIMING_1920x1080p_50Hz;
                        break;
                    case 0x07:
                        pBuf->repTiming_3d=E_MAPI_RAPTORS_HDMITX_QUERY_3DTIMING_1920x1080p_60Hz;
                        break;
                    default:
                        UBOOT_ERROR("Mailbox_ReadCommand_Report_3D_Structure Fail !!! Wrong Type of 3D Timing !!!\n");
                        return FALSE;
                }
            MS_U16 Data3D;
            Data3D = (pData[3]<<8) | pData[2];
            if(Data3D== 0x0001)
                pBuf->edid_3d=E_MAPI_RAPTORS_HDMITX_VIDEO_REPORT_3D_FramePacking;
            else if(Data3D== 0x0002)
                pBuf->edid_3d=E_MAPI_RAPTORS_HDMITX_VIDEO_REPORT_3D_FieldAlternative;
            else if(Data3D== 0x0004)
                pBuf->edid_3d=E_MAPI_RAPTORS_HDMITX_VIDEO_REPORT_3D_LineAlternative;
            else if(Data3D== 0x0008)
                pBuf->edid_3d=E_MAPI_RAPTORS_HDMITX_VIDEO_REPORT_3D_SidebySide_FULL;
            else if(Data3D== 0x0010)
                pBuf->edid_3d=E_MAPI_RAPTORS_HDMITX_VIDEO_REPORT_3D_L_Dep;
            else if(Data3D== 0x0020)
                pBuf->edid_3d=E_MAPI_RAPTORS_HDMITX_VIDEO_REPORT_3D_L_Dep_Graphic_Dep;
            else if(Data3D== 0x0040)
                pBuf->edid_3d=E_MAPI_RAPTORS_HDMITX_VIDEO_REPORT_3D_TopandBottom;
            else if(Data3D== 0x0080)
                pBuf->edid_3d=E_MAPI_RAPTORS_HDMITX_VIDEO_REPORT_3D_SidebySide_Half_Horizontal;
            else if(Data3D== 0x8000)
                pBuf->edid_3d=E_MAPI_RAPTORS_HDMITX_VIDEO_REPORT_3D_SidebySide_Half_Quincunx;
            else
                {
                UBOOT_ERROR("Mailbox_ReadCommand_Report_3D_Structure Fail !!! Wrong Type of 3D Timing !!!\n");
                return FALSE;
                }
            return TRUE;
            }
        else
            {
            UBOOT_ERROR("Mailbox_ReadCommand_Report_3D_Structure Fail !!! Wrong Query Type!!!\n");
            return FALSE;
            }
        }
    else
        {
        UBOOT_ERROR("Mailbox_ReadCommand_Report_3D_Structure Read Fail !!!\n");
        return FALSE;
        }
}

MS_BOOL Mailbox_ReadCommand_Report_Num_AudioDescriptor(ST_MAILBOX_COMMAND_REPORT_EDID_INFO_AUDIODES_NUM *pBuf)
{
    MS_U8 data_len=0x02;
    MS_U8 pData[2];
    if(Mailbox_ReadCommand(pBuf->command_idx,pBuf->data_len,pData))
        {
        if(pBuf->command_idx!=E_MAPI_MAILBOX_REPORT_EDID_INFO)
            {
            UBOOT_ERROR("Mailbox_ReadCommand_Report_Num_AudioDescriptor Fail!!! Wrong Command Type!!!");
            return FALSE;
            }
        if(pBuf->data_len!=data_len)
            {
            UBOOT_ERROR("Mailbox_ReadCommand_Report_Num_AudioDescriptor Fail!!! Wrong Data Length!!!");
            return FALSE;
            }
        if(pData[0]!=0x03)
            {
            UBOOT_ERROR("Mailbox_ReadCommand_Report_Num_AudioDescriptor Fail!!! Wrong Query Type!!!");
            return FALSE;
            }
        pBuf->auddes_num=pData[1];
        return TRUE;
        }
    else
        {
        UBOOT_ERROR("Mailbox_ReadCommand_Report_Num_AudioDescriptor Read Fail !!!\n");
        return FALSE;
        }
}

MS_BOOL Mailbox_ReadCommand_Report_AudioDescriptor(ST_MAILBOX_COMMAND_REPORT_EDID_INFO_AUDIODES *pBuf)
{
    MS_U8 pData[68];
    int i=0;
    if(Mailbox_ReadCommand(pBuf->command_idx,pBuf->data_len,pData))
        {
        if(pBuf->command_idx!=E_MAPI_MAILBOX_REPORT_EDID_INFO)
            {
            UBOOT_ERROR("Mailbox_ReadCommand_Report_AudioDescriptor Fail!!! Wrong Command Type!!!");
            return FALSE;
            }
        if(pData[0]!=0x04)
            {
            UBOOT_ERROR("Mailbox_ReadCommand_Report_AudioDescriptor Fail!!! Wrong Query Type!!!");
            return FALSE;
            }
        pBuf->audstr_Idx=pData[1];
        pBuf->audend_Idx=pData[2];
        for(i=0;i<(pBuf->data_len - 3);i++)
            {
            pBuf->aud_des[i]=pData[i+3];
            }
        return TRUE;
        }
    else
        {
        UBOOT_ERROR("Mailbox_ReadCommand_Report_AudioDescriptor Read Fail !!!\n");
        return FALSE;
        }
}

MS_BOOL Mailbox_ReadCommand_Report_Num_VideoDescriptor(ST_MAILBOX_COMMAND_REPORT_EDID_INFO_VIDEODES_NUM *pBuf)
{
    MS_U8 data_len=0x02;
    MS_U8 pData[2];
    if(Mailbox_ReadCommand(pBuf->command_idx,pBuf->data_len,pData))
        {
        if(pBuf->command_idx!=E_MAPI_MAILBOX_REPORT_EDID_INFO)
            {
            UBOOT_ERROR("Mailbox_ReadCommand_Report_Num_VideoDescriptor Fail!!! Wrong Command Type!!!");
            return FALSE;
            }
        if(pBuf->data_len!=data_len)
            {
            UBOOT_ERROR("Mailbox_ReadCommand_Report_Num_VideoDescriptor Fail!!! Wrong Data Length!!!");
            return FALSE;
            }
        if(pData[0]!=0x05)
            {
            UBOOT_ERROR("Mailbox_ReadCommand_Report_Num_VideoDescriptor Fail!!! Wrong Query Type!!!");
            return FALSE;
            }
        pBuf->viddes_num=pData[1];
        return TRUE;
        }
    else
        {
        UBOOT_ERROR("Mailbox_ReadCommand_Report_Num_VideoDescriptor Read Fail !!!\n");
        return FALSE;
        }
}

MS_BOOL Mailbox_ReadCommand_Report_VideoDescriptor(ST_MAILBOX_COMMAND_REPORT_EDID_INFO_VIDEODES *pBuf)
{
    MS_U8 pData[68];
    int i=0;
    if(Mailbox_ReadCommand(pBuf->command_idx,pBuf->data_len,pData))
        {
        if(pBuf->command_idx!=E_MAPI_MAILBOX_REPORT_EDID_INFO)
            {
            UBOOT_ERROR("Mailbox_ReadCommand_Report_VideoDescriptor Fail!!! Wrong Command Type!!!");
            return FALSE;
            }
        if(pData[0]!=0x06)
            {
            UBOOT_ERROR("Mailbox_ReadCommand_Report_VideoDescriptor Fail!!! Wrong Query Type!!!");
            return FALSE;
            }
        pBuf->vidstr_Idx=pData[1];
        pBuf->vidend_Idx=pData[2];
        for(i=0;i<(pBuf->data_len - 3);i++)
            {
            pBuf->vid_des[i]=pData[i+3];
            }
        return TRUE;
        }
    else
        {
        UBOOT_ERROR("Mailbox_ReadCommand_Report_VideoDescriptor Read Fail !!!\n");
        return FALSE;
        }
}

MS_BOOL Mailbox_ReadCommand_Report_EDID_RawData(ST_MAILBOX_COMMAND_REPORT_EDID_INFO_EDIDRAWDATA *pBuf)
{
    MS_U8 pData[68];
    int i=0;
    if(Mailbox_ReadCommand(pBuf->command_idx,pBuf->data_len,pData))
        {
        if(pBuf->command_idx!=E_MAPI_MAILBOX_REPORT_EDID_INFO)
            {
            UBOOT_ERROR("Mailbox_ReadCommand_Report_EDID_RawData Fail!!! Wrong Command Type!!!");
            return FALSE;
            }
        if(pData[0]!=0x07)
            {
            UBOOT_ERROR("Mailbox_ReadCommand_Report_EDID_RawData Fail!!! Wrong Query Type!!!");
            return FALSE;
            }
        pBuf->Idx_edid=pData[1]&0x0F;
        pBuf->Idx_64bytes=pData[1]&0xF0;
        for(i=0;i<(pBuf->data_len - 2);i++)
            {
            pBuf->raw_data[i]=pData[i+2];
            }
        return TRUE;
        }
    else
        {
        UBOOT_ERROR("Mailbox_ReadCommand_Report_EDID_RawData Read Fail !!!\n");
        return FALSE;
        }
}


MS_BOOL Mailbox_ReadCommand_ACK_Status(ST_MAILBOX_COMMAND_ACK_STATUS_RESPONSE *pBuf)
{
    MS_U8 data_len=0x02;
    MS_U8 pData[2];
    UBOOT_TRACE("IN\n");
    if(Mailbox_ReadCommand(pBuf->command_idx,pBuf->data_len,pData))
        {
        if(pBuf->data_len!=data_len)
            {
            UBOOT_ERROR("Mailbox_ReadCommand_ACK_Status Fail!!! Wrong Data Length!!!");
            return FALSE;
            }
        if(pBuf->command_idx==E_MAPI_MAILBOX_ACK_STATUS_RESPOND)
            {
            switch(pData[0])
                {
                    case 0x01:
                        pBuf->mbx_reccom=E_MAPI_MAILBOX_REPORT_CONFIQ_VIDEO;
                        break;
                    case 0x02:
                        pBuf->mbx_reccom=E_MAPI_MAILBOX_REPORT_CONFIQ_AUDIO;
                        break;
                    case 0x03:
                        pBuf->mbx_reccom=E_MAPI_MAILBOX_REPORT_AVMUTE_TIMING_RESET;
                        break;
                    case 0x04:
                        pBuf->mbx_reccom=E_MAPI_MAILBOX_REPORT_REPORT_EDID_INFO;
                        break;
                    default:
                        UBOOT_ERROR("Wrong Received Command Type !!!\n");
                        return FALSE;
                }

            switch(pData[1])
                {
                    case 0x00:
                        pBuf->mbx_status=E_MAPI_MAILBOX_REPORT_SUCCESS;
                        break;
                    case 0x01:
                        pBuf->mbx_status=E_MAPI_MAILBOX_REPORT_FAIL;
                        break;
                    case 0x02:
                        pBuf->mbx_status=E_MAPI_MAILBOX_REPORT_PARAMETER_INVALID;
                        break;
                    default:
                        UBOOT_ERROR("Wrong Status Type !!!\n");
                        return FALSE;
                }
            return TRUE;
            }
        else
            {
            printf("Mailbox_ReadCommand_ACK_Status Fail !!! Wrong Command Type!!!\n");
            return FALSE;
            }
        }
    else
        {
        printf("Mailbox_ReadCommand_ACK_Status Read Fail !!!\n");
        return FALSE;
        }
    UBOOT_TRACE("OK\n");
}



MS_BOOL EnterSerialDebugMode(MS_BOOL bEnable)
{

        MS_U8 u8DataBuf[5] = {0x53, 0x45, 0x52, 0x44, 0x42};
        MS_BOOL bResult = TRUE;
        MS_U8 u8cnt = 0;

        printf("EnterSerialDebugMode:%d ++++++++++++++++++++++++++\n", bEnable);
        if (bEnable)
        {
            u8DataBuf[0] = 0x53;

            while(Raptors_IIC_WriteBytes( 0, NULL, 5, u8DataBuf) == FALSE)
            {
                u8cnt++;
                bResult = FALSE;
                //printf("FAIL %d\n", u8cnt);
                if(u8cnt == 0x20)
                    break;
            }
        }
        else
        {
            u8DataBuf[0] = 0x45;

            while(Raptors_IIC_WriteBytes( 0, NULL, 5, u8DataBuf) == FALSE)
            {
                u8cnt++;
                bResult = FALSE;
                if(u8cnt == 0x20)
                    break;
            }
        }
        return bResult;


}

MS_BOOL IIC_Config(void)
{

    MS_U8 u8DataBuf;
    MS_BOOL bResult = TRUE;

    printf("IIC_Config ++++++++++++++++++++++++++++\n");

    // change to CH3
    u8DataBuf = 0x81;
    if(Raptors_IIC_WriteBytes(0, NULL, 1, &u8DataBuf) == FALSE)
    {
        printf("Ch3  0x81 fail\n");
        bResult &= Raptors_IIC_WriteBytes( 0, NULL, 1, &u8DataBuf);
    }

    u8DataBuf = 0x83;
    if(Raptors_IIC_WriteBytes( 0, NULL, 1, &u8DataBuf) == FALSE)
    {
        printf("Ch3  0x83 fail\n");
        bResult &= Raptors_IIC_WriteBytes( 0, NULL, 1, &u8DataBuf);
    }

    u8DataBuf = 0x84;
    if(Raptors_IIC_WriteBytes( 0, NULL, 1, &u8DataBuf) == FALSE)
    {
        printf("Ch3  0x84 fail\n");
        bResult &= Raptors_IIC_WriteBytes( 0, NULL, 1, &u8DataBuf);
    }

    // address 2 byte
    u8DataBuf = 0x51;
    if(Raptors_IIC_WriteBytes( 0, NULL, 1, &u8DataBuf) == FALSE)
    {
        printf("Ch3  0x51 fail\n");
        bResult &= Raptors_IIC_WriteBytes( 0, NULL, 1, &u8DataBuf);
    }
    // config
    u8DataBuf = 0x7F;
    if(Raptors_IIC_WriteBytes( 0, NULL, 1, &u8DataBuf) == FALSE)
    {
        printf("Ch3  0x7F fail\n");
        bResult &= Raptors_IIC_WriteBytes( 0, NULL, 1, &u8DataBuf);
    }

    return bResult;

}

MS_BOOL IICUseBus(MS_BOOL bEnable)
{

    MS_U8 u8DataBuf;
    MS_BOOL bResult = FALSE;
    UBOOT_TRACE("IN\n");

    if(bEnable)
        u8DataBuf = 0x35;
    else
        u8DataBuf = 0x34;

    printf("IICUseBus:%d ++++++++++++++++++++++++++++\n", bEnable);

    if(Raptors_IIC_WriteBytes( 0, NULL, 1, &u8DataBuf) == FALSE)
    {
        printf("IICUseBus read write fail\n");
        bResult = Raptors_IIC_WriteBytes( 0, NULL, 1, &u8DataBuf);
    }

    // shape IIC Plus
    u8DataBuf = 0x71;
    if(Raptors_IIC_WriteBytes( 0, NULL, 1, &u8DataBuf) == FALSE)
    {
        printf("IICUseBus shap IIC fail\n");
        bResult = Raptors_IIC_WriteBytes( 0, NULL, 1, &u8DataBuf);
    }
    UBOOT_TRACE("OK\n");

    return bResult;

}

void Raptors_IIC_Init(void)
{

    IICUseBus(FALSE);
    EnterSerialDebugMode(FALSE);

    //MsOS_DelayTask(10);

    EnterSerialDebugMode(TRUE);
    IIC_Config();
    IICUseBus(TRUE);
}

void Raptors_SPI_Init(void)
{
    UBOOT_TRACE("IN\n");
    Raptors_w2byte(0x1304, 0x0001);
    Raptors_w2byte(0x1E40, 0x0000);
    Raptors_w2byte(0x1E42, 0x0000);
    Raptors_w2byte(0x1E44, 0x0000);
    Raptors_w2byte(0x1E46, 0x0000);
    Raptors_w2byte(0x1E48, 0x0000);
    Raptors_w2byte(0x1E4A, 0x0000);
    Raptors_w2byte(0x1E4C, 0x0000);
    Raptors_w2byte(0x1E4E, 0x0000);
    Raptors_w2byte(0x1E60, 0x0000);
    Raptors_w2byte(0x1E62, 0x0000);
    Raptors_w2byte(0x1E64, 0x0000);
    Raptors_w2byte(0x1E66, 0x0000);
    Raptors_w2byte(0x1E68, 0x0000);
    Raptors_w2byte(0x1E74, 0x0000);
    Raptors_w2byte(0x1E76, 0x0020);
    Raptors_w2byte(0x1E78, 0x0000);
    Raptors_w2byte(0x1E7A, 0x0000);
    Raptors_w2byte(0x0B00, 0x0000); //all_pad_in = 0
    Raptors_w2byte(0x0B80, 0x1000); //[10] reg_spi_mode = 1 [12] reg_uart_mode1 = 1


    Raptors_w2byte(0x000FE4, 0x0001);

    Raptors_w2byte(0x000EC0, 0x000B); //reg_fsp_wd1[7:0], reg_fsp_wd0[7:0]
    Raptors_w2byte(0x000EC2, 0x0000); //reg_fsp_wd3[7:0], reg_fsp_wd2[7:0]
    Raptors_w2byte(0x000EC4, 0x0000); //reg_fsp_wd5[7:0], reg_fsp_wd4[7:0]
    Raptors_w2byte(0x000EC6, 0x0000); //reg_fsp_wd7[7:0], reg_fsp_wd6[7:0]
    Raptors_w2byte(0x000EC8, 0x0000); //reg_fsp_wd9[7:0], reg_fsp_wd8[7:0]
    Raptors_w2byte(0x000ED4, 0x0005); //reg_fsp_wbf_size2[3:0], reg_fsp_wbf_size1[3:0], reg_fsp_wbf_size0[3:0]
    Raptors_w2byte(0x000ED6, 0x0001); //reg_fsp_rbf_size2[3:0], reg_fsp_rbf_size1[3:0], reg_fsp_rbf_size0[3:0]

    Raptors_w2byte(0x000ED8, 0x0007); //reg_fsp_ctrl1[7:0], reg_fsp_ctrl0[7:0]

    Raptors_w2byte(0x000EDA, 0x0001); //reg_fsp_trigger_le_w
    mdelay(10);
    Raptors_w2byte(0x000EDE, 0x0001); //reg_fsp_clear_done_flag_le_w

    Raptors_w2byte(0x001E72, 0x0800);  //reg_sw_spi_clk, reg_ckg_spi[3:0]
    Raptors_w2byte(0x001E72, 0x1800);  //reg_sw_spi_clk, reg_ckg_spi[3:0]


    Raptors_w2byte(0x1E20, 0x0003);
    Raptors_w2byte(0x1C80, 0x0001);

    mdelay(1);
    Raptors_w2byte(0x1C80, 0x000F);
    UBOOT_TRACE("OK\n");
}



void Fn_Send_Interrupt_Raptors(void)
{
    Raptors_w2byte(0x2B0C, 0x0000);
    Raptors_w2byte(0x2B0C, 0x0001);
}


MS_BOOL Fn_ReadACK(void)
{
    MS_BOOL bgpio=FALSE;
    bgpio=mdrv_gpio_get_level(ROCKET2_GPIO_ACK);

    //read bank
    UBOOT_DEBUG("[Device Raptors] ACK = %x\n",(int)Raptors_r2byte(0x1250));

    //gpio
    UBOOT_DEBUG("[Device Raptors] GPIO = %x\n",bgpio);

    if(bgpio)
        return TRUE;
    else
        return FALSE;
}

void Fn_SetACK_Low(void)
{
    Raptors_w2byte(0x1250,0x0000);

    Raptors_w2byte(0x0A02, (Raptors_r2byte(0x0A02) & 0xFFDF));
    Raptors_w2byte(0x0A0A, (Raptors_r2byte(0x0A0A) | 0x0020));
}

MS_BOOL Fn_WaitACK(void)
{
    UBOOT_TRACE("IN\n");
    //read Raptors ACK
    MS_U32 bFirstTimer;
    MS_U32 bTimer;
    bFirstTimer=MsOS_GetSystemTime();
    UBOOT_DEBUG("[icebox] before set 4k2k60 System Time = %d\n",(int)bFirstTimer);
    while(!Fn_ReadACK())
        {
        mdelay(100);
        bTimer=MsOS_GetSystemTime();
        if((int)(bTimer-bFirstTimer)>5000)
            {
            UBOOT_ERROR("[Device Raptors] Read MBX ACK Fail! Time out!\n");
            return FALSE;
            }
        }
    UBOOT_TRACE("OK\n");

    return TRUE;
}

MS_BOOL TurnOnOff(MS_BOOL bEn)//Turn on bEn=1 ; Turn off bEn=0
{
    UBOOT_TRACE("IN\n");
    //set AVmute flag
    ST_MAILBOX_COMMAND_TIMING_CHANGE_AVMUTE setFlag;
    setFlag.command_idx=E_MAPI_MAILBOX_AVMUTE_TIMING_RESET;
    setFlag.data_len=0x02;
    setFlag.avmute_flag=TRUE;
    setFlag.timing_flag=TRUE;
    setFlag.video_flag=FALSE;
    setFlag.audio_flag=FALSE;
    if(!bEn)
        {
        setFlag.enAV_mute=E_MAPI_RAPTORS_HDMITX_AVMUTE_ENABLE;
        setFlag.enTiming =E_MAPI_RAPTORS_HDMITX_TIMING_CHANGE_ENABLE;
        }
    else
        {
        setFlag.enAV_mute=E_MAPI_RAPTORS_HDMITX_AVMUTE_DISABLE;
        setFlag.enTiming =E_MAPI_RAPTORS_HDMITX_TIMING_CHANGE_DISABLE;
        }
    MS_BOOL bFlag;
    bFlag=Mailbox_WriteCommand_TimingChange_AVmute(&setFlag);
    UBOOT_DEBUG("[Device Raptors] SetOutputBlack Mailbox_Write TimingChange_AVmute = %x\n",bFlag);
    Fn_Send_Interrupt_Raptors();
    //

    //read Raptors ACK
    if(!Fn_WaitACK())
        {
        UBOOT_ERROR("DEVICE_HDMITX::%s, %d , Setting Wrong!!! Read ACK Fail!!!\n",__func__,__LINE__);
        return FALSE;
        }

    Fn_SetACK_Low();//set ACK low after read it high
    UBOOT_TRACE("OK\n");
    return TRUE;
}


MS_BOOL MuteAudioFIFO(MS_BOOL bEn)
{
    UBOOT_TRACE("IN\n");
    //set AVmute flag
    ST_MAILBOX_COMMAND_TIMING_CHANGE_AVMUTE setFlag;
    setFlag.command_idx=E_MAPI_MAILBOX_AVMUTE_TIMING_RESET;
    setFlag.data_len=0x02;
    setFlag.audio_flag=TRUE;
    setFlag.command_idx=E_MAPI_MAILBOX_CONFIQ_VIDEO;
    if(bEn)
        setFlag.enAudio_mute=E_MAPI_RAPTORS_HDMITX_AUDIO_MUTE_ENABLE;
    else
        setFlag.enAudio_mute=E_MAPI_RAPTORS_HDMITX_AUDIO_MUTE_DISABLE;
    MS_BOOL bFlag;
    bFlag=Mailbox_WriteCommand_TimingChange_AVmute(&setFlag);
    UBOOT_DEBUG("[Device Raptors] SetOutputBlack Mailbox_Write TimingChange_AVmute = %x\n",bFlag);
    Fn_Send_Interrupt_Raptors();
    //

    //read Raptors ACK
    if(!Fn_WaitACK())
        {
        UBOOT_ERROR("DEVICE_HDMITX::%s, %d , Setting Wrong!!! Read ACK Fail!!!\n",__func__,__LINE__);
        return FALSE;
        }

    Fn_SetACK_Low();//set ACK low after read it high
    UBOOT_TRACE("OK\n");
    return TRUE;
}
MS_BOOL SetOutputBlack(MS_BOOL bEn)
{
    UBOOT_TRACE("IN\n");
    //set AVmute flag
    ST_MAILBOX_COMMAND_TIMING_CHANGE_AVMUTE setFlag;
    setFlag.command_idx=E_MAPI_MAILBOX_AVMUTE_TIMING_RESET;
    setFlag.data_len=0x02;
    setFlag.avmute_flag=TRUE;
    if(bEn)
        setFlag.enAV_mute=E_MAPI_RAPTORS_HDMITX_AVMUTE_ENABLE;
    else
        setFlag.enAV_mute=E_MAPI_RAPTORS_HDMITX_AVMUTE_DISABLE;
    MS_BOOL bFlag;
    bFlag=Mailbox_WriteCommand_TimingChange_AVmute(&setFlag);
    UBOOT_DEBUG("[Device Raptors] SetOutputBlack Mailbox_Write TimingChange_AVmute = %x\n",bFlag);
    Fn_Send_Interrupt_Raptors();
    //

    //read Raptors ACK
    if(!Fn_WaitACK())
        {
        UBOOT_ERROR("DEVICE_HDMITX::%s, %d , Setting Wrong!!! Read ACK Fail!!!\n",__func__,__LINE__);
        return FALSE;
        }

    Fn_SetACK_Low();//set ACK low after read it high
    UBOOT_TRACE("OK\n");
    return TRUE;
}

MS_BOOL Init(void)
{
    UBOOT_TRACE("IN\n");


    //init IIC
    Raptors_IIC_Init();

    //read ChipID
    MS_U8 chipID;
    Device_Hdmitx_IIC_ReadBytes(0x1E00,&chipID);
    printf("[Device Raptors] show ChipID 1E 00 =%x\n",chipID);
    if(chipID!=0x97)
        {
        printf("[Device HDMITX] Read Chip ID FAIL!!!\n");
        return FALSE;
        }

    //init Raptors
    Raptors_SPI_Init();

    //read Raptors CPU Status
    MS_U16 chipCPU;
    MS_U32 bFirstTimer;
    MS_U32 bTimer;

    chipCPU=Raptors_r2byte(0x1EFE);
    bFirstTimer=MsOS_GetSystemTime();
    while(chipCPU!=0xB430)
        {
        printf("[Device Raptors] show ChipCPU Status 1E FE =%x\n",chipCPU);
        bTimer=MsOS_GetSystemTime();
        if((int)(bTimer-bFirstTimer)>5000)
            {
            printf("[Device Raptors] Init Fail! CPU not ready! Time out!\n");
            return FALSE;
            }
        mdelay(100);
        chipCPU=Raptors_r2byte(0x1EFE);
        }

#if 0 // patch code
    //temp setTimingType (patch code)
    Monaco_4k2k60p_8lane();
    usleep(5000*1000);
    TurnOnOff(FALSE);
    Fn_set4K2K60Timing();
    TurnOnOff(TRUE);
    Fn_Set_Audio_2ch();
    Fn_Set_SPDIF();
    //
#endif
    UBOOT_TRACE("OK\n");

    return TRUE;
}



MS_BOOL SetTimingType(EN_MAPI_DEVICE_ROCKY_VIDEO_TIMING enTiming)
{
    UBOOT_TRACE("IN\n");



    switch(enTiming)
        {
       case E_MAPI_ROCKY_RES_720x576p:
            genHDMITx_VideoTiming = E_MAPI_RAPTORS_HDMITX_TIMING_720x576p_50Hz;
            break;
        case E_MAPI_ROCKY_RES_720x480p:
            genHDMITx_VideoTiming = E_MAPI_RAPTORS_HDMITX_TIMING_720x480p_60Hz;
            break;
        case E_MAPI_ROCKY_RES_1280x720p_50Hz:
            genHDMITx_VideoTiming = E_MAPI_RAPTORS_HDMITX_TIMING_1280x720p_50Hz;
            break;
        case E_MAPI_ROCKY_RES_1280x720p_60Hz:
            genHDMITx_VideoTiming = E_MAPI_RAPTORS_HDMITX_TIMING_1280x720p_60Hz;
            break;
        case E_MAPI_ROCKY_RES_1920x1080p_24Hz:
            genHDMITx_VideoTiming = E_MAPI_RAPTORS_HDMITX_TIMING_1920x1080p_24Hz;
            break;
        case E_MAPI_ROCKY_RES_1920x1080i_50Hz:
            genHDMITx_VideoTiming = E_MAPI_RAPTORS_HDMITX_TIMING_1920x1080i_50Hz;
            break;
        case E_MAPI_ROCKY_RES_1920x1080i_60Hz:
            genHDMITx_VideoTiming = E_MAPI_RAPTORS_HDMITX_TIMING_1920x1080i_60Hz;
            break;
        case E_MAPI_ROCKY_RES_1920x1080p_50Hz:
            genHDMITx_VideoTiming = E_MAPI_RAPTORS_HDMITX_TIMING_1920x1080p_50Hz;
            break;
        case E_MAPI_ROCKY_RES_1920x1080p_60Hz:
            genHDMITx_VideoTiming = E_MAPI_RAPTORS_HDMITX_TIMING_1920x1080p_60Hz;
            break;
        case E_MAPI_ROCKY_RES_4K2Kp_25Hz:
            genHDMITx_VideoTiming = E_MAPI_RAPTORS_HDMITX_TIMING_4K2Kp_25Hz;
            break;
        case E_MAPI_ROCKY_RES_4K2Kp_30Hz:
            genHDMITx_VideoTiming = E_MAPI_RAPTORS_HDMITX_TIMING_4K2Kp_30Hz;
            break;

        case E_MAPI_ROCKY_RES_4K2Kp_50Hz:
            genHDMITx_VideoTiming = E_MAPI_RAPTORS_HDMITX_TIMING_4K2Kp_50Hz;
            break;

        case E_MAPI_ROCKY_RES_4K2Kp_60Hz:
            genHDMITx_VideoTiming = E_MAPI_RAPTORS_HDMITX_TIMING_4K2Kp_60Hz;
            break;
        default:
            UBOOT_DEBUG("Wrong Mode of EN_MAPI_HDMITX_TIMING_TYPE \n");
            return FALSE;
        }

    genMAPI_HDMITxTming = enTiming;

    UBOOT_DEBUG("+++++++   DEVICE_HDMITX:: SetTimingTYpe   +++++++\n");
    UBOOT_DEBUG("+++++++++++++++++++++++++++++++++++++++++++++++++++++\n");

    //set Raptors video
    ST_MAILBOX_COMMAND_CONFIQ_VIDEO setTiming;
    setTiming.command_idx=E_MAPI_MAILBOX_CONFIQ_VIDEO;
    setTiming.data_len=0x03;
    setTiming.timing_present=TRUE;
    setTiming.color_present=TRUE;
    setTiming.VSinfo_3D_present=FALSE;
    setTiming.analog_present=FALSE;
    setTiming.timing_idx=genHDMITx_VideoTiming;
    setTiming.color_depth=genHDMITx_ColorDepth;
    setTiming.in_color=genHDMITx_InColor;
    setTiming.output_mode=genHDMITX_Mode;
    setTiming.out_color=genHDMITx_OutColor;
    MS_BOOL bIce;
    bIce=Mailbox_WriteCommand_Config_Video(&setTiming);
    UBOOT_DEBUG("[Device Raptors] Mailbox_Write Config Video = %x\n",bIce);
    Fn_Send_Interrupt_Raptors();
    //

    //read Raptors ACK
    if(!Fn_WaitACK())
        {
        UBOOT_ERROR("DEVICE_HDMITX::%s, %d , Setting Wrong!!! Read ACK Fail!!!\n",__func__,__LINE__);
        return FALSE;
        }

    Fn_SetACK_Low();//set ACK low after read it high
    //

    //set Raptors audio
    ST_MAILBOX_COMMAND_CONFIQ_AUDIO setAudio;
    setAudio.command_idx=E_MAPI_MAILBOX_CONFIQ_AUDIO;
    setAudio.data_len=0x05;
    setAudio.inform_present=TRUE;
    setAudio.source_present=TRUE;
    setAudio.fmt_present=TRUE;
    setAudio.frequency=genAudio_Freq;
    setAudio.channel_num=genAudio_Ch_Cnt;
    setAudio.code_type=genAudio_CodeType;
    setAudio.source_type=genAudio_SrcType;
    setAudio.source_fmt=genAudio_SrcFmt;

    bIce=Mailbox_WriteCommand_Config_Audio(&setAudio);
    UBOOT_DEBUG("[Device Raptors] Mailbox_Write Config Audio = %x\n",bIce);
    Fn_Send_Interrupt_Raptors();

    //read Raptors ACK
    if(!Fn_WaitACK())
        {
        UBOOT_ERROR("DEVICE_HDMITX::%s, %d , Setting Wrong!!! Read ACK Fail!!!\n",__func__,__LINE__);
        return FALSE;
        }

    Fn_SetACK_Low();//set ACK low after read it high
    //

#if 0 //will add when mboot is ready
    if(_isFirstBoot)
    {
        MApi_Lth_Set_Disable_All_Table_Setting(FALSE);
        MApi_HDMITx_DisableRegWrite(FALSE);
        _isFirstBoot = FALSE;
    }
#endif


    UBOOT_TRACE("OK\n");

    return TRUE;
}

EN_MAPI_DEVICE_ROCKY_VIDEO_TIMING getDeviceTimingType(EN_MAPI_HDMITX_TIMING_TYPE enTiming)
{
    UBOOT_TRACE("IN\n");

    switch(enTiming)
        {
            case E_MAPI_HDMITX_TIMING_480_60I:
                return  E_MAPI_ROCKY_RES_720x480i;
                break;
            case E_MAPI_HDMITX_TIMING_480_60P:
                return  E_MAPI_ROCKY_RES_720x480p;
                break;
            case E_MAPI_HDMITX_TIMING_576_50I:
                return  E_MAPI_ROCKY_RES_720x576i;
                break;
            case E_MAPI_HDMITX_TIMING_576_50P:
                return  E_MAPI_ROCKY_RES_720x576p;
                break;
            case E_MAPI_HDMITX_TIMING_720_50P:
                return  E_MAPI_ROCKY_RES_1280x720p_50Hz;
                break;
            case E_MAPI_HDMITX_TIMING_720_60P:
                return  E_MAPI_ROCKY_RES_1280x720p_60Hz;
                break;
            case E_MAPI_HDMITX_TIMING_1080_50I:
                return  E_MAPI_ROCKY_RES_1920x1080i_50Hz;
                break;
            case E_MAPI_HDMITX_TIMING_1080_50P:
                return  E_MAPI_ROCKY_RES_1920x1080p_50Hz;
                break;
            case E_MAPI_HDMITX_TIMING_1080_60I:
                return  E_MAPI_ROCKY_RES_1920x1080i_60Hz;
                break;
            case E_MAPI_HDMITX_TIMING_1080_60P:
                return  E_MAPI_ROCKY_RES_1920x1080p_60Hz;
                break;
            case E_MAPI_HDMITX_TIMING_1080_30P:
                return  E_MAPI_ROCKY_RES_1920x1080p_30Hz;
                break;
            case E_MAPI_HDMITX_TIMING_1080_25P:
                return  E_MAPI_ROCKY_RES_1920x1080p_25Hz;
                break;
            case E_MAPI_HDMITX_TIMING_1080_24P:
                return  E_MAPI_ROCKY_RES_1920x1080p_24Hz;
                break;
            case E_MAPI_HDMITX_TIMING_4K2K_60P:
                return  E_MAPI_ROCKY_RES_4K2Kp_60Hz;
                break;
            case E_MAPI_HDMITX_TIMING_4K2K_30P:
                return  E_MAPI_ROCKY_RES_4K2Kp_30Hz;
                break;
            case E_MAPI_HDMITX_TIMING_4K2K_25P:
                return  E_MAPI_ROCKY_RES_4K2Kp_25Hz;
                break;
            case E_MAPI_HDMITX_TIMING_1470_50P:
                return  E_MAPI_ROCKY_RES_1280x1470p_50Hz;
                break;
            case E_MAPI_HDMITX_TIMING_1470_60P:
                return  E_MAPI_ROCKY_RES_1280x1470p_60Hz;
                break;
            case E_MAPI_HDMITX_TIMING_1470_24P:
                return  E_MAPI_ROCKY_RES_1280x1470p_24Hz;
                break;
            case E_MAPI_HDMITX_TIMING_1470_30P:
                return  E_MAPI_ROCKY_RES_1280x1470p_30Hz;
                break;
            case E_MAPI_HDMITX_TIMING_2160_24P:
                return  E_MAPI_ROCKY_RES_1920x2205p_24Hz;
                break;
            case E_MAPI_HDMITX_TIMING_2160_30P:
                return  E_MAPI_ROCKY_RES_1920x2205p_30Hz;
                break;
            default:
                UBOOT_ERROR("Wrong Mode of EN_MAPI_HDMITX_TIMING_TYPE \n");
                return E_MAPI_ROCKY_RES_1920x1080p_60Hz;
        }
    UBOOT_TRACE("OK\n");
}

EN_MAPI_HDMITX_TIMING_TYPE getMapiTimingType(EN_MAPI_DEVICE_ROCKY_VIDEO_TIMING enTiming)
{

    UBOOT_TRACE("IN\n");

    switch(enTiming)
    {
        case E_MAPI_ROCKY_RES_720x480i:
            return  E_MAPI_HDMITX_TIMING_480_60I;
            break;
        case E_MAPI_ROCKY_RES_720x480p:
            return  E_MAPI_HDMITX_TIMING_480_60P;
            break;
        case E_MAPI_ROCKY_RES_720x576i:
            return  E_MAPI_HDMITX_TIMING_576_50I;
            break;
        case E_MAPI_ROCKY_RES_720x576p:
            return  E_MAPI_HDMITX_TIMING_576_50P;
            break;
        case E_MAPI_ROCKY_RES_1280x720p_50Hz:
            return  E_MAPI_HDMITX_TIMING_720_50P;
            break;
        case E_MAPI_ROCKY_RES_1280x720p_60Hz:
            return  E_MAPI_HDMITX_TIMING_720_60P;
            break;
        case E_MAPI_ROCKY_RES_1920x1080i_50Hz:
            return  E_MAPI_HDMITX_TIMING_1080_50I;
            break;
        case E_MAPI_ROCKY_RES_1920x1080p_50Hz:
            return  E_MAPI_HDMITX_TIMING_1080_50P;
            break;
        case E_MAPI_ROCKY_RES_1920x1080i_60Hz:
            return  E_MAPI_HDMITX_TIMING_1080_60I;
            break;
        case E_MAPI_ROCKY_RES_1920x1080p_60Hz:
            return  E_MAPI_HDMITX_TIMING_1080_60P;
            break;
        case E_MAPI_ROCKY_RES_1920x1080p_30Hz:
            return  E_MAPI_HDMITX_TIMING_1080_30P;
            break;
        case E_MAPI_ROCKY_RES_1920x1080p_25Hz:
            return  E_MAPI_HDMITX_TIMING_1080_25P;
            break;
        case E_MAPI_ROCKY_RES_1920x1080p_24Hz:
            return  E_MAPI_HDMITX_TIMING_1080_24P;
            break;
        case E_MAPI_ROCKY_RES_4K2Kp_60Hz:
            return  E_MAPI_HDMITX_TIMING_4K2K_60P;
            break;
        case E_MAPI_ROCKY_RES_4K2Kp_30Hz:
            return  E_MAPI_HDMITX_TIMING_4K2K_30P;
            break;
        case E_MAPI_ROCKY_RES_4K2Kp_25Hz:
            return  E_MAPI_HDMITX_TIMING_4K2K_25P;
            break;
        case E_MAPI_ROCKY_RES_1280x1470p_50Hz:
            return  E_MAPI_HDMITX_TIMING_1470_50P;
            break;
        case E_MAPI_ROCKY_RES_1280x1470p_60Hz:
            return  E_MAPI_HDMITX_TIMING_1470_60P;
            break;
        case E_MAPI_ROCKY_RES_1280x1470p_24Hz:
            return  E_MAPI_HDMITX_TIMING_1470_24P;
            break;
        case E_MAPI_ROCKY_RES_1280x1470p_30Hz:
            return  E_MAPI_HDMITX_TIMING_1470_30P;
            break;
        case E_MAPI_ROCKY_RES_1920x2205p_24Hz:
            return  E_MAPI_HDMITX_TIMING_2160_24P;
            break;
        case E_MAPI_ROCKY_RES_1920x2205p_30Hz:
            return  E_MAPI_HDMITX_TIMING_2160_30P;
            break;
        default:
            UBOOT_ERROR("Wrong Mode of EN_MAPI_DEVICE_ROCKY_VIDEO_TIMING \n");
            return E_MAPI_HDMITX_TIMING_1080_60P;
    }
    UBOOT_TRACE("OK\n");
}

MS_BOOL SetColorType(EN_MAPI_HDMITX_COLOR_TYPE enInputColor, EN_MAPI_HDMITX_COLOR_TYPE enOutputColor)
{
    UBOOT_TRACE("OK\n");

    EN_MAPI_RAPTORS_HDMITX_OUTPUT_COLOR_TYPE enOutColor;
    EN_MAPI_RAPTORS_HDMITX_INPUT_COLOR_TYPE enInColor;

    switch(enOutputColor)
    {
        case E_MAPI_HDMITX_COLOR_RGB444:
            enOutColor = E_MAPI_RAPTORS_HDMITX_COLORS_OUTPUT_RGB_444;
            break;
        case E_MAPI_HDMITX_COLOR_YUV422:
            enOutColor = E_MAPI_RAPTORS_HDMITX_COLORS_OUTPUT_YUV_422;
            break;
        case E_MAPI_HDMITX_COLOR_YUV444:
            enOutColor = E_MAPI_RAPTORS_HDMITX_COLORS_OUTPUT_YUV_444;
            break;
#if 0// will add when YUV420 is added in mapi_hdmitx_datatype api enum
        case E_MAPI_HDMITX_COLOR_YUV420:
            enOutColor = E_MAPI_RAPTORS_HDMITX_COLORS_OUTPUT_YUV_420;
            break;
#endif
        default:
            UBOOT_DEBUG("Wrong Mode of EN_MAPI_HDMITX_COLOR_TYPE for output color\n");
            return FALSE;
    }

    switch(enInputColor)
    {
        case E_MAPI_HDMITX_COLOR_RGB444:
            enInColor = E_MAPI_RAPTORS_HDMITX_COLORS_INPUT_RGB;
            break;
        case E_MAPI_HDMITX_COLOR_YUV422:
            enInColor = E_MAPI_RAPTORS_HDMITX_COLORS_INPUT_YUV;
            break;
        case E_MAPI_HDMITX_COLOR_YUV444:
            enInColor = E_MAPI_RAPTORS_HDMITX_COLORS_INPUT_YUV;
            break;
        default:
            UBOOT_DEBUG("Wrong Mode of EN_MAPI_HDMITX_COLOR_TYPE for input color\n");
            return FALSE;
    }

    if(enOutColor == genHDMITx_OutColor &&
       enInColor  == genHDMITx_InColor)
    {
        UBOOT_DEBUG("ColorSpace don't change. IN%s, OUT:%s\n", PARSING_IN_COLOR(enInColor), PARSING_OUT_COLOR(enOutColor));
        return TRUE;
    }

    printf("IN:%s, OUT:%s \n", PARSING_IN_COLOR(enInColor), PARSING_OUT_COLOR(enOutColor));


    //set AVmute
    SetOutputBlack(1);
    mdelay(100);
    //

    //set colortype
    ST_MAILBOX_COMMAND_CONFIQ_VIDEO setTiming;
    setTiming.command_idx=E_MAPI_MAILBOX_CONFIQ_VIDEO;
    setTiming.data_len=0x02;
    setTiming.color_present=TRUE;
    setTiming.in_color=enInColor;
    setTiming.out_color=enOutColor;
    MS_BOOL bColor;
    bColor=Mailbox_WriteCommand_Config_Video(&setTiming);
    printf("[Device Raptors] Mailbox_Write Config Video = %x\n",bColor);
    Fn_Send_Interrupt_Raptors();
    //

    //read Raptors ACK
    if(!Fn_WaitACK())
        {
        UBOOT_ERROR("DEVICE_HDMITX::%s, %d , Setting Wrong!!! Read ACK Fail!!!\n",__func__,__LINE__);
        return FALSE;
        }

    Fn_SetACK_Low();//set ACK low after read it high
    //

    //disable AVmute
    mdelay(100);
    SetOutputBlack(0);
    //

    genHDMITx_InColor  = enInColor;
    genHDMITx_OutColor = enOutColor;
    UBOOT_TRACE("OK\n");

    return TRUE;
}


MS_BOOL SetColorDepthType(EN_MAPI_HDMITX_COLOR_DEPTH_TYPE enColorDepth)
{
    UBOOT_TRACE("IN\n");


    genHDMITx_ColorDepth = enColorDepth == E_MAPI_HDMITX_COLOR_DEPTH_8BITS ?  E_MAPI_RAPTORS_HDMITX_COLORS_8bits :
                           enColorDepth == E_MAPI_HDMITX_COLOR_DEPTH_10BITS ? E_MAPI_RAPTORS_HDMITX_COLORS_10bits :
                           enColorDepth == E_MAPI_HDMITX_COLOR_DEPTH_12BITS ? E_MAPI_RAPTORS_HDMITX_COLORS_12bits :
                                                                              E_MAPI_RAPTORS_HDMITX_COLORS_NOID;

    if(_bHDMITx_Running)
    {
        TurnOnOff(FALSE);

        SetTimingType(genMAPI_HDMITxTming);

        TurnOnOff(TRUE);
    }
    UBOOT_TRACE("OK\n");
    return TRUE;
}

MS_BOOL SetOutputMode(EN_MAPI_HDMITX_OUTPUT_MODE enOutputMode)
{
    UBOOT_TRACE("IN\n");

    switch(enOutputMode)
    {
        case E_MAPI_HDMITX_VIDEO_HDMI:
            genHDMITX_Mode = E_MAPI_RAPTORS_HDMITX_VIDEO_OUTPUT_HDMI;
            break;
        case E_MAPI_HDMITX_VIDEO_HDMI_HDCP:
            genHDMITX_Mode = E_MAPI_RAPTORS_HDMITX_VIDEO_OUTPUT_HDMI_HDCP;
            break;
        case E_MAPI_HDMITX_VIDEO_DVI:
            genHDMITX_Mode = E_MAPI_RAPTORS_HDMITX_VIDEO_OUTPUT_DVI;
            break;
        case E_MAPI_HDMITX_VIDEO_DVI_HDCP:
            genHDMITX_Mode = E_MAPI_RAPTORS_HDMITX_VIDEO_OUTPUT_DVI_HDCP;
            break;
        default:
            UBOOT_DEBUG("Wrong Mode of EN_MAPI_HDMITX_OUTPUT_MODE \n");
            return FALSE;
    }

    if(_bHDMITx_Running == FALSE)
    {
        //set setTiming flag
        ST_MAILBOX_COMMAND_TIMING_CHANGE_AVMUTE setFlag;
        setFlag.command_idx=E_MAPI_MAILBOX_AVMUTE_TIMING_RESET;
        setFlag.data_len=0x02;
        setFlag.timing_flag=TRUE;
        setFlag.enTiming=E_MAPI_RAPTORS_HDMITX_TIMING_CHANGE_ENABLE;
        MS_BOOL bFlag;
        bFlag=Mailbox_WriteCommand_TimingChange_AVmute(&setFlag);
        UBOOT_DEBUG("[Device Raptors] SetOutputMode Mailbox_Write TimingChange_AVmute = %x\n",bFlag);
        Fn_Send_Interrupt_Raptors();
        //

        //read Raptors ACK
        if(!Fn_WaitACK())
            {
            UBOOT_ERROR("DEVICE_HDMITX::%s, %d , Setting Wrong!!! Read ACK Fail!!!\n",__func__,__LINE__);
            return FALSE;
            }

        Fn_SetACK_Low();//set ACK low after read it high
        //

        //set timing at configure video
        ST_MAILBOX_COMMAND_CONFIQ_VIDEO setTiming;
        setTiming.command_idx=E_MAPI_MAILBOX_CONFIQ_VIDEO;
        setTiming.data_len=0x03;
        setTiming.timing_present=TRUE;
        setTiming.timing_idx=genHDMITx_VideoTiming;
        setTiming.color_depth=genHDMITx_ColorDepth;
        setTiming.output_mode=genHDMITX_Mode;
        MS_BOOL bIce;
        bIce=Mailbox_WriteCommand_Config_Video(&setTiming);
        UBOOT_DEBUG("[Device Raptors] Mailbox_Write Config Video = %x\n",bIce);
        Fn_Send_Interrupt_Raptors();
        //

        //read Raptors ACK
        if(!Fn_WaitACK())
            {
            UBOOT_ERROR("DEVICE_HDMITX::%s, %d , Setting Wrong!!! Read ACK Fail!!!\n",__func__,__LINE__);
            return FALSE;
            }

        Fn_SetACK_Low();//set ACK low after read it high
        //

        //set setTiming flag
        ST_MAILBOX_COMMAND_TIMING_CHANGE_AVMUTE setFlag2;
        setFlag2.command_idx=E_MAPI_MAILBOX_AVMUTE_TIMING_RESET;
        setFlag2.data_len=0x02;
        setFlag2.timing_flag=TRUE;
        setFlag2.enTiming=E_MAPI_RAPTORS_HDMITX_TIMING_CHANGE_DISABLE;
        MS_BOOL bFlag2;
        bFlag2=Mailbox_WriteCommand_TimingChange_AVmute(&setFlag2);
        UBOOT_DEBUG("[Device Raptors] Mailbox_Write TimingChange_AVmute = %x\n",bFlag2);
        Fn_Send_Interrupt_Raptors();
        //

        //read Raptors ACK
        if(!Fn_WaitACK())
            {
            UBOOT_ERROR("DEVICE_HDMITX::%s, %d , Setting Wrong!!! Read ACK Fail!!!\n",__func__,__LINE__);
            return FALSE;
            }

        Fn_SetACK_Low();//set ACK low after read it high
        //

    }
    else
    {
        UBOOT_ERROR("DEVICE_HDMITX::%s, %d , Setting Wrong!!! Rocket reset Fail!!!\n",__func__,__LINE__);
    }
    UBOOT_TRACE("OK\n");
    return TRUE;
}







MS_BOOL SetAudioConfiguration(EN_MAPI_HDMITX_AUDIO_FREQUENCY freq, EN_MAPI_HDMITX_AUDIO_CHANNEL_COUNT ch, EN_MAPI_HDMITX_AUDIO_CODING_TYPE type)
{
    UBOOT_TRACE("IN\n");

    MS_BOOL bRet=TRUE;

    switch(freq)
    {
    case E_MAPI_HDMITX_AUDIO_32K:
        genAudio_Freq = E_MAPI_RAPTORS_HDMITX_AUDIO_32KHz;
    break;

    case E_MAPI_HDMITX_AUDIO_44K:
        genAudio_Freq = E_MAPI_RAPTORS_HDMITX_AUDIO_44KHz;
    break;

    case E_MAPI_HDMITX_AUDIO_48K:
        genAudio_Freq = E_MAPI_RAPTORS_HDMITX_AUDIO_48KHz;
    break;

    case E_MAPI_HDMITX_AUDIO_88K:
        genAudio_Freq = E_MAPI_RAPTORS_HDMITX_AUDIO_88KHz;
    break;

    case E_MAPI_HDMITX_AUDIO_96K:
        genAudio_Freq = E_MAPI_RAPTORS_HDMITX_AUDIO_96KHz;
    break;

    case E_MAPI_HDMITX_AUDIO_176K:
        genAudio_Freq = E_MAPI_RAPTORS_HDMITX_AUDIO_176KHz;
    break;

    case E_MAPI_HDMITX_AUDIO_192K:
        genAudio_Freq = E_MAPI_RAPTORS_HDMITX_AUDIO_192KHz;
    break;
    default:
    bRet=FALSE;
    }


    switch(ch)
    {
    case E_MAPI_HDMITX_AUDIO_CH_2:
        genAudio_Ch_Cnt = E_MAPI_RAPTORS_HDMITX_AUDIO_CH_2;
    break;

    case E_MAPI_HDMITX_AUDIO_CH_8:
        genAudio_Ch_Cnt = E_MAPI_RAPTORS_HDMITX_AUDIO_CH_8;
    break;

    default:
    bRet=FALSE;
    }


    switch(type)
    {
    case E_MAPI_HDMITX_AUDIO_PCM:
        genAudio_CodeType = E_MAPI_RAPTORS_HDMITX_AUDIO_PCM;
    break;

    case E_MAPI_HDMITX_AUDIO_NONPCM:
        genAudio_CodeType = E_MAPI_RAPTORS_HDMITX_AUDIO_NONPCM;
    break;

    default:
    bRet=FALSE;
    }

    if(bRet)
    {
        ST_MAILBOX_COMMAND_CONFIQ_AUDIO setAudio;
        setAudio.command_idx=E_MAPI_MAILBOX_CONFIQ_AUDIO;
        setAudio.data_len=0x03;
        setAudio.inform_present=TRUE;
        setAudio.frequency=genAudio_Freq;
        setAudio.channel_num=genAudio_Ch_Cnt;
        setAudio.code_type=genAudio_CodeType;

        MS_BOOL bIce;
        bIce=Mailbox_WriteCommand_Config_Audio(&setAudio);
        printf("[Device Raptors] Mailbox_Write Config Audio = %x\n",bIce);
        Fn_Send_Interrupt_Raptors();
        //

        //read Raptors ACK
        if(!Fn_WaitACK())
            {
            UBOOT_ERROR("DEVICE_HDMITX::%s, %d , Setting Wrong!!! Read ACK Fail!!!\n",__func__,__LINE__);
            return FALSE;
            }

        Fn_SetACK_Low();//set ACK low after read it high
        //
    }
    UBOOT_TRACE("OK\n");

    return bRet;

}

MS_BOOL SetAudioSourceFormat(EN_MAPI_HDMITX_AUDIO_SOURCE_FORMAT fmt)
{
    UBOOT_TRACE("IN\n");
    MS_BOOL bRet=TRUE;

    switch(fmt)
    {
    case E_MAPI_HDMITX_AUDIO_FORMAT_PCM:
        genAudio_SrcFmt = E_MAPI_RAPTORS_HDMITX_AUDIO_FORMAT_PCM;
    break;

    case E_MAPI_HDMITX_AUDIO_FORMAT_DSD:
        genAudio_SrcFmt = E_MAPI_RAPTORS_HDMITX_AUDIO_FORMAT_DSD;
    break;

    case E_MAPI_HDMITX_AUDIO_FORMAT_HBR:
        genAudio_SrcFmt = E_MAPI_RAPTORS_HDMITX_AUDIO_FORMAT_HBR;
    break;

    default:
     bRet=FALSE;
    }

    if(bRet)
    {
        ST_MAILBOX_COMMAND_CONFIQ_AUDIO setAudio;
        setAudio.command_idx=E_MAPI_MAILBOX_CONFIQ_AUDIO;
        setAudio.data_len=0x02;
        setAudio.fmt_present=TRUE;
        setAudio.source_fmt=genAudio_SrcFmt;

        MS_BOOL bIce;
        bIce=Mailbox_WriteCommand_Config_Audio(&setAudio);
        printf("[Device Raptors] Mailbox_Write Config Audio = %x\n",bIce);
        Fn_Send_Interrupt_Raptors();
        //

        //read Raptors ACK
        if(!Fn_WaitACK())
            {
            UBOOT_ERROR("DEVICE_HDMITX::%s, %d , Setting Wrong!!! Read ACK Fail!!!\n",__func__,__LINE__);
            return FALSE;
            }

        Fn_SetACK_Low();//set ACK low after read it high
        //
    }
    UBOOT_TRACE("OK\n");

    return bRet;
}


MS_BOOL Set_Audio_SourceType(EN_MAPI_HDMITX_AUDIO_TYPE enAudioType)
{
    UBOOT_TRACE("IN\n");
    MS_BOOL bRet = TRUE;

    switch(enAudioType)
    {
    case E_MAPI_HDMITX_AUDIO_SPDIF:
        genAudio_SrcType = E_MAPI_RAPTORS_HDMITX_AUDIO_SPDIF;
    break;

    case E_MAPI_HDMITX_AUDIO_I2S:
        genAudio_SrcType = E_MAPI_RAPTORS_HDMITX_AUDIO_I2S;
    break;

    default:
    bRet=FALSE;
    }

    if(bRet)
    {
        ST_MAILBOX_COMMAND_CONFIQ_AUDIO setAudio;
        setAudio.command_idx=E_MAPI_MAILBOX_CONFIQ_AUDIO;
        setAudio.data_len=0x02;
        setAudio.source_present=TRUE;
        setAudio.source_type=genAudio_SrcType;

        MS_BOOL bIce;
        bIce=Mailbox_WriteCommand_Config_Audio(&setAudio);
        printf("[Device Raptors] Mailbox_Write Config Audio = %x\n",bIce);
        Fn_Send_Interrupt_Raptors();
        //

        //read Raptors ACK
        if(!Fn_WaitACK())
            {
            UBOOT_ERROR("DEVICE_HDMITX::%s, %d , Setting Wrong!!! Read ACK Fail!!!\n",__func__,__LINE__);
            return FALSE;
            }

        Fn_SetACK_Low();//set ACK low after read it high
        //
    }

    UBOOT_TRACE("OK\n");

    return bRet;

}

MS_BOOL GetRxStatus(void)
{
    MS_BOOL bRet;
    bRet=(Raptors_r2byte(0x1252) & 0x0001);
    if(bRet)
        return TRUE;
    else
        return FALSE;
}




MS_BOOL msHdmitx_Rocket_Init(EN_MAPI_DEVICE_ROCKY_VIDEO_TIMING outPutType )
{
    UBOOT_TRACE("IN\n");
    MDrv_Rocker2_SWI2C_Init();
    Init();
    TurnOnOff(FALSE);
    SetTimingType(outPutType);

    TurnOnOff(TRUE);
    UBOOT_TRACE("OK\n");
    return TRUE;

}
