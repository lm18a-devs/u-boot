#include "common.h"
#include "smp.h"

unsigned int Interrupt_enabled(void)
{
    unsigned int val;
    asm("mrs %0, cpsr" : "=r" (val) : : "cc");
    val = (val & 0x80) >> 7;

    return val?0:1;
}
void Interrupt_enable(void)
{
    unsigned int val;
    asm("mrs %0, cpsr" : "=r" (val): : "cc");
    val = val & ~0x80;
    asm("msr cpsr, %0" ::"r" (val) : "cc");
}
void Interrupt_disable(void)
{
    unsigned int val;
    asm("mrs %0, cpsr" : "=r" (val) : : "cc");
    val = val | 0x80;
    asm("msr cpsr, %0" :: "r" (val) : "cc");
}
unsigned int Interrupt_save(void)
{
    unsigned int val;
    asm("mrs %0, cpsr" : "=r" (val) : : "cc");
    return val;
}
void Interrupt_restore(unsigned int cpsr)
{
    unsigned int val=cpsr;
    asm("msr cpsr, %0" :: "r" (val) : "cc");
}

void Interrupt_active(InterruptNum irq_num)
{
    if((irq_num>=E_INT_IPI_0x130_START) && (irq_num<=E_INT_IPI_0x130_END))
        mhal_ipi_unmask(irq_num-E_INT_IPI_0x130_START);
    else
        MsOS_EnableInterrupt(irq_num);
}

void Interrupt_deactive(InterruptNum irq_num)
{
    if((irq_num>=E_INT_IPI_0x130_START) && (irq_num<=E_INT_IPI_0x130_END))
        mhal_ipi_mask(irq_num-E_INT_IPI_0x130_START);
    else
        MsOS_DisableInterrupt(irq_num);
}

void Interrupt_request(InterruptNum irq_num, void* func, void * func_arg)
{
    if((irq_num>=E_INT_IPI_0x130_START) && (irq_num<=E_INT_IPI_0x130_END)) 
        mhal_ipi_attach( irq_num-E_INT_IPI_0x130_START, func, 0);
    else
        MsOS_AttachInterrupt(irq_num, func);
}

void Interrupt_free(InterruptNum irq_num)
{
    if((irq_num>=E_INT_IPI_0x130_START) && (irq_num<=E_INT_IPI_0x130_END))
        mhal_ipi_detach(irq_num-E_INT_IPI_0x130_START);
    else
        MsOS_DetachInterrupt(irq_num);
}

void ipi_send(unsigned int cpu_mask, InterruptNum ipi_num)
{
    if((ipi_num>=E_INT_IPI_0x130_START) && (ipi_num<=E_INT_IPI_0x130_END))
        mhal_ipi_send(cpu_mask, ipi_num-E_INT_IPI_0x130_START);
    else
        printf("Wrong ipi number: %d please refer to MsIRQ.h\n", ipi_num);
}

void ipi_broadcast(InterruptNum ipi_num)
{
    if((ipi_num>=E_INT_IPI_0x130_START) && (ipi_num<=E_INT_IPI_0x130_END))
        mhal_ipi_broadcast(ipi_num-E_INT_IPI_0x130_START); 
    else
        printf("Wrong ipi number: %d please refer to MsIRQ.h\n", ipi_num);
}

void ipi_send_target(unsigned int cpu_id, InterruptNum ipi_num)
{ 
    if((ipi_num>=E_INT_IPI_0x130_START) && (ipi_num<=E_INT_IPI_0x130_END))
        mhal_ipi_send_target(cpu_id, ipi_num-E_INT_IPI_0x130_START);
    else
        printf("Wrong ipi number: %d please refer to MsIRQ.h\n", ipi_num);
}

void ipi_send_self(InterruptNum ipi_num)
{
    if((ipi_num>=E_INT_IPI_0x130_START) && (ipi_num<=E_INT_IPI_0x130_END))
        mhal_ipi_send_self(ipi_num-E_INT_IPI_0x130_START);
    else
        printf("Wrong ipi number: %d please refer to MsIRQ.h\n", ipi_num);
}
