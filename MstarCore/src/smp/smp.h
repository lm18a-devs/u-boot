#include <MsIRQ.h>
#include <MsOS.h>

/* Struct define */
typedef void (*mhal_ipi_t)(unsigned int);

/* Extern function area */
extern void mhal_ipi_attach(unsigned int ipi_num, mhal_ipi_t isr, unsigned int dat);
extern void mhal_ipi_detach(unsigned int ipi_num);
extern void mhal_ipi_mask(unsigned int ipi_num);
extern void mhal_ipi_unmask(unsigned int ipi_num);
extern void mhal_ipi_send(unsigned int cpu_mask, unsigned int ipi_num);
extern void mhal_ipi_broadcast(unsigned int ipi_num);
extern void mhal_ipi_send_target(unsigned int cpu_id, unsigned int ipi_num);
extern void mhal_ipi_send_self(unsigned int ipi_num);

/* Interrupt api */
unsigned int Interrupt_enabled(void);
void Interrupt_enable(void);
void Interrupt_disable(void);
unsigned int Interrupt_save(void);
void Interrupt_restore(unsigned int cpsr);
void Interrupt_active(InterruptNum irq_num);
void Interrupt_deactive(InterruptNum irq_num);
void Interrupt_request(InterruptNum irq_num, void* func, void * func_arg);
void Interrupt_free(InterruptNum irq_num);

/* IPI functions */
void ipi_send(unsigned int cpu_mask, InterruptNum ipi_num);
void ipi_broadcast(InterruptNum ipi_num);
void ipi_send_target(unsigned int cpu_id, InterruptNum ipi_num);
void ipi_send_self(InterruptNum ipi_num);
