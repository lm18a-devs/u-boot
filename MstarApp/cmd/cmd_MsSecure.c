//<MStar Software>
//******************************************************************************
// MStar Software
// Copyright (c) 2010 - 2012 MStar Semiconductor, Inc. All rights reserved.
// All software, firmware and related documentation herein ("MStar Software") are
// intellectual property of MStar Semiconductor, Inc. ("MStar") and protected by
// law, including, but not limited to, copyright law and international treaties.
// Any use, modification, reproduction, retransmission, or republication of all 
// or part of MStar Software is expressly prohibited, unless prior written 
// permission has been granted by MStar. 
//
// By accessing, browsing and/or using MStar Software, you acknowledge that you
// have read, understood, and agree, to be bound by below terms ("Terms") and to
// comply with all applicable laws and regulations:
//
// 1. MStar shall retain any and all right, ownership and interest to MStar
//    Software and any modification/derivatives thereof.
//    No right, ownership, or interest to MStar Software and any
//    modification/derivatives thereof is transferred to you under Terms.
//
// 2. You understand that MStar Software might include, incorporate or be
//    supplied together with third party`s software and the use of MStar
//    Software may require additional licenses from third parties.  
//    Therefore, you hereby agree it is your sole responsibility to separately
//    obtain any and all third party right and license necessary for your use of
//    such third party`s software. 
//
// 3. MStar Software and any modification/derivatives thereof shall be deemed as
//    MStar`s confidential information and you agree to keep MStar`s 
//    confidential information in strictest confidence and not disclose to any
//    third party.  
//
// 4. MStar Software is provided on an "AS IS" basis without warranties of any
//    kind. Any warranties are hereby expressly disclaimed by MStar, including
//    without limitation, any warranties of merchantability, non-infringement of
//    intellectual property rights, fitness for a particular purpose, error free
//    and in conformity with any international standard.  You agree to waive any
//    claim against MStar for any loss, damage, cost or expense that you may
//    incur related to your use of MStar Software.
//    In no event shall MStar be liable for any direct, indirect, incidental or
//    consequential damages, including without limitation, lost of profit or
//    revenues, lost or damage of data, and unauthorized system use.
//    You agree that this Section 4 shall still apply without being affected
//    even if MStar Software has been modified by MStar in accordance with your
//    request or instruction for your use, except otherwise agreed by both
//    parties in writing.
//
// 5. If requested, MStar may from time to time provide technical supports or
//    services in relation with MStar Software to you for your use of
//    MStar Software in conjunction with your or your customer`s product
//    ("Services").
//    You understand and agree that, except otherwise agreed by both parties in
//    writing, Services are provided on an "AS IS" basis and the warranty
//    disclaimer set forth in Section 4 above shall apply.  
//
// 6. Nothing contained herein shall be construed as by implication, estoppels
//    or otherwise:
//    (a) conferring any license or right to use MStar name, trademark, service
//        mark, symbol or any other identification;
//    (b) obligating MStar or any of its affiliates to furnish any person,
//        including without limitation, you and your customers, any assistance
//        of any kind whatsoever, or any information; or 
//    (c) conferring any license or right under any intellectual property right.
//
// 7. These terms shall be governed by and construed in accordance with the laws
//    of Taiwan, R.O.C., excluding its conflict of law rules.
//    Any and all dispute arising out hereof or related hereto shall be finally
//    settled by arbitration referred to the Chinese Arbitration Association,
//    Taipei in accordance with the ROC Arbitration Law and the Arbitration
//    Rules of the Association by three (3) arbitrators appointed in accordance
//    with the said Rules.
//    The place of arbitration shall be in Taipei, Taiwan and the language shall
//    be English.  
//    The arbitration award shall be final and binding to both parties.
//
//******************************************************************************
//<MStar Software>
///////////////////////////////////////////////////////////////////////////////////////////////////
///
/// file    cmd_secure.c
/// @brief  SCS Main Function
/// @author MStar Semiconductor Inc.
///////////////////////////////////////////////////////////////////////////////////////////////////
#include <common.h>
#include <command.h>
#include <secure/MsSecureBoot.h>
#include <secure/MsSecureUpgrade.h>
#include <secure/MsSignature.h>


U_BOOT_CMD(
    store_secure_info, 3, 0, do_save_secure_info,
    "store secure info\n",
    "store_secure_info targetName(hex) imageDramAddr(hex)\n"
);

#ifdef CONFIG_MSTAR_STR_MINISIZE
U_BOOT_CMD_STR(
#else
U_BOOT_CMD(
#endif
    SecureInit, 7, 0, do_Secure_Init,
    "Init security booting.\n",
    "SecureInit\n"
);

U_BOOT_CMD(
    verify_sha256, 5, 0,do_verify_sha256,
    "This command is only for verification \n",
    "verify_sha256 intAddr(hex) outAddr(hex) intLen(hex) count(hex)\n"
);

U_BOOT_CMD(
    sha256, 4, 0,do_sha256,
    "The sha256 cacluate \n",
    "sha256 intAddr(hex) outAddr(hex) intLen(hex)\n"
);

U_BOOT_CMD(
    rsa, 4, 0,do_rsa,
    "rsa cacluate \n",
    "rsa signAddr(hex) keyAddr(hex) outAddr(hex)\n"
);

U_BOOT_CMD(
    stest, 3, 1,    do_performance_test,
    "test IO performance on continue mode or interleave mode]\n",
    "-c [partition]\n"
    "-i [partition]\n"
);


U_BOOT_CMD(authenticateAN, 2, 0, do_AN_Authenticate,
   "authenticate android's boot and recover image",
    "[dram addr]\n"
);

U_BOOT_CMD(authenticate_LG_KL, 2, 0, do_Authenticate_LG_KL,
   "authenticate the LG's kernel image",
    "[buffer address]\n"
);

U_BOOT_CMD(authenticate, 3, 0, do_Authenticate,
   "authenticate the kernel image",
    "[image name]\n"
    #if !(ENABLE_MODULE_ANDROID_BOOT == 1)
    "image name]:KL RFS,MSLIB,APP\n"
    #else
    "image name:boot recovery\n"
    #endif
);

U_BOOT_CMD(dumpSignature, 2, 0, do_DumpSignature,
    "dump the signature",
    "[target file]\n"
    #if !(ENABLE_MODULE_ANDROID_BOOT == 1)
    "target file:KL,RFS,MSLIB,APP\n"
    #else
    "target file:boot recovery\n"
    #endif
);

#if defined(CONFIG_BOOTING_FROM_EXT_SPI_WITH_PM51)
U_BOOT_CMD(se_ut1, 1, 0, do_DoSecureEngUnitTestCase1,
    "The test case 1 for secure engine \n",
    "se_ut1"
);
U_BOOT_CMD(se_ut2, 1, 0, do_DoSecureEngUnitTestCase2,
    "The test case 2 for secure engine \n",
    "se_ut2"
);
U_BOOT_CMD(se_ut3, 1, 0, do_DoSecureEngUnitTestCase3,
    "The test case 3 for secure engine \n",
    "se_ut3"
);

U_BOOT_CMD(se_ut4, 1, 0, do_DoSecureEngUnitTestCase4,
    "The test case 4 for secure engine \n",
    "se_ut4"
);

U_BOOT_CMD(se_ut5, 1, 0, do_DoSecureEngUnitTestCase5,
    "The test case 5 for secure engine \n",
    "se_ut5"
);
#endif

#if (ENABLE_MODULE_TEE==1)

U_BOOT_CMD(SecureBootCmd, 1, 0, do_SecureBootCmd,
    "SecureBootCmd \n",
    "fixed dram address for Kernel"
);
#endif

#if defined(CONFIG_SECURE_USB_UPGRADE)
U_BOOT_CMD(filepartloadSegAES, 7, 0, do_file_part_load_with_segment_aes_decrypted,
    "load a part of file to RAM and do descrypted",
    "filepartloadSegAES interface(hex) device(hex) addr(hex) filename(hex) offset(hex) bytes(hex)\n"
);

U_BOOT_CMD(fileSegRSA, 4, 0, do_file_segment_rsa_authendication,
    "do file segment rsa authendication",
    "fileSegRSA interface(hex) device(hex) filename(hex) \n"
);
#endif

#if defined (CONFIG_Securestorage)
U_BOOT_CMD(
	write_rsa_key, 4, 1, do_write_rsa_key,
	"write rsa key to boot2",
	"write_rsa_key [addr] [blk] [size]"
);
#endif

U_BOOT_CMD(
	sb_verify, 4, 1, do_sb_verify,
	"LG app verify",
	"sb_verify [offset] [image_size] [flag]"
);

U_BOOT_CMD(
	verify, 3, 1, do_sb_verify_union,
	"LG app verify union",
	"verify [offset] [image_size]"
);


