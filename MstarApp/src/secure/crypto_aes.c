//<MStar Software>
//***********************************************************************************
//MStar Software
//Copyright (c) 2010 - 2012 MStar Semiconductor, Inc. All rights reserved.
//All software, firmware and related documentation herein (��MStar Software��) are intellectual property of MStar Semiconductor, Inc. (��MStar��) and protected by law, including, but not limited to, copyright law and international treaties.  Any use, modification, reproduction, retransmission, or republication of all or part of MStar Software is expressly prohibited, unless prior written permission has been granted by MStar.
//By accessing, browsing and/or using MStar Software, you acknowledge that you have read, understood, and agree, to be bound by below terms (��Terms��) and to comply with all applicable laws and regulations:
//
//1. MStar shall retain any and all right, ownership and interest to MStar Software and any modification/derivatives thereof.  No right, ownership, or interest to MStar Software and any modification/derivatives thereof is transferred to you under Terms.
//2. You understand that MStar Software might include, incorporate or be supplied together with third party��s software and the use of MStar Software may require additional licenses from third parties.  Therefore, you hereby agree it is your sole responsibility to separately obtain any and all third party right and license necessary for your use of such third party��s software.
//3. MStar Software and any modification/derivatives thereof shall be deemed as MStar��s confidential information and you agree to keep MStar��s confidential information in strictest confidence and not disclose to any third party.
//4. MStar Software is provided on an ��AS IS�� basis without warranties of any kind. Any warranties are hereby expressly disclaimed by MStar, including without limitation, any warranties of merchantability, non-infringement of intellectual property rights, fitness for a particular purpose, error free and in conformity with any international standard.  You agree to waive any claim against MStar for any loss, damage, cost or expense that you may incur related to your use of MStar Software.
//   In no event shall MStar be liable for any direct, indirect, incidental or consequential damages, including without limitation, lost of profit or revenues, lost or damage of data, and unauthorized system use.  You agree that this Section 4 shall still apply without being affected even if MStar Software has been modified by MStar in accordance with your request or instruction for your use, except otherwise agreed by both parties in writing.
//5. If requested, MStar may from time to time provide technical supports or services in relation with MStar Software to you for your use of MStar Software in conjunction with your or your customer��s product (��Services��).  You understand and agree that, except otherwise agreed by both parties in writing, Services are provided on an ��AS IS�� basis and the warranty disclaimer set forth in Section 4 above shall apply.
//6. Nothing contained herein shall be construed as by implication, estoppels or otherwise: (a) conferring any license or right to use MStar name, trademark, service mark, symbol or any other identification; (b) obligating MStar or any of its affiliates to furnish any person, including without limitation, you and your customers, any assistance of any kind whatsoever, or any information; or (c) conferring any license or right under any intellectual property right.
//7. These terms shall be governed by and construed in accordance with the laws of Taiwan, R.O.C., excluding its conflict of law rules.  Any and all dispute arising out hereof or related hereto shall be finally settled by arbitration referred to the Chinese Arbitration Association, Taipei in accordance with the ROC Arbitration Law and the Arbitration Rules of the Association by three (3) arbitrators appointed in accordance with the said Rules.  The place of arbitration shall be in Taipei, Taiwan and the language shall be English.
//   The arbitration award shall be final and binding to both parties.
//***********************************************************************************
//<MStar Software>
#define CRYPTO_AES_C

#include <secure/crypto_aes.h>
#include <secure/MsSecureCommon.h>
#include <common.h>
#include <MsSystem.h>
#include <MsDebug.h>
#include <drvAESDMA.h>
#include <drvSYS.h>
#if defined(CONFIG_FOXCONN_ENABLE)
#include <MsSysUtility.h>
#endif
#if defined(CONFIG_MSTAR_CLEANBOOT)
#include <secure/apiSecureBoot.h>
#endif
static U8 hwKey[0x10]={0xE0, 0x10, 0x01, 0xFF, 0x0F, 0xAA, 0x55, 0xFC, \
                       0x92, 0x4D, 0x53, 0x54, 0x41, 0xFF, 0x07, 0x00}; // temporary

#define BIT(bits)                   (1<<bits)
typedef struct
{
    MS_U32 pChiperBuf;
    MS_U32 pPlainBuf;
    MS_U32 *pKey;
    MS_U32 *pIV;
    MS_U32 u32Len;
    MS_BOOL bIsDecrypt;
    DrvAESDMA_CipherMode eMode;
}AES_ParamStruct;

static int CommonAES128(AES_ParamStruct *AESPara)
{
    DRVAESDMA_RESULT result = DRVAESDMA_OK;
    MS_U32 u32AESInBuf, u32AESOutBuf;
    UBOOT_TRACE("IN\n");
    if((AESPara->pChiperBuf == 0)||(AESPara->pPlainBuf == 0))
    {
        UBOOT_ERROR("The input parameters are not correct\n");
        return DRVAESDMA_INVALID_PARAM;
    }

    flush_cache(AESPara->pChiperBuf, AESPara->u32Len);

    u32AESInBuf=VA2PA(AESPara->pChiperBuf);
    u32AESOutBuf=VA2PA(AESPara->pPlainBuf);
    MDrv_AESDMA_Init(0x00, 0x40000000, 2); // wait the AESDMA.a
    MDrv_AESDMA_Reset();
    MDrv_AESDMA_SelEng(AESPara->eMode, AESPara->bIsDecrypt);
    result=MDrv_AESDMA_SetFileInOut(u32AESInBuf, AESPara->u32Len, u32AESOutBuf, u32AESOutBuf+AESPara->u32Len-1);
    if (result == DRVAESDMA_MIU_ADDR_ERROR)
    {
        UBOOT_ERROR("CommonAES128 execte MDrv_AESDMA_SetFileInOut fail\n");
        return -1;
    }

    if(AESPara->eMode == E_DRVAESDMA_CIPHER_CBC)
    {
        MDrv_AESDMA_SetIV(AESPara->pIV);
    }
    char key[AES_KEY_LEN] = "\0";
    if(AESPara->pKey!=NULL)
    {
        memcpy(key,AESPara->pKey,AES_KEY_LEN);
        MDrv_AESDMA_SetKey((MS_U32*)key);
        //MDrv_AESDMA_SetKey(AESPara->pKey);
    }
    else
    {
#if defined(CONFIG_FOXCONN_ENABLE)

        MS_U16 dev_id[8]={0};

        dev_id[0]=Read2Byte(0x3800);
        dev_id[1]=Read2Byte(0x3802);
        dev_id[2]=Read2Byte(0x3804);
        //efuse key is disable, use device id as key

        UBOOT_DEBUG("device id key\n");
        UBOOT_DUMP("sizeof(dev_id)=%d\n",sizeof(dev_id));
        MDrv_AESDMA_SetKey((MS_U32*)dev_id);
#else
        //key is NULL ,we use Efuse key
        UBOOT_DEBUG("Use EFUSE Key\n");
        MDrv_AESDMA_SetSecureKey();
#endif

    }

    UBOOT_DEBUG("AESDMA Start\n");
    MDrv_AESDMA_Start(TRUE);
    UBOOT_DEBUG("AESDMA polling finish bits\n");

    while(MDrv_AESDMA_IsFinished() != DRVAESDMA_OK);

    flush_cache(AESPara->pPlainBuf, AESPara->u32Len);
    UBOOT_TRACE("OK\n");
    return 0;
}
int Secure_AES_ECB_Decrypt_HW(MS_U32 u32Addr, MS_U32 u32Len, MS_U8 *bKey)
{
    int ret=0;
    UBOOT_TRACE("IN\n");
    //check u32Len align 16
    if(0 != u32Len%16)
    {
         UBOOT_ERROR("u32InLen should align 16\n");
         return -1;
    }
	AES_ParamStruct AESParam;
    AESParam.eMode = E_DRVAESDMA_CIPHER_ECB;
    AESParam.pChiperBuf = u32Addr;
    AESParam.pPlainBuf = u32Addr;
    AESParam.pKey = (MS_U32 *)bKey;
    AESParam.u32Len = u32Len;
    AESParam.pIV= (MS_U32 *)NULL;
    AESParam.bIsDecrypt = TRUE;
    ret=CommonAES128(&AESParam);
    if(ret==0)
    {
        UBOOT_TRACE("OK\n");
    }
    else
    {
        UBOOT_ERROR("Secure_AES_Decrypt fail\n");
    }

    return ret;
}
int Secure_AES_ECB_Encrypt_HW(MS_U32 u32Addr, MS_U32 u32Len, MS_U8 *bKey)
{
    int ret=0;
    UBOOT_TRACE("IN\n");
    //check u32Len align 16
    if(0 != u32Len%16)
    {
         UBOOT_ERROR("u32InLen should align 16\n");
         return -1;
    }
	AES_ParamStruct AESParam;
    AESParam.eMode = E_DRVAESDMA_CIPHER_ECB;
    AESParam.pChiperBuf = u32Addr;
    AESParam.pPlainBuf = u32Addr;
    AESParam.pKey = (MS_U32 *)bKey;
    AESParam.u32Len = u32Len;
    AESParam.pIV= (MS_U32 *)NULL;
    AESParam.bIsDecrypt = FALSE;
    ret=CommonAES128(&AESParam);
    if(ret==0)
    {
        UBOOT_TRACE("OK\n");
    }
    else
    {
        UBOOT_ERROR("Secure_AES_Encrypt Error\n");
    }

    return ret;
}

int Secure_AES_ECB_Decrypt(MS_U32 u32Addr, MS_U32 u32Len, MS_U8 *bKey)
{
    int ret=0;
    UBOOT_TRACE("IN\n");
    //AESDMA
    ret = Secure_AES_ECB_Decrypt_HW(u32Addr,u32Len,bKey);
    if(ret==0)
    {
        UBOOT_TRACE("OK\n");
    }
    else
    {
        UBOOT_ERROR("AES Decrypt Error\n");
    }

    return ret;
}

int Secure_AES_ECB_Encrypt(MS_U32 u32Addr, MS_U32 u32Len, MS_U8 *bKey)
{
    int ret=0;
    UBOOT_TRACE("IN\n");
    //AESDMA
    ret = Secure_AES_ECB_Encrypt_HW(u32Addr,u32Len,bKey);
    if(ret==0)
    {
        UBOOT_TRACE("OK\n");
    }
    else
    {
        UBOOT_ERROR("AES Encrypt fail\n");
    }

    return ret;
}
void Secure_Get_DeviceKey(U8 *pu8HwKey)
{
    //use efuse to encrypt puCommonTaString
    const static U8 puCommonTaString[AES_KEY_LEN] = {'M', 'S', 't', 'a', 'r', 'C', 'o', 'm', 'm', 'o', 'n', 'T', 'A', 'K', 'e', 'y'};

    UBOOT_TRACE("IN\n");

    if(MDrv_SYS_Query(E_SYS_QUERY_SECURED_IC_SUPPORTED)==TRUE)
    {
        UBOOT_DEBUG("use Device key!!\n");
        memcpy(pu8HwKey,puCommonTaString,AES_KEY_LEN);
        Secure_AES_ECB_Encrypt((U32)pu8HwKey,AES_KEY_LEN,NULL);
        array_reverse((char*)pu8HwKey,AES_KEY_LEN);
    }
    else
    {
         UBOOT_DEBUG("use SW key!!\n");
         memcpy(pu8HwKey,hwKey,AES_KEY_LEN);
    }

    UBOOT_TRACE("OK\n");
}


#undef CRYPTO_AES_C

