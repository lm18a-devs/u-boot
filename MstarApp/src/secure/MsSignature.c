/*
 * FIPS 180-2 SHA-224/256/384/512 implementation
 * Last update: 02/02/2007
 * Issue date:  04/30/2005
 *
 * Copyright (C) 2005, 2007 Olivier Gay <olivier.gay@a3.epfl.ch>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the project nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE PROJECT AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE PROJECT OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */


///////////////////////////////////////////////////////////////////////////////////////////////////
///
/// file    cmd_secure.c
/// @brief  SCS Main Function
/// @author MStar Semiconductor Inc.
///////////////////////////////////////////////////////////////////////////////////////////////////


//-------------------------------------------------------------------------------------------------
//  Include Files
//-------------------------------------------------------------------------------------------------
#include <common.h>
#include <secure/MsSecureCommon.h>
#include <command.h>
#include <MsTypes.h>
#include <ShareType.h>
#include <MsDebug.h>
#include <exports.h>
#include <MsEnvironment.h>
#include <MsSystem.h>
#include <MsRawIO.h>
#if defined (CONFIG_SECURITY_STORE_IN_SPI_FLASH)
#include <MsApiSpi.h>
#elif defined (CONFIG_SECURITY_STORE_IN_NAND_FLASH)
//wait for implement
#else 
#include <MsMmc.h>
#endif

#if defined(LG_CHG)
#include <partinfo.h>
#endif

//-------------------------------------------------------------------------------------------------
//  Debug
//-------------------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------------
//  Local Defines
//-------------------------------------------------------------------------------------------------
#define NUMBER_OF_SECURE_INFO 2 // 2 : One is for continue mode, and the other one is for interleave mode
#if defined (CONFIG_SECURITY_STORE_IN_SPI_FLASH)
#define FLASH_DEFAULT_TARGET  E_RAW_DATA_IN_SPI
#define FLASH_DEFAULT_PARTITION SPI_DEFAULT_PARTITION
#define FLASH_DEFAULT_VOLUME SPI_DEFAULT_VOLUME
#define SECTOR_SIZE   0x10000
#elif defined (CONFIG_SECURITY_STORE_IN_NAND_FLASH)
extern  int ubi_get_volume_size(char *, size_t* );
extern int ubi_get_leb_size(void);
#define FLASH_DEFAULT_TARGET  E_RAW_DATA_IN_NAND
#define FLASH_DEFAULT_PARTITION NAND_DEFAULT_PARTITION
#define FLASH_DEFAULT_VOLUME NAND_DEFAULT_VOLUME
#define SECTOR_SIZE   ubi_get_leb_size()
#else
#define FLASH_DEFAULT_TARGET  E_RAW_DATA_IN_MMC
#define FLASH_DEFAULT_PARTITION MMC_DEFAULT_PARTITION
#define FLASH_DEFAULT_VOLUME MMC_DEFAULT_VOLUME /*"MPOOL"*/
#define SECTOR_SIZE   0x200
#define SECURE_INFOR_BACK_OFFSET 0x6000
#endif
#define SAFE_FREE(ptr){if(NULL != ptr){free(ptr);ptr = NULL;}}
//-------------------------------------------------------------------------------------------------
//  Local Structurs
//-------------------------------------------------------------------------------------------------


//-------------------------------------------------------------------------------------------------
//  Global Variables
//-------------------------------------------------------------------------------------------------


//-------------------------------------------------------------------------------------------------
//  Local Variables
//-------------------------------------------------------------------------------------------------
int SignatureSave(SECURITY_INFO *pBufferAddr);
int SignatureLoad(SECURITY_INFO *pBufferAddr);


//-------------------------------------------------------------------------------------------------
//  extern function
//-------------------------------------------------------------------------------------------------
extern int snprintf(char *str, size_t size, const char *fmt, ...);

//-------------------------------------------------------------------------------------------------
//  inline
//-------------------------------------------------------------------------------------------------


//-------------------------------------------------------------------------------------------------
//  Local function
//-------------------------------------------------------------------------------------------------


//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------
int get_signature_offset(unsigned int *u32SigOffset,unsigned int *u32SigBkOffset)
{
    int ret = 0;
    unsigned int u32Size = 0;
    UBOOT_TRACE("IN\n");

    ret = MsApiChunkHeader_GetValue(CH_SECURITY_INFO_AP_ROM_OFFSET,u32SigOffset);

    if(ret != 0)
    {
        UBOOT_ERROR("MsApiChunkHeader_GetValue fail!\n");
        return -1;
    }

#if defined (CONFIG_SECURITY_STORE_IN_SPI_FLASH)
    ret = getSpiSize(&u32Size);
    *u32SigOffset = u32Size - ((*u32SigOffset)*SECTOR_SIZE);
#elif defined (CONFIG_SECURITY_STORE_IN_NAND_FLASH)
    check_ubi_partition(NAND_DEFAULT_VOLUME,NAND_DEFAULT_PARTITION);
    ubi_get_volume_size(NAND_DEFAULT_VOLUME,&u32Size);
    UBOOT_DEBUG("u32Size : 0x%x\n",u32Size);
    UBOOT_DEBUG("ubi_get_leb_size : 0x%x\n",SECTOR_SIZE);
    *u32SigOffset = u32Size - ((*u32SigOffset)*SECTOR_SIZE);
#elif defined  (CONFIG_SECURITY_BOOT)  
    ret = get_mmc_partsize(FLASH_DEFAULT_VOLUME,&u32Size);
    *u32SigOffset = u32Size - ((*u32SigOffset)*SECTOR_SIZE);
#else
    #error "please set the correct security storage!!\n"
#endif

    #if defined  (CONFIG_SECURITY_BOOT)
    *u32SigBkOffset=*u32SigOffset+SECURE_INFOR_BACK_OFFSET;
    #else
    *u32SigBkOffset=*u32SigOffset+SECTOR_SIZE;
    #endif

    UBOOT_DEBUG("u32SigOffset : 0x%x\n",*u32SigOffset);
    UBOOT_DEBUG("u32SigBkOffset : 0x%x\n",*u32SigBkOffset);

    if(EN_SUCCESS == ret)
    {
        ret=0;
        UBOOT_TRACE("OK\n");
    }
    else
    {
        ret=-1;
        UBOOT_ERROR("get_signature_offset fail\n");
    }

    return ret;
}

#if defined(LG_CHG)
int SignatureSave(SECURITY_INFO *pBufferAddr)
{
   int ret=-1;
   int ret_bk =-1;
   unsigned int u32SigOffset = 0;
   unsigned int u32SigBkOffset = 0;

   UBOOT_TRACE("IN\n");

   if(pBufferAddr==NULL)
   {
      UBOOT_ERROR("The input parameter pBufferAddr' is a null pointer\n");
      return -1;
   }

   ret = raw_io_config(FLASH_DEFAULT_TARGET,FLASH_DEFAULT_PARTITION,FLASH_DEFAULT_VOLUME);
   if(ret != 0)
   {
       UBOOT_ERROR("raw_io_config setting fail!\n");
       return -1;
   }

   ret = get_signature_offset(&u32SigOffset,&u32SigBkOffset);
   if(ret != 0)
   {
       UBOOT_ERROR("get_signature_offset fail!\n");
       return -1;
   }

   UBOOT_DEBUG("u32SigOffset : 0x%x\n",u32SigOffset);
   UBOOT_DEBUG("u32SigBkOffset : 0x%x\n",u32SigBkOffset);

   ret = raw_write((unsigned int)pBufferAddr,u32SigOffset,sizeof(SECURITY_INFO));

   if(EN_SUCCESS == ret)
   {
       UBOOT_DEBUG("ret : %d , ret_bk : %d \n",ret,ret_bk);
       UBOOT_TRACE("OK\n");
       ret = 0;
   }
   else
   {
       ret= -1;
       UBOOT_ERROR("SignatureSave fail\n");
   }

    return ret;
}

extern int storage_read(uint32_t offset, size_t len, void* buf);

int _readSignature(unsigned int address, char *partitionName)
{
    struct partition_info   *mpi= NULL;
    U32 u32SignOffset=0;
    unsigned int				read_size;

    UBOOT_TRACE("IN\n");
    if(partitionName==NULL)
    {
        UBOOT_ERROR("partitionName is a null poniter\n");
        return -1;
    }
    UBOOT_DEBUG("partitionName=%s\n",partitionName);
    mpi = get_used_partition(partitionName);
	UBOOT_DEBUG("partition Offset=%d\n",(U32)mpi->offset);
    if (mpi == NULL) {
        UBOOT_ERROR("failed to get partition:%s\n",partitionName);
        return -1;
    }

	if (mpi->valid == NO) {
		UBOOT_ERROR("Partition is Not valid! => Skip!");
        return -1;
    }

	if(!mpi->filesize) {
		UBOOT_DEBUG("File image size is Zero, Using partition size!!");
		read_size = mpi->size;
	}
	else{
		read_size = mpi->filesize;
	}
	UBOOT_DEBUG("read_size=0x%x\n",read_size);

	u32SignOffset=mpi->offset+(read_size-sizeof(SUB_SECURE_INFO));
	UBOOT_DEBUG("u32SignOffset=0x%x\n",u32SignOffset);
	UBOOT_DEBUG("mpi->offse=0x%x\n",(U32)mpi->offset);
    UBOOT_DEBUG("The size of SUB_SECURE_INFO is 0x%x\n",sizeof(SUB_SECURE_INFO));

	if(0==storage_read((ulong)u32SignOffset, sizeof(SUB_SECURE_INFO), (void *)address))
    {
        UBOOT_DEBUG("dump the signature\n");
        UBOOT_DUMP(address,sizeof(SUB_SECURE_INFO));
    	UBOOT_TRACE("OK\n");
    	return 0;
    }
    else
    {
        UBOOT_ERROR("storage_read fail\n");
        return -1;
    }
}

int SignatureLoad(SECURITY_INFO *pBufferAddr)
{
    U8 err=0;
    SUB_SECURE_INFO secureInfo;
    UBOOT_TRACE("IN\n");

    if(0==_readSignature((U32)&secureInfo,"tzfw"))
	{
    	memcpy(&pBufferAddr->data.tee,&secureInfo.sInfo,sizeof(_SUB_SECURE_INFO));
    	memcpy(&pBufferAddr->data_interleave.tee,&secureInfo.sInfo_Interleave,sizeof(_SUB_SECURE_INFO));
    }
    else
    {
        err++;
    }

	if(0==_readSignature((U32)&secureInfo,"kernel"))
	{
    	memcpy(&pBufferAddr->data.Kernel,&secureInfo.sInfo,sizeof(_SUB_SECURE_INFO));
    	memcpy(&pBufferAddr->data_interleave.Kernel,&secureInfo.sInfo_Interleave,sizeof(_SUB_SECURE_INFO));
    }
    else
    {
        err++;
    }

   if(0==err)
   {
       UBOOT_TRACE("OK\n");
       return 0;
   }
   else
   {
       UBOOT_ERROR("SignatureLoad fail\n");
       return -1;
   }

}

#else
int SignatureSave(SECURITY_INFO *pBufferAddr)
{
   int ret=-1;
   int ret_bk =-1;
   unsigned int u32SigOffset = 0;
   unsigned int u32SigBkOffset = 0;
   unsigned int u32SecuritySize= 0;
   UBOOT_TRACE("IN\n");

   //Here, we check the CRC of SECUREITY_INFO, and the check range is from "pBufferAddr->data" to "pBufferAddr->data_interleave"
   u32SecuritySize = sizeof(_SECURITY_INFO_DATA) * NUMBER_OF_SECURE_INFO;

   if(pBufferAddr==NULL)
   {
      UBOOT_ERROR("The input parameter pBufferAddr' is a null pointer\n");
      return -1;
   }

   ret = raw_io_config(FLASH_DEFAULT_TARGET,FLASH_DEFAULT_PARTITION,FLASH_DEFAULT_VOLUME);
   if(ret != 0)
   {
       UBOOT_ERROR("raw_io_config setting fail!\n");
       return -1;
   }

   ret = get_signature_offset(&u32SigOffset,&u32SigBkOffset);
   if(ret != 0)
   {
       UBOOT_ERROR("get_signature_offset fail!\n");
       return -1;
   }

   UBOOT_DEBUG("u32SigOffset : 0x%x\n",u32SigOffset);
   UBOOT_DEBUG("u32SigBkOffset : 0x%x\n",u32SigBkOffset);

   // update CRC
   pBufferAddr->crc = crc32(0, (unsigned char const *)&pBufferAddr->data,u32SecuritySize);
   ret = raw_write((unsigned int)pBufferAddr,u32SigOffset,sizeof(SECURITY_INFO));
   ret_bk = raw_write((unsigned int)pBufferAddr,u32SigBkOffset,sizeof(SECURITY_INFO));

   if(EN_SUCCESS == ret || EN_SUCCESS == ret_bk)
   {
       UBOOT_DEBUG("ret : %d , ret_bk : %d \n",ret,ret_bk);
       UBOOT_TRACE("OK\n");
       ret = 0;
   }
   else
   {
       ret= -1;
       UBOOT_ERROR("SignatureSave fail\n");
   }

    return ret;
}

int SignatureLoad(SECURITY_INFO *pBufferAddr)
{
    int ret = -1;
    int flag1=0, flag2=0;
    unsigned int u32SigOffset = 0;
    unsigned int u32SigBkOffset = 0;
    unsigned int u32SecuritySize= 0;
    UBOOT_TRACE("IN\n");

    //Here, we check the CRC of SECUREITY_INFO, and the check range include "pBufferAddr->data" and "pBufferAddr->data_interleave"
    u32SecuritySize = sizeof(_SECURITY_INFO_DATA) * NUMBER_OF_SECURE_INFO;

    if(pBufferAddr==NULL)
    {
        UBOOT_ERROR("The input parameter pBufferAddr' is a null pointer\n");
        return -1;
    }

    ret = raw_io_config(FLASH_DEFAULT_TARGET,FLASH_DEFAULT_PARTITION,FLASH_DEFAULT_VOLUME);
    if(ret != 0)
    {
        UBOOT_ERROR("raw_io_config setting fail!\n");
        return -1;
    }

    ret = get_signature_offset(&u32SigOffset,&u32SigBkOffset);
    if(ret != 0)
    {
        UBOOT_ERROR("get_signature_offset fail!\n");
        return -1;
    }

    ret = raw_read((unsigned int)pBufferAddr,u32SigOffset,sizeof(SECURITY_INFO));

    if( (EN_SUCCESS == ret) && (pBufferAddr->crc == crc32(0, (unsigned char const *)&pBufferAddr->data,u32SecuritySize)) )
        flag1=1;

    ret = raw_read((unsigned int)pBufferAddr,u32SigBkOffset,sizeof(SECURITY_INFO));

    if( (EN_SUCCESS == ret) && (pBufferAddr->crc == crc32(0, (unsigned char const *)&pBufferAddr->data,u32SecuritySize)) )
        flag2=1;

    if( (flag2==0) && (flag1!=0) )
    {
        ret = raw_read((unsigned int)pBufferAddr,u32SigOffset,sizeof(SECURITY_INFO));
        if( (EN_SUCCESS == ret) && (pBufferAddr->crc == crc32(0, (unsigned char const *)&pBufferAddr->data,u32SecuritySize)))
        {
            ret = raw_write((unsigned int)pBufferAddr,u32SigBkOffset,sizeof(SECURITY_INFO));
        }
        else
        {
            UBOOT_ERROR("raw_read fail or caculate crc fail!\n");
            return -1;
        }
    }

    if((flag1==0)&&(flag2!=0))
    {
        ret = raw_write((unsigned int)pBufferAddr,u32SigOffset,sizeof(SECURITY_INFO));
    }

   if(EN_SUCCESS == ret)
   {
       ret=0;
       UBOOT_TRACE("OK\n");
   }
   else
   {
       ret=-1;
       UBOOT_ERROR("SignatureLoad fail\n");
   }

   return ret;
}
#endif

int do_save_secure_info(cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[])
{
    #define OBJECT_NAME argv[1]
    #define OBJECT_DRAM_ADDR argv[2]
    int ret=0;
    SUB_SECURE_INFO *pSubInfo=(SUB_SECURE_INFO *)simple_strtoul(OBJECT_DRAM_ADDR, NULL, 16);
    #if !(ENABLE_MODULE_ANDROID_BOOT == 1)
    SUB_SECURE_INFO_FOR_PARTIAL_AUTH *pSubInfoForParAuth=(SUB_SECURE_INFO_FOR_PARTIAL_AUTH *)simple_strtoul(OBJECT_DRAM_ADDR, NULL, 16);
    #endif
    SECURITY_INFO *pSecureInfo = (SECURITY_INFO *)malloc(sizeof(SECURITY_INFO));
    UBOOT_TRACE("IN\n");
    if(argc<3)
    {
        cmd_usage(cmdtp);
        SAFE_FREE(pSecureInfo);
        return -1;
    }


    if(pSecureInfo==NULL)
    {
        UBOOT_ERROR("malloc fail\n");
        return -1;
    }

    memset((void *)pSecureInfo,0,sizeof(SECURITY_INFO));

    ret = SignatureLoad(pSecureInfo);
    if(EN_ERROR_OF_CMD == ret)
    {
        free(pSecureInfo);
        UBOOT_ERROR("SignatureLoad fail\n");
        return -1;
    }
    else if(EN_ERROR_OF_CRC == ret)
    {
        UBOOT_INFO("\x1b[37;46m ===== [%s:%d] SECURITY_INFO might first upgrade !!! ===== \x1b[0m\n",__FUNCTION__,__LINE__);
    }

    if(strcmp(OBJECT_NAME,"keySet")==0){
        UBOOT_DEBUG("do_save_secure_info: keySet\n");
        memcpy((void *)&pSecureInfo->data.Key,(void *)simple_strtoul(OBJECT_DRAM_ADDR, NULL, 16),sizeof(SECURE_KEY_SET));
        memcpy((void *)&pSecureInfo->data_interleave.Key,(void *)simple_strtoul(OBJECT_DRAM_ADDR, NULL, 16),sizeof(SECURE_KEY_SET));
    }
    else{
        #if (ENABLE_MODULE_ANDROID_BOOT == 1)
        if(0==strcmp(OBJECT_NAME,"bootSign"))
        {
            UBOOT_DEBUG("do_save_secure_info: bootSign\n");
            memcpy((void *)&pSecureInfo->data.Boot,(void *)(&pSubInfo->sInfo),sizeof(_SUB_SECURE_INFO));
            memcpy((void *)&pSecureInfo->data_interleave.Boot,(void *)(&pSubInfo->sInfo_Interleave),sizeof(_SUB_SECURE_INFO));
        }
        else if(0==strcmp(OBJECT_NAME,"recoverySign"))
        {
            UBOOT_DEBUG("do_save_secure_info: recoverySign\n");
            memcpy((void *)&pSecureInfo->data.Recovery,(void *)(&pSubInfo->sInfo),sizeof(_SUB_SECURE_INFO));
            memcpy((void *)&pSecureInfo->data_interleave.Recovery,(void *)(&pSubInfo->sInfo_Interleave),sizeof(_SUB_SECURE_INFO));
        }
        else if(strcmp(OBJECT_NAME,"teeSign")==0){
            UBOOT_DEBUG("do_save_secure_info: teeSign\n");
            memcpy((void *)&pSecureInfo->data.tee,(void *)(&pSubInfo->sInfo),sizeof(_SUB_SECURE_INFO));
            memcpy((void *)&pSecureInfo->data_interleave.tee,(void *)(&pSubInfo->sInfo_Interleave),sizeof(_SUB_SECURE_INFO));
        }
        #if defined (CONFIG_SecureStorage)
        else if(strcmp(OBJECT_NAME,"ChunkSign")==0){
            UBOOT_DEBUG("do_save_secure_info: ChunkSign\n");
            memcpy((void *)&pSecureInfo->data.Chunk,(void *)(&pSubInfo->sInfo),sizeof(_SUB_SECURE_INFO));
            memcpy((void *)&pSecureInfo->data_interleave.Chunk,(void *)(&pSubInfo->sInfo_Interleave),sizeof(_SUB_SECURE_INFO));
        }
        else if(strcmp(OBJECT_NAME,"ChunkBackupSign")==0){
            UBOOT_DEBUG("do_save_secure_info: ChunkbackupSign\n");
            memcpy((void *)&pSecureInfo->data.Chunk_backup,(void *)(&pSubInfo->sInfo),sizeof(_SUB_SECURE_INFO));
            memcpy((void *)&pSecureInfo->data_interleave.Chunk_backup,(void *)(&pSubInfo->sInfo_Interleave),sizeof(_SUB_SECURE_INFO));
        }
        #endif//#if defined (CONFIG_SecureStorage)
        #else//#if (ENABLE_MODULE_ANDROID_BOOT == 1)
        if(strcmp(OBJECT_NAME,"kernelSign")==0){
            UBOOT_DEBUG("do_save_secure_info: kernelSign\n");
            memcpy((void *)&pSecureInfo->data.Kernel,(void *)(&pSubInfo->sInfo),sizeof(_SUB_SECURE_INFO));
            memcpy((void *)&pSecureInfo->data_interleave.Kernel,(void *)(&pSubInfo->sInfo_Interleave),sizeof(_SUB_SECURE_INFO));
        }
        else if(strcmp(OBJECT_NAME,"ROOTFSSign")==0){
            UBOOT_DEBUG("do_save_secure_info: ROOTFSSign\n");
            memcpy((void *)pSecureInfo->data.RootFs,(void *)(&pSubInfoForParAuth->sInfo),sizeof(_SUB_SECURE_INFO)*FRAGMENT_NUM);
            memcpy((void *)pSecureInfo->data_interleave.RootFs,(void *)(&pSubInfoForParAuth->sInfo_Interleave),sizeof(_SUB_SECURE_INFO)*FRAGMENT_NUM);
        }
        else if(strcmp(OBJECT_NAME,"mslibSign")==0){
            UBOOT_DEBUG("do_save_secure_info mslibSign:\n");
            memcpy((void *)pSecureInfo->data.MsLib,(void *)(&pSubInfoForParAuth->sInfo),sizeof(_SUB_SECURE_INFO)*FRAGMENT_NUM);
            memcpy((void *)pSecureInfo->data_interleave.MsLib,(void *)(&pSubInfoForParAuth->sInfo_Interleave),sizeof(_SUB_SECURE_INFO)*FRAGMENT_NUM);
        }
        else if(strcmp(OBJECT_NAME,"configSign")==0){
            UBOOT_DEBUG("do_save_secure_info configSign:\n");
            memcpy((void *)pSecureInfo->data.Config,(void *)(&pSubInfoForParAuth->sInfo),sizeof(_SUB_SECURE_INFO)*FRAGMENT_NUM);
            memcpy((void *)pSecureInfo->data_interleave.Config,(void *)(&pSubInfoForParAuth->sInfo_Interleave),sizeof(_SUB_SECURE_INFO)*FRAGMENT_NUM);
        }
        else if(strcmp(OBJECT_NAME,"applicationsSign")==0){
            UBOOT_DEBUG("do_save_secure_info: applicationsSign\n");
            memcpy((void *)pSecureInfo->data.App,(void *)(&pSubInfoForParAuth->sInfo),sizeof(_SUB_SECURE_INFO)*FRAGMENT_NUM);
            memcpy((void *)pSecureInfo->data_interleave.App,(void *)(&pSubInfoForParAuth->sInfo_Interleave),sizeof(_SUB_SECURE_INFO)*FRAGMENT_NUM);
        }
        else if(strcmp(OBJECT_NAME,"teeSign")==0){
            UBOOT_DEBUG("do_save_secure_info: teeSign\n");
            memcpy((void *)&pSecureInfo->data.tee,(void *)(&pSubInfo->sInfo),sizeof(_SUB_SECURE_INFO));
            memcpy((void *)&pSecureInfo->data_interleave.tee,(void *)(&pSubInfo->sInfo_Interleave),sizeof(_SUB_SECURE_INFO));
        }
        #endif
        else if(strcmp(OBJECT_NAME,"keySetSign")==0){
            UBOOT_DEBUG("do_save_secure_info: keySetSign\n");
        }
        else
        {
            free(pSecureInfo);
            UBOOT_ERROR("Invalid symbol %s\n",OBJECT_NAME);
            return -1;
        }
    }


    ret = SignatureSave(pSecureInfo);
    if(EN_SUCCESS != ret)
    {
        free(pSecureInfo);
        UBOOT_ERROR("SignatureSave fail\n");
        return -1;
    }

     free(pSecureInfo);
     UBOOT_TRACE("OK\n");
     return 0;
}

