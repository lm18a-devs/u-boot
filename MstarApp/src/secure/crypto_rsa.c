////////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2008-2009 MStar Semiconductor, Inc.
// All rights reserved.
//
// Unless otherwise stipulated in writing, any and all information contained
// herein regardless in any format shall remain the sole proprietary of
// MStar Semiconductor Inc. and be kept in strict confidence
// ("MStar Confidential Information") by the recipient.
// Any unauthorized act including without limitation unauthorized disclosure,
// copying, use, reproduction, sale, distribution, modification, disassembling,
// reverse engineering and compiling of the contents of MStar Confidential
// Information is unlawful and strictly prohibited. MStar hereby reserves the
// rights to any and all damages, losses, costs and expenses resulting therefrom.
//
//
// "Contains BIGDIGITS multiple-precision arithmetic code originally
// written by David Ireland, copyright (c) 2001-6 by D.I. Management
// Services Pty Limited <www.di-mgt.com.au>, and is used with
// permission."
//
////////////////////////////////////////////////////////////////////////////////

#define CRYPTO_RSA_C
#define HW_RSA2048

#include <secure/crypto_rsa.h>
#include <secure/crypto_func.h>
#include <secure/crypto_sha.h>
#include <common.h>
#include <MsSystem.h>
#include <MsDebug.h>
#include <MsTypes.h>
#include <secure/MsSecureCommon.h>
#include <drvAESDMA.h>

#if defined(CONFIG_MSTAR_CLEANBOOT)
#include <secure/apiSecureBoot.h>
#endif

extern void jump_to_console(void);
int RSA2048HWdecrypt(unsigned char *Signature, unsigned char *PublicKey_N, unsigned char *PublicKey_E, unsigned char *Sim_SignOut)
{
   UBOOT_TRACE("IN\n");
   if(NULL == Signature || NULL == PublicKey_N || NULL == PublicKey_E || NULL == Sim_SignOut)
   {
       UBOOT_ERROR("parameters Error\n");
       return -1;
   }
   DRVAESDMA_RESULT result = DRVAESDMA_OK;
   DrvAESDMA_RSASig RSASig;
   DrvAESDMA_RSAOut RSAOut;
   MDrv_AESDMA_Init(0x00, 0x40000000, 2); // wait the AESDMA.a
   memcpy(&RSASig,Signature,sizeof(RSASig));
   memset(&RSAOut,0,sizeof(RSAOut));
   DrvAESDMA_RSAKey RSAKey;
   memset(&RSAKey,0,sizeof(RSAKey));
   memcpy((unsigned char*)(RSAKey.u32KeyN),PublicKey_N,RSA_PUBLIC_KEY_N_LEN);
   memcpy((unsigned char*)(RSAKey.u32KeyE),PublicKey_E,RSA_PUBLIC_KEY_E_LEN);
   flush_cache((MS_U32)&RSASig,SIGNATURE_LEN);
   result=MDrv_RSA_Calculate(&RSASig,&RSAKey,E_DRVAESDMA_RSA2048_PUBLIC);
   if(DRVAESDMA_OK != result)
   {
      UBOOT_DEBUG("RSA HW decrypt error1\n");
      return -1;
   }
   memset((U8*)&RSAOut,0,sizeof(DrvAESDMA_RSAOut));
   while(DRVAESDMA_OK != MDrv_RSA_IsFinished());
   result= MDrv_RSA_Output(E_DRVAESDMA_RSA2048_PUBLIC, &RSAOut);
   if(DRVAESDMA_OK != result)
   {
      UBOOT_DEBUG("RSA HW decrypt error2\n");
      return -1;
   }
   flush_cache((MS_U32)(&RSAOut),256);
   memcpy(Sim_SignOut,&RSAOut,sizeof(RSAOut));
   UBOOT_TRACE("OK\n");
   return 0;
}

int rsa_main(unsigned char *Signature, unsigned char *PublicKey_N, unsigned char *PublicKey_E, unsigned char *Sim_SignOut)
{
    UBOOT_TRACE("IN\n");
    int ret = -1;
    //AESDMA
    ret = RSA2048HWdecrypt(Signature,PublicKey_N,PublicKey_E,Sim_SignOut);
    if(-1 == ret)
    {
        UBOOT_ERROR("RSA Decrypt Error\n");
    }
    else
    {
        UBOOT_TRACE("OK\n");
    }
	return ret;
}
#undef CRYPTO_RSA_C
