//<MStar Software>
//***********************************************************************************
//MStar Software
//Copyright (c) 2010 - 2012 MStar Semiconductor, Inc. All rights reserved.
//All software, firmware and related documentation herein (��MStar Software��) are intellectual property of MStar Semiconductor, Inc. (��MStar��) and protected by law, including, but not limited to, copyright law and international treaties.  Any use, modification, reproduction, retransmission, or republication of all or part of MStar Software is expressly prohibited, unless prior written permission has been granted by MStar.
//By accessing, browsing and/or using MStar Software, you acknowledge that you have read, understood, and agree, to be bound by below terms (��Terms��) and to comply with all applicable laws and regulations:
//
//1. MStar shall retain any and all right, ownership and interest to MStar Software and any modification/derivatives thereof.  No right, ownership, or interest to MStar Software and any modification/derivatives thereof is transferred to you under Terms.
//2. You understand that MStar Software might include, incorporate or be supplied together with third party��s software and the use of MStar Software may require additional licenses from third parties.  Therefore, you hereby agree it is your sole responsibility to separately obtain any and all third party right and license necessary for your use of such third party��s software.
//3. MStar Software and any modification/derivatives thereof shall be deemed as MStar��s confidential information and you agree to keep MStar��s confidential information in strictest confidence and not disclose to any third party.
//4. MStar Software is provided on an ��AS IS�� basis without warranties of any kind. Any warranties are hereby expressly disclaimed by MStar, including without limitation, any warranties of merchantability, non-infringement of intellectual property rights, fitness for a particular purpose, error free and in conformity with any international standard.  You agree to waive any claim against MStar for any loss, damage, cost or expense that you may incur related to your use of MStar Software.
//   In no event shall MStar be liable for any direct, indirect, incidental or consequential damages, including without limitation, lost of profit or revenues, lost or damage of data, and unauthorized system use.  You agree that this Section 4 shall still apply without being affected even if MStar Software has been modified by MStar in accordance with your request or instruction for your use, except otherwise agreed by both parties in writing.
//5. If requested, MStar may from time to time provide technical supports or services in relation with MStar Software to you for your use of MStar Software in conjunction with your or your customer��s product (��Services��).  You understand and agree that, except otherwise agreed by both parties in writing, Services are provided on an ��AS IS�� basis and the warranty disclaimer set forth in Section 4 above shall apply.
//6. Nothing contained herein shall be construed as by implication, estoppels or otherwise: (a) conferring any license or right to use MStar name, trademark, service mark, symbol or any other identification; (b) obligating MStar or any of its affiliates to furnish any person, including without limitation, you and your customers, any assistance of any kind whatsoever, or any information; or (c) conferring any license or right under any intellectual property right.
//7. These terms shall be governed by and construed in accordance with the laws of Taiwan, R.O.C., excluding its conflict of law rules.  Any and all dispute arising out hereof or related hereto shall be finally settled by arbitration referred to the Chinese Arbitration Association, Taipei in accordance with the ROC Arbitration Law and the Arbitration Rules of the Association by three (3) arbitrators appointed in accordance with the said Rules.  The place of arbitration shall be in Taipei, Taiwan and the language shall be English.
//   The arbitration award shall be final and binding to both parties.
//***********************************************************************************
//<MStar Software>
//-------------------------------------------------------------------------------------------------
//  Include Files
//-------------------------------------------------------------------------------------------------
#define CRYPTO_AUTH_C

#include <common.h>
#include <uboot_mmap.h>
#include <linux/types.h>
#include <secure/crypto_rsa.h>
#include <secure/crypto_func.h>
#include <secure/crypto_auth.h>
#include <secure/crypto_sha.h>

#include <MsTypes.h>
#include <MsIRQ.h>
#include <MsSystem.h>
//#include <drvISR.h>
#include <drvMBX.h>
//#if !defined(CONFIG_KAISERIN)
//#include <drvSMBX.h>
//#endif
#include <drvAESDMA.h>
#include <secure/apiSecureBoot.h>
#include <MsDebug.h>

//-------------------------------------------------------------------------------------------------
//  Local Defines
//-------------------------------------------------------------------------------------------------
#define VERIFY_SIGNATURE_LEN		256
#define VERIFY_RSA_PUBLIC_KEY_N_LEN	256
#define VERIFY_RSA_PUBLIC_KEY_E_LEN	4
#define VERIFY_RSA_PUBLIC_KEY_LEN	(VERIFY_RSA_PUBLIC_KEY_N_LEN+VERIFY_RSA_PUBLIC_KEY_E_LEN)
#define VERIFY_AES_IV_LEN			16
#define VERIFY_AES_KEY_LEN			16
#define VERIFY_HMAC_KEY_LEN			32
#define VERIFY_FRAGMENT_NUM			8
//-------------------------------------------------------------------------------------------------
//  Local Structurs
//-------------------------------------------------------------------------------------------------

typedef struct
{
	U32 u32Num;
 	U32 u32Size;
}IMAGE_INFO_VERIFY;

typedef struct
{
	U8 u8SecIdentify[8];
	IMAGE_INFO_VERIFY info;
	U8 u8Signature[VERIFY_SIGNATURE_LEN];
}_SUB_SECURE_INFO_VERIFY;

typedef struct
{
	_SUB_SECURE_INFO_VERIFY customer;
	U8 u8RSABootPublicKey[VERIFY_RSA_PUBLIC_KEY_LEN];
	U8 u8RSAUpgradePublicKey[VERIFY_RSA_PUBLIC_KEY_LEN];
	U8 u8RSAImagePublicKey[VERIFY_RSA_PUBLIC_KEY_LEN];
	U8 u8AESBootKey[VERIFY_AES_KEY_LEN];
	U8 u8AESUpgradeKey[VERIFY_AES_KEY_LEN];
	U8 u8MagicID[16];
	U8 crc[4];
}CUSTOMER_KEY_BANK_VERIFY;

//-------------------------------------------------------------------------------------------------
//  Public Variables
//-------------------------------------------------------------------------------------------------

extern CUSTOMER_KEY_BANK_VERIFY stCusKey;

/*unsigned char g_rawData[32] = {
	0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
	0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
	0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF
};

unsigned char g_Signature[32] = {
			0x1c ,0xfc ,0x26 ,0xfc ,0xbc ,0x89 ,0x85 ,0x79 ,0x97 ,0x85 ,0x8a ,0xc8 ,0x19,
		    0xb0 ,0x7a ,0x70 ,0xef ,0xde ,0x3b ,0x63 ,0x38 ,0x93 ,0x45 ,0xd4 ,0xd7 ,0x86,
		    0xe6 ,0xcf ,0xbe ,0x10 ,0x3f ,0x27
};

unsigned char AES_Signature[32] = {
		0x1b ,0x1d ,0x47 ,0x70 ,0x8e ,0xc6 ,0x32 ,0x08 ,0x9f ,0x2e ,0xe4 ,
		0x0b ,0xd9 ,0xaa ,0xa0 ,0x65 ,0x09 ,0xdf ,0x48 ,0xfd ,0x0d ,0x79 ,
		0xac ,0xcf ,0x23 ,0xa2 ,0x69 ,0xdb ,0x39 ,0xb9 ,0xe6 ,0x16
};*/

//-------------------------------------------------------------------------------------------------
//  External Functions
//-------------------------------------------------------------------------------------------------

extern int Secure_AES_ECB_Decrypt(MS_U32 u32Addr, MS_U32 u32Len, MS_U8 *bKey);

//-------------------------------------------------------------------------------------------------
//  Local Variables
//-------------------------------------------------------------------------------------------------


//-------------------------------------------------------------------------------------------------
//  Local functions
//-------------------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------
int Secure_verifySignature(U8 *u8PublicKey_N, U8 *u8PublicKey_E, U8 *u8Signature, U8 *digest)
{
    UBOOT_TRACE("IN\n");
    int ret;
    unsigned char signature[RSA_KEY_DIGI_LEN*4] = "\0";
    ret = rsa_main(u8Signature, u8PublicKey_N, u8PublicKey_E, signature);
    if(-1 == ret)
    {
        return -1;
    }

    if(memcmp(signature,digest,sizeof(digest))!=0)
    {
        UBOOT_DUMP((unsigned int)signature, SHA256_DIGEST_SIZE);
        UBOOT_DUMP((unsigned int)digest, SHA256_DIGEST_SIZE);
        UBOOT_ERROR("Error: Authentication Fail!!\n");
        return -1; //Need to Fix : temp to skip Secure_verifySignature
        //return 0;
    }
    UBOOT_TRACE("OK\n");
    return 0;
}


int Secure_doAuthentication(U8 *u8PublicKey_N, U8 *u8PublicKey_E, U8 *u8Signature, U8 *u8AuthData, U32 u32AuthDataLen)
{
    UBOOT_TRACE("IN\n");
    int ret;
    unsigned char digest[SHA256_DIGEST_SIZE]= "\0";
    
    flush_cache_all();

    ret = CommonSHA((unsigned int)u8AuthData, (unsigned int)digest,u32AuthDataLen);
    if(-1 == ret)
    {
        return -1;
    }
    if(0!=Secure_verifySignature(u8PublicKey_N, u8PublicKey_E, u8Signature, digest))
    {
        UBOOT_ERROR("Error: Authentication Fail!!\n");
        return -1;
    }
    UBOOT_TRACE("OK\n");
    return 0;
}


static void _inverse(U8 *dst, U8 *src, U32 len)
{
    int i=0;
    for(i=0;i<len;i++)
    {
        *(dst+i)=*(src+len-1-i);
    }
}

int snapshot_image_verify(U8 *u8Signature, U8 *u8AuthData, U32 u32AuthDataLen)
{
    UBOOT_TRACE("IN\n");
    int ret;
    unsigned char digest[SHA256_DIGEST_SIZE]= "\0";

    flush_cache_all();

    UBOOT_DUMP((unsigned int)u8AuthData, SHA256_DIGEST_SIZE);
#if 0 //USE SW SHA
    ret = CommonSHA((unsigned int)u8AuthData, (unsigned int)digest,u32AuthDataLen);
    if(-1 == ret)
    {
        return -1;
    }
#else
    {
        int i;
        unsigned char temp=0;
        sha256((const unsigned char *)u8AuthData, u32AuthDataLen, (unsigned char *)digest);
        for(i=0;i<(SHA256_DIGEST_SIZE/2);i++)
        {
            temp= ((unsigned char *)digest)[i];
            ((unsigned char *)digest)[i]=((unsigned char *)digest)[SHA256_DIGEST_SIZE-1-i];
            ((unsigned char *)digest)[SHA256_DIGEST_SIZE-1-i]=temp;
        }
    }
#endif

    unsigned char u8Key[16] = {0};
    _inverse(u8Key,stCusKey.u8AESBootKey,16);

    flush_cache((U32)u8Signature,32);
    Secure_AES_ECB_Decrypt((U32)u8Signature, 32, u8Key);

    if(memcmp(u8Signature,digest,sizeof(digest))!=0)
    {
        UBOOT_DUMP((unsigned int)u8Signature, SHA256_DIGEST_SIZE);
        UBOOT_DUMP((unsigned int)digest, SHA256_DIGEST_SIZE);
#if 0 //def SNAPSHOT_DEBUG
        printf("u8Signature\n");
        for(i=0; i<SHA256_DIGEST_SIZE; ++i)
            printf("%02x ", 0xFF & ((char*)u8Signature)[i]);
        printf("\n");


        printf("digest\n");
        for(i=0; i<SHA256_DIGEST_SIZE; ++i)
            printf("%02x ", 0xFF & ((char*)digest)[i]);
        printf("\n");
#endif
        UBOOT_ERROR("Error: Authentication Fail!!\n");
        return -1;
    }
#if 0 //def SNAPSHOT_DEBUG
	else
	{
		printf("zzindda signature\n");
		for(i=0; i<SHA256_DIGEST_SIZE; ++i)
			printf("%02x ", 0xFF & ((char*)u8Signature)[i]);
		printf("\n");


		printf("zzindda digest\n");
		for(i=0; i<SHA256_DIGEST_SIZE; ++i)
			printf("%02x ", 0xFF & ((char*)digest)[i]);
		printf("\n");
	}
#endif
    UBOOT_TRACE("OK\n");
    return 0;
}

/* test function
int snapshare_image_verify_test(void)
{
	int ret = 0;
	ret = snapshot_image_verify(AES_Signature,g_rawData,32);
	if(ret == -1)
	{
		UBOOT_ERROR("Error: snapshot_image_verify Fail!!\n");
	}
	return ret;
}
*/


#undef CRYPTO_AUTH_C
