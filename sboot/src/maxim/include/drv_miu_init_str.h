//<MStar Software>
//******************************************************************************
// MStar Software
// Copyright (c) 2010 - 2016 MStar Semiconductor, Inc. All rights reserved.
// All software, firmware and related documentation herein ("MStar Software") are
// intellectual property of MStar Semiconductor, Inc. ("MStar") and protected by
// law, including, but not limited to, copyright law and international treaties.
// Any use, modification, reproduction, retransmission, or republication of all
// or part of MStar Software is expressly prohibited, unless prior written
// permission has been granted by MStar.
//
// By accessing, browsing and/or using MStar Software, you acknowledge that you
// have read, understood, and agree, to be bound by below terms ("Terms") and to
// comply with all applicable laws and regulations:
//
// 1. MStar shall retain any and all right, ownership and interest to MStar
//    Software and any modification/derivatives thereof.
//    No right, ownership, or interest to MStar Software and any
//    modification/derivatives thereof is transferred to you under Terms.
//
// 2. You understand that MStar Software might include, incorporate or be
//    supplied together with third party`s software and the use of MStar
//    Software may require additional licenses from third parties.
//    Therefore, you hereby agree it is your sole responsibility to separately
//    obtain any and all third party right and license necessary for your use of
//    such third party`s software.
//
// 3. MStar Software and any modification/derivatives thereof shall be deemed as
//    MStar`s confidential information and you agree to keep MStar`s
//    confidential information in strictest confidence and not disclose to any
//    third party.
//
// 4. MStar Software is provided on an "AS IS" basis without warranties of any
//    kind. Any warranties are hereby expressly disclaimed by MStar, including
//    without limitation, any warranties of merchantability, non-infringement of
//    intellectual property rights, fitness for a particular purpose, error free
//    and in conformity with any international standard.  You agree to waive any
//    claim against MStar for any loss, damage, cost or expense that you may
//    incur related to your use of MStar Software.
//    In no event shall MStar be liable for any direct, indirect, incidental or
//    consequential damages, including without limitation, lost of profit or
//    revenues, lost or damage of data, and unauthorized system use.
//    You agree that this Section 4 shall still apply without being affected
//    even if MStar Software has been modified by MStar in accordance with your
//    request or instruction for your use, except otherwise agreed by both
//    parties in writing.
//
// 5. If requested, MStar may from time to time provide technical supports or
//    services in relation with MStar Software to you for your use of
//    MStar Software in conjunction with your or your customer`s product
//    ("Services").
//    You understand and agree that, except otherwise agreed by both parties in
//    writing, Services are provided on an "AS IS" basis and the warranty
//    disclaimer set forth in Section 4 above shall apply.
//
// 6. Nothing contained herein shall be construed as by implication, estoppels
//    or otherwise:
//    (a) conferring any license or right to use MStar name, trademark, service
//        mark, symbol or any other identification;
//    (b) obligating MStar or any of its affiliates to furnish any person,
//        including without limitation, you and your customers, any assistance
//        of any kind whatsoever, or any information; or
//    (c) conferring any license or right under any intellectual property right.
//
// 7. These terms shall be governed by and construed in accordance with the laws
//    of Taiwan, R.O.C., excluding its conflict of law rules.
//    Any and all dispute arising out hereof or related hereto shall be finally
//    settled by arbitration referred to the Chinese Arbitration Association,
//    Taipei in accordance with the ROC Arbitration Law and the Arbitration
//    Rules of the Association by three (3) arbitrators appointed in accordance
//    with the said Rules.
//    The place of arbitration shall be in Taipei, Taiwan and the language shall
//    be English.
//    The arbitration award shall be final and binding to both parties.
//
//******************************************************************************
//<MStar Software>
////////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2006-2012 MStar Semiconductor, Inc.
// All rights reserved.
//
// Unless otherwise stipulated in writing, any and all information contained
// herein regardless in any format shall remain the sole proprietary of
// MStar Semiconductor Inc. and be kept in strict confidence
// ("MStar Confidential Information") by the recipient.
// Any unauthorized act including without limitation unauthorized disclosure,
// copying, use, reproduction, sale, distribution, modification, disassembling,
// reverse engineering and compiling of the contents of MStar Confidential
// Information is unlawful and strictly prohibited. MStar hereby reserves the
// rights to any and all damages, losses, costs and expenses resulting therefrom.
//
///////////////////////////////////////////////////////////////////////////////

#ifndef _DRV_MIU_INIT_STR_H_
#define _DRV_MIU_INIT_STR_H_

#ifndef _BOARD_H_
#include "Board.h"
#endif

#ifndef _C_RIUBASE_H_
#include "c_riubase.h"
#endif

#define MIU_VER                         'M','I','U','_','M','A','X','I','M',' ','V','e','r',':','1','_','0','_','0'
#define REG_ADDR_BASE                   0x1f000000

#ifndef __ASSEMBLER__

#ifndef __DRV_RIU_H__
#include "drvRIU.h"
#endif

#ifndef CONFIG_ONEBIN_ENABLE
#ifndef _MIU_MST110B_20AUYGO_16183_MAXIM_STR_H_
#include "MIU_MST110B_20AUYGO_16183_MAXIM_STR.h"
#endif

#ifndef _MIU_MST110B_20AUYGO_16183_MAXIM_23p5_STR_H_
#include "MIU_MST110B_20AUYGO_16183_MAXIM_23p5_STR.h"
#endif

#ifndef _MIU_MST107B_10AUVC_16142_MAXIM_STR_H_
#include "MIU_MST107B_10AUVC_16142_MAXIM_STR.h"
#endif

#ifndef _MIU_MST108B_X_X_MAXIM_STR_H_
#include "MIU_MST108B_X_X_MAXIM_STR.h"
#endif

#ifndef _MIU_MST108B_X_X_MAXIM_23p5_STR_H_
#include "MIU_MST108B_X_X_MAXIM_23p5_STR.h"
#endif

#ifndef _MIU_MST111D_X_X_MAXIM_STR_H_
#include "MIU_MST111D_X_X_MAXIM_STR.h"
#endif

#ifndef _MIU_MST215C_D01B_S_MAXIM_STR_H_
#include "MIU_MST215C_D01B_S_MAXIM_STR.h"
#endif

#ifndef _MIU_MST253A_D01A_S_MAXIM_STR_H_
#include "MIU_MST253A_D01A_S_MAXIM_STR.h"
#endif

#ifndef _MIU_MST253B_D01A_S_MAXIM_STR_H_
#include "MIU_MST253B_D01A_S_MAXIM_STR.h"
#endif

#ifndef _MIU_MST253C_D01A_S_MAXIM_STR_H_
#include "MIU_MST253C_D01A_S_MAXIM_STR.h"
#endif
#endif

const MS_REG_INIT MIU_PreSetting_Str[] =
{
#if 0
    _RV32_2(0x16159C, 0x8004),
    _RV32_2(0x16159E, 0xFF08),
    _RV32_2(0x1615AC, 0x09F0),
    _RV32_2(0x1615AE, 0x2011),

    _RV32_2(0x16229C, 0x8004),
    _RV32_2(0x16229E, 0xFF08),
    _RV32_2(0x1622AC, 0x09F0),
    _RV32_2(0x1622AE, 0x2011),
#endif

    _RV32_2(0x16158E, 0x0001),     //GPU Highway

    _END_OF_TBL32_,
    MIU_VER
};

const MS_REG_INIT MIU_PreInit_Str[] =
{
//  _RV32_2(0x113BEC, 0x8000),  //enable dramobf

#if !defined(CONFIG_MIU0_DRAM_NONE)
    //MIU0 Software Reset
    _RV32_2(0x10121e, 0x0c00),
    _RV32_2(0x10121e, 0x0c00),
    _RV32_2(0x10121e, 0x0c00),
    _RV32_2(0x10121e, 0x0c01),

    //MIU0 Request Mask
    _RV32_2(0x101246, 0xFFFE),
    _RV32_2(0x101266, 0xFFFF),
    _RV32_2(0x101286, 0xFFFF),
    _RV32_2(0x1012A6, 0xFFFF),
    _RV32_2(0x161506, 0xFFFF),
    _RV32_2(0x161526, 0xFFFF),
    _RV32_2(0x152006, 0xFFFF),
    _RV32_2(0x152026, 0xFFFF),
#endif

#if !defined(CONFIG_MIU1_DRAM_NONE)
    //MIU1 Software Reset
    _RV32_2(0x10061e, 0x0000),
    _RV32_2(0x10061e, 0x0000),
    _RV32_2(0x10061e, 0x0000),
    _RV32_2(0x10061e, 0x0c01),

    //MIU1 Request Mask
    _RV32_2(0x100646, 0xFFFE),
    _RV32_2(0x100666, 0xFFFF),
    _RV32_2(0x100686, 0xFFFF),
    _RV32_2(0x1006A6, 0xFFFF),
    _RV32_2(0x162206, 0xFFFF),
    _RV32_2(0x162226, 0xFFFF),
    _RV32_2(0x152106, 0xFFFF),
    _RV32_2(0x152126, 0xFFFF),
#endif

    _END_OF_TBL32_,
    MIU_VER
};

const MS_REG_INIT MIU_PostInit_Str[] =
{
#if !defined(CONFIG_MIU0_DRAM_NONE)
    //open all MIU0 request mask (All IP can access MIU resource)
    _RV32_2(0x101246, 0x0000),  //Mask MIU0_group_0
    _RV32_2(0x101266, 0x0000),  //Mask MIU0_group_1
    _RV32_2(0x101286, 0x0000),  //Mask MIU0_group_2
    _RV32_2(0x1012a6, 0x0000),  //Mask MIU0_group_3
    _RV32_2(0x161506, 0x0000),  //Mask MIU0_group_4
    _RV32_2(0x161526, 0x0000),  //Mask MIU0_group_5

    _RV32_2(0x1615E2, 0x007E),
    _RV32_2(0x1615E8, 0x0078),
    _RV32_2(0x1615EA, 0x0418),
    _RV32_2(0x1615EC, 0x0101),

    _RV32_2(0x152006, 0x0000),
    _RV32_2(0x152026, 0x0000),
#endif

    _RV32_2(0x10121e, 0x8c08),  //SW initial done and turn on arbitor

#if !defined(CONFIG_MIU1_DRAM_NONE)
    //open all MIU1 request mask (All IP can access MIU resource)
    _RV32_2(0x100646, 0x0000),  //Mask MIU1_group_0
    _RV32_2(0x100666, 0x0000),  //Mask MIU1_group_1
    _RV32_2(0x100686, 0x0000),  //Mask MIU1_group_2
    _RV32_2(0x1006a6, 0x0000),  //Mask MIU1_group_3
    _RV32_2(0x162206, 0x0000),  //Mask MIU1_group_4
    _RV32_2(0x162226, 0x0000),  //Mask MIU1_group_5
    _RV32_2(0x1622E2, 0x007E),
    _RV32_2(0x1622E8, 0x0078),
    _RV32_2(0x1622EA, 0x0418),
    _RV32_2(0x1622EC, 0x0101),

   _RV32_2(0x152106, 0x0000),
    _RV32_2(0x152126, 0x0000),
#endif

    _RV32_2(0x1012fe, 0xa0e1),

    _RV32_2(0x1012f8, 0x0000),
    _RV32_2(0x1006f8, 0x0000),

    _END_OF_TBL32_,
    MIU_VER
};

#endif /* !__ASSEMBLER__ */

#endif /* _DRV_MIU_INIT_H_ */
