//<MStar Software>
//******************************************************************************
// MStar Software
// Copyright (c) 2010 - 2012 MStar Semiconductor, Inc. All rights reserved.
// All software, firmware and related documentation herein ("MStar Software") are
// intellectual property of MStar Semiconductor, Inc. ("MStar") and protected by
// law, including, but not limited to, copyright law and international treaties.
// Any use, modification, reproduction, retransmission, or republication of all
// or part of MStar Software is expressly prohibited, unless prior written
// permission has been granted by MStar.
//
// By accessing, browsing and/or using MStar Software, you acknowledge that you
// have read, understood, and agree, to be bound by below terms ("Terms") and to
// comply with all applicable laws and regulations:
//
// 1. MStar shall retain any and all right, ownership and interest to MStar
//    Software and any modification/derivatives thereof.
//    No right, ownership, or interest to MStar Software and any
//    modification/derivatives thereof is transferred to you under Terms.
//
// 2. You understand that MStar Software might include, incorporate or be
//    supplied together with third party`s software and the use of MStar
//    Software may require additional licenses from third parties.
//    Therefore, you hereby agree it is your sole responsibility to separately
//    obtain any and all third party right and license necessary for your use of
//    such third party`s software.
//
// 3. MStar Software and any modification/derivatives thereof shall be deemed as
//    MStar`s confidential information and you agree to keep MStar`s
//    confidential information in strictest confidence and not disclose to any
//    third party.
//
// 4. MStar Software is provided on an "AS IS" basis without warranties of any
//    kind. Any warranties are hereby expressly disclaimed by MStar, including
//    without limitation, any warranties of merchantability, non-infringement of
//    intellectual property rights, fitness for a particular purpose, error free
//    and in conformity with any international standard.  You agree to waive any
//    claim against MStar for any loss, damage, cost or expense that you may
//    incur related to your use of MStar Software.
//    In no event shall MStar be liable for any direct, indirect, incidental or
//    consequential damages, including without limitation, lost of profit or
//    revenues, lost or damage of data, and unauthorized system use.
//    You agree that this Section 4 shall still apply without being affected
//    even if MStar Software has been modified by MStar in accordance with your
//    request or instruction for your use, except otherwise agreed by both
//    parties in writing.
//
// 5. If requested, MStar may from time to time provide technical supports or
//    services in relation with MStar Software to you for your use of
//    MStar Software in conjunction with your or your customer`s product
//    ("Services").
//    You understand and agree that, except otherwise agreed by both parties in
//    writing, Services are provided on an "AS IS" basis and the warranty
//    disclaimer set forth in Section 4 above shall apply.
//
// 6. Nothing contained herein shall be construed as by implication, estoppels
//    or otherwise:
//    (a) conferring any license or right to use MStar name, trademark, service
//        mark, symbol or any other identification;
//    (b) obligating MStar or any of its affiliates to furnish any person,
//        including without limitation, you and your customers, any assistance
//        of any kind whatsoever, or any information; or
//    (c) conferring any license or right under any intellectual property right.
//
// 7. These terms shall be governed by and construed in accordance with the laws
//    of Taiwan, R.O.C., excluding its conflict of law rules.
//    Any and all dispute arising out hereof or related hereto shall be finally
//    settled by arbitration referred to the Chinese Arbitration Association,
//    Taipei in accordance with the ROC Arbitration Law and the Arbitration
//    Rules of the Association by three (3) arbitrators appointed in accordance
//    with the said Rules.
//    The place of arbitration shall be in Taipei, Taiwan and the language shall
//    be English.
//    The arbitration award shall be final and binding to both parties.
//
//******************************************************************************
//<MStar Software>
/*===========================================================
 * MStar Semiconductor Inc.
 *
 * eMMC Driver for FCIE4 - eMMC.h
 *
 * History
 *    - initial version, 2011.09.22, Hill.Sung
 *      please modify the eMMC_platform.h for your platform.
 *
 *
 *===========================================================*/
#ifndef eMMC_DRIVER_H
#define eMMC_DRIVER_H


//===========================================================
// debug macro
//===========================================================


//=====================================================================================
#include "../config/eMMC_config.h" // [CAUTION]: edit eMMC_config.h for your platform
//=====================================================================================
#include "eMMC_err_codes.h"


//===========================================================
// macro for Spec.
//===========================================================
#define eMMC_DEVTYPE_HS400_1_8V   BIT6 // ECSD[196]
#define eMMC_DEVTYPE_HS200_1_8V   BIT4
#define eMMC_DEVTYPE_DDR          BIT2
#define eMMC_DEVTYPE_ALL          (eMMC_DEVTYPE_HS400_1_8V|eMMC_DEVTYPE_HS200_1_8V|eMMC_DEVTYPE_DDR)

#define eMMC_SPEED_OLD            0
#define eMMC_SPEED_HIGH           1
#define eMMC_SPEED_HS200          2
#define eMMC_SPEED_HS400          3

#define eMMC_FLAG_TRIM            BIT0
#define eMMC_FLAG_HPI_CMD12       BIT1
#define eMMC_FLAG_HPI_CMD13       BIT2

//-------------------------------------------------------
// Devices has to be in 512B block length mode by default
// after power-on, or software reset.
//-------------------------------------------------------
#define eMMC_SECTOR_512BYTE       0x200
#define eMMC_SECTOR_512BYTE_BITS  9
#define eMMC_SECTOR_512BYTE_MASK  (eMMC_SECTOR_512BYTE-1)

#define eMMC_SECTOR_BUF_16KB      (eMMC_SECTOR_512BYTE * 0x20)

#define eMMC_SECTOR_BYTECNT       eMMC_SECTOR_512BYTE
#define eMMC_SECTOR_BYTECNT_BITS  eMMC_SECTOR_512BYTE_BITS
//-------------------------------------------------------

#define eMMC_ExtCSD_SetBit        1
#define eMMC_ExtCSD_ClrBit        2
#define eMMC_ExtCSD_WByte         3

#define eMMC_CMD_BYTE_CNT         5
#define eMMC_R1_BYTE_CNT          5
#define eMMC_R1b_BYTE_CNT         5
#define eMMC_R2_BYTE_CNT          16
#define eMMC_R3_BYTE_CNT          5
#define eMMC_R4_BYTE_CNT          5
#define eMMC_R5_BYTE_CNT          5
#define eMMC_MAX_RSP_BYTE_CNT     eMMC_R2_BYTE_CNT
#define eMMC_MIN_RSP_BYTE_CNT     6

//===========================================================
//  driver flag (u32_DrvFlag)
//===========================================================
#define FCIE_FLAG_GET_PART_INFO   BIT1
#define FCIE_FLAG_RSP_WAIT_D0H    BIT2 // currently only R1b
#define FCIE_FLAG_DDR_MODE        BIT3
#define FCIE_FLAG_DDR_TUNING      BIT4
#define FCIE_FLAG_SPEED_MASK      (BIT5|BIT6)
#define FCIE_FLAG_SPEED_HIGH      BIT5
#define FCIE_FLAG_SPEED_HS200     BIT6
#define FCIE_FLAG_SPEED_HS400     BIT7
#define FCIE_FLAG_WEAK_STRENGTH   BIT8


// ----------------------------------------------
typedef eMMC_PACK0 struct _eMMC_FCIE_DDRT_TABLE {

	U8 u8_SetCnt, u8_CurSetIdx;
	// ATOP (for  DDR52, HS200, HS400)
	eMMC_FCIE_ATOP_SET_t Set[1];

	U32 u32_ChkSum; // put in the last
	U32 u32_VerNo; // for auto update

} eMMC_PACK1 eMMC_FCIE_TIMING_TABLE_t;

#define REG_OP_W    1
#define REG_OP_CLRBIT  2
#define REG_OP_SETBIT  3

typedef eMMC_PACK0 struct _eMMC_FCIE_REG_SET {      //total 10 bytes
    U32 u32_RegAddress;             //(BANK_ADDRESS + REGISTER OFFSET ) << 2
    U16 u16_RegValue;
    U16 u16_RegMask;
    U16 u16_OpCode;
} eMMC_PACK1 eMMC_FCIE_REG_SET_t;

typedef eMMC_PACK0 struct _eMMC_FCIE_GEN_TIMING_TABLE {
    U32 u32_ChkSum;
	U32 u32_VerNo; // for auto update
    U32 u32_Clk;
    U8 u8_SpeedMode;
    U8 u8_CurSetIdx;
    U8 u8_RegisterCnt;
    U8 u8_SetCnt;
    U8 au8_CID[eMMC_MAX_RSP_BYTE_CNT];
    U32 u32_Dummy[6];      //for extension
    eMMC_FCIE_REG_SET_t RegSet[45]; //at most 45 register set
} eMMC_PACK1 eMMC_FCIE_GEN_TIMING_TABLE_t;

#define eMMC_TIMING_TABLE_CHKSUM_OFFSET  8



typedef struct _eMMC_DRIVER
{
	// ----------------------------------------
    // FCIE
    // ----------------------------------------
	U32 u32_DrvFlag;
	U16 u16_RCA;
	U16 u16_Reg10_Mode;
	U8  au8_Rsp[eMMC_MIN_RSP_BYTE_CNT];
    U8  au8_CID[eMMC_MAX_RSP_BYTE_CNT];
    // ----------------------------------------
    // eMMC
    // ----------------------------------------
	U8  u8_IfSectorMode;
	// ExtCSD
	U8  u8_BUS_WIDTH;
	U8  u8_ECSD185_HsTiming;
	U8  u8_ECSD196_DevType;
	U8  u8_PadType;
	eMMC_FCIE_TIMING_TABLE_t TimingTable_t;
    eMMC_FCIE_GEN_TIMING_TABLE_t TimingTable_G_t;
} eMMC_DRIVER, *P_eMMC_DRIVER;

typedef eMMC_PACK0 struct _eMMC_RPMB_DATA
{
   U16 u16_req_rsp;
   U16 u16_result;
   U16 u16_blk_cnt;
   U16 u16_addr;
   U32 u32_writecnt;
   U8  u8_nonce[16];
   U8  u8_data[256];
   U8  u8_auth_key[32];
   U8  u8_stuff[196];
}eMMC_PACK1 eMMC_RPMB_DATA;

typedef eMMC_PACK0 struct _eMMC_TEST_ALIGN_PACK {

	U8	u8_0;
	U16	u16_0;
	U32	u32_0, u32_1;

} eMMC_PACK1 eMMC_TEST_ALIGN_PACK_t;

extern U8 g_eMMCDrv_array[];

//===========================================================
// exposed APIs
//===========================================================
#include "../api/drv_eMMC.h"

//===========================================================
// internal used functions
//===========================================================
#include "eMMC_utl.h"
#include "eMMC_hal.h"

#define eMMC_RPMB_REQ_AUTH_KEY         0x01
#define eMMC_RPMB_REQ_WRITE_CNT_VAL    0x02
#define eMMC_RPMB_REQ_AUTH_DATA_W      0x03
#define eMMC_RPMB_REQ_AUTH_DATA_R      0x04
#define eMMC_RPMB_REQ_RESULT_R         0x05

#endif // eMMC_DRIVER_H

