//<MStar Software>
//******************************************************************************
// MStar Software
// Copyright (c) 2010 - 2012 MStar Semiconductor, Inc. All rights reserved.
// All software, firmware and related documentation herein ("MStar Software") are
// intellectual property of MStar Semiconductor, Inc. ("MStar") and protected by
// law, including, but not limited to, copyright law and international treaties.
// Any use, modification, reproduction, retransmission, or republication of all
// or part of MStar Software is expressly prohibited, unless prior written
// permission has been granted by MStar.
//
// By accessing, browsing and/or using MStar Software, you acknowledge that you
// have read, understood, and agree, to be bound by below terms ("Terms") and to
// comply with all applicable laws and regulations:
//
// 1. MStar shall retain any and all right, ownership and interest to MStar
//    Software and any modification/derivatives thereof.
//    No right, ownership, or interest to MStar Software and any
//    modification/derivatives thereof is transferred to you under Terms.
//
// 2. You understand that MStar Software might include, incorporate or be
//    supplied together with third party`s software and the use of MStar
//    Software may require additional licenses from third parties.
//    Therefore, you hereby agree it is your sole responsibility to separately
//    obtain any and all third party right and license necessary for your use of
//    such third party`s software.
//
// 3. MStar Software and any modification/derivatives thereof shall be deemed as
//    MStar`s confidential information and you agree to keep MStar`s
//    confidential information in strictest confidence and not disclose to any
//    third party.
//
// 4. MStar Software is provided on an "AS IS" basis without warranties of any
//    kind. Any warranties are hereby expressly disclaimed by MStar, including
//    without limitation, any warranties of merchantability, non-infringement of
//    intellectual property rights, fitness for a particular purpose, error free
//    and in conformity with any international standard.  You agree to waive any
//    claim against MStar for any loss, damage, cost or expense that you may
//    incur related to your use of MStar Software.
//    In no event shall MStar be liable for any direct, indirect, incidental or
//    consequential damages, including without limitation, lost of profit or
//    revenues, lost or damage of data, and unauthorized system use.
//    You agree that this Section 4 shall still apply without being affected
//    even if MStar Software has been modified by MStar in accordance with your
//    request or instruction for your use, except otherwise agreed by both
//    parties in writing.
//
// 5. If requested, MStar may from time to time provide technical supports or
//    services in relation with MStar Software to you for your use of
//    MStar Software in conjunction with your or your customer`s product
//    ("Services").
//    You understand and agree that, except otherwise agreed by both parties in
//    writing, Services are provided on an "AS IS" basis and the warranty
//    disclaimer set forth in Section 4 above shall apply.
//
// 6. Nothing contained herein shall be construed as by implication, estoppels
//    or otherwise:
//    (a) conferring any license or right to use MStar name, trademark, service
//        mark, symbol or any other identification;
//    (b) obligating MStar or any of its affiliates to furnish any person,
//        including without limitation, you and your customers, any assistance
//        of any kind whatsoever, or any information; or
//    (c) conferring any license or right under any intellectual property right.
//
// 7. These terms shall be governed by and construed in accordance with the laws
//    of Taiwan, R.O.C., excluding its conflict of law rules.
//    Any and all dispute arising out hereof or related hereto shall be finally
//    settled by arbitration referred to the Chinese Arbitration Association,
//    Taipei in accordance with the ROC Arbitration Law and the Arbitration
//    Rules of the Association by three (3) arbitrators appointed in accordance
//    with the said Rules.
//    The place of arbitration shall be in Taipei, Taiwan and the language shall
//    be English.
//    The arbitration award shall be final and binding to both parties.
//
//******************************************************************************
//<MStar Software>
//=======================================================================
//  MStar Semiconductor - Unified eMMC Driver
//
//  eMMC_platform.c - Storage Team, 2011/02/25
//
//  Design Notes: defines common platform-dependent functions.
//
//    1. 2012/05/02 - support A3 uboot platform
//
//=======================================================================
#include "../../inc/common/eMMC.h"
#if defined(UNIFIED_eMMC_DRIVER) && UNIFIED_eMMC_DRIVER

//=============================================================
U32 eMMC_hw_timer_delay(U32 u32us)
{
	#if 0
    int i = 0;

	for (i = 0; i < (u32us<<2); i++)
	{
		#if 1
		 int j = 0, tmp;
		for (j = 0; j < 0x2; j++)
		{
			tmp = j;
		}
		#endif
	}
	#else
    //udelay((u32us>>1)+(u32us>>4)+1);
    //udelay(u32us * 24);
    U32 u32HWTimer = 0;
	volatile U16 u16TimerLow = 0;
	volatile U16 u16TimerHigh = 0;

	// reset HW timer
	REG_FCIE_W(TIMER0_MAX_LOW, 0xFFFF);
	REG_FCIE_W(TIMER0_MAX_HIGH, 0xFFFF);
	REG_FCIE_W(TIMER0_ENABLE, 0);

	// start HW timer
	REG_FCIE_SETBIT(TIMER0_ENABLE, 0x0001);

	while( u32HWTimer < 12*u32us ) // wait for u32usTick micro seconds
	{
		REG_FCIE_R(TIMER0_CAP_LOW, u16TimerLow);
		REG_FCIE_R(TIMER0_CAP_HIGH, u16TimerHigh);

		u32HWTimer = (u16TimerHigh<<16) | u16TimerLow;
	}

	REG_FCIE_W(TIMER0_ENABLE, 0);
	#endif
	return u32us;
}


// use to performance test
U32 eMMC_hw_timer_start(void)
{
	return 0;
}

U32 eMMC_hw_timer_tick(void)
{
	// TIMER_FREERUN_32K  32 KHz
	// TIMER_FREERUN_XTAL 38.4 MHz,
	// counting down
	return 0;//HalTimerRead(TIMER_FREERUN_32K);
}

void *eMMC_memset (void *str, int c, unsigned int len)
{
	register char *st = str;

	while (len-- > 0)
		*st++ = c;
	return str;
}

void *eMMC_memcpy (void *destaddr, void const *srcaddr, unsigned int len)
{
	char *dest = destaddr;
	char const *src = srcaddr;

	while (len-- > 0)
		*dest++ = *src++;
	return destaddr;
}

int eMMC_memcmp(const void *cs,const void *ct,unsigned int count)
{
	const unsigned char *su1, *su2;
	int res = 0;

	for( su1 = cs, su2 = ct; 0 < count; ++su1, ++su2, count--)
		if ((res = *su1 - *su2) != 0)
			break;
	return res;
}


void *eMMC_get_RPMBContext_address(void)
{
	return (void*)(CONFIG_BOOTRAM_LOADADDR+0x20000);
}


U32 eMMC_pads_switch(U8 u8Mode)
{
	eMMC_DRIVER *g_eMMCDrv = (eMMC_DRIVER *)g_eMMCDrv_array;

	g_eMMCDrv->u8_PadType = u8Mode;

	// chiptop
	REG_FCIE_CLRBIT(reg_chiptop_0x5A, BIT_SD_CONFIG);
	REG_FCIE_CLRBIT(reg_chiptop_0x6F, BIT_NAND_MODE);
	REG_FCIE_CLRBIT(reg_chiptop_0x6E, BIT_EMMC_CONFIG_MSK);
	REG_FCIE_SETBIT(reg_chiptop_0x6E, BIT_EMMC_CONFIG_EMMC_MODE_1);
	REG_FCIE_CLRBIT(reg_chiptop_0x50, BIT_ALL_PAD_IN);

	// fcie
	REG_FCIE_CLRBIT(FCIE_DDR_MODE, BIT_FALL_LATCH|BIT_PAD_IN_SEL_SD|BIT_CLK2_SEL|BIT_32BIT_MACRO_EN|BIT_DDR_EN|BIT_8BIT_MACRO_EN);

	// emmc_pll
	REG_FCIE_CLRBIT(reg_emmcpll_0x09, BIT0);
	REG_FCIE_CLRBIT(reg_emmcpll_0x1a, BIT10|BIT5|BIT4);
	REG_FCIE_CLRBIT(reg_emmcpll_0x1c, BIT8|BIT9);
	REG_FCIE_CLRBIT(reg_emmcpll_0x20, BIT9|BIT10);
	REG_FCIE_CLRBIT(reg_emmcpll_0x63, BIT0);
	REG_FCIE_CLRBIT(reg_emmcpll_0x68, BIT0|BIT1);
	REG_FCIE_CLRBIT(reg_emmcpll_0x69, BIT7|BIT6|BIT5|BIT4|BIT3);
	REG_FCIE_CLRBIT(reg_emmcpll_0x6a, BIT0|BIT1);
	REG_FCIE_W(reg_emmcpll_0x6b, 0x0000);
	REG_FCIE_CLRBIT(reg_emmcpll_0x6d, BIT0);
	REG_FCIE_CLRBIT(reg_emmcpll_0x70, BIT11|BIT10|BIT8);
	REG_FCIE_CLRBIT(reg_emmcpll_0x7f, BIT10|BIT9|BIT8|BIT3|BIT2|BIT1);

	switch (u8Mode) {

	case FCIE_eMMC_BYPASS:

		// fcie
		REG_FCIE_SETBIT(FCIE_DDR_MODE,BIT_PAD_IN_SEL_SD|BIT_FALL_LATCH|BIT_CLK2_SEL);

		// emmc_pll
		REG_FCIE_SETBIT(reg_emmcpll_0x1a, BIT10);
		REG_FCIE_W(reg_emmcpll_0x71, 0xFFFF);
		REG_FCIE_W(reg_emmcpll_0x73, 0xFFFF);

		break;

	case FCIE_eMMC_SDR:

		// fcie
		REG_FCIE_SETBIT(FCIE_DDR_MODE, BIT_8BIT_MACRO_EN);

		// emmc_pll
		REG_FCIE_SETBIT(reg_emmcpll_0x1c, BIT9);
		REG_FCIE_SETBIT(reg_emmcpll_0x68, BIT0);
		REG_FCIE_W(reg_emmcpll_0x71, 0xFFFF);
		REG_FCIE_W(reg_emmcpll_0x73, 0xFFFF);

		break;

	case FCIE_eMMC_DDR:

		// fcie
		REG_FCIE_SETBIT(FCIE_DDR_MODE, BIT_DDR_EN|BIT_8BIT_MACRO_EN);

		// emmc_pll
		REG_FCIE_SETBIT(reg_emmcpll_0x1c, BIT9);
		REG_FCIE_SETBIT(reg_emmcpll_0x68, BIT0);
		REG_FCIE_SETBIT(reg_emmcpll_0x6a, 2<<0);
		REG_FCIE_W(reg_emmcpll_0x6b, 0x0113);
		REG_FCIE_SETBIT(reg_emmcpll_0x6d, BIT0);
		REG_FCIE_W(reg_emmcpll_0x71, 0xFFFF);
		REG_FCIE_W(reg_emmcpll_0x73, 0xFFFF);

		break;

	case FCIE_eMMC_HS200:

		// fcie
		REG_FCIE_SETBIT(FCIE_DDR_MODE, BIT_32BIT_MACRO_EN);

		// emmc_pll
		REG_FCIE_CLRBIT(reg_emmcpll_0x03, 0x0FFF);
		REG_FCIE_SETBIT(reg_emmcpll_0x1a, BIT5|BIT4);
		REG_FCIE_SETBIT(reg_emmcpll_0x1c, BIT8);
		REG_FCIE_SETBIT(reg_emmcpll_0x20, BIT9|BIT10);
		REG_FCIE_SETBIT(reg_emmcpll_0x68, BIT0);
		REG_FCIE_SETBIT(reg_emmcpll_0x69, 4<<4);
		REG_FCIE_SETBIT(reg_emmcpll_0x6a, 2<<0);
		REG_FCIE_W(reg_emmcpll_0x6b, 0x0213);
		REG_FCIE_SETBIT(reg_emmcpll_0x70, BIT8);
		REG_FCIE_W(reg_emmcpll_0x71, 0xF800);
		REG_FCIE_W(reg_emmcpll_0x73, 0xFD00);
		#if defined(ENABLE_eMMC_AFIFO) && ENABLE_eMMC_AFIFO
		REG_FCIE_SETBIT(reg_emmcpll_0x70, BIT10|BIT11);
		#endif

		break;

	case FCIE_eMMC_HS400:

		// fcie
		REG_FCIE_SETBIT(FCIE_DDR_MODE, BIT_32BIT_MACRO_EN|BIT_DDR_EN);

		// emmc_pll
		REG_FCIE_CLRBIT(reg_emmcpll_0x03, 0x0F0F);
		REG_FCIE_SETBIT(reg_emmcpll_0x09, BIT0);
		REG_FCIE_SETBIT(reg_emmcpll_0x1a, BIT5|BIT4);
		REG_FCIE_SETBIT(reg_emmcpll_0x1c, BIT8);
		REG_FCIE_SETBIT(reg_emmcpll_0x20, BIT10);
		REG_FCIE_SETBIT(reg_emmcpll_0x63, BIT0);
		REG_FCIE_SETBIT(reg_emmcpll_0x68, BIT0|BIT1);
		REG_FCIE_SETBIT(reg_emmcpll_0x69, 6<<4);
		REG_FCIE_SETBIT(reg_emmcpll_0x6a, 2<<0);
		REG_FCIE_W(reg_emmcpll_0x6b, 0x0113);
		REG_FCIE_SETBIT(reg_emmcpll_0x70, BIT8);
		REG_FCIE_W(reg_emmcpll_0x71, 0xF800);
		REG_FCIE_W(reg_emmcpll_0x73, 0xFD00);
		#if defined(ENABLE_eMMC_AFIFO) && ENABLE_eMMC_AFIFO
		REG_FCIE_SETBIT(reg_emmcpll_0x70, BIT10|BIT11);
		REG_FCIE_SETBIT(reg_emmcpll_0x7f, BIT2);
		#endif

		break;

	default:
		return eMMC_ST_ERR_PARAMETER;
		break;
	}

	// 8 bits macro reset + 32 bits macro reset
	REG_FCIE_CLRBIT(reg_emmcpll_0x6f, BIT0|BIT1);
	REG_FCIE_SETBIT(reg_emmcpll_0x6f, BIT0|BIT1); // 8 bits macro reset + 32 bits macro reset

	return eMMC_ST_SUCCESS;
}

U16 u16_OldPLLClkParam = 0xFFFF;

U32 eMMC_pll_setting(U16 u16_ClkParam)
{
    U32 u32_value_reg_emmc_pll_pdiv;

    if(u16_ClkParam == u16_OldPLLClkParam)
        return eMMC_ST_SUCCESS;
    else
        u16_OldPLLClkParam = u16_ClkParam;

    // 1. reset emmc pll
    REG_FCIE_SETBIT(reg_emmc_pll_reset,BIT0);
    REG_FCIE_CLRBIT(reg_emmc_pll_reset,BIT0);

    // 2. synth clock
    switch(u16_ClkParam)
    {
        case eMMC_PLL_CLK_200M: // 200M
            REG_FCIE_CLRBIT(reg_ddfset_23_16,0xffff);
            REG_FCIE_CLRBIT(reg_ddfset_15_00,0xffff);
        #if 0
            REG_FCIE_SETBIT(reg_ddfset_23_16,0x22); // 20xMHz
            REG_FCIE_SETBIT(reg_ddfset_15_00,0x8F5C);
        #else
            REG_FCIE_SETBIT(reg_ddfset_23_16,0x24); // 195MHz
            REG_FCIE_SETBIT(reg_ddfset_15_00,0x03D8);
        #endif
            u32_value_reg_emmc_pll_pdiv =1;// PostDIV: 2
            break;

        case eMMC_PLL_CLK_160M:
            REG_FCIE_CLRBIT(reg_ddfset_23_16,0xffff);
            REG_FCIE_CLRBIT(reg_ddfset_15_00,0xffff);
            REG_FCIE_SETBIT(reg_ddfset_23_16,0x2B);
            REG_FCIE_SETBIT(reg_ddfset_15_00,0x3333);
            u32_value_reg_emmc_pll_pdiv =1;
            break;

        case eMMC_PLL_CLK_140M:
            REG_FCIE_CLRBIT(reg_ddfset_23_16,0xffff);
            REG_FCIE_CLRBIT(reg_ddfset_15_00,0xffff);
            REG_FCIE_SETBIT(reg_ddfset_23_16,0x31);
            REG_FCIE_SETBIT(reg_ddfset_15_00,0x5F15);
            u32_value_reg_emmc_pll_pdiv = 1;
            break;

        case eMMC_PLL_CLK_120M:
            REG_FCIE_CLRBIT(reg_ddfset_23_16,0xffff);
            REG_FCIE_CLRBIT(reg_ddfset_15_00,0xffff);
            REG_FCIE_SETBIT(reg_ddfset_23_16,0x39);
            REG_FCIE_SETBIT(reg_ddfset_15_00,0x9999);
            u32_value_reg_emmc_pll_pdiv = 1;
            break;

        case eMMC_PLL_CLK_100M:
            REG_FCIE_CLRBIT(reg_ddfset_23_16,0xffff);
            REG_FCIE_CLRBIT(reg_ddfset_15_00,0xffff);
            REG_FCIE_SETBIT(reg_ddfset_23_16,0x45);
            REG_FCIE_SETBIT(reg_ddfset_15_00,0x1EB8);
            u32_value_reg_emmc_pll_pdiv = 1;
            break;

        case eMMC_PLL_CLK__86M:
            REG_FCIE_CLRBIT(reg_ddfset_23_16,0xffff);
            REG_FCIE_CLRBIT(reg_ddfset_15_00,0xffff);
            REG_FCIE_SETBIT(reg_ddfset_23_16,0x28);
            REG_FCIE_SETBIT(reg_ddfset_15_00,0x2FA0);
            u32_value_reg_emmc_pll_pdiv = 2;
            break;

        case eMMC_PLL_CLK__80M:
            REG_FCIE_CLRBIT(reg_ddfset_23_16,0xffff);
            REG_FCIE_CLRBIT(reg_ddfset_15_00,0xffff);
            REG_FCIE_SETBIT(reg_ddfset_23_16,0x2B);
            REG_FCIE_SETBIT(reg_ddfset_15_00,0x3333);
            u32_value_reg_emmc_pll_pdiv = 2;
            break;

        case eMMC_PLL_CLK__72M:
            REG_FCIE_CLRBIT(reg_ddfset_23_16,0xffff);
            REG_FCIE_CLRBIT(reg_ddfset_15_00,0xffff);
            REG_FCIE_SETBIT(reg_ddfset_23_16,0x30);
            REG_FCIE_SETBIT(reg_ddfset_15_00,0x0000);
            u32_value_reg_emmc_pll_pdiv = 2;
            break;

        case eMMC_PLL_CLK__62M:
            REG_FCIE_CLRBIT(reg_ddfset_23_16,0xffff);
            REG_FCIE_CLRBIT(reg_ddfset_15_00,0xffff);
            REG_FCIE_SETBIT(reg_ddfset_23_16,0x37);
            REG_FCIE_SETBIT(reg_ddfset_15_00,0xBDEF);
            u32_value_reg_emmc_pll_pdiv = 2;
            break;

        case eMMC_PLL_CLK__52M:
            REG_FCIE_CLRBIT(reg_ddfset_23_16,0xffff);
            REG_FCIE_CLRBIT(reg_ddfset_15_00,0xffff);
            REG_FCIE_SETBIT(reg_ddfset_23_16,0x42);
            REG_FCIE_SETBIT(reg_ddfset_15_00,0x7627);
            u32_value_reg_emmc_pll_pdiv = 2;
            break;

        case eMMC_PLL_CLK__48M:
            REG_FCIE_CLRBIT(reg_ddfset_23_16,0xffff);
            REG_FCIE_CLRBIT(reg_ddfset_15_00,0xffff);
            REG_FCIE_SETBIT(reg_ddfset_23_16,0x48);
            REG_FCIE_SETBIT(reg_ddfset_15_00,0x0000);
            u32_value_reg_emmc_pll_pdiv = 2;
            break;

        case eMMC_PLL_CLK__40M:
            REG_FCIE_CLRBIT(reg_ddfset_23_16,0xffff);
            REG_FCIE_CLRBIT(reg_ddfset_15_00,0xffff);
            REG_FCIE_SETBIT(reg_ddfset_23_16,0x2B);
            REG_FCIE_SETBIT(reg_ddfset_15_00,0x3333);
            u32_value_reg_emmc_pll_pdiv = 4;
            break;

        case eMMC_PLL_CLK__36M:
            REG_FCIE_CLRBIT(reg_ddfset_23_16,0xffff);
            REG_FCIE_CLRBIT(reg_ddfset_15_00,0xffff);
            REG_FCIE_SETBIT(reg_ddfset_23_16,0x30);
            REG_FCIE_SETBIT(reg_ddfset_15_00,0x0000);
            u32_value_reg_emmc_pll_pdiv = 4;
            break;

        case eMMC_PLL_CLK__32M:
            REG_FCIE_CLRBIT(reg_ddfset_23_16,0xffff);
            REG_FCIE_CLRBIT(reg_ddfset_15_00,0xffff);
            REG_FCIE_SETBIT(reg_ddfset_23_16,0x36);
            REG_FCIE_SETBIT(reg_ddfset_15_00,0x0000);
            u32_value_reg_emmc_pll_pdiv = 4;
            break;

        case eMMC_PLL_CLK__27M:
            REG_FCIE_CLRBIT(reg_ddfset_23_16,0xffff);
            REG_FCIE_CLRBIT(reg_ddfset_15_00,0xffff);
            REG_FCIE_SETBIT(reg_ddfset_23_16,0x40);
            REG_FCIE_SETBIT(reg_ddfset_15_00,0x0000);
            u32_value_reg_emmc_pll_pdiv = 4;
            break;

        case eMMC_PLL_CLK__20M:
            REG_FCIE_CLRBIT(reg_ddfset_23_16,0xffff);
            REG_FCIE_CLRBIT(reg_ddfset_15_00,0xffff);
            REG_FCIE_SETBIT(reg_ddfset_23_16,0x2B);
            REG_FCIE_SETBIT(reg_ddfset_15_00,0x3333);
            u32_value_reg_emmc_pll_pdiv = 7;
            break;

        default:
            return eMMC_ST_ERR_UNKNOWN_CLK;
            break;
    }

    // 3. VCO clock ( loop N = 4 )
    REG_FCIE_CLRBIT(reg_emmcpll_fbdiv,0xffff);
    REG_FCIE_SETBIT(reg_emmcpll_fbdiv,0x6);

    // 4. 1X clock
    REG_FCIE_CLRBIT(reg_emmcpll_pdiv,BIT2|BIT1|BIT0);
    REG_FCIE_SETBIT(reg_emmcpll_pdiv,u32_value_reg_emmc_pll_pdiv);

    if(u16_ClkParam==eMMC_PLL_CLK__20M)
    {
        REG_FCIE_SETBIT(reg_emmc_pll_test, BIT10);
    }
    else
    {
        REG_FCIE_CLRBIT(reg_emmc_pll_test, BIT10);
    }

    eMMC_hw_timer_delay(HW_TIMER_DELAY_100us);

    return eMMC_ST_SUCCESS;
}

U16 u16_OldPLLDLLClkParam = 0xFFFF;

void HalEmmcPll_dll_setting(U16 u16_ClkParam)
{
    volatile U16 u16_reg;

    if(u16_ClkParam == u16_OldPLLDLLClkParam)
        return ;
    else
        u16_OldPLLDLLClkParam = u16_ClkParam;

    REG_FCIE_CLRBIT(reg_emmcpll_0x09, BIT0);

    // Reset eMMC_DLL
    REG_FCIE_SETBIT(REG_EMMC_PLL_RX30, BIT2);
    REG_FCIE_CLRBIT(REG_EMMC_PLL_RX30, BIT2);

    //DLL pulse width and phase
    REG_FCIE_W(REG_EMMC_PLL_RX01, 0x7F72);

    // DLL code
    REG_FCIE_W(REG_EMMC_PLL_RX32, 0xF200);

    // DLL calibration
    REG_FCIE_W(REG_EMMC_PLL_RX30, 0x3378);
    REG_FCIE_SETBIT(REG_EMMC_PLL_RX33, BIT15);

    // Wait 100us
    eMMC_hw_timer_delay(250);

    // Get hw dll0 code
    REG_FCIE_R(REG_EMMC_PLL_RX33, u16_reg);

    REG_FCIE_CLRBIT(REG_EMMC_PLL_RX34, (BIT10 - 1));
    // Set dw dll0 code
    REG_FCIE_SETBIT(REG_EMMC_PLL_RX34, u16_reg & 0x03FF);

    // Disable reg_hw_upcode_en
    REG_FCIE_CLRBIT(REG_EMMC_PLL_RX30, BIT9);

    // Clear reg_emmc_dll_test[7]
    REG_FCIE_CLRBIT(reg_emmcpll_0x02, BIT15);

    // Enable reg_rxdll_dline_en
    REG_FCIE_SETBIT(reg_emmcpll_0x09, BIT0);

}


U32 eMMC_clock_setting(U16 u16_ClkParam)
{
	U16 u16_Tmp;
	eMMC_DRIVER *g_eMMCDrv = (eMMC_DRIVER *)g_eMMCDrv_array;

	eMMC_PlatformResetPre();

	REG_FCIE_CLRBIT(FCIE_SD_MODE, BIT_SD_CLK_EN);

	REG_FCIE_CLRBIT(reg_ckg_fcie, BIT_FCIE_CLK_SRC_SEL);
	REG_FCIE_R(reg_ckg_fcie, u16_Tmp);
	eMMC_hw_timer_delay(7);
	REG_FCIE_CLRBIT(reg_ckg_fcie, BIT_FCIE_CLK_Gate|BIT_FCIE_CLK_MASK);

	if (u16_ClkParam & eMMC_PLL_FLAG) {

		eMMC_pll_setting(u16_ClkParam);

		if((g_eMMCDrv->u32_DrvFlag & (FCIE_FLAG_SPEED_HS400 | FCIE_FLAG_DDR_MODE)) ==
			(FCIE_FLAG_SPEED_HS400 | FCIE_FLAG_DDR_MODE)) {

			REG_FCIE_SETBIT(reg_ckg_fcie, BIT_FCIE_CLK_EMMC_PLL_2X<<2);
			REG_FCIE_SETBIT(reg_ckg_fcie, BIT_FCIE_CLK_SRC_SEL);

			HalEmmcPll_dll_setting(u16_ClkParam); // tuning DLL setting
		} else {
			REG_FCIE_SETBIT(reg_ckg_fcie, BIT_FCIE_CLK_EMMC_PLL_1X<<2);
			REG_FCIE_SETBIT(reg_ckg_fcie, BIT_FCIE_CLK_SRC_SEL);
		}
	} else {
		REG_FCIE_SETBIT(reg_ckg_fcie, u16_ClkParam<<2);
		REG_FCIE_SETBIT(reg_ckg_fcie, BIT_FCIE_CLK_SRC_SEL);
	}

	eMMC_PlatformResetPost();

	return eMMC_ST_SUCCESS;
}

void eMMC_set_WatchDog(U8 u8_IfEnable)
{
	u8_IfEnable = u8_IfEnable;
	// do nothing
}

void eMMC_reset_WatchDog(void)
{
	// do nothing
}

U32 eMMC_translate_DMA_address_Ex(U32 u32_DMAAddr, U32 u32_ByteCnt)
{
	u32_ByteCnt = u32_ByteCnt;
	#if 0
	flush_cache(u32_DMAAddr, u32_ByteCnt);
	#endif

	// Need to set bank 0x1006 16 bits addr 0x7A bit9 on to make below code work,
	// otherwise, always DMA to MIU0

	REG_FCIE_CLRBIT(FCIE_MMA_PRI_REG, BIT_MIU_SELECT_MASK);
	if (u32_DMAAddr < CONFIG_MIU0_BUSADDR)
		return (u32_DMAAddr & 0xFFFF);
	else if (u32_DMAAddr < CONFIG_MIU1_BUSADDR)
		return (u32_DMAAddr - CONFIG_MIU0_BUSADDR);
	else {
		REG_FCIE_SETBIT(FCIE_MMA_PRI_REG, BIT_MIU1_SELECT);
		return (u32_DMAAddr - CONFIG_MIU1_BUSADDR);
	}

	return u32_DMAAddr;
}

void eMMC_Invalidate_data_cache_buffer(U32 u32_addr, S32 s32_size)
{
	u32_addr = u32_addr;
	s32_size = s32_size;
//	flush_cache(u32_addr, s32_size);
}

void eMMC_flush_miu_pipe(void)
{
}

U32 eMMC_PlatformResetPre(void)
{
	if ((REG_FCIE(reg_ckg_fcie_syn) & 0x3) != BIT0 )
	{
		REG_FCIE_CLRBIT(reg_ckg_fcie_syn, 0x3);
		REG_FCIE_SETBIT(reg_ckg_fcie_syn, BIT0);
	}

	return eMMC_ST_SUCCESS;
}

U32 eMMC_PlatformResetPost(void)
{
	return eMMC_ST_SUCCESS;
}


U32 eMMC_PlatformInit(void)
{
	if ((REG_FCIE(reg_emmcpll_0x1a) & BIT0) == 0) {
		//eMMC_debug(0, 0, "eMMC Err: not 1.8V IO setting\n");
		REG_FCIE_SETBIT(reg_emmcpll_0x1a, BIT0);// 1.8V must set this bit
		REG_FCIE_SETBIT(reg_emmcpll_0x1a, BIT2); // atop patch
		REG_FCIE_CLRBIT(reg_emmcpll_0x45, 0xffff);// 1.8V must set this bit
		REG_FCIE_SETBIT(reg_emmcpll_0x45, 0xf<<8);// 1.8V must set this bit
	}

	eMMC_clock_setting(FCIE_SLOWEST_CLK);
	eMMC_pads_switch(FCIE_eMMC_BYPASS);

	return eMMC_ST_SUCCESS;
}

#endif
