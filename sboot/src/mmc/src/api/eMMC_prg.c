//<MStar Software>
//******************************************************************************
// MStar Software
// Copyright (c) 2010 - 2012 MStar Semiconductor, Inc. All rights reserved.
// All software, firmware and related documentation herein ("MStar Software") are
// intellectual property of MStar Semiconductor, Inc. ("MStar") and protected by
// law, including, but not limited to, copyright law and international treaties.
// Any use, modification, reproduction, retransmission, or republication of all
// or part of MStar Software is expressly prohibited, unless prior written
// permission has been granted by MStar.
//
// By accessing, browsing and/or using MStar Software, you acknowledge that you
// have read, understood, and agree, to be bound by below terms ("Terms") and to
// comply with all applicable laws and regulations:
//
// 1. MStar shall retain any and all right, ownership and interest to MStar
//    Software and any modification/derivatives thereof.
//    No right, ownership, or interest to MStar Software and any
//    modification/derivatives thereof is transferred to you under Terms.
//
// 2. You understand that MStar Software might include, incorporate or be
//    supplied together with third party`s software and the use of MStar
//    Software may require additional licenses from third parties.
//    Therefore, you hereby agree it is your sole responsibility to separately
//    obtain any and all third party right and license necessary for your use of
//    such third party`s software.
//
// 3. MStar Software and any modification/derivatives thereof shall be deemed as
//    MStar`s confidential information and you agree to keep MStar`s
//    confidential information in strictest confidence and not disclose to any
//    third party.
//
// 4. MStar Software is provided on an "AS IS" basis without warranties of any
//    kind. Any warranties are hereby expressly disclaimed by MStar, including
//    without limitation, any warranties of merchantability, non-infringement of
//    intellectual property rights, fitness for a particular purpose, error free
//    and in conformity with any international standard.  You agree to waive any
//    claim against MStar for any loss, damage, cost or expense that you may
//    incur related to your use of MStar Software.
//    In no event shall MStar be liable for any direct, indirect, incidental or
//    consequential damages, including without limitation, lost of profit or
//    revenues, lost or damage of data, and unauthorized system use.
//    You agree that this Section 4 shall still apply without being affected
//    even if MStar Software has been modified by MStar in accordance with your
//    request or instruction for your use, except otherwise agreed by both
//    parties in writing.
//
// 5. If requested, MStar may from time to time provide technical supports or
//    services in relation with MStar Software to you for your use of
//    MStar Software in conjunction with your or your customer`s product
//    ("Services").
//    You understand and agree that, except otherwise agreed by both parties in
//    writing, Services are provided on an "AS IS" basis and the warranty
//    disclaimer set forth in Section 4 above shall apply.
//
// 6. Nothing contained herein shall be construed as by implication, estoppels
//    or otherwise:
//    (a) conferring any license or right to use MStar name, trademark, service
//        mark, symbol or any other identification;
//    (b) obligating MStar or any of its affiliates to furnish any person,
//        including without limitation, you and your customers, any assistance
//        of any kind whatsoever, or any information; or
//    (c) conferring any license or right under any intellectual property right.
//
// 7. These terms shall be governed by and construed in accordance with the laws
//    of Taiwan, R.O.C., excluding its conflict of law rules.
//    Any and all dispute arising out hereof or related hereto shall be finally
//    settled by arbitration referred to the Chinese Arbitration Association,
//    Taipei in accordance with the ROC Arbitration Law and the Arbitration
//    Rules of the Association by three (3) arbitrators appointed in accordance
//    with the said Rules.
//    The place of arbitration shall be in Taipei, Taiwan and the language shall
//    be English.
//    The arbitration award shall be final and binding to both parties.
//
//******************************************************************************
//<MStar Software>
//#include <config.h>
//#include <common.h>
//#include <command.h>
//#include <mmc.h>
//#include <part.h>
//#include <malloc.h>
//#include <asm/errno.h>

#include "../../inc/common/eMMC.h"


#if defined(UNIFIED_eMMC_DRIVER) && UNIFIED_eMMC_DRIVER
#if 0
static U32 eMMC_ReadPartitionInfo_Ex(void);
static U32 eMMC_GetPartitionIndex(U16 u16_PartType,
							               U32 u32_LogicIdx,
							               volatile U16 *pu16_PartIdx);
#endif
#define eMMC_NOT_READY_MARK    ~(('e'<<24)|('M'<<16)|('M'<<8)|'C')
static U32 sgu32_IfReadyGuard = eMMC_NOT_READY_MARK;

eMMC_ALIGN0 U8 g_eMMCDrv_array[512] eMMC_ALIGN1={0};

//========================================================
U32 eMMC_LoadImages(U32 u32_Addr, U32 u32_ByteCnt, U32 u32_BlkAddr)
{
	U32 u32_err;
    #if defined(eMMC_ROM_ReLoadImages) && eMMC_ROM_ReLoadImages
	U32 (*pf)(U32,U32,U32);

	pf = (void*)eMMC_ROM_LoadImages_Addr();

    if(u32_Addr<=(BOOT_SRAM_START_ADDRESS+Total_SRAM_SIZE))
    {
		u32_err = (*pf)(u32_Addr,u32_ByteCnt,1);
		if(u32_err != eMMC_ST_SUCCESS)
		{
			goto LABEL_LOAD_IMAGE_END;
		}
    }
	else
	{
		//eMMC Identify
		u32_err = eMMC_CheckIfReady();

		if(u32_err != eMMC_ST_SUCCESS)
		{
			goto LABEL_LOAD_IMAGE_END;
		}

		u32_err = eMMC_ReadBootPart((U8*)u32_Addr, u32_ByteCnt,u32_BlkAddr);
		if(u32_err != eMMC_ST_SUCCESS)
		{
			goto LABEL_LOAD_IMAGE_END;
		}
	}

	// --------------------------------
    return eMMC_ST_SUCCESS;
	LABEL_LOAD_IMAGE_END:
	return u32_err;
    #else
	//eMMC Identify
	u32_err = eMMC_CheckIfReady();

    if(u32_err != eMMC_ST_SUCCESS)
    {
	    goto LABEL_LOAD_IMAGE_END;
    }

	u32_err = eMMC_ReadBootPart((U8*)u32_Addr, u32_ByteCnt,u32_BlkAddr);
    if(u32_err != eMMC_ST_SUCCESS)
    {
	    goto LABEL_LOAD_IMAGE_END;
    }
	// --------------------------------
    return eMMC_ST_SUCCESS;
	LABEL_LOAD_IMAGE_END:
    while(1);
	return u32_err;
	#endif
}

//========================================================

U32 eMMC_Init_Device_Ex(void)
{
	U32 u32_err;
	U8  u8_retry = 0;

	eMMC_DRIVER *g_eMMCDrv = (eMMC_DRIVER *)g_eMMCDrv_array;

	// ---------------------------------
	// init platform & FCIE
	g_eMMCDrv->u16_RCA=1;
	eMMC_PlatformInit();

LABEL_INIT_START:

	eMMC_RST_L();
	eMMC_hw_timer_delay(HW_TIMER_DELAY_1ms);
	g_eMMCDrv->u8_BUS_WIDTH = BIT_SD_DATA_WIDTH_1;
	g_eMMCDrv->u16_Reg10_Mode = BIT_SD_DEFAULT_MODE_REG;
	eMMC_RST_H();
	eMMC_hw_timer_delay(HW_TIMER_DELAY_1ms);

	u32_err = eMMC_FCIE_Init();
	if (u32_err)
		goto  LABEL_INIT_END;

	// ---------------------------------
	// init eMMC device
	u32_err = eMMC_Identify();
	if (eMMC_ST_SUCCESS != u32_err) {
		if (u8_retry < 10) {
			u8_retry++;
			goto LABEL_INIT_START;
		}

		goto  LABEL_INIT_END;
	}

	//REG_FCIE_CLRBIT(FCIE_BOOT_CONFIG, BIT_NAND_BOOT_MODE_EN);
	eMMC_clock_setting(FCIE_SLOW_CLK);

	// setup eMMC device
	// CMD7
	u32_err = eMMC_CMD3_CMD7(g_eMMCDrv->u16_RCA, 7);
	if (eMMC_ST_SUCCESS != u32_err)
		goto  LABEL_INIT_END;

	u32_err = eMMC_ExtCSD_Config();
	if(eMMC_ST_SUCCESS != u32_err)
		return u32_err;

	u32_err = eMMC_SetBusWidth(8, 0);
	if (eMMC_ST_SUCCESS != u32_err)
		goto  LABEL_INIT_END;

	u32_err = eMMC_SetBusSpeed(eMMC_SPEED_HIGH);
	if (eMMC_ST_SUCCESS != u32_err)
		goto  LABEL_INIT_END;

	eMMC_clock_setting(BIT_FCIE_CLK_48M);

	sgu32_IfReadyGuard = ~eMMC_NOT_READY_MARK;

LABEL_INIT_END:

	return u32_err;
}

static U32 eMMC_Init_Ex(void)
{
	U32 u32_err = eMMC_ST_SUCCESS;

	u32_err = eMMC_Init_Device_Ex();
	if (eMMC_ST_SUCCESS != u32_err)
		goto  LABEL_INIT_END;

	u32_err = eMMC_FCIE_ChooseSpeedMode();
	if ((eMMC_ST_SUCCESS != u32_err) &&
		(eMMC_ST_ERR_DDRT_CHKSUM != u32_err) &&
		(eMMC_ST_ERR_DDRT_NONA != u32_err))
		goto  LABEL_INIT_END;

        REG_FCIE_W(FCIE_DBG0, 0x3697);

	return eMMC_ST_SUCCESS;

LABEL_INIT_END:

	REG_FCIE_W(FCIE_DBG0, 0x2454);

	u32_err = eMMC_Init_Device_Ex();
	if (eMMC_ST_SUCCESS != u32_err)
	{
		REG_FCIE_W(FCIE_DBG0, 0x0014); //eMMC_ST_ERR_NOT_INIT
	}

	return u32_err;
}

U32 eMMC_CheckIfReady(void)
{
	U32 u32_err = 0;

	if (eMMC_NOT_READY_MARK != sgu32_IfReadyGuard)
		return eMMC_ST_SUCCESS;

	REG_FCIE_CLRBIT(FCIE_BOOT_CONFIG, BIT_NAND_BOOT_MODE_EN);

	u32_err = eMMC_Init_Ex();

	return u32_err;
}

U32 eMMC_ReadBootPart(U8* pu8_DataBuf, U32 u32_DataByteCnt, U32 u32_BlkAddr)
{
	U32 u32_err=0;
	U16 u16_SecCnt;

	// set Access Boot Partition 1
	#ifdef IP_FCIE_VERSION_5
    u32_err = eMMC_ModifyExtCSD(eMMC_ExtCSD_WByte, 179, BIT3|BIT0);
	#else
	u32_err = eMMC_ModifyExtCSD(eMMC_ExtCSD_WByte, 179, BIT6|BIT3|BIT0);
	#endif
	if(eMMC_ST_SUCCESS != u32_err)
	{
		return u32_err;
	}

	// read Boot Code
	u16_SecCnt = (u32_DataByteCnt>>eMMC_SECTOR_512BYTE_BITS) + ((u32_DataByteCnt&0x1FF)?1:0);

	u32_err = eMMC_ReadData_MIU(pu8_DataBuf, u16_SecCnt<<eMMC_SECTOR_512BYTE_BITS, u32_BlkAddr);
	if(eMMC_ST_SUCCESS != u32_err)
	{
		return u32_err;
	}


	// clear Access Boot Partition
	#ifdef IP_FCIE_VERSION_5
    u32_err = eMMC_ModifyExtCSD(eMMC_ExtCSD_WByte, 179, BIT3);
	#else
	u32_err = eMMC_ModifyExtCSD(eMMC_ExtCSD_WByte, 179, BIT6|BIT3);
	#endif
	if(eMMC_ST_SUCCESS != u32_err)
	{
		return u32_err;
	}

	return u32_err;
}

void eMMC_FCIE_Apply_Reg(void)
{
    int i;
	eMMC_DRIVER *g_eMMCDrv = (eMMC_DRIVER *)g_eMMCDrv_array;


    for(i = 0; i < g_eMMCDrv->TimingTable_G_t.u8_RegisterCnt; i ++)
    {
        if(g_eMMCDrv->TimingTable_G_t.RegSet[i].u16_OpCode == REG_OP_W)
        {
            REG_FCIE_W(RIU_BASE + g_eMMCDrv->TimingTable_G_t.RegSet[i].u32_RegAddress, g_eMMCDrv->TimingTable_G_t.RegSet[i].u16_RegValue);
        }
        else if(g_eMMCDrv->TimingTable_G_t.RegSet[i].u16_OpCode == REG_OP_CLRBIT)
        {
            REG_FCIE_CLRBIT(RIU_BASE + g_eMMCDrv->TimingTable_G_t.RegSet[i].u32_RegAddress, g_eMMCDrv->TimingTable_G_t.RegSet[i].u16_RegValue);
        }
        else if(g_eMMCDrv->TimingTable_G_t.RegSet[i].u16_OpCode == REG_OP_SETBIT)
        {
            REG_FCIE_CLRBIT(RIU_BASE + g_eMMCDrv->TimingTable_G_t.RegSet[i].u32_RegAddress, g_eMMCDrv->TimingTable_G_t.RegSet[i].u16_RegMask);

            REG_FCIE_SETBIT(RIU_BASE + g_eMMCDrv->TimingTable_G_t.RegSet[i].u32_RegAddress, g_eMMCDrv->TimingTable_G_t.RegSet[i].u16_RegValue);
        }
    }

}

void eMMC_FCIE_SetATopTimingReg(void)
{
	eMMC_DRIVER *g_eMMCDrv = (eMMC_DRIVER *)g_eMMCDrv_array;


    if(g_eMMCDrv->TimingTable_t.Set[0].u8_Reg2Ch)
    {
       REG_FCIE_SETBIT(REG_ANL_SKEW4_INV, BIT_ANL_SKEW4_INV);
    }
    else
    {
       REG_FCIE_CLRBIT(REG_ANL_SKEW4_INV, BIT_ANL_SKEW4_INV);
    }
    REG_FCIE_CLRBIT(reg_emmcpll_0x03, BIT_SKEW4_MASK);
    REG_FCIE_SETBIT(reg_emmcpll_0x03, g_eMMCDrv->TimingTable_t.Set[0].u8_Skew4<<12);
}

// =======================================================
// u32_DataByteCnt: has to be 512B-boundary !
// =======================================================
#if 0
U32 eMMC_ReadData_CIFD(U8* pu8_DataBuf, U32 u32_DataByteCnt, U32 u32_BlkAddr)
{
	U32 u32_err;
	U16 u16_BlkCnt;

	u16_BlkCnt = 1;
    #ifdef IP_FCIE_VERSION_5
    eMMC_pads_switch_default();
    #endif
 	while(u32_DataByteCnt)
	{
	    u32_err = eMMC_CMD17_CIFD(u32_BlkAddr, pu8_DataBuf);

		if(eMMC_ST_SUCCESS != u32_err)
		{
			break;
		}

		u32_BlkAddr += u16_BlkCnt;
		pu8_DataBuf += u16_BlkCnt << eMMC_SECTOR_512BYTE_BITS;
		u32_DataByteCnt -= u16_BlkCnt << eMMC_SECTOR_512BYTE_BITS;
	}

	return u32_err;
}
#endif

// =======================================================
// u32_DataByteCnt: has to be 512B-boundary !
// =======================================================
U32 eMMC_ReadData_IMI(U8* pu8_DataBuf, U32 u32_DataByteCnt, U32 u32_BlkAddr)
{
    U32 u32_err;
    U16 u16_BlkCnt;

    u32_err = eMMC_CheckIfReady();
    if(eMMC_ST_SUCCESS != u32_err)
    {
    	REG_FCIE_W(FCIE_DBG0, (U16)(u32_err & 0xFFFF));
        return u32_err;
    }

    // read data
    // first 512 bytes, special handle if not cache line aligned
    u16_BlkCnt = (u32_DataByteCnt>>eMMC_SECTOR_512BYTE_BITS) + ((u32_DataByteCnt&0x1FF)?1:0);
    u32_err = eMMC_CMD18_IMI(u32_BlkAddr, pu8_DataBuf, u16_BlkCnt);

    if(eMMC_ST_SUCCESS != u32_err)
    {
    	REG_FCIE_W(FCIE_DBG0, (U16)(u32_err & 0xFFFF));
        return u32_err;
    }

    return eMMC_ST_SUCCESS;
}

U32 eMMC_LoadTimingTable(U8 u8_PadType)
{
	U32 u32_err = eMMC_ST_SUCCESS;
	eMMC_DRIVER *g_eMMCDrv = (eMMC_DRIVER *)g_eMMCDrv_array;
	U32 u32_ChkSum = 0;

	u32_err = eMMC_CMD18_IMI(eMMC_HS400EXTTABLE_BLK_0, gau8_eMMC_SectorBuf, 1);
	if (u32_err)
		goto LABEL_END;

	memcpy((U8*)&g_eMMCDrv->TimingTable_G_t, gau8_eMMC_SectorBuf, sizeof(g_eMMCDrv->TimingTable_G_t));

	if (FCIE_eMMC_HS400 == u8_PadType) {
		u32_ChkSum = eMMC_ChkSum((U8*)&(g_eMMCDrv->TimingTable_G_t.u32_VerNo),
                                     (sizeof(g_eMMCDrv->TimingTable_G_t) - sizeof(U32)/*checksum*/));

		if (u32_ChkSum != g_eMMCDrv->TimingTable_G_t.u32_ChkSum) {
			u32_err = eMMC_ST_ERR_DDRT_CHKSUM;
			goto LABEL_END;
		}

		if (u32_ChkSum == 0) {
			u32_err = eMMC_ST_ERR_DDRT_CHKSUM;
			goto LABEL_END;
		}

		if (eMMC_memcmp(g_eMMCDrv->TimingTable_G_t.au8_CID, g_eMMCDrv->au8_CID, eMMC_MAX_RSP_BYTE_CNT - 1))
			u32_err = eMMC_ST_ERR_DDRT_NONA;

	} else if (FCIE_eMMC_HS200 == u8_PadType) {
		u32_err = eMMC_CMD18_IMI(eMMC_HS200TABLE_BLK_0, gau8_eMMC_SectorBuf, 1);

		if (u32_err)
			goto LABEL_END;

		eMMC_memcpy((U8*)&g_eMMCDrv->TimingTable_t, gau8_eMMC_SectorBuf,
			sizeof(g_eMMCDrv->TimingTable_t));

		u32_ChkSum = eMMC_ChkSum((U8*)&g_eMMCDrv->TimingTable_t,
			sizeof(g_eMMCDrv->TimingTable_t)-eMMC_TIMING_TABLE_CHKSUM_OFFSET);

		if (u32_ChkSum != g_eMMCDrv->TimingTable_t.u32_ChkSum) {
			u32_err = eMMC_ST_ERR_DDRT_CHKSUM;
			goto LABEL_END;
		}

		if (u32_ChkSum ==0) {
			u32_err = eMMC_ST_ERR_DDRT_CHKSUM;
			goto LABEL_END;
		}

		if (eMMC_memcmp(g_eMMCDrv->TimingTable_G_t.au8_CID, g_eMMCDrv->au8_CID, eMMC_MAX_RSP_BYTE_CNT - 1))
			u32_err = eMMC_ST_ERR_DDRT_NONA;
	} else
		u32_err = eMMC_ST_ERR_DDRT_NONA;

 LABEL_END:

	return u32_err;

}

U32 eMMC_FCIE_EnableFastMode_Ex(U8 u8_PadType)
{
	U32 u32_err = eMMC_ST_SUCCESS;
	eMMC_DRIVER *g_eMMCDrv = (eMMC_DRIVER *)g_eMMCDrv_array;

	switch (u8_PadType) {

	case FCIE_eMMC_HS200:

		u32_err = eMMC_SetBusWidth(8, 0);
		if (u32_err)
			return u32_err;

		u32_err = eMMC_SetBusSpeed(eMMC_SPEED_HS200);
		if (u32_err)
			return u32_err;

		eMMC_clock_setting(g_eMMCDrv->TimingTable_t.Set[0].u8_Clk);

		break;

	case FCIE_eMMC_HS400:

		u32_err = eMMC_SetBusWidth(8, 1);// enable DDR
		if (u32_err)
			return u32_err;

		if ((g_eMMCDrv->au8_CID[1] == 0x15) ||
		    (g_eMMCDrv->au8_CID[1] == 0x11)) {
				g_eMMCDrv->u8_ECSD185_HsTiming &= ~(0xF0);
				g_eMMCDrv->u8_ECSD185_HsTiming |= 4<<4;
		}

		u32_err = eMMC_SetBusSpeed(eMMC_SPEED_HS400);
		if (u32_err)
			return u32_err;

		eMMC_clock_setting(g_eMMCDrv->TimingTable_G_t.u32_Clk);

		break;
	}

	eMMC_pads_switch(u8_PadType);

	if (u8_PadType == FCIE_eMMC_HS400)
		eMMC_FCIE_Apply_Reg();

	if(u8_PadType == FCIE_eMMC_HS200)
		eMMC_FCIE_SetATopTimingReg();

	return eMMC_ST_SUCCESS;
}

U32 eMMC_FCIE_EnableFastMode(U8 u8_PadType)
{
	U32 u32_err = eMMC_ST_SUCCESS;

	u32_err = eMMC_LoadTimingTable(u8_PadType);
	if (eMMC_ST_SUCCESS != u32_err)
		return u32_err;

	u32_err = eMMC_FCIE_EnableFastMode_Ex(u8_PadType);

	return u32_err;
}

U32 eMMC_FCIE_ChooseSpeedMode(void)
{
	U32 u32_err = eMMC_ST_SUCCESS;
	eMMC_DRIVER *g_eMMCDrv = (eMMC_DRIVER *)g_eMMCDrv_array;

	if (g_eMMCDrv->u8_ECSD196_DevType & eMMC_DEVTYPE_HS400_1_8V) {
		u32_err = eMMC_FCIE_EnableFastMode(FCIE_eMMC_HS400);
		if (u32_err)
			goto LABEL_END;
	} else if (g_eMMCDrv->u8_ECSD196_DevType & eMMC_DEVTYPE_HS200_1_8V) {
		u32_err = eMMC_FCIE_EnableFastMode(FCIE_eMMC_HS200);
		if (u32_err)
			goto LABEL_END;
	} else {
		u32_err = eMMC_ST_SUCCESS;
	}

LABEL_END:

	return u32_err;
}

U32 eMMC_ReadData_MIU(U8* pu8_DataBuf, U32 u32_DataByteCnt, U32 u32_BlkAddr)
{
	U32 u32_err;
	U16 u16_BlkCnt;

	// read data
	// first 512 bytes, special handle if not cache line aligned
	u16_BlkCnt = (u32_DataByteCnt>>eMMC_SECTOR_512BYTE_BITS) + ((u32_DataByteCnt&0x1FF)?1:0);
	u32_err = eMMC_CMD18_MIU(u32_BlkAddr, pu8_DataBuf, u16_BlkCnt);

	if(eMMC_ST_SUCCESS != u32_err)
	{
		REG_FCIE_W(FCIE_DBG0, (U16)(u32_err & 0xFFFF));
		return u32_err;
	}

	return eMMC_ST_SUCCESS;
}

U32 __ATTR_DRAM_CODE__ eMMC_WriteData_MIU(U8* pu8_DataBuf, U32 u32_DataByteCnt, U32 u32_BlkAddr)
{
	U32 u32_err;
	U16 u16_BlkCnt;

	// write data
	// first 512 bytes, special handle if not cache line aligned
    u16_BlkCnt = (u32_DataByteCnt>>eMMC_SECTOR_512BYTE_BITS) + ((u32_DataByteCnt&0x1FF)?1:0);
	u32_err = eMMC_CMD25_MIU(u32_BlkAddr, pu8_DataBuf, u16_BlkCnt);

	if(eMMC_ST_SUCCESS != u32_err)
	{
        while(1);
	}

	return eMMC_ST_SUCCESS;
}


void __ATTR_DRAM_CODE__ eMMC_GetCID(U8 *pu8_CID)
{
    eMMC_DRIVER *g_eMMCDrv = (eMMC_DRIVER *)g_eMMCDrv_array;
    U8 u8_i;

    for(u8_i=0;u8_i<15;u8_i++)
        pu8_CID[u8_i]= g_eMMCDrv->au8_CID[u8_i+1];
}

U32 eMMC_ChkSum(U8 *pu8_Data, U32 u32_ByteCnt)
{
    volatile U16 u16_Tmp;
    volatile U32 u32_Sum=0;
    for (u16_Tmp=0; u16_Tmp < u32_ByteCnt; u16_Tmp++)
       u32_Sum += pu8_Data[u16_Tmp];
    return (u32_Sum);
}


#if defined(CONFIG_DUAL_SYSTEM)
#define BOOT_MODE_TMP_BUF_ADDR  (CONFIG_UBOOT_LOADADDR)

U32 eMMC_CheckBootFlag(void)
{
    U32 u32_err;
    U8 bootflag = 0;

    u32_err = eMMC_CMD18_MIU(0x7880, (U32 *)BOOT_MODE_TMP_BUF_ADDR, 1);  //bootfalg partition addr = 0xF10000
    if(eMMC_ST_SUCCESS != u32_err)
    {
        while(1);
    }

    bootflag = *(U8 *)(BOOT_MODE_TMP_BUF_ADDR);
    if (0 == bootflag)  //bring up MBootBAK
    {
        return 0;
    }
    else  //bring up MBoot
    {
        return 1;
    }
}
#endif

#endif

