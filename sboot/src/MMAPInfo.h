//<MStar Software>
//******************************************************************************
// MStar Software
// Copyright (c) 2010 - 2012 MStar Semiconductor, Inc. All rights reserved.
// All software, firmware and related documentation herein ("MStar Software") are
// intellectual property of MStar Semiconductor, Inc. ("MStar") and protected by
// law, including, but not limited to, copyright law and international treaties.
// Any use, modification, reproduction, retransmission, or republication of all
// or part of MStar Software is expressly prohibited, unless prior written
// permission has been granted by MStar.
//
// By accessing, browsing and/or using MStar Software, you acknowledge that you
// have read, understood, and agree, to be bound by below terms ("Terms") and to
// comply with all applicable laws and regulations:
//
// 1. MStar shall retain any and all right, ownership and interest to MStar
//    Software and any modification/derivatives thereof.
//    No right, ownership, or interest to MStar Software and any
//    modification/derivatives thereof is transferred to you under Terms.
//
// 2. You understand that MStar Software might include, incorporate or be
//    supplied together with third party`s software and the use of MStar
//    Software may require additional licenses from third parties.
//    Therefore, you hereby agree it is your sole responsibility to separately
//    obtain any and all third party right and license necessary for your use of
//    such third party`s software.
//
// 3. MStar Software and any modification/derivatives thereof shall be deemed as
//    MStar`s confidential information and you agree to keep MStar`s
//    confidential information in strictest confidence and not disclose to any
//    third party.
//
// 4. MStar Software is provided on an "AS IS" basis without warranties of any
//    kind. Any warranties are hereby expressly disclaimed by MStar, including
//    without limitation, any warranties of merchantability, non-infringement of
//    intellectual property rights, fitness for a particular purpose, error free
//    and in conformity with any international standard.  You agree to waive any
//    claim against MStar for any loss, damage, cost or expense that you may
//    incur related to your use of MStar Software.
//    In no event shall MStar be liable for any direct, indirect, incidental or
//    consequential damages, including without limitation, lost of profit or
//    revenues, lost or damage of data, and unauthorized system use.
//    You agree that this Section 4 shall still apply without being affected
//    even if MStar Software has been modified by MStar in accordance with your
//    request or instruction for your use, except otherwise agreed by both
//    parties in writing.
//
// 5. If requested, MStar may from time to time provide technical supports or
//    services in relation with MStar Software to you for your use of
//    MStar Software in conjunction with your or your customer`s product
//    ("Services").
//    You understand and agree that, except otherwise agreed by both parties in
//    writing, Services are provided on an "AS IS" basis and the warranty
//    disclaimer set forth in Section 4 above shall apply.
//
// 6. Nothing contained herein shall be construed as by implication, estoppels
//    or otherwise:
//    (a) conferring any license or right to use MStar name, trademark, service
//        mark, symbol or any other identification;
//    (b) obligating MStar or any of its affiliates to furnish any person,
//        including without limitation, you and your customers, any assistance
//        of any kind whatsoever, or any information; or
//    (c) conferring any license or right under any intellectual property right.
//
// 7. These terms shall be governed by and construed in accordance with the laws
//    of Taiwan, R.O.C., excluding its conflict of law rules.
//    Any and all dispute arising out hereof or related hereto shall be finally
//    settled by arbitration referred to the Chinese Arbitration Association,
//    Taipei in accordance with the ROC Arbitration Law and the Arbitration
//    Rules of the Association by three (3) arbitrators appointed in accordance
//    with the said Rules.
//    The place of arbitration shall be in Taipei, Taiwan and the language shall
//    be English.
//    The arbitration award shall be final and binding to both parties.
//
//******************************************************************************
//<MStar Software>
////////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2008-2009 MStar Semiconductor, Inc.
// All rights reserved.
//
// Unless otherwise stipulated in writing, any and all information contained
// herein regardless in any format shall remain the sole proprietary of
// MStar Semiconductor Inc. and be kept in strict confidence
// ("MStar Confidential Information") by the recipient.
// Any unauthorized act including without limitation unauthorized disclosure,
// copying, use, reproduction, sale, distribution, modification, disassembling,
// reverse engineering and compiling of the contents of MStar Confidential
// Information is unlawful and strictly prohibited. MStar hereby reserves the
// rights to any and all damages, losses, costs and expenses resulting therefrom.
//
////////////////////////////////////////////////////////////////////////////////

#ifndef _MMAP_ID_H_
#define _MMAP_ID_H_
//-------------------------Note !!-------------------------------
// This file was generated in Middleware/Makefile automatically
// Do not modify it ...
//---------------------------------------------------------------

/// MMapp item enum
typedef enum
{
    E_MMAP_ID_VDEC_CPU=0,
    E_MMAP_ID_VDEC_FRAMEBUFFER,
    E_MMAP_ID_VDEC_BITSTREAM,
    E_MMAP_ID_VDEC_SUB_FRAMEBUFFER,
    E_MMAP_ID_VDEC_SUB_BITSTREAM,
    E_MMAP_ID_VDEC_MVC_BITSTREAM,
    E_MMAP_ID_VDEC_MVC_FRAMEBUFFER,
    E_MMAP_ID_VDEC_FRAMEBUFFER_SD,
    E_MMAP_ID_VDEC_BITSTREAM_SD,
    E_MMAP_ID_VDEC_SHARE_MEM,
    E_MMAP_ID_MAD_DEC,
    E_MMAP_ID_MAD_SE,
    E_MMAP_ID_MAD_R2,
    E_MMAP_ID_ARMFW_MEM,
    E_MMAP_ID_OPTEE_MEM,
    E_MMAP_ID_HW_AES_BUF,
    E_MMAP_ID_NUTTX_RAMLOG,
    E_SECURE_SHM,
    E_SECURE_UPDATE_AREA,
    E_SECURE_TSP,
    E_MMAP_ID_HW_SECURE_BUFFER,
    E_MMAP_ID_DIP_MEM,
    E_DFB_JPD_WRITE,
    E_DFB_FRAMEBUFFER,
    E_MMAP_ID_PVR_DOWNLOAD,
    E_MMAP_ID_PVR_UPLOAD,
    E_MMAP_ID_PVR_SECURE_DOWNLOAD,
    E_MMAP_ID_PVR_SECURE_UPLOAD,
    E_MMAP_ID_DMX_VQUEUE,
    E_MIU1_INTERVAL,
    E_MIU2_INTERVAL,
    E_MMAP_ID_MAX   //Must be placed at the end
} EN_MMAP_ID;

#endif // _MMAP_ID_H_
