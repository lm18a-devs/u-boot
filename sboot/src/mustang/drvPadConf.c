//<MStar Software>
//******************************************************************************
// MStar Software
// Copyright (c) 2010 - 2012 MStar Semiconductor, Inc. All rights reserved.
// All software, firmware and related documentation herein ("MStar Software") are
// intellectual property of MStar Semiconductor, Inc. ("MStar") and protected by
// law, including, but not limited to, copyright law and international treaties.
// Any use, modification, reproduction, retransmission, or republication of all
// or part of MStar Software is expressly prohibited, unless prior written
// permission has been granted by MStar.
//
// By accessing, browsing and/or using MStar Software, you acknowledge that you
// have read, understood, and agree, to be bound by below terms ("Terms") and to
// comply with all applicable laws and regulations:
//
// 1. MStar shall retain any and all right, ownership and interest to MStar
//    Software and any modification/derivatives thereof.
//    No right, ownership, or interest to MStar Software and any
//    modification/derivatives thereof is transferred to you under Terms.
//
// 2. You understand that MStar Software might include, incorporate or be
//    supplied together with third party`s software and the use of MStar
//    Software may require additional licenses from third parties.
//    Therefore, you hereby agree it is your sole responsibility to separately
//    obtain any and all third party right and license necessary for your use of
//    such third party`s software.
//
// 3. MStar Software and any modification/derivatives thereof shall be deemed as
//    MStar`s confidential information and you agree to keep MStar`s
//    confidential information in strictest confidence and not disclose to any
//    third party.
//
// 4. MStar Software is provided on an "AS IS" basis without warranties of any
//    kind. Any warranties are hereby expressly disclaimed by MStar, including
//    without limitation, any warranties of merchantability, non-infringement of
//    intellectual property rights, fitness for a particular purpose, error free
//    and in conformity with any international standard.  You agree to waive any
//    claim against MStar for any loss, damage, cost or expense that you may
//    incur related to your use of MStar Software.
//    In no event shall MStar be liable for any direct, indirect, incidental or
//    consequential damages, including without limitation, lost of profit or
//    revenues, lost or damage of data, and unauthorized system use.
//    You agree that this Section 4 shall still apply without being affected
//    even if MStar Software has been modified by MStar in accordance with your
//    request or instruction for your use, except otherwise agreed by both
//    parties in writing.
//
// 5. If requested, MStar may from time to time provide technical supports or
//    services in relation with MStar Software to you for your use of
//    MStar Software in conjunction with your or your customer`s product
//    ("Services").
//    You understand and agree that, except otherwise agreed by both parties in
//    writing, Services are provided on an "AS IS" basis and the warranty
//    disclaimer set forth in Section 4 above shall apply.
//
// 6. Nothing contained herein shall be construed as by implication, estoppels
//    or otherwise:
//    (a) conferring any license or right to use MStar name, trademark, service
//        mark, symbol or any other identification;
//    (b) obligating MStar or any of its affiliates to furnish any person,
//        including without limitation, you and your customers, any assistance
//        of any kind whatsoever, or any information; or
//    (c) conferring any license or right under any intellectual property right.
//
// 7. These terms shall be governed by and construed in accordance with the laws
//    of Taiwan, R.O.C., excluding its conflict of law rules.
//    Any and all dispute arising out hereof or related hereto shall be finally
//    settled by arbitration referred to the Chinese Arbitration Association,
//    Taipei in accordance with the ROC Arbitration Law and the Arbitration
//    Rules of the Association by three (3) arbitrators appointed in accordance
//    with the said Rules.
//    The place of arbitration shall be in Taipei, Taiwan and the language shall
//    be English.
//    The arbitration award shall be final and binding to both parties.
//
//******************************************************************************
//<MStar Software>

#include <stdio.h>

#include "datatype.h"
#include "drvRIU.h"
#include "Board.h"
#include "c_riubase.h"
#include "hwreg_mustang.h"
#include "chip/bond.h"

#define GPIO_NONE               0       // Not GPIO pin (default)
#define GPIO_IN                 1       // GPI
#define GPIO_OUT_LOW            2       // GPO output low
#define GPIO_OUT_HIGH           3       // GPO output high

#if defined(ARM_CHAKRA) || defined (MIPS_CHAKRA) || defined(MSOS_TYPE_LINUX)
#define _MapBase_nonPM_MUSTANG      (RIU_MAP + 0x200000)
#define _MapBase_PM_MUSTANG         RIU_MAP
#else
#define _MapBase_nonPM_MUSTANG      0xA0200000
#define _MapBase_PM_MUSTANG         0xA0000000
#endif

#define _MEMMAP_nonPM_          _RVM1(0x0000, 0x10, 0xFF)
#define _MEMMAP_PM_             _RVM1(0x0000, 0x00, 0xFF)
#define _MEMMAP_nonPM_SPARE0_   _RVM1(0x0000, 0x12, 0xFF)
#define _MEMMAP_nonPM_LINEIN_   _RVM1(0x0000, 0x11, 0xFF)

const U8 padInitTbl_PreInit[] =
{
    0x39, 0xB6, 0x5B, 0x53,      // magic code for ISP_Tool

    // programable device number
    // spi flash count
    1 + (PIN_SPI_CZ1 != 0) + (PIN_SPI_CZ2 != 0) + (PIN_SPI_CZ3 != 0),
    0x00,                       // nor
    0x00,                       // nand
    0x00,                       // reserved
    0x00,                       // reserved
    0x00,                       // reserved

//---------------------------------------------------------------------
// GPIO Configuartion
//---------------------------------------------------------------------
    _MEMMAP_PM_,

    #if(PAD_PM_SPI_CK_IS_GPIO != GPIO_NONE)
    #define PAD_PM_SPI_CK_OEN (PAD_PM_SPI_CK_IS_GPIO == GPIO_IN ? BIT0: 0)
    #define PAD_PM_SPI_CK_OUT (PAD_PM_SPI_CK_IS_GPIO == GPIO_OUT_HIGH ? BIT1: 0)
    _RVM1(0x0f30, PAD_PM_SPI_CK_OUT, BIT1),
    _RVM1(0x0f30, PAD_PM_SPI_CK_OEN, BIT0),
    //reg_spi_nand_mode
    _RVM1(0x2ea0, 0, BIT0 ),   //reg[2ea0]#0 = 0
    //reg_spi_gpio
    _RVM1(0x0e6a, BIT0 , BIT0 ),   //reg[0e6a]#0 = 1
    #endif

    #if(PAD_PM_SPI_DI_IS_GPIO != GPIO_NONE)
    #define PAD_PM_SPI_DI_OEN (PAD_PM_SPI_DI_IS_GPIO == GPIO_IN ? BIT0: 0)
    #define PAD_PM_SPI_DI_OUT (PAD_PM_SPI_DI_IS_GPIO == GPIO_OUT_HIGH ? BIT1: 0)
    _RVM1(0x0f32, PAD_PM_SPI_DI_OUT, BIT1),
    _RVM1(0x0f32, PAD_PM_SPI_DI_OEN, BIT0),
    //reg_spi_nand_mode
    _RVM1(0x2ea0, 0, BIT0 ),   //reg[2ea0]#0 = 0
    //reg_spi_gpio
    _RVM1(0x0e6a, BIT0 , BIT0 ),   //reg[0e6a]#0 = 1
    #endif

    #if(PAD_PM_SPI_DO_IS_GPIO != GPIO_NONE)
    #define PAD_PM_SPI_DO_OEN (PAD_PM_SPI_DO_IS_GPIO == GPIO_IN ? BIT0: 0)
    #define PAD_PM_SPI_DO_OUT (PAD_PM_SPI_DO_IS_GPIO == GPIO_OUT_HIGH ? BIT1: 0)
    _RVM1(0x0f34, PAD_PM_SPI_DO_OUT, BIT1),
    _RVM1(0x0f34, PAD_PM_SPI_DO_OEN, BIT0),
    //reg_spi_nand_mode
    _RVM1(0x2ea0, 0, BIT0 ),   //reg[2ea0]#0 = 0
    //reg_spi_gpio
    _RVM1(0x0e6a, BIT0 , BIT0 ),   //reg[0e6a]#0 = 1
    #endif

    #if(PAD_PM_SPI_HOLDN_IS_GPIO != GPIO_NONE)
    #define PAD_PM_SPI_HOLDN_OEN (PAD_PM_SPI_HOLDN_IS_GPIO == GPIO_IN ? BIT0: 0)
    #define PAD_PM_SPI_HOLDN_OUT (PAD_PM_SPI_HOLDN_IS_GPIO == GPIO_OUT_HIGH ? BIT1: 0)
    _RVM1(0x0f36, PAD_PM_SPI_HOLDN_OUT, BIT1),
    _RVM1(0x0f36, PAD_PM_SPI_HOLDN_OEN, BIT0),
    //reg_spi_nand_mode
    _RVM1(0x2ea0, 0, BIT0 ),   //reg[2ea0]#0 = 0
    //reg_spi_wpn_holdn_gpio
    _RVM1(0x0e6a, BIT6 , BIT6 ),   //reg[0e6a]#6 = 1
    #endif

    #if(PAD_PM_SPI_WPN_IS_GPIO != GPIO_NONE)
    #define PAD_PM_SPI_WPN_OEN (PAD_PM_SPI_WPN_IS_GPIO == GPIO_IN ? BIT0: 0)
    #define PAD_PM_SPI_WPN_OUT (PAD_PM_SPI_WPN_IS_GPIO == GPIO_OUT_HIGH ? BIT1: 0)
    _RVM1(0x0f2c, PAD_PM_SPI_WPN_OUT, BIT1),
    _RVM1(0x0f2c, PAD_PM_SPI_WPN_OEN, BIT0),
    //reg_spi_nand_mode
    _RVM1(0x2ea0, 0, BIT0 ),   //reg[2ea0]#0 = 0
    //reg_pwm_mode
    _RVM1(0x0ed8, 0, BIT0 ),   //reg[0ed8]#0 = 0
    //reg_spi_wpn_holdn_gpio
    _RVM1(0x0e6a, BIT6 , BIT6 ),   //reg[0e6a]#6 = 1
    #endif

    #if(PAD_VID0_IS_GPIO != GPIO_NONE)
    #define PAD_VID0_OEN (PAD_VID0_IS_GPIO == GPIO_IN ? BIT1: 0)
    #define PAD_VID0_OUT (PAD_VID0_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
    _RVM1(0x2e84, PAD_VID0_OUT, BIT0),
    _RVM1(0x2e84, PAD_VID0_OEN, BIT1),
    //reg_led_use_pad_vid
    _RVM1(0x2ea2, 0, BIT0 ),   //reg[2ea2]#0 =0
    //reg_vid_is_gpio
    _RVM1(0x0e39, BIT2 , BIT2 ),   //reg[0e39]#2 = 1
    #endif

    #if(PAD_VID1_IS_GPIO != GPIO_NONE)
    #define PAD_VID1_OEN (PAD_VID1_IS_GPIO == GPIO_IN ? BIT1: 0)
    #define PAD_VID1_OUT (PAD_VID1_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
    _RVM1(0x2e85, PAD_VID1_OUT, BIT0),
    _RVM1(0x2e85, PAD_VID1_OEN, BIT1),
    //reg_led_use_pad_vid
    _RVM1(0x2ea2, 0, BIT0 ),   //reg[2ea2]#0 =0
    //reg_vid_is_gpio
    _RVM1(0x0e39, BIT2 , BIT2 ),   //reg[0e39]#2 = 1
    #endif

    #if(PAD_IRIN_IS_GPIO != GPIO_NONE)
    #define PAD_IRIN_OEN (PAD_IRIN_IS_GPIO == GPIO_IN ? BITalways input mode: 0)
    #define PAD_IRIN_OUT (PAD_IRIN_IS_GPIO == GPIO_OUT_HIGH ? BIT1: 0)
    _RVM1(0x0f26, PAD_IRIN_OUT, BIT1),
    _RVM1(0xys input mod, PAD_IRIN_OEN, BITalways input mode),
    //reg_ir_is_gpio
    _RVM1(0x0e38, BIT4 , BIT4 ),   //reg[0e38]#4 = 1
    #endif

    #if(PAD_CEC_IS_GPIO != GPIO_NONE)
    #define PAD_CEC_OEN (PAD_CEC_IS_GPIO == GPIO_IN ? BIT0: 0)
    #define PAD_CEC_OUT (PAD_CEC_IS_GPIO == GPIO_OUT_HIGH ? BIT1: 0)
    _RVM1(0x0f2a, PAD_CEC_OUT, BIT1),
    _RVM1(0x0f2a, PAD_CEC_OEN, BIT0),
    //reg_cec_is_gpio
    _RVM1(0x0e38, BIT6 , BIT6 ),   //reg[0e38]#6 = 1
    #endif

    #if(PAD_GPIO_PM0_IS_GPIO != GPIO_NONE)
    #define PAD_GPIO_PM0_OEN (PAD_GPIO_PM0_IS_GPIO == GPIO_IN ? BIT0: 0)
    #define PAD_GPIO_PM0_OUT (PAD_GPIO_PM0_IS_GPIO == GPIO_OUT_HIGH ? BIT1: 0)
    _RVM1(0x0f00, PAD_GPIO_PM0_OUT, BIT1),
    _RVM1(0x0f00, PAD_GPIO_PM0_OEN, BIT0),
    //default is GPIO
    #endif

    #if(PAD_GPIO_PM1_IS_GPIO != GPIO_NONE)
    #define PAD_GPIO_PM1_OEN (PAD_GPIO_PM1_IS_GPIO == GPIO_IN ? BIT0: 0)
    #define PAD_GPIO_PM1_OUT (PAD_GPIO_PM1_IS_GPIO == GPIO_OUT_HIGH ? BIT1: 0)
    _RVM1(0x0f02, PAD_GPIO_PM1_OUT, BIT1),
    _RVM1(0x0f02, PAD_GPIO_PM1_OEN, BIT0),
    //reg_uart_is_gpio[1]
    _RVM1(0x0e6b, 0, BIT1 ),   //reg[0e6b]#1 = 0
    //reg_uart_is_gpio[3]
    _RVM1(0x0e6b, 0, BIT3 ),   //reg[0e6b]#3 = 0
    //reg_uart_is_gpio_3[0]
    _RVM1(0x0eec, 0, BIT2 ),   //reg[0eec]#2 = 0
    #endif
    _RVM1(0x0e6b, BIT3, BIT3),

    #if(PAD_GPIO_PM2_IS_GPIO != GPIO_NONE)
    #define PAD_GPIO_PM2_OEN (PAD_GPIO_PM2_IS_GPIO == GPIO_IN ? BIT0: 0)
    #define PAD_GPIO_PM2_OUT (PAD_GPIO_PM2_IS_GPIO == GPIO_OUT_HIGH ? BIT1: 0)
    _RVM1(0x0f04, PAD_GPIO_PM2_OUT, BIT1),
    _RVM1(0x0f04, PAD_GPIO_PM2_OEN, BIT0),
    //default is GPIO
    #endif

    #if(PAD_GPIO_PM3_IS_GPIO != GPIO_NONE)
    #define PAD_GPIO_PM3_OEN (PAD_GPIO_PM3_IS_GPIO == GPIO_IN ? BIT0: 0)
    #define PAD_GPIO_PM3_OUT (PAD_GPIO_PM3_IS_GPIO == GPIO_OUT_HIGH ? BIT1: 0)
    _RVM1(0x0f06, PAD_GPIO_PM3_OUT, BIT1),
    _RVM1(0x0f06, PAD_GPIO_PM3_OEN, BIT0),
    //default is GPIO
    #endif

    #if(PAD_GPIO_PM4_IS_GPIO != GPIO_NONE)
    #define PAD_GPIO_PM4_OEN (PAD_GPIO_PM4_IS_GPIO == GPIO_IN ? BIT0: 0)
    #define PAD_GPIO_PM4_OUT (PAD_GPIO_PM4_IS_GPIO == GPIO_OUT_HIGH ? BIT1: 0)
    _RVM1(0x0f08, PAD_GPIO_PM4_OUT, BIT1),
    _RVM1(0x0f08, PAD_GPIO_PM4_OEN, BIT0),
    //reg_gpio_pm_lock
    _RVM1(0x0e24, 0xFF, 0xBE ),   //reg[0e24]#7 ~ #0 = 10111110b
    _RVM1(0x0e25, 0xFF, 0xBA ),   //reg[0e25]#7 ~ #0 = 10111010b
    #endif

    #if(PAD_GPIO_PM5_IS_GPIO != GPIO_NONE)
    #define PAD_GPIO_PM5_OEN (PAD_GPIO_PM5_IS_GPIO == GPIO_IN ? BIT0: 0)
    #define PAD_GPIO_PM5_OUT (PAD_GPIO_PM5_IS_GPIO == GPIO_OUT_HIGH ? BIT1: 0)
    _RVM1(0x0f0a, PAD_GPIO_PM5_OUT, BIT1),
    _RVM1(0x0f0a, PAD_GPIO_PM5_OEN, BIT0),
    //default is GPIO
    #endif

    #if(PAD_GPIO_PM6_IS_GPIO != GPIO_NONE)
    #define PAD_GPIO_PM6_OEN (PAD_GPIO_PM6_IS_GPIO == GPIO_IN ? BIT0: 0)
    #define PAD_GPIO_PM6_OUT (PAD_GPIO_PM6_IS_GPIO == GPIO_OUT_HIGH ? BIT1: 0)
    _RVM1(0x0f0c, PAD_GPIO_PM6_OUT, BIT1),
    _RVM1(0x0f0c, PAD_GPIO_PM6_OEN, BIT0),
    //reg_spicsz1_gpio
    _RVM1(0x0e6a, BIT2 , BIT2 ),   //reg[0e6a]#2 = 1
    #endif

    #if(PAD_GPIO_PM7_IS_GPIO != GPIO_NONE)
    #define PAD_GPIO_PM7_OEN (PAD_GPIO_PM7_IS_GPIO == GPIO_IN ? BIT0: 0)
    #define PAD_GPIO_PM7_OUT (PAD_GPIO_PM7_IS_GPIO == GPIO_OUT_HIGH ? BIT1: 0)
    _RVM1(0x0f0e, PAD_GPIO_PM7_OUT, BIT1),
    _RVM1(0x0f0e, PAD_GPIO_PM7_OEN, BIT0),
    //default is GPIO
    #endif

    #if(PAD_GPIO_PM8_IS_GPIO != GPIO_NONE)
    #define PAD_GPIO_PM8_OEN (PAD_GPIO_PM8_IS_GPIO == GPIO_IN ? BIT0: 0)
    #define PAD_GPIO_PM8_OUT (PAD_GPIO_PM8_IS_GPIO == GPIO_OUT_HIGH ? BIT1: 0)
    _RVM1(0x0f10, PAD_GPIO_PM8_OUT, BIT1),
    _RVM1(0x0f10, PAD_GPIO_PM8_OEN, BIT0),
    //reg_uart_is_gpio[0]
    _RVM1(0x0e6b, 0, BIT0 ),   //reg[0e6b]#0 = 0
    //reg_uart_is_gpio_2[0]
    _RVM1(0x0eec, 0, BIT0 ),   //reg[0eec]#0 = 0
    //reg_uart_is_gpio_2[1]
    _RVM1(0x0eec, 0, BIT1 ),   //reg[0eec]#1 = 0
    //reg_uart_is_gpio_3[1]
    _RVM1(0x0eec, 0, BIT3 ),   //reg[0eec]#3 = 0
    #endif

    #if(PAD_GPIO_PM9_IS_GPIO != GPIO_NONE)
    #define PAD_GPIO_PM9_OEN (PAD_GPIO_PM9_IS_GPIO == GPIO_IN ? BIT0: 0)
    #define PAD_GPIO_PM9_OUT (PAD_GPIO_PM9_IS_GPIO == GPIO_OUT_HIGH ? BIT1: 0)
    _RVM1(0x0f12, PAD_GPIO_PM9_OUT, BIT1),
    _RVM1(0x0f12, PAD_GPIO_PM9_OEN, BIT0),
    //default is GPIO
    #endif

    #if(PAD_GPIO_PM10_IS_GPIO != GPIO_NONE)
    #define PAD_GPIO_PM10_OEN (PAD_GPIO_PM10_IS_GPIO == GPIO_IN ? BIT0: 0)
    #define PAD_GPIO_PM10_OUT (PAD_GPIO_PM10_IS_GPIO == GPIO_OUT_HIGH ? BIT1: 0)
    _RVM1(0x0f14, PAD_GPIO_PM10_OUT, BIT1),
    _RVM1(0x0f14, PAD_GPIO_PM10_OEN, BIT0),
    //reg_spicsz2_gpio
    _RVM1(0x0e6a, BIT3 , BIT3 ),   //reg[0e6a]#3 = 1
    #endif

    #if(PAD_GPIO_PM11_IS_GPIO != GPIO_NONE)
    #define PAD_GPIO_PM11_OEN (PAD_GPIO_PM11_IS_GPIO == GPIO_IN ? BIT0: 0)
    #define PAD_GPIO_PM11_OUT (PAD_GPIO_PM11_IS_GPIO == GPIO_OUT_HIGH ? BIT1: 0)
    _RVM1(0x0f16, PAD_GPIO_PM11_OUT, BIT1),
    _RVM1(0x0f16, PAD_GPIO_PM11_OEN, BIT0),
    //default is GPIO
    #endif

    #if(PAD_GPIO_PM12_IS_GPIO != GPIO_NONE)
    #define PAD_GPIO_PM12_OEN (PAD_GPIO_PM12_IS_GPIO == GPIO_IN ? BIT0: 0)
    #define PAD_GPIO_PM12_OUT (PAD_GPIO_PM12_IS_GPIO == GPIO_OUT_HIGH ? BIT1: 0)
    _RVM1(0x0f18, PAD_GPIO_PM12_OUT, BIT1),
    _RVM1(0x0f18, PAD_GPIO_PM12_OEN, BIT0),
    //reg_uart_is_gpio_1[0]
    _RVM1(0x0e6b, 0, BIT6 ),   //reg[0e6b]#6 = 0
    //reg_uart_is_gpio_1[1]
    _RVM1(0x0e6b, 0, BIT7 ),   //reg[0e6b]#7 = 0
    //reg_uart_is_gpio_3[2]
    _RVM1(0x0eec, 0, BIT4 ),   //reg[0eec]#4 = 0
    #endif

    #if(PAD_GPIO_PM13_IS_GPIO != GPIO_NONE)
    #define PAD_GPIO_PM13_OEN (PAD_GPIO_PM13_IS_GPIO == GPIO_IN ? BIT0: 0)
    #define PAD_GPIO_PM13_OUT (PAD_GPIO_PM13_IS_GPIO == GPIO_OUT_HIGH ? BIT1: 0)
    _RVM1(0x0f1a, PAD_GPIO_PM13_OUT, BIT1),
    _RVM1(0x0f1a, PAD_GPIO_PM13_OEN, BIT0),
    //reg_mhl_cable_detect_sel
    _RVM1(0x0ee4, 0, BIT6 ),   //reg[0ee4]#6 = 0
    #endif

    #if(PAD_GPIO_PM14_IS_GPIO != GPIO_NONE)
    #define PAD_GPIO_PM14_OEN (PAD_GPIO_PM14_IS_GPIO == GPIO_IN ? BIT0: 0)
    #define PAD_GPIO_PM14_OUT (PAD_GPIO_PM14_IS_GPIO == GPIO_OUT_HIGH ? BIT1: 0)
    _RVM1(0x0f1c, PAD_GPIO_PM14_OUT, BIT1),
    _RVM1(0x0f1c, PAD_GPIO_PM14_OEN, BIT0),
    //reg_vbus_en_sel
    _RVM1(0x0ee4, 0, BIT7 ),   //reg[0ee4]#7 = 0
    #endif

    #if(PAD_GPIO_PM15_IS_GPIO != GPIO_NONE)
    #define PAD_GPIO_PM15_OEN (PAD_GPIO_PM15_IS_GPIO == GPIO_IN ? BIT0: 0)
    #define PAD_GPIO_PM15_OUT (PAD_GPIO_PM15_IS_GPIO == GPIO_OUT_HIGH ? BIT1: 0)
    _RVM1(0x0f1e, PAD_GPIO_PM15_OUT, BIT1),
    _RVM1(0x0f1e, PAD_GPIO_PM15_OEN, BIT0),
    //reg_cbus_debug_sel
    _RVM1(0x0ee5, 0, BIT0 ),   //reg[0ee5]#0 = 0
    #endif

    #if(PAD_HOTPLUGA_IS_GPIO != GPIO_NONE)
    #define PAD_HOTPLUGA_OEN (PAD_HOTPLUGA_IS_GPIO == GPIO_IN ? BIT0: 0)
    #define PAD_HOTPLUGA_OUT (PAD_HOTPLUGA_IS_GPIO == GPIO_OUT_HIGH ? BIT4: 0)
    _RVM1(0x0e4e, PAD_HOTPLUGA_OUT, BIT4),
    _RVM1(0x0e4e, PAD_HOTPLUGA_OEN, BIT0),
    //default is GPIO
    #endif

    #if(PAD_HOTPLUGB_IS_GPIO != GPIO_NONE)
    #define PAD_HOTPLUGB_OEN (PAD_HOTPLUGB_IS_GPIO == GPIO_IN ? BIT1: 0)
    #define PAD_HOTPLUGB_OUT (PAD_HOTPLUGB_IS_GPIO == GPIO_OUT_HIGH ? BIT5: 0)
    _RVM1(0x0e4e, PAD_HOTPLUGB_OUT, BIT5),
    _RVM1(0x0e4e, PAD_HOTPLUGB_OEN, BIT1),
    //default is GPIO
    #endif

    #if(PAD_HOTPLUGC_IS_GPIO != GPIO_NONE)
    #define PAD_HOTPLUGC_OEN (PAD_HOTPLUGC_IS_GPIO == GPIO_IN ? BIT2: 0)
    #define PAD_HOTPLUGC_OUT (PAD_HOTPLUGC_IS_GPIO == GPIO_OUT_HIGH ? BIT6: 0)
    _RVM1(0x0e4e, PAD_HOTPLUGC_OUT, BIT6),
    _RVM1(0x0e4e, PAD_HOTPLUGC_OEN, BIT2),
    //reg_hplugc_mhl_en
    _RVM1(0x0ee6, 0, BIT0 ),   //reg[0ee6]#0 = 0
    #endif

    #if(PAD_HOTPLUGD_IS_GPIO != GPIO_NONE)
    #define PAD_HOTPLUGD_OEN (PAD_HOTPLUGD_IS_GPIO == GPIO_IN ? BIT3: 0)
    #define PAD_HOTPLUGD_OUT (PAD_HOTPLUGD_IS_GPIO == GPIO_OUT_HIGH ? BIT7: 0)
    _RVM1(0x0e4e, PAD_HOTPLUGD_OUT, BIT7),
    _RVM1(0x0e4e, PAD_HOTPLUGD_OEN, BIT3),
    //reg_hpluga_mhl_en
    _RVM1(0x0ee7, 0, BIT0 ),   //reg[0ee7]#0 = 0
    _RVM1(0x0ee7, 1, BIT6 ),   //reg[0ee7]#6 = 1
    #endif

    #if(PAD_DDCDA_CK_IS_GPIO != GPIO_NONE)
    #define PAD_DDCDA_CK_OEN (PAD_DDCDA_CK_IS_GPIO == GPIO_IN ? BIT1: 0)
    #define PAD_DDCDA_CK_OUT (PAD_DDCDA_CK_IS_GPIO == GPIO_OUT_HIGH ? BIT2: 0)
    _RVM1(0x0496, PAD_DDCDA_CK_OUT, BIT2),
    _RVM1(0x0496, PAD_DDCDA_CK_OEN, BIT1),
    //reg_ej_mode
    _RVM1(0x2ec4, 0, BIT0 ),   //reg[2ec4]#0 = 0
    //reg_ej_mode
    _RVM1(0x2ec4, 0, BIT1 ),   //reg[2ec4]#1 = 0
    //reg_gpio2do_en
    _RVM1(0x0496, BIT7 , BIT7 ),   //reg[0496]#7 = 1
    #endif

    #if(PAD_DDCDA_DA_IS_GPIO != GPIO_NONE)
    #define PAD_DDCDA_DA_OEN (PAD_DDCDA_DA_IS_GPIO == GPIO_IN ? BIT5: 0)
    #define PAD_DDCDA_DA_OUT (PAD_DDCDA_DA_IS_GPIO == GPIO_OUT_HIGH ? BIT6: 0)
    _RVM1(0x0496, PAD_DDCDA_DA_OUT, BIT6),
    _RVM1(0x0496, PAD_DDCDA_DA_OEN, BIT5),
    //reg_ej_mode
    _RVM1(0x2ec4, 0, BIT0 ),   //reg[2ec4]#0 = 0
    //reg_ej_mode
    _RVM1(0x2ec4, 0, BIT1 ),   //reg[2ec4]#1 = 0
    //reg_gpio2do_en
    _RVM1(0x0496, BIT7 , BIT7 ),   //reg[0496]#7 = 1
    #endif

    #if(PAD_DDCDB_CK_IS_GPIO != GPIO_NONE)
    #define PAD_DDCDB_CK_OEN (PAD_DDCDB_CK_IS_GPIO == GPIO_IN ? BIT1: 0)
    #define PAD_DDCDB_CK_OUT (PAD_DDCDB_CK_IS_GPIO == GPIO_OUT_HIGH ? BIT2: 0)
    _RVM1(0x0497, PAD_DDCDB_CK_OUT, BIT2),
    _RVM1(0x0497, PAD_DDCDB_CK_OEN, BIT1),
    //reg_ej_mode
    _RVM1(0x2ec4, 0, BIT0 ),   //reg[2ec4]#0 = 0
    //reg_ej_mode
    _RVM1(0x2ec4, 0, BIT1 ),   //reg[2ec4]#1 = 0
    //reg_gpio2d1_en
    _RVM1(0x0497, BIT7 , BIT7 ),   //reg[0497]#7 = 1
    #endif

    #if(PAD_DDCDB_DA_IS_GPIO != GPIO_NONE)
    #define PAD_DDCDB_DA_OEN (PAD_DDCDB_DA_IS_GPIO == GPIO_IN ? BIT5: 0)
    #define PAD_DDCDB_DA_OUT (PAD_DDCDB_DA_IS_GPIO == GPIO_OUT_HIGH ? BIT6: 0)
    _RVM1(0x0497, PAD_DDCDB_DA_OUT, BIT6),
    _RVM1(0x0497, PAD_DDCDB_DA_OEN, BIT5),
    //reg_ej_mode
    _RVM1(0x2ec4, 0, BIT0 ),   //reg[2ec4]#0 = 0
    //reg_ej_mode
    _RVM1(0x2ec4, 0, BIT1 ),   //reg[2ec4]#1 = 0
    //reg_gpio2d1_en
    _RVM1(0x0497, BIT7 , BIT7 ),   //reg[0497]#7 = 1
    #endif

    #if(PAD_DDCDC_CK_IS_GPIO != GPIO_NONE)
    #define PAD_DDCDC_CK_OEN (PAD_DDCDC_CK_IS_GPIO == GPIO_IN ? BIT1: 0)
    #define PAD_DDCDC_CK_OUT (PAD_DDCDC_CK_IS_GPIO == GPIO_OUT_HIGH ? BIT2: 0)
    //_RVM1(0x0498, PAD_DDCDC_CK_OUT, BIT2),
    //_RVM1(0x0498, PAD_DDCDC_CK_OEN, BIT1),
    //reg_ej_mode
    _RVM1(0x2ec4, 0, BIT0 ),   //reg[2ec4]#0 = 0
    //reg_ej_mode
    _RVM1(0x2ec4, 0, BIT1 ),   //reg[2ec4]#1 = 0
    //reg_gpio2d2_en
    _RVM1(0x0498, BIT7 , BIT7 ),   //reg[0498]#7 = 1
    #endif

    #if(PAD_DDCDC_DA_IS_GPIO != GPIO_NONE)
    #define PAD_DDCDC_DA_OEN (PAD_DDCDC_DA_IS_GPIO == GPIO_IN ? BIT5: 0)
    #define PAD_DDCDC_DA_OUT (PAD_DDCDC_DA_IS_GPIO == GPIO_OUT_HIGH ? BIT6: 0)
    //_RVM1(0x0498, PAD_DDCDC_DA_OUT, BIT6),
    //_RVM1(0x0498, PAD_DDCDC_DA_OEN, BIT5),
    //reg_ej_mode
    _RVM1(0x2ec4, 0, BIT0 ),   //reg[2ec4]#0 = 0
    //reg_ej_mode
    _RVM1(0x2ec4, 0, BIT1 ),   //reg[2ec4]#1 = 0
    //reg_gpio2d2_en
    _RVM1(0x0498, BIT7 , BIT7 ),   //reg[0498]#7 = 1
    #endif

    #if(PAD_DDCDD_CK_IS_GPIO != GPIO_NONE)
    #define PAD_DDCDD_CK_OEN (PAD_DDCDD_CK_IS_GPIO == GPIO_IN ? BIT1: 0)
    #define PAD_DDCDD_CK_OUT (PAD_DDCDD_CK_IS_GPIO == GPIO_OUT_HIGH ? BIT2: 0)
    _RVM1(0x0499, PAD_DDCDD_CK_OUT, BIT2),
    _RVM1(0x0499, PAD_DDCDD_CK_OEN, BIT1),
    //reg_ej_mode
    _RVM1(0x2ec4, 0, BIT0 ),   //reg[2ec4]#0 = 0
    //reg_ej_mode
    _RVM1(0x2ec4, 0, BIT1 ),   //reg[2ec4]#1 = 0
    //reg_gpio2d3_en
    _RVM1(0x0499, BIT7 , BIT7 ),   //reg[0499]#7 = 1
    #endif

    #if(PAD_DDCDD_DA_IS_GPIO != GPIO_NONE)
    #define PAD_DDCDD_DA_OEN (PAD_DDCDD_DA_IS_GPIO == GPIO_IN ? BIT5: 0)
    #define PAD_DDCDD_DA_OUT (PAD_DDCDD_DA_IS_GPIO == GPIO_OUT_HIGH ? BIT6: 0)
    _RVM1(0x0499, PAD_DDCDD_DA_OUT, BIT6),
    _RVM1(0x0499, PAD_DDCDD_DA_OEN, BIT5),
    //reg_ej_mode
    _RVM1(0x2ec4, 0, BIT0 ),   //reg[2ec4]#0 = 0
    //reg_ej_mode
    _RVM1(0x2ec4, 0, BIT1 ),   //reg[2ec4]#1 = 0
    //reg_gpio2d3_en
    _RVM1(0x0499, BIT7 , BIT7 ),   //reg[0499]#7 = 1
    #endif

    #if(PAD_SAR0_IS_GPIO != GPIO_NONE)
    #define PAD_SAR0_OEN (PAD_SAR0_IS_GPIO == GPIO_IN ? BIT0: 0)
    #define PAD_SAR0_OUT (PAD_SAR0_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
    _RVM1(0x1424, PAD_SAR0_OUT, BIT0),
    _RVM1(0x1423, PAD_SAR0_OEN, BIT0),
    //reg_sar_aisel[0]
    _RVM1(0x1422, 0, BIT0 ),   //reg[1422]#0 = 0
    #endif

    #if(PAD_SAR1_IS_GPIO != GPIO_NONE)
    #define PAD_SAR1_OEN (PAD_SAR1_IS_GPIO == GPIO_IN ? BIT1: 0)
    #define PAD_SAR1_OUT (PAD_SAR1_IS_GPIO == GPIO_OUT_HIGH ? BIT1: 0)
    _RVM1(0x1424, PAD_SAR1_OUT, BIT1),
    _RVM1(0x1423, PAD_SAR1_OEN, BIT1),
    //reg_sar_aisel[1]
    _RVM1(0x1422, 0, BIT1 ),   //reg[1422]#1 = 0
    #endif

    #if(PAD_SAR2_IS_GPIO != GPIO_NONE)
    #define PAD_SAR2_OEN (PAD_SAR2_IS_GPIO == GPIO_IN ? BIT2: 0)
    #define PAD_SAR2_OUT (PAD_SAR2_IS_GPIO == GPIO_OUT_HIGH ? BIT2: 0)
    _RVM1(0x1424, PAD_SAR2_OUT, BIT2),
    _RVM1(0x1423, PAD_SAR2_OEN, BIT2),
    //reg_sar_aisel[2]
    _RVM1(0x1422, 0, BIT2 ),   //reg[1422]#2 = 0
    #endif

    #if(PAD_SAR3_IS_GPIO != GPIO_NONE)
    #define PAD_SAR3_OEN (PAD_SAR3_IS_GPIO == GPIO_IN ? BIT3: 0)
    #define PAD_SAR3_OUT (PAD_SAR3_IS_GPIO == GPIO_OUT_HIGH ? BIT3: 0)
    _RVM1(0x1424, PAD_SAR3_OUT, BIT3),
    _RVM1(0x1423, PAD_SAR3_OEN, BIT3),
    //reg_sar_aisel[3]
    _RVM1(0x1422, 0, BIT3 ),   //reg[1422]#3 = 0
    #endif

    #if(PAD_SAR4_IS_GPIO != GPIO_NONE)
    #define PAD_SAR4_OEN (PAD_SAR4_IS_GPIO == GPIO_IN ? BIT4: 0)
    #define PAD_SAR4_OUT (PAD_SAR4_IS_GPIO == GPIO_OUT_HIGH ? BIT4: 0)
    _RVM1(0x1424, PAD_SAR4_OUT, BIT4),
    _RVM1(0x1423, PAD_SAR4_OEN, BIT4),
    //reg_sar_aisel[4]
    _RVM1(0x1422, 0, BIT4 ),   //reg[1422]#4 = 0
    #endif

    #if(PAD_PWM_PM_IS_GPIO != GPIO_NONE)
    #define PAD_PWM_PM_OEN (PAD_PWM_PM_IS_GPIO == GPIO_IN ? BIT0: 0)
    #define PAD_PWM_PM_OUT (PAD_PWM_PM_IS_GPIO == GPIO_OUT_HIGH ? BIT1: 0)
    _RVM1(0x0f28, PAD_PWM_PM_OUT, BIT1),
    _RVM1(0x0f28, PAD_PWM_PM_OEN, BIT0),
    //reg_pwm_pm_is_gpio
    _RVM1(0x0e38, BIT5 , BIT5 ),   //reg[0e38]#5 = 1
    #endif

    #if(PAD_DDCA_CK_IS_GPIO != GPIO_NONE)
    #define PAD_DDCA_CK_OEN (PAD_DDCA_CK_IS_GPIO == GPIO_IN ? BIT1: 0)
    #define PAD_DDCA_CK_OUT (PAD_DDCA_CK_IS_GPIO == GPIO_OUT_HIGH ? BIT2: 0)
    _RVM1(0x0494, PAD_DDCA_CK_OUT, BIT2),
    _RVM1(0x0494, PAD_DDCA_CK_OEN, BIT1),
    //reg_gpio2a0_en
    _RVM1(0x0494, BIT7 , BIT7 ),   //reg[0494]#7 = 1
    #endif

    #if(PAD_DDCA_DA_IS_GPIO != GPIO_NONE)
    #define PAD_DDCA_DA_OEN (PAD_DDCA_DA_IS_GPIO == GPIO_IN ? BIT5: 0)
    #define PAD_DDCA_DA_OUT (PAD_DDCA_DA_IS_GPIO == GPIO_OUT_HIGH ? BIT6: 0)
    _RVM1(0x0494, PAD_DDCA_DA_OUT, BIT6),
    _RVM1(0x0494, PAD_DDCA_DA_OEN, BIT5),
    //reg_gpio2a0_en
    _RVM1(0x0494, BIT7 , BIT7 ),   //reg[0494]#7 = 1
    #endif

    #if(PAD_WOL_INT_OUT_IS_GPIO != GPIO_NONE)
    #define PAD_WOL_INT_OUT_OEN (PAD_WOL_INT_OUT_IS_GPIO == GPIO_IN ? BIT0: 0)
    #define PAD_WOL_INT_OUT_OUT (PAD_WOL_INT_OUT_IS_GPIO == GPIO_OUT_HIGH ? BIT1: 0)
    _RVM1(0x0f3c, PAD_WOL_INT_OUT_OUT, BIT1),
    _RVM1(0x0f3c, PAD_WOL_INT_OUT_OEN, BIT0),
    //reg_wol_is_gpio
    _RVM1(0x0e39, BIT1 , BIT1 ),   //reg[0e39]#1 = 1
    #endif

    #if(PAD_LED0_IS_GPIO != GPIO_NONE)
    #define PAD_LED0_OEN (PAD_LED0_IS_GPIO == GPIO_IN ? BIT0: 0)
    #define PAD_LED0_OUT (PAD_LED0_IS_GPIO == GPIO_OUT_HIGH ? BIT1: 0)
    _RVM1(0x0f38, PAD_LED0_OUT, BIT1),
    _RVM1(0x0f38, PAD_LED0_OEN, BIT0),
    //reg_wol_is_gpio
    _RVM1(0x0e39, BIT1 , BIT1 ),   //reg[0e39]#1 = 1
    #endif

    #if(PAD_LED1_IS_GPIO != GPIO_NONE)
    #define PAD_LED1_OEN (PAD_LED1_IS_GPIO == GPIO_IN ? BIT0: 0)
    #define PAD_LED1_OUT (PAD_LED1_IS_GPIO == GPIO_OUT_HIGH ? BIT1: 0)
    _RVM1(0x0f3a, PAD_LED1_OUT, BIT1),
    _RVM1(0x0f3a, PAD_LED1_OEN, BIT0),
    //reg_wol_is_gpio
    _RVM1(0x0e39, BIT1 , BIT1 ),   //reg[0e39]#1 = 1
    #endif

    _MEMMAP_nonPM_,

    #if(PAD_GPIO0_IS_GPIO != GPIO_NONE)
    #define PAD_GPIO0_OEN (PAD_GPIO0_IS_GPIO == GPIO_IN ? BIT0: 0)
    #define PAD_GPIO0_OUT (PAD_GPIO0_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
    _RVM1(0x1e56, PAD_GPIO0_OUT, BIT0),
    _RVM1(0x1e5C, PAD_GPIO0_OEN, BIT0),
    //reg_tconconfig2[1:0]
    _RVM1(0x1EB9, 0, BIT5 | BIT4 ),   //reg[101EB9]#5 ~ #4 = 00b
    //reg_3dflagconfig[2:0]
    _RVM1(0x1EB3, 0, BIT7 | BIT6 | BIT5 ),   //reg[101EB3]#7 ~ #5 = 000b
    //reg_bt656_ctrl[1:0]
    _RVM1(0x1EDF, 0, BIT7 | BIT6 ),   //reg[101EDF]#7 ~ #6 = 00b
    //reg_usbdrvvbusconfig[1:0]
    _RVM1(0x1E04, 0, BIT1 | BIT0 ),   //reg[101E04]#1  ~ #0 = 00b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    //reg_test_out_mode[2:0]
    _RVM1(0x1E24, 0, BIT6 | BIT5 | BIT4 ),   //reg[101E24]#6  ~ #4 = 000b
    //reg_p1_enable_b0
    _RVM1(0x1EB8, 0, BIT0  ),   //reg[101EB8]#0  = 0b
    #endif

    #if(PAD_GPIO1_IS_GPIO != GPIO_NONE)
    #define PAD_GPIO1_OEN (PAD_GPIO1_IS_GPIO == GPIO_IN ? BIT1: 0)
    #define PAD_GPIO1_OUT (PAD_GPIO1_IS_GPIO == GPIO_OUT_HIGH ? BIT1: 0)
    _RVM1(0x1e56, PAD_GPIO1_OUT, BIT1),
    _RVM1(0x1e5C, PAD_GPIO1_OEN, BIT1),
    //reg_p1_enable_b1
    _RVM1(0x1EB8, 0, BIT1  ),   //reg[101EB8]#1  = 0b
    //reg_bt656_ctrl[1:0]
    _RVM1(0x1EDF, 0, BIT7 | BIT6 ),   //reg[101EDF]#7 ~ #6 = 00b
    //reg_tconconfig3[1:0]
    _RVM1(0x1EB9, 0, BIT7 | BIT6 ),   //reg[101EB9]#7 ~ #6 = 00b
    //reg_usbdrvvbusconfig[1:0]
    _RVM1(0x1E04, 0, BIT1 | BIT0 ),   //reg[101E04]#1  ~ #0 = 00b
    //reg_ld_spi3_config[1:0]
    _RVM1(0x1EB7, 0, BIT1 | BIT0 ),   //reg[101EB7]#1 ~ #0 = 00b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    //reg_test_out_mode[2:0]
    _RVM1(0x1E24, 0, BIT6 | BIT5 | BIT4 ),   //reg[101E24]#6  ~ #4 = 000b
    #endif

    #if(PAD_GPIO2_IS_GPIO != GPIO_NONE)
    #define PAD_GPIO2_OEN (PAD_GPIO2_IS_GPIO == GPIO_IN ? BIT2: 0)
    #define PAD_GPIO2_OUT (PAD_GPIO2_IS_GPIO == GPIO_OUT_HIGH ? BIT2: 0)
    _RVM1(0x1e56, PAD_GPIO2_OUT, BIT2),
    _RVM1(0x1e5C, PAD_GPIO2_OEN, BIT2),
    //reg_bt656_ctrl[1:0]
    _RVM1(0x1EDF, 0, BIT7 | BIT6 ),   //reg[101EDF]#7 ~ #6 = 00b
    //reg_ld_spi3_config[1:0]
    _RVM1(0x1EB7, 0, BIT1 | BIT0 ),   //reg[101EB7]#1 ~ #0 = 00b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    //reg_p1_enable_b2
    _RVM1(0x1EB8, 0, BIT2  ),   //reg[101EB8]#2  = 0b
    //reg_tconconfig4[1:0]
    _RVM1(0x1EBA, 0, BIT1 | BIT0 ),   //reg[101EBA]#1  ~ #0 = 00b
    //reg_ej_config[2:0]
    _RVM1(0x1E27, 0, BIT0 ),   //reg[101E27]#2 ~ #0 = 000b
    //reg_usbdrvvbusconfig[1:0]
    _RVM1(0x1E04, 0, BIT1 | BIT0 ),   //reg[101E04]#1  ~ #0 = 00b
    //reg_test_out_mode[2:0]
    _RVM1(0x1E24, 0, BIT6 | BIT5 | BIT4 ),   //reg[101E24]#6  ~ #4 = 000b
    #endif

    #if(PAD_GPIO3_IS_GPIO != GPIO_NONE)
    #define PAD_GPIO3_OEN (PAD_GPIO3_IS_GPIO == GPIO_IN ? BIT3: 0)
    #define PAD_GPIO3_OUT (PAD_GPIO3_IS_GPIO == GPIO_OUT_HIGH ? BIT3: 0)
    _RVM1(0x1e56, PAD_GPIO3_OUT, BIT3),
    _RVM1(0x1e5C, PAD_GPIO3_OEN, BIT3),
    //reg_od2nduart[1:0]
    _RVM1(0x1EB6, 0, BIT1 | BIT0 ),   //reg[101EB6]#1  ~ #0 = 00b
    //reg_bt656_ctrl[1:0]
    _RVM1(0x1EDF, 0, BIT7 | BIT6 ),   //reg[101EDF]#7 ~ #6 = 00b
    //reg_diseqc_out_config[1:0]
    _RVM1(0x1E25, 0, BIT7 | BIT6 ),   //reg[101E25]#7 ~ #6 = 00b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    //reg_ej_config[2:0]
    _RVM1(0x1E27, 0, BIT0 ),   //reg[101E27]#2 ~ #0 = 000b
    //reg_seconduartmode[1:0]
    _RVM1(0x1E05, 0, BIT1 | BIT0 ),   //reg[101E05]#1 ~ #0 = 00b
    //reg_usbdrvvbusconfig[1:0]
    _RVM1(0x1E04, 0, BIT1 | BIT0 ),   //reg[101E04]#1  ~ #0 = 00b
    //reg_p1_enable_b3
    _RVM1(0x1EB8, 0, BIT3  ),   //reg[101EB8]#3  = 0b
    //reg_test_out_mode[2:0]
    _RVM1(0x1E24, 0, BIT6 | BIT5 | BIT4 ),   //reg[101E24]#6  ~ #4 = 000b
    #endif

    #if(PAD_GPIO4_IS_GPIO != GPIO_NONE)
    #define PAD_GPIO4_OEN (PAD_GPIO4_IS_GPIO == GPIO_IN ? BIT4: 0)
    #define PAD_GPIO4_OUT (PAD_GPIO4_IS_GPIO == GPIO_OUT_HIGH ? BIT4: 0)
    _RVM1(0x1e56, PAD_GPIO4_OUT, BIT4),
    _RVM1(0x1e5C, PAD_GPIO4_OEN, BIT4),
    //reg_diseqc_in_config[1:0]
    _RVM1(0x1E25, 0, BIT5 | BIT4 ),   //reg[101E25]#5 ~ #4 = 00b
    //reg_od2nduart[1:0]
    _RVM1(0x1EB6, 0, BIT1 | BIT0 ),   //reg[101EB6]#1  ~ #0 = 00b
    //reg_bt656_ctrl[1:0]
    _RVM1(0x1EDF, 0, BIT7 | BIT6 ),   //reg[101EDF]#7 ~ #6 = 00b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    //reg_p1_enable_b4
    _RVM1(0x1EB8, 0, BIT4  ),   //reg[101EB8]#4  = 0b
    //reg_ej_config[2:0]
    _RVM1(0x1E27, 0, BIT0 ),   //reg[101E27]#2 ~ #0 = 000b
    //reg_seconduartmode[1:0]
    _RVM1(0x1E05, 0, BIT1 | BIT0 ),   //reg[101E05]#1 ~ #0 = 00b
    //reg_test_out_mode[2:0]
    _RVM1(0x1E24, 0, BIT6 | BIT5 | BIT4 ),   //reg[101E24]#6  ~ #4 = 000b
    #endif

    #if(PAD_GPIO5_IS_GPIO != GPIO_NONE)
    #define PAD_GPIO5_OEN (PAD_GPIO5_IS_GPIO == GPIO_IN ? BIT5: 0)
    #define PAD_GPIO5_OUT (PAD_GPIO5_IS_GPIO == GPIO_OUT_HIGH ? BIT5: 0)
    _RVM1(0x1e56, PAD_GPIO5_OUT, BIT5),
    _RVM1(0x1e5C, PAD_GPIO5_OEN, BIT5),
    //reg_ej_config[2:0]
    _RVM1(0x1E27, 0, BIT0 ),   //reg[101E27]#2 ~ #0 = 000b
    //reg_bt656_ctrl[1:0]
    _RVM1(0x1EDF, 0, BIT7 | BIT6 ),   //reg[101EDF]#7 ~ #6 = 00b
    //reg_odfastuart[1:0]
    _RVM1(0x1EB6, 0, BIT7 | BIT6 ),   //reg[101EB6]#7  ~ #6 = 00b
    //reg_fastuartmode[1:0]
    _RVM1(0x1E04, 0, BIT5 | BIT4 ),   //reg[101E04]#5  ~ #4 = 00b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    //reg_p1_enable_b5
    _RVM1(0x1EB8, 0, BIT5  ),   //reg[101EB8]#5  = 0b
    //reg_test_out_mode[2:0]
    _RVM1(0x1E24, 0, BIT6 | BIT5 | BIT4 ),   //reg[101E24]#6  ~ #4 = 000b
    #endif

    #if(PAD_GPIO6_IS_GPIO != GPIO_NONE)
    #define PAD_GPIO6_OEN (PAD_GPIO6_IS_GPIO == GPIO_IN ? BIT6: 0)
    #define PAD_GPIO6_OUT (PAD_GPIO6_IS_GPIO == GPIO_OUT_HIGH ? BIT6: 0)
    _RVM1(0x1e56, PAD_GPIO6_OUT, BIT6),
    _RVM1(0x1e5C, PAD_GPIO6_OEN, BIT6),
    //reg_bt656_ctrl[1:0]
    _RVM1(0x1EDF, 0, BIT7 | BIT6 ),   //reg[101EDF]#7 ~ #6 = 00b
    //reg_odfastuart[1:0]
    _RVM1(0x1EB6, 0, BIT7 | BIT6 ),   //reg[101EB6]#7  ~ #6 = 00b
    //reg_fastuartmode[1:0]
    _RVM1(0x1E04, 0, BIT5 | BIT4 ),   //reg[101E04]#5  ~ #4 = 00b
    //reg_p1_enable_b6
    _RVM1(0x1EB8, 0, BIT6  ),   //reg[101EB8]#6  = 0b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    //reg_test_out_mode[2:0]
    _RVM1(0x1E24, 0, BIT6 | BIT5 | BIT4 ),   //reg[101E24]#6  ~ #4 = 000b
    #endif

    #if(PAD_GPIO7_IS_GPIO != GPIO_NONE)
    #define PAD_GPIO7_OEN (PAD_GPIO7_IS_GPIO == GPIO_IN ? BIT7: 0)
    #define PAD_GPIO7_OUT (PAD_GPIO7_IS_GPIO == GPIO_OUT_HIGH ? BIT7: 0)
    _RVM1(0x1e56, PAD_GPIO7_OUT, BIT7),
    _RVM1(0x1e5C, PAD_GPIO7_OEN, BIT7),
    //reg_od2nduart[1:0]
    _RVM1(0x1EB6, 0, BIT1 | BIT0 ),   //reg[101EB6]#1  ~ #0 = 00b
    //reg_bt656_ctrl[1:0]
    _RVM1(0x1EDF, 0, BIT7 | BIT6 ),   //reg[101EDF]#7 ~ #6 = 00b
    //reg_seconduartmode[1:0]
    _RVM1(0x1E05, 0, BIT1 | BIT0 ),   //reg[101E05]#1 ~ #0 = 00b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    //reg_p1_enable_b7
    _RVM1(0x1EB8, 0, BIT7  ),   //reg[101EB8]#7  = 0b
    //reg_test_out_mode[2:0]
    _RVM1(0x1E24, 0, BIT6 | BIT5 | BIT4 ),   //reg[101E24]#6  ~ #4 = 000b
    #endif

    #if(PAD_GPIO8_IS_GPIO != GPIO_NONE)
    #define PAD_GPIO8_OEN (PAD_GPIO8_IS_GPIO == GPIO_IN ? BIT0: 0)
    #define PAD_GPIO8_OUT (PAD_GPIO8_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
    _RVM1(0x1e57, PAD_GPIO8_OUT, BIT0),
    _RVM1(0x1e5D, PAD_GPIO8_OEN, BIT0),
    //reg_od2nduart[1:0]
    _RVM1(0x1EB6, 0, BIT1 | BIT0 ),   //reg[101EB6]#1  ~ #0 = 00b
    //reg_bt656_ctrl[1:0]
    _RVM1(0x1EDF, 0, BIT7 | BIT6 ),   //reg[101EDF]#7 ~ #6 = 00b
    //reg_seconduartmode[1:0]
    _RVM1(0x1E05, 0, BIT1 | BIT0 ),   //reg[101E05]#1 ~ #0 = 00b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    //reg_test_out_mode[2:0]
    _RVM1(0x1E24, 0, BIT6 | BIT5 | BIT4 ),   //reg[101E24]#6  ~ #4 = 000b
    #endif

    #if(PAD_GPIO9_IS_GPIO != GPIO_NONE)
    #define PAD_GPIO9_OEN (PAD_GPIO9_IS_GPIO == GPIO_IN ? BIT1: 0)
    #define PAD_GPIO9_OUT (PAD_GPIO9_IS_GPIO == GPIO_OUT_HIGH ? BIT1: 0)
    _RVM1(0x1e57, PAD_GPIO9_OUT, BIT1),
    _RVM1(0x1e5D, PAD_GPIO9_OEN, BIT1),
    //reg_thirduartmode[1:0]
    _RVM1(0x1E05, 0, BIT3 | BIT2 ),   //reg[101E05]#3 ~ #2 = 00b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    //reg_od3rduart[1:0]
    _RVM1(0x1EB6, 0, BIT3 | BIT2 ),   //reg[101EB6]#3  ~ #2 = 00b
    //reg_test_out_mode[2:0]
    _RVM1(0x1E24, 0, BIT6 | BIT5 | BIT4 ),   //reg[101E24]#6  ~ #4 = 000b
    #endif

    #if(PAD_GPIO10_IS_GPIO != GPIO_NONE)
    #define PAD_GPIO10_OEN (PAD_GPIO10_IS_GPIO == GPIO_IN ? BIT2: 0)
    #define PAD_GPIO10_OUT (PAD_GPIO10_IS_GPIO == GPIO_OUT_HIGH ? BIT2: 0)
    _RVM1(0x1e57, PAD_GPIO10_OUT, BIT2),
    _RVM1(0x1e5D, PAD_GPIO10_OEN, BIT2),
    //reg_thirduartmode[1:0]
    _RVM1(0x1E05, 0, BIT3 | BIT2 ),   //reg[101E05]#3 ~ #2 = 00b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    //reg_od3rduart[1:0]
    _RVM1(0x1EB6, 0, BIT3 | BIT2 ),   //reg[101EB6]#3  ~ #2 = 00b
    //reg_test_out_mode[2:0]
    _RVM1(0x1E24, 0, BIT6 | BIT5 | BIT4 ),   //reg[101E24]#6  ~ #4 = 000b
    #endif

    #if(PAD_GPIO11_IS_GPIO != GPIO_NONE)
    #define PAD_GPIO11_OEN (PAD_GPIO11_IS_GPIO == GPIO_IN ? BIT3: 0)
    #define PAD_GPIO11_OUT (PAD_GPIO11_IS_GPIO == GPIO_OUT_HIGH ? BIT3: 0)
    _RVM1(0x1e57, PAD_GPIO11_OUT, BIT3),
    _RVM1(0x1e5D, PAD_GPIO11_OEN, BIT3),
    //reg_od4thuart[1:0]
    _RVM1(0x1EB6, 0, BIT5 | BIT4 ),   //reg[101EB6]#5  ~ #4 = 00b
    //reg_fourthuartmode[1:0]
    _RVM1(0x1E04, 0, BIT7 | BIT6 ),   //reg[101E04]#7  ~ #6 = 00b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    #endif

    #if(PAD_GPIO12_IS_GPIO != GPIO_NONE)
    #define PAD_GPIO12_OEN (PAD_GPIO12_IS_GPIO == GPIO_IN ? BIT4: 0)
    #define PAD_GPIO12_OUT (PAD_GPIO12_IS_GPIO == GPIO_OUT_HIGH ? BIT4: 0)
    _RVM1(0x1e57, PAD_GPIO12_OUT, BIT4),
    _RVM1(0x1e5D, PAD_GPIO12_OEN, BIT4),
    //reg_od4thuart[1:0]
    _RVM1(0x1EB6, 0, BIT5 | BIT4 ),   //reg[101EB6]#5  ~ #4 = 00b
    //reg_fourthuartmode[1:0]
    _RVM1(0x1E04, 0, BIT7 | BIT6 ),   //reg[101E04]#7  ~ #6 = 00b
    //reg_mpif_mode[1:0]
    _RVM1(0x1EB5, 0, BIT7 | BIT6 ),   //reg[101EB5]#7 ~ #6 = 00b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    #endif

    #if(PAD_GPIO13_IS_GPIO != GPIO_NONE)
    #define PAD_GPIO13_OEN (PAD_GPIO13_IS_GPIO == GPIO_IN ? BIT5: 0)
    #define PAD_GPIO13_OUT (PAD_GPIO13_IS_GPIO == GPIO_OUT_HIGH ? BIT5: 0)
    _RVM1(0x1e57, PAD_GPIO13_OUT, BIT5),
    _RVM1(0x1e5D, PAD_GPIO13_OEN, BIT5),
    //reg_diseqc_out_eco
    _RVM1(0x1E38, 0, BIT5  ),   //reg[101E38]#5  = 0b
    //reg_odfastuart[1:0]
    _RVM1(0x1EB6, 0, BIT7 | BIT6 ),   //reg[101EB6]#7  ~ #6 = 00b
    //reg_mpif_mode[1:0]
    _RVM1(0x1EB5, 0, BIT7 | BIT6 ),   //reg[101EB5]#7 ~ #6 = 00b
    //reg_fastuartmode[1:0]
    _RVM1(0x1E04, 0, BIT5 | BIT4 ),   //reg[101E04]#5  ~ #4 = 00b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    #endif

    #if(PAD_GPIO14_IS_GPIO != GPIO_NONE)
    #define PAD_GPIO14_OEN (PAD_GPIO14_IS_GPIO == GPIO_IN ? BIT6: 0)
    #define PAD_GPIO14_OUT (PAD_GPIO14_IS_GPIO == GPIO_OUT_HIGH ? BIT6: 0)
    _RVM1(0x1e57, PAD_GPIO14_OUT, BIT6),
    _RVM1(0x1e5D, PAD_GPIO14_OEN, BIT6),
    //reg_odfastuart[1:0]
    _RVM1(0x1EB6, 0, BIT7 | BIT6 ),   //reg[101EB6]#7  ~ #6 = 00b
    //reg_mpif_mode[1:0]
    _RVM1(0x1EB5, 0, BIT7 | BIT6 ),   //reg[101EB5]#7 ~ #6 = 00b
    //reg_fastuartmode[1:0]
    _RVM1(0x1E04, 0, BIT5 | BIT4 ),   //reg[101E04]#5  ~ #4 = 00b
    //reg_miic_mode2[1:0]
    _RVM1(0x1EB5, 0, BIT5 | BIT4 ),   //reg[101EB5]#5 ~ #4 = 00b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    #endif

    #if(PAD_GPIO15_IS_GPIO != GPIO_NONE)
    #define PAD_GPIO15_OEN (PAD_GPIO15_IS_GPIO == GPIO_IN ? BIT7: 0)
    #define PAD_GPIO15_OUT (PAD_GPIO15_IS_GPIO == GPIO_OUT_HIGH ? BIT7: 0)
    _RVM1(0x1e57, PAD_GPIO15_OUT, BIT7),
    _RVM1(0x1e5D, PAD_GPIO15_OEN, BIT7),
    //reg_mpif_mode[1:0]
    _RVM1(0x1EB5, 0, BIT7 | BIT6 ),   //reg[101EB5]#7 ~ #6 = 00b
    //reg_i2smutemode[1:0]
    _RVM1(0x1E05, 0, BIT7 | BIT6 ),   //reg[101E05]#7 ~ #6 = 00b
    //reg_miic_mode2[1:0]
    _RVM1(0x1EB5, 0, BIT5 | BIT4 ),   //reg[101EB5]#5 ~ #4 = 00b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    #endif

    #if(PAD_GPIO16_IS_GPIO != GPIO_NONE)
    #define PAD_GPIO16_OEN (PAD_GPIO16_IS_GPIO == GPIO_IN ? BIT0: 0)
    #define PAD_GPIO16_OUT (PAD_GPIO16_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
    _RVM1(0x1e58, PAD_GPIO16_OUT, BIT0),
    _RVM1(0x1e5E, PAD_GPIO16_OEN, BIT0),
    //reg_tserrout[1:0]
    _RVM1(0x1EA2, 0, BIT7 | BIT6 ),   //reg[101EA2]#7  ~ #6 = 00b
    //reg_mpif_mode[1:0]
    _RVM1(0x1EB5, 0, BIT7 | BIT6 ),   //reg[101EB5]#7 ~ #6 = 00b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    #endif

    #if(PAD_GPIO17_IS_GPIO != GPIO_NONE)
    #define PAD_GPIO17_OEN (PAD_GPIO17_IS_GPIO == GPIO_IN ? BIT1: 0)
    #define PAD_GPIO17_OUT (PAD_GPIO17_IS_GPIO == GPIO_OUT_HIGH ? BIT1: 0)
    _RVM1(0x1e58, PAD_GPIO17_OUT, BIT1),
    _RVM1(0x1e5E, PAD_GPIO17_OEN, BIT1),
    //reg_miic_mode0[1:0]
    _RVM1(0x1EB5, 0, BIT1 | BIT0 ),   //reg[101EB5]#1 ~ #0 = 00b
    //reg_mpif_mode[1:0]
    _RVM1(0x1EB5, 0, BIT7 | BIT6 ),   //reg[101EB5]#7 ~ #6 = 00b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    #endif

    #if(PAD_GPIO18_IS_GPIO != GPIO_NONE)
    #define PAD_GPIO18_OEN (PAD_GPIO18_IS_GPIO == GPIO_IN ? BIT2: 0)
    #define PAD_GPIO18_OUT (PAD_GPIO18_IS_GPIO == GPIO_OUT_HIGH ? BIT2: 0)
    _RVM1(0x1e58, PAD_GPIO18_OUT, BIT2),
    _RVM1(0x1e5E, PAD_GPIO18_OEN, BIT2),
    //reg_miic_mode0[1:0]
    _RVM1(0x1EB5, 0, BIT1 | BIT0 ),   //reg[101EB5]#1 ~ #0 = 00b
    //reg_mpif_mode[1:0]
    _RVM1(0x1EB5, 0, BIT7 | BIT6 ),   //reg[101EB5]#7 ~ #6 = 00b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    #endif

    #if(PAD_GPIO19_IS_GPIO != GPIO_NONE)
    #define PAD_GPIO19_OEN (PAD_GPIO19_IS_GPIO == GPIO_IN ? BIT3: 0)
    #define PAD_GPIO19_OUT (PAD_GPIO19_IS_GPIO == GPIO_OUT_HIGH ? BIT3: 0)
    _RVM1(0x1e58, PAD_GPIO19_OUT, BIT3),
    _RVM1(0x1e5E, PAD_GPIO19_OEN, BIT3),
    //reg_agc_dbg
    _RVM1(0x1E06, 0, BIT5  ),   //reg[101E06]#5  = 0b
    //reg_led_mode[1:0]
    _RVM1(0x1EB4, 0, BIT5 | BIT4 ),   //reg[101EB4]#5  ~ #4 = 00b
    //reg_od2nduart[1:0]
    _RVM1(0x1EB6, 0, BIT1 | BIT0 ),   //reg[101EB6]#1  ~ #0 = 00b
    //reg_et_test_mode[1:0]
    _RVM1(0x1EDF, 0, BIT3 | BIT2 ),   //reg[101EDF]#3 ~ #2 = 00b
    //reg_seconduartmode[1:0]
    _RVM1(0x1E05, 0, BIT1 | BIT0 ),   //reg[101E05]#1 ~ #0 = 00b
    //reg_et_mode
    _RVM1(0x1EDF, 0, BIT0 ),   //reg[101EDF]#0 = 0b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    #endif

    #if(PAD_GPIO20_IS_GPIO != GPIO_NONE)
    #define PAD_GPIO20_OEN (PAD_GPIO20_IS_GPIO == GPIO_IN ? BIT4: 0)
    #define PAD_GPIO20_OUT (PAD_GPIO20_IS_GPIO == GPIO_OUT_HIGH ? BIT4: 0)
    _RVM1(0x1e58, PAD_GPIO20_OUT, BIT4),
    _RVM1(0x1e5E, PAD_GPIO20_OEN, BIT4),
    //reg_agc_dbg
    _RVM1(0x1E06, 0, BIT5  ),   //reg[101E06]#5  = 0b
    //reg_led_mode[1:0]
    _RVM1(0x1EB4, 0, BIT5 | BIT4 ),   //reg[101EB4]#5  ~ #4 = 00b
    //reg_od2nduart[1:0]
    _RVM1(0x1EB6, 0, BIT1 | BIT0 ),   //reg[101EB6]#1  ~ #0 = 00b
    //reg_et_test_mode[1:0]
    _RVM1(0x1EDF, 0, BIT3 | BIT2 ),   //reg[101EDF]#3 ~ #2 = 00b
    //reg_seconduartmode[1:0]
    _RVM1(0x1E05, 0, BIT1 | BIT0 ),   //reg[101E05]#1 ~ #0 = 00b
    //reg_et_mode
    _RVM1(0x1EDF, 0, BIT0 ),   //reg[101EDF]#0 = 0b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    #endif

    #if(PAD_GPIO21_IS_GPIO != GPIO_NONE)
    #define PAD_GPIO21_OEN (PAD_GPIO21_IS_GPIO == GPIO_IN ? BIT5: 0)
    #define PAD_GPIO21_OUT (PAD_GPIO21_IS_GPIO == GPIO_OUT_HIGH ? BIT5: 0)
    _RVM1(0x1e58, PAD_GPIO21_OUT, BIT5),
    _RVM1(0x1e5E, PAD_GPIO21_OEN, BIT5),
    //reg_et_test_mode[1:0]
    _RVM1(0x1EDF, 0, BIT3 | BIT2 ),   //reg[101EDF]#3 ~ #2 = 00b
    //reg_extint5
    _RVM1(0x1EE0, 0, BIT5  ),   //reg[101EE0]#5  = 0b
    //reg_et_mode
    _RVM1(0x1EDF, 0, BIT0 ),   //reg[101EDF]#0 = 0b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    //reg_od3rduart[1:0]
    _RVM1(0x1EB6, 0, BIT3 | BIT2 ),   //reg[101EB6]#3  ~ #2 = 00b
    #endif

    #if(PAD_GPIO22_IS_GPIO != GPIO_NONE)
    #define PAD_GPIO22_OEN (PAD_GPIO22_IS_GPIO == GPIO_IN ? BIT6: 0)
    #define PAD_GPIO22_OUT (PAD_GPIO22_IS_GPIO == GPIO_OUT_HIGH ? BIT6: 0)
    _RVM1(0x1e58, PAD_GPIO22_OUT, BIT6),
    _RVM1(0x1e5E, PAD_GPIO22_OEN, BIT6),
    //reg_et_test_mode[1:0]
    _RVM1(0x1EDF, 0, BIT3 | BIT2 ),   //reg[101EDF]#3 ~ #2 = 00b
    //reg_et_mode
    _RVM1(0x1EDF, 0, BIT0 ),   //reg[101EDF]#0 = 0b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    //reg_od3rduart[1:0]
    _RVM1(0x1EB6, 0, BIT3 | BIT2 ),   //reg[101EB6]#3  ~ #2 = 00b
    #endif

    #if(PAD_GPIO23_IS_GPIO != GPIO_NONE)
    #define PAD_GPIO23_OEN (PAD_GPIO23_IS_GPIO == GPIO_IN ? BIT7: 0)
    #define PAD_GPIO23_OUT (PAD_GPIO23_IS_GPIO == GPIO_OUT_HIGH ? BIT7: 0)
    _RVM1(0x1e58, PAD_GPIO23_OUT, BIT7),
    _RVM1(0x1e5E, PAD_GPIO23_OEN, BIT7),
    //reg_et_test_mode[1:0]
    _RVM1(0x1EDF, 0, BIT3 | BIT2 ),   //reg[101EDF]#3 ~ #2 = 00b
    //reg_od4thuart[1:0]
    _RVM1(0x1EB6, 0, BIT5 | BIT4 ),   //reg[101EB6]#5  ~ #4 = 00b
    //reg_fourthuartmode[1:0]
    _RVM1(0x1E04, 0, BIT7 | BIT6 ),   //reg[101E04]#7  ~ #6 = 00b
    //reg_et_mode
    _RVM1(0x1EDF, 0, BIT0 ),   //reg[101EDF]#0 = 0b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    #endif

    #if(PAD_GPIO24_IS_GPIO != GPIO_NONE)
    #define PAD_GPIO24_OEN (PAD_GPIO24_IS_GPIO == GPIO_IN ? BIT0: 0)
    #define PAD_GPIO24_OUT (PAD_GPIO24_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
    _RVM1(0x1e59, PAD_GPIO24_OUT, BIT0),
    _RVM1(0x1e5F, PAD_GPIO24_OEN, BIT0),
    //reg_et_test_mode[1:0]
    _RVM1(0x1EDF, 0, BIT3 | BIT2 ),   //reg[101EDF]#3 ~ #2 = 00b
    //reg_fourthuartmode[1:0]
    _RVM1(0x1E04, 0, BIT7 | BIT6 ),   //reg[101E04]#7  ~ #6 = 00b
    //reg_usbdrvvbusconfig[1:0]
    _RVM1(0x1E04, 0, BIT1 | BIT0 ),   //reg[101E04]#1  ~ #0 = 00b
    //reg_et_mode
    _RVM1(0x1EDF, 0, BIT0 ),   //reg[101EDF]#0 = 0b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    #endif

    #if(PAD_GPIO25_IS_GPIO != GPIO_NONE)
    #define PAD_GPIO25_OEN (PAD_GPIO25_IS_GPIO == GPIO_IN ? BIT1: 0)
    #define PAD_GPIO25_OUT (PAD_GPIO25_IS_GPIO == GPIO_OUT_HIGH ? BIT1: 0)
    _RVM1(0x1e59, PAD_GPIO25_OUT, BIT1),
    _RVM1(0x1e5F, PAD_GPIO25_OEN, BIT1),
    //reg_et_test_mode[1:0]
    _RVM1(0x1EDF, 0, BIT3 | BIT2 ),   //reg[101EDF]#3 ~ #2 = 00b
    //reg_odfastuart[1:0]
    _RVM1(0x1EB6, 0, BIT7 | BIT6 ),   //reg[101EB6]#7  ~ #6 = 00b
    //reg_fastuartmode[1:0]
    _RVM1(0x1E04, 0, BIT5 | BIT4 ),   //reg[101E04]#5  ~ #4 = 00b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    //reg_usbdrvvbusconfig[1:0]
    _RVM1(0x1E04, 0, BIT1 | BIT0 ),   //reg[101E04]#1  ~ #0 = 00b
    //reg_et_mode
    _RVM1(0x1EDF, 0, BIT0 ),   //reg[101EDF]#0 = 0b
    //reg_mdio_mode
    _RVM1(0x1EEF, 0, BIT0 ),   //reg[101EEF]#0 = 0b
    //reg_extint1
    _RVM1(0x1EE0, 0, BIT1  ),   //reg[101EE0]#1  = 0b
    #endif

    #if(PAD_GPIO26_IS_GPIO != GPIO_NONE)
    #define PAD_GPIO26_OEN (PAD_GPIO26_IS_GPIO == GPIO_IN ? BIT2: 0)
    #define PAD_GPIO26_OUT (PAD_GPIO26_IS_GPIO == GPIO_OUT_HIGH ? BIT2: 0)
    _RVM1(0x1e59, PAD_GPIO26_OUT, BIT2),
    _RVM1(0x1e5F, PAD_GPIO26_OEN, BIT2),
    //reg_et_test_mode[1:0]
    _RVM1(0x1EDF, 0, BIT3 | BIT2 ),   //reg[101EDF]#3 ~ #2 = 00b
    //reg_odfastuart[1:0]
    _RVM1(0x1EB6, 0, BIT7 | BIT6 ),   //reg[101EB6]#7  ~ #6 = 00b
    //reg_fastuartmode[1:0]
    _RVM1(0x1E04, 0, BIT5 | BIT4 ),   //reg[101E04]#5  ~ #4 = 00b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    //reg_led_mode[1:0]
    _RVM1(0x1EB4, 0, BIT5 | BIT4 ),   //reg[101EB4]#5  ~ #4 = 00b
    //reg_extint7
    _RVM1(0x1EE0, 0, BIT7  ),   //reg[101EE0]#7  = 0b
    //reg_usbdrvvbusconfig[1:0]
    _RVM1(0x1E04, 0, BIT1 | BIT0 ),   //reg[101E04]#1  ~ #0 = 00b
    //reg_et_mode
    _RVM1(0x1EDF, 0, BIT0 ),   //reg[101EDF]#0 = 0b
    //reg_mdio_mode
    _RVM1(0x1EEF, 0, BIT0 ),   //reg[101EEF]#0 = 0b
    #endif

    #if(PAD_GPIO27_IS_GPIO != GPIO_NONE)
    #define PAD_GPIO27_OEN (PAD_GPIO27_IS_GPIO == GPIO_IN ? BIT3: 0)
    #define PAD_GPIO27_OUT (PAD_GPIO27_IS_GPIO == GPIO_OUT_HIGH ? BIT3: 0)
    _RVM1(0x1e59, PAD_GPIO27_OUT, BIT3),
    _RVM1(0x1e5F, PAD_GPIO27_OEN, BIT3),
    //reg_led_mode[1:0]
    _RVM1(0x1EB4, 0, BIT5 | BIT4 ),   //reg[101EB4]#5  ~ #4 = 00b
    //reg_et_test_mode[1:0]
    _RVM1(0x1EDF, 0, BIT3 | BIT2 ),   //reg[101EDF]#3 ~ #2 = 00b
    //reg_extint4
    _RVM1(0x1EE0, 0, BIT4  ),   //reg[101EE0]#4  = 0b
    //reg_usbdrvvbusconfig[1:0]
    _RVM1(0x1E04, 0, BIT1 | BIT0 ),   //reg[101E04]#1  ~ #0 = 00b
    //reg_et_mode
    _RVM1(0x1EDF, 0, BIT0 ),   //reg[101EDF]#0 = 0b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    #endif

    #if(PAD_UART_RX2_IS_GPIO != GPIO_NONE)
    #define PAD_UART_RX2_OEN (PAD_UART_RX2_IS_GPIO == GPIO_IN ? BIT1: 0)
    #define PAD_UART_RX2_OUT (PAD_UART_RX2_IS_GPIO == GPIO_OUT_HIGH ? BIT1: 0)
    _MEMMAP_nonPM_SPARE0_,
    _RVM1(0x1000, PAD_UART_RX2_OUT, BIT1),
    _RVM1(0x1002, PAD_UART_RX2_OEN, BIT1),
    _MEMMAP_nonPM_,
    //reg_thirduartmode[1:0]
    _RVM1(0x1E05, 0, BIT3 | BIT2 ),   //reg[101E05]#3 ~ #2 = 00b
    //reg_tconconfig1[1:0]
    _RVM1(0x1EB9, 0, BIT3 | BIT2 ),   //reg[101EB9]#3 ~ #2 = 00b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    #endif

    #if(PAD_UART_TX2_IS_GPIO != GPIO_NONE)
    #define PAD_UART_TX2_OEN (PAD_UART_TX2_IS_GPIO == GPIO_IN ? BIT0: 0)
    #define PAD_UART_TX2_OUT (PAD_UART_TX2_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
    _MEMMAP_nonPM_SPARE0_,
    _RVM1(0x1000, PAD_UART_TX2_OUT, BIT0),
    _RVM1(0x1002, PAD_UART_TX2_OEN, BIT0),
    _MEMMAP_nonPM_,
    //reg_thirduartmode[1:0]
    _RVM1(0x1E05, 0, BIT3 | BIT2 ),   //reg[101E05]#3 ~ #2 = 00b
    //reg_tconconfig0[1:0]
    _RVM1(0x1EB9, 0, BIT1 | BIT0 ),   //reg[101EB9]#1 ~ #0 = 00b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    #endif

    #if(PAD_PWM0_IS_GPIO != GPIO_NONE)
    #define PAD_PWM0_OEN (PAD_PWM0_IS_GPIO == GPIO_IN ? BIT0: 0)
    #define PAD_PWM0_OUT (PAD_PWM0_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
    _MEMMAP_nonPM_SPARE0_,
    _RVM1(0x1098, PAD_PWM0_OUT, BIT0),
    _RVM1(0x1096, PAD_PWM0_OEN, BIT0),
    _MEMMAP_nonPM_,
    //reg_pwm0_mode[1:0]
    _RVM1(0x1EA2, 0, BIT1 | BIT0 ),   //reg[101EA2]#1  ~ #0 = 00b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    #endif

    #if(PAD_PWM1_IS_GPIO != GPIO_NONE)
    #define PAD_PWM1_OEN (PAD_PWM1_IS_GPIO == GPIO_IN ? BIT1: 0)
    #define PAD_PWM1_OUT (PAD_PWM1_IS_GPIO == GPIO_OUT_HIGH ? BIT1: 0)
    _MEMMAP_nonPM_SPARE0_,
    _RVM1(0x1098, PAD_PWM1_OUT, BIT1),
    _RVM1(0x1096, PAD_PWM1_OEN, BIT1),
    _MEMMAP_nonPM_,
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    //reg_pwm1_mode[1:0]
    _RVM1(0x1EAA, 0, BIT5 | BIT4 ),   //reg[101EAA]#5  ~ #4 = 00b
    #endif

    #if(PAD_PWM2_IS_GPIO != GPIO_NONE)
    #define PAD_PWM2_OEN (PAD_PWM2_IS_GPIO == GPIO_IN ? BIT2: 0)
    #define PAD_PWM2_OUT (PAD_PWM2_IS_GPIO == GPIO_OUT_HIGH ? BIT2: 0)
    _MEMMAP_nonPM_SPARE0_,
    _RVM1(0x1098, PAD_PWM2_OUT, BIT2),
    _RVM1(0x1096, PAD_PWM2_OEN, BIT2),
    _MEMMAP_nonPM_,
    //reg_pwm2_mode
    _RVM1(0x1EA2, 0, BIT2  ),   //reg[101EA2]#2  = 0b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    #endif

    #if(PAD_PWM3_IS_GPIO != GPIO_NONE)
    #define PAD_PWM3_OEN (PAD_PWM3_IS_GPIO == GPIO_IN ? BIT3: 0)
    #define PAD_PWM3_OUT (PAD_PWM3_IS_GPIO == GPIO_OUT_HIGH ? BIT3: 0)
    _MEMMAP_nonPM_SPARE0_,
    _RVM1(0x1098, PAD_PWM3_OUT, BIT3),
    _RVM1(0x1096, PAD_PWM3_OEN, BIT3),
    _MEMMAP_nonPM_,
    //reg_pwm3_mode
    _RVM1(0x1EA2, 0, BIT3  ),   //reg[101EA2]#3  = 0b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    #endif

    #if(PAD_PWM4_IS_GPIO != GPIO_NONE)
    #define PAD_PWM4_OEN (PAD_PWM4_IS_GPIO == GPIO_IN ? BIT4: 0)
    #define PAD_PWM4_OUT (PAD_PWM4_IS_GPIO == GPIO_OUT_HIGH ? BIT4: 0)
    _MEMMAP_nonPM_SPARE0_,
    _RVM1(0x1098, PAD_PWM4_OUT, BIT4),
    _RVM1(0x1096, PAD_PWM4_OEN, BIT4),
    _MEMMAP_nonPM_,
    //reg_tserrout[1:0]
    _RVM1(0x1EA2, 0, BIT7 | BIT6 ),   //reg[101EA2]#7  ~ #6 = 00b
    //reg_i2smutemode[1:0]
    _RVM1(0x1E05, 0, BIT7 | BIT6 ),   //reg[101E05]#7 ~ #6 = 00b
    //reg_pwm4_mode
    _RVM1(0x1EA2, 0, BIT4  ),   //reg[101EA2]#4  = 0b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    #endif

    #if(PAD_DDCR_DA_IS_GPIO != GPIO_NONE)
    #define PAD_DDCR_DA_OEN (PAD_DDCR_DA_IS_GPIO == GPIO_IN ? BIT0: 0)
    #define PAD_DDCR_DA_OUT (PAD_DDCR_DA_IS_GPIO == GPIO_OUT_HIGH ? BIT2: 0)
    _MEMMAP_nonPM_SPARE0_,
    _RVM1(0x109a, PAD_DDCR_DA_OUT, BIT2),
    _RVM1(0x109a, PAD_DDCR_DA_OEN, BIT0),
    _MEMMAP_nonPM_,
    //reg_ddcrmode[2:0]
    _RVM1(0x1EAF, 0, BIT7 | BIT6 | BIT5 ),   //reg[101EAF]#7 ~ #5 = 000b
    //reg_test_in_mode[2:0]
    _RVM1(0x1E24, 0, BIT2 | BIT1 | BIT0 ),   //reg[101E24]#2  ~ #0 = 000b
    //reg_ej_config[2:0]
    _RVM1(0x1E27, 0, BIT1),   //reg[101E27]#2 ~ #0 = 000b
    //reg_ld_spi3_config[1:0]
    _RVM1(0x1EB7, 0, BIT1 | BIT0 ),   //reg[101EB7]#1 ~ #0 = 00b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    //reg_tconconfig5[1:0]
    _RVM1(0x1EBA, 0, BIT3 | BIT2 ),   //reg[101EBA]#3  ~ #2 = 00b
    //reg_test_out_mode[2:0]
    _RVM1(0x1E24, 0, BIT6 | BIT5 | BIT4 ),   //reg[101E24]#6  ~ #4 = 000b
    #endif

    #if(PAD_DDCR_CK_IS_GPIO != GPIO_NONE)
    #define PAD_DDCR_CK_OEN (PAD_DDCR_CK_IS_GPIO == GPIO_IN ? BIT1: 0)
    #define PAD_DDCR_CK_OUT (PAD_DDCR_CK_IS_GPIO == GPIO_OUT_HIGH ? BIT3: 0)
    _MEMMAP_nonPM_SPARE0_,
    _RVM1(0x109a, PAD_DDCR_CK_OUT, BIT3),
    _RVM1(0x109a, PAD_DDCR_CK_OEN, BIT1),
    _MEMMAP_nonPM_,
    //reg_ddcrmode[2:0]
    _RVM1(0x1EAF, 0, BIT7 | BIT6 | BIT5 ),   //reg[101EAF]#7 ~ #5 = 000b
    //reg_test_in_mode[2:0]
    _RVM1(0x1E24, 0, BIT2 | BIT1 | BIT0 ),   //reg[101E24]#2  ~ #0 = 000b
    //reg_ej_config[2:0]
    _RVM1(0x1E27, 0, BIT1),   //reg[101E27]#2 ~ #0 = 000b
    //reg_tconconfig6[1:0]
    _RVM1(0x1EBA, 0, BIT5 | BIT4 ),   //reg[101EBA]#5  ~ #4 = 00b
    //reg_ld_spi3_config[1:0]
    _RVM1(0x1EB7, 0, BIT1 | BIT0 ),   //reg[101EB7]#1 ~ #0 = 00b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    //reg_test_out_mode[2:0]
    _RVM1(0x1E24, 0, BIT6 | BIT5 | BIT4 ),   //reg[101E24]#6  ~ #4 = 000b
    #endif

    #if(PAD_TGPIO0_IS_GPIO != GPIO_NONE)
    #define PAD_TGPIO0_OEN (PAD_TGPIO0_IS_GPIO == GPIO_IN ? BIT0: 0)
    #define PAD_TGPIO0_OUT (PAD_TGPIO0_IS_GPIO == GPIO_OUT_HIGH ? BIT4: 0)
    _MEMMAP_nonPM_SPARE0_,
    _RVM1(0x109c, PAD_TGPIO0_OUT, BIT4),
    _RVM1(0x109c, PAD_TGPIO0_OEN, BIT0),
    _MEMMAP_nonPM_,
    //reg_diseqc_in_config[1:0]
    _RVM1(0x1E25, 0, BIT5 | BIT4 ),   //reg[101E25]#5 ~ #4 = 00b
    //reg_dspejtagmode[1:0]
    _RVM1(0x1E07, 0, BIT1 | BIT0 ),   //reg[101E07]#1 ~ #0 = 00b
    //reg_vsync_vif_out_en
    _RVM1(0x1EA3, 0, BIT6 ),   //reg[101EA3]#6 = 0b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    //reg_mcu_jtag_mode[1:0]
    _RVM1(0x1E06, 0, BIT7 | BIT6 ),   //reg[101E06]#7  ~ #6 = 00b
    #endif

    #if(PAD_TGPIO1_IS_GPIO != GPIO_NONE)
    #define PAD_TGPIO1_OEN (PAD_TGPIO1_IS_GPIO == GPIO_IN ? BIT1: 0)
    #define PAD_TGPIO1_OUT (PAD_TGPIO1_IS_GPIO == GPIO_OUT_HIGH ? BIT5: 0)
    _MEMMAP_nonPM_SPARE0_,
    _RVM1(0x109c, PAD_TGPIO1_OUT, BIT5),
    _RVM1(0x109c, PAD_TGPIO1_OEN, BIT1),
    _MEMMAP_nonPM_,
    //reg_dspejtagmode[1:0]
    _RVM1(0x1E07, 0, BIT1 | BIT0 ),   //reg[101E07]#1 ~ #0 = 00b
    //reg_diseqc_out_config[1:0]
    _RVM1(0x1E25, 0, BIT7 | BIT6 ),   //reg[101E25]#7 ~ #6 = 00b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    //reg_mcu_jtag_mode[1:0]
    _RVM1(0x1E06, 0, BIT7 | BIT6 ),   //reg[101E06]#7  ~ #6 = 00b
    #endif

    #if(PAD_TGPIO2_IS_GPIO != GPIO_NONE)
    #define PAD_TGPIO2_OEN (PAD_TGPIO2_IS_GPIO == GPIO_IN ? BIT2: 0)
    #define PAD_TGPIO2_OUT (PAD_TGPIO2_IS_GPIO == GPIO_OUT_HIGH ? BIT6: 0)
    _MEMMAP_nonPM_SPARE0_,
    _RVM1(0x109c, PAD_TGPIO2_OUT, BIT6),
    _RVM1(0x109c, PAD_TGPIO2_OEN, BIT2),
    _MEMMAP_nonPM_,
    //reg_dspejtagmode[1:0]
    _RVM1(0x1E07, 0, BIT1 | BIT0 ),   //reg[101E07]#1 ~ #0 = 00b
    //reg_test_in_mode[2:0]
    _RVM1(0x1E24, 0, BIT2 | BIT1 | BIT0 ),   //reg[101E24]#2  ~ #0 = 000b
    //reg_miic_mode1[1:0]
    _RVM1(0x1EB5, 0, BIT3 | BIT2 ),   //reg[101EB5]#3 ~ #2 = 00b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    //reg_test_out_mode[2:0]
    _RVM1(0x1E24, 0, BIT6 | BIT5 | BIT4 ),   //reg[101E24]#6  ~ #4 = 000b
    //reg_mcu_jtag_mode[1:0]
    _RVM1(0x1E06, 0, BIT7 | BIT6 ),   //reg[101E06]#7  ~ #6 = 00b
    #endif

    #if(PAD_TGPIO3_IS_GPIO != GPIO_NONE)
    #define PAD_TGPIO3_OEN (PAD_TGPIO3_IS_GPIO == GPIO_IN ? BIT3: 0)
    #define PAD_TGPIO3_OUT (PAD_TGPIO3_IS_GPIO == GPIO_OUT_HIGH ? BIT7: 0)
    _MEMMAP_nonPM_SPARE0_,
    _RVM1(0x109c, PAD_TGPIO3_OUT, BIT7),
    _RVM1(0x109c, PAD_TGPIO3_OEN, BIT3),
    _MEMMAP_nonPM_,
    //reg_dspejtagmode[1:0]
    _RVM1(0x1E07, 0, BIT1 | BIT0 ),   //reg[101E07]#1 ~ #0 = 00b
    //reg_test_in_mode[2:0]
    _RVM1(0x1E24, 0, BIT2 | BIT1 | BIT0 ),   //reg[101E24]#2  ~ #0 = 000b
    //reg_miic_mode1[1:0]
    _RVM1(0x1EB5, 0, BIT3 | BIT2 ),   //reg[101EB5]#3 ~ #2 = 00b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    //reg_test_out_mode[2:0]
    _RVM1(0x1E24, 0, BIT6 | BIT5 | BIT4 ),   //reg[101E24]#6  ~ #4 = 000b
    //reg_mcu_jtag_mode[1:0]
    _RVM1(0x1E06, 0, BIT7 | BIT6 ),   //reg[101E06]#7  ~ #6 = 00b
    #endif

    #if(PAD_TS0_D0_IS_GPIO != GPIO_NONE)
    #define PAD_TS0_D0_OEN (PAD_TS0_D0_IS_GPIO == GPIO_IN ? BIT0: 0)
    #define PAD_TS0_D0_OUT (PAD_TS0_D0_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
    _RVM1(0x1e64, PAD_TS0_D0_OUT, BIT0),
    _RVM1(0x1e68, PAD_TS0_D0_OEN, BIT0),
    //reg_ts0config[1:0]
    _RVM1(0x1EA3, 0, BIT2 ~ BIT1 ),   //reg[101EA3]#2 ~ #1 = 00b
    //reg_test_in_mode[2:0]
    _RVM1(0x1E24, 0, BIT2 | BIT1 | BIT0 ),   //reg[101E24]#2  ~ #0 = 000b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    //reg_test_out_mode[2:0]
    _RVM1(0x1E24, 0, BIT6 | BIT5 | BIT4 ),   //reg[101E24]#6  ~ #4 = 000b
    #endif

    #if(PAD_TS0_D1_IS_GPIO != GPIO_NONE)
    #define PAD_TS0_D1_OEN (PAD_TS0_D1_IS_GPIO == GPIO_IN ? BIT1: 0)
    #define PAD_TS0_D1_OUT (PAD_TS0_D1_IS_GPIO == GPIO_OUT_HIGH ? BIT1: 0)
    _RVM1(0x1e64, PAD_TS0_D1_OUT, BIT1),
    _RVM1(0x1e68, PAD_TS0_D1_OEN, BIT1),
    //reg_ts0config[1:0]
    _RVM1(0x1EA3, 0, BIT2 ~ BIT1 ),   //reg[101EA3]#2 ~ #1 = 00b
    //reg_test_in_mode[2:0]
    _RVM1(0x1E24, 0, BIT2 | BIT1 | BIT0 ),   //reg[101E24]#2  ~ #0 = 000b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    //reg_test_out_mode[2:0]
    _RVM1(0x1E24, 0, BIT6 | BIT5 | BIT4 ),   //reg[101E24]#6  ~ #4 = 000b
    #endif

    #if(PAD_TS0_D2_IS_GPIO != GPIO_NONE)
    #define PAD_TS0_D2_OEN (PAD_TS0_D2_IS_GPIO == GPIO_IN ? BIT2: 0)
    #define PAD_TS0_D2_OUT (PAD_TS0_D2_IS_GPIO == GPIO_OUT_HIGH ? BIT2: 0)
    _RVM1(0x1e64, PAD_TS0_D2_OUT, BIT2),
    _RVM1(0x1e68, PAD_TS0_D2_OEN, BIT2),
    //reg_ts0config[1:0]
    _RVM1(0x1EA3, 0, BIT2 ~ BIT1 ),   //reg[101EA3]#2 ~ #1 = 00b
    //reg_test_in_mode[2:0]
    _RVM1(0x1E24, 0, BIT2 | BIT1 | BIT0 ),   //reg[101E24]#2  ~ #0 = 000b
    //reg_ts2config[2:0]
    _RVM1(0x1EA8, 0, BIT6 | BIT5 | BIT4 ),   //reg[101EA8]#6  ~ #4 = 000b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    //reg_test_out_mode[2:0]
    _RVM1(0x1E24, 0, BIT6 | BIT5 | BIT4 ),   //reg[101E24]#6  ~ #4 = 000b
    #endif

    #if(PAD_TS0_D3_IS_GPIO != GPIO_NONE)
    #define PAD_TS0_D3_OEN (PAD_TS0_D3_IS_GPIO == GPIO_IN ? BIT3: 0)
    #define PAD_TS0_D3_OUT (PAD_TS0_D3_IS_GPIO == GPIO_OUT_HIGH ? BIT3: 0)
    _RVM1(0x1e64, PAD_TS0_D3_OUT, BIT3),
    _RVM1(0x1e68, PAD_TS0_D3_OEN, BIT3),
    //reg_ts0config[1:0]
    _RVM1(0x1EA3, 0, BIT2 ~ BIT1 ),   //reg[101EA3]#2 ~ #1 = 00b
    //reg_test_in_mode[2:0]
    _RVM1(0x1E24, 0, BIT2 | BIT1 | BIT0 ),   //reg[101E24]#2  ~ #0 = 000b
    //reg_ts2config[2:0]
    _RVM1(0x1EA8, 0, BIT6 | BIT5 | BIT4 ),   //reg[101EA8]#6  ~ #4 = 000b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    //reg_test_out_mode[2:0]
    _RVM1(0x1E24, 0, BIT6 | BIT5 | BIT4 ),   //reg[101E24]#6  ~ #4 = 000b
    #endif

    #if(PAD_TS0_D4_IS_GPIO != GPIO_NONE)
    #define PAD_TS0_D4_OEN (PAD_TS0_D4_IS_GPIO == GPIO_IN ? BIT4: 0)
    #define PAD_TS0_D4_OUT (PAD_TS0_D4_IS_GPIO == GPIO_OUT_HIGH ? BIT4: 0)
    _RVM1(0x1e64, PAD_TS0_D4_OUT, BIT4),
    _RVM1(0x1e68, PAD_TS0_D4_OEN, BIT4),
    //reg_ts0config[1:0]
    _RVM1(0x1EA3, 0, BIT2 ~ BIT1 ),   //reg[101EA3]#2 ~ #1 = 00b
    //reg_test_in_mode[2:0]
    _RVM1(0x1E24, 0, BIT2 | BIT1 | BIT0 ),   //reg[101E24]#2  ~ #0 = 000b
    //reg_ts2config[2:0]
    _RVM1(0x1EA8, 0, BIT6 | BIT5 | BIT4 ),   //reg[101EA8]#6  ~ #4 = 000b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    #endif

    #if(PAD_TS0_D5_IS_GPIO != GPIO_NONE)
    #define PAD_TS0_D5_OEN (PAD_TS0_D5_IS_GPIO == GPIO_IN ? BIT5: 0)
    #define PAD_TS0_D5_OUT (PAD_TS0_D5_IS_GPIO == GPIO_OUT_HIGH ? BIT5: 0)
    _RVM1(0x1e64, PAD_TS0_D5_OUT, BIT5),
    _RVM1(0x1e68, PAD_TS0_D5_OEN, BIT5),
    //reg_ts0config[1:0]
    _RVM1(0x1EA3, 0, BIT2 ~ BIT1 ),   //reg[101EA3]#2 ~ #1 = 00b
    //reg_test_in_mode[2:0]
    _RVM1(0x1E24, 0, BIT2 | BIT1 | BIT0 ),   //reg[101E24]#2  ~ #0 = 000b
    //reg_ts2config[2:0]
    _RVM1(0x1EA8, 0, BIT6 | BIT5 | BIT4 ),   //reg[101EA8]#6  ~ #4 = 000b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    #endif

    #if(PAD_TS0_D6_IS_GPIO != GPIO_NONE)
    #define PAD_TS0_D6_OEN (PAD_TS0_D6_IS_GPIO == GPIO_IN ? BIT6: 0)
    #define PAD_TS0_D6_OUT (PAD_TS0_D6_IS_GPIO == GPIO_OUT_HIGH ? BIT6: 0)
    _RVM1(0x1e64, PAD_TS0_D6_OUT, BIT6),
    _RVM1(0x1e68, PAD_TS0_D6_OEN, BIT6),
    //reg_ts0config[1:0]
    _RVM1(0x1EA3, 0, BIT2 ~ BIT1 ),   //reg[101EA3]#2 ~ #1 = 00b
    //reg_test_in_mode[2:0]
    _RVM1(0x1E24, 0, BIT2 | BIT1 | BIT0 ),   //reg[101E24]#2  ~ #0 = 000b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    #endif

    #if(PAD_TS0_D7_IS_GPIO != GPIO_NONE)
    #define PAD_TS0_D7_OEN (PAD_TS0_D7_IS_GPIO == GPIO_IN ? BIT7: 0)
    #define PAD_TS0_D7_OUT (PAD_TS0_D7_IS_GPIO == GPIO_OUT_HIGH ? BIT7: 0)
    _RVM1(0x1e64, PAD_TS0_D7_OUT, BIT7),
    _RVM1(0x1e68, PAD_TS0_D7_OEN, BIT7),
    //reg_ts0config[1:0]
    _RVM1(0x1EA3, 0, BIT2 ~ BIT1 ),   //reg[101EA3]#2 ~ #1 = 00b
    //reg_test_in_mode[2:0]
    _RVM1(0x1E24, 0, BIT2 | BIT1 | BIT0 ),   //reg[101E24]#2  ~ #0 = 000b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    #endif

    #if(PAD_TS0_VLD_IS_GPIO != GPIO_NONE)
    #define PAD_TS0_VLD_OEN (PAD_TS0_VLD_IS_GPIO == GPIO_IN ? BIT0: 0)
    #define PAD_TS0_VLD_OUT (PAD_TS0_VLD_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
    _RVM1(0x1e65, PAD_TS0_VLD_OUT, BIT0),
    _RVM1(0x1e69, PAD_TS0_VLD_OEN, BIT0),
    //reg_ts0config[1:0]
    _RVM1(0x1EA3, 0, BIT2 ~ BIT1 ),   //reg[101EA3]#2 ~ #1 = 00b
    //reg_test_in_mode[2:0]
    _RVM1(0x1E24, 0, BIT2 | BIT1 | BIT0 ),   //reg[101E24]#2  ~ #0 = 000b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    #endif

    #if(PAD_TS0_SYNC_IS_GPIO != GPIO_NONE)
    #define PAD_TS0_SYNC_OEN (PAD_TS0_SYNC_IS_GPIO == GPIO_IN ? BIT1: 0)
    #define PAD_TS0_SYNC_OUT (PAD_TS0_SYNC_IS_GPIO == GPIO_OUT_HIGH ? BIT1: 0)
    _RVM1(0x1e65, PAD_TS0_SYNC_OUT, BIT1),
    _RVM1(0x1e69, PAD_TS0_SYNC_OEN, BIT1),
    //reg_ts0config[1:0]
    _RVM1(0x1EA3, 0, BIT2 ~ BIT1 ),   //reg[101EA3]#2 ~ #1 = 00b
    //reg_test_in_mode[2:0]
    _RVM1(0x1E24, 0, BIT2 | BIT1 | BIT0 ),   //reg[101E24]#2  ~ #0 = 000b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    #endif

    #if(PAD_TS0_CLK_IS_GPIO != GPIO_NONE)
    #define PAD_TS0_CLK_OEN (PAD_TS0_CLK_IS_GPIO == GPIO_IN ? BIT2: 0)
    #define PAD_TS0_CLK_OUT (PAD_TS0_CLK_IS_GPIO == GPIO_OUT_HIGH ? BIT2: 0)
    _RVM1(0x1e65, PAD_TS0_CLK_OUT, BIT2),
    _RVM1(0x1e69, PAD_TS0_CLK_OEN, BIT2),
    //reg_ts0config[1:0]
    _RVM1(0x1EA3, 0, BIT2 ~ BIT1 ),   //reg[101EA3]#2 ~ #1 = 00b
    //reg_test_in_mode[2:0]
    _RVM1(0x1E24, 0, BIT2 | BIT1 | BIT0 ),   //reg[101E24]#2  ~ #0 = 000b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    #endif

    #if(PAD_TS1_D0_IS_GPIO != GPIO_NONE)
    #define PAD_TS1_D0_OEN (PAD_TS1_D0_IS_GPIO == GPIO_IN ? BIT2: 0)
    #define PAD_TS1_D0_OUT (PAD_TS1_D0_IS_GPIO == GPIO_OUT_HIGH ? BIT2: 0)
    _RVM1(0x1e4D, PAD_TS1_D0_OUT, BIT2),
    _RVM1(0x1e4F, PAD_TS1_D0_OEN, BIT2),
    //reg_test_in_mode[2:0]
    _RVM1(0x1E24, 0, BIT2 | BIT1 | BIT0 ),   //reg[101E24]#2  ~ #0 = 000b
    //reg_miic_mode1[1:0]
    _RVM1(0x1EB5, 0, BIT3 | BIT2 ),   //reg[101EB5]#3 ~ #2 = 00b
    //reg_bt656_ctrl[1:0]
    _RVM1(0x1EDF, 0, BIT7 | BIT6 ),   //reg[101EDF]#7 ~ #6 = 00b
    //reg_ts1config[2:0]
    _RVM1(0x1EA3, 0, BIT5 ~ BIT3 ),   //reg[101EA3]#5 ~ #3 = 000b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    //reg_ts_out_mode
    _RVM1(0x1EA3, 0, BIT0 ),   //reg[101EA3]#0 = 0b
    //reg_w_debug_out
    _RVM1(0x1EF0, 0, BIT2  ),   //reg[101EF0]#2  = 0b
    #endif

    #if(PAD_TS1_D1_IS_GPIO != GPIO_NONE)
    #define PAD_TS1_D1_OEN (PAD_TS1_D1_IS_GPIO == GPIO_IN ? BIT1: 0)
    #define PAD_TS1_D1_OUT (PAD_TS1_D1_IS_GPIO == GPIO_OUT_HIGH ? BIT1: 0)
    _RVM1(0x1e4D, PAD_TS1_D1_OUT, BIT1),
    _RVM1(0x1e4F, PAD_TS1_D1_OEN, BIT1),
    //reg_test_in_mode[2:0]
    _RVM1(0x1E24, 0, BIT2 | BIT1 | BIT0 ),   //reg[101E24]#2  ~ #0 = 000b
    //reg_sm_config[1:0]
    _RVM1(0x1EDC, 0, BIT5 | BIT4 ),   //reg[101EDC]#5  ~ #4 = 00b
    //reg_bt656_ctrl[1:0]
    _RVM1(0x1EDF, 0, BIT7 | BIT6 ),   //reg[101EDF]#7 ~ #6 = 00b
    //reg_ts1config[2:0]
    _RVM1(0x1EA3, 0, BIT5 ~ BIT3 ),   //reg[101EA3]#5 ~ #3 = 000b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    //reg_ts_out_mode
    _RVM1(0x1EA3, 0, BIT0 ),   //reg[101EA3]#0 = 0b
    //reg_w_debug_out
    _RVM1(0x1EF0, 0, BIT2  ),   //reg[101EF0]#2  = 0b
    #endif

    #if(PAD_TS1_D2_IS_GPIO != GPIO_NONE)
    #define PAD_TS1_D2_OEN (PAD_TS1_D2_IS_GPIO == GPIO_IN ? BIT0: 0)
    #define PAD_TS1_D2_OUT (PAD_TS1_D2_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
    _RVM1(0x1e4D, PAD_TS1_D2_OUT, BIT0),
    _RVM1(0x1e4F, PAD_TS1_D2_OEN, BIT0),
    //reg_test_in_mode[2:0]
    _RVM1(0x1E24, 0, BIT2 | BIT1 | BIT0 ),   //reg[101E24]#2  ~ #0 = 000b
    //reg_sm_config[1:0]
    _RVM1(0x1EDC, 0, BIT5 | BIT4 ),   //reg[101EDC]#5  ~ #4 = 00b
    //reg_bt656_ctrl[1:0]
    _RVM1(0x1EDF, 0, BIT7 | BIT6 ),   //reg[101EDF]#7 ~ #6 = 00b
    //reg_ts1config[2:0]
    _RVM1(0x1EA3, 0, BIT5 ~ BIT3 ),   //reg[101EA3]#5 ~ #3 = 000b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    //reg_ts_out_mode
    _RVM1(0x1EA3, 0, BIT0 ),   //reg[101EA3]#0 = 0b
    //reg_w_debug_out
    _RVM1(0x1EF0, 0, BIT2  ),   //reg[101EF0]#2  = 0b
    #endif

    #if(PAD_TS1_D3_IS_GPIO != GPIO_NONE)
    #define PAD_TS1_D3_OEN (PAD_TS1_D3_IS_GPIO == GPIO_IN ? BIT7: 0)
    #define PAD_TS1_D3_OUT (PAD_TS1_D3_IS_GPIO == GPIO_OUT_HIGH ? BIT7: 0)
    _RVM1(0x1e4C, PAD_TS1_D3_OUT, BIT7),
    _RVM1(0x1e4E, PAD_TS1_D3_OEN, BIT7),
    //reg_test_in_mode[2:0]
    _RVM1(0x1E24, 0, BIT2 | BIT1 | BIT0 ),   //reg[101E24]#2  ~ #0 = 000b
    //reg_sm_config[1:0]
    _RVM1(0x1EDC, 0, BIT5 | BIT4 ),   //reg[101EDC]#5  ~ #4 = 00b
    //reg_bt656_ctrl[1:0]
    _RVM1(0x1EDF, 0, BIT7 | BIT6 ),   //reg[101EDF]#7 ~ #6 = 00b
    //reg_ts1config[2:0]
    _RVM1(0x1EA3, 0, BIT5 ~ BIT3 ),   //reg[101EA3]#5 ~ #3 = 000b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    //reg_ts_out_mode
    _RVM1(0x1EA3, 0, BIT0 ),   //reg[101EA3]#0 = 0b
    //reg_w_debug_out
    _RVM1(0x1EF0, 0, BIT2  ),   //reg[101EF0]#2  = 0b
    #endif

    #if(PAD_TS1_D4_IS_GPIO != GPIO_NONE)
    #define PAD_TS1_D4_OEN (PAD_TS1_D4_IS_GPIO == GPIO_IN ? BIT6: 0)
    #define PAD_TS1_D4_OUT (PAD_TS1_D4_IS_GPIO == GPIO_OUT_HIGH ? BIT6: 0)
    _RVM1(0x1e4C, PAD_TS1_D4_OUT, BIT6),
    _RVM1(0x1e4E, PAD_TS1_D4_OEN, BIT6),
    //reg_dspejtagmode[1:0]
    _RVM1(0x1E07, 0, BIT1 | BIT0 ),   //reg[101E07]#1 ~ #0 = 00b
    //reg_bt656_ctrl[1:0]
    _RVM1(0x1EDF, 0, BIT7 | BIT6 ),   //reg[101EDF]#7 ~ #6 = 00b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    //reg_w_debug_out
    _RVM1(0x1EF0, 0, BIT2  ),   //reg[101EF0]#2  = 0b
    //reg_test_in_mode[2:0]
    _RVM1(0x1E24, 0, BIT2 | BIT1 | BIT0 ),   //reg[101E24]#2  ~ #0 = 000b
    //reg_sm_config[1:0]
    _RVM1(0x1EDC, 0, BIT5 | BIT4 ),   //reg[101EDC]#5  ~ #4 = 00b
    //reg_ts1config[2:0]
    _RVM1(0x1EA3, 0, BIT5 ~ BIT3 ),   //reg[101EA3]#5 ~ #3 = 000b
    //reg_ts_out_mode
    _RVM1(0x1EA3, 0, BIT0 ),   //reg[101EA3]#0 = 0b
    #endif

    #if(PAD_TS1_D5_IS_GPIO != GPIO_NONE)
    #define PAD_TS1_D5_OEN (PAD_TS1_D5_IS_GPIO == GPIO_IN ? BIT5: 0)
    #define PAD_TS1_D5_OUT (PAD_TS1_D5_IS_GPIO == GPIO_OUT_HIGH ? BIT5: 0)
    _RVM1(0x1e4C, PAD_TS1_D5_OUT, BIT5),
    _RVM1(0x1e4E, PAD_TS1_D5_OEN, BIT5),
    //reg_dspejtagmode[1:0]
    _RVM1(0x1E07, 0, BIT1 | BIT0 ),   //reg[101E07]#1 ~ #0 = 00b
    //reg_bt656_ctrl[1:0]
    _RVM1(0x1EDF, 0, BIT7 | BIT6 ),   //reg[101EDF]#7 ~ #6 = 00b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    //reg_w_debug_out
    _RVM1(0x1EF0, 0, BIT2  ),   //reg[101EF0]#2  = 0b
    //reg_test_in_mode[2:0]
    _RVM1(0x1E24, 0, BIT2 | BIT1 | BIT0 ),   //reg[101E24]#2  ~ #0 = 000b
    //reg_sm_config[1:0]
    _RVM1(0x1EDC, 0, BIT5 | BIT4 ),   //reg[101EDC]#5  ~ #4 = 00b
    //reg_ts1config[2:0]
    _RVM1(0x1EA3, 0, BIT5 ~ BIT3 ),   //reg[101EA3]#5 ~ #3 = 000b
    //reg_ts_out_mode
    _RVM1(0x1EA3, 0, BIT0 ),   //reg[101EA3]#0 = 0b
    #endif

    #if(PAD_TS1_D6_IS_GPIO != GPIO_NONE)
    #define PAD_TS1_D6_OEN (PAD_TS1_D6_IS_GPIO == GPIO_IN ? BIT4: 0)
    #define PAD_TS1_D6_OUT (PAD_TS1_D6_IS_GPIO == GPIO_OUT_HIGH ? BIT4: 0)
    _RVM1(0x1e4C, PAD_TS1_D6_OUT, BIT4),
    _RVM1(0x1e4E, PAD_TS1_D6_OEN, BIT4),
    //reg_dspejtagmode[1:0]
    _RVM1(0x1E07, 0, BIT1 | BIT0 ),   //reg[101E07]#1 ~ #0 = 00b
    //reg_bt656_ctrl[1:0]
    _RVM1(0x1EDF, 0, BIT7 | BIT6 ),   //reg[101EDF]#7 ~ #6 = 00b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    //reg_w_debug_out
    _RVM1(0x1EF0, 0, BIT2  ),   //reg[101EF0]#2  = 0b
    //reg_test_in_mode[2:0]
    _RVM1(0x1E24, 0, BIT2 | BIT1 | BIT0 ),   //reg[101E24]#2  ~ #0 = 000b
    //reg_sm_config[1:0]
    _RVM1(0x1EDC, 0, BIT5 | BIT4 ),   //reg[101EDC]#5  ~ #4 = 00b
    //reg_ts1config[2:0]
    _RVM1(0x1EA3, 0, BIT5 ~ BIT3 ),   //reg[101EA3]#5 ~ #3 = 000b
    //reg_ts_out_mode
    _RVM1(0x1EA3, 0, BIT0 ),   //reg[101EA3]#0 = 0b
    #endif

    #if(PAD_TS1_D7_IS_GPIO != GPIO_NONE)
    #define PAD_TS1_D7_OEN (PAD_TS1_D7_IS_GPIO == GPIO_IN ? BIT3: 0)
    #define PAD_TS1_D7_OUT (PAD_TS1_D7_IS_GPIO == GPIO_OUT_HIGH ? BIT3: 0)
    _RVM1(0x1e4C, PAD_TS1_D7_OUT, BIT3),
    _RVM1(0x1e4E, PAD_TS1_D7_OEN, BIT3),
    //reg_dspejtagmode[1:0]
    _RVM1(0x1E07, 0, BIT1 | BIT0 ),   //reg[101E07]#1 ~ #0 = 00b
    //reg_test_in_mode[2:0]
    _RVM1(0x1E24, 0, BIT2 | BIT1 | BIT0 ),   //reg[101E24]#2  ~ #0 = 000b
    //reg_bt656_ctrl[1:0]
    _RVM1(0x1EDF, 0, BIT7 | BIT6 ),   //reg[101EDF]#7 ~ #6 = 00b
    //reg_ts1config[2:0]
    _RVM1(0x1EA3, 0, BIT5 ~ BIT3 ),   //reg[101EA3]#5 ~ #3 = 000b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    //reg_ts_out_mode
    _RVM1(0x1EA3, 0, BIT0 ),   //reg[101EA3]#0 = 0b
    #endif

    #if(PAD_TS1_VLD_IS_GPIO != GPIO_NONE)
    #define PAD_TS1_VLD_OEN (PAD_TS1_VLD_IS_GPIO == GPIO_IN ? BIT2: 0)
    #define PAD_TS1_VLD_OUT (PAD_TS1_VLD_IS_GPIO == GPIO_OUT_HIGH ? BIT2: 0)
    _RVM1(0x1e4C, PAD_TS1_VLD_OUT, BIT2),
    _RVM1(0x1e4E, PAD_TS1_VLD_OEN, BIT2),
    //reg_miic_mode1[1:0]
    _RVM1(0x1EB5, 0, BIT3 | BIT2 ),   //reg[101EB5]#3 ~ #2 = 00b
    //reg_pwm0_mode[1:0]
    _RVM1(0x1EA2, 0, BIT1 | BIT0 ),   //reg[101EA2]#1  ~ #0 = 00b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    //reg_w_debug_out
    _RVM1(0x1EF0, 0, BIT2  ),   //reg[101EF0]#2  = 0b
    //reg_led_mode[1:0]
    _RVM1(0x1EB4, 0, BIT5 | BIT4 ),   //reg[101EB4]#5  ~ #4 = 00b
    //reg_test_in_mode[2:0]
    _RVM1(0x1E24, 0, BIT2 | BIT1 | BIT0 ),   //reg[101E24]#2  ~ #0 = 000b
    //reg_ts1config[2:0]
    _RVM1(0x1EA3, 0, BIT5 ~ BIT3 ),   //reg[101EA3]#5 ~ #3 = 000b
    //reg_test_out_mode[2:0]
    _RVM1(0x1E24, 0, BIT6 | BIT5 | BIT4 ),   //reg[101E24]#6  ~ #4 = 000b
    //reg_ts_out_mode
    _RVM1(0x1EA3, 0, BIT0 ),   //reg[101EA3]#0 = 0b
    #endif

    #if(PAD_TS1_SYNC_IS_GPIO != GPIO_NONE)
    #define PAD_TS1_SYNC_OEN (PAD_TS1_SYNC_IS_GPIO == GPIO_IN ? BIT1: 0)
    #define PAD_TS1_SYNC_OUT (PAD_TS1_SYNC_IS_GPIO == GPIO_OUT_HIGH ? BIT1: 0)
    _RVM1(0x1e4C, PAD_TS1_SYNC_OUT, BIT1),
    _RVM1(0x1e4E, PAD_TS1_SYNC_OEN, BIT1),
    //reg_miic_mode0[1:0]
    _RVM1(0x1EB5, 0, BIT1 | BIT0 ),   //reg[101EB5]#1 ~ #0 = 00b
    //reg_ddcrmode[2:0]
    _RVM1(0x1EAF, 0, BIT7 | BIT6 | BIT5 ),   //reg[101EAF]#7 ~ #5 = 000b
    //reg_test_in_mode[2:0]
    _RVM1(0x1E24, 0, BIT2 | BIT1 | BIT0 ),   //reg[101E24]#2  ~ #0 = 000b
    //reg_ts1config[2:0]
    _RVM1(0x1EA3, 0, BIT5 ~ BIT3 ),   //reg[101EA3]#5 ~ #3 = 000b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    //reg_ts_out_mode
    _RVM1(0x1EA3, 0, BIT0 ),   //reg[101EA3]#0 = 0b
    //reg_w_debug_out
    _RVM1(0x1EF0, 0, BIT2  ),   //reg[101EF0]#2  = 0b
    #endif

    #if(PAD_TS1_CLK_IS_GPIO != GPIO_NONE)
    #define PAD_TS1_CLK_OEN (PAD_TS1_CLK_IS_GPIO == GPIO_IN ? BIT0: 0)
    #define PAD_TS1_CLK_OUT (PAD_TS1_CLK_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
    _RVM1(0x1e4C, PAD_TS1_CLK_OUT, BIT0),
    _RVM1(0x1e4E, PAD_TS1_CLK_OEN, BIT0),
    //reg_miic_mode0[1:0]
    _RVM1(0x1EB5, 0, BIT1 | BIT0 ),   //reg[101EB5]#1 ~ #0 = 00b
    //reg_3dflagconfig[2:0]
    _RVM1(0x1EB3, 0, BIT7 | BIT6 | BIT5 ),   //reg[101EB3]#7 ~ #5 = 000b
    //reg_bt656_ctrl[1:0]
    _RVM1(0x1EDF, 0, BIT7 | BIT6 ),   //reg[101EDF]#7 ~ #6 = 00b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    //reg_test_in_mode[2:0]
    _RVM1(0x1E24, 0, BIT2 | BIT1 | BIT0 ),   //reg[101E24]#2  ~ #0 = 000b
    //reg_ddcrmode[2:0]
    _RVM1(0x1EAF, 0, BIT7 | BIT6 | BIT5 ),   //reg[101EAF]#7 ~ #5 = 000b
    //reg_ts1config[2:0]
    _RVM1(0x1EA3, 0, BIT5 ~ BIT3 ),   //reg[101EA3]#5 ~ #3 = 000b
    //reg_test_out_mode[2:0]
    _RVM1(0x1E24, 0, BIT6 | BIT5 | BIT4 ),   //reg[101E24]#6  ~ #4 = 000b
    //reg_ts_out_mode
    _RVM1(0x1EA3, 0, BIT0 ),   //reg[101EA3]#0 = 0b
    #endif

    #if(PAD_PCM_A4_IS_GPIO != GPIO_NONE)
    #define PAD_PCM_A4_OEN (PAD_PCM_A4_IS_GPIO == GPIO_IN ? BIT6: 0)
    #define PAD_PCM_A4_OUT (PAD_PCM_A4_IS_GPIO == GPIO_OUT_HIGH ? BIT6: 0)
    _RVM1(0x1e78, PAD_PCM_A4_OUT, BIT6),
    _RVM1(0x1e7e, PAD_PCM_A4_OEN, BIT6),
    //reg_test_in_mode[2:0]
    _RVM1(0x1E24, 0, BIT2 | BIT1 | BIT0 ),   //reg[101E24]#2  ~ #0 = 000b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    //reg_pcmadconfig
    _RVM1(0x1E9F, 0, BIT6 ),   //reg[101E9F]#6 = 0b
    //reg_nand_mode[2:0]
    _RVM1(0x1EA0, 0, BIT7 ),   //reg[101EA0]#9  ~ #7 = 000b
    _RVM1(0x1EA1, 0, BIT0 | BIT1 ),   //reg[101EA0]#9  ~ #7 = 000b
    //reg_test_out_mode[2:0]
    _RVM1(0x1E24, 0, BIT6 | BIT5 | BIT4 ),   //reg[101E24]#6  ~ #4 = 000b
    //reg_emmc_config2
    _RVM1(0x1E3B, 0, BIT0 ),   //reg[101E3B]#0 = 0b
    #endif

    #if(PAD_PCM_WAIT_N_IS_GPIO != GPIO_NONE)
    #define PAD_PCM_WAIT_N_OEN (PAD_PCM_WAIT_N_IS_GPIO == GPIO_IN ? BIT5: 0)
    #define PAD_PCM_WAIT_N_OUT (PAD_PCM_WAIT_N_IS_GPIO == GPIO_OUT_HIGH ? BIT5: 0)
    _RVM1(0x1e78, PAD_PCM_WAIT_N_OUT, BIT5),
    _RVM1(0x1e7e, PAD_PCM_WAIT_N_OEN, BIT5),
    //reg_test_in_mode[2:0]
    _RVM1(0x1E24, 0, BIT2 | BIT1 | BIT0 ),   //reg[101E24]#2  ~ #0 = 000b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    //reg_test_out_mode[2:0]
    _RVM1(0x1E24, 0, BIT6 | BIT5 | BIT4 ),   //reg[101E24]#6  ~ #4 = 000b
    //reg_pcmctrlconfig
    _RVM1(0x1E9F, 0, BIT7 ),   //reg[101E9F]#7 = 0b
    #endif

    #if(PAD_PCM_A5_IS_GPIO != GPIO_NONE)
    #define PAD_PCM_A5_OEN (PAD_PCM_A5_IS_GPIO == GPIO_IN ? BIT4: 0)
    #define PAD_PCM_A5_OUT (PAD_PCM_A5_IS_GPIO == GPIO_OUT_HIGH ? BIT4: 0)
    _RVM1(0x1e78, PAD_PCM_A5_OUT, BIT4),
    _RVM1(0x1e7e, PAD_PCM_A5_OEN, BIT4),
    //reg_test_in_mode[2:0]
    _RVM1(0x1E24, 0, BIT2 | BIT1 | BIT0 ),   //reg[101E24]#2  ~ #0 = 000b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    //reg_pcmadconfig
    _RVM1(0x1E9F, 0, BIT6 ),   //reg[101E9F]#6 = 0b
    //reg_nand_mode[2:0]
    _RVM1(0x1EA0, 0, BIT7 ),   //reg[101EA0]#9  ~ #7 = 000b
    _RVM1(0x1EA1, 0, BIT1 | BIT0 ),   //reg[101EA0]#9  ~ #7 = 000b
    //reg_test_out_mode[2:0]
    _RVM1(0x1E24, 0, BIT6 | BIT5 | BIT4 ),   //reg[101E24]#6  ~ #4 = 000b
    //reg_emmc_config2
    _RVM1(0x1E3B, 0, BIT0 ),   //reg[101E3B]#0 = 0b
    #endif

    #if(PAD_PCM_A6_IS_GPIO != GPIO_NONE)
    #define PAD_PCM_A6_OEN (PAD_PCM_A6_IS_GPIO == GPIO_IN ? BIT3: 0)
    #define PAD_PCM_A6_OUT (PAD_PCM_A6_IS_GPIO == GPIO_OUT_HIGH ? BIT3: 0)
    _RVM1(0x1e78, PAD_PCM_A6_OUT, BIT3),
    _RVM1(0x1e7e, PAD_PCM_A6_OEN, BIT3),
    //reg_test_in_mode[2:0]
    _RVM1(0x1E24, 0, BIT2 | BIT1 | BIT0 ),   //reg[101E24]#2  ~ #0 = 000b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    //reg_pcmadconfig
    _RVM1(0x1E9F, 0, BIT6 ),   //reg[101E9F]#6 = 0b
    //reg_nand_mode[2:0]
    _RVM1(0x1EA0, 0, BIT7 ),   //reg[101EA0]#9  ~ #7 = 000b
    _RVM1(0x1EA1, 0, BIT1 | BIT0 ),   //reg[101EA0]#9  ~ #7 = 000b
    //reg_test_out_mode[2:0]
    _RVM1(0x1E24, 0, BIT6 | BIT5 | BIT4 ),   //reg[101E24]#6  ~ #4 = 000b
    //reg_emmc_config2
    _RVM1(0x1E3B, 0, BIT0 ),   //reg[101E3B]#0 = 0b
    #endif

    #if(PAD_PCM_A7_IS_GPIO != GPIO_NONE)
    #define PAD_PCM_A7_OEN (PAD_PCM_A7_IS_GPIO == GPIO_IN ? BIT2: 0)
    #define PAD_PCM_A7_OUT (PAD_PCM_A7_IS_GPIO == GPIO_OUT_HIGH ? BIT2: 0)
    _RVM1(0x1e78, PAD_PCM_A7_OUT, BIT2),
    _RVM1(0x1e7e, PAD_PCM_A7_OEN, BIT2),
    //reg_test_in_mode[2:0]
    _RVM1(0x1E24, 0, BIT2 | BIT1 | BIT0 ),   //reg[101E24]#2  ~ #0 = 000b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    //reg_pcmadconfig
    _RVM1(0x1E9F, 0, BIT6 ),   //reg[101E9F]#6 = 0b
    //reg_nand_mode[2:0]
    _RVM1(0x1EA0, 0, BIT7 ),   //reg[101EA0]#9  ~ #7 = 000b
    _RVM1(0x1EA1, 0, BIT1 | BIT0 ),   //reg[101EA0]#9  ~ #7 = 000b
    //reg_test_out_mode[2:0]
    _RVM1(0x1E24, 0, BIT6 | BIT5 | BIT4 ),   //reg[101E24]#6  ~ #4 = 000b
    //reg_pwm1_mode[1:0]
    _RVM1(0x1EAA, 0, BIT5 | BIT4 ),   //reg[101EAA]#5  ~ #4 = 00b
    //reg_emmc_config2
    _RVM1(0x1E3B, 0, BIT0 ),   //reg[101E3B]#0 = 0b
    #endif

    #if(PAD_PCM_A12_IS_GPIO != GPIO_NONE)
    #define PAD_PCM_A12_OEN (PAD_PCM_A12_IS_GPIO == GPIO_IN ? BIT1: 0)
    #define PAD_PCM_A12_OUT (PAD_PCM_A12_IS_GPIO == GPIO_OUT_HIGH ? BIT1: 0)
    _RVM1(0x1e78, PAD_PCM_A12_OUT, BIT1),
    _RVM1(0x1e7e, PAD_PCM_A12_OEN, BIT1),
    //reg_test_in_mode[2:0]
    _RVM1(0x1E24, 0, BIT2 | BIT1 | BIT0 ),   //reg[101E24]#2  ~ #0 = 000b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    //reg_pcmadconfig
    _RVM1(0x1E9F, 0, BIT6 ),   //reg[101E9F]#6 = 0b
    //reg_test_out_mode[2:0]
    _RVM1(0x1E24, 0, BIT6 | BIT5 | BIT4 ),   //reg[101E24]#6  ~ #4 = 000b
    #endif

    #if(PAD_PCM_IRQA_N_IS_GPIO != GPIO_NONE)
    #define PAD_PCM_IRQA_N_OEN (PAD_PCM_IRQA_N_IS_GPIO == GPIO_IN ? BIT0: 0)
    #define PAD_PCM_IRQA_N_OUT (PAD_PCM_IRQA_N_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
    _RVM1(0x1e78, PAD_PCM_IRQA_N_OUT, BIT0),
    _RVM1(0x1e7e, PAD_PCM_IRQA_N_OEN, BIT0),
    //reg_test_in_mode[2:0]
    _RVM1(0x1E24, 0, BIT2 | BIT1 | BIT0 ),   //reg[101E24]#2  ~ #0 = 000b
    //reg_sm_config[1:0]
    _RVM1(0x1EDC, 0, BIT5 | BIT4 ),   //reg[101EDC]#5  ~ #4 = 00b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    //reg_test_out_mode[2:0]
    _RVM1(0x1E24, 0, BIT6 | BIT5 | BIT4 ),   //reg[101E24]#6  ~ #4 = 000b
    //reg_pcmctrlconfig
    _RVM1(0x1E9F, 0, BIT7 ),   //reg[101E9F]#7 = 0b
    #endif

    #if(PAD_PCM_A14_IS_GPIO != GPIO_NONE)
    #define PAD_PCM_A14_OEN (PAD_PCM_A14_IS_GPIO == GPIO_IN ? BIT6: 0)
    #define PAD_PCM_A14_OUT (PAD_PCM_A14_IS_GPIO == GPIO_OUT_HIGH ? BIT6: 0)
    _RVM1(0x1e77, PAD_PCM_A14_OUT, BIT6),
    _RVM1(0x1e7d, PAD_PCM_A14_OEN, BIT6),
    //reg_test_in_mode[2:0]
    _RVM1(0x1E24, 0, BIT2 | BIT1 | BIT0 ),   //reg[101E24]#2  ~ #0 = 000b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    //reg_pcmadconfig
    _RVM1(0x1E9F, 0, BIT6 ),   //reg[101E9F]#6 = 0b
    //reg_test_out_mode[2:0]
    _RVM1(0x1E24, 0, BIT6 | BIT5 | BIT4 ),   //reg[101E24]#6  ~ #4 = 000b
    #endif

    #if(PAD_PCM_A13_IS_GPIO != GPIO_NONE)
    #define PAD_PCM_A13_OEN (PAD_PCM_A13_IS_GPIO == GPIO_IN ? BIT5: 0)
    #define PAD_PCM_A13_OUT (PAD_PCM_A13_IS_GPIO == GPIO_OUT_HIGH ? BIT5: 0)
    _RVM1(0x1e77, PAD_PCM_A13_OUT, BIT5),
    _RVM1(0x1e7d, PAD_PCM_A13_OEN, BIT5),
    //reg_test_in_mode[2:0]
    _RVM1(0x1E24, 0, BIT2 | BIT1 | BIT0 ),   //reg[101E24]#2  ~ #0 = 000b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    //reg_pcmadconfig
    _RVM1(0x1E9F, 0, BIT6 ),   //reg[101E9F]#6 = 0b
    //reg_test_out_mode[2:0]
    _RVM1(0x1E24, 0, BIT6 | BIT5 | BIT4 ),   //reg[101E24]#6  ~ #4 = 000b
    #endif

    #if(PAD_PCM_A8_IS_GPIO != GPIO_NONE)
    #define PAD_PCM_A8_OEN (PAD_PCM_A8_IS_GPIO == GPIO_IN ? BIT4: 0)
    #define PAD_PCM_A8_OUT (PAD_PCM_A8_IS_GPIO == GPIO_OUT_HIGH ? BIT4: 0)
    _RVM1(0x1e77, PAD_PCM_A8_OUT, BIT4),
    _RVM1(0x1e7d, PAD_PCM_A8_OEN, BIT4),
    //reg_test_in_mode[2:0]
    _RVM1(0x1E24, 0, BIT2 | BIT1 | BIT0 ),   //reg[101E24]#2  ~ #0 = 000b
    //reg_bt656_ctrl[1:0]
    _RVM1(0x1EDF, 0, BIT7 | BIT6 ),   //reg[101EDF]#7 ~ #6 = 00b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    //reg_pcmadconfig
    _RVM1(0x1E9F, 0, BIT6 ),   //reg[101E9F]#6 = 0b
    //reg_test_out_mode[2:0]
    _RVM1(0x1E24, 0, BIT6 | BIT5 | BIT4 ),   //reg[101E24]#6  ~ #4 = 000b
    #endif

    #if(PAD_PCM_IOWR_N_IS_GPIO != GPIO_NONE)
    #define PAD_PCM_IOWR_N_OEN (PAD_PCM_IOWR_N_IS_GPIO == GPIO_IN ? BIT3: 0)
    #define PAD_PCM_IOWR_N_OUT (PAD_PCM_IOWR_N_IS_GPIO == GPIO_OUT_HIGH ? BIT3: 0)
    _RVM1(0x1e77, PAD_PCM_IOWR_N_OUT, BIT3),
    _RVM1(0x1e7d, PAD_PCM_IOWR_N_OEN, BIT3),
    //reg_test_in_mode[2:0]
    _RVM1(0x1E24, 0, BIT2 | BIT1 | BIT0 ),   //reg[101E24]#2  ~ #0 = 000b
    //reg_sm_config[1:0]
    _RVM1(0x1EDC, 0, BIT5 | BIT4 ),   //reg[101EDC]#5  ~ #4 = 00b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    //reg_test_out_mode[2:0]
    _RVM1(0x1E24, 0, BIT6 | BIT5 | BIT4 ),   //reg[101E24]#6  ~ #4 = 000b
    //reg_pcmctrlconfig
    _RVM1(0x1E9F, 0, BIT7 ),   //reg[101E9F]#7 = 0b
    #endif

    #if(PAD_PCM_A9_IS_GPIO != GPIO_NONE)
    #define PAD_PCM_A9_OEN (PAD_PCM_A9_IS_GPIO == GPIO_IN ? BIT2: 0)
    #define PAD_PCM_A9_OUT (PAD_PCM_A9_IS_GPIO == GPIO_OUT_HIGH ? BIT2: 0)
    _RVM1(0x1e77, PAD_PCM_A9_OUT, BIT2),
    _RVM1(0x1e7d, PAD_PCM_A9_OEN, BIT2),
    //reg_test_in_mode[2:0]
    _RVM1(0x1E24, 0, BIT2 | BIT1 | BIT0 ),   //reg[101E24]#2  ~ #0 = 000b
    //reg_bt656_ctrl[1:0]
    _RVM1(0x1EDF, 0, BIT7 | BIT6 ),   //reg[101EDF]#7 ~ #6 = 00b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    //reg_pcmadconfig
    _RVM1(0x1E9F, 0, BIT6 ),   //reg[101E9F]#6 = 0b
    //reg_test_out_mode[2:0]
    _RVM1(0x1E24, 0, BIT6 | BIT5 | BIT4 ),   //reg[101E24]#6  ~ #4 = 000b
    #endif

    #if(PAD_PCM_IORD_N_IS_GPIO != GPIO_NONE)
    #define PAD_PCM_IORD_N_OEN (PAD_PCM_IORD_N_IS_GPIO == GPIO_IN ? BIT1: 0)
    #define PAD_PCM_IORD_N_OUT (PAD_PCM_IORD_N_IS_GPIO == GPIO_OUT_HIGH ? BIT1: 0)
    _RVM1(0x1e77, PAD_PCM_IORD_N_OUT, BIT1),
    _RVM1(0x1e7d, PAD_PCM_IORD_N_OEN, BIT1),
    //reg_test_in_mode[2:0]
    _RVM1(0x1E24, 0, BIT2 | BIT1 | BIT0 ),   //reg[101E24]#2  ~ #0 = 000b
    //reg_sm_config[1:0]
    _RVM1(0x1EDC, 0, BIT5 | BIT4 ),   //reg[101EDC]#5  ~ #4 = 00b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    //reg_test_out_mode[2:0]
    _RVM1(0x1E24, 0, BIT6 | BIT5 | BIT4 ),   //reg[101E24]#6  ~ #4 = 000b
    //reg_pcmctrlconfig
    _RVM1(0x1E9F, 0, BIT7 ),   //reg[101E9F]#7 = 0b
    #endif

    #if(PAD_PCM_A11_IS_GPIO != GPIO_NONE)
    #define PAD_PCM_A11_OEN (PAD_PCM_A11_IS_GPIO == GPIO_IN ? BIT0: 0)
    #define PAD_PCM_A11_OUT (PAD_PCM_A11_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
    _RVM1(0x1e77, PAD_PCM_A11_OUT, BIT0),
    _RVM1(0x1e7d, PAD_PCM_A11_OEN, BIT0),
    //reg_test_in_mode[2:0]
    _RVM1(0x1E24, 0, BIT2 | BIT1 | BIT0 ),   //reg[101E24]#2  ~ #0 = 000b
    //reg_bt656_ctrl[1:0]
    _RVM1(0x1EDF, 0, BIT7 | BIT6 ),   //reg[101EDF]#7 ~ #6 = 00b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    //reg_pcmadconfig
    _RVM1(0x1E9F, 0, BIT6 ),   //reg[101E9F]#6 = 0b
    //reg_test_out_mode[2:0]
    _RVM1(0x1E24, 0, BIT6 | BIT5 | BIT4 ),   //reg[101E24]#6  ~ #4 = 000b
    #endif

    #if(PAD_PCM_OE_N_IS_GPIO != GPIO_NONE)
    #define PAD_PCM_OE_N_OEN (PAD_PCM_OE_N_IS_GPIO == GPIO_IN ? BIT7: 0)
    #define PAD_PCM_OE_N_OUT (PAD_PCM_OE_N_IS_GPIO == GPIO_OUT_HIGH ? BIT7: 0)
    _RVM1(0x1e76, PAD_PCM_OE_N_OUT, BIT7),
    _RVM1(0x1e7c, PAD_PCM_OE_N_OEN, BIT7),
    //reg_test_in_mode[2:0]
    _RVM1(0x1E24, 0, BIT2 | BIT1 | BIT0 ),   //reg[101E24]#2  ~ #0 = 000b
    //reg_sm_config[1:0]
    _RVM1(0x1EDC, 0, BIT5 | BIT4 ),   //reg[101EDC]#5  ~ #4 = 00b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    //reg_test_out_mode[2:0]
    _RVM1(0x1E24, 0, BIT6 | BIT5 | BIT4 ),   //reg[101E24]#6  ~ #4 = 000b
    //reg_pcmctrlconfig
    _RVM1(0x1E9F, 0, BIT7 ),   //reg[101E9F]#7 = 0b
    #endif

    #if(PAD_PCM_A10_IS_GPIO != GPIO_NONE)
    #define PAD_PCM_A10_OEN (PAD_PCM_A10_IS_GPIO == GPIO_IN ? BIT6: 0)
    #define PAD_PCM_A10_OUT (PAD_PCM_A10_IS_GPIO == GPIO_OUT_HIGH ? BIT6: 0)
    _RVM1(0x1e76, PAD_PCM_A10_OUT, BIT6),
    _RVM1(0x1e7c, PAD_PCM_A10_OEN, BIT6),
    //reg_test_in_mode[2:0]
    _RVM1(0x1E24, 0, BIT2 | BIT1 | BIT0 ),   //reg[101E24]#2  ~ #0 = 000b
    //reg_bt656_ctrl[1:0]
    _RVM1(0x1EDF, 0, BIT7 | BIT6 ),   //reg[101EDF]#7 ~ #6 = 00b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    //reg_pcmadconfig
    _RVM1(0x1E9F, 0, BIT6 ),   //reg[101E9F]#6 = 0b
    //reg_test_out_mode[2:0]
    _RVM1(0x1E24, 0, BIT6 | BIT5 | BIT4 ),   //reg[101E24]#6  ~ #4 = 000b
    #endif

    #if(PAD_PCM_CE_N_IS_GPIO != GPIO_NONE)
    #define PAD_PCM_CE_N_OEN (PAD_PCM_CE_N_IS_GPIO == GPIO_IN ? BIT5: 0)
    #define PAD_PCM_CE_N_OUT (PAD_PCM_CE_N_IS_GPIO == GPIO_OUT_HIGH ? BIT5: 0)
    _RVM1(0x1e76, PAD_PCM_CE_N_OUT, BIT5),
    _RVM1(0x1e7c, PAD_PCM_CE_N_OEN, BIT5),
    //reg_test_in_mode[2:0]
    _RVM1(0x1E24, 0, BIT2 | BIT1 | BIT0 ),   //reg[101E24]#2  ~ #0 = 000b
    //reg_sm_config[1:0]
    _RVM1(0x1EDC, 0, BIT5 | BIT4 ),   //reg[101EDC]#5  ~ #4 = 00b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    //reg_test_out_mode[2:0]
    _RVM1(0x1E24, 0, BIT6 | BIT5 | BIT4 ),   //reg[101E24]#6  ~ #4 = 000b
    //reg_pcmctrlconfig
    _RVM1(0x1E9F, 0, BIT7 ),   //reg[101E9F]#7 = 0b
    #endif

    #if(PAD_PCM_D7_IS_GPIO != GPIO_NONE)
    #define PAD_PCM_D7_OEN (PAD_PCM_D7_IS_GPIO == GPIO_IN ? BIT4: 0)
    #define PAD_PCM_D7_OUT (PAD_PCM_D7_IS_GPIO == GPIO_OUT_HIGH ? BIT4: 0)
    _RVM1(0x1e76, PAD_PCM_D7_OUT, BIT4),
    _RVM1(0x1e7c, PAD_PCM_D7_OEN, BIT4),
    //reg_sd_config2[1:0]
    _RVM1(0x1EAF, 0, BIT3 | BIT2 ),   //reg[101EAF]#3 ~ #2 = 00b
    //reg_bt656_ctrl[1:0]
    _RVM1(0x1EDF, 0, BIT7 | BIT6 ),   //reg[101EDF]#7 ~ #6 = 00b
    //reg_sd_config[1:0]
    _RVM1(0x1EAF, 0, BIT1 | BIT0 ),   //reg[101EAF]#1 ~ #0 = 00b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    //reg_pcmadconfig
    _RVM1(0x1E9F, 0, BIT6 ),   //reg[101E9F]#6 = 0b
    //reg_nand_mode[2:0]
    _RVM1(0x1EA0, 0, BIT7 ),   //reg[101EA0]#9  ~ #7 = 000b
    _RVM1(0x1EA1, 0, BIT1 | BIT0 ),   //reg[101EA0]#9  ~ #7 = 000b
    //reg_test_out_mode[2:0]
    _RVM1(0x1E24, 0, BIT6 | BIT5 | BIT4 ),   //reg[101E24]#6  ~ #4 = 000b
    #endif

    #if(PAD_PCM_D6_IS_GPIO != GPIO_NONE)
    #define PAD_PCM_D6_OEN (PAD_PCM_D6_IS_GPIO == GPIO_IN ? BIT3: 0)
    #define PAD_PCM_D6_OUT (PAD_PCM_D6_IS_GPIO == GPIO_OUT_HIGH ? BIT3: 0)
    _RVM1(0x1e76, PAD_PCM_D6_OUT, BIT3),
    _RVM1(0x1e7c, PAD_PCM_D6_OEN, BIT3),
    //reg_bt656_ctrl[1:0]
    _RVM1(0x1EDF, 0, BIT7 | BIT6 ),   //reg[101EDF]#7 ~ #6 = 00b
    //reg_sd_config[1:0]
    _RVM1(0x1EAF, 0, BIT1 | BIT0 ),   //reg[101EAF]#1 ~ #0 = 00b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    //reg_pcmadconfig
    _RVM1(0x1E9F, 0, BIT6 ),   //reg[101E9F]#6 = 0b
    //reg_nand_mode[2:0]
    _RVM1(0x1EA0, 0, BIT7 ),   //reg[101EA0]#9  ~ #7 = 000b
    _RVM1(0x1EA1, 0, BIT1 | BIT0 ),   //reg[101EA0]#9  ~ #7 = 000b
    //reg_mcu_jtag_mode[1:0]
    _RVM1(0x1E06, 0, BIT7 | BIT6 ),   //reg[101E06]#7  ~ #6 = 00b
    //reg_sd_config2[1:0]
    _RVM1(0x1EAF, 0, BIT3 | BIT2 ),   //reg[101EAF]#3 ~ #2 = 00b
    //reg_test_out_mode[2:0]
    _RVM1(0x1E24, 0, BIT6 | BIT5 | BIT4 ),   //reg[101E24]#6  ~ #4 = 000b
    #endif

    #if(PAD_PCM_D5_IS_GPIO != GPIO_NONE)
    #define PAD_PCM_D5_OEN (PAD_PCM_D5_IS_GPIO == GPIO_IN ? BIT2: 0)
    #define PAD_PCM_D5_OUT (PAD_PCM_D5_IS_GPIO == GPIO_OUT_HIGH ? BIT2: 0)
    _RVM1(0x1e76, PAD_PCM_D5_OUT, BIT2),
    _RVM1(0x1e7c, PAD_PCM_D5_OEN, BIT2),
    //reg_bt656_ctrl[1:0]
    _RVM1(0x1EDF, 0, BIT7 | BIT6 ),   //reg[101EDF]#7 ~ #6 = 00b
    //reg_sd_config[1:0]
    _RVM1(0x1EAF, 0, BIT1 | BIT0 ),   //reg[101EAF]#1 ~ #0 = 00b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    //reg_pcmadconfig
    _RVM1(0x1E9F, 0, BIT6 ),   //reg[101E9F]#6 = 0b
    //reg_nand_mode[2:0]
    _RVM1(0x1EA0, 0, BIT7 ),   //reg[101EA0]#9  ~ #7 = 000b
    _RVM1(0x1EA1, 0, BIT1 | BIT0 ),   //reg[101EA0]#9  ~ #7 = 000b
    //reg_mcu_jtag_mode[1:0]
    _RVM1(0x1E06, 0, BIT7 | BIT6 ),   //reg[101E06]#7  ~ #6 = 00b
    //reg_sd_config2[1:0]
    _RVM1(0x1EAF, 0, BIT3 | BIT2 ),   //reg[101EAF]#3 ~ #2 = 00b
    //reg_test_out_mode[2:0]
    _RVM1(0x1E24, 0, BIT6 | BIT5 | BIT4 ),   //reg[101E24]#6  ~ #4 = 000b
    #endif

    #if(PAD_PCM_D4_IS_GPIO != GPIO_NONE)
    #define PAD_PCM_D4_OEN (PAD_PCM_D4_IS_GPIO == GPIO_IN ? BIT1: 0)
    #define PAD_PCM_D4_OUT (PAD_PCM_D4_IS_GPIO == GPIO_OUT_HIGH ? BIT1: 0)
    _RVM1(0x1e76, PAD_PCM_D4_OUT, BIT1),
    _RVM1(0x1e7c, PAD_PCM_D4_OEN, BIT1),
    //reg_bt656_ctrl[1:0]
    _RVM1(0x1EDF, 0, BIT7 | BIT6 ),   //reg[101EDF]#7 ~ #6 = 00b
    //reg_sd_config[1:0]
    _RVM1(0x1EAF, 0, BIT1 | BIT0 ),   //reg[101EAF]#1 ~ #0 = 00b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    //reg_pcmadconfig
    _RVM1(0x1E9F, 0, BIT6 ),   //reg[101E9F]#6 = 0b
    //reg_nand_mode[2:0]
    _RVM1(0x1EA0, 0, BIT7 ),   //reg[101EA0]#9  ~ #7 = 000b
    _RVM1(0x1EA1, 0, BIT1 | BIT0 ),   //reg[101EA0]#9  ~ #7 = 000b
    //reg_mcu_jtag_mode[1:0]
    _RVM1(0x1E06, 0, BIT7 | BIT6 ),   //reg[101E06]#7  ~ #6 = 00b
    //reg_sd_config2[1:0]
    _RVM1(0x1EAF, 0, BIT3 | BIT2 ),   //reg[101EAF]#3 ~ #2 = 00b
    //reg_test_out_mode[2:0]
    _RVM1(0x1E24, 0, BIT6 | BIT5 | BIT4 ),   //reg[101E24]#6  ~ #4 = 000b
    #endif

    #if(PAD_PCM_D3_IS_GPIO != GPIO_NONE)
    #define PAD_PCM_D3_OEN (PAD_PCM_D3_IS_GPIO == GPIO_IN ? BIT0: 0)
    #define PAD_PCM_D3_OUT (PAD_PCM_D3_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
    _RVM1(0x1e76, PAD_PCM_D3_OUT, BIT0),
    _RVM1(0x1e7c, PAD_PCM_D3_OEN, BIT0),
    //reg_bt656_ctrl[1:0]
    _RVM1(0x1EDF, 0, BIT7 | BIT6 ),   //reg[101EDF]#7 ~ #6 = 00b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    //reg_pcmadconfig
    _RVM1(0x1E9F, 0, BIT6 ),   //reg[101E9F]#6 = 0b
    //reg_nand_mode[2:0]
    _RVM1(0x1EA0, 0, BIT7 ),   //reg[101EA0]#9  ~ #7 = 000b
    _RVM1(0x1EA1, 0, BIT1 | BIT0 ),   //reg[101EA0]#9  ~ #7 = 000b
    //reg_test_out_mode[2:0]
    _RVM1(0x1E24, 0, BIT6 | BIT5 | BIT4 ),   //reg[101E24]#6  ~ #4 = 000b
    //reg_mcu_jtag_mode[1:0]
    _RVM1(0x1E06, 0, BIT7 | BIT6 ),   //reg[101E06]#7  ~ #6 = 00b
    #endif

    #if(PAD_PCM_A3_IS_GPIO != GPIO_NONE)
    #define PAD_PCM_A3_OEN (PAD_PCM_A3_IS_GPIO == GPIO_IN ? BIT7: 0)
    #define PAD_PCM_A3_OUT (PAD_PCM_A3_IS_GPIO == GPIO_OUT_HIGH ? BIT7: 0)
    _RVM1(0x1e78, PAD_PCM_A3_OUT, BIT7),
    _RVM1(0x1e7e, PAD_PCM_A3_OEN, BIT7),
    //reg_sd_config[1:0]
    _RVM1(0x1EAF, 0, BIT1 | BIT0 ),   //reg[101EAF]#1 ~ #0 = 00b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    //reg_pcmadconfig
    _RVM1(0x1E9F, 0, BIT6 ),   //reg[101E9F]#6 = 0b
    //reg_nand_mode[2:0]
    _RVM1(0x1EA0, 0, BIT7 ),   //reg[101EA0]#9  ~ #7 = 000b
    _RVM1(0x1EA1, 0, BIT1 | BIT0 ),   //reg[101EA0]#9  ~ #7 = 000b
    //reg_test_in_mode[2:0]
    _RVM1(0x1E24, 0, BIT2 | BIT1 | BIT0 ),   //reg[101E24]#2  ~ #0 = 000b
    //reg_sd_config2[1:0]
    _RVM1(0x1EAF, 0, BIT3 | BIT2 ),   //reg[101EAF]#3 ~ #2 = 00b
    //reg_test_out_mode[2:0]
    _RVM1(0x1E24, 0, BIT6 | BIT5 | BIT4 ),   //reg[101E24]#6  ~ #4 = 000b
    //reg_emmc_config2
    _RVM1(0x1E3B, 0, BIT0 ),   //reg[101E3B]#0 = 0b
    #endif

    #if(PAD_PCM_A2_IS_GPIO != GPIO_NONE)
    #define PAD_PCM_A2_OEN (PAD_PCM_A2_IS_GPIO == GPIO_IN ? BIT0: 0)
    #define PAD_PCM_A2_OUT (PAD_PCM_A2_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
    _RVM1(0x1e79, PAD_PCM_A2_OUT, BIT0),
    _RVM1(0x1e7f, PAD_PCM_A2_OEN, BIT0),
    //reg_sd_config[1:0]
    _RVM1(0x1EAF, 0, BIT1 | BIT0 ),   //reg[101EAF]#1 ~ #0 = 00b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    //reg_pcmadconfig
    _RVM1(0x1E9F, 0, BIT6 ),   //reg[101E9F]#6 = 0b
    //reg_nand_mode[2:0]
    _RVM1(0x1EA0, 0, BIT7 ),   //reg[101EA0]#9  ~ #7 = 000b
    _RVM1(0x1EA1, 0, BIT1 | BIT0 ),   //reg[101EA0]#9  ~ #7 = 000b
    //reg_test_in_mode[2:0]
    _RVM1(0x1E24, 0, BIT2 | BIT1 | BIT0 ),   //reg[101E24]#2  ~ #0 = 000b
    //reg_sd_config2[1:0]
    _RVM1(0x1EAF, 0, BIT3 | BIT2 ),   //reg[101EAF]#3 ~ #2 = 00b
    //reg_test_out_mode[2:0]
    _RVM1(0x1E24, 0, BIT6 | BIT5 | BIT4 ),   //reg[101E24]#6  ~ #4 = 000b
    //reg_emmc_config2
    _RVM1(0x1E3B, 0, BIT0 ),   //reg[101E3B]#0 = 0b
    #endif

    #if(PAD_PCM_REG_N_IS_GPIO != GPIO_NONE)
    #define PAD_PCM_REG_N_OEN (PAD_PCM_REG_N_IS_GPIO == GPIO_IN ? BIT1: 0)
    #define PAD_PCM_REG_N_OUT (PAD_PCM_REG_N_IS_GPIO == GPIO_OUT_HIGH ? BIT1: 0)
    _RVM1(0x1e79, PAD_PCM_REG_N_OUT, BIT1),
    _RVM1(0x1e7f, PAD_PCM_REG_N_OEN, BIT1),
    //reg_test_in_mode[2:0]
    _RVM1(0x1E24, 0, BIT2 | BIT1 | BIT0 ),   //reg[101E24]#2  ~ #0 = 000b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    //reg_pcmctrlconfig
    _RVM1(0x1E9F, 0, BIT7 ),   //reg[101E9F]#7 = 0b
    #endif

    #if(PAD_PCM_A1_IS_GPIO != GPIO_NONE)
    #define PAD_PCM_A1_OEN (PAD_PCM_A1_IS_GPIO == GPIO_IN ? BIT2: 0)
    #define PAD_PCM_A1_OUT (PAD_PCM_A1_IS_GPIO == GPIO_OUT_HIGH ? BIT2: 0)
    _RVM1(0x1e79, PAD_PCM_A1_OUT, BIT2),
    _RVM1(0x1e7f, PAD_PCM_A1_OEN, BIT2),
    //reg_sd_config[1:0]
    _RVM1(0x1EAF, 0, BIT1 | BIT0 ),   //reg[101EAF]#1 ~ #0 = 00b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    //reg_pcmadconfig
    _RVM1(0x1E9F, 0, BIT6 ),   //reg[101E9F]#6 = 0b
    //reg_nand_mode[2:0]
    _RVM1(0x1EA0, 0, BIT7 ),   //reg[101EA0]#9  ~ #7 = 000b
    _RVM1(0x1EA1, 0, BIT1 | BIT0 ),   //reg[101EA0]#9  ~ #7 = 000b
    //reg_test_in_mode[2:0]
    _RVM1(0x1E24, 0, BIT2 | BIT1 | BIT0 ),   //reg[101E24]#2  ~ #0 = 000b
    //reg_sd_config2[1:0]
    _RVM1(0x1EAF, 0, BIT3 | BIT2 ),   //reg[101EAF]#3 ~ #2 = 00b
    //reg_test_out_mode[2:0]
    _RVM1(0x1E24, 0, BIT6 | BIT5 | BIT4 ),   //reg[101E24]#6  ~ #4 = 000b
    //reg_emmc_config2
    _RVM1(0x1E3B, 0, BIT0 ),   //reg[101E3B]#0 = 0b
    #endif

    #if(PAD_PCM_A0_IS_GPIO != GPIO_NONE)
    #define PAD_PCM_A0_OEN (PAD_PCM_A0_IS_GPIO == GPIO_IN ? BIT3: 0)
    #define PAD_PCM_A0_OUT (PAD_PCM_A0_IS_GPIO == GPIO_OUT_HIGH ? BIT3: 0)
    _RVM1(0x1e79, PAD_PCM_A0_OUT, BIT3),
    _RVM1(0x1e7f, PAD_PCM_A0_OEN, BIT3),
    //reg_sd_config[1:0]
    _RVM1(0x1EAF, 0, BIT1 | BIT0 ),   //reg[101EAF]#1 ~ #0 = 00b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    //reg_pcmadconfig
    _RVM1(0x1E9F, 0, BIT6 ),   //reg[101E9F]#6 = 0b
    //reg_nand_mode[2:0]
    _RVM1(0x1EA0, 0, BIT7 ),   //reg[101EA0]#9  ~ #7 = 000b
    _RVM1(0x1EA1, 0, BIT1 | BIT0 ),   //reg[101EA0]#9  ~ #7 = 000b
    //reg_test_in_mode[2:0]
    _RVM1(0x1E24, 0, BIT2 | BIT1 | BIT0 ),   //reg[101E24]#2  ~ #0 = 000b
    //reg_sd_config2[1:0]
    _RVM1(0x1EAF, 0, BIT3 | BIT2 ),   //reg[101EAF]#3 ~ #2 = 00b
    //reg_test_out_mode[2:0]
    _RVM1(0x1E24, 0, BIT6 | BIT5 | BIT4 ),   //reg[101E24]#6  ~ #4 = 000b
    //reg_emmc_config2
    _RVM1(0x1E3B, 0, BIT0 ),   //reg[101E3B]#0 = 0b
    #endif

    #if(PAD_PCM_D0_IS_GPIO != GPIO_NONE)
    #define PAD_PCM_D0_OEN (PAD_PCM_D0_IS_GPIO == GPIO_IN ? BIT4: 0)
    #define PAD_PCM_D0_OUT (PAD_PCM_D0_IS_GPIO == GPIO_OUT_HIGH ? BIT4: 0)
    _RVM1(0x1e79, PAD_PCM_D0_OUT, BIT4),
    _RVM1(0x1e7f, PAD_PCM_D0_OEN, BIT4),
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    //reg_pcmadconfig
    _RVM1(0x1E9F, 0, BIT6 ),   //reg[101E9F]#6 = 0b
    //reg_nand_mode[2:0]
    _RVM1(0x1EA0, 0, BIT7 ),   //reg[101EA0]#9  ~ #7 = 000b
    _RVM1(0x1EA1, 0, BIT1 | BIT0 ),   //reg[101EA0]#9  ~ #7 = 000b
    #endif

    #if(PAD_PCM_D1_IS_GPIO != GPIO_NONE)
    #define PAD_PCM_D1_OEN (PAD_PCM_D1_IS_GPIO == GPIO_IN ? BIT5: 0)
    #define PAD_PCM_D1_OUT (PAD_PCM_D1_IS_GPIO == GPIO_OUT_HIGH ? BIT5: 0)
    _RVM1(0x1e79, PAD_PCM_D1_OUT, BIT5),
    _RVM1(0x1e7f, PAD_PCM_D1_OEN, BIT5),
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    //reg_pcmadconfig
    _RVM1(0x1E9F, 0, BIT6 ),   //reg[101E9F]#6 = 0b
    //reg_nand_mode[2:0]
    _RVM1(0x1EA0, 0, BIT7 ),   //reg[101EA0]#9  ~ #7 = 000b
    #endif

    #if(PAD_PCM_D2_IS_GPIO != GPIO_NONE)
    #define PAD_PCM_D2_OEN (PAD_PCM_D2_IS_GPIO == GPIO_IN ? BIT6: 0)
    #define PAD_PCM_D2_OUT (PAD_PCM_D2_IS_GPIO == GPIO_OUT_HIGH ? BIT6: 0)
    _RVM1(0x1e79, PAD_PCM_D2_OUT, BIT6),
    _RVM1(0x1e7f, PAD_PCM_D2_OEN, BIT6),
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    //reg_pcmadconfig
    _RVM1(0x1E9F, 0, BIT6 ),   //reg[101E9F]#6 = 0b
    //reg_nand_mode[2:0]
    _RVM1(0x1EA0, 0, BIT7 ),   //reg[101EA0]#9  ~ #7 = 000b
    _RVM1(0x1EA1, 0, BIT1 | BIT0 ),   //reg[101EA0]#9  ~ #7 = 000b
    #endif

    #if(PAD_PCM_RESET_IS_GPIO != GPIO_NONE)
    #define PAD_PCM_RESET_OEN (PAD_PCM_RESET_IS_GPIO == GPIO_IN ? BIT7: 0)
    #define PAD_PCM_RESET_OUT (PAD_PCM_RESET_IS_GPIO == GPIO_OUT_HIGH ? BIT7: 0)
    _RVM1(0x1e79, PAD_PCM_RESET_OUT, BIT7),
    _RVM1(0x1e7f, PAD_PCM_RESET_OEN, BIT7),
    //reg_test_in_mode[2:0]
    _RVM1(0x1E24, 0, BIT2 | BIT1 | BIT0 ),   //reg[101E24]#2  ~ #0 = 000b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    //reg_pcmctrlconfig
    _RVM1(0x1E9F, 0, BIT7 ),   //reg[101E9F]#7 = 0b
    #endif

    #if(PAD_PCM_CD_N_IS_GPIO != GPIO_NONE)
    #define PAD_PCM_CD_N_OEN (PAD_PCM_CD_N_IS_GPIO == GPIO_IN ? BIT0: 0)
    #define PAD_PCM_CD_N_OUT (PAD_PCM_CD_N_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
    _RVM1(0x1e7a, PAD_PCM_CD_N_OUT, BIT0),
    _RVM1(0x1e80, PAD_PCM_CD_N_OEN, BIT0),
    //reg_test_in_mode[2:0]
    _RVM1(0x1E24, 0, BIT2 | BIT1 | BIT0 ),   //reg[101E24]#2  ~ #0 = 000b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    //reg_pcmctrlconfig
    _RVM1(0x1E9F, 0, BIT7 ),   //reg[101E9F]#7 = 0b
    #endif

    #if(PAD_PCM2_CE_N_IS_GPIO != GPIO_NONE)
    #define PAD_PCM2_CE_N_OEN (PAD_PCM2_CE_N_IS_GPIO == GPIO_IN ? BIT0: 0)
    #define PAD_PCM2_CE_N_OUT (PAD_PCM2_CE_N_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
    _RVM1(0x1e7B, PAD_PCM2_CE_N_OUT, BIT0),
    _RVM1(0x1e81, PAD_PCM2_CE_N_OEN, BIT0),
    _RVM1(0x1E1A, BIT4, BIT4  ),
    //reg_extint0
    _RVM1(0x1EE0, 0, BIT0  ),   //reg[101EE0]#0  = 0b
    //reg_sd_config[1:0]
    _RVM1(0x1EAF, 0, BIT1 | BIT0 ),   //reg[101EAF]#1 ~ #0 = 00b
    //reg_pwm0_mode[1:0]
    _RVM1(0x1EA2, 0, BIT1 | BIT0 ),   //reg[101EA2]#1  ~ #0 = 00b
    //reg_pcm2ctrlconfig
    _RVM1(0x1E9F, 0, BIT5 ),   //reg[101E9F]#5 = 0b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    #endif

    #if(PAD_PCM2_IRQA_N_IS_GPIO != GPIO_NONE)
    #define PAD_PCM2_IRQA_N_OEN (PAD_PCM2_IRQA_N_IS_GPIO == GPIO_IN ? BIT1: 0)
    #define PAD_PCM2_IRQA_N_OUT (PAD_PCM2_IRQA_N_IS_GPIO == GPIO_OUT_HIGH ? BIT1: 0)
    _RVM1(0x1e7B, PAD_PCM2_IRQA_N_OUT, BIT1),
    _RVM1(0x1e81, PAD_PCM2_IRQA_N_OEN, BIT1),
    _RVM1(0x1E1A, BIT2, BIT2  ),
    //reg_od5thuart
    _RVM1(0x1EC8, 0, BIT4  ),   //reg[101EC8]#4  = 0b
    //reg_fifthuartmode
    _RVM1(0x1E1E, 0, BIT0  ),   //reg[101E1E]#0  = 0b
    //reg_od4thuart[1:0]
    _RVM1(0x1EB6, 0, BIT5 | BIT4 ),   //reg[101EB6]#5  ~ #4 = 00b
    //reg_extint2
    _RVM1(0x1EE0, 0, BIT2  ),   //reg[101EE0]#2  = 0b
    //reg_sd_config[1:0]
    _RVM1(0x1EAF, 0, BIT1 | BIT0 ),   //reg[101EAF]#1 ~ #0 = 00b
    //reg_pcm2ctrlconfig
    _RVM1(0x1E9F, 0, BIT5 ),   //reg[101E9F]#5 = 0b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    #endif

    #if(PAD_PCM2_WAIT_N_IS_GPIO != GPIO_NONE)
    #define PAD_PCM2_WAIT_N_OEN (PAD_PCM2_WAIT_N_IS_GPIO == GPIO_IN ? BIT2: 0)
    #define PAD_PCM2_WAIT_N_OUT (PAD_PCM2_WAIT_N_IS_GPIO == GPIO_OUT_HIGH ? BIT2: 0)
    _RVM1(0x1e7B, PAD_PCM2_WAIT_N_OUT, BIT2),
    _RVM1(0x1e81, PAD_PCM2_WAIT_N_OEN, BIT2),
    //reg_od5thuart
    _RVM1(0x1EC8, 0, BIT4  ),   //reg[101EC8]#4  = 0b
    //reg_fifthuartmode
    _RVM1(0x1E1E, 0, BIT0  ),   //reg[101E1E]#0  = 0b
    //reg_sd_config2[1:0]
    _RVM1(0x1EAF, 0, BIT3 | BIT2 ),   //reg[101EAF]#3 ~ #2 = 00b
    //reg_pcm2ctrlconfig
    _RVM1(0x1E9F, 0, BIT5 ),   //reg[101E9F]#5 = 0b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    #endif

    #if(PAD_PCM2_RESET_IS_GPIO != GPIO_NONE)
    #define PAD_PCM2_RESET_OEN (PAD_PCM2_RESET_IS_GPIO == GPIO_IN ? BIT3: 0)
    #define PAD_PCM2_RESET_OUT (PAD_PCM2_RESET_IS_GPIO == GPIO_OUT_HIGH ? BIT3: 0)
    _RVM1(0x1e7B, PAD_PCM2_RESET_OUT, BIT3),
    _RVM1(0x1e81, PAD_PCM2_RESET_OEN, BIT3),
    //reg_sd_config2[1:0]
    _RVM1(0x1EAF, 0, BIT3 | BIT2 ),   //reg[101EAF]#3 ~ #2 = 00b
    //reg_pcm2ctrlconfig
    _RVM1(0x1E9F, 0, BIT5 ),   //reg[101E9F]#5 = 0b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    #endif

    #if(PAD_PCM2_CD_N_IS_GPIO != GPIO_NONE)
    #define PAD_PCM2_CD_N_OEN (PAD_PCM2_CD_N_IS_GPIO == GPIO_IN ? BIT4: 0)
    #define PAD_PCM2_CD_N_OUT (PAD_PCM2_CD_N_IS_GPIO == GPIO_OUT_HIGH ? BIT4: 0)
    _RVM1(0x1e7B, PAD_PCM2_CD_N_OUT, BIT4),
    _RVM1(0x1e81, PAD_PCM2_CD_N_OEN, BIT4),
    //reg_pcm2_cdn_config
    _RVM1(0x1E3C, 0, BIT5  ),   //reg[101E3C]#5  = 0b
    //reg_pcm2ctrlconfig
    _RVM1(0x1E9F, 0, BIT5 ),   //reg[101E9F]#5 = 0b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    //reg_extint3
    _RVM1(0x1EE0, 0, BIT3  ),   //reg[101EE0]#3  = 0b
    #endif

    #if(PAD_EMMC_IO0_IS_GPIO != GPIO_NONE)
    #define PAD_EMMC_IO0_OEN (PAD_EMMC_IO0_IS_GPIO == GPIO_IN ? BIT0: 0)
    #define PAD_EMMC_IO0_OUT (PAD_EMMC_IO0_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
    _RVM1(0x1e89, PAD_EMMC_IO0_OUT, BIT0),
    _RVM1(0x1e88, PAD_EMMC_IO0_OEN, BIT0),
    //reg_test_in_mode[2:0]
    _RVM1(0x1E24, 0, BIT2 | BIT1 | BIT0 ),   //reg[101E24]#2  ~ #0 = 000b
    //reg_fourthuartmode[1:0]
    _RVM1(0x1E04, 0, BIT7 | BIT6 ),   //reg[101E04]#7  ~ #6 = 00b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    //reg_nand_mode[2:0]
    _RVM1(0x1EA0, 0, BIT7 ),   //reg[101EA0]#9  ~ #7 = 000b
    _RVM1(0x1EA1, 0, BIT1 | BIT0 ),   //reg[101EA0]#9  ~ #7 = 000b
    //reg_spi_nand
    _RVM1(0x1EF0, 0, BIT0  ),   //reg[101EF0]#0  = 0b
    //reg_test_out_mode[2:0]
    _RVM1(0x1E24, 0, BIT6 | BIT5 | BIT4 ),   //reg[101E24]#6  ~ #4 = 000b
    //reg_emmc_config2
    _RVM1(0x1E3B, 0, BIT0 ),   //reg[101E3B]#0 = 0b
    #endif

    #if(PAD_EMMC_IO1_IS_GPIO != GPIO_NONE)
    #define PAD_EMMC_IO1_OEN (PAD_EMMC_IO1_IS_GPIO == GPIO_IN ? BIT5: 0)
    #define PAD_EMMC_IO1_OUT (PAD_EMMC_IO1_IS_GPIO == GPIO_OUT_HIGH ? BIT5: 0)
    _RVM1(0x1e89, PAD_EMMC_IO1_OUT, BIT5),
    _RVM1(0x1e88, PAD_EMMC_IO1_OEN, BIT5),
    //reg_led_mode[1:0]
    _RVM1(0x1EB4, 0, BIT5 | BIT4 ),   //reg[101EB4]#5  ~ #4 = 00b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    //reg_nand_mode[2:0]
    _RVM1(0x1EA0, 0, BIT7 ),   //reg[101EA0]#9  ~ #7 = 000b
    _RVM1(0x1EA1, 0, BIT1 | BIT0 ),   //reg[101EA0]#9  ~ #7 = 000b
    //reg_pwm1_mode[1:0]
    _RVM1(0x1EAA, 0, BIT5 | BIT4 ),   //reg[101EAA]#5  ~ #4 = 00b
    #endif

    #if(PAD_EMMC_IO2_IS_GPIO != GPIO_NONE)
    #define PAD_EMMC_IO2_OEN (PAD_EMMC_IO2_IS_GPIO == GPIO_IN ? BIT2: 0)
    #define PAD_EMMC_IO2_OUT (PAD_EMMC_IO2_IS_GPIO == GPIO_OUT_HIGH ? BIT2: 0)
    _RVM1(0x1e89, PAD_EMMC_IO2_OUT, BIT2),
    _RVM1(0x1e88, PAD_EMMC_IO2_OEN, BIT2),
    //reg_spi_nor
    _RVM1(0x1EF0, 0, BIT1  ),   //reg[101EF0]#1  = 0b
    //reg_test_in_mode[2:0]
    _RVM1(0x1E24, 0, BIT2 | BIT1 | BIT0 ),   //reg[101E24]#2  ~ #0 = 000b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    //reg_nand_mode[2:0]
    _RVM1(0x1EA0, 0, BIT7 ),   //reg[101EA0]#9  ~ #7 = 000b
    _RVM1(0x1EA1, 0, BIT1 | BIT0 ),   //reg[101EA0]#9  ~ #7 = 000b
    //reg_spi_nand
    _RVM1(0x1EF0, 0, BIT0  ),   //reg[101EF0]#0  = 0b
    //reg_test_out_mode[2:0]
    _RVM1(0x1E24, 0, BIT6 | BIT5 | BIT4 ),   //reg[101E24]#6  ~ #4 = 000b
    //reg_emmc_config2
    _RVM1(0x1E3B, 0, BIT0 ),   //reg[101E3B]#0 = 0b
    #endif

    #if(PAD_EMMC_IO3_IS_GPIO != GPIO_NONE)
    #define PAD_EMMC_IO3_OEN (PAD_EMMC_IO3_IS_GPIO == GPIO_IN ? BIT0: 0)
    #define PAD_EMMC_IO3_OUT (PAD_EMMC_IO3_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
    _RVM1(0x100F, PAD_EMMC_IO3_OUT, BIT0),
    _RVM1(0x1011, PAD_EMMC_IO3_OEN, BIT0),
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    //reg_nand_mode[2:0]
    _RVM1(0x1EA0, 0, BIT7 ),   //reg[101EA0]#9  ~ #7 = 000b
    _RVM1(0x1EA1, 0, BIT1 | BIT0 ),   //reg[101EA0]#9  ~ #7 = 000b
    #endif

    #if(PAD_EMMC_IO4_IS_GPIO != GPIO_NONE)
    #define PAD_EMMC_IO4_OEN (PAD_EMMC_IO4_IS_GPIO == GPIO_IN ? BIT3: 0)
    #define PAD_EMMC_IO4_OUT (PAD_EMMC_IO4_IS_GPIO == GPIO_OUT_HIGH ? BIT3: 0)
    _RVM1(0x1e89, PAD_EMMC_IO4_OUT, BIT3),
    _RVM1(0x1e88, PAD_EMMC_IO4_OEN, BIT3),
    //reg_test_in_mode[2:0]
    _RVM1(0x1E24, 0, BIT2 | BIT1 | BIT0 ),   //reg[101E24]#2  ~ #0 = 000b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    //reg_nand_mode[2:0]
    _RVM1(0x1EA0, 0, BIT7 ),   //reg[101EA0]#9  ~ #7 = 000b
    _RVM1(0x1EA1, 0, BIT1 | BIT0 ),   //reg[101EA0]#9  ~ #7 = 000b
    //reg_spi_nand
    _RVM1(0x1EF0, 0, BIT0  ),   //reg[101EF0]#0  = 0b
    //reg_test_out_mode[2:0]
    _RVM1(0x1E24, 0, BIT6 | BIT5 | BIT4 ),   //reg[101E24]#6  ~ #4 = 000b
    //reg_emmc_config2
    _RVM1(0x1E3B, 0, BIT0 ),   //reg[101E3B]#0 = 0b
    #endif

    #if(PAD_EMMC_IO5_IS_GPIO != GPIO_NONE)
    #define PAD_EMMC_IO5_OEN (PAD_EMMC_IO5_IS_GPIO == GPIO_IN ? BIT1: 0)
    #define PAD_EMMC_IO5_OUT (PAD_EMMC_IO5_IS_GPIO == GPIO_OUT_HIGH ? BIT1: 0)
    _RVM1(0x1e89, PAD_EMMC_IO5_OUT, BIT1),
    _RVM1(0x1e88, PAD_EMMC_IO5_OEN, BIT1),
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    //reg_nand_cs1_en
    _RVM1(0x1EA1, 0, BIT2 ),   //reg[101EA1]#2 = 0b
    #endif

    #if(PAD_EMMC_IO6_IS_GPIO != GPIO_NONE)
    #define PAD_EMMC_IO6_OEN (PAD_EMMC_IO6_IS_GPIO == GPIO_IN ? BIT4: 0)
    #define PAD_EMMC_IO6_OUT (PAD_EMMC_IO6_IS_GPIO == GPIO_OUT_HIGH ? BIT4: 0)
    _RVM1(0x1e89, PAD_EMMC_IO6_OUT, BIT4),
    _RVM1(0x1e88, PAD_EMMC_IO6_OEN, BIT4),
    //reg_spi_nor
    _RVM1(0x1EF0, 0, BIT1  ),   //reg[101EF0]#1  = 0b
    //reg_test_in_mode[2:0]
    _RVM1(0x1E24, 0, BIT2 | BIT1 | BIT0 ),   //reg[101E24]#2  ~ #0 = 000b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    //reg_nand_mode[2:0]
    _RVM1(0x1EA0, 0, BIT7 ),   //reg[101EA0]#9  ~ #7 = 000b
    _RVM1(0x1EA1, 0, BIT1 | BIT0 ),   //reg[101EA0]#9  ~ #7 = 000b
    //reg_spi_nand
    _RVM1(0x1EF0, 0, BIT0  ),   //reg[101EF0]#0  = 0b
    //reg_test_out_mode[2:0]
    _RVM1(0x1E24, 0, BIT6 | BIT5 | BIT4 ),   //reg[101E24]#6  ~ #4 = 000b
    #endif

    #if(PAD_EMMC_IO7_IS_GPIO != GPIO_NONE)
    #define PAD_EMMC_IO7_OEN (PAD_EMMC_IO7_IS_GPIO == GPIO_IN ? BIT1: 0)
    #define PAD_EMMC_IO7_OUT (PAD_EMMC_IO7_IS_GPIO == GPIO_OUT_HIGH ? BIT1: 0)
    _MEMMAP_nonPM_SPARE0_,
    _RVM1(0x100F, PAD_EMMC_IO7_OUT, BIT1),
    _RVM1(0x1011, PAD_EMMC_IO7_OEN, BIT1),
    _MEMMAP_nonPM_,
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    #endif

    #if(PAD_EMMC_IO8_IS_GPIO != GPIO_NONE)
    #define PAD_EMMC_IO8_OEN (PAD_EMMC_IO8_IS_GPIO == GPIO_IN ? BIT6: 0)
    #define PAD_EMMC_IO8_OUT (PAD_EMMC_IO8_IS_GPIO == GPIO_OUT_HIGH ? BIT6: 0)
    _RVM1(0x1e89, PAD_EMMC_IO8_OUT, BIT6),
    _RVM1(0x1e88, PAD_EMMC_IO8_OEN, BIT6),
    //reg_spi_nor
    _RVM1(0x1EF0, 0, BIT1  ),   //reg[101EF0]#1  = 0b
    //reg_fourthuartmode[1:0]
    _RVM1(0x1E04, 0, BIT7 | BIT6 ),   //reg[101E04]#7  ~ #6 = 00b
    //reg_sd_config_eco
    _RVM1(0x1E3B, 0, BIT1 ),   //reg[101E3B]#1 = 0b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    //reg_nand_mode[2:0]
    _RVM1(0x1EA0, 0, BIT7 ),   //reg[101EA0]#9  ~ #7 = 000b
    _RVM1(0x1EA1, 0, BIT1 | BIT0 ),   //reg[101EA0]#9  ~ #7 = 000b
    //reg_test_in_mode[2:0]
    _RVM1(0x1E24, 0, BIT2 | BIT1 | BIT0 ),   //reg[101E24]#2  ~ #0 = 000b
    //reg_test_out_mode[2:0]
    _RVM1(0x1E24, 0, BIT6 | BIT5 | BIT4 ),   //reg[101E24]#6  ~ #4 = 000b
    //reg_spi_nand
    _RVM1(0x1EF0, 0, BIT0  ),   //reg[101EF0]#0  = 0b
    #endif

    #if(PAD_EMMC_IO9_IS_GPIO != GPIO_NONE)
    #define PAD_EMMC_IO9_OEN (PAD_EMMC_IO9_IS_GPIO == GPIO_IN ? BIT7: 0)
    #define PAD_EMMC_IO9_OUT (PAD_EMMC_IO9_IS_GPIO == GPIO_OUT_HIGH ? BIT7: 0)
    _MEMMAP_nonPM_SPARE0_,
    _RVM1(0x100E, PAD_EMMC_IO9_OUT, BIT7),
    _RVM1(0x1010, PAD_EMMC_IO9_OEN, BIT7),
    _MEMMAP_nonPM_,
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    //reg_nand_mode[2:0]
    _RVM1(0x1EA0, 0, BIT7 ),   //reg[101EA0]#9  ~ #7 = 000b
    _RVM1(0x1EA1, 0, BIT0 | BIT1 ),   //reg[101EA0]#9  ~ #7 = 000b
    #endif

    #if(PAD_EMMC_IO10_IS_GPIO != GPIO_NONE)
    #define PAD_EMMC_IO10_OEN (PAD_EMMC_IO10_IS_GPIO == GPIO_IN ? BIT7: 0)
    #define PAD_EMMC_IO10_OUT (PAD_EMMC_IO10_IS_GPIO == GPIO_OUT_HIGH ? BIT7: 0)
    _RVM1(0x1e89, PAD_EMMC_IO10_OUT, BIT7),
    _RVM1(0x1e88, PAD_EMMC_IO10_OEN, BIT7),
    //reg_spi_nor
    _RVM1(0x1EF0, 0, BIT1  ),   //reg[101EF0]#1  = 0b
    //reg_test_in_mode[2:0]
    _RVM1(0x1E24, 0, BIT2 | BIT1 | BIT0 ),   //reg[101E24]#2  ~ #0 = 000b
    //reg_sd_config_eco
    _RVM1(0x1E3B, 0, BIT1 ),   //reg[101E3B]#1 = 0b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    //reg_nand_mode[2:0]
    _RVM1(0x1EA0, 0, BIT7 ),   //reg[101EA0]#9  ~ #7 = 000b
    _RVM1(0x1EA1, 0, BIT1 | BIT0 ),   //reg[101EA0]#9  ~ #7 = 000b
    //reg_spi_nand
    _RVM1(0x1EF0, 0, BIT0  ),   //reg[101EF0]#0  = 0b
    //reg_test_out_mode[2:0]
    _RVM1(0x1E24, 0, BIT6 | BIT5 | BIT4 ),   //reg[101E24]#6  ~ #4 = 000b
    #endif

    #if(PAD_EMMC_IO11_IS_GPIO != GPIO_NONE)
    #define PAD_EMMC_IO11_OEN (PAD_EMMC_IO11_IS_GPIO == GPIO_IN ? BIT6: 0)
    #define PAD_EMMC_IO11_OUT (PAD_EMMC_IO11_IS_GPIO == GPIO_OUT_HIGH ? BIT6: 0)
    _MEMMAP_nonPM_SPARE0_,
    _RVM1(0x100E, PAD_EMMC_IO11_OUT, BIT6),
    _RVM1(0x1010, PAD_EMMC_IO11_OEN, BIT6),
    _MEMMAP_nonPM_,
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    //reg_nand_mode[2:0]
    _RVM1(0x1EA0, 0, BIT7 ),   //reg[101EA0]#9  ~ #7 = 000b
    _RVM1(0x1EA1, 0, BIT1 | BIT0 ),   //reg[101EA0]#9  ~ #7 = 000b
    #endif

    #if(PAD_EMMC_IO12_IS_GPIO != GPIO_NONE)
    #define PAD_EMMC_IO12_OEN (PAD_EMMC_IO12_IS_GPIO == GPIO_IN ? BIT5: 0)
    #define PAD_EMMC_IO12_OUT (PAD_EMMC_IO12_IS_GPIO == GPIO_OUT_HIGH ? BIT5: 0)
    _MEMMAP_nonPM_SPARE0_,
    _RVM1(0x100E, PAD_EMMC_IO12_OUT, BIT5),
    _RVM1(0x1010, PAD_EMMC_IO12_OEN, BIT5),
    _MEMMAP_nonPM_,
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    //reg_nand_mode[2:0]
    _RVM1(0x1EA0, 0, BIT7 ),   //reg[101EA0]#9  ~ #7 = 000b
    _RVM1(0x1EA1, 0, BIT1 | BIT0 ),   //reg[101EA0]#9  ~ #7 = 000b
    #endif

    #if(PAD_EMMC_IO13_IS_GPIO != GPIO_NONE)
    #define PAD_EMMC_IO13_OEN (PAD_EMMC_IO13_IS_GPIO == GPIO_IN ? BIT4: 0)
    #define PAD_EMMC_IO13_OUT (PAD_EMMC_IO13_IS_GPIO == GPIO_OUT_HIGH ? BIT4: 0)
    _MEMMAP_nonPM_SPARE0_,
    _RVM1(0x100E, PAD_EMMC_IO13_OUT, BIT4),
    _RVM1(0x1010, PAD_EMMC_IO13_OEN, BIT4),
    _MEMMAP_nonPM_,
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    //reg_nand_mode[2:0]
    _RVM1(0x1EA0, 0, BIT7 ),   //reg[101EA0]#9  ~ #7 = 000b
    _RVM1(0x1EA1, 0, BIT1 | BIT0 ),   //reg[101EA0]#9  ~ #7 = 000b
    #endif

    #if(PAD_EMMC_IO14_IS_GPIO != GPIO_NONE)
    #define PAD_EMMC_IO14_OEN (PAD_EMMC_IO14_IS_GPIO == GPIO_IN ? BIT3: 0)
    #define PAD_EMMC_IO14_OUT (PAD_EMMC_IO14_IS_GPIO == GPIO_OUT_HIGH ? BIT3: 0)
    _MEMMAP_nonPM_SPARE0_,
    _RVM1(0x100E, PAD_EMMC_IO14_OUT, BIT3),
    _RVM1(0x1010, PAD_EMMC_IO14_OEN, BIT3),
    _MEMMAP_nonPM_,
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    //reg_nand_mode[2:0]
    _RVM1(0x1EA0, 0, BIT7 ),   //reg[101EA0]#9  ~ #7 = 000b
    _RVM1(0x1EA1, 0, BIT1 | BIT0 ),   //reg[101EA0]#9  ~ #7 = 000b
    #endif

    #if(PAD_EMMC_IO15_IS_GPIO != GPIO_NONE)
    #define PAD_EMMC_IO15_OEN (PAD_EMMC_IO15_IS_GPIO == GPIO_IN ? BIT2: 0)
    #define PAD_EMMC_IO15_OUT (PAD_EMMC_IO15_IS_GPIO == GPIO_OUT_HIGH ? BIT2: 0)
    _MEMMAP_nonPM_SPARE0_,
    _RVM1(0x100E, PAD_EMMC_IO15_OUT, BIT2),
    _RVM1(0x1010, PAD_EMMC_IO15_OEN, BIT2),
    _MEMMAP_nonPM_,
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    //reg_nand_mode[2:0]
    _RVM1(0x1EA0, 0, BIT7 ),   //reg[101EA0]#9  ~ #7 = 000b
    _RVM1(0x1EA1, 0, BIT1 | BIT0 ),   //reg[101EA0]#9  ~ #7 = 000b
    #endif

    #if(PAD_EMMC_IO16_IS_GPIO != GPIO_NONE)
    #define PAD_EMMC_IO16_OEN (PAD_EMMC_IO16_IS_GPIO == GPIO_IN ? BIT1: 0)
    #define PAD_EMMC_IO16_OUT (PAD_EMMC_IO16_IS_GPIO == GPIO_OUT_HIGH ? BIT1: 0)
    _MEMMAP_nonPM_SPARE0_,
    _RVM1(0x100E, PAD_EMMC_IO16_OUT, BIT1),
    _RVM1(0x1010, PAD_EMMC_IO16_OEN, BIT1),
    _MEMMAP_nonPM_,
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    //reg_nand_mode[2:0]
    _RVM1(0x1EA0, 0, BIT7 ),   //reg[101EA0]#9  ~ #7 = 000b
    _RVM1(0x1EA1, 0, BIT1 | BIT0 ),   //reg[101EA0]#9  ~ #7 = 000b
    #endif

    #if(PAD_EMMC_IO17_IS_GPIO != GPIO_NONE)
    #define PAD_EMMC_IO17_OEN (PAD_EMMC_IO17_IS_GPIO == GPIO_IN ? BIT0: 0)
    #define PAD_EMMC_IO17_OUT (PAD_EMMC_IO17_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
    _MEMMAP_nonPM_SPARE0_,
    _RVM1(0x100E, PAD_EMMC_IO17_OUT, BIT0),
    _RVM1(0x1010, PAD_EMMC_IO17_OEN, BIT0),
    _MEMMAP_nonPM_,
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    //reg_nand_mode[2:0]
    _RVM1(0x1EA0, 0, BIT7 ),   //reg[101EA0]#9  ~ #7 = 000b
    _RVM1(0x1EA1, 0, BIT1 | BIT0 ),   //reg[101EA0]#9  ~ #7 = 000b
    #endif

    #if(PAD_VSYNC_LIKE_IS_GPIO != GPIO_NONE)
    #define PAD_VSYNC_LIKE_OEN (PAD_VSYNC_LIKE_IS_GPIO == GPIO_IN ? BIT4: 0)
    #define PAD_VSYNC_LIKE_OUT (PAD_VSYNC_LIKE_IS_GPIO == GPIO_OUT_HIGH ? BIT5: 0)
    _RVM1(0x1e91, PAD_VSYNC_LIKE_OUT, BIT5),
    _RVM1(0x1e91, PAD_VSYNC_LIKE_OEN, BIT4),
    //reg_ld_spi1cz_config[1:0]
    _RVM1(0x1E3A, 0, BIT5 | BIT4 ),   //reg[101E3A]#5  ~ #4 = 00b
    //reg_pwm5_mode[1:0]
    _RVM1(0x1E3A, 0, BIT1 | BIT0 ),   //reg[101E3A]#1  ~ #0 = 00b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    //reg_ld_spi4_config
    _RVM1(0x1EB4, 0, BIT3  ),   //reg[101EB4]#3  = 0b
    #endif

    #if(PAD_I2S_IN_WS_IS_GPIO != GPIO_NONE)
    #define PAD_I2S_IN_WS_OEN (PAD_I2S_IN_WS_IS_GPIO == GPIO_IN ? BIT0: 0)
    #define PAD_I2S_IN_WS_OUT (PAD_I2S_IN_WS_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
    _RVM1(0x1e6C, PAD_I2S_IN_WS_OUT, BIT0),
    _RVM1(0x1e6E, PAD_I2S_IN_WS_OEN, BIT0),
    //reg_i2s_in_master_mode
    _RVM1(0x1E3E, 0, BIT0  ),   //reg[101E3E]#0  = 0b
    //reg_extint6
    _RVM1(0x1EE0, 0, BIT6  ),   //reg[101EE0]#6  = 0b
    //reg_dspejtagmode[1:0]
    _RVM1(0x1E07, 0, BIT1 | BIT0 ),   //reg[101E07]#1 ~ #0 = 00b
    //reg_i2sinconfig
    _RVM1(0x1EAE, 0, BIT2  ),   //reg[101EAE]#2  = 0b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    //reg_mcu_jtag_mode[1:0]
    _RVM1(0x1E06, 0, BIT7 | BIT6 ),   //reg[101E06]#7  ~ #6 = 00b
    //reg_thirduartmode[1:0]
    _RVM1(0x1E05, 0, BIT3 | BIT2 ),   //reg[101E05]#3 ~ #2 = 00b
    //reg_test_out_mode[2:0]
    _RVM1(0x1E24, 0, BIT6 | BIT5 | BIT4 ),   //reg[101E24]#6  ~ #4 = 000b
    //reg_od3rduart[1:0]
    _RVM1(0x1EB6, 0, BIT3 | BIT2 ),   //reg[101EB6]#3  ~ #2 = 00b
    #endif

    #if(PAD_I2S_IN_BCK_IS_GPIO != GPIO_NONE)
    #define PAD_I2S_IN_BCK_OEN (PAD_I2S_IN_BCK_IS_GPIO == GPIO_IN ? BIT1: 0)
    #define PAD_I2S_IN_BCK_OUT (PAD_I2S_IN_BCK_IS_GPIO == GPIO_OUT_HIGH ? BIT1: 0)
    _RVM1(0x1e6C, PAD_I2S_IN_BCK_OUT, BIT1),
    _RVM1(0x1e6E, PAD_I2S_IN_BCK_OEN, BIT1),
    //reg_i2s_in_master_mode
    _RVM1(0x1E3E, 0, BIT0  ),   //reg[101E3E]#0  = 0b
    //reg_dspejtagmode[1:0]
    _RVM1(0x1E07, 0, BIT1 | BIT0 ),   //reg[101E07]#1 ~ #0 = 00b
    //reg_i2sinconfig
    _RVM1(0x1EAE, 0, BIT2  ),   //reg[101EAE]#2  = 0b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    //reg_thirduartmode[1:0]
    _RVM1(0x1E05, 0, BIT3 | BIT2 ),   //reg[101E05]#3 ~ #2 = 00b
    //reg_miic_mode2[1:0]
    _RVM1(0x1EB5, 0, BIT5 | BIT4 ),   //reg[101EB5]#5 ~ #4 = 00b
    //reg_test_out_mode[2:0]
    _RVM1(0x1E24, 0, BIT6 | BIT5 | BIT4 ),   //reg[101E24]#6  ~ #4 = 000b
    //reg_od3rduart[1:0]
    _RVM1(0x1EB6, 0, BIT3 | BIT2 ),   //reg[101EB6]#3  ~ #2 = 00b
    #endif

    #if(PAD_I2S_IN_SD_IS_GPIO != GPIO_NONE)
    #define PAD_I2S_IN_SD_OEN (PAD_I2S_IN_SD_IS_GPIO == GPIO_IN ? BIT2: 0)
    #define PAD_I2S_IN_SD_OUT (PAD_I2S_IN_SD_IS_GPIO == GPIO_OUT_HIGH ? BIT2: 0)
    _RVM1(0x1e6C, PAD_I2S_IN_SD_OUT, BIT2),
    _RVM1(0x1e6E, PAD_I2S_IN_SD_OEN, BIT2),
    //reg_dspejtagmode[1:0]
    _RVM1(0x1E07, 0, BIT1 | BIT0 ),   //reg[101E07]#1 ~ #0 = 00b
    //reg_3dflagconfig[2:0]
    _RVM1(0x1EB3, 0, BIT7 | BIT6 | BIT5 ),   //reg[101EB3]#7 ~ #5 = 000b
    //reg_tserrout[1:0]
    _RVM1(0x1EA2, 0, BIT7 | BIT6 ),   //reg[101EA2]#7  ~ #6 = 00b
    //reg_miic_mode2[1:0]
    _RVM1(0x1EB5, 0, BIT5 | BIT4 ),   //reg[101EB5]#5 ~ #4 = 00b
    //reg_i2sinconfig
    _RVM1(0x1EAE, 0, BIT2  ),   //reg[101EAE]#2  = 0b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    //reg_test_out_mode[2:0]
    _RVM1(0x1E24, 0, BIT6 | BIT5 | BIT4 ),   //reg[101E24]#6  ~ #4 = 000b
    #endif

    #if(PAD_SPDIF_IN_IS_GPIO != GPIO_NONE)
    #define PAD_SPDIF_IN_OEN (PAD_SPDIF_IN_IS_GPIO == GPIO_IN ? BIT3: 0)
    #define PAD_SPDIF_IN_OUT (PAD_SPDIF_IN_IS_GPIO == GPIO_OUT_HIGH ? BIT3: 0)
    _RVM1(0x1e6C, PAD_SPDIF_IN_OUT, BIT3),
    _RVM1(0x1e6E, PAD_SPDIF_IN_OEN, BIT3),
    //reg_spdifinconfig
    _RVM1(0x1EAE, 0, BIT6  ),   //reg[101EAE]#6  = 0b
    //reg_dspejtagmode[1:0]
    _RVM1(0x1E07, 0, BIT1 | BIT0 ),   //reg[101E07]#1 ~ #0 = 00b
    //reg_3dflagconfig[2:0]
    _RVM1(0x1EB3, 0, BIT7 | BIT6 | BIT5 ),   //reg[101EB3]#7 ~ #5 = 000b
    //reg_usbdrvvbusconfig[1:0]
    _RVM1(0x1E04, 0, BIT1 | BIT0 ),   //reg[101E04]#1  ~ #0 = 00b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    //reg_test_out_mode[2:0]
    _RVM1(0x1E24, 0, BIT6 | BIT5 | BIT4 ),   //reg[101E24]#6  ~ #4 = 000b
    #endif

    #if(PAD_SPDIF_OUT_IS_GPIO != GPIO_NONE)
    #define PAD_SPDIF_OUT_OEN (PAD_SPDIF_OUT_IS_GPIO == GPIO_IN ? BIT4: 0)
    #define PAD_SPDIF_OUT_OUT (PAD_SPDIF_OUT_IS_GPIO == GPIO_OUT_HIGH ? BIT4: 0)
    _RVM1(0x1e6C, PAD_SPDIF_OUT_OUT, BIT4),
    _RVM1(0x1e6E, PAD_SPDIF_OUT_OEN, BIT4),
    //reg_test_in_mode[2:0]
    _RVM1(0x1E24, 0, BIT2 | BIT1 | BIT0 ),   //reg[101E24]#2  ~ #0 = 000b
    //reg_ej_config[2:0]
    _RVM1(0x1E27, 0, BIT1),   //reg[101E27]#2 ~ #0 = 000b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    //reg_spdifoutconfig
    _RVM1(0x1EAE, 0, BIT7  ),   //reg[101EAE]#7  = 0b
    //reg_test_out_mode[2:0]
    _RVM1(0x1E24, 0, BIT6 | BIT5 | BIT4 ),   //reg[101E24]#6  ~ #4 = 000b
    #endif

    #if(PAD_I2S_OUT_MCK_IS_GPIO != GPIO_NONE)
    #define PAD_I2S_OUT_MCK_OEN (PAD_I2S_OUT_MCK_IS_GPIO == GPIO_IN ? BIT6: 0)
    #define PAD_I2S_OUT_MCK_OUT (PAD_I2S_OUT_MCK_IS_GPIO == GPIO_OUT_HIGH ? BIT6: 0)
    _RVM1(0x1e6C, PAD_I2S_OUT_MCK_OUT, BIT6),
    _RVM1(0x1e6E, PAD_I2S_OUT_MCK_OEN, BIT6),
    //reg_i2soutconfig[1:0]
    _RVM1(0x1EAE, 0, BIT5 | BIT4 ),   //reg[101EAE]#5  ~ #4 = 00b
    //reg_test_in_mode[2:0]
    _RVM1(0x1E24, 0, BIT2 | BIT1 | BIT0 ),   //reg[101E24]#2  ~ #0 = 000b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    //reg_test_out_mode[2:0]
    _RVM1(0x1E24, 0, BIT6 | BIT5 | BIT4 ),   //reg[101E24]#6  ~ #4 = 000b
    #endif

    #if(PAD_I2S_OUT_WS_IS_GPIO != GPIO_NONE)
    #define PAD_I2S_OUT_WS_OEN (PAD_I2S_OUT_WS_IS_GPIO == GPIO_IN ? BIT5: 0)
    #define PAD_I2S_OUT_WS_OUT (PAD_I2S_OUT_WS_IS_GPIO == GPIO_OUT_HIGH ? BIT5: 0)
    _RVM1(0x1e6C, PAD_I2S_OUT_WS_OUT, BIT5),
    _RVM1(0x1e6E, PAD_I2S_OUT_WS_OEN, BIT5),
    //reg_i2soutconfig[1:0]
    _RVM1(0x1EAE, 0, BIT5 | BIT4 ),   //reg[101EAE]#5  ~ #4 = 00b
    //reg_test_in_mode[2:0]
    _RVM1(0x1E24, 0, BIT2 | BIT1 | BIT0 ),   //reg[101E24]#2  ~ #0 = 000b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    //reg_test_out_mode[2:0]
    _RVM1(0x1E24, 0, BIT6 | BIT5 | BIT4 ),   //reg[101E24]#6  ~ #4 = 000b
    #endif

    #if(PAD_I2S_OUT_BCK_IS_GPIO != GPIO_NONE)
    #define PAD_I2S_OUT_BCK_OEN (PAD_I2S_OUT_BCK_IS_GPIO == GPIO_IN ? BIT7: 0)
    #define PAD_I2S_OUT_BCK_OUT (PAD_I2S_OUT_BCK_IS_GPIO == GPIO_OUT_HIGH ? BIT7: 0)
    _RVM1(0x1e6C, PAD_I2S_OUT_BCK_OUT, BIT7),
    _RVM1(0x1e6E, PAD_I2S_OUT_BCK_OEN, BIT7),
    //reg_i2soutconfig[1:0]
    _RVM1(0x1EAE, 0, BIT5 | BIT4 ),   //reg[101EAE]#5  ~ #4 = 00b
    //reg_test_in_mode[2:0]
    _RVM1(0x1E24, 0, BIT2 | BIT1 | BIT0 ),   //reg[101E24]#2  ~ #0 = 000b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    //reg_test_out_mode[2:0]
    _RVM1(0x1E24, 0, BIT6 | BIT5 | BIT4 ),   //reg[101E24]#6  ~ #4 = 000b
    #endif

    #if(PAD_I2S_OUT_SD_IS_GPIO != GPIO_NONE)
    #define PAD_I2S_OUT_SD_OEN (PAD_I2S_OUT_SD_IS_GPIO == GPIO_IN ? BIT0: 0)
    #define PAD_I2S_OUT_SD_OUT (PAD_I2S_OUT_SD_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
    _RVM1(0x1e6D, PAD_I2S_OUT_SD_OUT, BIT0),
    _RVM1(0x1e6F, PAD_I2S_OUT_SD_OEN, BIT0),
    //reg_i2soutconfig[1:0]
    _RVM1(0x1EAE, 0, BIT5 | BIT4 ),   //reg[101EAE]#5  ~ #4 = 00b
    //reg_test_in_mode[2:0]
    _RVM1(0x1E24, 0, BIT2 | BIT1 | BIT0 ),   //reg[101E24]#2  ~ #0 = 000b
    //reg_ej_config[2:0]
    _RVM1(0x1E27, 0, BIT1),   //reg[101E27]#2 ~ #0 = 000b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    //reg_test_out_mode[2:0]
    _RVM1(0x1E24, 0, BIT6 | BIT5 | BIT4 ),   //reg[101E24]#6  ~ #4 = 000b
    #endif

    #if(PAD_I2S_OUT_SD1_IS_GPIO != GPIO_NONE)
    #define PAD_I2S_OUT_SD1_OEN (PAD_I2S_OUT_SD1_IS_GPIO == GPIO_IN ? BIT1: 0)
    #define PAD_I2S_OUT_SD1_OUT (PAD_I2S_OUT_SD1_IS_GPIO == GPIO_OUT_HIGH ? BIT1: 0)
    _RVM1(0x1e6D, PAD_I2S_OUT_SD1_OUT, BIT1),
    _RVM1(0x1e6F, PAD_I2S_OUT_SD1_OEN, BIT1),
    //reg_i2soutconfig[1:0]
    _RVM1(0x1EAE, 0, BIT5 | BIT4 ),   //reg[101EAE]#5  ~ #4 = 00b
    //reg_spdifoutconfig2
    _RVM1(0x1EAF, 0, BIT4 ),   //reg[101EAF]#4 = 0b
    //reg_usbdrvvbusconfig[1:0]
    _RVM1(0x1E04, 0, BIT1 | BIT0 ),   //reg[101E04]#1  ~ #0 = 00b
    //reg_miic_mode5
    _RVM1(0x1EC8, 0, BIT3  ),   //reg[101EC8]#3  = 0b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    //reg_mcu_jtag_mode[1:0]
    _RVM1(0x1E06, 0, BIT7 | BIT6 ),   //reg[101E06]#7  ~ #6 = 00b
    #endif

    #if(PAD_I2S_OUT_SD2_IS_GPIO != GPIO_NONE)
    #define PAD_I2S_OUT_SD2_OEN (PAD_I2S_OUT_SD2_IS_GPIO == GPIO_IN ? BIT2: 0)
    #define PAD_I2S_OUT_SD2_OUT (PAD_I2S_OUT_SD2_IS_GPIO == GPIO_OUT_HIGH ? BIT2: 0)
    _RVM1(0x1e6D, PAD_I2S_OUT_SD2_OUT, BIT2),
    _RVM1(0x1e6F, PAD_I2S_OUT_SD2_OEN, BIT2),
    //reg_od4thuart[1:0]
    _RVM1(0x1EB6, 0, BIT5 | BIT4 ),   //reg[101EB6]#5  ~ #4 = 00b
    //reg_i2soutconfig[1:0]
    _RVM1(0x1EAE, 0, BIT5 | BIT4 ),   //reg[101EAE]#5  ~ #4 = 00b
    //reg_usbdrvvbusconfig[1:0]
    _RVM1(0x1E04, 0, BIT1 | BIT0 ),   //reg[101E04]#1  ~ #0 = 00b
    //reg_miic_mode5
    _RVM1(0x1EC8, 0, BIT3  ),   //reg[101EC8]#3  = 0b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    //reg_mcu_jtag_mode[1:0]
    _RVM1(0x1E06, 0, BIT7 | BIT6 ),   //reg[101E06]#7  ~ #6 = 00b
    #endif

    #if(PAD_I2S_OUT_SD3_IS_GPIO != GPIO_NONE)
    #define PAD_I2S_OUT_SD3_OEN (PAD_I2S_OUT_SD3_IS_GPIO == GPIO_IN ? BIT3: 0)
    #define PAD_I2S_OUT_SD3_OUT (PAD_I2S_OUT_SD3_IS_GPIO == GPIO_OUT_HIGH ? BIT3: 0)
    _RVM1(0x1e6D, PAD_I2S_OUT_SD3_OUT, BIT3),
    _RVM1(0x1e6F, PAD_I2S_OUT_SD3_OEN, BIT3),
    //reg_od4thuart[1:0]
    _RVM1(0x1EB6, 0, BIT5 | BIT4 ),   //reg[101EB6]#5  ~ #4 = 00b
    //reg_i2soutconfig[1:0]
    _RVM1(0x1EAE, 0, BIT5 | BIT4 ),   //reg[101EAE]#5  ~ #4 = 00b
    //reg_usbdrvvbusconfig[1:0]
    _RVM1(0x1E04, 0, BIT1 | BIT0 ),   //reg[101E04]#1  ~ #0 = 00b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    //reg_mcu_jtag_mode[1:0]
    _RVM1(0x1E06, 0, BIT7 | BIT6 ),   //reg[101E06]#7  ~ #6 = 00b
    #endif

    #if(PAD_B_ODD0_IS_GPIO != GPIO_NONE)
    #define PAD_B_ODD0_OEN (PAD_B_ODD0_IS_GPIO == GPIO_IN ? BIT3: 0)
    #define PAD_B_ODD0_OUT (PAD_B_ODD0_IS_GPIO == GPIO_OUT_HIGH ? BIT3: 0)
    _RVM1(0x32a1, PAD_B_ODD0_OUT, BIT3),
    _RVM1(0x32a5, PAD_B_ODD0_OEN, BIT3),
    //reg_test_in_mode[2:0]
    _RVM1(0x1e24, 0, BIT2 | BIT1 | BIT0 ),   //reg[101e24]#2~#0 = 000b
    //reg_test_out_mode[2:0]
    _RVM1(0x1e24, 0, BIT6 | BIT5 | BIT4 ),   //reg[101e24]#6~#4 = 000b
    _RVM1(0x328f, BIT3 , BIT3 ),   //reg[10328f]  #3 = 1
    _RVM1(0x329d, BIT3 , BIT3 ),   //reg[10329d] #3 = 1
    _RVM1(0x32dd, 0, BIT3~BIT2 ),   //reg[1032dd] #3~#2 = 00
    #endif

    #if(PAD_B_ODD1_IS_GPIO != GPIO_NONE)
    #define PAD_B_ODD1_OEN (PAD_B_ODD1_IS_GPIO == GPIO_IN ? BIT2: 0)
    #define PAD_B_ODD1_OUT (PAD_B_ODD1_IS_GPIO == GPIO_OUT_HIGH ? BIT2: 0)
    _RVM1(0x32a1, PAD_B_ODD1_OUT, BIT2),
    _RVM1(0x32a5, PAD_B_ODD1_OEN, BIT2),
    //reg_test_in_mode[2:0]
    _RVM1(0x1e24, 0, BIT2 | BIT1 | BIT0 ),   //reg[101e24]#2~#0 = 000b
    //reg_test_out_mode[2:0]
    _RVM1(0x1e24, 0, BIT6 | BIT5 | BIT4 ),   //reg[101e24]#6~#4 = 000b
    _RVM1(0x328f, BIT2 , BIT2 ),   //reg[10328f]  #2 = 1
    _RVM1(0x329d, BIT2 , BIT2 ),   //reg[10329d] #2 = 1
    _RVM1(0x32dd, 0, BIT3~BIT2 ),   //reg[1032dd] #3~#2 = 00
    #endif

    #if(PAD_B_ODD2_IS_GPIO != GPIO_NONE)
    #define PAD_B_ODD2_OEN (PAD_B_ODD2_IS_GPIO == GPIO_IN ? BIT1: 0)
    #define PAD_B_ODD2_OUT (PAD_B_ODD2_IS_GPIO == GPIO_OUT_HIGH ? BIT1: 0)
    _RVM1(0x32a1, PAD_B_ODD2_OUT, BIT1),
    _RVM1(0x32a5, PAD_B_ODD2_OEN, BIT1),
    //reg_test_in_mode[2:0]
    _RVM1(0x1e24, 0, BIT2 | BIT1 | BIT0 ),   //reg[101e24]#2~#0 = 000b
    //reg_test_out_mode[2:0]
    _RVM1(0x1e24, 0, BIT6 | BIT5 | BIT4 ),   //reg[101e24]#6~#4 = 000b
    _RVM1(0x328f, BIT1 , BIT1 ),   //reg[10328f]  #1 = 1
    _RVM1(0x329d, BIT1 , BIT1 ),   //reg[10329d] #1 = 1
    _RVM1(0x32dd, 0, BIT1 | BIT0 ),   //reg[1032dd] #1~#0 = 00
    #endif

    #if(PAD_B_ODD3_IS_GPIO != GPIO_NONE)
    #define PAD_B_ODD3_OEN (PAD_B_ODD3_IS_GPIO == GPIO_IN ? BIT0: 0)
    #define PAD_B_ODD3_OUT (PAD_B_ODD3_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
    _RVM1(0x32a1, PAD_B_ODD3_OUT, BIT0),
    _RVM1(0x32a5, PAD_B_ODD3_OEN, BIT0),
    //reg_test_in_mode[2:0]
    _RVM1(0x1e24, 0, BIT2 | BIT1 | BIT0 ),   //reg[101e24]#2~#0 = 000b
    //reg_test_out_mode[2:0]
    _RVM1(0x1e24, 0, BIT6 | BIT5 | BIT4 ),   //reg[101e24]#6~#4 = 000b
    _RVM1(0x328f, BIT0 , BIT0 ),   //reg[10328f]  #0 = 1
    _RVM1(0x329d, BIT0 , BIT0 ),   //reg[10329d] #0 = 1
    _RVM1(0x32dd, 0, BIT1 | BIT0 ),   //reg[1032dd] #1~#0 = 00
    #endif

    #if(PAD_B_ODD4_IS_GPIO != GPIO_NONE)
    #define PAD_B_ODD4_OEN (PAD_B_ODD4_IS_GPIO == GPIO_IN ? BIT7: 0)
    #define PAD_B_ODD4_OUT (PAD_B_ODD4_IS_GPIO == GPIO_OUT_HIGH ? BIT7: 0)
    _RVM1(0x32a0, PAD_B_ODD4_OUT, BIT7),
    _RVM1(0x32a4, PAD_B_ODD4_OEN, BIT7),
    //reg_test_in_mode[2:0]
    _RVM1(0x1e24, 0, BIT2 | BIT1 | BIT0 ),   //reg[101e24]#2~#0 = 000b
    //reg_test_out_mode[2:0]
    _RVM1(0x1e24, 0, BIT6 | BIT5 | BIT4 ),   //reg[101e24]#6~#4 = 000b
    _RVM1(0x328e, BIT7 , BIT7 ),   //reg[10328e] #7 = 1
    _RVM1(0x329c, BIT7 , BIT7 ),   //reg[10329c] #7 = 1
    _RVM1(0x32dc, 0, BIT7 | BIT6 ),   //reg[1032dc] #7~#6 = 00
    #endif

    #if(PAD_B_ODD5_IS_GPIO != GPIO_NONE)
    #define PAD_B_ODD5_OEN (PAD_B_ODD5_IS_GPIO == GPIO_IN ? BIT6: 0)
    #define PAD_B_ODD5_OUT (PAD_B_ODD5_IS_GPIO == GPIO_OUT_HIGH ? BIT6: 0)
    _RVM1(0x32a0, PAD_B_ODD5_OUT, BIT6),
    _RVM1(0x32a4, PAD_B_ODD5_OEN, BIT6),
    //reg_test_in_mode[2:0]
    _RVM1(0x1e24, 0, BIT2 | BIT1 | BIT0 ),   //reg[101e24]#2~#0 = 000b
    //reg_test_out_mode[2:0]
    _RVM1(0x1e24, 0, BIT6 | BIT5 | BIT4 ),   //reg[101e24]#6~#4 = 000b
    _RVM1(0x328e, BIT6 , BIT6 ),   //reg[10328e] #6 = 1
    _RVM1(0x329c, BIT6 , BIT6 ),   //reg[10329c] #6 = 1
    _RVM1(0x32dc, 0, BIT7 | BIT6 ),   //reg[1032dc] #7~#6 = 00
    #endif

    #if(PAD_B_ODD6_IS_GPIO != GPIO_NONE)
    #define PAD_B_ODD6_OEN (PAD_B_ODD6_IS_GPIO == GPIO_IN ? BIT5: 0)
    #define PAD_B_ODD6_OUT (PAD_B_ODD6_IS_GPIO == GPIO_OUT_HIGH ? BIT5: 0)
    _RVM1(0x32a0, PAD_B_ODD6_OUT, BIT5),
    _RVM1(0x32a4, PAD_B_ODD6_OEN, BIT5),
    //reg_test_in_mode[2:0]
    _RVM1(0x1e24, 0, BIT2 | BIT1 | BIT0 ),   //reg[101e24]#2~#0 = 000b
    //reg_test_out_mode[2:0]
    _RVM1(0x1e24, 0, BIT6 | BIT5 | BIT4 ),   //reg[101e24]#6~#4 = 000b
    _RVM1(0x328e, BIT5 , BIT5 ),   //reg[10328e] #5 = 1
    _RVM1(0x329c, BIT5 , BIT5 ),   //reg[10329c] #5 = 1
    _RVM1(0x32dc, 0, BIT5 | BIT4 ),   //reg[1032dc] #5~#4 = 00
    #endif

    #if(PAD_B_ODD7_IS_GPIO != GPIO_NONE)
    #define PAD_B_ODD7_OEN (PAD_B_ODD7_IS_GPIO == GPIO_IN ? BIT4: 0)
    #define PAD_B_ODD7_OUT (PAD_B_ODD7_IS_GPIO == GPIO_OUT_HIGH ? BIT4: 0)
    _RVM1(0x32a0, PAD_B_ODD7_OUT, BIT4),
    _RVM1(0x32a4, PAD_B_ODD7_OEN, BIT4),
    //reg_test_in_mode[2:0]
    _RVM1(0x1e24, 0, BIT2 | BIT1 | BIT0 ),   //reg[101e24]#2~#0 = 000b
    //reg_test_out_mode[2:0]
    _RVM1(0x1e24, 0, BIT6 | BIT5 | BIT4 ),   //reg[101e24]#6~#4 = 000b
    _RVM1(0x328e, BIT4 , BIT4 ),   //reg[10328e] #4 = 1
    _RVM1(0x329c, BIT4 , BIT4 ),   //reg[10329c] #4 = 1
    _RVM1(0x32dc, 0, BIT5 | BIT4 ),   //reg[1032dc] #5~#4 = 00
    #endif

    #if(PAD_G_ODD0_IS_GPIO != GPIO_NONE)
    #define PAD_G_ODD0_OEN (PAD_G_ODD0_IS_GPIO == GPIO_IN ? BIT3: 0)
    #define PAD_G_ODD0_OUT (PAD_G_ODD0_IS_GPIO == GPIO_OUT_HIGH ? BIT3: 0)
    _RVM1(0x32a0, PAD_G_ODD0_OUT, BIT3),
    _RVM1(0x32a4, PAD_G_ODD0_OEN, BIT3),
    //reg_test_in_mode[2:0]
    _RVM1(0x1e24, 0, BIT2 | BIT1 | BIT0 ),   //reg[101e24]#2~#0 = 000b
    //reg_test_out_mode[2:0]
    _RVM1(0x1e24, 0, BIT6 | BIT5 | BIT4 ),   //reg[101e24]#6~#4 = 000b
    _RVM1(0x328e, BIT3 , BIT3 ),   //reg[10328e] #3 = 1
    _RVM1(0x329c, BIT3 , BIT3 ),   //reg[10329c] #3 = 1
    _RVM1(0x32dc, 0, BIT3 | BIT2 ),   //reg[1032dc] #3~#2 = 00
    #endif

    #if(PAD_G_ODD1_IS_GPIO != GPIO_NONE)
    #define PAD_G_ODD1_OEN (PAD_G_ODD1_IS_GPIO == GPIO_IN ? BIT2: 0)
    #define PAD_G_ODD1_OUT (PAD_G_ODD1_IS_GPIO == GPIO_OUT_HIGH ? BIT2: 0)
    _RVM1(0x32a0, PAD_G_ODD1_OUT, BIT2),
    _RVM1(0x32a4, PAD_G_ODD1_OEN, BIT2),
    //reg_test_in_mode[2:0]
    _RVM1(0x1e24, 0, BIT2 | BIT1 | BIT0 ),   //reg[101e24]#2~#0 = 000b
    //reg_test_out_mode[2:0]
    _RVM1(0x1e24, 0, BIT6 | BIT5 | BIT4 ),   //reg[101e24]#6~#4 = 000b
    _RVM1(0x328e, BIT2 , BIT2 ),   //reg[10328e] #2 = 1
    _RVM1(0x329c, BIT2 , BIT2 ),   //reg[10329c] #2 = 1
    _RVM1(0x32dc, 0, BIT3 | BIT2 ),   //reg[1032dc] #3~#2 = 00
    #endif

    #if(PAD_G_ODD2_IS_GPIO != GPIO_NONE)
    #define PAD_G_ODD2_OEN (PAD_G_ODD2_IS_GPIO == GPIO_IN ? BIT1: 0)
    #define PAD_G_ODD2_OUT (PAD_G_ODD2_IS_GPIO == GPIO_OUT_HIGH ? BIT1: 0)
    _RVM1(0x32a0, PAD_G_ODD2_OUT, BIT1),
    _RVM1(0x32a4, PAD_G_ODD2_OEN, BIT1),
    //reg_test_in_mode[2:0]
    _RVM1(0x1e24, 0, BIT2 | BIT1 | BIT0 ),   //reg[101e24]#2~#0 = 000b
    //reg_test_out_mode[2:0]
    _RVM1(0x1e24, 0, BIT6 | BIT5 | BIT4 ),   //reg[101e24]#6~#4 = 000b
    _RVM1(0x328e, BIT1 , BIT1 ),   //reg[10328e] #1 = 1
    _RVM1(0x329c, BIT1 , BIT1 ),   //reg[10329c] #1 = 1
    _RVM1(0x32dc, 0, BIT1 | BIT0 ),   //reg[1032dc] #1~#0 = 00
    #endif

    #if(PAD_G_ODD3_IS_GPIO != GPIO_NONE)
    #define PAD_G_ODD3_OEN (PAD_G_ODD3_IS_GPIO == GPIO_IN ? BIT0: 0)
    #define PAD_G_ODD3_OUT (PAD_G_ODD3_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
    _RVM1(0x32a0, PAD_G_ODD3_OUT, BIT0),
    _RVM1(0x32a4, PAD_G_ODD3_OEN, BIT0),
    //reg_test_in_mode[2:0]
    _RVM1(0x1e24, 0, BIT2 | BIT1 | BIT0 ),   //reg[101e24]#2~#0 = 000b
    //reg_test_out_mode[2:0]
    _RVM1(0x1e24, 0, BIT6 | BIT5 | BIT4 ),   //reg[101e24]#6~#4 = 000b
    _RVM1(0x328e, BIT0 , BIT0 ),   //reg[10328e] #0 = 1
    _RVM1(0x329c, BIT0 , BIT0 ),   //reg[10329c] #0 = 1
    _RVM1(0x32dc, 0, BIT1 | BIT0 ),   //reg[1032dc] #1~#0 = 00
    #endif

    #if(PAD_G_ODD4_IS_GPIO != GPIO_NONE)
    #define PAD_G_ODD4_OEN (PAD_G_ODD4_IS_GPIO == GPIO_IN ? BIT7: 0)
    #define PAD_G_ODD4_OUT (PAD_G_ODD4_IS_GPIO == GPIO_OUT_HIGH ? BIT7: 0)
    _RVM1(0x329f, PAD_G_ODD4_OUT, BIT7),
    _RVM1(0x32a3, PAD_G_ODD4_OEN, BIT7),
    //reg_test_in_mode[2:0]
    _RVM1(0x1e24, 0, BIT2 | BIT1 | BIT0 ),   //reg[101e24]#2~#0 = 000b
    //reg_test_out_mode[2:0]
    _RVM1(0x1e24, 0, BIT6 | BIT5 | BIT4 ),   //reg[101e24]#6~#4 = 000b
    _RVM1(0x328d, BIT7 , BIT7 ),   //reg[10328d] #7 = 1
    _RVM1(0x329b, BIT7 , BIT7 ),   //reg[10329b] #7 = 1
    _RVM1(0x32db, 0, BIT7 | BIT6 ),   //reg[1032db] #7~#6 = 00
    #endif

    #if(PAD_G_ODD5_IS_GPIO != GPIO_NONE)
    #define PAD_G_ODD5_OEN (PAD_G_ODD5_IS_GPIO == GPIO_IN ? BIT6: 0)
    #define PAD_G_ODD5_OUT (PAD_G_ODD5_IS_GPIO == GPIO_OUT_HIGH ? BIT6: 0)
    _RVM1(0x329f, PAD_G_ODD5_OUT, BIT6),
    _RVM1(0x32a3, PAD_G_ODD5_OEN, BIT6),
    //reg_test_in_mode[2:0]
    _RVM1(0x1e24, 0, BIT2 | BIT1 | BIT0 ),   //reg[101e24]#2~#0 = 000b
    //reg_test_out_mode[2:0]
    _RVM1(0x1e24, 0, BIT6 | BIT5 | BIT4 ),   //reg[101e24]#6~#4 = 000b
    _RVM1(0x328d, BIT6 , BIT6 ),   //reg[10328d] #6 = 1
    _RVM1(0x329b, BIT6 , BIT6 ),   //reg[10329b] #6 = 1
    _RVM1(0x32db, 0, BIT7 | BIT6 ),   //reg[1032db] #7~#6 = 00
    #endif

    #if(PAD_G_ODD6_IS_GPIO != GPIO_NONE)
    #define PAD_G_ODD6_OEN (PAD_G_ODD6_IS_GPIO == GPIO_IN ? BIT5: 0)
    #define PAD_G_ODD6_OUT (PAD_G_ODD6_IS_GPIO == GPIO_OUT_HIGH ? BIT5: 0)
    _RVM1(0x329f, PAD_G_ODD6_OUT, BIT5),
    _RVM1(0x32a3, PAD_G_ODD6_OEN, BIT5),
    //reg_test_in_mode[2:0]
    _RVM1(0x1e24, 0, BIT2 | BIT1 | BIT0 ),   //reg[101e24]#2~#0 = 000b
    //reg_test_out_mode[2:0]
    _RVM1(0x1e24, 0, BIT6 | BIT5 | BIT4 ),   //reg[101e24]#6~#4 = 000b
    _RVM1(0x328d, BIT5 , BIT5 ),   //reg[10328d] #5 = 1
    _RVM1(0x329b, BIT5 , BIT5 ),   //reg[10329b] #5 = 1
    _RVM1(0x32db, 0, BIT5 | BIT4 ),   //reg[1032db] #5~#4 = 00
    #endif

    #if(PAD_G_ODD7_IS_GPIO != GPIO_NONE)
    #define PAD_G_ODD7_OEN (PAD_G_ODD7_IS_GPIO == GPIO_IN ? BIT4: 0)
    #define PAD_G_ODD7_OUT (PAD_G_ODD7_IS_GPIO == GPIO_OUT_HIGH ? BIT4: 0)
    _RVM1(0x329f, PAD_G_ODD7_OUT, BIT4),
    _RVM1(0x32a3, PAD_G_ODD7_OEN, BIT4),
    //reg_test_in_mode[2:0]
    _RVM1(0x1e24, 0, BIT2 | BIT1 | BIT0 ),   //reg[101e24]#2~#0 = 000b
    //reg_test_out_mode[2:0]
    _RVM1(0x1e24, 0, BIT6 | BIT5 | BIT4 ),   //reg[101e24]#6~#4 = 000b
    _RVM1(0x328d, BIT4 , BIT4 ),   //reg[10328d] #4 = 1
    _RVM1(0x329b, BIT4 , BIT4 ),   //reg[10329b] #4 = 1
    _RVM1(0x32db, 0, BIT5 | BIT4 ),   //reg[1032db] #5~#4 = 00
    #endif

    #if(PAD_R_ODD0_IS_GPIO != GPIO_NONE)
    #define PAD_R_ODD0_OEN (PAD_R_ODD0_IS_GPIO == GPIO_IN ? BIT3: 0)
    #define PAD_R_ODD0_OUT (PAD_R_ODD0_IS_GPIO == GPIO_OUT_HIGH ? BIT3: 0)
    _RVM1(0x329f, PAD_R_ODD0_OUT, BIT3),
    _RVM1(0x32a3, PAD_R_ODD0_OEN, BIT3),
    //reg_test_in_mode[2:0]
    _RVM1(0x1e24, 0, BIT2 | BIT1 | BIT0 ),   //reg[101e24]#2~#0 = 000b
    //reg_test_out_mode[2:0]
    _RVM1(0x1e24, 0, BIT6 | BIT5 | BIT4 ),   //reg[101e24]#6~#4 = 000b
    _RVM1(0x328d, BIT3 , BIT3 ),   //reg[10328d] #3 = 1
    _RVM1(0x329b, BIT3 , BIT3 ),   //reg[10329b] #3 = 1
    _RVM1(0x32db, 0, BIT3 | BIT2 ),   //reg[1032db] #3~#2 = 00
    #endif

    #if(PAD_R_ODD1_IS_GPIO != GPIO_NONE)
    #define PAD_R_ODD1_OEN (PAD_R_ODD1_IS_GPIO == GPIO_IN ? BIT2: 0)
    #define PAD_R_ODD1_OUT (PAD_R_ODD1_IS_GPIO == GPIO_OUT_HIGH ? BIT2: 0)
    _RVM1(0x329f, PAD_R_ODD1_OUT, BIT2),
    _RVM1(0x32a3, PAD_R_ODD1_OEN, BIT2),
    //reg_test_in_mode[2:0]
    _RVM1(0x1e24, 0, BIT2 | BIT1 | BIT0 ),   //reg[101e24]#2~#0 = 000b
    //reg_test_out_mode[2:0]
    _RVM1(0x1e24, 0, BIT6 | BIT5 | BIT4 ),   //reg[101e24]#6~#4 = 000b
    _RVM1(0x328d, BIT2 , BIT2 ),   //reg[10328d] #2 = 1
    _RVM1(0x329b, BIT2 , BIT2 ),   //reg[10329b] #2 = 1
    _RVM1(0x32db, 0, BIT3 | BIT2 ),   //reg[1032db] #3~#2 = 00
    #endif

    #if(PAD_R_ODD2_IS_GPIO != GPIO_NONE)
    #define PAD_R_ODD2_OEN (PAD_R_ODD2_IS_GPIO == GPIO_IN ? BIT1: 0)
    #define PAD_R_ODD2_OUT (PAD_R_ODD2_IS_GPIO == GPIO_OUT_HIGH ? BIT1: 0)
    _RVM1(0x329f, PAD_R_ODD2_OUT, BIT1),
    _RVM1(0x32a3, PAD_R_ODD2_OEN, BIT1),
    //reg_test_in_mode[2:0]
    _RVM1(0x1e24, 0, BIT2 | BIT1 | BIT0 ),   //reg[101e24]#2~#0 = 000b
    //reg_test_out_mode[2:0]
    _RVM1(0x1e24, 0, BIT6 | BIT5 | BIT4 ),   //reg[101e24]#6~#4 = 000b
    _RVM1(0x328d, BIT1 , BIT1 ),   //reg[10328d] #1 = 1
    _RVM1(0x329b, BIT1 , BIT1 ),   //reg[10329b] #1 = 1
    _RVM1(0x32db, 0, BIT1 | BIT0 ),   //reg[1032db] #1~#0 = 00
    #endif

    #if(PAD_R_ODD3_IS_GPIO != GPIO_NONE)
    #define PAD_R_ODD3_OEN (PAD_R_ODD3_IS_GPIO == GPIO_IN ? BIT0: 0)
    #define PAD_R_ODD3_OUT (PAD_R_ODD3_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
    _RVM1(0x329f, PAD_R_ODD3_OUT, BIT0),
    _RVM1(0x32a3, PAD_R_ODD3_OEN, BIT0),
    //reg_test_in_mode[2:0]
    _RVM1(0x1e24, 0, BIT2 | BIT1 | BIT0 ),   //reg[101e24]#2~#0 = 000b
    //reg_test_out_mode[2:0]
    _RVM1(0x1e24, 0, BIT6 | BIT5 | BIT4 ),   //reg[101e24]#6~#4 = 000b
    _RVM1(0x328d, BIT0 , BIT0 ),   //reg[10328d] #0 = 1
    _RVM1(0x329b, BIT0 , BIT0 ),   //reg[10329b] #0 = 1
    _RVM1(0x32db, 0, BIT1 | BIT0 ),   //reg[1032db] #1~#0 = 00
    #endif

    #if(PAD_R_ODD4_IS_GPIO != GPIO_NONE)
    #define PAD_R_ODD4_OEN (PAD_R_ODD4_IS_GPIO == GPIO_IN ? BIT7: 0)
    #define PAD_R_ODD4_OUT (PAD_R_ODD4_IS_GPIO == GPIO_OUT_HIGH ? BIT7: 0)
    _RVM1(0x329e, PAD_R_ODD4_OUT, BIT7),
    _RVM1(0x32a2, PAD_R_ODD4_OEN, BIT7),
    //reg_test_in_mode[2:0]
    _RVM1(0x1e24, 0, BIT2 | BIT1 | BIT0 ),   //reg[101e24]#2~#0 = 000b
    //reg_test_out_mode[2:0]
    _RVM1(0x1e24, 0, BIT6 | BIT5 | BIT4 ),   //reg[101e24]#6~#4 = 000b
    _RVM1(0x328c, BIT7 , BIT7 ),   //reg[10328c] #7 = 1
    _RVM1(0x329a, BIT7 , BIT7 ),   //reg[10329a] #7 = 1
    _RVM1(0x32da, 0, BIT7 | BIT6 ),   //reg[1032da] #7~#6 = 00
    #endif

    #if(PAD_R_ODD5_IS_GPIO != GPIO_NONE)
    #define PAD_R_ODD5_OEN (PAD_R_ODD5_IS_GPIO == GPIO_IN ? BIT6: 0)
    #define PAD_R_ODD5_OUT (PAD_R_ODD5_IS_GPIO == GPIO_OUT_HIGH ? BIT6: 0)
    _RVM1(0x329e, PAD_R_ODD5_OUT, BIT6),
    _RVM1(0x32a2, PAD_R_ODD5_OEN, BIT6),
    //reg_test_in_mode[2:0]
    _RVM1(0x1e24, 0, BIT2 | BIT1 | BIT0 ),   //reg[101e24]#2~#0 = 000b
    //reg_test_out_mode[2:0]
    _RVM1(0x1e24, 0, BIT6 | BIT5 | BIT4 ),   //reg[101e24]#6~#4 = 000b
    _RVM1(0x328c, BIT6 , BIT6 ),   //reg[10328c] #6 = 1
    _RVM1(0x329a, BIT6 , BIT6 ),   //reg[10329a] #6 = 1
    _RVM1(0x32da, 0, BIT7 | BIT6 ),   //reg[1032da] #7~#6 = 00
    #endif

    #if(PAD_R_ODD6_IS_GPIO != GPIO_NONE)
    #define PAD_R_ODD6_OEN (PAD_R_ODD6_IS_GPIO == GPIO_IN ? BIT5: 0)
    #define PAD_R_ODD6_OUT (PAD_R_ODD6_IS_GPIO == GPIO_OUT_HIGH ? BIT5: 0)
    _RVM1(0x329e, PAD_R_ODD6_OUT, BIT5),
    _RVM1(0x32a2, PAD_R_ODD6_OEN, BIT5),
    //reg_test_in_mode[2:0]
    _RVM1(0x1e24, 0, BIT2 | BIT1 | BIT0 ),   //reg[101e24]#2~#0 = 000b
    //reg_test_out_mode[2:0]
    _RVM1(0x1e24, 0, BIT6 | BIT5 | BIT4 ),   //reg[101e24]#6~#4 = 000b
    _RVM1(0x328c, BIT5 , BIT5 ),   //reg[10328c] #5 = 1
    _RVM1(0x329a, BIT5 , BIT5 ),   //reg[10329a] #5 = 1
    _RVM1(0x32da, 0, BIT5 | BIT4 ),   //reg[1032da] #5~#4 = 00
    #endif

    #if(PAD_R_ODD7_IS_GPIO != GPIO_NONE)
    #define PAD_R_ODD7_OEN (PAD_R_ODD7_IS_GPIO == GPIO_IN ? BIT4: 0)
    #define PAD_R_ODD7_OUT (PAD_R_ODD7_IS_GPIO == GPIO_OUT_HIGH ? BIT4: 0)
    _RVM1(0x329e, PAD_R_ODD7_OUT, BIT4),
    _RVM1(0x32a2, PAD_R_ODD7_OEN, BIT4),
    //reg_test_in_mode[2:0]
    _RVM1(0x1e24, 0, BIT2 | BIT1 | BIT0 ),   //reg[101e24]#2~#0 = 000b
    //reg_test_out_mode[2:0]
    _RVM1(0x1e24, 0, BIT6 | BIT5 | BIT4 ),   //reg[101e24]#6~#4 = 000b
    _RVM1(0x328c, BIT4 , BIT4 ),   //reg[10328c] #4 = 1
    _RVM1(0x329a, BIT4 , BIT4 ),   //reg[10329a] #4 = 1
    _RVM1(0x32da, 0, BIT5 | BIT4 ),   //reg[1032da] #5~#4 = 00
    #endif

    #if(PAD_LCK_IS_GPIO != GPIO_NONE)
    #define PAD_LCK_OEN (PAD_LCK_IS_GPIO == GPIO_IN ? BIT3: 0)
    #define PAD_LCK_OUT (PAD_LCK_IS_GPIO == GPIO_OUT_HIGH ? BIT3: 0)
    _RVM1(0x329e, PAD_LCK_OUT, BIT3),
    _RVM1(0x32a2, PAD_LCK_OEN, BIT3),
    _RVM1(0x328c, BIT3 , BIT3 ),   //reg[10328c] #3 = 1
    _RVM1(0x329a, BIT3 , BIT3 ),   //reg[10329a] #3 = 1
    _RVM1(0x32da, 0, BIT3 | BIT2 ),   //reg[1032da] #3~#2 = 00
    #endif

    #if(PAD_LDE_IS_GPIO != GPIO_NONE)
    #define PAD_LDE_OEN (PAD_LDE_IS_GPIO == GPIO_IN ? BIT2: 0)
    #define PAD_LDE_OUT (PAD_LDE_IS_GPIO == GPIO_OUT_HIGH ? BIT2: 0)
    _RVM1(0x329e, PAD_LDE_OUT, BIT2),
    _RVM1(0x32a2, PAD_LDE_OEN, BIT2),
    _RVM1(0x328c, BIT2 , BIT2 ),   //reg[10328c] #2 = 1
    _RVM1(0x329a, BIT2 , BIT2 ),   //reg[10329a] #2 = 1
    _RVM1(0x32da, 0, BIT3 | BIT2 ),   //reg[1032da] #3~#2 = 00
    #endif

    #if(PAD_LHSYNC_IS_GPIO != GPIO_NONE)
    #define PAD_LHSYNC_OEN (PAD_LHSYNC_IS_GPIO == GPIO_IN ? BIT1: 0)
    #define PAD_LHSYNC_OUT (PAD_LHSYNC_IS_GPIO == GPIO_OUT_HIGH ? BIT1: 0)
    _RVM1(0x329e, PAD_LHSYNC_OUT, BIT1),
    _RVM1(0x32a2, PAD_LHSYNC_OEN, BIT1),
    _RVM1(0x328c, BIT1 , BIT1 ),   //reg[10328c] #1 = 1
    _RVM1(0x329a, BIT1 , BIT1 ),   //reg[10329a] #1 = 1
    _RVM1(0x32da, 0, BIT1 | BIT0 ),   //reg[1032da] #1~#0 = 00
    #endif

    #if(PAD_LVSYNC_IS_GPIO != GPIO_NONE)
    #define PAD_LVSYNC_OEN (PAD_LVSYNC_IS_GPIO == GPIO_IN ? BIT0: 0)
    #define PAD_LVSYNC_OUT (PAD_LVSYNC_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
    _RVM1(0x329e, PAD_LVSYNC_OUT, BIT0),
    _RVM1(0x32a2, PAD_LVSYNC_OEN, BIT0),
    _RVM1(0x328c, BIT0 , BIT0 ),   //reg[10328c] #0 = 1
    _RVM1(0x329a, BIT0 , BIT0 ),   //reg[10329a] #0 = 1
    _RVM1(0x32da, 0, BIT1 | BIT0 ),   //reg[1032da] #1~#0 = 00
    #endif

    #if(PAD_PCM_WE_N_IS_GPIO != GPIO_NONE)
    #define PAD_PCM_WE_N_OEN (PAD_PCM_WE_N_IS_GPIO == GPIO_IN ? BIT7: 0)
    #define PAD_PCM_WE_N_OUT (PAD_PCM_WE_N_IS_GPIO == GPIO_OUT_HIGH ? BIT7: 0)
    _RVM1(0x1e77, PAD_PCM_WE_N_OUT, BIT7),
    _RVM1(0x1e7d, PAD_PCM_WE_N_OEN, BIT7),
    //reg_test_in_mode[2:0]
    _RVM1(0x1E24, 0, BIT2 | BIT1 | BIT0 ),   //reg[101E24]#2  ~ #0 = 000b
    //reg_sm_config[1:0]
    _RVM1(0x1EDC, 0, BIT5 | BIT4 ),   //reg[101EDC]#5  ~ #4 = 00b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    //reg_test_out_mode[2:0]
    _RVM1(0x1E24, 0, BIT6 | BIT5 | BIT4 ),   //reg[101E24]#6  ~ #4 = 000b
    //reg_pcmctrlconfig
    _RVM1(0x1E9F, 0, BIT7 ),   //reg[101E9F]#7 = 0b
    #endif

    #if(PAD_SPI1_CK_IS_GPIO != GPIO_NONE)
    #define PAD_SPI1_CK_OEN (PAD_SPI1_CK_IS_GPIO == GPIO_IN ? BIT0: 0)
    #define PAD_SPI1_CK_OUT (PAD_SPI1_CK_IS_GPIO == GPIO_OUT_HIGH ? BIT4: 0)
    _RVM1(0x1e90, PAD_SPI1_CK_OUT, BIT4),
    _RVM1(0x1e90, PAD_SPI1_CK_OEN, BIT0),
    //reg_pwm5_mode[1:0]
    _RVM1(0x1E3A, 0, BIT1 | BIT0 ),   //reg[101E3A]#1  ~ #0 = 00b
    //reg_ld_spi1_config
    _RVM1(0x1EB4, 0, BIT0  ),   //reg[101EB4]#0  = 0b
    //reg_ld_spi3_config[1:0]
    _RVM1(0x1EB7, 0, BIT1 | BIT0 ),   //reg[101EB7]#1 ~ #0 = 00b
    //reg_sd_config[1:0]
    _RVM1(0x1EAF, 0, BIT1 | BIT0 ),   //reg[101EAF]#1 ~ #0 = 00b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    #endif

    #if(PAD_SPI1_DI_IS_GPIO != GPIO_NONE)
    #define PAD_SPI1_DI_OEN (PAD_SPI1_DI_IS_GPIO == GPIO_IN ? BIT1: 0)
    #define PAD_SPI1_DI_OUT (PAD_SPI1_DI_IS_GPIO == GPIO_OUT_HIGH ? BIT5: 0)
    _RVM1(0x1e90, PAD_SPI1_DI_OUT, BIT5),
    _RVM1(0x1e90, PAD_SPI1_DI_OEN, BIT1),
    //reg_ld_spi1_config
    _RVM1(0x1EB4, 0, BIT0  ),   //reg[101EB4]#0  = 0b
    //reg_ld_spi3_config[1:0]
    _RVM1(0x1EB7, 0, BIT1 | BIT0 ),   //reg[101EB7]#1 ~ #0 = 00b
    //reg_sd_config[1:0]
    _RVM1(0x1EAF, 0, BIT1 | BIT0 ),   //reg[101EAF]#1 ~ #0 = 00b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    #endif

    #if(PAD_SPI2_CK_IS_GPIO != GPIO_NONE)
    #define PAD_SPI2_CK_OEN (PAD_SPI2_CK_IS_GPIO == GPIO_IN ? BIT2: 0)
    #define PAD_SPI2_CK_OUT (PAD_SPI2_CK_IS_GPIO == GPIO_OUT_HIGH ? BIT6: 0)
    _RVM1(0x1e90, PAD_SPI2_CK_OUT, BIT6),
    _RVM1(0x1e90, PAD_SPI2_CK_OEN, BIT2),
    //reg_pwm5_mode[1:0]
    _RVM1(0x1E3A, 0, BIT1 |	BIT0 ),   //reg[101E3A]#1  ~ #0 = 00b
    //reg_ld_spi3_config[1:0]
    _RVM1(0x1EB7, 0, BIT1 | BIT0 ),   //reg[101EB7]#1 ~ #0 = 00b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    //reg_ld_spi2_config
    _RVM1(0x1EB4, 0, BIT1  ),   //reg[101EB4]#1  = 0b
    #endif

    #if(PAD_SPI2_DI_IS_GPIO != GPIO_NONE)
    #define PAD_SPI2_DI_OEN (PAD_SPI2_DI_IS_GPIO == GPIO_IN ? BIT3: 0)
    #define PAD_SPI2_DI_OUT (PAD_SPI2_DI_IS_GPIO == GPIO_OUT_HIGH ? BIT7: 0)
    _RVM1(0x1e90, PAD_SPI2_DI_OUT, BIT7),
    _RVM1(0x1e90, PAD_SPI2_DI_OEN, BIT3),
    //reg_ld_spi3_config[1:0]
    _RVM1(0x1EB7, 0, BIT1 | BIT0 ),   //reg[101EB7]#1 ~ #0 = 00b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    //reg_ld_spi2_config
    _RVM1(0x1EB4, 0, BIT1  ),   //reg[101EB4]#1  = 0b
    #endif

    #if(PAD_TCON0_IS_GPIO != GPIO_NONE)
    #define PAD_TCON0_OEN (PAD_TCON0_IS_GPIO == GPIO_IN ? BIT0: 0)
    #define PAD_TCON0_OUT (PAD_TCON0_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
    _MEMMAP_nonPM_SPARE0_,
    _RVM1(0x1008, PAD_TCON0_OUT, BIT0),
    _RVM1(0x100A, PAD_TCON0_OEN, BIT0),
    _MEMMAP_nonPM_,
    //reg_sixthuartmode
    _RVM1(0x1EC9, 0, BIT0 ),   //reg[101EC9]#0 = 0b
    //reg_od6thuart
    _RVM1(0x1EC8, 0, BIT5  ),   //reg[101EC8]#5  = 0b
    //reg_tconconfig0[1:0]
    _RVM1(0x1EB9, 0, BIT1 | BIT0 ),   //reg[101EB9]#1 ~ #0 = 00b
    //reg_miic_mode3
    _RVM1(0x1EBA, 0, BIT7  ),   //reg[101EBA]#7  = 0b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    #endif

    #if(PAD_TCON1_IS_GPIO != GPIO_NONE)
    #define PAD_TCON1_OEN (PAD_TCON1_IS_GPIO == GPIO_IN ? BIT1: 0)
    #define PAD_TCON1_OUT (PAD_TCON1_IS_GPIO == GPIO_OUT_HIGH ? BIT1: 0)
    _MEMMAP_nonPM_SPARE0_,
    _RVM1(0x1008, PAD_TCON1_OUT, BIT1),
    _RVM1(0x100A, PAD_TCON1_OEN, BIT1),
    _MEMMAP_nonPM_,
    //reg_sixthuartmode
    _RVM1(0x1EC9, 0, BIT0 ),   //reg[101EC9]#0 = 0b
    //reg_od6thuart
    _RVM1(0x1EC8, 0, BIT5  ),   //reg[101EC8]#5  = 0b
    //reg_tconconfig1[1:0]
    _RVM1(0x1EB9, 0, BIT3 | BIT2 ),   //reg[101EB9]#3 ~ #2 = 00b
    //reg_miic_mode3
    _RVM1(0x1EBA, 0, BIT7  ),   //reg[101EBA]#7  = 0b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    #endif

    #if(PAD_TCON2_IS_GPIO != GPIO_NONE)
    #define PAD_TCON2_OEN (PAD_TCON2_IS_GPIO == GPIO_IN ? BIT2: 0)
    #define PAD_TCON2_OUT (PAD_TCON2_IS_GPIO == GPIO_OUT_HIGH ? BIT2: 0)
    _MEMMAP_nonPM_SPARE0_,
    _RVM1(0x1008, PAD_TCON2_OUT, BIT2),
    _RVM1(0x100A, PAD_TCON2_OEN, BIT2),
    _MEMMAP_nonPM_,
    //reg_tconconfig2[1:0]
    _RVM1(0x1EB9, 0, BIT5 | BIT4 ),   //reg[101EB9]#5 ~ #4 = 00b
    //reg_od7thuart
    _RVM1(0x1EC8, 0, BIT6  ),   //reg[101EC8]#6  = 0b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    //reg_seventhuartmode
    _RVM1(0x1EC8, 0, BIT7  ),   //reg[101EC8]#7  = 0b
    #endif

    #if(PAD_TCON3_IS_GPIO != GPIO_NONE)
    #define PAD_TCON3_OEN (PAD_TCON3_IS_GPIO == GPIO_IN ? BIT3: 0)
    #define PAD_TCON3_OUT (PAD_TCON3_IS_GPIO == GPIO_OUT_HIGH ? BIT3: 0)
    _MEMMAP_nonPM_SPARE0_,
    _RVM1(0x1008, PAD_TCON3_OUT, BIT3),
    _RVM1(0x100A, PAD_TCON3_OEN, BIT3),
    _MEMMAP_nonPM_,
    //reg_tconconfig3[1:0]
    _RVM1(0x1EB9, 0, BIT7 | BIT6 ),   //reg[101EB9]#7 ~ #6 = 00b
    //reg_od7thuart
    _RVM1(0x1EC8, 0, BIT6  ),   //reg[101EC8]#6  = 0b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    //reg_seventhuartmode
    _RVM1(0x1EC8, 0, BIT7  ),   //reg[101EC8]#7  = 0b
    #endif

    #if(PAD_TCON4_IS_GPIO != GPIO_NONE)
    #define PAD_TCON4_OEN (PAD_TCON4_IS_GPIO == GPIO_IN ? BIT4: 0)
    #define PAD_TCON4_OUT (PAD_TCON4_IS_GPIO == GPIO_OUT_HIGH ? BIT4: 0)
    _MEMMAP_nonPM_SPARE0_,
    _RVM1(0x1008, PAD_TCON4_OUT, BIT4),
    _RVM1(0x100A, PAD_TCON4_OEN, BIT4),
    _MEMMAP_nonPM_,
    //reg_miic_mode4
    _RVM1(0x1EC8, 0, BIT2  ),   //reg[101EC8]#2  = 0b
    //reg_tconconfig4[1:0]
    _RVM1(0x1EBA, 0, BIT1 | BIT0 ),   //reg[101EBA]#1  ~ #0 = 00b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    #endif

    #if(PAD_TCON5_IS_GPIO != GPIO_NONE)
    #define PAD_TCON5_OEN (PAD_TCON5_IS_GPIO == GPIO_IN ? BIT5: 0)
    #define PAD_TCON5_OUT (PAD_TCON5_IS_GPIO == GPIO_OUT_HIGH ? BIT5: 0)
    _MEMMAP_nonPM_SPARE0_,
    _RVM1(0x1008, PAD_TCON5_OUT, BIT5),
    _RVM1(0x100A, PAD_TCON5_OEN, BIT5),
    _MEMMAP_nonPM_,
    //reg_miic_mode4
    _RVM1(0x1EC8, 0, BIT2  ),   //reg[101EC8]#2  = 0b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    //reg_tconconfig5[1:0]
    _RVM1(0x1EBA, 0, BIT3 | BIT2 ),   //reg[101EBA]#3  ~ #2 = 00b
    #endif

    #if(PAD_TCON6_IS_GPIO != GPIO_NONE)
    #define PAD_TCON6_OEN (PAD_TCON6_IS_GPIO == GPIO_IN ? BIT6: 0)
    #define PAD_TCON6_OUT (PAD_TCON6_IS_GPIO == GPIO_OUT_HIGH ? BIT6: 0)
    _MEMMAP_nonPM_SPARE0_,
    _RVM1(0x1008, PAD_TCON6_OUT, BIT6),
    _RVM1(0x100A, PAD_TCON6_OEN, BIT6),
    _MEMMAP_nonPM_,
    //reg_tconconfig6[1:0]
    _RVM1(0x1EBA, 0, BIT5 | BIT4 ),   //reg[101EBA]#5  ~ #4 = 00b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    #endif

    #if(PAD_TCON7_IS_GPIO != GPIO_NONE)
    #define PAD_TCON7_OEN (PAD_TCON7_IS_GPIO == GPIO_IN ? BIT7: 0)
    #define PAD_TCON7_OUT (PAD_TCON7_IS_GPIO == GPIO_OUT_HIGH ? BIT7: 0)
    _MEMMAP_nonPM_SPARE0_,
    _RVM1(0x1008, PAD_TCON7_OUT, BIT7),
    _RVM1(0x100A, PAD_TCON7_OEN, BIT7),
    _MEMMAP_nonPM_,
    //reg_tconconfig7
    _RVM1(0x1EBA, 0, BIT6  ),   //reg[101EBA]#6  = 0b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    #endif

    #if(PAD_TS2_D0_IS_GPIO != GPIO_NONE)
    #define PAD_TS2_D0_OEN (PAD_TS2_D0_IS_GPIO == GPIO_IN ? BIT0: 0)
    #define PAD_TS2_D0_OUT (PAD_TS2_D0_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
    _MEMMAP_nonPM_SPARE0_,
    _RVM1(0x1014, PAD_TS2_D0_OUT, BIT0),
    _RVM1(0x1016, PAD_TS2_D0_OEN, BIT0),
    _MEMMAP_nonPM_,
    //reg_ts2config[2:0]
    _RVM1(0x1EA8, 0, BIT6 | BIT5 | BIT4 ),   //reg[101EA8]#6  ~ #4 = 000b
    //reg_ts1config[2:0]
    _RVM1(0x1EA3, 0, BIT5 | BIT4 | BIT3 ),   //reg[101EA3]#5 ~ #3 = 000b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    //reg_w_debug_out
    _RVM1(0x1EF0, 0, BIT2  ),   //reg[101EF0]#2  = 0b
    #endif

    #if(PAD_TS2_D1_IS_GPIO != GPIO_NONE)
    #define PAD_TS2_D1_OEN (PAD_TS2_D1_IS_GPIO == GPIO_IN ? BIT1: 0)
    #define PAD_TS2_D1_OUT (PAD_TS2_D1_IS_GPIO == GPIO_OUT_HIGH ? BIT1: 0)
    _MEMMAP_nonPM_SPARE0_,
    _RVM1(0x1014, PAD_TS2_D1_OUT, BIT1),
    _RVM1(0x1016, PAD_TS2_D1_OEN, BIT1),
    _MEMMAP_nonPM_,
    //reg_ts2config[2:0]
    _RVM1(0x1EA8, 0, BIT6 | BIT5 | BIT4 ),   //reg[101EA8]#6  ~ #4 = 000b
    //reg_ts1config[2:0]
    _RVM1(0x1EA3, 0, BIT5 | BIT4 | BIT3 ),   //reg[101EA3]#5 ~ #3 = 000b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    //reg_w_debug_out
    _RVM1(0x1EF0, 0, BIT2  ),   //reg[101EF0]#2  = 0b
    #endif

    #if(PAD_TS2_D2_IS_GPIO != GPIO_NONE)
    #define PAD_TS2_D2_OEN (PAD_TS2_D2_IS_GPIO == GPIO_IN ? BIT2: 0)
    #define PAD_TS2_D2_OUT (PAD_TS2_D2_IS_GPIO == GPIO_OUT_HIGH ? BIT2: 0)
    _MEMMAP_nonPM_SPARE0_,
    _RVM1(0x1014, PAD_TS2_D2_OUT, BIT2),
    _RVM1(0x1016, PAD_TS2_D2_OEN, BIT2),
    _MEMMAP_nonPM_,
    //reg_ts2config[2:0]
    _RVM1(0x1EA8, 0, BIT6 | BIT5 | BIT4 ),   //reg[101EA8]#6  ~ #4 = 000b
    //reg_ts1config[2:0]
    _RVM1(0x1EA3, 0, BIT5 | BIT4 | BIT3 ),   //reg[101EA3]#5 ~ #3 = 000b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    //reg_w_debug_out
    _RVM1(0x1EF0, 0, BIT2  ),   //reg[101EF0]#2  = 0b
    #endif

    #if(PAD_TS2_D3_IS_GPIO != GPIO_NONE)
    #define PAD_TS2_D3_OEN (PAD_TS2_D3_IS_GPIO == GPIO_IN ? BIT3: 0)
    #define PAD_TS2_D3_OUT (PAD_TS2_D3_IS_GPIO == GPIO_OUT_HIGH ? BIT3: 0)
    _MEMMAP_nonPM_SPARE0_,
    _RVM1(0x1014, PAD_TS2_D3_OUT, BIT3),
    _RVM1(0x1016, PAD_TS2_D3_OEN, BIT3),
    _MEMMAP_nonPM_,
    //reg_ts2config[2:0]
    _RVM1(0x1EA8, 0, BIT6 | BIT5 | BIT4 ),   //reg[101EA8]#6  ~ #4 = 000b
    //reg_ts1config[2:0]
    _RVM1(0x1EA3, 0, BIT5 | BIT4 | BIT3 ),   //reg[101EA3]#5 ~ #3 = 000b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    //reg_w_debug_out
    _RVM1(0x1EF0, 0, BIT2  ),   //reg[101EF0]#2  = 0b
    #endif

    #if(PAD_TS2_D4_IS_GPIO != GPIO_NONE)
    #define PAD_TS2_D4_OEN (PAD_TS2_D4_IS_GPIO == GPIO_IN ? BIT4: 0)
    #define PAD_TS2_D4_OUT (PAD_TS2_D4_IS_GPIO == GPIO_OUT_HIGH ? BIT4: 0)
    _MEMMAP_nonPM_SPARE0_,
    _RVM1(0x1014, PAD_TS2_D4_OUT, BIT4),
    _RVM1(0x1016, PAD_TS2_D4_OEN, BIT4),
    _MEMMAP_nonPM_,
    //reg_ts2config[2:0]
    _RVM1(0x1EA8, 0, BIT6 | BIT5 | BIT4 ),   //reg[101EA8]#6  ~ #4 = 000b
    //reg_ts1config[2:0]
    _RVM1(0x1EA3, 0, BIT5 | BIT4 | BIT3 ),   //reg[101EA3]#5 ~ #3 = 000b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    //reg_w_debug_out
    _RVM1(0x1EF0, 0, BIT2  ),   //reg[101EF0]#2  = 0b
    #endif

    #if(PAD_TS2_D5_IS_GPIO != GPIO_NONE)
    #define PAD_TS2_D5_OEN (PAD_TS2_D5_IS_GPIO == GPIO_IN ? BIT5: 0)
    #define PAD_TS2_D5_OUT (PAD_TS2_D5_IS_GPIO == GPIO_OUT_HIGH ? BIT5: 0)
    _MEMMAP_nonPM_SPARE0_,
    _RVM1(0x1014, PAD_TS2_D5_OUT, BIT5),
    _RVM1(0x1016, PAD_TS2_D5_OEN, BIT5),
    _MEMMAP_nonPM_,
    //reg_ts2config[2:0]
    _RVM1(0x1EA8, 0, BIT6 | BIT5 | BIT4 ),   //reg[101EA8]#6  ~ #4 = 000b
    //reg_ts1config[2:0]
    _RVM1(0x1EA3, 0, BIT5 | BIT4 | BIT3 ),   //reg[101EA3]#5 ~ #3 = 000b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    //reg_w_debug_out
    _RVM1(0x1EF0, 0, BIT2  ),   //reg[101EF0]#2  = 0b
    #endif

    #if(PAD_TS2_D6_IS_GPIO != GPIO_NONE)
    #define PAD_TS2_D6_OEN (PAD_TS2_D6_IS_GPIO == GPIO_IN ? BIT6: 0)
    #define PAD_TS2_D6_OUT (PAD_TS2_D6_IS_GPIO == GPIO_OUT_HIGH ? BIT6: 0)
    _MEMMAP_nonPM_SPARE0_,
    _RVM1(0x1014, PAD_TS2_D6_OUT, BIT6),
    _RVM1(0x1016, PAD_TS2_D6_OEN, BIT6),
    _MEMMAP_nonPM_,
    //reg_ts2config[2:0]
    _RVM1(0x1EA8, 0, BIT6 | BIT5 | BIT4 ),   //reg[101EA8]#6  ~ #4 = 000b
    //reg_ts1config[2:0]
    _RVM1(0x1EA3, 0, BIT5 | BIT4 | BIT3 ),   //reg[101EA3]#5 ~ #3 = 000b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    //reg_w_debug_out
    _RVM1(0x1EF0, 0, BIT2  ),   //reg[101EF0]#2  = 0b
    #endif

    #if(PAD_TS2_D7_IS_GPIO != GPIO_NONE)
    #define PAD_TS2_D7_OEN (PAD_TS2_D7_IS_GPIO == GPIO_IN ? BIT7: 0)
    #define PAD_TS2_D7_OUT (PAD_TS2_D7_IS_GPIO == GPIO_OUT_HIGH ? BIT7: 0)
    _MEMMAP_nonPM_SPARE0_,
    _RVM1(0x1014, PAD_TS2_D7_OUT, BIT7),
    _RVM1(0x1016, PAD_TS2_D7_OEN, BIT7),
    _MEMMAP_nonPM_,
    //reg_ts2config[2:0]
    _RVM1(0x1EA8, 0, BIT6 | BIT5 | BIT4 ),   //reg[101EA8]#6  ~ #4 = 000b
    //reg_ts1config[2:0]
    _RVM1(0x1EA3, 0, BIT5 | BIT4 | BIT3 ),   //reg[101EA3]#5 ~ #3 = 000b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    #endif

    #if(PAD_TS2_CLK_IS_GPIO != GPIO_NONE)
    #define PAD_TS2_CLK_OEN (PAD_TS2_CLK_IS_GPIO == GPIO_IN ? BIT0: 0)
    #define PAD_TS2_CLK_OUT (PAD_TS2_CLK_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
    _MEMMAP_nonPM_SPARE0_,
    _RVM1(0x1015, PAD_TS2_CLK_OUT, BIT0),
    _RVM1(0x1017, PAD_TS2_CLK_OEN, BIT0),
    _MEMMAP_nonPM_,
    //reg_ts2config[2:0]
    _RVM1(0x1EA8, 0, BIT6 | BIT5 | BIT4 ),   //reg[101EA8]#6  ~ #4 = 000b
    //reg_ts1config[2:0]
    _RVM1(0x1EA3, 0, BIT5 | BIT4 | BIT3 ),   //reg[101EA3]#5 ~ #3 = 000b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    #endif

    #if(PAD_TS2_SYNC_IS_GPIO != GPIO_NONE)
    #define PAD_TS2_SYNC_OEN (PAD_TS2_SYNC_IS_GPIO == GPIO_IN ? BIT1: 0)
    #define PAD_TS2_SYNC_OUT (PAD_TS2_SYNC_IS_GPIO == GPIO_OUT_HIGH ? BIT1: 0)
    _MEMMAP_nonPM_SPARE0_,
    _RVM1(0x1015, PAD_TS2_SYNC_OUT, BIT1),
    _RVM1(0x1017, PAD_TS2_SYNC_OEN, BIT1),
    _MEMMAP_nonPM_,
    //reg_ts2config[2:0]
    _RVM1(0x1EA8, 0, BIT6 | BIT5 | BIT4 ),   //reg[101EA8]#6  ~ #4 = 000b
    //reg_ts1config[2:0]
    _RVM1(0x1EA3, 0, BIT5 | BIT4 | BIT3 ),   //reg[101EA3]#5 ~ #3 = 000b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    //reg_w_debug_out
    _RVM1(0x1EF0, 0, BIT2  ),   //reg[101EF0]#2  = 0b
    #endif

    #if(PAD_TS2_VLD_IS_GPIO != GPIO_NONE)
    #define PAD_TS2_VLD_OEN (PAD_TS2_VLD_IS_GPIO == GPIO_IN ? BIT2: 0)
    #define PAD_TS2_VLD_OUT (PAD_TS2_VLD_IS_GPIO == GPIO_OUT_HIGH ? BIT2: 0)
    _MEMMAP_nonPM_SPARE0_,
    _RVM1(0x1015, PAD_TS2_VLD_OUT, BIT2),
    _RVM1(0x1017, PAD_TS2_VLD_OEN, BIT2),
    _MEMMAP_nonPM_,
    //reg_ts2config[2:0]
    _RVM1(0x1EA8, 0, BIT6 | BIT5 | BIT4 ),   //reg[101EA8]#6  ~ #4 = 000b
    //reg_ts1config[2:0]
    _RVM1(0x1EA3, 0, BIT5 | BIT4 | BIT3 ),   //reg[101EA3]#5 ~ #3 = 000b
    //reg_allpad_in
    _RVM1(0x1EA1, 0, BIT7 ),   //reg[101EA1]#7 = 0b
    //reg_w_debug_out
    _RVM1(0x1EF0, 0, BIT2  ),   //reg[101EF0]#2  = 0b
    #endif

    _MEMMAP_nonPM_LINEIN_,

    #if(PAD_LINEIN_L1_IS_GPIO != GPIO_NONE)
    #define PAD_LINEIN_L1_OEN (PAD_LINEIN_L1_IS_GPIO == GPIO_IN ? BIT2: 0)
    #define PAD_LINEIN_L1_OUT (PAD_LINEIN_L1_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
    _RVM1(0x2CF7, PAD_LINEIN_L1_OUT, BIT0),
    _RVM1(0x2CF7, PAD_LINEIN_L1_OEN, BIT2),
    _RVM1(0x2CF7, BIT1 , BIT1 ),   //reg[112CF7] #1 = 1b
    #endif

    #if(PAD_LINEIN_R1_IS_GPIO != GPIO_NONE)
    #define PAD_LINEIN_R1_OEN (PAD_LINEIN_R1_IS_GPIO == GPIO_IN ? BIT6: 0)
    #define PAD_LINEIN_R1_OUT (PAD_LINEIN_R1_IS_GPIO == GPIO_OUT_HIGH ? BIT4: 0)
    _RVM1(0x2CF7, PAD_LINEIN_R1_OUT, BIT4),
    _RVM1(0x2CF7, PAD_LINEIN_R1_OEN, BIT6),
    _RVM1(0x2CF7, BIT5 , BIT5 ),   //reg[112CF7] #5 = 1b
    #endif

    #if(PAD_LINEIN_L2_IS_GPIO != GPIO_NONE)
    #define PAD_LINEIN_L2_OEN (PAD_LINEIN_L2_IS_GPIO == GPIO_IN ? BIT2: 0)
    #define PAD_LINEIN_L2_OUT (PAD_LINEIN_L2_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
    _RVM1(0x2CF8, PAD_LINEIN_L2_OUT, BIT0),
    _RVM1(0x2CF8, PAD_LINEIN_L2_OEN, BIT2),
    _RVM1(0x2CF8, BIT1 , BIT1 ),   //reg[112CF8] #1 = 1b
    #endif

    #if(PAD_LINEIN_R2_IS_GPIO != GPIO_NONE)
    #define PAD_LINEIN_R2_OEN (PAD_LINEIN_R2_IS_GPIO == GPIO_IN ? BIT6: 0)
    #define PAD_LINEIN_R2_OUT (PAD_LINEIN_R2_IS_GPIO == GPIO_OUT_HIGH ? BIT4: 0)
    _RVM1(0x2CF8, PAD_LINEIN_R2_OUT, BIT4),
    _RVM1(0x2CF8, PAD_LINEIN_R2_OEN, BIT6),
    _RVM1(0x2CF8, BIT5 , BIT5 ),   //reg[112CF8] #5 = 1b
    #endif

    #if(PAD_LINEIN_L3_IS_GPIO != GPIO_NONE)
    #define PAD_LINEIN_L3_OEN (PAD_LINEIN_L3_IS_GPIO == GPIO_IN ? BIT2: 0)
    #define PAD_LINEIN_L3_OUT (PAD_LINEIN_L3_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
    _RVM1(0x2CF9, PAD_LINEIN_L3_OUT, BIT0),
    _RVM1(0x2CF9, PAD_LINEIN_L3_OEN, BIT2),
    _RVM1(0x2CF9, BIT1 , BIT1 ),   //reg[112CF9] #1 = 1b
    #endif

    #if(PAD_LINEIN_R3_IS_GPIO != GPIO_NONE)
    #define PAD_LINEIN_R3_OEN (PAD_LINEIN_R3_IS_GPIO == GPIO_IN ? BIT6: 0)
    #define PAD_LINEIN_R3_OUT (PAD_LINEIN_R3_IS_GPIO == GPIO_OUT_HIGH ? BIT4: 0)
    _RVM1(0x2CF9, PAD_LINEIN_R3_OUT, BIT4),
    _RVM1(0x2CF9, PAD_LINEIN_R3_OEN, BIT6),
    _RVM1(0x2CF9, BIT5 , BIT5 ),   //reg[112CF9] #5 = 1b
    #endif

    #if(PAD_LINEIN_L4_IS_GPIO != GPIO_NONE)
    #define PAD_LINEIN_L4_OEN (PAD_LINEIN_L4_IS_GPIO == GPIO_IN ? BIT2: 0)
    #define PAD_LINEIN_L4_OUT (PAD_LINEIN_L4_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
    _RVM1(0x2CFA, PAD_LINEIN_L4_OUT, BIT0),
    _RVM1(0x2CFA, PAD_LINEIN_L4_OEN, BIT2),
    _RVM1(0x2CFA, BIT1 , BIT1 ),   //reg[112CFA] #1 = 1b
    #endif

    #if(PAD_LINEIN_R4_IS_GPIO != GPIO_NONE)
    #define PAD_LINEIN_R4_OEN (PAD_LINEIN_R4_IS_GPIO == GPIO_IN ? BIT6: 0)
    #define PAD_LINEIN_R4_OUT (PAD_LINEIN_R4_IS_GPIO == GPIO_OUT_HIGH ? BIT4: 0)
    _RVM1(0x2CFA, PAD_LINEIN_R4_OUT, BIT4),
    _RVM1(0x2CFA, PAD_LINEIN_R4_OEN, BIT6),
    _RVM1(0x2CFA, BIT5 , BIT5 ),   //reg[112CFA] #5 = 1b
    #endif

    #if(PAD_LINEIN_L5_IS_GPIO != GPIO_NONE)
    #define PAD_LINEIN_L5_OEN (PAD_LINEIN_L5_IS_GPIO == GPIO_IN ? BIT2: 0)
    #define PAD_LINEIN_L5_OUT (PAD_LINEIN_L5_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
    _RVM1(0x2CFB, PAD_LINEIN_L5_OUT, BIT0),
    _RVM1(0x2CFB, PAD_LINEIN_L5_OEN, BIT2),
    _RVM1(0x2CFB, BIT1 , BIT1 ),   //reg[112CFB] #1 = 1b
    #endif

    #if(PAD_LINEIN_R5_IS_GPIO != GPIO_NONE)
    #define PAD_LINEIN_R5_OEN (PAD_LINEIN_R5_IS_GPIO == GPIO_IN ? BIT6: 0)
    #define PAD_LINEIN_R5_OUT (PAD_LINEIN_R5_IS_GPIO == GPIO_OUT_HIGH ? BIT4: 0)
    _RVM1(0x2CFB, PAD_LINEIN_R5_OUT, BIT4),
    _RVM1(0x2CFB, PAD_LINEIN_R5_OEN, BIT6),
    _RVM1(0x2CFB, BIT5 , BIT5 ),   //reg[112CFB] #5 = 1b
    #endif
    _MEMMAP_nonPM_,
    #if(PADA_HSYNC0_IS_GPIO != GPIO_NONE)
    #define PADA_HSYNC0_OEN (PADA_HSYNC0_IS_GPIO == GPIO_IN ? BIT0: 0)
    #define PADA_HSYNC0_OUT (PADA_HSYNC0_IS_GPIO == GPIO_OUT_HIGH ? BIT6: 0)
    _RVM1(0x255A, PADA_HSYNC0_OUT, BIT6),
    _RVM1(0x255A, PADA_HSYNC0_OEN, BIT0),
    _RVM1(0x2558, BIT0 , BIT0 ),   //reg[102558] #0 = 1b
    #endif

    #if(PADA_VSYNC0_IS_GPIO != GPIO_NONE)
    #define PADA_VSYNC0_OEN (PADA_VSYNC0_IS_GPIO == GPIO_IN ? BIT1: 0)
    #define PADA_VSYNC0_OUT (PADA_VSYNC0_IS_GPIO == GPIO_OUT_HIGH ? BIT7: 0)
    _RVM1(0x255A, PADA_VSYNC0_OUT, BIT7),
    _RVM1(0x255A, PADA_VSYNC0_OEN, BIT1),
    _RVM1(0x2558, BIT1 , BIT1 ),   //reg[102558] #1 = 1b
    #endif

    #if(PADA_HSYNC1_IS_GPIO != GPIO_NONE)
    #define PADA_HSYNC1_OEN (PADA_HSYNC1_IS_GPIO == GPIO_IN ? BIT2: 0)
    #define PADA_HSYNC1_OUT (PADA_HSYNC1_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
    _RVM1(0x255B, PADA_HSYNC1_OUT, BIT0),
    _RVM1(0x255A, PADA_HSYNC1_OEN, BIT2),
    _RVM1(0x2558, BIT2 , BIT2 ),   //reg[102558] #2 = 1b
    #endif

    #if(PADA_VSYNC1_IS_GPIO != GPIO_NONE)
    #define PADA_VSYNC1_OEN (PADA_VSYNC1_IS_GPIO == GPIO_IN ? BIT3: 0)
    #define PADA_VSYNC1_OUT (PADA_VSYNC1_IS_GPIO == GPIO_OUT_HIGH ? BIT1: 0)
    _RVM1(0x255B, PADA_VSYNC1_OUT, BIT1),
    _RVM1(0x255A, PADA_VSYNC1_OEN, BIT3),
    _RVM1(0x2558, BIT3 , BIT3 ),   //reg[102558] #3 = 1b
    #endif

    #if(PADA_HSYNC2_IS_GPIO != GPIO_NONE)
    #define PADA_HSYNC2_OEN (PADA_HSYNC2_IS_GPIO == GPIO_IN ? BIT4: 0)
    #define PADA_HSYNC2_OUT (PADA_HSYNC2_IS_GPIO == GPIO_OUT_HIGH ? BIT2: 0)
    _RVM1(0x255B, PADA_HSYNC2_OUT, BIT2),
    _RVM1(0x255A, PADA_HSYNC2_OEN, BIT4),
    _RVM1(0x2558, BIT4 , BIT4 ),   //reg[102558] #4 = 1b
    #endif

    #if(PADA_VSYNC2_IS_GPIO != GPIO_NONE)
    #define PADA_VSYNC2_OEN (PADA_VSYNC2_IS_GPIO == GPIO_IN ? BIT5: 0)
    #define PADA_VSYNC2_OUT (PADA_VSYNC2_IS_GPIO == GPIO_OUT_HIGH ? BIT3: 0)
    _RVM1(0x255B, PADA_VSYNC2_OUT, BIT3),
    _RVM1(0x255A, PADA_VSYNC2_OEN, BIT5),
    _RVM1(0x2558, BIT5 , BIT5 ),   //reg[102558] #5 = 1b
    #endif

    #if(PADA_RIN0P_IS_GPIO != GPIO_NONE)
    #define PADA_RIN0P_OEN (PADA_RIN0P_IS_GPIO == GPIO_IN ? BIT0: 0)
    #define PADA_RIN0P_OUT (PADA_RIN0P_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
    _RVM1(0x2537, PADA_RIN0P_OUT, BIT0),
    _RVM1(0x2536, PADA_RIN0P_OEN, BIT0),
    _RVM1(0x2534, BIT0 , BIT0 ),   //reg[102534] #0 = 1b
    #endif

    #if(PADA_RIN1P_IS_GPIO != GPIO_NONE)
    #define PADA_RIN1P_OEN (PADA_RIN1P_IS_GPIO == GPIO_IN ? BIT1: 0)
    #define PADA_RIN1P_OUT (PADA_RIN1P_IS_GPIO == GPIO_OUT_HIGH ? BIT1: 0)
    _RVM1(0x2537, PADA_RIN1P_OUT, BIT1),
    _RVM1(0x2536, PADA_RIN1P_OEN, BIT1),
    _RVM1(0x2534, BIT1 , BIT1 ),   //reg[102534] #1 = 1b
    #endif

    #if(PADA_GIN0P_IS_GPIO != GPIO_NONE)
    #define PADA_GIN0P_OEN (PADA_GIN0P_IS_GPIO == GPIO_IN ? BIT0: 0)
    #define PADA_GIN0P_OUT (PADA_GIN0P_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
    _RVM1(0x2539, PADA_GIN0P_OUT, BIT0),
    _RVM1(0x2538, PADA_GIN0P_OEN, BIT0),
    _RVM1(0x2534, BIT0 , BIT0 ),   //reg[102534] #0 = 1b
    #endif

    #if(PADA_GIN1P_IS_GPIO != GPIO_NONE)
    #define PADA_GIN1P_OEN (PADA_GIN1P_IS_GPIO == GPIO_IN ? BIT1: 0)
    #define PADA_GIN1P_OUT (PADA_GIN1P_IS_GPIO == GPIO_OUT_HIGH ? BIT1: 0)
    _RVM1(0x2539, PADA_GIN1P_OUT, BIT1),
    _RVM1(0x2538, PADA_GIN1P_OEN, BIT1),
    _RVM1(0x2534, BIT1 , BIT1 ),   //reg[102534] #1 = 1b
    #endif

    #if(PADA_BIN0P_IS_GPIO != GPIO_NONE)
    #define PADA_BIN0P_OEN (PADA_BIN0P_IS_GPIO == GPIO_IN ? BIT0: 0)
    #define PADA_BIN0P_OUT (PADA_BIN0P_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
    _RVM1(0x253B, PADA_BIN0P_OUT, BIT0),
    _RVM1(0x253A, PADA_BIN0P_OEN, BIT0),
    _RVM1(0x2534, BIT0 , BIT0 ),   //reg[102534] #0 = 1b
    #endif

    #if(PADA_BIN1P_IS_GPIO != GPIO_NONE)
    #define PADA_BIN1P_OEN (PADA_BIN1P_IS_GPIO == GPIO_IN ? BIT1: 0)
    #define PADA_BIN1P_OUT (PADA_BIN1P_IS_GPIO == GPIO_OUT_HIGH ? BIT1: 0)
    _RVM1(0x253B, PADA_BIN1P_OUT, BIT1),
    _RVM1(0x253A, PADA_BIN1P_OEN, BIT1),
    _RVM1(0x2534, BIT1 , BIT1 ),   //reg[102534] #1 = 1b
    #endif

    #if(PADA_GIN0M_IS_GPIO != GPIO_NONE)
    #define PADA_GIN0M_OEN (PADA_GIN0M_IS_GPIO == GPIO_IN ? BIT4: 0)
    #define PADA_GIN0M_OUT (PADA_GIN0M_IS_GPIO == GPIO_OUT_HIGH ? BIT4: 0)
    _RVM1(0x2539, PADA_GIN0M_OUT, BIT4),
    _RVM1(0x2538, PADA_GIN0M_OEN, BIT4),
    _RVM1(0x2534, BIT0 , BIT0 ),   //reg[102534] #0 = 1b
    #endif

    #if(PADA_GIN1M_IS_GPIO != GPIO_NONE)
    #define PADA_GIN1M_OEN (PADA_GIN1M_IS_GPIO == GPIO_IN ? BIT5: 0)
    #define PADA_GIN1M_OUT (PADA_GIN1M_IS_GPIO == GPIO_OUT_HIGH ? BIT5: 0)
    _RVM1(0x2539, PADA_GIN1M_OUT, BIT5),
    _RVM1(0x2538, PADA_GIN1M_OEN, BIT5),
    _RVM1(0x2534, BIT1 , BIT1 ),   //reg[102534] #1 = 1b
    #endif

    #if(PAD_ARC0_IS_GPIO != GPIO_NONE)
    #define PAD_ARC0_OEN (PAD_ARC0_IS_GPIO == GPIO_IN ? BIT2: 0)
    #define PAD_ARC0_OUT (PAD_ARC0_IS_GPIO == GPIO_OUT_HIGH ? BIT1: 0)
    _RVM1(0x1EFE, PAD_ARC0_OUT, BIT1),
    _RVM1(0x1EFE, PAD_ARC0_OEN, BIT2),
    //reg_arc_gpio_en
    _RVM1(0x1EFE, BIT4 , BIT4 ),   //reg[101EFE] #4 = 1b
    //reg_arc_mode
    _RVM1(0x1EFE, 0, BIT5 ),   //reg[101EFE] #5 = 0b
    #endif

//---------------------------------------------------------------------
// Pad Configuartion
//---------------------------------------------------------------------
    _MEMMAP_nonPM_,

#if (PADS_DISEQCOUT_MODE != Unknow_pad_mux)
#if (PADS_DISEQCOUT_MODE == DISEQCOUT_MODE(1))
            _RVM1(0x1E38, BIT5, BIT5),
#endif
#if (PADS_DISEQCOUT_MODE == DISEQCOUT_MODE(2))
            _RVM1(0x1E25, 0, BIT6),
            _RVM1(0x1E25, BIT7, BIT7),
#endif
#endif

#if (PADS_TS0_CONFIG != Unknown_pad_mux)
#define TS0_CONFIG   ((PADS_TS0_CONFIG == TS0_MODE(PARALLEL_IN)) ? BIT1 : \
                      (PADS_TS0_CONFIG == TS0_MODE(SERIAL_IN)) ? BIT2 : 0)
            _RVM1(0x1EA3, TS0_CONFIG, BIT2|BIT1),
#endif

#if (PADS_TS1_CONFIG != Unknown_pad_mux)
#define TS1_CONFIG   ((PADS_TS1_CONFIG == TS1_MODE(PARALLEL_IN)) ? BIT3 : \
                      (PADS_TS1_CONFIG == TS1_MODE(PARALLEL_OUT)) ? BIT4 : \
                      (PADS_TS1_CONFIG == TS1_MODE(SERIAL_IN)) ? (BIT4|BIT3) :\
                      (PADS_TS1_CONFIG == TS1_MODE(TS1_TSP_MSPI)) ? BIT5 : \
                      (PADS_TS1_CONFIG == TS1_MODE(WDM_SCAN_MODE)) ? (BIT5|BIT3): 0)
            _RVM1(0x1EA3, TS1_CONFIG, BIT5|BIT4|BIT4),
#endif

#if (PADS_TS1_CLK != Unknown_pad_mux)                             // TS1_CLK driving  :8MA
	_RVM1(0x1e12, 1, BIT7|BIT6|BIT5|BIT4|BIT3|BIT2|BIT1|BIT0 ),   //reg[101e12]#7 ~ #0 = 11111111b
    _RVM1(0x1e13, 1, BIT2|BIT1|BIT0 ),                            //reg[101e13]#2 ~ #0 = 111b
    _RVM1(0x1e13, 0, BIT7|BIT6|BIT5|BIT4|BIT3 ),                  //reg[101e13]#7 ~ #3 = 00000b
#endif

#if (PADS_TS_OUT != Unknown_pad_mux)
#define TS_OUT_MODE   ((PADS_TS_OUT == TSD_MODE(ENABLE)) ? BIT0: 0)
            _RVM1(0x1EA3, TS_OUT_MODE, BIT0),
#endif

#if (PADS_TS2_CONFIG != Unknown_pad_mux)
#define TS2_CONFIG   ((PADS_TS2_CONFIG == TS2_MODE(PARALLEL_IN)) ? BIT4 : \
                      (PADS_TS2_CONFIG == TS2_MODE(SERIAL_IN))   ? BIT5 : \
                      (PADS_TS2_CONFIG == TS2_TSP_MSPI) ? (BIT5|BIT4) : 0)
            _RVM1(0x1EA8, TS2_CONFIG, BIT5|BIT4),
#endif

#if (PADS_NAND_MODE != Unknown_pad_mux)
#if (PADS_NAND_MODE == PCM_D) || (PADS_NAND_MODE == NAND_AD)
    _RVM1(0x1EA0, BIT7, BIT7),
#else
    _RVM1(0x1EA0, 0, BIT7),
#endif
#define NAND_MODE   ((PADS_NAND_MODE == PCM_A) ? BIT0: \
                     (PADS_NAND_MODE == NAND_AD) ? BIT0: \
                     (PADS_NAND_MODE == NAND_AD_DQS) ? BIT1 : 0)
    _RVM1(0x1EA1, NAND_MODE, BITMASK(1:0)),
#else
    _RVM1(0x1EA0, 0x00, BIT7),
    _RVM1(0x1EA1, 0x00, BITMASK(1:0)),
#endif

#ifdef PADS_EMMC_MODE
    #if (PADS_EMMC_MODE != Unknown_pad_mux)
    #define EMMC_MODE   ((PADS_EMMC_MODE == PAD_EMMC) ? BIT6 : 0)
        _RVM1(0x1EDC, EMMC_MODE, BIT6),
    #else
        _RVM1(0x1EDC, 0,  BIT6),
    #endif
#endif

#if (PADS_PCM1_CTRL != Unknown_pad_mux)
#define PCM1_CTRL   ((PADS_PCM1_CTRL == PCMCTRL_MODE(1)) ? BIT7 : 0)
        _RVM1(0x1E9F, PCM1_CTRL, BIT7),
#endif

#if (PADS_PCM1_AD != Unknown_pad_mux)
#define PCM1_AD   ((PADS_PCM1_AD == PCMAD_MODE(1)) ? BIT6 : 0)
        _RVM1(0x1E9F, PCM1_AD, BIT6),
#endif

#if (PADS_PCM1_PE != Unknown_pad_mux)
#if (PADS_PCM1_PE == PCM_PE_ON)
        _RVM1(0x1E16, 0xFF, 0xFF),
        _RVM1(0x1E17, 0xFF, 0xFF),
        _RVM1(0x1E18, 0xFF, 0xFF),
        _RVM1(0x1E19, 0xFF, 0xFF),
        _RVM1(0x1E1A, 0x03, 0x03),
#else
        _RVM1(0x1E16, 0x00, 0xFF),
        _RVM1(0x1E17, 0x00, 0xFF),
        _RVM1(0x1E18, 0x00, 0xFF),
        _RVM1(0x1E19, 0x00, 0xFF),
        _RVM1(0x1E1A, 0x00, 0x03),
#endif
#endif

#if (PADS_CI_CTRL != Unknown_pad_mux)
#define CI_CTRL   ((PADS_CI_CTRL == CI_CTRL_MODE(1)) ? BIT1 : 0)
        _RVM1(0x1EC8, CI_CTRL, BIT1),
#endif

#if (PADS_CI_AD != Unknown_pad_mux)
#define CI_AD   ((PADS_CI_AD == CI_AD_MODE(1)) ? BIT0 : 0)
        _RVM1(0x1EC8, CI_AD, BIT0),
#endif

#if (PADS_PCM2_CTRL != Unknown_pad_mux)
#define PCM2_CTRL   ((PADS_PCM2_CTRL == ENABLE) ? BIT5 : 0)
        _RVM1(0x1E9F, PCM2_CTRL, BIT5),
#endif

#if (PADS_SPDIF_OUT != Unknown_pad_mux)
#define SPDIF_OUT   ((PADS_SPDIF_OUT == SPDIF_OUT_MODE(1)) ? BIT7 : 0)
        _RVM1(0x1EAE, SPDIF_OUT, BIT7),
#endif

#if (PADS_SPDIF_IN != Unknown_pad_mux)
#define SPDIF_IN   ((PADS_SPDIF_IN == SPDIF_IN_MODE(1)) ? BIT6 : 0)
        _RVM1(0x1EAE, SPDIF_IN, BIT6),
#endif

#if (PADS_SPDIF_OUT2 != Unknown_pad_mux)
#define SPDIF_OUT2   ((PADS_SPDIF_OUT2 == SPDIF_OUT_MODE(2)) ? BIT4 : 0)
        _RVM1(0x1EAF, SPDIF_OUT2, BIT4),
#endif

#if (PADS_I2S_IN != Unknown_pad_mux)
#define I2S_IN    ((PADS_I2S_IN == I2SIN_MODE(ENABLE)) ? BIT2 : 0)
        _RVM1(0x1EAE, I2S_IN, BIT2),
#endif

#if (PADS_I2S_MUTE != Unknown_pad_mux)
#define I2S_MUTE   ((PADS_I2S_MUTE == I2S_MUTE(1)) ? BIT6 : \
                    (PADS_I2S_MUTE == I2S_MUTE(2)) ? BIT7 : 0)
        _RVM1(0x1E05, I2S_MUTE, BIT7|BIT6),
#endif

#if (PADS_I2S_OUT != Unknown_pad_mux)
#define I2S_OUT   ((PADS_I2S_OUT == I2SOUT_MODE(SINGLE_CH_I2S_OUT)) ? BIT4 : 0)
        _RVM1(0x1EAE, I2S_OUT, BIT4),
#endif

#if (PADS_MPIF_MODE != Unknown_pad_mux)
#define MPIF_CFG   ((PADS_MPIF_MODE == MPIF_MODE(1)) ? BIT6 : \
                    (PADS_MPIF_MODE == MPIF_MODE(2)) ? BIT7 : \
                    (PADS_MPIF_MODE == MPIF_MODE(3)) ? BIT7|BIT6 : 0)
        _RVM1(0x1EB5, MPIF_CFG, BIT7|BIT6),
#endif

#if (PADS_ET_MODE != Unknown_pad_mux)
#define ET_MODE   ((PADS_ET_MODE == ENABLE) ? BIT4 : 0)
        _RVM1(0x1EB4, ET_MODE, BIT4),
#endif

#if (PADS_3D_FLAG_CONFIG != Unknown_pad_mux)
#define FLAG_CONFIG   ((PADS_3D_FLAG_CONFIG == PAD_SPDIF_IN) ? BIT5 : \
                       (PADS_3D_FLAG_CONFIG == PAD_TS1_CLK) ? BIT6 : \
                       (PADS_3D_FLAG_CONFIG == PAD_I2S_IN_SD) ? BIT6|BIT5 :\
                       (PADS_3D_FLAG_CONFIG == PAD_GPIO0) ? BIT7	: 0)
        _RVM1(0x1EB3, FLAG_CONFIG, BIT7|BIT6|BIT5),
#endif

//==========
// UART Modes
//==========
#if (PADS_UART2_MODE != Unknown_pad_mux)
#define UART2_CFG   ((PADS_UART2_MODE == UART2_MODE(1))  ? BIT0: \
                      (PADS_UART2_MODE == UART2_MODE(2))  ? BIT1: \
                      (PADS_UART2_MODE == UART2_MODE(3))  ? BIT1|BIT0: 0 )
        _RVM1(0x1E05, UART2_CFG, BIT1|BIT0),
#endif

#if (PADS_UART3_MODE != Unknown_pad_mux)
#define UART3_CFG   ((PADS_UART3_MODE == UART3_MODE(1)) ? BIT2: \
                      (PADS_UART3_MODE == UART3_MODE(2)) ? BIT3: \
                      (PADS_UART3_MODE == UART3_MODE(3)) ? BIT3|BIT2: 0 )
        _RVM1(0x1E05, UART3_CFG, BIT3|BIT2),
#endif

#if (PADS_UART4_MODE != Unknown_pad_mux)
#define UART4_CFG   ((PADS_UART4_MODE == UART4_MODE(1)) ? BIT6: \
                      (PADS_UART4_MODE == UART4_MODE(2)) ? BIT7: \
                      (PADS_UART4_MODE == UART4_MODE(3)) ? BIT7|BIT6: 0 )
        _RVM1(0x1E04, UART4_CFG, BIT7|BIT6),
#endif

#if (PADS_UART5_MODE != Unknown_pad_mux)
#define UART5_CFG   ((PADS_UART4_MODE == UART5_MODE(1)) ? BIT0: 0 )
        _RVM1(0x1E1E, UART5_CFG, BIT0),
#endif

#if (PADS_UART6_MODE != Unknown_pad_mux)
#define UART6_CFG   ((PADS_UART4_MODE == UART6_MODE(7)) ? BIT7: 0 )
        _RVM1(0x1EC8, UART6_CFG, BIT7),
#endif

#if (PADS_UART7_MODE != Unknown_pad_mux)
#define UART7_CFG   ((PADS_UART4_MODE == UART7_MODE(0)) ? BIT0: 0 )
        _RVM1(0x1EC9, UART7_CFG, BIT0),
#endif


#if (PADS_FAST_UART_MODE != Unknown_pad_mux)
#define FAST_UART_MODE   ((PADS_FAST_UART_MODE == UARTF_MODE(1)) ? BIT4: \
                          (PADS_FAST_UART_MODE == UARTF_MODE(2)) ? BIT5: \
                          (PADS_FAST_UART_MODE == UARTF_MODE(3)) ? BIT5|BIT4: 0 )
        _RVM1(0x1E04, FAST_UART_MODE, BIT5|BIT4),
#endif


#if (PADS_MSPI_MODE != Unknown_pad_mux)
#define MSPI_CFG1   ((PADS_MSPI_MODE == MSPI_MODE(1)) ? BIT0:  \
                     (PADS_MSPI_MODE == MSPI_MODE(2)) ? BIT1:  \
                     (PADS_MSPI_MODE == MSPI_MODE(4)) ? BIT3: 0)
#define MSPI_CFG2   ((PADS_MSPI_MODE == MSPI_MODE(3)) ? BIT0:  \
                     (PADS_MSPI_MODE == MSPI_MODE(5)) ? BIT1: 0)

        _RVM1(0x1EB4, MSPI_CFG1, BIT3|BIT2|BIT1|BIT0),
        _RVM1(0x1EB7, MSPI_CFG2, BIT1|BIT0),

#endif

#if (PADS_ARC_MODE != UNKNOWN_PAD_MUX)
#define ARC_CFG ((PADS_ARC_MODE == ARC_MODE(1)) ? BIT5 : 0)
        _RVM1(0x1EFE, ARC_CFG, BIT5),
#endif

//==========
// UART Select
//==========
#define UART_INV    ((UART0_INV ? BIT0 : 0) | \
                     (UART1_INV ? BIT1 : 0) | \
                     (UART2_INV ? BIT2 : 0) | \
                     (UART3_INV ? BIT3 : 0) | \
                     (UART4_INV ? BIT4 : 0))

        _RVM1(0x1EAB, UART_INV, BITMASK(4:0)),
        _RVM1(0x1EA6, (UART1_SRC_SEL << 4) | (UART0_SRC_SEL << 0), BITMASK(7:0)),
        _RVM1(0x1EA7, (UART3_SRC_SEL << 4) | (UART2_SRC_SEL << 0), BITMASK(7:0)),
        _RVM1(0x1EA8, (UART4_SRC_SEL << 0), BITMASK(3:0)),

#if (PADS_PWM0_MODE != Unknown_pad_mux)
#define PWM0_CFG   ((PADS_PWM0_MODE == PWM0_MODE(1)) ? BIT0:  \
                    (PADS_PWM0_MODE == PWM0_MODE(2)) ? BIT1:  \
                    (PADS_PWM0_MODE == PWM0_MODE(3)) ? BIT0|BIT1: 0)
        _RVM1(0x1EA2, PWM0_CFG, BIT1|BIT0),
#endif

#if (PADS_PWM1_MODE != Unknown_pad_mux)
#define PWM1_CFG   ((PADS_PWM1_MODE == PWM1_MODE(1)) ? BIT4:  \
                    (PADS_PWM1_MODE == PWM1_MODE(2)) ? BIT5:  \
                    (PADS_PWM1_MODE == PWM1_MODE(3)) ? BIT4|BIT5:  0)
        _RVM1(0x1EAA, PWM1_CFG, BIT4|BIT5),
#endif

#if (PADS_PWM2_MODE != Unknown_pad_mux)
#define PWM2_CFG   ((PADS_PWM2_MODE == PWM2_MODE(1)) ? BIT2:  0)
        _RVM1(0x1EA2, PWM2_CFG, BIT2),
        _RVM1(0x1E06, ~(BIT2), BIT2),//pwm2 output enable
#endif

#if (PADS_PWM3_MODE != Unknown_pad_mux)
#define PWM3_CFG   ((PADS_PWM3_MODE == PWM3_MODE(1)) ? BIT3:  0)
        _RVM1(0x1EA2, PWM3_CFG, BIT3),
#endif

#if (PADS_PWM4_MODE != Unknown_pad_mux) //??
#define PWM4_CFG   ((PADS_PWM4_MODE == PWM4_MODE(1)) ? BIT4:  0)
        _RVM1(0x1EA2, PWM4_CFG, BIT4),
#endif

#if (PADS_PWM5_MODE != Unknown_pad_mux)
#define PWM5_CFG   ((PADS_PWM5_MODE == PWM5_MODE(1)) ? BIT0:  \
                    (PADS_PWM5_MODE == PWM5_MODE(2)) ? BIT1:  \
                    (PADS_PWM5_MODE == PWM5_MODE(3)) ? BIT1|BIT0: 0)
        _RVM1(0x1E3A, PWM5_CFG, BIT1|BIT0),
#endif

#if (PADS_NAND_CS1_EN != Unknown_pad_mux)
#define NAND_CS1_EN   ((PADS_NAND_CS1_EN == PAD_NAND_CEZ1) ? BIT2:  0)
        _RVM1(0x1EA1, NAND_CS1_EN, BIT2),
#endif

#if (PADS_TSERR_OUT != Unknown_pad_mux)
#define TSERR_OUT   ((PADS_TSERR_OUT == PAD_I2S_IN_SD) ? BIT7:  \
                     (PADS_TSERR_OUT == PAD_GPIO16) ? BIT8:  \
                     (PADS_TSERR_OUT == PAD_PWM4) ? BIT8|BIT7:  0)
        _RVM2(0x1EA2, TSERR_OUT, BIT7),
		_RVM1(0x1EA3, TSERR_OUT, BIT0),
#endif

#if (PADS_BT656_CTRL != Unknown_pad_mux)
#define BT656_CTR   ((PADS_BT656_CTRL == BT656_CTRL_MODE(1)) ? BIT6:  \
                     (PADS_BT656_CTRL == BT656_CTRL_MODE(2)) ? BIT7:  \
                     (PADS_BT656_CTRL == BT656_CTRL_MODE(3)) ? BIT7|BIT6:  0)
        _RVM1(0x1EDF, BT656_CTR, BIT7|BIT6),
#endif


#if (PADS_DDCR_MODE != Unknown_pad_mux)
#define DDCR_CFG   ((PADS_DDCR_MODE == DDCR_MODE(1)) ? BIT5:  \
                     (PADS_DDCR_MODE == DDCR_MODE(2)) ? BIT6:   \
                     (PADS_DDCR_MODE == DDCR_MODE(3))  ? (BIT6|BIT5)\
                     (PADS_DDCR_MODE == DDCR_MODE(4)) ? BIT7: 0
        _RVM1(0x1EAF, DDCR_CFG, BIT7|BIT6|BIT5),
#endif

#if (PADS_MIIC_MODE0 != Unknown_pad_mux)
#define MIIC_MODE0  ((PADS_MIIC_MODE0 == MIIC_MODE(1)) ? BIT0: \
                     (PADS_MIIC_MODE0 == MIIC_MODE(2)) ? BIT1: 0)
    _RVM1(0x1EB5, MIIC_MODE0, BIT1|BIT0),
#endif

//PAD_TS1_VLD & PAD_TS1_D0: priority => miic_mode1 higher than miic_mode2
#if (PADS_MIIC_MODE1 != Unknown_pad_mux)
#define MIIC_MODE1  ((PADS_MIIC_MODE1 == MIIC_MODE(1)) ? BIT2:  \
                     (PADS_MIIC_MODE1 == MIIC_MODE(2)) ? BIT2:  0)
    _RVM1(0x1EB5, MIIC_MODE1, BIT3|BIT2),
#endif

#if (PADS_MIIC_MODE2 != Unknown_pad_mux)
#define MIIC_MODE2  ((PADS_MIIC_MODE2 == MIIC_MODE(1)) ? BIT4:  \
                     (PADS_MIIC_MODE2 == MIIC_MODE(2)) ? BIT4:  0)
    _RVM1(0x1EB5, MIIC_MODE2, BIT5|BIT4),
#endif

#if (PADS_MIIC_MODE3 != Unknown_pad_mux)
#define MIIC_MODE3  ((PADS_MIIC_MODE3 == MIIC_MODE(1)) ? BIT7:  0)
    _RVM1(0x1EBA, MIIC_MODE3, BIT7),
#endif

#if (PADS_MIIC_MODE4 != Unknown_pad_mux)
#define MIIC_MODE4  ((PADS_MIIC_MODE4 == MIIC_MODE(1)) ? BIT2:  0)
    _RVM1(0x1EC8, MIIC_MODE4, BIT2),
#endif

#if (PADS_MIIC_MODE5 != Unknown_pad_mux)
#define MIIC_MODE5  ((PADS_MIIC_MODE5 == MIIC_MODE(1)) ? BIT3:  0)
    _RVM1(0x1EC8, MIIC_MODE5, BIT3),
#endif

#if (PADS_P1_ENABLE != Unknown_pad_mux)
#define P1_B0    ((PADS_P1_ENABLE & BIT0) ? BIT0 : 0)
#define P1_B1    ((PADS_P1_ENABLE & BIT1) ? BIT1 : 0)
#define P1_B2    ((PADS_P1_ENABLE & BIT2) ? BIT2 : 0)
#define P1_B3    ((PADS_P1_ENABLE & BIT3) ? BIT3 : 0)
#define P1_B4    ((PADS_P1_ENABLE & BIT4) ? BIT4 : 0)
#define P1_B5    ((PADS_P1_ENABLE & BIT5) ? BIT5 : 0)
#define P1_B6    ((PADS_P1_ENABLE & BIT6) ? BIT6 : 0)
#define P1_B7    ((PADS_P1_ENABLE & BIT7) ? BIT7 : 0)

#define SET_P1 P1_B0|P1_B1|P1_B2|P1_B3|P1_B4|P1_B5|P1_B6|P1_B7
        _RVM3(0x1EB8, SET_P1, 0xFF),
#endif

    _MEMMAP_PM_,

    _RVM1(0x0E13, 0,    BIT3),   // RX0_ENABLE
    _RVM1(0x0E13, BIT4, BIT4),   // UART0 -> reg_uart_sel0

    _MEMMAP_nonPM_,

    // Clear all pad in
    _RVM1(0x1EA1, 0, BIT7),
    _END_OF_TBL_,
};

