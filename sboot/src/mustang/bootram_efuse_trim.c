////////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2006-2009 MStar Semiconductor, Inc.
// All rights reserved.
//
// Unless otherwise stipulated in writing, any and all information contained
// herein regardless in any format shall remain the sole proprietary of
// MStar Semiconductor Inc. and be kept in strict confidence
// ("MStar Confidential Information") by the recipient.
// Any unauthorized act including without limitation unauthorized disclosure,
// copying, use, reproduction, sale, distribution, modification, disassembling,
// reverse engineering and compiling of the contents of MStar Confidential
// Information is unlawful and strictly prohibited. MStar hereby reserves the
// rights to any and all damages, losses, costs and expenses resulting therefrom.
//
////////////////////////////////////////////////////////////////////////////////

//---------------------------------------------------------------------------

//---------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------------
//  Define
//-------------------------------------------------------------------------------------------------
#define MIU_RIU_REG_BASE                   0x1F000000

#define PM_SLEEP_RIU_BANK                  0x0e

#define EFUSE_ADDRESS_MASK                 0x007F
#define EFUSE_RIU_BANK                     0x0020
#define REG_EFUSE_FSM1_TRIG                0x4C
#define REG_EFUSE_FSM1_CTRL                0x4E
#define REG_EFUSE_FSM1_RDATA_15_0          0x80
#define REG_EFUSE_FSM1_RDATA_31_16         0x82
#define REG_EFUSE_FSM1_RDATA_47_32         0x84
#define REG_EFUSE_FSM1_RDATA_63_48         0x86
#define EFUSE_OFFSET_DDR_1                 0x24  // 0x5d-0x38-1 = 0x24
#define EFUSE_OFFSET_DDR_2                 0x26  // 0x61-0x38-1 = 0x28
#define EFUSE_OFFSET_DDR_3                 0x2A  // 0x62-0x38 = 0x2A
#define EFUSE_OFFSET_DDR_4                 0x2A  // 0x63-0x38-1 = 0x2A
#define EFUSE_OFFSET_DDR_5                 0x2C  // 0x64-0x38 = 0x2C

#define EFUSSE_ENABLE_FLAG                 0x0  //ENABLE
//#define DEBUG
#define POWER_DIVING_DEFAULT							 0x7477

typedef unsigned char   u8;
typedef unsigned int   u16;
typedef unsigned long  u32;

#define MHal_EFUSE_ReadReg16(u32reg ) *(( volatile u16* ) (MIU_RIU_REG_BASE + EFUSE_RIU_BANK*0x100*2 + ((u32reg) << 1)))
#define MHal_EFUSE_WritReg16(u32reg, u16val ) *(( volatile u16* ) (MIU_RIU_REG_BASE + EFUSE_RIU_BANK*0x100*2 + ((u32reg) << 1))) = (u16val)

#define MHal_PM_ReadReg16(u32reg ) *(( volatile u16* ) (MIU_RIU_REG_BASE + PM_SLEEP_RIU_BANK*0x100*2 + ((u32reg) << 1)))
#define MHal_PM_WritReg16(u32reg, u16val ) *(( volatile u16* ) (MIU_RIU_REG_BASE + PM_SLEEP_RIU_BANK*0x100*2 + ((u32reg) << 1))) = (u16val)


void InitPMSARTrim(void)
{

    u16 u16PMSAR=0;
    u16 u16Temp_0x64=0;
    // read efuse  
    // miu0    
    MHal_EFUSE_WritReg16(0x4E, 0x26);
    MHal_EFUSE_WritReg16(0x4C, 0x01);
    while((MHal_EFUSE_ReadReg16(0x4C) & 0x0001) != 0);
    u16PMSAR = MHal_EFUSE_ReadReg16(0x82);
    u16PMSAR &= 0x3F;
    u16Temp_0x64 = MHal_PM_ReadReg16(0xC8);
    u16Temp_0x64 = ((u16Temp_0x64 & 0xFF81)|((u16PMSAR)<<1));
    MHal_PM_WritReg16(0xC8,u16Temp_0x64); 
}

