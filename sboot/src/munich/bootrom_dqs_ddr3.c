////////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2006-2009 MStar Semiconductor, Inc.
// All rights reserved.
//
// Unless otherwise stipulated in writing, any and all information contained
// herein regardless in any format shall remain the sole proprietary of
// MStar Semiconductor Inc. and be kept in strict confidence
// ("MStar Confidential Information") by the recipient.
// Any unauthorized act including without limitation unauthorized disclosure,
// copying, use, reproduction, sale, distribution, modification, disassembling,
// reverse engineering and compiling of the contents of MStar Confidential
// Information is unlawful and strictly prohibited. MStar hereby reserves the
// rights to any and all damages, losses, costs and expenses resulting therefrom.
//
////////////////////////////////////////////////////////////////////////////////

//---------------------------------------------------------------------------

//---------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------------
//  Define
//-------------------------------------------------------------------------------------------------
#define MIU_RIU_REG_BASE                   0x1F000000
#define MIU0_BUS_BASE                      0x20000000
#define MIU1_BUS_BASE                      0xA0000000

#define MIU0_RIU_DTOP                      0x1012
#define MIU1_RIU_DTOP                      0x1006

#define MIU0_RIU_ARB                       0x1615
#define MIU1_RIU_ARB                       0x1622

#define MIU0_RIU_ATOP                      0x110d
#define MIU1_RIU_ATOP                      0x1616

#define PM_SLEEP_RIU_BANK                  0xe

#define EFUSE_ADDRESS_MASK                 0x007F
#define EFUSE_RIU_BANK                     0x0020
#define REG_EFUSE_FSM1_TRIG                0x4C
#define REG_EFUSE_FSM1_CTRL                0x4E
#define REG_EFUSE_FSM1_RDATA_15_0          0x80
#define REG_EFUSE_FSM1_RDATA_31_16         0x82
#define REG_EFUSE_FSM1_RDATA_47_32         0x84
#define REG_EFUSE_FSM1_RDATA_63_48         0x86
#define EFUSE_OFFSET_DDR_1                 0x24  // 0x5d-0x38-1 = 0x24
#define EFUSE_OFFSET_DDR_2                 0x26  // 0x61-0x38-1 = 0x28
#define EFUSE_OFFSET_DDR_3                 0x2A  // 0x62-0x38 = 0x2A
#define EFUSE_OFFSET_DDR_4                 0x2A  // 0x63-0x38-1 = 0x2A
#define EFUSE_OFFSET_DDR_5                 0x2C  // 0x64-0x38 = 0x2C

#define EFUSSE_ENABLE_FLAG                 0x0  //ENABLE
//#define DEBUG
#define POWER_DIVING_DEFAULT							 0x7477

typedef unsigned char   u8;
typedef unsigned int   u16;
typedef unsigned long  u32;

#define MHal_MIU_ReadReg16(u32bank, u32reg ) *( ( volatile u16* ) (MIU_RIU_REG_BASE + (u32bank)*0x100*2 + ((u32reg) << 1)) )
#define MHal_MIU_WritReg16(u32bank, u32reg, u16val ) *( ( volatile u16* ) (MIU_RIU_REG_BASE + (u32bank)*0x100*2 + ((u32reg) << 1))  ) = (u16val)
#define MHal_MIU_ReadMem16(u32offset) *( ( volatile u16* ) (u32offset))
#define MHal_MIU_WritMem16(u32offset, u16val ) *( ( volatile u16* ) (u32offset)) = (u16val)
//#define HAL_SYS_EfuseRead2Byte(MS_U32 u32RegAddr) ((volatile MS_U16*)(_gMIO_efuse_MapBase))[u32RegAddr];

#define MHal_EFUSE_ReadReg16(u32reg ) *(( volatile u16* ) (MIU_RIU_REG_BASE + EFUSE_RIU_BANK*0x100*2 + ((u32reg) << 1)))
#define MHal_EFUSE_WritReg16(u32reg, u16val ) *(( volatile u16* ) (MIU_RIU_REG_BASE + EFUSE_RIU_BANK*0x100*2 + ((u32reg) << 1))) = (u16val)

#define MHal_PM_ReadReg16(u32reg ) *(( volatile u16* ) (MIU_RIU_REG_BASE + PM_SLEEP_RIU_BANK*0x100*2 + ((u32reg) << 1)))
#define MHal_PM_WritReg16(u32reg, u16val ) *(( volatile u16* ) (MIU_RIU_REG_BASE + PM_SLEEP_RIU_BANK*0x100*2 + ((u32reg) << 1))) = (u16val)


//-------------------------------------------------------------------------------------------------
//  Prototypes
//-------------------------------------------------------------------------------------------------
void putn( u8 n );
void putk( char c );
void delayus(u32 us);
void BootRom_MiuDdr3Dqs(u8 u8MiuDev);
void setPowerSaving(void);
void setMiuXPowerSaving(u16 u16RegMiuAtop);
void InitTrimDRVP_N(void);
void InitDDRTrim(void);
void InitCBUSTrim(void);
void DrvingUpdate(u8 *drvpara, u8 efuse);

//-------------------------------------------------------------------------------------------------
//  Local variables
//-------------------------------------------------------------------------------------------------


//-------------------------------------------------------------------------------------------------
//  Functions
//-------------------------------------------------------------------------------------------------
void putn(u8 n)
{
    char c = '0' + n;

   *(volatile unsigned int*)(0x1F201300) = c;
}

void putk(char c)
{
   *(volatile unsigned int*)(0x1F201300) = c;
}

void delayus(u32 us)
{
    u16 u16RegVal0;

    u16RegVal0 = ((us* 12) & 0xffff);
    MHal_MIU_WritReg16(0x30, 0x24, u16RegVal0);

    u16RegVal0 = ((us* 12) >> 16);
    MHal_MIU_WritReg16(0x30, 0x26, u16RegVal0);

    u16RegVal0 = 0x0002;
    MHal_MIU_WritReg16(0x30, 0x20, u16RegVal0);

    do{
        u16RegVal0 = MHal_MIU_ReadReg16(0x30, 0x22);
    }while((u16RegVal0 & 0x0001) == 0);
}

void BootRom_MiuDdr3Dqs(u8 u8MiuDev)
{

    u16 u16RegVal0;
    u16 u16RegMiuArb;
    u16 u16RegMiuAtop;
    u16 u16RegMiuDtop;
    u16 u16DQSMaxCenter[4];
    u16 u16KCode;
    u16 u16KCodeOffset[4];
    u16 u16RegVal2 = 0;
    u8 i = 0;

    if(u8MiuDev == 0)
    {
        u16RegMiuArb = MIU0_RIU_ARB;
        u16RegMiuAtop = MIU0_RIU_ATOP;
        u16RegMiuDtop = MIU0_RIU_DTOP;;
    }
    else if(u8MiuDev == 1)
    {
        u16RegMiuArb = MIU1_RIU_ARB;
        u16RegMiuAtop = MIU1_RIU_ATOP;
        u16RegMiuDtop = MIU1_RIU_DTOP;;
    }
    else
    {
        return;
    }
#if 0
    if(u8MiuDev == 1)
    {
        InitDDRTrim();

        InitCBUSTrim();
    }
#endif
    /* unmask MIU0_group_4 bit15 */
    MHal_MIU_WritReg16(u16RegMiuArb, 0x06, 0x7fff);

    u16RegVal0 = MHal_MIU_ReadReg16(MIU0_RIU_DTOP, 0xfe);
    MHal_MIU_WritReg16(MIU0_RIU_DTOP, 0xfe, u16RegVal0 & ~(0x0800));
    MHal_MIU_WritReg16(MIU1_RIU_DTOP, 0xf8, 0x0000);
    MHal_MIU_WritReg16(MIU0_RIU_DTOP, 0xe0, 0x0000);
    MHal_MIU_WritReg16(MIU0_RIU_DTOP, 0xe2, 0x0000);

    /* Set test length */
    MHal_MIU_WritReg16(MIU0_RIU_DTOP, 0xe6, 0x0000);

    /* Enable SI mode */
    MHal_MIU_WritReg16(MIU0_RIU_DTOP, 0xd4, MHal_MIU_ReadReg16(MIU0_RIU_DTOP, 0xd4) | 0x01);

    /*Enable miu_test limit mask*/
    MHal_MIU_WritReg16(u16RegMiuArb, 0x1c, 0x8000);
    
    /* DPAT engine pre-setting */
    MHal_MIU_WritReg16(MIU0_RIU_ARB, 0x78, 0x0004);
    MHal_MIU_WritReg16(MIU0_RIU_ARB, 0x78, 0x0000);
    MHal_MIU_WritReg16(u16RegMiuArb, 0x70, 0x0000);
    MHal_MIU_WritReg16(u16RegMiuArb, 0x72, 0x0000);
    //======================================
    //W/R SI mode pattern
    //======================================
    if(u8MiuDev == 0)
    {
        /* switch to MIU0 */
    MHal_MIU_WritReg16(MIU0_RIU_DTOP, 0xf0, 0x0000);
    }
    if(u8MiuDev == 1)
    {
        /* switch to MIU1 */
    MHal_MIU_WritReg16(MIU0_RIU_DTOP, 0xf8, 0x8000);
    }

    MHal_MIU_WritReg16(MIU0_RIU_DTOP, 0xe4, 0x2400); //test length for RX
    MHal_MIU_WritReg16(MIU0_RIU_DTOP, 0xe0, 0x0001);
    do{
        u16RegVal2 = MHal_MIU_ReadReg16(MIU0_RIU_DTOP, 0xe0);
    }while((u16RegVal2 & 0x8000) == 0);
    
    MHal_MIU_WritReg16(MIU0_RIU_DTOP, 0xe0, 0x0000);
    MHal_MIU_WritReg16(u16RegMiuArb, 0x7c, 0x800b);

    MHal_MIU_WritReg16(u16RegMiuArb, 0x74, 0x0c61);

//    //======================================
//    //add protect setting
//    //======================================
//    MHal_MIU_WritReg16(u16RegMiuDtop, 0xc0, 0x0000); //[15: 0] reg_protect0_start
//    MHal_MIU_WritReg16(u16RegMiuDtop, 0xc2, 0x1fff); //[15: 0] reg_protect0_end
//    MHal_MIU_WritReg16(u16RegMiuDtop, 0x2e, 0x0001); //[ 6: 0] reg_protect_id0
//    MHal_MIU_WritReg16(u16RegMiuDtop, 0x20, 0x0001); //[15: 0] reg_protect0_id_enable
//    MHal_MIU_WritReg16(u16RegMiuDtop, 0xd2, MHal_MIU_ReadReg16(u16RegMiuDtop, 0xd2) | 0x0001);


    MHal_MIU_WritReg16(u16RegMiuArb, 0x72, 0x2000);
    
    MHal_MIU_WritReg16(MIU0_RIU_ATOP, 0x70, 0x0000);
    MHal_MIU_WritReg16(MIU1_RIU_ATOP, 0x70, 0x0000);
    
    //MHal_MIU_WritReg16(u16RegMiuArb, 0x74, 0x0c61);
    MHal_MIU_WritReg16(u16RegMiuArb, 0x78, 0x0000);
    MHal_MIU_WritReg16(MIU0_RIU_ARB, 0x78, 0x0004);
    MHal_MIU_WritReg16(MIU0_RIU_ARB, 0x78, 0x0000);
    
    MHal_MIU_WritReg16(MIU0_RIU_ATOP, 0x90, 0x00f0);
    MHal_MIU_WritReg16(MIU0_RIU_ATOP, 0x40, 0xffff);
    MHal_MIU_WritReg16(MIU0_RIU_ATOP, 0x42, 0xffff);
    MHal_MIU_WritReg16(MIU0_RIU_ATOP, 0x44, 0x000f);
    MHal_MIU_WritReg16(MIU0_RIU_ATOP, 0x70, 0x0800);
    
    MHal_MIU_WritReg16(MIU1_RIU_ATOP, 0x90, 0x00f0);
    MHal_MIU_WritReg16(MIU1_RIU_ATOP, 0x40, 0xffff);
    MHal_MIU_WritReg16(MIU1_RIU_ATOP, 0x42, 0xffff);
    MHal_MIU_WritReg16(MIU1_RIU_ATOP, 0x44, 0x000f);
    MHal_MIU_WritReg16(MIU1_RIU_ATOP, 0x70, 0x0800);    
    
    MHal_MIU_WritReg16(u16RegMiuArb, 0x76, 0x0001);

    MHal_MIU_WritReg16(u16RegMiuArb, 0x78, 0x0013);

    do{
        u16RegVal0 = MHal_MIU_ReadReg16(u16RegMiuArb, 0x78);
    }while((u16RegVal0 & 0x8000) == 0);

    /* STEP 5 : Read RX DQ deskew result & write to REGs */
    MHal_MIU_WritReg16(MIU0_RIU_ATOP, 0xc0, 0x0000);
    u16RegVal0 = MHal_MIU_ReadReg16(MIU0_RIU_ATOP, 0xc2);
    MHal_MIU_WritReg16(MIU0_RIU_ATOP, 0xa0, u16RegVal0);
    u16RegVal0 = MHal_MIU_ReadReg16(MIU0_RIU_ATOP, 0xc4);
    MHal_MIU_WritReg16(MIU0_RIU_ATOP, 0xa2, u16RegVal0);
    u16DQSMaxCenter[0] = MHal_MIU_ReadReg16(MIU0_RIU_ATOP, 0xc6);
    u16DQSMaxCenter[0] = u16DQSMaxCenter[0] & 0x007F;

    MHal_MIU_WritReg16(MIU0_RIU_ATOP, 0xc0, 0x0001);
    u16RegVal0 = MHal_MIU_ReadReg16(MIU0_RIU_ATOP, 0xc2);
    MHal_MIU_WritReg16(MIU0_RIU_ATOP, 0xa4, u16RegVal0);
    u16RegVal0 = MHal_MIU_ReadReg16(MIU0_RIU_ATOP, 0xc4);
    MHal_MIU_WritReg16(MIU0_RIU_ATOP, 0xa6, u16RegVal0);
    u16DQSMaxCenter[1] = MHal_MIU_ReadReg16(MIU0_RIU_ATOP, 0xc6);
    u16DQSMaxCenter[1] = u16DQSMaxCenter[1] & 0x007F;

    MHal_MIU_WritReg16(MIU0_RIU_ATOP, 0xc0, 0x0002);
    u16RegVal0 = MHal_MIU_ReadReg16(MIU0_RIU_ATOP, 0xc2);
    MHal_MIU_WritReg16(MIU1_RIU_ATOP, 0xa0, u16RegVal0);
    u16RegVal0 = MHal_MIU_ReadReg16(MIU0_RIU_ATOP, 0xc4);
    MHal_MIU_WritReg16(MIU1_RIU_ATOP, 0xa2, u16RegVal0);
    u16DQSMaxCenter[2] = MHal_MIU_ReadReg16(MIU0_RIU_ATOP, 0xc6);
    u16DQSMaxCenter[2] = u16DQSMaxCenter[2] & 0x007F;

    MHal_MIU_WritReg16(MIU0_RIU_ATOP, 0xc0, 0x0003);
    u16RegVal0 = MHal_MIU_ReadReg16(MIU0_RIU_ATOP, 0xc2);
    MHal_MIU_WritReg16(MIU1_RIU_ATOP, 0xa4, u16RegVal0);
    u16RegVal0 = MHal_MIU_ReadReg16(MIU0_RIU_ATOP, 0xc4);
    MHal_MIU_WritReg16(MIU1_RIU_ATOP, 0xa6, u16RegVal0);
     u16DQSMaxCenter[3] = MHal_MIU_ReadReg16(MIU0_RIU_ATOP, 0xc6);
    u16DQSMaxCenter[3] = u16DQSMaxCenter[3] & 0x007F;

    MHal_MIU_WritReg16(MIU0_RIU_ATOP, 0x60, 0x33c8);
    u16KCode = MHal_MIU_ReadReg16(MIU0_RIU_ATOP, 0x66);
    u16KCode = ((u16KCode & 0x007F) >> 1);

    for(i= 0; i < 4; i++)
    {
        if(u16DQSMaxCenter[i] > u16KCode)
        {
            u16RegVal0 = u16DQSMaxCenter[i] - u16KCode;
            if (u16RegVal0 >= 0xf)
            {
                u16KCodeOffset[i] = 0x0f;
            }
            else
            {
                u16KCodeOffset[i] = u16RegVal0;
            }
        }
        else
        {
            u16RegVal0 = u16KCode - u16DQSMaxCenter[i];
            if (u16RegVal0 >= 0xf)
            {
                u16KCodeOffset[i] = 0x0f;
            }
            else
            {
                u16KCodeOffset[i] = u16RegVal0;
            }
            u16KCodeOffset[i] = u16KCodeOffset[i] | 0x0010;
        }
    }
    u16RegVal0 = (u16KCodeOffset[1] << 8) | u16KCodeOffset[0];
    MHal_MIU_WritReg16(MIU0_RIU_ATOP, 0x94, u16RegVal0);
    u16RegVal0 = (u16KCodeOffset[3] << 8) | u16KCodeOffset[2];
    MHal_MIU_WritReg16(MIU1_RIU_ATOP, 0x94, u16RegVal0);

    /* STEP 6 : Disable DPAT engine & Set DQS Phase = 1/2* Kcode+offset (ratio mode) */
    MHal_MIU_WritReg16(MIU0_RIU_ATOP, 0x70, 0x0000);
    MHal_MIU_WritReg16(MIU0_RIU_ATOP, 0x40, 0x0000);
    MHal_MIU_WritReg16(MIU0_RIU_ATOP, 0x42, 0x0000);
    MHal_MIU_WritReg16(MIU0_RIU_ATOP, 0x44, 0x0000);
    MHal_MIU_WritReg16(MIU0_RIU_ATOP, 0x90, 0xf0f1);
    MHal_MIU_WritReg16(MIU1_RIU_ATOP, 0x70, 0x0000);
    MHal_MIU_WritReg16(MIU1_RIU_ATOP, 0x40, 0x0000);
    MHal_MIU_WritReg16(MIU1_RIU_ATOP, 0x42, 0x0000);
    MHal_MIU_WritReg16(MIU1_RIU_ATOP, 0x44, 0x0000);
    MHal_MIU_WritReg16(MIU1_RIU_ATOP, 0x90, 0xf0f1);    
    
    MHal_MIU_WritReg16(u16RegMiuArb, 0x78, 0x0000);
    MHal_MIU_WritReg16(u16RegMiuArb, 0x74, 0x0c60);
    
    MHal_MIU_WritReg16(MIU0_RIU_ATOP, 0x70, 0x0800);
    MHal_MIU_WritReg16(MIU1_RIU_ATOP, 0x70, 0x0800);

    MHal_MIU_WritReg16(u16RegMiuArb, 0x1c, 0x0000);

    //MHal_MIU_WritReg16(u16RegMiuDtop, 0xc0, 0x0000);
    //MHal_MIU_WritReg16(u16RegMiuDtop, 0xc2, 0x0000);
    //MHal_MIU_WritReg16(u16RegMiuDtop, 0x2e, 0x0000);
    //MHal_MIU_WritReg16(u16RegMiuDtop, 0x20, 0x0000);
    //MHal_MIU_WritReg16(u16RegMiuDtop, 0xd2, MHal_MIU_ReadReg16(u16RegMiuDtop, 0xd2) & ~(0x0001));

    /* Disable SI mode */
    //u16RegVal0 = MHal_MIU_ReadReg16(MIU0_RIU_DTOP, 0xfe);
    //MHal_MIU_WritReg16(MIU0_RIU_DTOP, 0xfe, u16RegVal0 | 0x0800);
    MHal_MIU_WritReg16(MIU0_RIU_DTOP, 0xd4, MHal_MIU_ReadReg16(MIU0_RIU_DTOP, 0xd4) & ~0x01);

    putk('M');
    putk('I');
    putk('U');
    putn(u8MiuDev);
    putk('_');
    putk('D');
    putk('Q');
    putk('S');
    putk('-');
    putk('O');
    putk('K');
    putk('\n');
    putk('\r');

}


void setMiuXPowerSaving(u16 u16RegMiuAtop)
{

    u16 u16RegVal;
    u16 u16Data;
		
    // low word, low byte
    u16Data = 0x3F;
    u16RegVal = MHal_MIU_ReadReg16(u16RegMiuAtop, 0xD0);
    u16Data &= ~(1<<((u16RegVal) & 0xF));           //d0[3:0] = DQ0
    u16Data &= ~(1<<((u16RegVal >> 4) & 0xF));      //d0[7:4] = DQ1
    u16Data &= ~(1<<((u16RegVal >> 8) & 0xF));      //d0[11:8] = DQ2
    u16Data &= ~(1<<((u16RegVal >> 12) & 0xF));     //d0[15:12] = DQ3

    u16RegVal = MHal_MIU_ReadReg16(u16RegMiuAtop, 0xD2);
    u16Data &= ~(1<<((u16RegVal) & 0xF));           //d2[3:0] = DQ4
    u16Data &= ~(1<<((u16RegVal >> 4) & 0xF));      //d2[7:4] = DQ5
    u16Data &= ~(1<<((u16RegVal >> 8) & 0xF));      //d2[11:8] = DQ6
    u16Data &= ~(1<<((u16RegVal >> 12) & 0xF));     //d2[15:12] = DQ7
    
    u16RegVal = MHal_MIU_ReadReg16(u16RegMiuAtop, 0xD8);    
    u16Data &= ~(1<<((u16RegVal) & 0xF));           //d8[3:0] = DQM0
    
    u16RegVal = (MHal_MIU_ReadReg16(u16RegMiuAtop, 0xFE) & (~0x3F00))|(u16Data << 8);  //fe[13 : 8]
    MHal_MIU_WritReg16(u16RegMiuAtop, 0xFE, u16RegVal);

    //low word, high byte
    u16Data = 0x3F;
    u16RegVal = MHal_MIU_ReadReg16(u16RegMiuAtop, 0xD4);
    u16Data &= ~(1<<((u16RegVal) & 0xF));           //d4[3:0] = DQ8
    u16Data &= ~(1<<((u16RegVal >> 4) & 0xF));      //d4[7:4] = DQ9
    u16Data &= ~(1<<((u16RegVal >> 8) & 0xF));      //d4[11:8] = DQ10
    u16Data &= ~(1<<((u16RegVal >> 12) & 0xF));     //d4[15:12] = DQ11

    u16RegVal = MHal_MIU_ReadReg16(u16RegMiuAtop, 0xD6);
    u16Data &= ~(1<<((u16RegVal) & 0xF));           //d6[3:0] = DQ12
    u16Data &= ~(1<<((u16RegVal >> 4) & 0xF));      //d6[7:4] = DQ13
    u16Data &= ~(1<<((u16RegVal >> 8) & 0xF));      //d6[11:8] = DQ14
    u16Data &= ~(1<<((u16RegVal >> 12) & 0xF));     //d6[15:12] = DQ15

    u16RegVal = MHal_MIU_ReadReg16(u16RegMiuAtop, 0xD8);    
    u16Data &= ~(1<<((u16RegVal >> 4) & 0xF));      //d8[7:4] = DQM1
    
    u16RegVal = (MHal_MIU_ReadReg16(u16RegMiuAtop, 0xFE) & (~0x3F))|(u16Data);  //fe[5 : 0]
    MHal_MIU_WritReg16(u16RegMiuAtop, 0xFE, u16RegVal);

    //high word, low byte
    u16Data = 0x3F;
    u16RegVal = MHal_MIU_ReadReg16(u16RegMiuAtop, 0xE0);
    u16Data &= ~(1<<((u16RegVal) & 0xF));           //e0[3:0] = DQ16
    u16Data &= ~(1<<((u16RegVal >> 4) & 0xF));      //e0[7:4] = DQ17
    u16Data &= ~(1<<((u16RegVal >> 8) & 0xF));      //e0[11:8] = DQ18
    u16Data &= ~(1<<((u16RegVal >> 12) & 0xF));     //e0[15:12] = DQ19

    u16RegVal = MHal_MIU_ReadReg16(u16RegMiuAtop, 0xE2);
    u16Data &= ~(1<<((u16RegVal) & 0xF));           //e2[3:0] = DQ20
    u16Data &= ~(1<<((u16RegVal >> 4) & 0xF));      //e2[7:4] = DQ21
    u16Data &= ~(1<<((u16RegVal >> 8) & 0xF));      //e2[11:8] = DQ22
    u16Data &= ~(1<<((u16RegVal >> 12) & 0xF));     //e2[15:12] = DQ23
    
    u16RegVal = MHal_MIU_ReadReg16(u16RegMiuAtop, 0xE8);    
    u16Data &= ~(1<<((u16RegVal) & 0xF));           //e8[3:0] = DQM2
    
    u16RegVal = (MHal_MIU_ReadReg16(u16RegMiuAtop, 0xFC) & (~0x3F00))|(u16Data << 8);  //fc[13 : 8]
    MHal_MIU_WritReg16(u16RegMiuAtop, 0xFC, u16RegVal);

    //high word, high byte
    u16Data = 0x3F;
    u16RegVal = MHal_MIU_ReadReg16(u16RegMiuAtop, 0xE4);
    u16Data &= ~(1<<((u16RegVal) & 0xF));           //e4[3:0] = DQ24
    u16Data &= ~(1<<((u16RegVal >> 4) & 0xF));      //e4[7:4] = DQ25
    u16Data &= ~(1<<((u16RegVal >> 8) & 0xF));      //e4[11:8] = DQ26
    u16Data &= ~(1<<((u16RegVal >> 12) & 0xF));     //e4[15:12] = DQ27

    u16RegVal = MHal_MIU_ReadReg16(u16RegMiuAtop, 0xE6);
    u16Data &= ~(1<<((u16RegVal) & 0xF));           //e6[3:0] = DQ28
    u16Data &= ~(1<<((u16RegVal >> 4) & 0xF));      //e6[7:4] = DQ29
    u16Data &= ~(1<<((u16RegVal >> 8) & 0xF));      //e6[11:8] = DQ30
    u16Data &= ~(1<<((u16RegVal >> 12) & 0xF));     //e6[15:12] = DQ31

    u16RegVal = MHal_MIU_ReadReg16(u16RegMiuAtop, 0xE8);    
    u16Data &= ~(1<<((u16RegVal >> 4) & 0xF));      //e8[7:4] = DQM3
    
    u16RegVal = (MHal_MIU_ReadReg16(u16RegMiuAtop, 0xFC) & (~0x3F))|(u16Data);  //fc[5 : 0]
    MHal_MIU_WritReg16(u16RegMiuAtop, 0xFC, u16RegVal);
}

void setPowerSaving(void)
{
    //
    // MIU0, 0x110D
    //
    setMiuXPowerSaving (MIU0_RIU_ATOP);

    //
    // MIU1, 0x1616
    //
    setMiuXPowerSaving (MIU1_RIU_ATOP);
}


void DrvingUpdate(u8 *drvpara, u8 efuse)
{
    if((efuse & 3) == 0)
        return;
    if (efuse & 0x04)
    {
        if((*drvpara & 0x8) == 0)
        {
            *drvpara  += (efuse & 0x03);
            if(*drvpara & 0x8)
                *drvpara += 5;
        }
        else
        {
            *drvpara  += (efuse & 0x03);

        }
    }
    else
    {
        if(*drvpara & 0x8)
        {
            *drvpara  -= (efuse & 0x03); 
            if((*drvpara & 0x8) == 0)
            {
                *drvpara -= 5;
            }
        }
        else
        {
            *drvpara  -= (efuse & 0x03); 
        }
    }

}

#if 1
void InitDDRTrim(void)
{
    u8  u8Miu0DrvCs_n=0;
    u8  u8Miu0DrvCs_p=0;
    u8  u8Miu0DrvCmd_n=0;
    u8  u8Miu0DrvCmd_p=0;
    u8  u8Miu0DrvDqs_n = 0;
    u8  u8Miu0DrvDqs_p = 0;
    u8  u8Miu0DrvDq_p = 0;
    u8  u8Miu0DrvDq_n = 0;
    u8  u8Miu0DrvClk_p = 0;
    u8  u8Miu0DrvClk_n = 0;
    u8  u8Miu0EfuseVale_n=0;
    u8  u8Miu0EfuseVale_p=0;
    u8  u8Miu1DrvCs_n=0;
    u8  u8Miu1DrvCs_p=0;
    u8  u8Miu1DrvCmd_n=0;
    u8  u8Miu1DrvCmd_p=0;
    u8  u8Miu1EfuseVale_n=0;
    u8  u8Miu1EfuseVale_p=0;
    u8  u8Miu1DrvDqs_n = 0;
    u8  u8Miu1DrvDqs_p = 0;
    u8  u8Miu1DrvDq_p = 0;
    u8  u8Miu1DrvDq_n = 0;
    u8  u8Miu1DrvClk_p = 0;
    u8  u8Miu1DrvClk_n = 0;
    u16 u16Temp_0x2c=0;
    u16 u16Temp_0x2e=0;
    u16 u16Temp_0x2f=0;
    u8  u8MIU0_FLAG = 0;
    u8  u8MIU1_FLAG = 0;
    u8  u8ZQ_A = 0;
    u8  u8ZQ_B = 0;
    // read efuse  
    // miu0    
    MHal_EFUSE_WritReg16(0x4E, 0x26);
    MHal_EFUSE_WritReg16(0x4C, 0x01);
    while((MHal_EFUSE_ReadReg16(0x4C) & 0x0001) != 0);
    u16Temp_0x2c = MHal_EFUSE_ReadReg16(0x80);
    u8Miu0EfuseVale_p = (u16Temp_0x2c & 0x7);
    u8Miu0EfuseVale_n = ((u16Temp_0x2c & 0x38) >> 3);
    u8MIU0_FLAG = (u16Temp_0x2c & 0x40) >> 6;
    u8MIU1_FLAG = (u16Temp_0x2c & 0x80) >> 7;
    u8ZQ_A = (u16Temp_0x2c & 0xFF00) >> 8;
    // miu 1
    MHal_EFUSE_WritReg16(0x4E, 0x25);
    MHal_EFUSE_WritReg16(0x4C, 0x01);
    while((MHal_EFUSE_ReadReg16(0x4C) & 0x0001) != 0);
    u8Miu1EfuseVale_p = (MHal_EFUSE_ReadReg16(0x86) & 0x1C) >> 2;
    u8Miu1EfuseVale_n = (MHal_EFUSE_ReadReg16(0x86) & 0xE0) >> 5;
    u8ZQ_B = (MHal_EFUSE_ReadReg16(0x86) & 0xFF00) >> 8;
    // MIU0

    if(u8ZQ_A&0x80)
    {
        MHal_MIU_WritReg16(MIU0_RIU_ATOP, 0xB0, (MHal_MIU_ReadReg16(MIU0_RIU_ATOP, 0xB0) & (~0x7F00)) | ((u8ZQ_A & 0x7F) << 8));
    }

    if(u8ZQ_B&0x80)
    {
        MHal_MIU_WritReg16(MIU1_RIU_ATOP, 0xB0, (MHal_MIU_ReadReg16(MIU1_RIU_ATOP, 0xB0) & (~0xFF00)) | ((u8ZQ_B & 0x7F) << 8));
    }

    if(u8MIU0_FLAG)
    {
        u16Temp_0x2c = MHal_MIU_ReadReg16(MIU0_RIU_ATOP, 0x58);
        u16Temp_0x2e = MHal_MIU_ReadReg16(MIU0_RIU_ATOP, 0x5c);
        u16Temp_0x2f = MHal_MIU_ReadReg16(MIU0_RIU_ATOP, 0x5e);
        u8Miu0DrvCs_n =  (u16Temp_0x2c & 0x07);
        u8Miu0DrvCs_p =  ((u16Temp_0x2c >> 8) & 0x07);
        u8Miu0DrvCmd_n = ((u16Temp_0x2e >> 8) & 0x07);
        u8Miu0DrvCmd_p = ((u16Temp_0x2f >> 8) & 0x07);
        u8Miu0DrvDqs_n = (u16Temp_0x2e & 0x07);
        u8Miu0DrvDqs_p = (u16Temp_0x2f & 0x07);
        u8Miu0DrvDq_p  = ((u16Temp_0x2f >> 4) & 0x07);
        u8Miu0DrvDq_n  = ((u16Temp_0x2e >> 4) & 0x07);
        u8Miu0DrvClk_p = ((u16Temp_0x2f >> 12) & 0x07);
        u8Miu0DrvClk_n = ((u16Temp_0x2e >> 12) &0x07);
        DrvingUpdate(&u8Miu0DrvCs_n, u8Miu0EfuseVale_n);
        DrvingUpdate(&u8Miu0DrvCmd_n, u8Miu0EfuseVale_n);
        DrvingUpdate(&u8Miu0DrvDqs_n, u8Miu0EfuseVale_n);
        DrvingUpdate(&u8Miu0DrvDq_n, u8Miu0EfuseVale_n);
        DrvingUpdate(&u8Miu0DrvClk_n, u8Miu0EfuseVale_n);

        DrvingUpdate(&u8Miu0DrvCs_p, u8Miu0EfuseVale_p);
        DrvingUpdate(&u8Miu0DrvCmd_p, u8Miu0EfuseVale_p);
        DrvingUpdate(&u8Miu0DrvDqs_p, u8Miu0EfuseVale_p);
        DrvingUpdate(&u8Miu0DrvDq_p, u8Miu0EfuseVale_p);
        DrvingUpdate(&u8Miu0DrvClk_p, u8Miu0EfuseVale_p);

        MHal_MIU_WritReg16(MIU0_RIU_ATOP, 0x58, (MHal_MIU_ReadReg16(MIU0_RIU_ATOP, 0x58) & (~0x000f)) | (u8Miu0DrvCs_n & 0x0f));
        MHal_MIU_WritReg16(MIU0_RIU_ATOP, 0x58, (MHal_MIU_ReadReg16(MIU0_RIU_ATOP, 0x58) & (~0x0f00)) | (u8Miu0DrvCs_p & 0x0f) << 8);
        MHal_MIU_WritReg16(MIU0_RIU_ATOP, 0x5c, ((u8Miu0DrvClk_n << 12) | (u8Miu0DrvCmd_n << 8) | (u8Miu0DrvDq_n << 4) | u8Miu0DrvDqs_n));
        MHal_MIU_WritReg16(MIU0_RIU_ATOP, 0x5e, ((u8Miu0DrvClk_p << 12) | (u8Miu0DrvCmd_p << 8) | (u8Miu0DrvDq_p << 4) | u8Miu0DrvDqs_p));  
    }
    // Miu1
    if(u8MIU1_FLAG)
    {
        u16Temp_0x2c = MHal_MIU_ReadReg16(MIU1_RIU_ATOP, 0x58);
        u16Temp_0x2e = MHal_MIU_ReadReg16(MIU1_RIU_ATOP, 0x5c);
        u16Temp_0x2f = MHal_MIU_ReadReg16(MIU1_RIU_ATOP, 0x5e);

        u8Miu1DrvCs_n =  (u16Temp_0x2c & 0x07);
        u8Miu1DrvCs_p =  ((u16Temp_0x2c >> 8) & 0x07);
        u8Miu1DrvCmd_n = ((u16Temp_0x2e >> 8) & 0x07);
        u8Miu1DrvCmd_p = ((u16Temp_0x2f >> 8) & 0x07);
        u8Miu1DrvDqs_n = (u16Temp_0x2e & 0x07);
        u8Miu1DrvDqs_p = (u16Temp_0x2f & 0x07);
        u8Miu1DrvDq_p  = ((u16Temp_0x2f >> 4) & 0x07);
        u8Miu1DrvDq_n  = ((u16Temp_0x2e >> 4) & 0x07);
        u8Miu1DrvClk_p = ((u16Temp_0x2f >> 12) & 0x07);
        u8Miu1DrvClk_n = ((u16Temp_0x2e >> 12) &0x07);  
        DrvingUpdate(&u8Miu1DrvCs_n, u8Miu1EfuseVale_n);
        DrvingUpdate(&u8Miu1DrvCmd_n, u8Miu1EfuseVale_n);
        DrvingUpdate(&u8Miu1DrvDqs_n, u8Miu1EfuseVale_n);
        DrvingUpdate(&u8Miu1DrvDq_n, u8Miu1EfuseVale_n);
        DrvingUpdate(&u8Miu1DrvClk_n, u8Miu1EfuseVale_n);

        DrvingUpdate(&u8Miu1DrvCs_p, u8Miu1EfuseVale_p);
        DrvingUpdate(&u8Miu1DrvCmd_p, u8Miu1EfuseVale_p);
        DrvingUpdate(&u8Miu1DrvDqs_p, u8Miu1EfuseVale_p);
        DrvingUpdate(&u8Miu1DrvDq_p, u8Miu1EfuseVale_p);
        DrvingUpdate(&u8Miu1DrvClk_p, u8Miu1EfuseVale_p);

 
        MHal_MIU_WritReg16(MIU1_RIU_ATOP, 0x58, (MHal_MIU_ReadReg16(MIU1_RIU_ATOP, 0x58) & (~0x000f)) | (u8Miu1DrvCs_n & 0x0f));
        MHal_MIU_WritReg16(MIU1_RIU_ATOP, 0x58, (MHal_MIU_ReadReg16(MIU1_RIU_ATOP, 0x58) & (~0x0f00)) | (u8Miu1DrvCs_p & 0x0f) << 8);
        MHal_MIU_WritReg16(MIU1_RIU_ATOP, 0x5c, ((u8Miu1DrvClk_n << 12) | (u8Miu1DrvCmd_n << 8) | (u8Miu1DrvDq_n << 4) | u8Miu1DrvDqs_n));
        MHal_MIU_WritReg16(MIU1_RIU_ATOP, 0x5e, ((u8Miu1DrvClk_p << 12) | (u8Miu1DrvCmd_p << 8) | (u8Miu1DrvDq_p << 4) | u8Miu1DrvDqs_p));
     }
             
}


void InitCBUSTrim(void)
{
    u16 regTempCBUS1 = 0;
    u8  u8CbusActiveFlagD = 0;
    u8  u8CbusActiveFlagC = 0;
    u8  u8PD1KLOWD = 0;
    u8  u8PD1KLOWC = 0;
    u8  u8PD1KHIGHD = 0;
    u8  u8PD1KHIGHC = 0;
    u8  u8PD100KLOWD = 0;
    u8  u8PD100KLOWC = 0;
    u8  u8PD100KHIGHD = 0;
    u8  u8PD100KHIGHC = 0;
    MHal_EFUSE_WritReg16(0x4E, 0x27);
    MHal_EFUSE_WritReg16(0x4C, 0x01);
    while((MHal_EFUSE_ReadReg16(0x4C) & 0x0001) != 0);
    u8CbusActiveFlagD = ((MHal_EFUSE_ReadReg16(0x84) >> 12) & 1);
    u8CbusActiveFlagC = ((MHal_EFUSE_ReadReg16(0x86) >> 1) & 1);
    u8PD100KLOWC = (MHal_EFUSE_ReadReg16(0x86) & 1);
    *(u16*)0x1f200A00 = MHal_EFUSE_ReadReg16(0x86);
    *(u16*)0x1f200A04 = MHal_EFUSE_ReadReg16(0x84);
    if(u8CbusActiveFlagD)
    {
        u8PD1KLOWD = ((MHal_EFUSE_ReadReg16(0x84) >> 9) & 1);
        u8PD100KLOWD = ((MHal_EFUSE_ReadReg16(0x84) >> 11) & 1); 
        u8PD1KHIGHD =  ((MHal_EFUSE_ReadReg16(0x84) >> 8) & 1);
        u8PD100KHIGHD = ((MHal_EFUSE_ReadReg16(0x84) >> 10) & 1);
        regTempCBUS1 = ((MHal_PM_ReadReg16(0x68) &(~0xf)) | (u8PD1KLOWD | (u8PD100KLOWD << 1) | (u8PD1KHIGHD << 2) | (u8PD100KHIGHD << 3)));
        MHal_PM_WritReg16(0x68 , regTempCBUS1);
    }
    if(u8CbusActiveFlagC)
    {

       u8PD1KLOWC = ((MHal_EFUSE_ReadReg16(0x84) >> 14) & 1);
       u8PD1KHIGHC = ((MHal_EFUSE_ReadReg16(0x84) >> 13) & 1);
       u8PD100KHIGHC = ((MHal_EFUSE_ReadReg16(0x84) >> 15) & 1);
	 
       regTempCBUS1 =  ((MHal_PM_ReadReg16(0x68) &(~0xf0)) | ((u8PD1KLOWC << 4) | (u8PD100KLOWC << 5) | (u8PD1KHIGHC << 6) | (u8PD100KHIGHC << 7)));
       MHal_PM_WritReg16(0x68 , regTempCBUS1);
    }
    
}

#if 0
void Miu_DramSizeCheck(u8 u8MiuDev)
{
    u16 u16MemVal;
    u16 u16RegVal;
    u16 u16RegMiuDtop;
    u32 u32MemBusBase;

    if(u8MiuDev == 0)
    {
        u16RegMiuDtop = MIU0_RIU_DTOP;
        u32MemBusBase = MIU0_BUS_BASE;
    }
    else if(u8MiuDev == 1)
    {
        u16RegMiuDtop = MIU1_RIU_DTOP;
        u32MemBusBase = MIU1_BUS_BASE;
    }
    else
    {
        return;
    }
    
    /* Check if dram exist on the specific MII# */
    u16RegVal = MHal_MIU_ReadReg16(u16RegMiuDtop, 0xd2);
    u16RegVal = u16RegVal & 0xf000;
    if(u16RegVal == 0x1000 )
    {
        goto dram_size_reset;
    }
    
    putk('\n');
    putk('\r');
    putk('M');
    putk('I');
    putk('U');
    putn(u8MiuDev);
    putk('-');

    /* Reset dram with offset 0M */
    MHal_MIU_WritMem16(u32MemBusBase + 0x0, 0x0);
    
    /* Check if dram is equal to 512M */
    MHal_MIU_WritMem16(u32MemBusBase + 0x20000000, 0x9);
    u16MemVal = MHal_MIU_ReadMem16(u32MemBusBase + 0x0);
    if(u16MemVal == 0x9)
    {
        MHal_MIU_WritReg16(u16RegMiuDtop, 0xd2, 0x9000);
        MHal_MIU_WritReg16(u16RegMiuDtop, 0x0e, 0x4080);
        putk('5');
        putk('1');
        putk('2');
        goto dram_check_exit;
    }
    
    /* Set dram is equal to 1024M */
    MHal_MIU_WritReg16(u16RegMiuDtop, 0xd2, 0xa000);
    putk('1');
    putk('0');
    putk('2');
    putk('4');
    goto dram_check_exit;

dram_size_reset:
    /* Set dram size 0M*/
    MHal_MIU_WritReg16(u16RegMiuDtop, 0xd2, 0x0000);

dram_check_exit:
    /* Clear Dram hit log */
    MHal_MIU_WritReg16(u16RegMiuDtop, 0xde, 0x0001);
    MHal_MIU_WritReg16(u16RegMiuDtop, 0xde, 0x0000);
}
#endif
#endif

