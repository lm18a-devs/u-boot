#include <stdlib.h>
#include <string.h>
#include <stdio.h>

//L2 Cache Init
#define L2X0_CACHE_ID           0x000
#define L2X0_CACHE_ID_PART_MASK   (0xf << 6)
#define L2X0_CACHE_ID_PART_L210   (1 << 6)
#define L2X0_CACHE_ID_PART_L310   (3 << 6)
#define L2X0_CACHE_TYPE         0x004
#define L2X0_CTRL           0x100
#define L2X0_AUX_CTRL           0x104
#define L2X0_TAG_LATENCY_CTRL       0x108
#define L2X0_DATA_LATENCY_CTRL      0x10C
#define L2X0_EVENT_CNT_CTRL     0x200
#define L2X0_EVENT_CNT1_CFG     0x204
#define L2X0_EVENT_CNT0_CFG     0x208
#define L2X0_EVENT_CNT1_VAL     0x20C
#define L2X0_EVENT_CNT0_VAL     0x210
#define L2X0_INTR_MASK          0x214
#define L2X0_MASKED_INTR_STAT       0x218
#define L2X0_RAW_INTR_STAT      0x21C
#define L2X0_INTR_CLEAR         0x220
#define L2X0_CACHE_SYNC         0x730
#define L2X0_INV_LINE_PA        0x770
#define L2X0_INV_WAY            0x77C
#define L2X0_CLEAN_LINE_PA      0x7B0
#define L2X0_CLEAN_LINE_IDX     0x7B8
#define L2X0_CLEAN_WAY          0x7BC
#define L2X0_CLEAN_INV_LINE_PA      0x7F0
#define L2X0_CLEAN_INV_LINE_IDX     0x7F8
#define L2X0_CLEAN_INV_WAY      0x7FC
#define L2X0_LOCKDOWN_WAY_D     0x900
#define L2X0_LOCKDOWN_WAY_I     0x904
#define L2X0_TEST_OPERATION     0xF00
#define L2X0_LINE_DATA          0xF10
#define L2X0_LINE_TAG           0xF30
#define L2X0_DEBUG_CTRL         0xF40

#define readl_relaxed(addr) *(volatile unsigned long*)(addr)
#define writel_relaxed(value,addr) *(volatile unsigned long*)(addr) = value
#define L2_CACHE_PHYS         0x15000000
#define L2_CACHE_OFFSET       0x00000000
#define L2_CACHE_SIZE         0x1000
#define L2_CACHE_VIRT         (L2_CACHE_PHYS + L2_CACHE_OFFSET)
#define L2_CACHE_p2v(pa)      ((pa) + L2_CACHE_OFFSET)
#define L2_CACHE_v2p(va)      ((va) - L2_CACHE_OFFSET)
#define L2_CACHE_ADDRESS(x)   L2_CACHE_p2v(x)

/* Constants of AMBER3 L2 Linefill & Prefetch */
#define L2_LINEFILL             1
#define L2_PREFETCH             1
#define PREFETCH_CTL_REG        0xF60

#define DOUBLE_LINEFILL_ENABLE  0x40000000  //[30]
#define I_PREFETCH_ENABLE       0x20000000  //[29]
#define D_PREFETCH_ENABLE       0x10000000  //[28]
#define LINEFILL_WRAP_DISABLE   0x08000000  //[27]
#define PREFETCH_OFFSET         0x00000000  //[4:0] ,only support 0-7,15,23,31

#define L2_CACHE_write(v,a)         (*(volatile unsigned int *)L2_CACHE_ADDRESS(a) = (v))
#define L2_CACHE_read(a)            (*(volatile unsigned int *)L2_CACHE_ADDRESS(a))
#define L2X0_TAG_LATENCY_CTRL       0x108
#define L2X0_DATA_LATENCY_CTRL      0x10C

static void *l2x0_base;
static unsigned long l2x0_way_mask; /* Bitmask of active ways */
static inline void cache_wait_way(void *reg, unsigned long mask)
{
    /* wait for cache operation by line or way to complete */
    while (readl_relaxed(reg) & mask)
    ;
}
static inline void cache_sync(void)
{
    void *base = l2x0_base;
    writel_relaxed(0, (unsigned long)base + L2X0_CACHE_SYNC);
}
static inline void l2x0_inv_all(void)
{
    /* invalidate all ways */
    writel_relaxed(l2x0_way_mask, (unsigned long)l2x0_base + L2X0_INV_WAY);
    cache_wait_way((void *) ((unsigned long) l2x0_base + L2X0_INV_WAY), l2x0_way_mask);
    cache_sync();
}
void l2x0_init(void *base, unsigned long aux_val, unsigned long aux_mask)
{
    unsigned long aux;
    unsigned long cache_id;
    int ways;
    const char *type;

    l2x0_base = base;

    cache_id = readl_relaxed((unsigned long)l2x0_base + L2X0_CACHE_ID);
    aux = readl_relaxed((unsigned long)l2x0_base + L2X0_AUX_CTRL);

    aux &= aux_mask;
    aux |= aux_val;

    /* Determine the number of ways */
    switch (cache_id & L2X0_CACHE_ID_PART_MASK)
    {
        case L2X0_CACHE_ID_PART_L310:
        if (aux & (1 << 16))
        ways = 16;
        else
        ways = 8;
        type = "L310";
        break;
        case L2X0_CACHE_ID_PART_L210:
        ways = (aux >> 13) & 0xf;
        type = "L210";
        break;
        default:
        /* Assume unknown chips have 8 ways */
        ways = 8;
        type = "L2x0 series";
        break;
    }

    l2x0_way_mask = (1 << ways) - 1;

    /*
     * Check if l2x0 controller is already enabled.
     * If you are booting from non-secure mode
     * accessing the below registers will fault.
     */
    if (!(readl_relaxed((unsigned long)l2x0_base + L2X0_CTRL) & 1))
    {

        /* l2x0 controller is disabled */
        writel_relaxed(aux, (unsigned long)l2x0_base + L2X0_AUX_CTRL);

        l2x0_inv_all();

        /* enable L2X0 */
        writel_relaxed(1, (unsigned long)l2x0_base + L2X0_CTRL);
    }
}

void l2_cache_init(void)
{
    unsigned int val = 0;
    void * l2x0_base = (void *) (L2_CACHE_ADDRESS(L2_CACHE_PHYS));

    /*L2_LINEFILL*/
    val = L2_CACHE_read(L2_CACHE_PHYS + PREFETCH_CTL_REG);

    L2_CACHE_write(( val | DOUBLE_LINEFILL_ENABLE | LINEFILL_WRAP_DISABLE ), L2_CACHE_PHYS + PREFETCH_CTL_REG);

    /*L2_PREFETCH*/
    val = L2_CACHE_read(L2_CACHE_PHYS + PREFETCH_CTL_REG);
    L2_CACHE_write(( val | I_PREFETCH_ENABLE | D_PREFETCH_ENABLE | PREFETCH_OFFSET ), L2_CACHE_PHYS + PREFETCH_CTL_REG);

    /* set RAM latencies to 2 cycle for this core tile. */
    writel_relaxed(0x121, (unsigned long)l2x0_base + L2X0_TAG_LATENCY_CTRL);
    writel_relaxed(0x121, (unsigned long)l2x0_base + L2X0_DATA_LATENCY_CTRL);

    l2x0_init(l2x0_base, 0x00400000, 0xfe0fffff);
}
