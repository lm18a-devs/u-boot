/*
 * Copyright (C) 2001 MontaVista Software Inc.
 * Author: Jun Sun, jsun@mvista.com or jsun@junsun.net
 *
 * This program is free software; you can redistribute  it and/or modify it
 * under  the terms of  the GNU General  Public License as published by the
 * Free Software Foundation;  either version 2 of the  License, or (at your
 * option) any later version.
 *
 */

#ifndef _strboottee_h_
#define _strboottee_h_

#define NUTTX_HEADER_LEN 0x80
#define NUTTX_BOOT_CONFIG_SIZE 0x1000
#define TEE_BOOT_CONFIG_ID           "TEE_BOOT_CONFIG"
#define HW_AES_ADDR                  "E_MMAP_ID_HW_AES_BUF"
#define NUTTX_ADDR                   "E_MMAP_ID_NUTTX_MEM"
#define ENV_TEE_DEBUG_LEVEL          "teeDebugLevel"
#define UART_DBG_SRC    0x0D

#define ALIGN_BYTES 16
#define FOUR_BIT_SHIFT 4
#define CMD_BUF 128
#define R2_CACHE_ADDR 0x00000000
#define MAGIC_ID_LEN 16
#define HEADER_SIZE 128

#define MEM_START_ADDR 0x20100000

#define BOOT_CONFIG_FLAG_SIZE 1
#define ONE_G_MIU_INTERVAL_FLAG_SIZE 1

/// definition for TRUE
#define TRUE                        1
/// definition for FALSE
#define FALSE                       0

// For Co-Processor
#define R2_REG_BASE                             0x022A00UL    //0x122A00 //sec_r2
#define R2_MAULV1_REG_BASE                      0x022B00UL    //0x122B00 //sec_r2_maulv1
#define R2_MAULV2_REG_BASE                      0x063900UL    //0X163A00 //sec_r2_maulv2
// For Non-PM 
#define NPM_REG_CLKGEN0                         0x000B00UL    //0x100b00 //clkgen0
#define NPM_REG_CLKGEN1                         0x003300UL    //0x103300 //clkgen1
#define NPM_REG_CHIPTOP                         0x001E00UL    //0x101E00 //chiptop
#define NPM_REG_MIU0                            0x001200UL    //0x101200 //miu0
#define NPM_REG_MIU1                            0x000600UL    //0x100600 //miu1
#define NPM_REG_ANAMISC                         0x010C00UL    //0x110c00 //ana misc
//For PM
#define PM_REG_BASE_SLEEP                       0x000E00UL    //0x002E00 //pm_sleep
#define PM_REG_BASE_MISC                        0x002E00UL    //0x002E00 //pm_misc

//------------------------------------------------------------------------------
// MCU and PIU Reg
//------------------------------------------------------------------------------
// For Co-Processor
#define R2_REG_STOP                             (R2_REG_BASE+0x0080UL)
#define R2_REG_SDR_LO_INST_BASE                 (R2_REG_BASE+0x0082UL)
#define R2_REG_SDR_HI_INST_BASE                 (R2_REG_BASE+0x0084UL)
#define R2_REG_SDR_LO_DATA_BASE                 (R2_REG_BASE+0x0086UL)
#define R2_REG_SDR_HI_DATA_BASE                 (R2_REG_BASE+0x0088UL)
#define R2_REG_RIU_BASE                         (R2_REG_BASE+0x008AUL)
#define R2_REG_RST_BASE                         (R2_REG_BASE+0x00B4UL)
#define R2_REG_IO1_BASE                         (R2_REG_BASE+0x00AAUL)
#define R2_REG_SPI_BASE                         (R2_REG_BASE+0x0090UL)
#define R2_REG_SPI_BASE1                        (R2_REG_BASE+0x00ACUL)
#define R2_REG_DQMEM_BASE                       (R2_REG_BASE+0x009CUL)
#define R2_REG_SPACE_EN                         (R2_REG_BASE+0x00B0UL)
#define R2_REG_QMEM_MASK_HIGH                   (R2_REG_BASE+0x00A0UL)
#define R2_REG_QMEM_BASE_HIGH                   (R2_REG_BASE+0x009CUL) 

#define R2_MAULV1_REG                           (R2_MAULV1_REG_BASE + 0x0002UL)    //0x122B00 //sec_r2_maulv1
#define R2_MAULV2_REG                           (R2_MAULV2_REG_BASE + 0x0002UL)    //0X163A00 //sec_r2_maulv2

// For Non-PM
#define NPM_REG_CLKGEN1_SECR2                   (NPM_REG_CLKGEN0+0x0094UL) //3D*2
#define NPM_REG_CHIPTOP_UART                    (NPM_REG_CHIPTOP+0x00A6UL)
#define NPM_REG_MIPS_PLLCLK                     (NPM_REG_ANAMISC+0x0026UL)
#define NPM_REG_LPF_LOW                         (NPM_REG_ANAMISC + 0x00C0UL)
#define NPM_REG_LPF_HIGH                        (NPM_REG_ANAMISC + 0x00C2UL)

// For PM
#define PM_REG_CPUX_SW_RSTZ                     (PM_REG_BASE_MISC+0x0052UL)

#define NONE_CACHEABLE                          0x80000000UL

typedef enum
{
    EN_SUCCESS      =0,
    EN_ERROR_OF_CMD   ,
    EN_ERROR_OF_CRC
}EN_SIGNATURE_STORAGE;

typedef enum
{
    EN_NUTTX_CONFIG_512=0,
    EN_NUTTX_CONFIG_768,
     EN_NUTTX_CONFIG_1024,
    EN_NUTTX_CONFIG_INVALID,
    EN_NUTTX_CONFIG_MAX=EN_NUTTX_CONFIG_INVALID
}EN_NUTTX_LEVEL;

typedef struct
{
    unsigned int uitee_header_len;
    unsigned int uitee_start_paddr;    
    unsigned int uitee_start_vaddr;
    unsigned int uitee_image_size;
    unsigned long long ulltime;
    unsigned long long ullTeeBinVersion;
    unsigned int uitee_mem_len;
    unsigned int uihw_aes_paddr;    
    unsigned int uihw_aes_buffer_len;    
    unsigned int uiHeaderVersion;
    unsigned int uicrc_value;
}_ST_TEE_HEADER;


typedef struct
{
    _ST_TEE_HEADER _sthd;
    unsigned char reserved[HEADER_SIZE-sizeof(_ST_TEE_HEADER)-ONE_G_MIU_INTERVAL_FLAG_SIZE-BOOT_CONFIG_FLAG_SIZE-MAGIC_ID_LEN];    
    unsigned char one_g_miu_interval[ONE_G_MIU_INTERVAL_FLAG_SIZE];
    unsigned char bootConfigFlag[BOOT_CONFIG_FLAG_SIZE];
    unsigned char ucMagicID[MAGIC_ID_LEN];
}ST_TEE_HEADER;


typedef enum
{
    EN_TEE_DEBUG_LEVEL_DISABLE=0,
    EN_TEE_DEBUG_LEVEL_ERROR=0x01,
    EN_TEE_DEBUG_LEVEL_INFO=0x02,
    EN_TEE_DEBUG_LEVEL_TRACE=0x04,
    EN_TEE_DEBUG_LEVEL_DEBUG=0x08,
    EN_TEE_DEBUG_LEVEL_DEBUG_L2=0x10,
    EN_TEE_DEBUG_LEVEL_INVALID,
    EN_TEE_DEBUG_LEVEL_MAX=EN_TEE_DEBUG_LEVEL_INVALID
}EN_TEE_DEBUG_LEVEL;

typedef enum
{
    //this valuse is offset in chunk header.
    CH_ITEM_FIRST=0x00,
    CH_UBOOT_ROM_START=CH_ITEM_FIRST,
    CH_UBOOT_RAM_START,//0x04
    CH_UBOOT_RAM_END,//0x08
    CH_UBOOT_ROM_END,//0x0c
    CH_UBOOT_RAM_ENTRY,//0x10
    RESERVED1,//0x14,Reserved1
    RESERVED2,//0x18,Reserved2
    CH_BINARY_ID,//0x1c
    CH_LOGO_ROM_OFFSET,//0x20
    CH_LOGO_ROM_SIZE,//0x24
    CH_SBOOT_ROM_OFFSET,//0x28
    CH_SBOOT_SIZE,//0x2c
    CH_SBOOT_RAM_OFFSET,//0x30
    CH_PM_ROM_OFFSET,//0x34
    CH_PM_SIZE,//0x38
    CH_PM_RAM_OFFSET,//0x3c
    CH_SECURITY_INFO_LOADER_ROM_OFFSET,//0x40
    CH_SECURITY_INFO_LOADER_SIZE,//0x44
    CH_SECURITY_INFO_LOADER_RAM_OFFSET,//0x48
    CH_CUSTOMER_KEY_BANK_ROM_OFFSET,//0x4c
    CH_CUSTOMER_KEY_BANK_SIZE,//0x50
    CH_CUSTOMER_KEY_BANK_RAM_OFFSET,//0x54
    CH_SECURITY_INFO_AP_ROM_OFFSET,//0x58
    CH_SECURITY_INFO_AP_SIZE,//0x5C
    CH_UBOOT_ENVIRONMENT_ROM_OFFSET,//0x60
    CH_UBOOT_ENVIRONMENT_SIZE,//0x64
    CH_DDR_BACKUP_TABLE_ROM_OFFSET,//0x68
    CH_POWER_SEQUENCE_TABLE_ROM_OFFSET,//0x6c
    CH_UBOOT_POOL_ROM_OFFSET,//0x70
    CH_UBOOT_POOL_SIZE,//0x74
    CH_CLEANBOOT_ROM_OFFSET,//0x78
    CH_CLEANBOOT_SIZE,//0x7c
    CH_RT_AEON_ROM_OFFSET,//0x80
    CH_RT_AEON_SIZE,//0x84
    CH_NUTTX_CONFIG_OFFSET,//0x88
    CH_NUTTX_CONFIG_SIZE,//0x8c
    CH_MBOOT_CONFIG_BINARY_OFFSET, //0x90
    CH_MBOOT_CONFIG_BINARY_SIZE, //0x94
    CH_RESCURE_ENVIRONMENT_ROM_OFFSET,//0x98
    CH_RESCURE_ENVIRONMENT_SIZE,//0x9c
    CH_RESCURE_STATUS_ROM_OFFSET,//0xa0
    CH_RESCURE_STATUS_SIZE,//0xa4
    CH_ITEM_LAST=CH_RESCURE_STATUS_SIZE
} EN_CHUNK_HEADER_ITEM;

typedef struct
{
    U8  u8BootConfigID[16];
    EN_TEE_DEBUG_LEVEL  enDebugLevel;
    U8  u8dummy[3];
    U8  u8FastBootMode;
    U8  u8Reserved[64];
}ST_TEE_BOOT_CONFIG;

typedef struct
{
    U8  u8BootConfigID[16];
    EN_TEE_DEBUG_LEVEL  enDebugLevel;
    U8  u8FastBootMode;
    U8  u8Reserved[64];
}ST_TEE_BOOT_CONFIG_ALIGN;

#define SIGNATURE_LEN               256
#define RSA_PUBLIC_KEY_N_LEN 256
#define RSA_PUBLIC_KEY_E_LEN 4
#define RSA_PUBLIC_KEY_LEN          (RSA_PUBLIC_KEY_N_LEN+RSA_PUBLIC_KEY_E_LEN)

#define AES_KEY_LEN 16


typedef struct
{
    U32 u32Num;
    U32 u32Size;
}IMAGE_INFO;

typedef struct
{
  U8 u8SecIdentify[8]; 
  IMAGE_INFO info;
  U8 u8Signature[SIGNATURE_LEN];
}_SUB_SECURE_INFO;

typedef struct
{
    _SUB_SECURE_INFO customer;
    U8 u8RSABootPublicKey[RSA_PUBLIC_KEY_LEN];
    U8 u8RSAUpgradePublicKey[RSA_PUBLIC_KEY_LEN];
    U8 u8RSAImagePublicKey[RSA_PUBLIC_KEY_LEN];
    U8 u8AESBootKey[AES_KEY_LEN];
    U8 u8AESUpgradeKey[AES_KEY_LEN];
    U8 u8MagicID[16];
    U8 crc[4];
}CUSTOMER_KEY_BANK;

typedef struct
{
    U32 u32Nuttx_size;
    U32 u32Start_data_addr;
    U32 u32End_data_addr;
    U32 u32Start_ta_addr;
    U32 u32End_ta_addr;
}ST_BACKUP_HEADER_INFO;

//AESDMA Lite
//#include "MsTypes.h"
//#include "MsDevice.h"

typedef U32                         DRVAESDMA_RESULT;
#define DRVAESDMA_OK                   0x00000000
#define DRVAESDMA_FAIL                 0x00000001
#define DRVAESDMA_INVALID_PARAM        0x00000002
#define DRVAESDMA_FUNC_ERROR           0x00000003
#define DRVAESDMA_MIU_ADDR_ERROR       0x00000004

typedef enum
{
    E_DRVAESDMA_CIPHER_ECB = 0,
    E_DRVAESDMA_CIPHER_CTR,
    E_DRVAESDMA_CIPHER_CBC,
    E_DRVAESDMA_CIPHER_DES_ECB,
    E_DRVAESDMA_CIPHER_DES_CTR,
    E_DRVAESDMA_CIPHER_DES_CBC,
    E_DRVAESDMA_CIPHER_TDES_ECB,
    E_DRVAESDMA_CIPHER_TDES_CTR,
    E_DRVAESDMA_CIPHER_TDES_CBC,
    E_DRVAESDMA_CIPHER_CTS_CBC,
    E_DRVAESDMA_CIPHER_CTS_ECB,
    E_DRVAESDMA_CIPHER_DES_CTS_CBC,
    E_DRVAESDMA_CIPHER_DES_CTS_ECB,
    E_DRVAESDMA_CIPHER_TDES_CTS_CBC,
    E_DRVAESDMA_CIPHER_TDES_CTS_ECB,
} DrvAESDMA_CipherMode;

//-------------------------------------------------------------------------------------------------
/// Initialize AESDMA
/// @ingroup G_AES_INIT
/// @param  u32miu0addr         \b IN: MIU0 Base Address (Unused)
/// @param  u32miu1addr         \b IN: MIU1 Base Address (Unused)
/// @param  u32miunum           \b IN: Numbers of MIU
/// @return DRVAESDMA_OK : Success
/// @return Others : Fail
//-------------------------------------------------------------------------------------------------
DRVAESDMA_RESULT MDrv_AESDMA_Init_Lite(
  U32 u32miu0addr,
  U32 u32miu1addr,
  U32 u32miunum);

//-------------------------------------------------------------------------------------------------
/// Set File in/out address and size to AESDMA
/// @ingroup G_AES_EnDecrypt
/// @param u32FileinAddr \b IN: Source Address
/// @param u32FileInNum \b IN: Size of Data
/// @param u32FileOutSAddr \b IN: Dest Start Address
/// @param u32FileOutEAddr \b IN: Dest End Address
/// @return DRVAESDMA_OK : Success
/// @return Others : Fail
//-------------------------------------------------------------------------------------------------
DRVAESDMA_RESULT MDrv_AESDMA_SetFileInOut_Lite (
  U32 u32FileinAddr,
  U32 u32FileInNum,
  U32 u32FileOutSAddr,
  U32 u32FileOutEAddr
  );

//-------------------------------------------------------------------------------------------------
/// Set Cipher mode and En/Decrypt
/// @ingroup G_AES_EnDecrypt
/// @param  eMode         \b IN: Cipher Mode
/// @param  bDescrypt     \b IN: TRUE: Decrypt, FALSE: Encrypt
/// @return DRVAESDMA_OK : Success
/// @return Others : Fail
//-------------------------------------------------------------------------------------------------
DRVAESDMA_RESULT MDrv_AESDMA_SelEng_Lite (
  DrvAESDMA_CipherMode eMode,
  BOOL bDescrypt);

//-------------------------------------------------------------------------------------------------
/// Trigger AESDMA engine
/// @ingroup G_AES_EnDecrypt
/// @return DRVAESDMA_OK : Success
/// @return Others : Fail
//-------------------------------------------------------------------------------------------------
DRVAESDMA_RESULT MDrv_AESDMA_Start_Lite (
  BOOL bStart);

//-------------------------------------------------------------------------------------------------
/// Reset AESDMA engine
/// @ingroup G_AES_INIT
/// @return DRVAESDMA_OK : Success
/// @return Others : Fail
//-------------------------------------------------------------------------------------------------
DRVAESDMA_RESULT MDrv_AESDMA_Reset_Lite(
  void);

//-------------------------------------------------------------------------------------------------
/// Set a set of even key to AESDMA
/// @ingroup G_AES_EnDecrypt
/// @param pCipherKey \b IN: Key set
/// @return DRVAESDMA_OK : Success
/// @return DRVAESDMA_INVALID_PARAM : Invalid key length
/// @return Others : Fail
//-------------------------------------------------------------------------------------------------
DRVAESDMA_RESULT MDrv_AESDMA_SetKey_Lite (
  U32 *pCipherKey);

//-------------------------------------------------------------------------------------------------
/// Set a set of default IV (even iv) to AESDMA
/// @ingroup G_AES_EnDecrypt
/// @param pCipherKey \b IN: IV
/// @return DRVAESDMA_OK : Success
/// @return Others : Fail
//-------------------------------------------------------------------------------------------------
DRVAESDMA_RESULT MDrv_AESDMA_SetIV_Lite (
  U32 *pInitVector);

//-------------------------------------------------------------------------------------------------
/// Check the status of AESDMA engine
/// @ingroup G_AES_EnDecrypt
/// @return DRVAESDMA_OK : Done
/// @return Others : Not yet
//-------------------------------------------------------------------------------------------------
DRVAESDMA_RESULT MDrv_AESDMA_IsFinished_Lite (
  void);

DRVAESDMA_RESULT MDrv_AESDMA_MOVE (
  DrvAESDMA_CipherMode eMode,
  BOOL bDescrypt,
  U32 u32FileinAddr,
  U32 u32FileInNum,
  U32 u32FileOutSAddr,
  U32 u32FileOutEAddr,
  U32 *pCipherKey,
  U32 *pInitVector,
  BOOL bEfuseKeyEn
  );

#endif

