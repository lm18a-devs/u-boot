#include "partinfo.h"
#include "../src/mmc/inc/api/drv_eMMC.h"

#ifdef LG_BOOT_VERSION
#include <autoversion.h>
#endif

#define DEBUG 1

#define TMP_BUF_SZ 0x1600 //0x400,0x1600
#define PARTINFO_TOTAL sizeof(struct partmap_info)
#define BOOT_NAME		"boot"

extern int _ld_miu_reload_start[];
extern unsigned int boot_selector(void);

//static unsigned char tmp_buf[TMP_BUF_SZ];
static unsigned char *tmp_buf = (unsigned char *) ((unsigned int)_ld_miu_reload_start);
static U32 crc32_table[256];
static struct partmap_info *pmi;
static U32 fTableOk;

#ifdef LG_BOOT_VERSION
#define FIRST_BOOT_VERSION_ADDRESS  0x20100000
#define FIRST_BOOT_VERSION_SIZE     0x10
#ifndef FIRST_BOOT_VERSION_INDEX
#define FIRST_BOOT_VERSION_INDEX    "0.01.00"
#endif

void set_first_boot_version(void)
{
	memcpy((void *)FIRST_BOOT_VERSION_ADDRESS, (void *)FIRST_BOOT_VERSION_INDEX, FIRST_BOOT_VERSION_SIZE);
}
#endif

#if DEBUG
static inline void uart_putc(char ch)
{
	//Wait for Transmit-hold-register empty
	while (!(*(volatile unsigned int*)(0x1F201300 + (5<<3)) & 0x20));

	*(volatile unsigned int*)(0x1F201300) = ch;
}

static inline char uart_getc(void)
{
	//Wait for Transmit-hold-register empty
	while (!(*(volatile unsigned int*)(0x1F201300 + (5<<3)) & 0x20));

	return *(volatile unsigned int*)(0x1F201300);
}

static inline void uart_puts(const char *str)
{
	while (*str) {
		if ('\n' == *str)
			uart_putc('\r');
		uart_putc(*str++);
	}
}
#else
#define uart_putc(str)
#define uart_getc(str)
#define uart_puts(str)
#endif

static int storage_read(U32 offset, U32 len, void* buf)
{
	int ret;
//	uart_puts("storage_read enter\n");
	if (len & 0x1ff)
		len = (len & ~0x1ff) + 0x200;
	//ret = eMMC_ReadData_CIFD((unsigned char *) buf, len, offset >> 9);
	//ret = eMMC_ReadData_MIU((unsigned char *) buf, len, offset >> 9);
	ret = eMMC_ReadData_IMI((unsigned char *) buf, len, offset >> 9);
//	uart_puts("storage_read exit\n");
	return ret;
}

static void init_crc32(void)
{
#define CRC32_POLY	0x04c11db7		/* AUTODIN II, Ethernet, & FDDI */
	int	i, j;
	U32	c;

	for (i = 0; i < 256; ++i) {
		for (c = i << 24, j = 8; j > 0; --j)
			c = c & 0x80000000 ? (c << 1) ^ CRC32_POLY : (c << 1);
		crc32_table[i] = c;
	}
}

static void calc_crc32_part(unsigned int *crc, unsigned char *buf, U32 len)
{
	unsigned char	*p;
//	static	U32		crc = 0xffffffff;
	//printf("%s buf=%p len=%x\n", __FUNCTION__, buf, len);
	if (fTableOk == 0)
	{
		init_crc32();   	/* build table */
		fTableOk = 1;
	}

//	crc = 0xffffffff;       /* preload shift register, per CRC-32 spec */

	if (buf) {
		for (p = buf; len > 0; ++p, --len)
			*crc = (*crc << 8) ^ crc32_table[(*crc >> 24) ^ *p];
	} else {
		for (; len > 0; --len)
			*crc = (*crc << 8) ^ crc32_table[(*crc >> 24) ^ 0];
	}

//	return(crc);
}

static int _get_partition_info(void)
{
	unsigned int crc_partial, crc_offset, *pcrc;
	struct partmap_info *ppartmap_info;
	int i, partial_size, offset, remain_size;
	fTableOk = 0;

	storage_read(DEFAULT_PARTINF_BASE, TMP_BUF_SZ, tmp_buf);

	ppartmap_info = (struct partmap_info*) tmp_buf;
	if (ppartmap_info->npartition > PARTITION_MAX) {
		uart_puts("ppartmap_info->npartition is is bigger than PARTITION_MAX\n");//printf("Error! ppartmap_info->npartition=%d is bigger than PARTITION_MAX=%d\n", ppartmap_info->npartition, PARTITION_MAX);
		return 0;
	}
	partial_size = 80 + (sizeof(struct partition_info) * ppartmap_info->npartition);
	//printf("v2 4. ppartmap_info->npartition=%d, partial_size=%d\n", ppartmap_info->npartition, partial_size);

	i = 1;
	crc_partial = 0xffffffff;
	remain_size = partial_size - i * TMP_BUF_SZ;
	//printf("remain_size=%x crc_partial=%x buf_size=%x\n", remain_size, crc_partial, TMP_BUF_SZ);
	if (remain_size < 0) {
		calc_crc32_part(&crc_partial, tmp_buf, partial_size);
		//printf("remain_size=%x crc_partial=%x buf_size=%x\n", remain_size, crc_partial, TMP_BUF_SZ);
	} else {
		while (1) {
			offset = i * TMP_BUF_SZ;
			calc_crc32_part(&crc_partial, tmp_buf, TMP_BUF_SZ);
			remain_size = partial_size - i * TMP_BUF_SZ;
			//printf("remain_size=%x crc_partial=%x buf_size=%x\n", remain_size, crc_partial, TMP_BUF_SZ);
			if (remain_size < TMP_BUF_SZ) {
				storage_read(DEFAULT_PARTINF_BASE + offset, remain_size, tmp_buf);
				calc_crc32_part(&crc_partial, tmp_buf, remain_size);
				break;
			}
			storage_read(DEFAULT_PARTINF_BASE + offset, TMP_BUF_SZ, tmp_buf);
			i++;
		}
	}

	//printf("4. crc_static: %x, crc_total: %x, crc_partial: %x\n", crc_static, crc_total, crc_partial);
	calc_crc32_part(&crc_partial, NULL, PARTINFO_TOTAL - partial_size);
	crc_offset = PARTINFO_TOTAL + 4;
	i = crc_offset / 0x200;
	remain_size = crc_offset - (i * 0x200);
	if (!remain_size) {
		i--;
		remain_size = 0x200;
	}
	storage_read(DEFAULT_PARTINF_BASE + (i * 0x200), 0x200, tmp_buf);
	pcrc = (unsigned int *)(tmp_buf + remain_size - 4);

	//printf("4. i=%d remain_size=%d crc=%x\n", i, remain_size, *pcrc);
	//printf("4. crc_static: %x, crc_total: %x, crc_partial: %x\n", crc_static, crc_total, crc_partial);
	if (*pcrc == crc_partial)
		return 1;
	else
		return 0;
}

static int _strncmp(const char *cs, const char *ct, int count)
{
	unsigned char c1, c2;

	while (count) {
		c1 = *cs++;
		c2 = *ct++;
		if (c1 != c2)
			return c1 < c2 ? -1 : 1;
		if (!c1)
			break;
		count--;
	}
	return 0;
}

static int _get_partition_idx(const char *name)
{
	struct partition_info *pi;
	unsigned int idx = 0;

	//uart_puts("_get_partition_idx\n");

	do {
		pi = &(pmi->partition[idx]);
		if(pi->used && (_strncmp(name, pi->name, 4) == 0))
			return idx;
	} while (++idx < 5);
	//} while (++idx < pmi->npartition);

	return -1;
}

static unsigned int _get_offset(void)
{
	unsigned int boot_offset, idx;

	storage_read(DEFAULT_PARTINF_BASE, 0x400, tmp_buf);	// reload partition info
	pmi = (struct partmap_info *) tmp_buf;

	idx = _get_partition_idx(BOOT_NAME);
	if (idx < 0) {
		uart_puts("fail to get " BOOT_NAME " partition idx and use DEFAULT_BOOT__1_BASE\n");
		boot_offset = DEFAULT_BOOT__1_BASE;
	} else {
#if 0
		uart_puts("success to get " BOOT_NAME " partition (idx = ");
		uart_putc('0' + idx);
		uart_puts(")\n");
#endif
		boot_offset = pmi->partition[idx].offset;
	}

#if BUILD_RESCUE_BOOT
	if (BUILD_RESCUE_BOOT) {
#else
	if ('~' == uart_getc()) {
#endif
		uart_puts("force to use DEFAULT_BOOT__1_BASE !!!\n");
		return DEFAULT_BOOT__1_BASE;	// for RESCUE_MODE
	} else
		return boot_offset;
}

unsigned int boot_selector(void)
{
	if (_get_partition_info()) {
		//uart_puts("boot_selector get partition info... Pass!\n");
	} else {
		uart_puts("boot_selector get partition info... Fail!\n");
		return DEFAULT_BOOT__1_BASE;	// for RESCUE_MODE
	}

	return _get_offset();
}
