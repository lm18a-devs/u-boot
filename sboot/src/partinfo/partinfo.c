#include <string.h>
#include "partinfo.h"
#include "../src/mmc/inc/api/drv_eMMC.h"
#ifdef LG_BOOT_VERSION
#include <autoversion.h>
#endif

#define DEBUG 0
#define RESCUE_MODE 1

#define SECUREBOOT_NAME	"secureboot"
#define BOOT_NAME		"boot"
#define BOOT_OFFSET		0x200000
#define BOOT_BACKUP_NAME	"bkboot"
#define BOOT_BACKUP_OFFSET	0x0

#define TMP_RAM_ADDR1	(CONFIG_UBOOT_LOADADDR - 0x100000)
#define TMP_RAM_ADDR2	(TMP_RAM_ADDR1 - 0x100000)
#define CRC32_POLY	0x04c11db7		/* AUTODIN II, Ethernet, & FDDI */

#define FIRST_BOOT_VERSION_ADDRESS  0x20100000
#define FIRST_BOOT_VERSION_SIZE     0x10
#ifndef FIRST_BOOT_VERSION_INDEX
#define FIRST_BOOT_VERSION_INDEX    "0.01.00"
#endif

extern unsigned int get_2nd_bootloader_offset(void);
extern void chip_reset(void);
extern void BootRam_AuthenticationFail(void);

//#if DEBUG
static inline void uart_putc(char ch)
{
	//Wait for Transmit-hold-register empty
	while (!(*(volatile unsigned int*)(0x1F201300 + (5<<3)) & 0x20));

	*(volatile unsigned int*)(0x1F201300) = ch;
}
//#else
//#define uart_putc(ch)
//#endif

#if RESCUE_MODE
static inline char uart_getc(void)
{
	//Wait for Transmit-hold-register empty
	while (!(*(volatile unsigned int*)(0x1F201300 + (5<<3)) & 0x20));

	return *(volatile unsigned int*)(0x1F201300);
}
#endif

#if DEBUG
static inline void uart_puts(const char *str)
{
	while (*str) {
		if ('\n' == *str)
			uart_putc('\r');
		uart_putc(*str++);
	}
}
#else
#define uart_puts(str)
#endif

static U32 crc32_table[256];
static struct partmap_info *pmi = NULL;
static struct partmap_info default_partinfo = DEFAULT_PARTMAP_INFO;

static int storage_read(U32 offset, U32 len, void* buf)
{
	return eMMC_ReadData_MIU(buf, len, offset >> 9);
}

static int storage_write(U32 offset, U32 len, void* buf)
{
	return eMMC_WriteData_MIU(buf, len, offset >> 9);
}

static void init_crc32(void)
{
	int	i, j;
	U32	c;

	for (i = 0; i < 256; ++i) {
		for (c = i << 24, j = 8; j > 0; --j)
			c = c & 0x80000000 ? (c << 1) ^ CRC32_POLY : (c << 1);
		crc32_table[i] = c;
	}
}

static U32 calc_crc32(U8 *buf, U32 len)
{
	unsigned char *p;
	U32 crc;
	static U32 fTableOk = 0;

	if (fTableOk == 0) {
		init_crc32();   	/* build table */
		fTableOk = 1;
	}

	crc = 0xffffffff;       /* preload shift register, per CRC-32 spec */

	for (p = buf; len > 0; ++p, --len)
		crc = (crc << 8) ^ crc32_table[(crc >> 24) ^ *p];

	return(crc);
}

int strncmp(const char *cs, const char *ct, size_t count)
{
	unsigned char c1, c2;

	while (count) {
		c1 = *cs++;
		c2 = *ct++;
		if (c1 != c2)
			return c1 < c2 ? -1 : 1;
		if (!c1)
			break;
		count--;
	}
	return 0;
}

void get_partition_info(void)
{
	U32 size = sizeof(struct partmap_info);
	U32 crc = 0, calcrc = 0;
	int crc_offset = 4;
	unsigned char *offset = (unsigned char *) TMP_RAM_ADDR1;

	if (pmi == NULL) {
		memcpy((void *)FIRST_BOOT_VERSION_ADDRESS, (void *)FIRST_BOOT_VERSION_INDEX, FIRST_BOOT_VERSION_SIZE);
		storage_read(DEFAULT_PARTINF_BASE, size + crc_offset, offset);
		memcpy((void*)&crc, (void*)(offset + size), crc_offset);
		uart_puts("Checking partmap_info CRC ... ");
		if ((calcrc = calc_crc32(offset, size)) != crc) {
			uart_puts("fail!\n");
			uart_puts("  use the default partinfo\n");
			pmi = &default_partinfo;
		} else {
			uart_puts("pass!\n");
			pmi = (struct partmap_info *) offset;
		}
	}
}

int get_partition_idx(const char *name)
{
	struct partition_info *pi;
	unsigned int idx = 0;

	do {
		pi = &(pmi->partition[idx]);
		if(pi->used && (strncmp(name, pi->name, 4) == 0))
			return idx;
	} while (++idx < pmi->npartition);

	return -1;
}

struct partition_info * get_used_partition(const char *name)
{
	struct partition_info *pi = NULL;
	unsigned int idx = 0;

	do {
		pi = &(pmi->partition[idx]);
		if(pi->used && (strncmp(name, pi->name, 4) == 0))
			return pi;
	}while(++idx <  pmi->npartition);

	return (struct partition_info*)(NULL);
}

struct partition_info * get_unused_partition(const char *name)
{
	struct partition_info * pi = NULL;
	unsigned int idx = 0;

	do {
		pi = &(pmi->partition[idx]);
		if(!pi->used && (strncmp(name, pi->name, 4) == 0))
			return pi;
	}while(++idx < pmi->npartition);

	return (struct partition_info*)(NULL);
}

int storage_get_partition(const char* name, storage_partition_t* info)
{
	struct partition_info *pi;

	if (((pi = get_used_partition(name)) == NULL) &&
		((pi = get_unused_partition(name)) == NULL))
	{
		return -1;
	}

//	strcpy(info->name, pi->name);
	info->offset = pi->offset;
	info->size = pi->size;
	info->filesize = pi->filesize;
	info->used = pi->used;
	info->valid = pi->valid;

	return 0;
}

unsigned int swap_partition(struct partition_info * pi1, struct partition_info *pi2)
{
	struct partition_info temp;

	temp = *pi1;
	*pi1 = *pi2;
	*pi2 = temp;

	return 0;
}

unsigned int save_partinfo(void)
{
	int ret;
	int crc_offset = 4;
	U32 size;
	unsigned char *buf;
	U32 calcrc;

	uart_puts("save_partinfo\n");

	buf = (unsigned char *) pmi;
	pmi->magic = PARTMAP_MAGIC ;
	size = sizeof(struct partmap_info);
	calcrc = calc_crc32(buf, size);

	buf = (unsigned char *) TMP_RAM_ADDR2;
	//copy partinfo to buf
	memcpy((void *)buf, pmi, size);
	//add crc to buf
	memcpy((void *)(buf + size), &calcrc, crc_offset);

	uart_puts("write to DEFAULT_MAPBAK__BASE ...\n");
	ret = storage_write(DEFAULT_MAPBAK__BASE, size + crc_offset, (struct partition_info *) buf);
	if (ret) {
		uart_puts("emmc write failed..--;;\n");
		return ret;
	}

	uart_puts("write to DEFAULT_PARTINF_BASE ...\n");
	ret = storage_write(DEFAULT_PARTINF_BASE, size + crc_offset, (struct partition_info *) buf);
	if (ret) {
		uart_puts("emmc write failed..--;;\n");
		return ret;
	}

	return ret;
}


extern unsigned int get_2nd_bootloader_offset(void)
{
	unsigned int boot_offset, idx;

#if RESCUE_MODE
#if (BUILD_RESCUE_BOOT == 0)
    if ('~' == uart_getc())
#endif
    {
		//uart_puts("\n\nenter rescue mode ...\n\n");
		uart_putc('\r');
		uart_putc('\n');
		uart_putc('\r');
		uart_putc('\n');
		uart_putc('R');
		uart_putc('E');
		uart_putc('S');
		uart_putc('C');
		uart_putc('U');
		uart_putc('E');
		uart_putc('\r');
		uart_putc('\n');
		uart_putc('\r');
		uart_putc('\n');
		*(volatile unsigned int*)(0x1F206720) = BOOT_OFFSET >> 9;
		return BOOT_OFFSET >> 9;
	}
#endif

	get_partition_info();

	idx = get_partition_idx(BOOT_NAME);
	if(get_partition_idx(SECUREBOOT_NAME) < 0)
	{
		uart_puts("Fail to get " SECUREBOOT_NAME " partition idx\n");
		if (idx < 0) {
			uart_puts("fail to get " BOOT_NAME " partition idx\n");
			uart_puts("try to get " BOOT_BACKUP_NAME " partition idx\n");
			idx = get_partition_idx(BOOT_BACKUP_NAME);
			if (idx < 0) {
				uart_puts("fail to get " BOOT_BACKUP_NAME " partition idx\n");
				while (1);
			} else {
				uart_puts("success to get " BOOT_BACKUP_NAME " partition idx\n");
				boot_offset = pmi->partition[idx].offset + BOOT_BACKUP_OFFSET;
			}
		} else {
			uart_puts("success to get " BOOT_NAME " partition idx\n");
			boot_offset = pmi->partition[idx].offset + BOOT_OFFSET;
		}
	}
	else
	{
		uart_puts("Success to get " SECUREBOOT_NAME " partition idx\n");
		if (idx < 0) {
			uart_puts("fail to get " BOOT_NAME " partition idx\n");
			uart_puts("try to get " BOOT_BACKUP_NAME " partition idx\n");
			idx = get_partition_idx(BOOT_BACKUP_NAME);
			if (idx < 0) {
				uart_puts("fail to get " BOOT_BACKUP_NAME " partition idx\n");
				while (1);
			} else {
				uart_puts("success to get " BOOT_BACKUP_NAME " partition idx\n");
				boot_offset = pmi->partition[idx].offset;
			}
		} else {
			uart_puts("success to get " BOOT_NAME " partition idx\n");
			boot_offset = pmi->partition[idx].offset;
		}
	}
    *(volatile unsigned int*)(0x1F206720) = boot_offset >> 9;
	return boot_offset >> 9;
}

int check_updated_partition(const char* part_name)
{
	struct partition_info	*pi;

	if ((pi = get_used_partition(part_name)) == NULL)
	{
		uart_puts("can't find 'boot' partition\n");
		return -1;
	}

	return (pi->valid == PART_VALID_FLG_UNKNOWN)? 1:0;
}

int set_valid_flag(char* update_list[])
{
	int i = 0;
	struct partition_info	*pi;

	for (; update_list[i] != NULL; i++)
	{
		if ((pi = get_used_partition(update_list[i])) == NULL)
		{
			uart_puts("can't find partition\n");
			return -1;
		}
		pi->valid = PART_VALID_FLG_VALID;
	}
	save_partinfo();
	return 0;
}

int swap_backup_partition(char* update_list[])
{
	int i = 0;
	struct partition_info	*pi;
	struct partition_info	*unpi;

	uart_puts("swap partition!!\n");
	for (; update_list[i] != NULL; i++)
	{
		if ((pi = get_used_partition(update_list[i])) == NULL)
		{
			uart_puts("can't find used partition\n");
			return -1;
		}

		if ((unpi = get_unused_partition(update_list[i])) == NULL)
		{
			uart_puts("can't find backup partition\n");
			return -1;
		}

		pi->valid = PART_VALID_FLG_INVALID;
		pi->used		= NO;
		unpi->used		= YES;

		swap_partition(pi, unpi);
	}

	save_partinfo();
	return 0;
}

int make_updated_part_list(char* update_list[])
{
	int part_idx = 0, update_idx = 0;
	struct partition_info *pi = NULL;
	uart_puts("Make updated partition list\n");
	do
	{
		pi = &(pmi->partition[part_idx]);
		if((pi->name != NULL) && (pi->valid == PART_VALID_FLG_UNKNOWN) \
			&& pi->used && (get_unused_partition(pi->name) != NULL))
		{
			update_list[update_idx++] = pi->name;
		}
	} while(++part_idx < pmi->npartition);

	if(update_idx == 0)
	{
		uart_puts("Fail! There is no updated partition\n");
		return -1;
	}
	else
	{
		uart_puts("Success!\n");
		return 0;
	}
}

#define MAGIC(a,b,c,d)		((a) | (b) << 8 | (c) << 16 | (d) << 24)
#define BACK_MAGIC			MAGIC('B','A','C','K')

static int backup_part_booting(int magic)
{
	unsigned int idx = get_partition_idx("emer");
	storage_partition_t partition;
	char* update_list[UPDATE_PARTITION_MAX] = {NULL, };

	if(make_updated_part_list(update_list)==-1)
		return -1;

	if(storage_get_partition("emer", &partition) < 0)
	{
		uart_puts("emergency parition is not valid !!!\n");
		return -1;
	}
	if(storage_write(pmi->partition[idx].offset, sizeof(int), (void*)&magic) < 0)
	{
		uart_puts("can not write emergency parition!!!\n");
		return -1;
	}

	swap_backup_partition(update_list);

	return 0;
}

void chip_reset(void)
{
	uart_putc('C');
	uart_putc('Y');
	uart_putc('r');
	uart_putc('s');
	uart_putc('t');
	uart_putc('\r');
	uart_putc('\n');

	*((volatile unsigned char *) 0x1F005CB8) = 0x79;
	while(1);
}

extern void BootRam_AuthenticationFail(void)
{
	unsigned int idx = get_partition_idx(BOOT_NAME);

	uart_puts("BootRam_AuthenticationFail\n");

	if(pmi->partition[idx].valid == PART_VALID_FLG_UNKNOWN)
	{
		uart_puts("Try backup partition booting..\n");
		backup_part_booting(BACK_MAGIC);
		chip_reset();
	}
	else
	{
		uart_puts(BOOT_NAME "'s valid is not UNKNOWN!\n");
		while(1);
	}
}

