#include "./partinfo/partinfo.h"
#ifdef CONFIG_MSTAR_MUNICH
#include "./munich/include/datatype.h"
#endif
#ifdef CONFIG_MSTAR_MUSTANG
#include "./mustang/include/datatype.h"
#endif
#ifdef CONFIG_MSTAR_MAXIM
#include "./maxim/include/datatype.h"
#endif
#ifdef CONFIG_MSTAR_M7621
#include "./M7621/include/datatype.h"
#endif
#include "./mmc/inc/api/drv_eMMC.h"
#ifdef CONFIG_MSTAR_CMA
#include "../../u-boot-2011.06/arch/arm/include/asm/mmap_hwopt_512_1024_cma.h"
#else
#include "../../u-boot-2011.06/arch/arm/include/asm/mmap_hwopt_512_1024.h"
#endif
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <malloc.h>
#include <unistd.h>
#include "strBootTEE.h"

#define MIU0_BASE 0x20000000
#define TEE_STR_DEBUG 0
#define FAST_STR_BOOT 1
#define NUTTX_HEADER_OFFSET (0xE6)
//BDMA

#define BDMA_BANK                       0x1f201200
#define DRAM_MIU_0                      0x40
#define DRAM_MIU_1                      0x41
#define MIU_SELECT_OFFESET              0x02*2
#define DATA_SOURCE_OFFESET_LOW         0x04*2
#define DATA_SOURCE_OFFESET_HIGH        0x05*2
#define DATA_DES_OFFESET_LOW            0x06*2
#define DATA_DES_OFFESET_HIGH           0x07*2
#define DATA_LENGTH_OFFESET_LOW         0x08*2
#define DATA_LENGTH_OFFESET_HIGH        0x09*2
static ST_BACKUP_HEADER_INFO BackUpInfo ={0,};
//Memory Process Function
extern int resume;
void _chip_flush_miu_pipe(void)
{
    unsigned short dwReadData = 0;

    //toggle the flush miu pipe fire bit
    *(volatile unsigned short *) (0x1f203114) &= ~(0x0001);
    *(volatile unsigned short *) (0x1f203114) |= 0x0001;

    do
    {
        dwReadData = *(volatile unsigned short *) (0x1f203140);
        dwReadData &= BIT12;  //Check Status of Flush Pipe Finish
        //uart_print("Check Status of Flush Pipe Finish\n");
    } while (dwReadData == 0);
}
void uart_put(char ch)
{
    //Wait for Transmit-hold-register empty
    while (!(*(volatile unsigned int*) (0x1F201300 + (5 << 3)) & 0x20))
        ;

    *(volatile unsigned int*) (0x1F201300) = ch;
}

void uart_print(const char *str)
{
#if(TEE_STR_DEBUG == 1)
    while (*str)
    {
        if ('\n' == *str)
            uart_put('\r');
        uart_put(*str++);
    }
#endif
}
int strn_cmp(unsigned char *cs, const char *ct, size_t count)
{
    unsigned char c1, c2;

    while (count)
    {
        c1 = *cs++;
        c2 = *ct++;
        if (c1 != c2)
            return c1 < c2 ? -1 : 1;
        if (!c1)
            break;
        count--;
    }
    return 0;
}

int _memcmpForSTR(const void* s1, const void* s2, size_t n)
{
    const unsigned char *p1 = s1, *p2 = s2;
    while (n--)
        if (*p1 != *p2)
            return *p1 - *p2;
        else
            p1++, p2++;
    return 0;
}

void _memcpyForSTR(void *dst, const void *src, size_t n)
{
    char *pdst = (char *) dst;
    const char *psrc = (const char *) src;
    for (; n > 0; ++pdst, ++psrc, --n)
    {
        *pdst = *psrc;
    }
    return (dst);
}

#if 0

void *_memsetForSTR(void * s, int c, size_t count)
{
    char *xs = (char *) s;

    while (count--)
    {
        *xs++ = c;
    }

    return s;
}

#endif

BOOL HAL_COPRO_RegWriteByte(U32 u32RegAddr, U8 u8Val)
{
    if (!u32RegAddr)
    {
        uart_print("HAL_COPRO_RegWriteByte reg error!\n");
        return FALSE;
    }
    ((volatile U8*) (0x1f200000))[(u32RegAddr << 1) - (u32RegAddr & 1)] = u8Val;
    return TRUE;
}

BOOL HAL_COPRO_RegWrite2Byte(U32 u32RegAddr, U16 u16Val)
{
    if (!u32RegAddr)
    {
        uart_print("HAL_COPRO_RegWrite2Byte reg error!\n");
        return FALSE;
    }

    ((volatile U16*) (0x1f200000))[u32RegAddr] = u16Val;
    return TRUE;
}

BOOL HAL_CPU_NPM_RegWriteByte(U32 u32RegAddr, U8 u8Val)
{
    if (!u32RegAddr)
    {
        uart_print("HAL_CPU_NPM_RegWriteByte reg error!\n");
        return FALSE;
    }

    ((volatile U8*) (0x1f200000))[(u32RegAddr << 1) - (u32RegAddr & 1)] = u8Val;
    return TRUE;
}

BOOL HAL_CPU_PM_RegWrite2Byte(U32 u32RegAddr, U16 u16Val)
{
    if (!u32RegAddr)
    {
        uart_print("HAL_CPU_PM_RegWrite2Byte reg error!\n");
        return FALSE;
    }

    ((volatile U16*) (0x1f000000))[u32RegAddr] = u16Val;
    return TRUE;
}

U16 HAL_CPU_PM_RegRead2Byte(U32 u32RegAddr)
{
    return ((volatile U16*) (0x1f000000))[u32RegAddr];
}

void HAL_COPRO_Disable(void)
{
    HAL_COPRO_RegWriteByte(R2_REG_STOP, 0x00UL);  // reg_r2_enable = 0x00
}

void MApi_R2_StartFrDRAM(U32 u32_ADR)
{
    U32 base_addr;
    //(1) Disable R2 -> Stop(Reset)
    HAL_COPRO_Disable();
    //(2) Set R2 clock
    // [1:0] = 2'b00
    // [3:2] =  2'b00: 240MHz,  2'b01: 192HMz, 2'b10: 172MHz, 2'b11: Xtal Clock
    // Set to 216 MHz
    HAL_CPU_NPM_RegWriteByte(NPM_REG_CLKGEN1_SECR2, 0x00UL);
    //(3) Set CPU reset base(vector)
    base_addr = u32_ADR >> 16;
    HAL_COPRO_RegWrite2Byte(R2_REG_RST_BASE, base_addr);  // reset vector address 0x0(64K alignment)

//(4)Set MAU Mapping
    //MIU0
    HAL_COPRO_RegWrite2Byte(R2_MAULV1_REG, 0x8800UL);
    //MIU1
    HAL_COPRO_RegWrite2Byte(R2_MAULV2_REG, 0x8400UL);

    //(4) Set RIU   base
    HAL_COPRO_RegWrite2Byte(R2_REG_RIU_BASE, 0xFA00UL);  // RIU  base address
    //(5) Set UART base
    HAL_COPRO_RegWrite2Byte(R2_REG_IO1_BASE, 0xF800UL);  // UART base address

    HAL_COPRO_RegWrite2Byte(R2_REG_SPI_BASE, 0xF900UL);

    HAL_COPRO_RegWrite2Byte(R2_REG_SPI_BASE1, 0xF900UL);

    HAL_COPRO_RegWrite2Byte(R2_REG_DQMEM_BASE, 0xFB00UL);

    HAL_COPRO_RegWrite2Byte(R2_REG_QMEM_MASK_HIGH, 0xFF00UL);
    //(7) Set IO space enable (UART, RIU) with QMEM space disabled
    HAL_COPRO_RegWrite2Byte(R2_REG_SPACE_EN, 0x0003UL);  // io_space_en[3:0]: UART,RIU, qmem_space_en[4] : disable
    //(8) CPU SW reset R2
    // reg_cpux_sw_rstz =  aeon sw rstz(low)
    //*(volatile U32*)(0xBF000000+0x5CA4UL) &= ~BIT13;
    HAL_CPU_PM_RegWrite2Byte(PM_REG_CPUX_SW_RSTZ, HAL_CPU_PM_RegRead2Byte(PM_REG_CPUX_SW_RSTZ) & (~0x2000));
    HAL_CPU_PM_RegWrite2Byte(PM_REG_CPUX_SW_RSTZ, HAL_CPU_PM_RegRead2Byte(PM_REG_CPUX_SW_RSTZ) | (0x2000));

//(1) Enable R2 -> Start
    HAL_COPRO_RegWriteByte(R2_REG_STOP, 0x24UL);  // miu_sw_rst and sdram_boot = 1
    HAL_COPRO_RegWriteByte(R2_REG_STOP, 0x27UL);  // sdram_boot and (miu/r2/r2_rst) =1
}

void TEE_SetUp_MagicID(void)
{
    U32 u32TEEStoreSize = 0x100000;
    U32 u32BootConfigSize = NUTTX_BOOT_CONFIG_SIZE;
    //Write the Magic ID For STR
    U32* u32TEESTRBOOTFLAG = NULL;

    u32TEESTRBOOTFLAG = (U32*)(NFDRM_HW_AES_BUF_ADR + MIU0_BASE - 0xF0);

#ifdef CONFIG_STR
    if(resume)
    {
        *u32TEESTRBOOTFLAG = 0x5A5A5A5A;
        //u32TEESTRBOOTFLAG = 0x5A5A5A5A;
        //_memcpyForSTR((void*)u32Nuttx_End_Addr + 1,(void*)&u32TEESTRBOOTFLAG,sizeof(U32));
        //_chip_flush_miu_pipe();
    }else
#endif
    {
        *u32TEESTRBOOTFLAG = 0x00;
        //u32TEESTRBOOTFLAG = 0x00;
        //_memcpyForSTR((void*)u32Nuttx_End_Addr + 1,(void*)&u32TEESTRBOOTFLAG,sizeof(U32));
        //_chip_flush_miu_pipe();
    }

}

#if(FAST_STR_BOOT == 1)

#define TEESTORESIZE     0X100000
#define STR_FLAG_SUSPEND_FINISH             0xFFFF8888
#define STR_FLAG_RESUME_FINISH              0XFFFF9999
#define TEESTRBOOTFLAG   (U32*)(NFDRM_HW_AES_BUF_ADR + MIU0_BASE - 0xF0)
void delay_ms(unsigned int msDelay)
{
    unsigned int loopsPerMillisecond = (1200000 / 1000) / 3;  //3 clock cycles per loop
    while (msDelay--)
    {
        asm volatile  //this routine waits (approximately) one millisecond
        (
                "mov r3, %[loopsPerMillisecond] \n\t"//load the initial loop counter
                "loop: \n\t"
                "subs r3, #1 \n\t"
                "bne loop \n\t"
                ://empty output list
                : [loopsPerMillisecond] "r" (loopsPerMillisecond)//input to the asm routine
                : "r3", "cc"//clobber list
        );
    }
}

int IsTEE_Resume_Done(U32 address)
{
    char *s = NULL;
    U32 timeOut = 0;
    U32 count = 0;
    timeOut = 600000;
    while (1)
    {
        if ( *((U32*)address) == STR_FLAG_RESUME_FINISH)
        {
            uart_print("TEE Resume OK\n");
            return 0;
        }
        count++;
        if (count == timeOut)
        {
            break;
        }
    }

    return -1;
}

void wait_tee_resume_finish(void)
{
    uart_print("Wait for secure memory done\n");
    if (-1 == IsTEE_Resume_Done(TEESTRBOOTFLAG))
    {
        uart_print("SecureOS resume fail!!\n");
        //MApi_Disable_R2();
    }
    else
    {
        uart_print("SecureOS resume success, set HK Ongoing\n");
    }
}

void FastBootTEE(void)
{
    //Read Header
    _memcpyForSTR((void*)&BackUpInfo,(void*)((NFDRM_NUTTX_MEM_ADR + NUTTX_HEADER_OFFSET +MIU0_BASE)),sizeof(ST_BACKUP_HEADER_INFO));

    U32 u32Nuttx_End_Addr = 0;
    U32 u32NConfigSize = 0x10000;
    U32 u32TEEStoreSize = TEESTORESIZE;
    U32 u32BootConfigSize = NUTTX_BOOT_CONFIG_SIZE;
    U32 u32DataSectionStoreAddr = 0;
    U32 u32DataSectionLength = BackUpInfo.u32End_data_addr - BackUpInfo.u32Start_data_addr;
    U32 u32TASectionLength = BackUpInfo.u32End_ta_addr - BackUpInfo.u32Start_ta_addr;
    U32 u32TEEBackUpSize =  u32BootConfigSize/*default usage*/ + u32NConfigSize/*backup*/ + sizeof(CUSTOMER_KEY_BANK)/*backup*/;

    u32Nuttx_End_Addr = NFDRM_HW_AES_BUF_ADR - u32TEEStoreSize - u32BootConfigSize;

    //ReStore data section
    //u32DataSectionStoreAddr = u32Nuttx_End_Addr + u32TEEBackUpSize +1 + sizeof(U32)/*STR Boot Magic ID*/;//backup data section address
    u32DataSectionStoreAddr = u32Nuttx_End_Addr + u32TEEBackUpSize + sizeof(U32)/*STR Boot Magic ID*/;//backup data section address
#if 0  //No need to restore data section in Real R2 STR mode.
    BDMAMoveData_MIU(0,(void*)u32DataSectionStoreAddr,0,(void*)(BackUpInfo.u32Start_data_addr),u32DataSectionLength);
    //_memcpyForSTR((void*)(BackUpInfo.u32Start_data_addr+MIU0_BASE),(void*)u32DataSectionStoreAddr,u32DataSectionLength);
    //_chip_flush_miu_pipe();
    //Restore ta section

    BDMAMoveData_MIU(0,u32DataSectionStoreAddr + u32DataSectionLength +1,0,(void*)(BackUpInfo.u32Start_ta_addr),u32TASectionLength);
#endif
    //_memcpyForSTR((void*)(BackUpInfo.u32Start_ta_addr+MIU0_BASE),(void*)u32DataSectionStoreAddr + u32DataSectionLength +1,u32TASectionLength);
    //_chip_flush_miu_pipe();
    //Reset TEE
    MApi_R2_StartFrDRAM(NFDRM_NUTTX_MEM_ADR);
    wait_tee_resume_finish();
}

void Write2Byte( unsigned long u32Reg, unsigned short u16Val )
{
    *((volatile unsigned long *)u32Reg)=u16Val;
}
unsigned short Read2Byte( unsigned long u32Reg )
{
    return (unsigned short)(*(volatile unsigned long *)u32Reg);
}


void BDMAMoveData_MIU(U8 srcMiu, unsigned long u32SrcAddr, U8 dstMiu, unsigned long u32DstAddr, unsigned long u32Len)
{
    unsigned int  u16MiuSelect = 0;

    _chip_flush_miu_pipe();

    Write2Byte(BDMA_BANK + 0x00, 0x0000);

    //uart_print("######## BDMAMoveData_MIU 1111 ########\n");
    if (u32Len == 0)
    {
        uart_print("######## BDMAMoveData_MIU error size ########\n");
        return;
    }
    //Only for monaco, because of we have miu2. force bdma switch to miu0/1
    //uart_print("######## BDMAMoveData_MIU 2222 ########\n");
    //Write2Byte(BDMA_BANK + MIU_SOURCE_OFFESET*2, 0x0000);
    //Write2Byte(BDMA_BANK + MIU_DES_OFFESET*2, 0x0000);
    //uart_print("######## BDMAMoveData_MIU 3333 ########\n");

    u16MiuSelect = Read2Byte(BDMA_BANK + MIU_SELECT_OFFESET*2);
    if (srcMiu == 1)
    {
        Write2Byte(BDMA_BANK + MIU_SELECT_OFFESET*2, u16MiuSelect | DRAM_MIU_1);  // miu 1
    }
    else
    {
        Write2Byte(BDMA_BANK + MIU_SELECT_OFFESET*2, u16MiuSelect | DRAM_MIU_0);  // miu 0
    }
    //uart_print("######## BDMAMoveData_MIU 4444 ########\n");

    u16MiuSelect = Read2Byte(BDMA_BANK + MIU_SELECT_OFFESET*2);
    if (dstMiu == 1)
    {
        Write2Byte(BDMA_BANK + MIU_SELECT_OFFESET*2, u16MiuSelect | DRAM_MIU_1 << 8);  // miu 1
    }
    else
    {
        Write2Byte(BDMA_BANK + MIU_SELECT_OFFESET*2, u16MiuSelect | DRAM_MIU_0 << 8);  // miu 0
    }
    //uart_print("######## BDMAMoveData_MIU 5555 ########\n");
    Write2Byte(BDMA_BANK + DATA_SOURCE_OFFESET_LOW*2, u32SrcAddr & 0x0FFFF);
    Write2Byte(BDMA_BANK + DATA_SOURCE_OFFESET_HIGH*2, (u32SrcAddr >> 16) & 0x0FFFF);
    Write2Byte(BDMA_BANK + DATA_DES_OFFESET_LOW*2, u32DstAddr & 0x0FFFF);
    Write2Byte(BDMA_BANK + DATA_DES_OFFESET_HIGH*2, (u32DstAddr >> 16) & 0x0FFFF);
    Write2Byte(BDMA_BANK + DATA_LENGTH_OFFESET_LOW*2, u32Len & 0x0FFFF);
    Write2Byte(BDMA_BANK + DATA_LENGTH_OFFESET_HIGH*2, (u32Len >> 16) & 0x0FFFF);
    Write2Byte(BDMA_BANK, Read2Byte(BDMA_BANK) | BIT0);
    while (!(Read2Byte(BDMA_BANK + 0x01*2*2) & BIT3));
    //uart_print("######## BDMAMoveData_MIU 6666 ########\n");
    //uart_print("######## BDMAMoveData_MIU OK ########\n");

    return;
}


#else
#define BYPASS_VERIFICATION 0
extern void __BDMA_FlashCopy2MIU0(U32 FlashPA, U32 DRAMPA, U32 Len);

static char* pNuttxAlignMallocAddr = NULL;
static char* pNuttxOriMallocAddr = NULL;
static int NuttxAlignSize = 0;
static int g32NuttxFWPhyAddr = 0;
char TeeMagicID[MAGIC_ID_LEN] = "MSTARUPGRADEBIN";
char TeeHandShake[22] = "Secure memory is done";
char HKHandShake[MAGIC_ID_LEN] = "HK is going";
char SecureInfor[8] = "SECURITY";

U32 gu32TeeBootConfigAddr = 0;
U32 gu32TeeHandShakeAddr = 0;
U32 gu32CustomerKeyBankAddr = 0;
U32 u32CurrentAddr = 0;
CUSTOMER_KEY_BANK stCusKey = {0,};
static U32 gu32ChunkHeader[CH_ITEM_LAST] = {0,};
//static char idForContinueMode[]={'S','E','C','U','R','I','T','Y'};
//static char idForInterleaveMode[]={'I','N','T','E','R','L','V','E'};
//static SECURITY_INFO stSecureInfo;
static U8 gu8EMMCBuffer[512] = {0,};
static U8 u8MagicID[16] = { 0x4D, 0x73, 0x74, 0x61, 0x72, 0x2E, 0x4B, 0x65, 0x79, 0x2E, 0x42, 0x61, 0x6E, 0x6B, 0x2E, 0x2E };
static U8 const u8MstarDefRSAImagePublicKey[RSA_PUBLIC_KEY_LEN] = {
0x84, 0x62, 0x76, 0xCC, 0xBD, 0x5A, 0x5A, 0x40, 0x30, 0xC0, 0x96, 0x40, 0x87, 0x28, 0xDB, 0x85, 0xED, 0xED, 0x9F, 0x3E, 0xDE, 0x4E, 0x65, 0xE6, 0x7B, 0x1B, 0x78, 0x17, 0x87, 0x9D, 0xF6, 0x16, 0xC3, 0xD3, 0x27, 0xBC, 0xB4, 0x5A, 0x3, 0x13, 0x35, 0xB0, 0x96, 0x5A, 0x96, 0x41, 0x74, 0x4E, 0xB9, 0xD1,
        0x77, 0x96, 0xF7, 0x8D, 0xE2, 0xE7, 0x15, 0x9, 0x65, 0x9C, 0x46, 0x79, 0xEA, 0xF0, 0x91, 0x67, 0x35, 0xFA, 0x69, 0x4C, 0x83, 0xF7, 0xDC, 0xCF, 0x97, 0x20, 0xF2, 0xA5, 0xBA, 0x72, 0x80, 0x9D, 0x55, 0x79, 0x17, 0xDC, 0x6E, 0x60, 0xA5, 0xE7, 0xE, 0x9E, 0x89, 0x9B, 0x46, 0x6, 0x52, 0xFC, 0x64,
        0x56, 0x2, 0x8, 0x9A, 0x96, 0x41, 0xE2, 0x4F, 0xDB, 0xB6, 0x60, 0xC3, 0x38, 0xDF, 0xF4, 0x97, 0x81, 0x5D, 0x12, 0x2, 0xAE, 0x2B, 0x9F, 0x9, 0x29, 0xB9, 0x9D, 0x51, 0x45, 0xD2, 0x9E, 0x2B, 0xAF, 0x64, 0xCA, 0x9A, 0x6, 0x4E, 0x94, 0x35, 0x67, 0xF7, 0x8E, 0x4, 0x7B, 0x24, 0x38, 0xA0, 0xDF,
        0xE7, 0x5F, 0x1E, 0x6D, 0x29, 0x8E, 0x30, 0xD7, 0x83, 0x8C, 0xB4, 0x41, 0xD2, 0xFD, 0xBF, 0x5B, 0x18, 0xCA, 0x50, 0xD1, 0x27, 0xD1, 0xF6, 0x7D, 0x54, 0x3E, 0x80, 0x5F, 0x20, 0xDC, 0x88, 0x82, 0xCF, 0xBE, 0xE1, 0x46, 0x2A, 0xD6, 0x63, 0xB9, 0xB9, 0x9D, 0xA3, 0xC7, 0x68, 0x3E, 0x48, 0xCE,
        0x6A, 0x62, 0x6F, 0xD1, 0x6A, 0xC3, 0xB6, 0xDE, 0xF3, 0x39, 0x25, 0xEC, 0xF6, 0x79, 0x20, 0xB5, 0xF2, 0x30, 0x25, 0x6E, 0x99, 0xAE, 0x39, 0x56, 0xDA, 0xAF, 0x83, 0xD6, 0xB8, 0x49, 0x15, 0x78, 0x81, 0xCC, 0x3C, 0x4F, 0x66, 0x5D, 0x95, 0x7E, 0x31, 0xD4, 0x37, 0x2A, 0xBE, 0xFC, 0xB4, 0x66,
        0xF8, 0x91, 0x1, 0xA, 0x53, 0x3C, 0x3C, 0xAB, 0x86, 0xB9, 0x80, 0xB7, 0x0, 0x1, 0x0, 0x1 };


void delay_ms(unsigned int msDelay)
{
    unsigned int loopsPerMillisecond = (1200000 / 1000) / 3;  //3 clock cycles per loop
    while (msDelay--)
    {
        asm volatile  //this routine waits (approximately) one millisecond
        (
                "mov r3, %[loopsPerMillisecond] \n\t"//load the initial loop counter
                "loop: \n\t"
                "subs r3, #1 \n\t"
                "bne loop \n\t"
                ://empty output list
                : [loopsPerMillisecond] "r" (loopsPerMillisecond)//input to the asm routine
                : "r3", "cc"//clobber list
        );
    }
}



#if 0 //L2 Cache Init
#define L2X0_CACHE_ID           0x000
#define L2X0_CACHE_ID_PART_MASK   (0xf << 6)
#define L2X0_CACHE_ID_PART_L210   (1 << 6)
#define L2X0_CACHE_ID_PART_L310   (3 << 6)
#define L2X0_CACHE_TYPE         0x004
#define L2X0_CTRL           0x100
#define L2X0_AUX_CTRL           0x104
#define L2X0_TAG_LATENCY_CTRL       0x108
#define L2X0_DATA_LATENCY_CTRL      0x10C
#define L2X0_EVENT_CNT_CTRL     0x200
#define L2X0_EVENT_CNT1_CFG     0x204
#define L2X0_EVENT_CNT0_CFG     0x208
#define L2X0_EVENT_CNT1_VAL     0x20C
#define L2X0_EVENT_CNT0_VAL     0x210
#define L2X0_INTR_MASK          0x214
#define L2X0_MASKED_INTR_STAT       0x218
#define L2X0_RAW_INTR_STAT      0x21C
#define L2X0_INTR_CLEAR         0x220
#define L2X0_CACHE_SYNC         0x730
#define L2X0_INV_LINE_PA        0x770
#define L2X0_INV_WAY            0x77C
#define L2X0_CLEAN_LINE_PA      0x7B0
#define L2X0_CLEAN_LINE_IDX     0x7B8
#define L2X0_CLEAN_WAY          0x7BC
#define L2X0_CLEAN_INV_LINE_PA      0x7F0
#define L2X0_CLEAN_INV_LINE_IDX     0x7F8
#define L2X0_CLEAN_INV_WAY      0x7FC
#define L2X0_LOCKDOWN_WAY_D     0x900
#define L2X0_LOCKDOWN_WAY_I     0x904
#define L2X0_TEST_OPERATION     0xF00
#define L2X0_LINE_DATA          0xF10
#define L2X0_LINE_TAG           0xF30
#define L2X0_DEBUG_CTRL         0xF40

#define readl_relaxed(addr) *(volatile unsigned long*)(addr)
#define writel_relaxed(value,addr) *(volatile unsigned long*)(addr) = value
#define L2_CACHE_PHYS         0x15000000
#define L2_CACHE_OFFSET       0x00000000
#define L2_CACHE_SIZE         0x1000
#define L2_CACHE_VIRT         (L2_CACHE_PHYS + L2_CACHE_OFFSET)
#define L2_CACHE_p2v(pa)      ((pa) + L2_CACHE_OFFSET)
#define L2_CACHE_v2p(va)      ((va) - L2_CACHE_OFFSET)
#define L2_CACHE_ADDRESS(x)   L2_CACHE_p2v(x)

/* Constants of AMBER3 L2 Linefill & Prefetch */
#define L2_LINEFILL             1
#define L2_PREFETCH             1
#define PREFETCH_CTL_REG        0xF60

#define DOUBLE_LINEFILL_ENABLE  0x40000000  //[30]
#define I_PREFETCH_ENABLE       0x20000000  //[29]
#define D_PREFETCH_ENABLE       0x10000000  //[28]
#define LINEFILL_WRAP_DISABLE   0x08000000  //[27]
#define PREFETCH_OFFSET         0x00000000  //[4:0] ,only support 0-7,15,23,31

#define L2_CACHE_write(v,a)         (*(volatile unsigned int *)L2_CACHE_ADDRESS(a) = (v))
#define L2_CACHE_read(a)            (*(volatile unsigned int *)L2_CACHE_ADDRESS(a))
#define L2X0_TAG_LATENCY_CTRL       0x108
#define L2X0_DATA_LATENCY_CTRL      0x10C

static void *l2x0_base;
static unsigned long l2x0_way_mask; /* Bitmask of active ways */
static inline void cache_wait_way(void *reg, unsigned long mask)
{
    /* wait for cache operation by line or way to complete */
    while (readl_relaxed(reg) & mask)
    ;
}
static inline void cache_sync(void)
{
    void *base = l2x0_base;
    writel_relaxed(0, (unsigned long)base + L2X0_CACHE_SYNC);
}
static inline void l2x0_inv_all(void)
{
    /* invalidate all ways */
    writel_relaxed(l2x0_way_mask, (unsigned long)l2x0_base + L2X0_INV_WAY);
    cache_wait_way((void *) ((unsigned long) l2x0_base + L2X0_INV_WAY), l2x0_way_mask);
    cache_sync();
}
void l2x0_init(void *base, unsigned long aux_val, unsigned long aux_mask)
{
    unsigned long aux;
    unsigned long cache_id;
    int ways;
    const char *type;

    l2x0_base = base;

    cache_id = readl_relaxed((unsigned long)l2x0_base + L2X0_CACHE_ID);
    aux = readl_relaxed((unsigned long)l2x0_base + L2X0_AUX_CTRL);

    aux &= aux_mask;
    aux |= aux_val;

    /* Determine the number of ways */
    switch (cache_id & L2X0_CACHE_ID_PART_MASK)
    {
        case L2X0_CACHE_ID_PART_L310:
        if (aux & (1 << 16))
        ways = 16;
        else
        ways = 8;
        type = "L310";
        break;
        case L2X0_CACHE_ID_PART_L210:
        ways = (aux >> 13) & 0xf;
        type = "L210";
        break;
        default:
        /* Assume unknown chips have 8 ways */
        ways = 8;
        type = "L2x0 series";
        break;
    }

    l2x0_way_mask = (1 << ways) - 1;

    /*
     * Check if l2x0 controller is already enabled.
     * If you are booting from non-secure mode
     * accessing the below registers will fault.
     */
    if (!(readl_relaxed((unsigned long)l2x0_base + L2X0_CTRL) & 1))
    {

        /* l2x0 controller is disabled */
        writel_relaxed(aux, (unsigned long)l2x0_base + L2X0_AUX_CTRL);

        l2x0_inv_all();

        /* enable L2X0 */
        writel_relaxed(1, (unsigned long)l2x0_base + L2X0_CTRL);
    }
}

void l2_cache_init(void)
{
    unsigned int val = 0;
    void * l2x0_base = (void *) (L2_CACHE_ADDRESS(L2_CACHE_PHYS));

    /*L2_LINEFILL*/
    val = L2_CACHE_read(L2_CACHE_PHYS + PREFETCH_CTL_REG);

    L2_CACHE_write(( val | DOUBLE_LINEFILL_ENABLE | LINEFILL_WRAP_DISABLE ), L2_CACHE_PHYS + PREFETCH_CTL_REG);

    /*L2_PREFETCH*/
    val = L2_CACHE_read(L2_CACHE_PHYS + PREFETCH_CTL_REG);
    L2_CACHE_write(( val | I_PREFETCH_ENABLE | D_PREFETCH_ENABLE | PREFETCH_OFFSET ), L2_CACHE_PHYS + PREFETCH_CTL_REG);

    /* set RAM latencies to 2 cycle for this core tile. */
    writel_relaxed(0x111, (unsigned long)l2x0_base + L2X0_TAG_LATENCY_CTRL);
    writel_relaxed(0x111, (unsigned long)l2x0_base + L2X0_DATA_LATENCY_CTRL);

    l2x0_init(l2x0_base, 0x00400000, 0xfe0fffff);
}
#endif
#if 0//Dcache Flush
void flush_cache(unsigned long addr, unsigned long len)
{
    extern void sboot_v7_dma_flush_range(const void *, const void *);
    asm("adds    r1, r1, r0");
    asm("blx sboot_v7_dma_flush_range");
    _chip_flush_miu_pipe();

    return;
}
extern void v7_flush_cache_all(void);
void flush_cache_all(void)
{
    v7_flush_cache_all();

    _chip_flush_miu_pipe();
}

static void cache_flush(void)
{
    //Mstar End
    asm ("mcr p15, 0, %0, c7, c5, 0": :"r" (0));
}
#endif

#if 0
//BDMA
#define RIU_MAP                         0x1F000000
#define LOCAL_BDMA_REG_BASE             0x900
#define RIU                             ((unsigned short volatile *) RIU_MAP)
#define RIU8                            ((unsigned char volatile *) RIU_MAP)
#define BDMA_BANK 0x1f201200

void WriteByte(U32 u32RegAddr, U8 u8Val)
{
    ((volatile U8*) (RIU_MAP))[(u32RegAddr << 1) - (u32RegAddr & 1)] = u8Val;
}

void Write2Byte(U32 u32RegAddr, U16 u16Val)
{
    WriteByte(u32RegAddr, (U8) u16Val);
    WriteByte((u32RegAddr + 1), (U8) (u16Val >> 8));
}

U16 Read2Byte(U32 u32RegAddr)
{
    U16 u16Value = 0;
    U16 u8Value = 0;
    u16Value = (U16) ((volatile U8*) (RIU_MAP))[(u32RegAddr << 1) - (u32RegAddr & 1)];
    u32RegAddr++;
    u8Value = ((volatile U8*) (RIU_MAP))[(u32RegAddr << 1) - (u32RegAddr & 1)];
    u16Value = u16Value | (u8Value << 8);
    return u16Value;
}

void BDMAMoveData_MIU(U8 srcMiu, unsigned long u32SrcAddr, U8 dstMiu, unsigned long u32DstAddr, unsigned long u32Len)
{
    unsigned long u32CRC = 0;
    Write2Byte(BDMA_BANK + 0x0c * 2, 0);
    Write2Byte(BDMA_BANK + 0x0d * 2, 0);
    uart_print("######## BDMAMoveData_MIU 1111 ########\n");
    if (u32Len == 0)
    {
        uart_print("######## error size ########\n");
    }
    //Only for monaco, because of we have miu2. force bdma switch to miu0/1
    uart_print("######## BDMAMoveData_MIU 2222 ########\n");
    Write2Byte(BDMA_BANK + 0x03 * 2, 0x4000);
    uart_print("######## BDMAMoveData_MIU 3333 ########\n");
    if (srcMiu == 1)
    {
        WriteByte(BDMA_BANK + 0x02 * 2, 0x41);  // miu 1
    }
    else if (srcMiu == 0)
    {
        WriteByte(BDMA_BANK + 0x02 * 2, 0x40);  // miu 0
    }
    else
    {
        WriteByte(BDMA_BANK + 0x02 * 2, 0x40);  // miu 0
    }
    uart_print("######## BDMAMoveData_MIU 4444 ########\n");
    if (dstMiu == 1)
    {
        WriteByte(BDMA_BANK + 0x02 * 2 + 1, 0x41);  // miu 1
    }
    else if (dstMiu == 0)
    {
        WriteByte(BDMA_BANK + 0x02 * 2 + 1, 0x40);  // miu 0
    }
    else
    {
        WriteByte(BDMA_BANK + 0x02 * 2 + 1, 0x40);  // miu 0
    }
    uart_print("######## BDMAMoveData_MIU 5555 ########\n");
    Write2Byte(BDMA_BANK + 0x04 * 2, u32SrcAddr & 0x0FFFF);
    Write2Byte(BDMA_BANK + 0x05 * 2, (u32SrcAddr >> 16) & 0x0FFFF);
    Write2Byte(BDMA_BANK + 0x06 * 2, u32DstAddr & 0x0FFFF);
    Write2Byte(BDMA_BANK + 0x07 * 2, (u32DstAddr >> 16) & 0x0FFFF);
    Write2Byte(BDMA_BANK + 0x08 * 2, u32Len & 0x0FFFF);
    Write2Byte(BDMA_BANK + 0x09 * 2, (u32Len >> 16) & 0x0FFFF);
    Write2Byte(BDMA_BANK + 0x0a * 2, 0x0);
    Write2Byte(BDMA_BANK + 0x0b * 2, 0x0);
    Write2Byte(BDMA_BANK, Read2Byte(BDMA_BANK) | BIT0);
    while (0 == (Read2Byte(BDMA_BANK + 0x01 * 2) & BIT3))
    ;
    uart_print("######## BDMAMoveData_MIU 6666 ########\n");
    return;
}
#endif

int get_chunk_header()
{
#if 0
    uart_print("IN\n");

    if (0!=eMMC_ReadData_MIU(gu32ChunkHeader,(sizeof(gu32ChunkHeader)),0x1000))
    {
        uart_print("fail\n");
        return -1;
    }
#else

    struct partition_info *mpi = NULL;

    unsigned int read_size = 0;

    get_partition_info();

    mpi = get_used_partition("boot");

    //printf("partition Offset=%d\n",(U32)mpi->offset);
    if (mpi == NULL)
    {
        uart_print("failed to get partition:boot\n");
        return -1;
    }

    if (mpi->valid == NO)
    {
        uart_print("Partition is Not valid! => Skip!");
        return -1;
    }

    if(!mpi->filesize)
    {
        uart_print("File image size is Zero, Using partition size!!");
        read_size = mpi->size;
    }
    else
    {
        read_size = mpi->filesize;
    }
    //uart_print("read_size=0x%x\n",read_size);

    //printf("mpi->offse=0x%x\n",(U32)mpi->offset);
    //printf("offset=0x%x\n",offset);
    //printf("size=0x%x\n",size);
    uart_print("_readImage1");
    if(0== eMMC_ReadData_MIU((void *)gu8EMMCBuffer, sizeof(gu32ChunkHeader), ((U32)mpi->offset) >> 9))
    {
        _memcpyForSTR((U8*)gu32ChunkHeader,(U8*)gu8EMMCBuffer,sizeof(gu32ChunkHeader));
        return 0;
    }
    else
    {
        uart_print("get_chunk_header storage_read fail\n");
        return -1;
    }

#endif
    return 0;
}


#if 0
void DeAllocateNuttxbuffer(void* BufAddr)
{
    if(BufAddr!=NULL)
    {
        free(BufAddr);
        BufAddr=NULL;
    }
    return;
}
#endif
int _LoadCustomerKeyBank(U32 u32CusKeyOffset, U32 u32CusKeySize, U32 u32HwAesVABufAddr)
{
    uart_print("_LoadCustomerKeyBank IN\n");
    if (0 != eMMC_ReadData_MIU((void*) u32HwAesVABufAddr, u32CusKeySize, u32CusKeyOffset))
    {
        uart_print("eMMC_ReadData_MIU Read Customer Key Bank Fail\n");
        return -1;

    }

#if(BYPASS_VERIFICATION == 0)//use Efuse key
#if 0
    MDrv_AESDMA_MOVE(
            E_DRVAESDMA_CIPHER_ECB,
            TRUE,  //false: encrypt, true:decrypt
            gu32CustomerKeyBankAddr, u32CusKeySize,
            gu32CustomerKeyBankAddr, u32CusKeySize,
            NULL,
            NULL,
            TRUE
            );
#endif
#endif
#if(TEE_STR_DEBUG == 1)    //53 45 43 55 52 49 54 59 01 00 00 00 40 03 00 00
      if (0 != _memcmpForSTR((void*) u32HwAesVABufAddr, SecureInfor, sizeof(SecureInfor)))
      {
          uart_print("MDrv_AESDMA_MOVE Decrypted Customer Key bank Fail\n");
          return -1;
      }
      else
      {
          uart_print("MDrv_AESDMA_MOVE Decrypted Customer Key bank Sucess!!!!\n");
      }
#endif

    _memcpyForSTR(&stCusKey, (void*) u32HwAesVABufAddr, u32CusKeySize);

#if(TEE_STR_DEBUG == 1)

    if (0 != _memcmpForSTR(stCusKey.u8RSAImagePublicKey, u8MstarDefRSAImagePublicKey, RSA_PUBLIC_KEY_LEN) == 0)
    {
        uart_print("Mstar's default keys inside\n");
    }
    else
    {
        uart_print("Customer's keys inside\n");
    }
#endif
    uart_print("OK\n");

    return 0;
}

int CopyCustomerKeyBank(U32 u32DramAddr)
{
    uart_print("CopyCustomerKeyBank IN\n");

    CUSTOMER_KEY_BANK *CustomerKeyBank = (CUSTOMER_KEY_BANK*) gu32CustomerKeyBankAddr;

    if (0 != LoadCustomerKeyBank())
    {
        uart_print("LoadCustomerKeyBank Fail\n");
    }
#if(TEE_STR_DEBUG == 1)
    if (0 != _memcmpForSTR(u8MagicID, stCusKey.u8MagicID, sizeof(u8MagicID)))
    {
        uart_print("Customer key bank is not ready\n");
        return -1;
    }

    if (0 != _memcmpForSTR(u8MagicID, CustomerKeyBank->u8MagicID, sizeof(u8MagicID)))
    {
        uart_print("Customer key bank(PA) is not ready\n");
        return -1;
    }

    if (0 != _memcmpForSTR((unsigned char*) gu32CustomerKeyBankAddr, (unsigned char*) &stCusKey, sizeof(CUSTOMER_KEY_BANK)))
    {
        uart_print("Customer key bank PA and Global stCusKey not Match\n");
    }
#endif
    uart_print("CopyCustomerKeyBank OK\n");
    return 0;
}

int LoadCustomerKeyBank(void)
{
    U32 u32CusKeyOffset = 0x00;
    U32 u32HwAesBufLen = 0;
    U8 *pu8Buf = 0;
    U8 *pu8TempBuf = 0;

    uart_print("LoadCustomerKeyBank IN\n");

    pu8Buf = (U8*) (gu32CustomerKeyBankAddr);  //malloc(sizeof(stCusKey) * 2);
    if (pu8Buf == NULL)
    {
        uart_print("malloc for customer key bank fail\n");
        return -1;
    }

    pu8TempBuf = (U8 *) ((unsigned int) pu8Buf + (32 - ((unsigned int) pu8Buf % 32)));

    get_chunk_header();

    //u32CusKeyOffset = gu32ChunkHeader[CH_CUSTOMER_KEY_BANK_ROM_OFFSET];/*/0x200512*//*) + 0x2C00eMMC Block*/;
    u32CusKeyOffset = (gu32ChunkHeader[CH_CUSTOMER_KEY_BANK_ROM_OFFSET]/0x200/*512*/) + 0x2C00/*eMMC Block*/;
    ///Warning: "_LoadCustomerKeyBank" must behind of "SignatureLoad".
    ///why??
    ///Becase we have a simple secure booting solution on MIPS booting mode.
    ///In this case, we store both the sensitive keys and signatures in "SECURITY_INFO" in SPI.
    ///So, we have to do "SignatureLoad" first, and then copy the sensitive keys from stSecureInfo.
    if (_LoadCustomerKeyBank(u32CusKeyOffset, sizeof(CUSTOMER_KEY_BANK), (U32) pu8Buf) == -1)
    {
        //free(pu8Buf);
        uart_print("_LoadCustomerKeyBank fail\n");
        return -1;
    }

    uart_print("OK\n");
    return 0;
}
extern void MDrv_AESDMA_SecureAuthen(U32 u32SHAInBuf, U32 u32Size, U32 u32RSAInBuf, U32 u32SHAOutBuf, U32 u32RSAOutBuf, U32 u32SramSel);
#define PAGE_SIZE 4096
#define MAX_FRAG_NUM 32
struct APP_FULL_IMAGE_SIGNATURE
{
    unsigned char signature[SIGNATURE_LEN];
};
struct APP_PARTIAL_IMAGE_SIGNATURE
{
    unsigned char signature[MAX_FRAG_NUM][SIGNATURE_LEN];
}__attribute__((aligned(PAGE_SIZE)));

#define APP_SIGN_TOTAL_LEN (sizeof(struct APP_PARTIAL_IMAGE_SIGNATURE)+sizeof(struct APP_FULL_IMAGE_SIGNATURE)+sizeof(struct APP_SIGNATURE_HEADER))
#define APP_SIGN_HEADER_OFFSET (offset+image_size-APP_SIGN_TOTAL_LEN)
#define APP_PARTIAL_SIG_OFFSET(i) (APP_SIGN_HEADER_OFFSET+sizeof(struct APP_SIGNATURE_HEADER)+i*SIGNATURE_LEN)
#define APP_FULL_SIG_OFFSET (offset+image_size-SIGNATURE_LEN)

int sb_get_signature(unsigned long offset, unsigned long size, unsigned char *buf)
{
    int ret = 0;

    ret = eMMC_ReadData_MIU(buf, offset >> 9, size);  //>> 9 need check

    return ret;
}

int sb_get_verify_data(unsigned long offset, unsigned long image_size, unsigned int num, unsigned int frag_num, unsigned int frag_unit_size, unsigned char *buf)
{
    unsigned char partial = 0;
    unsigned int i = 0;
    unsigned group_size = 0;

    if ((frag_unit_size != 0) && (frag_num != 0))
    {
        partial = 1;
    }

    if (partial == 0)
    {
        eMMC_ReadData_MIU(offset, image_size, buf);
    }
    else
    {
        group_size = frag_num * frag_unit_size;
        while (image_size)
        {
            eMMC_ReadData_MIU(buf, (offset + (i * group_size) + num * frag_unit_size) >> 9, frag_unit_size);  //>> 9 need check
            i++;
            image_size -= frag_unit_size;
            buf += frag_unit_size;
        }
    }

    return 0;
}

int verify_tzfw(U32 ulImageSize)
{
    int ret = 0;
    U8 *signature = NULL;
    U8 *pBuf = NULL;

    if (0 != sb_get_signature((0x7c580000 + 0x100000), SIGNATURE_LEN, signature))
    {
        ret = -1;
        return ret;
    }

    if (0 != sb_get_verify_data((0x7c580000 + 0x100000) - SIGNATURE_LEN, ulImageSize, 0, 0, 0, pBuf))
    {
        ret = -1;
        return ret;
    }

    return ret;
}

int _readTee(unsigned int address, unsigned int size)
{
    return _readImage(address, 0x100000, size);
}

int _readTeeFW(unsigned int address, unsigned int offset, unsigned int size)
{
    address -= offset;

    return _readImage(address, 0x100000, size);
}

int _readNuttxConfig(unsigned int address, unsigned int size)
{
    int nuttx_index = EN_NUTTX_CONFIG_1024;  //hardcode first, it need to sync with Henry. how to get the mmap type in sboot

    int ret = 0;

    if (EN_NUTTX_CONFIG_512 == nuttx_index)
    {
        ret = _readImage(address, 0, size);
    }
    else if (EN_NUTTX_CONFIG_768 == nuttx_index)
    {
        ret = _readImage(address, 0x30000, size);
    }
    else if (EN_NUTTX_CONFIG_1024 == nuttx_index)
    {
        ret = _readImage(address, 0x60000, size);
    }

    return ret;
}

int _readImage(unsigned int address, unsigned int offset, unsigned int size)
{
    struct partition_info *mpi = NULL;

    unsigned int read_size = 0;
#if 1
    get_partition_info();

    mpi = get_used_partition("tzfw");

    //printf("partition Offset=%d\n",(U32)mpi->offset);
    if (mpi == NULL)
    {
        uart_print("failed to get partition:tzfw\n");
        return -1;
    }

    if (mpi->valid == NO)
    {
        uart_print("Partition is Not valid! => Skip!");
        return -1;
    }

    if(!mpi->filesize)
    {
        uart_print("File image size is Zero, Using partition size!!");
        read_size = mpi->size;
    }
    else
    {
        read_size = mpi->filesize;
    }
    //uart_print("read_size=0x%x\n",read_size);

    //printf("mpi->offse=0x%x\n",(U32)mpi->offset);
    //printf("offset=0x%x\n",offset);
    //printf("size=0x%x\n",size);
    uart_print("_readImage1");
    if(0== eMMC_ReadData_MIU((void *)address, size, ((U32)mpi->offset+offset) >> 9))
#else
    if (0 == eMMC_ReadData_MIU((void *) address, size, (0x7c580000 + offset) >> 9))  //Hard Code For STR
#endif
    {
        return 0;
    }
    else
    {
        //uart_print("storage_read fail\n");
        return -1;
    }
}

int CalculateNuttxAlignSize(char* pu8R2HeaderAddr)
{
    ST_TEE_HEADER* pstTeeHd = NULL;
    U32 NuttxSize = 0;
    pstTeeHd = (ST_TEE_HEADER*) pu8R2HeaderAddr;
    if (0 == strn_cmp(pstTeeHd->ucMagicID, TeeMagicID, 15))
    {
        // printf("pstTeeHd->_sthd.uitee_image_size =%x \n",pstTeeHd->_sthd.uitee_image_size);
        // uart_print(" pstTeeHd->ucMagicID,TeeMagicID,16 \n");

        if (pstTeeHd->_sthd.uitee_image_size == 0)
        {
            uart_print(" error size\n");
            return 0;
        }

        if (pstTeeHd->_sthd.uitee_image_size % ALIGN_BYTES)
        {
            NuttxSize = (((pstTeeHd->_sthd.uitee_image_size + ALIGN_BYTES) >> FOUR_BIT_SHIFT) << FOUR_BIT_SHIFT) + NUTTX_HEADER_LEN;
        }
        else
        {
            NuttxSize = pstTeeHd->_sthd.uitee_image_size + NUTTX_HEADER_LEN;
        }
    }
    else
    {
        uart_print(" pstTeeHd->ucMagicID,TeeMagicID doent exist \n");
    }

    return NuttxSize;
}

void* AllocateNuttxbuffer(int BufSize)
{
    U8* pu8R2MallocAddr = NULL;
    pu8R2MallocAddr = (U32*) (HW_SECURE_BUFFER_ADR + MIU0_BASE);
    //_memsetForSTR(pu8R2MallocAddr, 0, BufSize);
    return pu8R2MallocAddr;
}
int supportTeeBootConfig(U32 u32NuttxVirtAddr)
{
    ST_TEE_HEADER* pstTeeHd = NULL;

    //printf("u32NuttxVirtAddr : %x \n",u32NuttxVirtAddr);
    //UBOOT_DUMP(u32NuttxVirtAddr, sizeof(ST_TEE_HEADER));
    pstTeeHd = (ST_TEE_HEADER*) u32NuttxVirtAddr;

    if (pstTeeHd->bootConfigFlag[0] == 0x11)
    {
        uart_print("The tee.bin support the boot config function \n");
        return 0;
    }
    else
    {
        uart_print("The tee.bin doesn't support the boot config function\n");
        return -1;
    }

}

U32 MoveNuttx2Dest(U32 NuttxVirtMallocAddr, U32 NuttxSize)
{
    ST_TEE_HEADER* pstTeeHd = NULL;
    unsigned long ulTempAddr = 0;
    unsigned long ulImageSize = 0;
    unsigned long ulHeaderLen = 0;
    U32 u32NuttxPhyAddr = 0;
    U32 NuttxPhyMallocAddr = 0;

    pstTeeHd = (ST_TEE_HEADER*) NuttxVirtMallocAddr;

    uart_print("*****compare magic id*****\n");
    if (0 == strn_cmp(pstTeeHd->ucMagicID, TeeMagicID, 15))
    {
        uart_print("*****compare magic id Sucess*****\n");
        if ((pstTeeHd->_sthd.uitee_start_paddr == 0) && (pstTeeHd->_sthd.uitee_start_vaddr == 0) && (pstTeeHd->_sthd.uitee_image_size == 0))
        {
            uart_print("header info in wrong, please check again");
            return 0;
        }
        ulTempAddr = pstTeeHd->_sthd.uitee_start_paddr;
        ulImageSize = pstTeeHd->_sthd.uitee_image_size;
        ulHeaderLen = pstTeeHd->_sthd.uitee_header_len;
        u32NuttxPhyAddr = NuttxVirtMallocAddr;  //PA
        _readTeeFW(ulTempAddr, ulHeaderLen, ulImageSize);
#if(BYPASS_VERIFICATION == 0)//use Efuse key
        //verify_tzfw(ulImageSize); -->Hank need to check
#endif
        u32NuttxPhyAddr = ulTempAddr;
    }
    else
    {
        uart_print("*****Without Header*****\n");
        NuttxPhyMallocAddr = NuttxVirtMallocAddr;
        _memcpyForSTR((void*) NuttxPhyMallocAddr, (void*) u32NuttxPhyAddr, NuttxSize);
#if(TEE_STR_DEBUG == 1)
        if (0 != _memcmpForSTR((void*) NuttxPhyMallocAddr, (void*) u32NuttxPhyAddr, ulImageSize))
        {
            uart_print("MoveNuttx2Dest Memory not match\n");
        }
#endif
    }
    return u32NuttxPhyAddr;
}

U32 get_hw_aes_addr_from_tee_hd(U32 u32NuttxVirtAddr)
{
    ST_TEE_HEADER* pstTeeHd = NULL;

    pstTeeHd = (ST_TEE_HEADER*) u32NuttxVirtAddr;

    return pstTeeHd->_sthd.uihw_aes_paddr;
}

U32 get_hw_aes_len_from_tee_hd(U32 u32NuttxVirtAddr)
{
    ST_TEE_HEADER* pstTeeHd = NULL;
    pstTeeHd = (ST_TEE_HEADER*) u32NuttxVirtAddr;
    return pstTeeHd->_sthd.uihw_aes_buffer_len;
}

int LoadNConfig(U32 u32NConfigBufAddr)
{
    if (NConfigLoad(u32NConfigBufAddr) != EN_SUCCESS)
    {
        uart_print("Load signature fail\n");
        return -1;
    }
    return 0;
}

int NConfigLoad(U32 u32NConfigBufAddr)
{
    int ret = -1;
    unsigned int u32NConfigSize = 0;

#define NCONFIG_RAW_DATA_SIZE (u32NConfigSize-4)

#if 0
    ret = raw_io_config(FLASH_DEFAULT_TARGET,FLASH_DEFAULT_PARTITION,FLASH_DEFAULT_VOLUME);
    if(ret != 0)
    {
        uart_print("raw_io_config setting fail!\n");
        return -1;
    }

    ret = get_nconfig_offset(&u32NConfigOffset,&u32NConfigBakOffset);
    if(ret != 0)
    {
        uart_print("get_signature_offset fail!\n");
        return -1;
    }
    uart_print("u32NConfigOffset =%x \n",u32NConfigOffset);
    uart_print("u32NConfigBakOffset =%x \n",u32NConfigBakOffset);
#endif

    u32NConfigSize = 0x10000;  //hardcode    //get_nconfig_size(&u32NConfigSize);

    ret = _readNuttxConfig((unsigned int) u32NConfigBufAddr, u32NConfigSize);
    if (EN_SUCCESS != ret)
    {
        uart_print("Load NConfig fail\n");
        return -1;
    }
    return ret;
#undef NCONFIG_RAW_DATA_SIZE
}

void _strncpy(unsigned char *ct, const char *cs, size_t count)
{
#if 0
    while (count > 0)
    {
        ct[count - 1] = cs[count - 1];
        count--;
    }
#else
     if (count != 0) {
         unsigned char *d = ct;
         const char *s = cs;

         do {
             if ((*d++ = *s++) == 0) {
                 /* NUL pad the remaining n-1 bytes */
                 while (--count) {
                     *d++ = 0;
                 }
                 break;
             }
         } while (--count);
     }
#endif
}

void set_tee_boot_config(U32 u32BootConfigAddr)
{
    uart_print("set_tee_boot_config In\n");

    ST_TEE_BOOT_CONFIG *tmp = (ST_TEE_BOOT_CONFIG *) u32BootConfigAddr;

    ST_TEE_BOOT_CONFIG *stTeeBootConfig = (ST_TEE_BOOT_CONFIG *) u32BootConfigAddr;

    if (sizeof(tmp->enDebugLevel) == 1)
    {
        ST_TEE_BOOT_CONFIG *stTeeBootConfig = (ST_TEE_BOOT_CONFIG *) u32BootConfigAddr;
    }
    else if (sizeof(tmp->enDebugLevel) == 4)
    {
        ST_TEE_BOOT_CONFIG_ALIGN *stTeeBootConfig = (ST_TEE_BOOT_CONFIG_ALIGN *) u32BootConfigAddr;
    }

    _strncpy(stTeeBootConfig->u8BootConfigID, TEE_BOOT_CONFIG_ID, sizeof(stTeeBootConfig->u8BootConfigID));

    stTeeBootConfig->enDebugLevel = EN_TEE_DEBUG_LEVEL_INFO;

    stTeeBootConfig->u8FastBootMode = 0x1;

}

void _set_tee_debug_mode(ST_TEE_BOOT_CONFIG *pstTeeBootConfig)
{
    EN_TEE_DEBUG_LEVEL dbgLevel = (EN_TEE_DEBUG_LEVEL_ERROR + EN_TEE_DEBUG_LEVEL_INFO);
    dbgLevel = EN_TEE_DEBUG_LEVEL_INFO;
    pstTeeBootConfig->enDebugLevel = dbgLevel;
}

void Secure_MailBox_Init(void)
{
#if 0
    if(MsApi_MBX_Init()==ture)
    {

        if( E_MBX_SUCCESS != MDrv_MBX_RegisterMSG(E_MBX_CLASS_SECURE_WAIT, SECUREBOOT_MBX_QUEUESIZE))
        {
            uart_print("E_MBX_CLASS_SECURE_WAIT - MBX register msg error\n");
        }
        uart_print("OK\n");
        return true;
    }
    else
    {
        uart_print("fail\n");
        return false;
    }
#endif
}

int IsSecureMemDone(U32 address)
{
    char *s = NULL;
    U32 timeOut = 0;
    U32 count = 0;
    timeOut = 600000;
    while (1)
    {
        delay_ms(1);
        if (strn_cmp((void *) address, TeeHandShake, sizeof(TeeHandShake)) == 0)
        {
            uart_print("TEE HankShake OK\n");
            return 0;
        }
        count++;
        if (count == timeOut)
        {
            break;
        }
    }

    return -1;
}

void SetHKtoGo(U32 address)
{
    _memcpyForSTR((void*) address, HKHandShake, sizeof(HKHandShake));

    //_chip_flush_miu_pipe();

    if (strn_cmp((void*) address, HKHandShake, sizeof(HKHandShake)) == 0)
    {
        uart_print("TEE SetHKtoGo OK\n");
    }

    return;
}

void wait_tee_ready(void)
{
    uart_print("Wait for secure memory done\n");
    if (-1 == IsSecureMemDone(gu32TeeHandShakeAddr))
    {
        uart_print("SecureOS is dead\n");
        //MApi_Disable_R2();
    }
    else
    {
        SetHKtoGo(gu32TeeHandShakeAddr);

        uart_print("Set HK Ongoing\n");

    }
}

// START TO READ THE NUTTX IMAGE
int do_readNuttx(void)
{
    //*((volatile unsigned int *)0x1F101EA6) = 0xFFD4;
    uart_print("do_Read Nuttx In\n");
#define _ALIGN_SIZE_ 32
    char* pu8NuttxHeaderAddr = NULL;
    char* _pu8NuttxHeaderAddr = NULL;
    int ret = 0;

    _pu8NuttxHeaderAddr = (U32 *) (HW_SECURE_BUFFER_ADR + MIU0_BASE);  //-->Ask

    //_memsetForSTR(_pu8NuttxHeaderAddr, 0x00, 0x10000);

    pu8NuttxHeaderAddr = (unsigned int *) ((unsigned int) _pu8NuttxHeaderAddr + (_ALIGN_SIZE_ - ((unsigned int) _pu8NuttxHeaderAddr % _ALIGN_SIZE_)));

    //_memsetForSTR(pu8NuttxHeaderAddr, 0, NUTTX_HEADER_LEN);

    _readTee((U32) pu8NuttxHeaderAddr, NUTTX_HEADER_LEN);

    NuttxAlignSize = CalculateNuttxAlignSize(pu8NuttxHeaderAddr);

    if (NuttxAlignSize != 0)
    {
        pNuttxOriMallocAddr = AllocateNuttxbuffer(NuttxAlignSize + 32);
        if (pNuttxOriMallocAddr == NULL)
        {
            uart_print("malloc fail\n");
            return -1;
        }

        pNuttxAlignMallocAddr = (char*) ((((U32) (((U32) pNuttxOriMallocAddr) + 32)) >> 5) << 5);
        _readTee((U32) pNuttxAlignMallocAddr, NuttxAlignSize);

    }
    else
    {
#define TEE_PART_SIZE 0x600000
        pNuttxAlignMallocAddr = AllocateNuttxbuffer(TEE_PART_SIZE);
        if (pNuttxAlignMallocAddr == NULL)
        {
            return -1;
        }
        _readTee((U32) pNuttxAlignMallocAddr, NuttxAlignSize);
        NuttxAlignSize = TEE_PART_SIZE;
    }

    uart_print("do_Read Nuttx OK\n");

    return ret;
}

int do_bootNuttx(void)
{
    int ret = 0;
    U32 u32NuttxPhyAddr = 0;
    U32 u32NuttxVirtAddr = 0;
    U32 u32HwAesPABufAddr = 0;
    U32 u32HwAesBufSize = 0;
    U32 u32CustomerKeyBankAddr = 0;
    U32 u32NConfigAddr = 0;
    U32 u32NConfigSize = 0;
    uart_print("do_bootNuttx In\n");

    u32NuttxVirtAddr = (U32) pNuttxAlignMallocAddr;

    //it need to dicuss with Jerry-CP. if VA=PA, should it move nuttx ?
    u32NuttxPhyAddr = MoveNuttx2Dest(u32NuttxVirtAddr, NuttxAlignSize);
    if (u32NuttxPhyAddr == 0)
    {
        uart_print("MoveNuttx2Dest fail\n");
        return -1;
    }

    g32NuttxFWPhyAddr = u32NuttxPhyAddr;

    if (-1 == supportTeeBootConfig(u32NuttxVirtAddr))
    {
        return -1;
    }

    u32HwAesPABufAddr = get_hw_aes_addr_from_tee_hd(u32NuttxVirtAddr);
    u32HwAesBufSize = get_hw_aes_len_from_tee_hd(u32NuttxVirtAddr);

    if ((u32HwAesPABufAddr == 0) || (u32HwAesPABufAddr == 0xffffffff) || (u32HwAesBufSize == 0) || (u32HwAesBufSize == 0xffffffff))
    {
        uart_print("tee header doesn't have hw aes info, using hw aes info in mmap\n");
    }

    u32CustomerKeyBankAddr = u32HwAesPABufAddr + MIU0_BASE;
    gu32CustomerKeyBankAddr = u32CustomerKeyBankAddr;
    u32NConfigAddr = u32CustomerKeyBankAddr + sizeof(CUSTOMER_KEY_BANK);

    gu32TeeBootConfigAddr = u32HwAesPABufAddr - NUTTX_BOOT_CONFIG_SIZE + MIU0_BASE;  //0x1f800000 - 0x1000 = 1F7FF000
    gu32TeeHandShakeAddr = gu32TeeBootConfigAddr + 0x58;  //sizeof(ST_TEE_BOOT_CONFIG);  //0x1f7ff058 - 1F7FF000 = 58

    u32NConfigSize = 0x10000;  //hardcode-->//get_nconfig_size(&u32NConfigSize);

    if ((sizeof(CUSTOMER_KEY_BANK) + u32NConfigSize) > u32HwAesBufSize)
    {
        uart_print("Data is too big\n");
        return -1;
    }

    if (-1 == CopyCustomerKeyBank(u32CustomerKeyBankAddr))
    {
        uart_print("Load CopyCustomerKeyBank fail\n");
        return -1;
    }

    uart_print("Load NConfig Start \n");

    LoadNConfig(u32NConfigAddr);

    set_tee_boot_config(gu32TeeBootConfigAddr);

    MApi_R2_StartFrDRAM(g32NuttxFWPhyAddr);

    wait_tee_ready();
#if 0
    uart_print("MDrv_AESDMA_MOVE Encrypt and Decrypt Test\n");
    ST_TEE_HEADER* pTeeHd = (ST_TEE_HEADER*) pNuttxAlignMallocAddr;
    CUSTOMER_KEY_BANK *CustomerKeyBank = (CUSTOMER_KEY_BANK*) gu32CustomerKeyBankAddr;

    memcpy(u8MagicID, CustomerKeyBank->u8MagicID, 16);
    MDrv_AESDMA_MOVE(
            E_DRVAESDMA_CIPHER_ECB,
            FALSE,  //false: encrypt, true:decrypt
            gu32CustomerKeyBankAddr, sizeof(CUSTOMER_KEY_BANK),
            gu32CustomerKeyBankAddr, sizeof(CUSTOMER_KEY_BANK),
            NULL,
            NULL,
            FALSE
            );

    MDrv_AESDMA_MOVE(
            E_DRVAESDMA_CIPHER_ECB,
            TRUE,  //false: encrypt, true:decrypt
            gu32CustomerKeyBankAddr, sizeof(CUSTOMER_KEY_BANK),
            gu32CustomerKeyBankAddr, sizeof(CUSTOMER_KEY_BANK),
            NULL,
            NULL,
            TRUE
            );

    if (0 != _memcmpForSTR(u8MagicID, CustomerKeyBank->u8MagicID, 16))
    {
        uart_print("MDrv_AESDMA_MOVE Encrypt and Decrypt fail\n");
    }
    else
    {
        uart_print("MDrv_AESDMA_MOVE Encrypt and Decrypt Passed\n");
    }

#endif

    return ret;
}

#define BANK_AESDMA                    0x123C
#define REG_AESDMA_DMA_CTRL_ADDR       0x50
#define REG_AESDMA_ENG_ADDR            0x51
#define REG_AESDMA_START_ADDR_L        0x52
#define REG_AESDMA_START_ADDR_H        0x53
#define REG_AESDMA_SIZE_ADDR_L         0x54
#define REG_AESDMA_SIZE_ADDR_H         0x55
#define REG_AESDMA_END_S_ADDR_L        0x56
#define REG_AESDMA_END_S_ADDR_H        0x57
#define REG_AESDMA_END_E_ADDR_L        0x58
#define REG_AESDMA_END_E_ADDR_H        0x59
#define REG_AESDMA_DMA2_CTRL_ADDR      0x5E
#define REG_AESDMA_ENG3_CTRL_ADDR      0x77
#define REG_AESDMA_CIPHER_KEY_ADDR     0x60  //Key 0x60~0x67
#define REG_AESDMA_CIPHER_IV_ADDR      0x68  //Key 0x68~0x6F
#define REG_AESDMA_DMA_DONE_ADDR       0x7F

//0x50, REG_AESDMA_DMA_CTRL_ADDR
#define AESDMA_CTRL_FILEIN_START            0x0100
#define AESDMA_CTRL_FILEOUT_START           0x0001
#define AESDMA_CTRL_SW_RST                  0x0080
#define AESDMA_CTRL_BURST_LENGTH            0x1000

#define AESDMA_ENG_PS_RELEASE               0x00010000UL
#define AESDMA_ENG_PS_IN_EN                 0x00100000UL
#define AESDMA_ENG_PS_OUT_EN                0x00200000UL

//0x51, REG_AESDMA_ENG_ADDR
#define AESDMA_ENG_AES_EN                   0x0100
#define AESDMA_ENG_DES_EN                   0x0004
#define AESDMA_ENG_TDES_EN                  0x0008
#define AESDMA_ENG_DESCRYPT                 0x0200
#define AESDMA_ENG_CTR_MODE                 0x1000
#define AESDMA_ENG_CBC_MODE                 0x2000
#define AESDMA_ENG_CTS_CBC_MODE             0x4000
#define AESDMA_ENG_CTS_ECB_MODE             0x0000
#define AESDMA_ECO_FIX_LAST_BYTE            0x8000

//0x5E, REG_AESDMA_DMA2_CTRL_ADDR
#define AESDMA_DMA_USE_TDES_EN              0x0100
#define AESDMA_SECRET_KEY_EN                0x1000

//0X77, REG_AESDMA_ENG3_CTRL_ADDR
#define AESDMA_ENG_SPEEDUP                  0x03BF

//0X7F, REG_AESDMA_DMA_DONE_ADDR
#define AESDMA_IS_FINISHED                  0x0001

//--------------------------------------------------------------------------------------------------
//  Private members
//--------------------------------------------------------------------------------------------------
static U32 _u32MIU0_Addr = 0;
static U32 _u32MIU1_Addr = 0;
static U32 _u32MIU2_Addr = 0;
static U32 _u32MIU_num = 0;
static U32 _gMIO_MapBase = MIU0_BASE;

//--------------------------------------------------------------------------------------------------
//  Private Functions
//--------------------------------------------------------------------------------------------------
U16 HAL_AESDMA_Read2Byte(U32 u32Bank,  //Bank
        U32 u32RegAddr)  //Reg, 16 bit mode
{
    // return ((volatile U16*)(_gMIO_MapBase))[u32RegAddr];
    return ((volatile U16*) (_gMIO_MapBase))[((u32Bank & ~0x1000) << 8) | (u32RegAddr << 1)];
}

BOOL HAL_AESDMA_Write2Byte(U32 u32Bank,  //Bank
        U32 u32RegAddr,  //Reg, 16 bit mode
        U16 u16Val)  //Value, 2Bytes
{
    // ((volatile U16*)(_gMIO_MapBase))[u32RegAddr] = u16Val;
    ((volatile U16*) (_gMIO_MapBase))[((u32Bank & ~0x1000) << 8) | (u32RegAddr << 1)] = u16Val;
    return TRUE;
}

//--------------------------------------------------------------------------------------------------
//  Public Functions
//--------------------------------------------------------------------------------------------------
DRVAESDMA_RESULT MDrv_AESDMA_MOVE(DrvAESDMA_CipherMode eMode, BOOL bDescrypt,
        U32 u32FileinAddr, U32 u32FileInNum, U32 u32FileOutSAddr, U32 u32FileOutEAddr,
        U32 *pCipherKey, U32 *pInitVector,BOOL bEfuseKeyEn)
{
    MDrv_AESDMA_Reset_Lite();
    MDrv_AESDMA_SetFileInOut_Lite(u32FileinAddr, u32FileInNum, u32FileOutSAddr, u32FileOutSAddr + u32FileInNum - 1);
    if (pCipherKey != NULL)
    {
        MDrv_AESDMA_SetKey_Lite(pCipherKey);
    }
    if (pInitVector != NULL)
    {
        MDrv_AESDMA_SetIV_Lite(pInitVector);
    }
    if (bEfuseKeyEn == TRUE)
    {
        //
        //Enable 1St Efuse Key
        //
        HAL_AESDMA_Write2Byte(BANK_AESDMA, REG_AESDMA_DMA2_CTRL_ADDR, HAL_AESDMA_Read2Byte(BANK_AESDMA, REG_AESDMA_DMA2_CTRL_ADDR) | AESDMA_SECRET_KEY_EN);
    }
    else
    {
        HAL_AESDMA_Write2Byte(BANK_AESDMA, REG_AESDMA_DMA2_CTRL_ADDR, HAL_AESDMA_Read2Byte(BANK_AESDMA, REG_AESDMA_DMA2_CTRL_ADDR) & ~AESDMA_SECRET_KEY_EN);
    }
    MDrv_AESDMA_SelEng_Lite(eMode, bDescrypt);
    MDrv_AESDMA_Start_Lite(TRUE);
    while (MDrv_AESDMA_IsFinished_Lite() != DRVAESDMA_OK);

    return DRVAESDMA_OK;
}

DRVAESDMA_RESULT MDrv_AESDMA_IsFinished_Lite(void)
{
    //AESDMA_DEBUG ("\n");
    return HAL_AESDMA_Read2Byte(BANK_AESDMA, REG_AESDMA_DMA_DONE_ADDR) & AESDMA_IS_FINISHED == AESDMA_IS_FINISHED ? DRVAESDMA_OK : DRVAESDMA_FAIL;
}

DRVAESDMA_RESULT MDrv_AESDMA_SetKey_Lite(U32 *pCipherKey)
{
    //AESDMA_DEBUG ("\n");
    U32 i;

    if (pCipherKey == NULL)
    {
        return DRVAESDMA_INVALID_PARAM;
    }

    for (i = 0; i < 4; i++)
    {
        //AESDMA_DEBUG ("pCipherKey[i]                        = 0x%x\n", pCipherKey[i] );
        //AESDMA_DEBUG ("-REG_AESDMA_CIPHER_KEY_ADDR+ (i*2):   0x%x   = 0x%x\n", REG_AESDMA_CIPHER_KEY_ADDR+ (i*2), pCipherKey[i] & 0xFFFF);
        //AESDMA_DEBUG ("-REG_AESDMA_CIPHER_KEY_ADDR+ (i*2+1): 0x%x = 0x%x\n", REG_AESDMA_CIPHER_KEY_ADDR+ (i*2+1), (pCipherKey[i] >> 16) & 0xFFFF);
        HAL_AESDMA_Write2Byte(BANK_AESDMA, REG_AESDMA_CIPHER_KEY_ADDR + (i * 2), pCipherKey[i] & 0xFFFF);
        HAL_AESDMA_Write2Byte(BANK_AESDMA, REG_AESDMA_CIPHER_KEY_ADDR + (i * 2 + 1), (pCipherKey[i] >> 16) & 0xFFFF);
    }

    return DRVAESDMA_OK;
}

DRVAESDMA_RESULT MDrv_AESDMA_SetIV_Lite(U32 *pInitVector)
{
    //AESDMA_DEBUG ("\n");
    U32 i;

    if (pInitVector == NULL)
    {
        return DRVAESDMA_INVALID_PARAM;
    }

    for (i = 0; i < 4; i++)
    {
        //AESDMA_DEBUG ("pInitVector[i]                        = 0x%x\n", pInitVector[i] );
        //AESDMA_DEBUG ("-REG_AESDMA_CIPHER_KEY_ADDR+ (i*2): 0x%x    = 0x%x\n", REG_AESDMA_CIPHER_IV_ADDR+ (i*2), pInitVector[i] & 0xFFFF);
        //AESDMA_DEBUG ("-REG_AESDMA_CIPHER_KEY_ADDR+ (i*2+1): 0x%x  = 0x%x\n", REG_AESDMA_CIPHER_IV_ADDR+ (i*2+1), (pInitVector[i] >> 16) & 0xFFFF);
        HAL_AESDMA_Write2Byte(BANK_AESDMA, REG_AESDMA_CIPHER_IV_ADDR + (i * 2), pInitVector[i] & 0xFFFF);
        HAL_AESDMA_Write2Byte(BANK_AESDMA, REG_AESDMA_CIPHER_IV_ADDR + (i * 2 + 1), (pInitVector[i] >> 16) & 0xFFFF);
    }

    return DRVAESDMA_OK;
}

DRVAESDMA_RESULT MDrv_AESDMA_Init_Lite(U32 u32miu0addr, U32 u32miu1addr, U32 u32miunum)
{
    //AESDMA_DEBUG ("\n");
    U32 u32BaseSize;

    //if (!MDrv_MMIO_GetBASE(&_gMIO_MapBase, &u32BaseSize, MS_MODULE_BDMA))
    //{
    //AESDMA_DEBUG ("Get Non-PM Base Fail\n");
    //}
    //AESDMA_DEBUG ("_gMIO_MapBase: 0x%x\n", _gMIO_MapBase);

    //
    // clk_aesdma   0x100A_0x19 = 0x0000
    //
    // u32Val = _AESDMA_REG32_R(&_AESDMAClk[0].Reg_Gate_Clk_AESDMA);
    // u32Val &= 0x0000FFFF;
    // REG32_W((&_AESDMAClk[0].Reg_Gate_Clk_AESDMA) , u32Val);

    HAL_AESDMA_Write2Byte(0x100A, 0x19, 0x0000);

    // HAL_AESDMA_Write2Byte (0x123C, 0x52, 0xABCD);
    // HAL_AESDMA_Write2Byte (0x123C, 0x54, 0xDCBA);
    // //AESDMA_DEBUG ("Read: 0x52 = 0x%x\n", HAL_AESDMA_Read2Byte (0x123C, 0x52));

    //HW Patch
    // REG32_W((&_AESDMACtrl[0].Dma_Ctrl) , _AESDMA_REG32_R(&_AESDMACtrl[0].Dma_Ctrl)|AESDMA_ECO_FIX_LAST_BYTE);
    // REG32_W((&_AESDMACtrl[0].Dma_Ctrl) , _AESDMA_REG32_R(&_AESDMACtrl[0].Dma_Ctrl)|AESDMA_CTRL_BURST_LENGTH);
    // REG32_W((&_AESDMACtrl[0].Dma_Eng3_Ctrl) , _AESDMA_REG32_R(&_AESDMACtrl[0].Dma_Eng3_Ctrl)|AESDMA_ENG_SPEEDUP);

    HAL_AESDMA_Write2Byte(BANK_AESDMA, REG_AESDMA_ENG_ADDR, HAL_AESDMA_Read2Byte(BANK_AESDMA, REG_AESDMA_ENG_ADDR) | AESDMA_ECO_FIX_LAST_BYTE);
    HAL_AESDMA_Write2Byte(BANK_AESDMA, REG_AESDMA_ENG_ADDR, HAL_AESDMA_Read2Byte(BANK_AESDMA, REG_AESDMA_ENG_ADDR) | AESDMA_CTRL_BURST_LENGTH);
    HAL_AESDMA_Write2Byte(BANK_AESDMA, REG_AESDMA_ENG3_CTRL_ADDR, HAL_AESDMA_Read2Byte(BANK_AESDMA, REG_AESDMA_ENG3_CTRL_ADDR) | AESDMA_ENG_SPEEDUP);

    return DRVAESDMA_OK;
}

DRVAESDMA_RESULT MDrv_AESDMA_SetFileInOut_Lite(U32 u32FileinAddr, U32 u32FileInNum, U32 u32FileOutSAddr, U32 u32FileOutEAddr)
{
    //AESDMA_DEBUG ("\n");

    //
    // Set Start Address
    // 0x123C_52, Start: low
    // 0x123C_53, Start: high
    //
    HAL_AESDMA_Write2Byte(BANK_AESDMA, REG_AESDMA_START_ADDR_L, u32FileinAddr & 0xFFFF);
    HAL_AESDMA_Write2Byte(BANK_AESDMA, REG_AESDMA_START_ADDR_H, (u32FileinAddr >> 16) & 0xFFFF);

    //
    // Set Size
    // 0x123C_54, Size: low
    // 0x123C_55, Size: high
    //
    HAL_AESDMA_Write2Byte(BANK_AESDMA, REG_AESDMA_SIZE_ADDR_L, u32FileInNum & 0xFFFF);
    HAL_AESDMA_Write2Byte(BANK_AESDMA, REG_AESDMA_SIZE_ADDR_H, (u32FileInNum >> 16) & 0xFFFF);

    //
    // Set End_S Address
    // 0x123C_56, End_S: low
    // 0x123C_57, End_S: high
    //
    HAL_AESDMA_Write2Byte(BANK_AESDMA, REG_AESDMA_END_S_ADDR_L, u32FileOutSAddr & 0xFFFF);
    HAL_AESDMA_Write2Byte(BANK_AESDMA, REG_AESDMA_END_S_ADDR_H, (u32FileOutSAddr >> 16) & 0xFFFF);

    //
    // Set End_E Address
    // 0x123C_58, End_S: low
    // 0x123C_59, End_S: high
    //
    HAL_AESDMA_Write2Byte(BANK_AESDMA, REG_AESDMA_END_E_ADDR_L, u32FileOutEAddr & 0xFFFF);
    HAL_AESDMA_Write2Byte(BANK_AESDMA, REG_AESDMA_END_E_ADDR_H, (u32FileOutEAddr >> 16) & 0xFFFF);

    return DRVAESDMA_OK;
}

DRVAESDMA_RESULT MDrv_AESDMA_SelEng_Lite(DrvAESDMA_CipherMode eMode, BOOL bDescrypt)
{
    U32 u32Cmd = 0;
    BOOL bDescryptEn = bDescrypt;

    //AESDMA_DEBUG ("\n");

    switch (eMode)
    {
        case E_DRVAESDMA_CIPHER_CTR:
            u32Cmd |= (AESDMA_ENG_AES_EN | AESDMA_ENG_CTR_MODE);
            bDescryptEn = FALSE;
            break;
        case E_DRVAESDMA_CIPHER_DES_CTR:
            u32Cmd |= (AESDMA_ENG_DES_EN | AESDMA_ENG_CTR_MODE);
            bDescryptEn = FALSE;
            break;
        case E_DRVAESDMA_CIPHER_TDES_CTR:
            u32Cmd |= (AESDMA_ENG_TDES_EN | AESDMA_ENG_CTR_MODE);
            bDescryptEn = FALSE;
            break;

        case E_DRVAESDMA_CIPHER_ECB:
            u32Cmd |= AESDMA_ENG_AES_EN;
            break;
        case E_DRVAESDMA_CIPHER_CBC:
            u32Cmd |= (AESDMA_ENG_AES_EN | AESDMA_ENG_CBC_MODE);
            break;
        case E_DRVAESDMA_CIPHER_DES_ECB:
            u32Cmd |= AESDMA_ENG_DES_EN;
            break;
        case E_DRVAESDMA_CIPHER_DES_CBC:
            u32Cmd |= (AESDMA_ENG_DES_EN | AESDMA_ENG_CBC_MODE);
            break;
        case E_DRVAESDMA_CIPHER_TDES_ECB:
            u32Cmd |= AESDMA_ENG_TDES_EN;
            break;
        case E_DRVAESDMA_CIPHER_TDES_CBC:
            u32Cmd |= (AESDMA_ENG_TDES_EN | AESDMA_ENG_CBC_MODE);
            break;
        case E_DRVAESDMA_CIPHER_CTS_CBC:
            u32Cmd |= (AESDMA_ENG_AES_EN | AESDMA_ENG_CTS_CBC_MODE);
            break;
        case E_DRVAESDMA_CIPHER_CTS_ECB:
            u32Cmd |= (AESDMA_ENG_AES_EN | AESDMA_ENG_CTS_ECB_MODE);
            break;
        case E_DRVAESDMA_CIPHER_DES_CTS_CBC:
            u32Cmd |= (AESDMA_ENG_DES_EN | AESDMA_ENG_CTS_CBC_MODE);
            break;
        case E_DRVAESDMA_CIPHER_DES_CTS_ECB:
            u32Cmd |= (AESDMA_ENG_DES_EN | AESDMA_ENG_CTS_ECB_MODE);
            break;
        case E_DRVAESDMA_CIPHER_TDES_CTS_CBC:
            u32Cmd |= (AESDMA_ENG_TDES_EN | AESDMA_ENG_CTS_CBC_MODE);
            break;
        case E_DRVAESDMA_CIPHER_TDES_CTS_ECB:
            u32Cmd |= (AESDMA_ENG_TDES_EN | AESDMA_ENG_CTS_ECB_MODE);
            break;
        default:
            return DRVAESDMA_INVALID_PARAM;
    }

    if (eMode >= E_DRVAESDMA_CIPHER_DES_ECB)
    {
        HAL_AESDMA_Write2Byte(BANK_AESDMA, REG_AESDMA_DMA2_CTRL_ADDR, HAL_AESDMA_Read2Byte(BANK_AESDMA, REG_AESDMA_DMA2_CTRL_ADDR) | AESDMA_DMA_USE_TDES_EN);
    }

    // 0x1012_51
    //AESDMA_DEBUG ("u32Cmd: 0x%x\n", u32Cmd);
    u32Cmd |= AESDMA_ENG_DESCRYPT & (bDescryptEn << 9);
    //AESDMA_DEBUG ("u32Cmd: 0x%x\n", u32Cmd);
    HAL_AESDMA_Write2Byte(BANK_AESDMA, REG_AESDMA_ENG_ADDR, HAL_AESDMA_Read2Byte(BANK_AESDMA, REG_AESDMA_ENG_ADDR) | u32Cmd | AESDMA_ECO_FIX_LAST_BYTE);

    return DRVAESDMA_OK;
}

DRVAESDMA_RESULT MDrv_AESDMA_Start_Lite(BOOL bStart)
{
    //AESDMA_DEBUG ("\n");
    // 0x1012_50
    HAL_AESDMA_Write2Byte(BANK_AESDMA, REG_AESDMA_DMA_CTRL_ADDR, 0x0);
    if (bStart == TRUE)
    {
        HAL_AESDMA_Write2Byte(BANK_AESDMA, REG_AESDMA_DMA_CTRL_ADDR, (AESDMA_CTRL_BURST_LENGTH | AESDMA_CTRL_FILEIN_START | AESDMA_CTRL_FILEOUT_START));
    }

    return DRVAESDMA_OK;
}

DRVAESDMA_RESULT MDrv_AESDMA_Reset_Lite(void)
{

    U32 i;

    //Clear 0x50~0x6F
    for (i = 0; i < 32; i++)
    {
        HAL_AESDMA_Write2Byte(BANK_AESDMA, REG_AESDMA_DMA_CTRL_ADDR + i, 0x0);
    }

    return DRVAESDMA_OK;
}
#endif
