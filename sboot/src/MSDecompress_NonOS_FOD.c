////////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2006-2009 MStar Semiconductor, Inc.
// All rights reserved.
//
// Unless otherwise stipulated in writing, any and all information contained
// herein regardless in any format shall remain the sole proprietary of
// MStar Semiconductor Inc. and be kept in strict confidence
// ("MStar Confidential Information") by the recipient.
// Any unauthorized act including without limitation unauthorized disclosure,
// copying, use, reproduction, sale, distribution, modification, disassembling,
// reverse engineering and compiling of the contents of MStar Confidential
// Information is unlawful and strictly prohibited. MStar hereby reserves the
// rights to any and all damages, losses, costs and expenses resulting therefrom.
//
////////////////////////////////////////////////////////////////////////////////

#if (ENABLE_NON_OS)

 #if (ENABLE_MSTAR_MARLON || ENABLE_MSTAR_WHISKY)
    #define ENABLE_FLASH_ON_DRAM    1

 #else
    #define ENABLE_FLASH_ON_DRAM    0

 #endif


#if( ENABLE_FLASH_ON_DRAM)

#define FOD_MAGIC_ID        0x12344321
#define FOD_MAGIC_ID_2      0x43211234

#if( defined(__aeon__) )
#define PROG_IMG_INFO_OFFSET            (0x1100)
#elif( defined(__mips__) )
#define PROG_IMG_INFO_OFFSET            (0xF80)
#endif

#define PROG_IMG_INFO_FOD_POINTER_OFFSET (PROG_IMG_INFO_OFFSET+0x58)

typedef struct
{
    U32 u32FODMagicId_1;

    U32 u32DramBaseAddr;

    U32 u32FlashDataAddr_1;
    U32 u32FlashDataSize_1;

    U32 u32FlashDataAddr_2;
    U32 u32FlashDataSize_2;

    U32 u32FODMagicId_2;
} __attribute__ ((packed)) StuFODDataForImgInfo;

static StuFODDataForImgInfo g_FOD_Header;

#if( defined(__aeon__) )
    #define FOD_printf  Ld_printf

#elif( defined(__mips__) )
    #define FOD_printf  Ld_printf

#endif

BOOL __loader_2nd FOD_ReadHeader(U32 u32ApRamStartAddr);
BOOL __loader_2nd FOD_ReadHeader(U32 u32ApRamStartAddr)
{
    U32 u32FOD_Pointer = *(U32*)(u32ApRamStartAddr+PROG_IMG_INFO_FOD_POINTER_OFFSET);

    //DEBUG_DECOMP_FOD( FOD_printf("u32ApRamStartAddr=%X\n", u32ApRamStartAddr); );
    //DEBUG_DECOMP_FOD( FOD_printf("pointer addr=%X\n", u32ApRamStartAddr+PROG_IMG_INFO_FOD_POINTER_OFFSET); );
    DEBUG_DECOMP_FOD( FOD_printf("u32FOD_Pointer=%X\n", u32FOD_Pointer); );

    if( u32FOD_Pointer == 0 )
    {
        FOD_printf("No FOD data\n", 0);
        return FALSE;
    }

    // Copy data to g_FOD_Header
    Loader_MemCpy( (U8*)&g_FOD_Header, (U8*)u32FOD_Pointer, sizeof(StuFODDataForImgInfo) );

    //DEBUG_DECOMP_FOD( uart_printf("id1=%X\n", g_FOD_Header.u32FODMagicId_1); );
    //DEBUG_DECOMP_FOD( uart_printf("id2=%X\n", g_FOD_Header.u32FODMagicId_2); );

    if( g_FOD_Header.u32FODMagicId_1 != FOD_MAGIC_ID
      ||g_FOD_Header.u32FODMagicId_2 != FOD_MAGIC_ID_2
      )
    {
        FOD_printf("FOD id error=%X,", g_FOD_Header.u32FODMagicId_1);
        FOD_printf("%X\n", g_FOD_Header.u32FODMagicId_2);
        return FALSE;
    }

    DEBUG_DECOMP_FOD( FOD_printf("u32DramBaseAddr=%X\n", g_FOD_Header.u32DramBaseAddr); );

    DEBUG_DECOMP_FOD( FOD_printf("u32FlashDataAddr_1=%X\n", g_FOD_Header.u32FlashDataAddr_1); );
    DEBUG_DECOMP_FOD( FOD_printf("u32FlashDataSize_1=%X\n", g_FOD_Header.u32FlashDataSize_1); );

    if( g_FOD_Header.u32FlashDataSize_2 )
    {
        DEBUG_DECOMP_FOD( FOD_printf("u32FlashDataAddr_2=%X\n", g_FOD_Header.u32FlashDataAddr_2); );
        DEBUG_DECOMP_FOD( FOD_printf("u32FlashDataSize_2=%X\n", g_FOD_Header.u32FlashDataSize_2); );
    }

    return TRUE;
}

#endif // ENABLE_FLASH_ON_DRAM

#endif // ENABLE_NON_OS

