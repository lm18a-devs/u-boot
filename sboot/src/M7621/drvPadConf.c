//<MStar Software>
//******************************************************************************
// MStar Software
// Copyright (c) 2010 - 2017 MStar Semiconductor, Inc. All rights reserved.
// All software, firmware and related documentation herein ("MStar Software") are
// intellectual property of MStar Semiconductor, Inc. ("MStar") and protected by
// law, including, but not limited to, copyright law and international treaties.
// Any use, modification, reproduction, retransmission, or republication of all
// or part of MStar Software is expressly prohibited, unless prior written
// permission has been granted by MStar.
//
// By accessing, browsing and/or using MStar Software, you acknowledge that you
// have read, understood, and agree, to be bound by below terms ("Terms") and to
// comply with all applicable laws and regulations:
//
// 1. MStar shall retain any and all right, ownership and interest to MStar
//    Software and any modification/derivatives thereof.
//    No right, ownership, or interest to MStar Software and any
//    modification/derivatives thereof is transferred to you under Terms.
//
// 2. You understand that MStar Software might include, incorporate or be
//    supplied together with third party`s software and the use of MStar
//    Software may require additional licenses from third parties.
//    Therefore, you hereby agree it is your sole responsibility to separately
//    obtain any and all third party right and license necessary for your use of
//    such third party`s software.
//
// 3. MStar Software and any modification/derivatives thereof shall be deemed as
//    MStar`s confidential information and you agree to keep MStar`s
//    confidential information in strictest confidence and not disclose to any
//    third party.
//
// 4. MStar Software is provided on an "AS IS" basis without warranties of any
//    kind. Any warranties are hereby expressly disclaimed by MStar, including
//    without limitation, any warranties of merchantability, non-infringement of
//    intellectual property rights, fitness for a particular purpose, error free
//    and in conformity with any international standard.  You agree to waive any
//    claim against MStar for any loss, damage, cost or expense that you may
//    incur related to your use of MStar Software.
//    In no event shall MStar be liable for any direct, indirect, incidental or
//    consequential damages, including without limitation, lost of profit or
//    revenues, lost or damage of data, and unauthorized system use.
//    You agree that this Section 4 shall still apply without being affected
//    even if MStar Software has been modified by MStar in accordance with your
//    request or instruction for your use, except otherwise agreed by both
//    parties in writing.
//
// 5. If requested, MStar may from time to time provide technical supports or
//    services in relation with MStar Software to you for your use of
//    MStar Software in conjunction with your or your customer`s product
//    ("Services").
//    You understand and agree that, except otherwise agreed by both parties in
//    writing, Services are provided on an "AS IS" basis and the warranty
//    disclaimer set forth in Section 4 above shall apply.
//
// 6. Nothing contained herein shall be construed as by implication, estoppels
//    or otherwise:
//    (a) conferring any license or right to use MStar name, trademark, service
//        mark, symbol or any other identification;
//    (b) obligating MStar or any of its affiliates to furnish any person,
//        including without limitation, you and your customers, any assistance
//        of any kind whatsoever, or any information; or
//    (c) conferring any license or right under any intellectual property right.
//
// 7. These terms shall be governed by and construed in accordance with the laws
//    of Taiwan, R.O.C., excluding its conflict of law rules.
//    Any and all dispute arising out hereof or related hereto shall be finally
//    settled by arbitration referred to the Chinese Arbitration Association,
//    Taipei in accordance with the ROC Arbitration Law and the Arbitration
//    Rules of the Association by three (3) arbitrators appointed in accordance
//    with the said Rules.
//    The place of arbitration shall be in Taipei, Taiwan and the language shall
//    be English.
//    The arbitration award shall be final and binding to both parties.
//
//******************************************************************************
//<MStar Software>

#include <stdio.h>

#include "datatype.h"
#include "drvRIU.h"
#include "Board.h"
#include "c_riubase.h"
#include "hwreg_M7621.h"
#include "chip/bond.h"

#define GPIO_NONE               0       // Not GPIO pin (default)
#define GPIO_IN                 1       // GPI
#define GPIO_OUT_LOW            2       // GPO output low
#define GPIO_OUT_HIGH           3       // GPO output high

#if defined(ARM_CHAKRA) || defined (MIPS_CHAKRA) || defined(MSOS_TYPE_LINUX)
#define _MapBase_nonPM_M7621      (RIU_MAP + 0x200000)
#define _MapBase_PM_M7621         RIU_MAP
#else
#define _MapBase_nonPM_M7621      0xA0200000
#define _MapBase_PM_M7621         0xA0000000
#endif

#define _MEMMAP_PM_                 _RVM1(0x0000, 0x00, 0xFF)
#define _MEMMAP_nonPM_              _RVM1(0x0000, 0x10, 0xFF)
#define _MEMMAP_nonPM_00_           _RVM1(0x0000, 0x00, 0xFF)
#define _MEMMAP_nonPM_01_           _RVM1(0x0000, 0x01, 0xFF)
#define _MEMMAP_nonPM_10_           _RVM1(0x0000, 0x10, 0xFF)
#define _MEMMAP_nonPM_11_           _RVM1(0x0000, 0x11, 0xFF)
#define _MEMMAP_nonPM_15_           _RVM1(0x0000, 0x15, 0xFF)

const U8 padInitTbl_PreInit[] =
{
    0xFF, 0xFF, 0xFF, 0xFF,         // magic code for ISP_Tool

    // programable device number
    // spi flash count
    0,
    0x00,                           // nor
    0x00,                           // nand
    0x00,                           // reserved
    0x00,                           // reserved
    0x00,                           // reserved

//---------------------------------------------------------------------
// GPIO Configuartion
//---------------------------------------------------------------------
    _MEMMAP_PM_,

#if(PAD_VID0_IS_GPIO != GPIO_NONE)
    #define PAD_VID0_OEN (PAD_VID0_IS_GPIO == GPIO_IN ? BIT1: 0)
    #define PAD_VID0_OUT (PAD_VID0_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2e84, PAD_VID0_OUT, BIT0),
        _RVM1(0x2e84, PAD_VID0_OEN, BIT1),
        //reg_vid_is_gpio
        _RVM1(0x0e39, BIT2, BIT2),   //reg[0e39]#2 = 1b
        //reg_miic_mode0
        _RVM1(0x0ec9, 0, BIT7 | BIT6),   //reg[0ec9]#7~#6 = 0b
#endif

#if(PAD_VID1_IS_GPIO != GPIO_NONE)
    #define PAD_VID1_OEN (PAD_VID1_IS_GPIO == GPIO_IN ? BIT1: 0)
    #define PAD_VID1_OUT (PAD_VID1_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2e85, PAD_VID1_OUT, BIT0),
        _RVM1(0x2e85, PAD_VID1_OEN, BIT1),
        //reg_vid_is_gpio
        _RVM1(0x0e39, BIT2, BIT2),   //reg[0e39]#2 = 1b
        //reg_miic_mode0
        _RVM1(0x0ec9, 0, BIT7 | BIT6),   //reg[0ec9]#7~#6 = 0b
#endif

#if(PAD_VID2_IS_GPIO != GPIO_NONE)
    #define PAD_VID2_OEN (PAD_VID2_IS_GPIO == GPIO_IN ? BIT1: 0)
    #define PAD_VID2_OUT (PAD_VID2_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2e86, PAD_VID2_OUT, BIT0),
        _RVM1(0x2e86, PAD_VID2_OEN, BIT1),
        //reg_vid_is_gpio
        _RVM1(0x0e39, BIT2, BIT2),   //reg[0e39]#2 = 1b
#endif

#if(PAD_VID3_IS_GPIO != GPIO_NONE)
    #define PAD_VID3_OEN (PAD_VID3_IS_GPIO == GPIO_IN ? BIT1: 0)
    #define PAD_VID3_OUT (PAD_VID3_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2e87, PAD_VID3_OUT, BIT0),
        _RVM1(0x2e87, PAD_VID3_OEN, BIT1),
        //reg_vid_is_gpio
        _RVM1(0x0e39, BIT2, BIT2),   //reg[0e39]#2 = 1b
#endif

//---------------------------------------------------------------------
// Pad Configuartion
//---------------------------------------------------------------------

    _MEMMAP_nonPM_,

// SDR Mode
#ifdef PADS_NAND_MODE
#if (PADS_NAND_MODE != CONFIG_PADMUX_UNKNOWN)
#define CONFIG_NAND_MODE_MODE1 ((PADS_NAND_MODE == CONFIG_PADMUX_MODE1) ? (0x01 << 6) : \
                                (PADS_NAND_MODE == CONFIG_PADMUX_MODE2) ? (0x02 << 6) : \
                                (PADS_NAND_MODE == CONFIG_PADMUX_MODE3) ? (0x03 << 6) : 0)
    _RVM1(0x1ede, CONFIG_NAND_MODE_MODE1, BITMASK(7:6)),
#endif
#endif

#ifdef PADS_EMMC_CONFIG
#if (PADS_EMMC_CONFIG != CONFIG_PADMUX_UNKNOWN)
#define CONFIG_EMMC_CONFIG_MODE1 ((PADS_EMMC_CONFIG == CONFIG_PADMUX_MODE1) ? (0x01 << 6) : 0)
    _RVM1(0x1edc, CONFIG_EMMC_CONFIG_MODE1, BITMASK(7:6)),
#endif
#endif

//=============================================================================
    //reg_allpad_in
    _RVM1(0x1ea1, 0, BIT7),     //reg[101ea1]#7 = 0b
    _END_OF_TBL_,
};

const U8 padInitTbl[] =
{
    0x39, 0xB6, 0x5B, 0x53,     // magic code for ISP_Tool

    // programable device number
    // spi flash count
    1 + (PIN_SPI_CZ1 != 0) + (PIN_SPI_CZ2 != 0) + (PIN_SPI_CZ3 != 0),
    0x00,                       // nor
    0x00,                       // nand
    0x00,                       // reserved
    0x00,                       // reserved
    0x00,                       // reserved

//---------------------------------------------------------------------
// GPIO Configuartion
//---------------------------------------------------------------------

_MEMMAP_PM_,
    #if(PAD_PM_SPI_CK_IS_GPIO != GPIO_NONE)
        #define PAD_PM_SPI_CK_OEN (PAD_PM_SPI_CK_IS_GPIO == GPIO_IN ? BIT0: 0)
        #define PAD_PM_SPI_CK_OUT (PAD_PM_SPI_CK_IS_GPIO == GPIO_OUT_HIGH ? BIT1: 0)
        _RVM1(0x0f48, PAD_PM_SPI_CK_OUT, BIT1),
        _RVM1(0x0f48, PAD_PM_SPI_CK_OEN, BIT0),
        //reg_spi_gpio
        _RVM1(0x0e6a, BIT0, BIT0),   //reg[0e6a]#0 = 1b
    #endif

    #if(PAD_PM_SPI_DI_IS_GPIO != GPIO_NONE)
        #define PAD_PM_SPI_DI_OEN (PAD_PM_SPI_DI_IS_GPIO == GPIO_IN ? BIT0: 0)
        #define PAD_PM_SPI_DI_OUT (PAD_PM_SPI_DI_IS_GPIO == GPIO_OUT_HIGH ? BIT1: 0)
        _RVM1(0x0f4a, PAD_PM_SPI_DI_OUT, BIT1),
        _RVM1(0x0f4a, PAD_PM_SPI_DI_OEN, BIT0),
        //reg_spi_gpio
        _RVM1(0x0e6a, BIT0, BIT0),   //reg[0e6a]#0 = 1b
    #endif

    #if(PAD_PM_SPI_DO_IS_GPIO != GPIO_NONE)
        #define PAD_PM_SPI_DO_OEN (PAD_PM_SPI_DO_IS_GPIO == GPIO_IN ? BIT0: 0)
        #define PAD_PM_SPI_DO_OUT (PAD_PM_SPI_DO_IS_GPIO == GPIO_OUT_HIGH ? BIT1: 0)
        _RVM1(0x0f4c, PAD_PM_SPI_DO_OUT, BIT1),
        _RVM1(0x0f4c, PAD_PM_SPI_DO_OEN, BIT0),
        //reg_spi_gpio
        _RVM1(0x0e6a, BIT0, BIT0),   //reg[0e6a]#0 = 1b
    #endif

    #if(PAD_IRIN_IS_GPIO != GPIO_NONE)
        #define PAD_IRIN_OEN (PAD_IRIN_IS_GPIO == GPIO_IN ? BIT0: 0)
        #define PAD_IRIN_OUT (PAD_IRIN_IS_GPIO == GPIO_OUT_HIGH ? BIT1: 0)
        _RVM1(0x0f3e, PAD_IRIN_OUT, BIT1),
        _RVM1(0x0f3e, PAD_IRIN_OEN, BIT0),
        //reg_ir_is_gpio
        _RVM1(0x0e38, BIT4, BIT4),   //reg[0e38]#4 = 1b
    #endif

    #if(PAD_CEC0_IS_GPIO != GPIO_NONE)
        #define PAD_CEC0_OEN (PAD_CEC0_IS_GPIO == GPIO_IN ? BIT0: 0)
        #define PAD_CEC0_OUT (PAD_CEC0_IS_GPIO == GPIO_OUT_HIGH ? BIT1: 0)
        _RVM1(0x0f42, PAD_CEC0_OUT, BIT1),
        _RVM1(0x0f42, PAD_CEC0_OEN, BIT0),
        //reg_cec_is_gpio
        _RVM1(0x0e38, BIT6, BIT6),   //reg[0e38]#6 = 1b
    #endif

    #if(PAD_PWM_PM_IS_GPIO != GPIO_NONE)
        #define PAD_PWM_PM_OEN (PAD_PWM_PM_IS_GPIO == GPIO_IN ? BIT0: 0)
        #define PAD_PWM_PM_OUT (PAD_PWM_PM_IS_GPIO == GPIO_OUT_HIGH ? BIT1: 0)
        _RVM1(0x0f40, PAD_PWM_PM_OUT, BIT1),
        _RVM1(0x0f40, PAD_PWM_PM_OEN, BIT0),
        //reg_pwm_pm_is_gpio
        _RVM1(0x0e38, BIT5, BIT5),   //reg[0e38]#5 = 1b
    #endif

    #if(PAD_DDCA_CK_IS_GPIO != GPIO_NONE)
        #define PAD_DDCA_CK_OEN (PAD_DDCA_CK_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_DDCA_CK_OUT (PAD_DDCA_CK_IS_GPIO == GPIO_OUT_HIGH ? BIT2: 0)
        _RVM1(0x0494, PAD_DDCA_CK_OUT, BIT2),
        _RVM1(0x0494, PAD_DDCA_CK_OEN, BIT1),
        //reg_gpio2a0_en
        _RVM1(0x0494, BIT7, BIT7),   //reg[0494]#7 = 1b
    #endif

    #if(PAD_DDCA_DA_IS_GPIO != GPIO_NONE)
        #define PAD_DDCA_DA_OEN (PAD_DDCA_DA_IS_GPIO == GPIO_IN ? BIT5: 0)
        #define PAD_DDCA_DA_OUT (PAD_DDCA_DA_IS_GPIO == GPIO_OUT_HIGH ? BIT6: 0)
        _RVM1(0x0494, PAD_DDCA_DA_OUT, BIT6),
        _RVM1(0x0494, PAD_DDCA_DA_OEN, BIT5),
        //reg_gpio2a0_en
        _RVM1(0x0494, BIT7, BIT7),   //reg[0494]#7 = 1b
    #endif

    #if(PAD_GPIO0_PM_IS_GPIO != GPIO_NONE)
        #define PAD_GPIO0_PM_OEN (PAD_GPIO0_PM_IS_GPIO == GPIO_IN ? BIT0: 0)
        #define PAD_GPIO0_PM_OUT (PAD_GPIO0_PM_IS_GPIO == GPIO_OUT_HIGH ? BIT1: 0)
        _RVM1(0x0f00, PAD_GPIO0_PM_OUT, BIT1),
        _RVM1(0x0f00, PAD_GPIO0_PM_OEN, BIT0),
    #endif

    #if(PAD_GPIO1_PM_IS_GPIO != GPIO_NONE)
        #define PAD_GPIO1_PM_OEN (PAD_GPIO1_PM_IS_GPIO == GPIO_IN ? BIT0: 0)
        #define PAD_GPIO1_PM_OUT (PAD_GPIO1_PM_IS_GPIO == GPIO_OUT_HIGH ? BIT1: 0)
        _RVM1(0x0f02, PAD_GPIO1_PM_OUT, BIT1),
        _RVM1(0x0f02, PAD_GPIO1_PM_OEN, BIT0),
        //reg_uart_is_gpio[3:0]
        _RVM1(0x0e6b, 0, 0x0F),   //reg[0e6b]#3~#0 = 0b
        //reg_uart_is_gpio_3[3:0]
        _RVM1(0x0eec, 0, 0x3C),   //reg[0eec]#5~#2 = 0b
    #endif

    #if(PAD_GPIO2_PM_IS_GPIO != GPIO_NONE)
        #define PAD_GPIO2_PM_OEN (PAD_GPIO2_PM_IS_GPIO == GPIO_IN ? BIT0: 0)
        #define PAD_GPIO2_PM_OUT (PAD_GPIO2_PM_IS_GPIO == GPIO_OUT_HIGH ? BIT1: 0)
        _RVM1(0x0f04, PAD_GPIO2_PM_OUT, BIT1),
        _RVM1(0x0f04, PAD_GPIO2_PM_OEN, BIT0),
        //reg_hdmi_tx_hotplug_sel
        _RVM1(0x0ee4, 0, BIT12),   //reg[0ee4]#12 = 0b
        //reg_miic_mode0
        _RVM1(0x0ec9, 0, BIT7 | BIT6),   //reg[0ec9]#7~#6 = 0b
    #endif

    #if(PAD_GPIO3_PM_IS_GPIO != GPIO_NONE)
        #define PAD_GPIO3_PM_OEN (PAD_GPIO3_PM_IS_GPIO == GPIO_IN ? BIT0: 0)
        #define PAD_GPIO3_PM_OUT (PAD_GPIO3_PM_IS_GPIO == GPIO_OUT_HIGH ? BIT1: 0)
        _RVM1(0x0f06, PAD_GPIO3_PM_OUT, BIT1),
        _RVM1(0x0f06, PAD_GPIO3_PM_OEN, BIT0),
        //reg_uart_is_gpio_2[1:0]
        _RVM1(0x0eec, 0, BIT1 | BIT0),   //reg[0eec]#1~#0 = 0b
        //reg_uart_is_gpio_3[3:0]
        _RVM1(0x0eec, 0, 0x3C),   //reg[0eec]#5~#2 = 0b
    #endif

    #if(PAD_GPIO4_PM_IS_GPIO != GPIO_NONE)
        #define PAD_GPIO4_PM_OEN (PAD_GPIO3_PM_IS_GPIO == GPIO_IN ? BIT0: 0)
        #define PAD_GPIO4_PM_OUT (PAD_GPIO3_PM_IS_GPIO == GPIO_OUT_HIGH ? BIT1: 0)
        _RVM1(0x0f08, PAD_GPIO3_PM_OUT, BIT1),
        _RVM1(0x0f08, PAD_GPIO3_PM_OEN, BIT0),
    #endif

    #if(PAD_GPIO5_PM_IS_GPIO != GPIO_NONE)
        #define PAD_GPIO5_PM_OEN (PAD_GPIO5_PM_IS_GPIO == GPIO_IN ? BIT0: 0)
        #define PAD_GPIO5_PM_OUT (PAD_GPIO5_PM_IS_GPIO == GPIO_OUT_HIGH ? BIT1: 0)
        _RVM1(0x0f0a, PAD_GPIO5_PM_OUT, BIT1),
        _RVM1(0x0f0a, PAD_GPIO5_PM_OEN, BIT0),
        //reg_uart_is_gpio[3:0]
        _RVM1(0x0e6b, 0, 0x0F),   //reg[0e6b]#3~#0 = 0b
        //reg_uart_is_gpio_3[3:0]
        _RVM1(0x0eec, 0, 0x3C),   //reg[0eec]#5~#2 = 0b
    #endif

    #if(PAD_GPIO6_PM_IS_GPIO != GPIO_NONE)
        #define PAD_GPIO6_PM_OEN (PAD_GPIO6_PM_IS_GPIO == GPIO_IN ? BIT0: 0)
        #define PAD_GPIO6_PM_OUT (PAD_GPIO6_PM_IS_GPIO == GPIO_OUT_HIGH ? BIT1: 0)
        _RVM1(0x0f0c, PAD_GPIO6_PM_OUT, BIT1),
        _RVM1(0x0f0c, PAD_GPIO6_PM_OEN, BIT0),
        //reg_spicsz1_gpio
        _RVM1(0x0e6a, BIT2, BIT2),   //reg[0e6a]#2 = 1b
    #endif

    #if(PAD_GPIO7_PM_IS_GPIO != GPIO_NONE)
        #define PAD_GPIO7_PM_OEN (PAD_GPIO7_PM_IS_GPIO == GPIO_IN ? BIT0: 0)
        #define PAD_GPIO7_PM_OUT (PAD_GPIO7_PM_IS_GPIO == GPIO_OUT_HIGH ? BIT1: 0)
        _RVM1(0x0f0e, PAD_GPIO7_PM_OUT, BIT1),
        _RVM1(0x0f0e, PAD_GPIO7_PM_OEN, BIT0),
    #endif

    #if(PAD_GPIO8_PM_IS_GPIO != GPIO_NONE)
        #define PAD_GPIO8_PM_OEN (PAD_GPIO8_PM_IS_GPIO == GPIO_IN ? BIT0: 0)
        #define PAD_GPIO8_PM_OUT (PAD_GPIO8_PM_IS_GPIO == GPIO_OUT_HIGH ? BIT1: 0)
        _RVM1(0x0f10, PAD_GPIO8_PM_OUT, BIT1),
        _RVM1(0x0f10, PAD_GPIO8_PM_OEN, BIT0),
        //reg_uart_is_gpio[3:0]
        _RVM1(0x0e6b, 0, 0x0F),   //reg[0e6b]#3~#0 = 0b
        //reg_uart_is_gpio_2[1:0]
        _RVM1(0x0eec, 0, BIT1 | BIT0),   //reg[0eec]#1~#0 = 0b
        //reg_uart_is_gpio_3[3:0]
        _RVM1(0x0eec, 0, 0x3C),   //reg[0eec]#5~#2 = 0b
    #endif

    #if(PAD_GPIO9_PM_IS_GPIO != GPIO_NONE)
        #define PAD_GPIO9_PM_OEN (PAD_GPIO9_PM_IS_GPIO == GPIO_IN ? BIT0: 0)
        #define PAD_GPIO9_PM_OUT (PAD_GPIO9_PM_IS_GPIO == GPIO_OUT_HIGH ? BIT1: 0)
        _RVM1(0x0f12, PAD_GPIO9_PM_OUT, BIT1),
        _RVM1(0x0f12, PAD_GPIO9_PM_OEN, BIT0),
        //reg_miic_mode0
        _RVM1(0x0ec9, 0, BIT7 | BIT6),   //reg[0ec9]#7~#6 = 0b
    #endif

    #if(PAD_GPIO10_PM_IS_GPIO != GPIO_NONE)
        #define PAD_GPIO10_PM_OEN (PAD_GPIO10_PM_IS_GPIO == GPIO_IN ? BIT0: 0)
        #define PAD_GPIO10_PM_OUT (PAD_GPIO10_PM_IS_GPIO == GPIO_OUT_HIGH ? BIT1: 0)
        _RVM1(0x0f14, PAD_GPIO10_PM_OUT, BIT1),
        _RVM1(0x0f14, PAD_GPIO10_PM_OEN, BIT0),
        //reg_spicsz1_gpio
        _RVM1(0x0e6a, BIT2, BIT2),   //reg[0e6a]#2 = 1b
    #endif

    #if(PAD_GPIO11_PM_IS_GPIO != GPIO_NONE)
        #define PAD_GPIO11_PM_OEN (PAD_GPIO11_PM_IS_GPIO == GPIO_IN ? BIT0: 0)
        #define PAD_GPIO11_PM_OUT (PAD_GPIO11_PM_IS_GPIO == GPIO_OUT_HIGH ? BIT1: 0)
        _RVM1(0x0f16, PAD_GPIO11_PM_OUT, BIT1),
        _RVM1(0x0f16, PAD_GPIO11_PM_OEN, BIT0),
        //reg_uart_is_gpio_1[1:0]
        _RVM1(0x0e6b, 0, BIT7 | BIT6),   //reg[0e6b]#7~#6 = 0b
        //reg_uart_is_gpio_3[3:0]
        _RVM1(0x0eec, 0, 0x3C),   //reg[0eec]#5~#2 = 0b
    #endif

    #if(PAD_GPIO12_PM_IS_GPIO != GPIO_NONE)
        #define PAD_GPIO12_PM_OEN (PAD_GPIO12_PM_IS_GPIO == GPIO_IN ? BIT0: 0)
        #define PAD_GPIO12_PM_OUT (PAD_GPIO12_PM_IS_GPIO == GPIO_OUT_HIGH ? BIT1: 0)
        _RVM1(0x0f18, PAD_GPIO12_PM_OUT, BIT1),
        _RVM1(0x0f18, PAD_GPIO12_PM_OEN, BIT0),
        //reg_uart_is_gpio_1[1:0]
        _RVM1(0x0e6b, 0, BIT7 | BIT6),   //reg[0e6b]#7~#6 = 0b
        //reg_uart_is_gpio_3[3:0]
        _RVM1(0x0eec, 0, 0x3C),   //reg[0eec]#5~#2 = 0b
    #endif

    _RVM1(0x0e6b, BIT3, BIT3),    //Set PM1/PM5 as uart pad

    #if(PAD_GPIO13_PM_IS_GPIO != GPIO_NONE)
        #define PAD_GPIO13_PM_OEN (PAD_GPIO13_PM_IS_GPIO == GPIO_IN ? BIT0: 0)
        #define PAD_GPIO13_PM_OUT (PAD_GPIO13_PM_IS_GPIO == GPIO_OUT_HIGH ? BIT1: 0)
        _RVM1(0x0f1a, PAD_GPIO13_PM_OUT, BIT1),
        _RVM1(0x0f1a, PAD_GPIO13_PM_OEN, BIT0),
    #endif

    #if(PAD_GPIO14_PM_IS_GPIO != GPIO_NONE)
        #define PAD_GPIO14_PM_OEN (PAD_GPIO14_PM_IS_GPIO == GPIO_IN ? BIT0: 0)
        #define PAD_GPIO14_PM_OUT (PAD_GPIO14_PM_IS_GPIO == GPIO_OUT_HIGH ? BIT1: 0)
        _RVM1(0x0f1c, PAD_GPIO14_PM_OUT, BIT1),
        _RVM1(0x0f1c, PAD_GPIO14_PM_OEN, BIT0),
        //reg_mhl_cable_detect_sel
        _RVM1(0x0ee4, 0, BIT6),   //reg[0ee4]#6 = 0b
    #endif

    #if(PAD_GPIO15_PM_IS_GPIO != GPIO_NONE)
        #define PAD_GPIO15_PM_OEN (PAD_GPIO15_PM_IS_GPIO == GPIO_IN ? BIT0: 0)
        #define PAD_GPIO15_PM_OUT (PAD_GPIO15_PM_IS_GPIO == GPIO_OUT_HIGH ? BIT1: 0)
        _RVM1(0x0f1e, PAD_GPIO15_PM_OUT, BIT1),
        _RVM1(0x0f1e, PAD_GPIO15_PM_OEN, BIT0),
        //reg_vbus_en_sel
        _RVM1(0x0ee4, 0, BIT7),   //reg[0ee4]#7 = 0b
    #endif

    #if(PAD_GPIO16_PM_IS_GPIO != GPIO_NONE)
        #define PAD_GPIO16_PM_OEN (PAD_GPIO16_PM_IS_GPIO == GPIO_IN ? BIT0: 0)
        #define PAD_GPIO16_PM_OUT (PAD_GPIO16_PM_IS_GPIO == GPIO_OUT_HIGH ? BIT1: 0)
        _RVM1(0x0f20, PAD_GPIO16_PM_OUT, BIT1),
        _RVM1(0x0f20, PAD_GPIO16_PM_OEN, BIT0),
        //reg_cbus_debug_sel
        _RVM1(0x0ee5, 0, BIT0),   //reg[0ee5]#0 = 0b
    #endif

    #if(PAD_GPIO17_PM_IS_GPIO != GPIO_NONE)
        #define PAD_GPIO17_PM_OEN (PAD_GPIO17_PM_IS_GPIO == GPIO_IN ? BIT0: 0)
        #define PAD_GPIO17_PM_OUT (PAD_GPIO17_PM_IS_GPIO == GPIO_OUT_HIGH ? BIT1: 0)
        _RVM1(0x0f22, PAD_GPIO17_PM_OUT, BIT1),
        _RVM1(0x0f22, PAD_GPIO17_PM_OEN, BIT0),
        //reg_spi2_gpio
        _RVM1(0x0e6a, 0, BIT6),   //reg[0e6a]#6 = 0b
    #endif

    #if(PAD_GPIO18_PM_IS_GPIO != GPIO_NONE)
        #define PAD_GPIO18_PM_OEN (PAD_GPIO18_PM_IS_GPIO == GPIO_IN ? BIT0: 0)
        #define PAD_GPIO18_PM_OUT (PAD_GPIO18_PM_IS_GPIO == GPIO_OUT_HIGH ? BIT1: 0)
        _RVM1(0x0f24, PAD_GPIO18_PM_OUT, BIT1),
        _RVM1(0x0f24, PAD_GPIO18_PM_OEN, BIT0),
        //reg_spi2_gpio
        _RVM1(0x0e6a, 0, BIT6),   //reg[0e6a]#6 = 0b
    #endif

    #if(PAD_GPIO19_PM_IS_GPIO != GPIO_NONE)
        #define PAD_GPIO19_PM_OEN (PAD_GPIO19_PM_IS_GPIO == GPIO_IN ? BIT0: 0)
        #define PAD_GPIO19_PM_OUT (PAD_GPIO19_PM_IS_GPIO == GPIO_OUT_HIGH ? BIT1: 0)
        _RVM1(0x0f26, PAD_GPIO19_PM_OUT, BIT1),
        _RVM1(0x0f26, PAD_GPIO19_PM_OEN, BIT0),
        //reg_spi2_gpio
        _RVM1(0x0e6a, 0, BIT6),   //reg[0e6a]#6 = 0b
    #endif

        //For FCIC _hank_
        _RVM1(0x0e6a, BIT3, BIT3),
        _RVM1(0x0e6a, BIT6, BIT6),

    #if(PAD_GPIO20_PM_IS_GPIO != GPIO_NONE)
        #define PAD_GPIO20_PM_OEN (PAD_GPIO20_PM_IS_GPIO == GPIO_IN ? BIT0: 0)
        #define PAD_GPIO20_PM_OUT (PAD_GPIO20_PM_IS_GPIO == GPIO_OUT_HIGH ? BIT1: 0)
        _RVM1(0x0f28, PAD_GPIO20_PM_OUT, BIT1),
        _RVM1(0x0f28, PAD_GPIO20_PM_OEN, BIT0),
    #endif

    #if(PAD_GPIO21_PM_IS_GPIO != GPIO_NONE)
        #define PAD_GPIO21_PM_OEN (PAD_GPIO21_PM_IS_GPIO == GPIO_IN ? BIT0: 0)
        #define PAD_GPIO21_PM_OUT (PAD_GPIO21_PM_IS_GPIO == GPIO_OUT_HIGH ? BIT1: 0)
        _RVM1(0x0f2a, PAD_GPIO21_PM_OUT, BIT1),
        _RVM1(0x0f2a, PAD_GPIO21_PM_OEN, BIT0),
    #endif

    #if(PAD_GPIO22_PM_IS_GPIO != GPIO_NONE)
        #define PAD_GPIO22_PM_OEN (PAD_GPIO22_PM_IS_GPIO == GPIO_IN ? BIT0: 0)
        #define PAD_GPIO22_PM_OUT (PAD_GPIO22_PM_IS_GPIO == GPIO_OUT_HIGH ? BIT1: 0)
        _RVM1(0x0f2c, PAD_GPIO22_PM_OUT, BIT1),
        _RVM1(0x0f2c, PAD_GPIO22_PM_OEN, BIT0),
    #endif

    #if(PAD_GPIO23_PM_IS_GPIO != GPIO_NONE)
        #define PAD_GPIO23_PM_OEN (PAD_GPIO23_PM_IS_GPIO == GPIO_IN ? BIT0: 0)
        #define PAD_GPIO23_PM_OUT (PAD_GPIO23_PM_IS_GPIO == GPIO_OUT_HIGH ? BIT1: 0)
        _RVM1(0x0f2e, PAD_GPIO23_PM_OUT, BIT1),
        _RVM1(0x0f2e, PAD_GPIO23_PM_OEN, BIT0),
    #endif

    #if(PAD_GPIO24_PM_IS_GPIO != GPIO_NONE)
        #define PAD_GPIO24_PM_OEN (PAD_GPIO24_PM_IS_GPIO == GPIO_IN ? BIT0: 0)
        #define PAD_GPIO24_PM_OUT (PAD_GPIO24_PM_IS_GPIO == GPIO_OUT_HIGH ? BIT1: 0)
        _RVM1(0x0f30, PAD_GPIO24_PM_OUT, BIT1),
        _RVM1(0x0f30, PAD_GPIO24_PM_OEN, BIT0),
    #endif

    #if(PAD_LED0_IS_GPIO != GPIO_NONE)
        #define PAD_LED0_OEN (PAD_LED0_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_LED0_OUT (PAD_LED0_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2e80, PAD_LED0_OUT, BIT0),
        _RVM1(0x2e80, PAD_LED0_OEN, BIT1),
        //reg_seperate_wol_led_is_gpio
        _RVM1(0x0e39, BIT7, BIT7),   //reg[0e39]#7 = 1b
        //reg_led_is_gpio
        _RVM1(0x0e39, BIT0, BIT0),   //reg[0e39]#0 = 1b
    #endif

    #if(PAD_LED1_IS_GPIO != GPIO_NONE)
        #define PAD_LED1_OEN (PAD_LED1_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_LED1_OUT (PAD_LED1_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2e81, PAD_LED1_OUT, BIT0),
        _RVM1(0x2e81, PAD_LED1_OEN, BIT1),
        //reg_seperate_wol_led_is_gpio
        _RVM1(0x0e39, BIT7, BIT7),   //reg[0e39]#7 = 1b
        //reg_led_is_gpio
        _RVM1(0x0e39, BIT0, BIT0),   //reg[0e39]#0 = 1b
    #endif

    #if(PAD_HOTPLUGA_IS_GPIO != GPIO_NONE)
        #define PAD_HOTPLUGA_OEN (PAD_HOTPLUGA_IS_GPIO == GPIO_IN ? BIT0: 0)
        #define PAD_HOTPLUGA_OUT (PAD_HOTPLUGA_IS_GPIO == GPIO_OUT_HIGH ? BIT4: 0)
        _RVM1(0x0e4e, PAD_HOTPLUGA_OUT, BIT4),
        _RVM1(0x0e4e, PAD_HOTPLUGA_OEN, BIT0),
        #define PAD_HOTPLUGA_PU1K (PAD_HOTPLUGA_IS_GPIO == GPIO_IN ? BIT4: 0)
        _RVM1(0x0e6d, PAD_HOTPLUGA_PU1K, BIT4),
        //reg_riu_dummy[0]
        _RVM1(0x0e69, 0, BIT0),   //reg[0e69]#0 = 0b
    #endif

    #if(PAD_HOTPLUGB_IS_GPIO != GPIO_NONE)
        #define PAD_HOTPLUGB_OEN (PAD_HOTPLUGB_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_HOTPLUGB_OUT (PAD_HOTPLUGB_IS_GPIO == GPIO_OUT_HIGH ? BIT5: 0)
        _RVM1(0x0e4e, PAD_HOTPLUGB_OUT, BIT5),
        _RVM1(0x0e4e, PAD_HOTPLUGB_OEN, BIT1),
        //defaultisgpio
        #define PAD_HOTPLUGB_PU1K (PAD_HOTPLUGA_IS_GPIO == GPIO_IN ? BIT5: 0)
        _RVM1(0x0e6d, PAD_HOTPLUGB_PU1K, BIT5),
    #endif

    #if(PAD_HOTPLUGC_IS_GPIO != GPIO_NONE)
        #define PAD_HOTPLUGC_OEN (PAD_HOTPLUGC_IS_GPIO == GPIO_IN ? BIT2: 0)
        #define PAD_HOTPLUGC_OUT (PAD_HOTPLUGC_IS_GPIO == GPIO_OUT_HIGH ? BIT6: 0)
        _RVM1(0x0e4e, PAD_HOTPLUGC_OUT, BIT6),
        _RVM1(0x0e4e, PAD_HOTPLUGC_OEN, BIT2),
        //reg_hplugc_mhl_en
        _RVM1(0x0ee6, 0, BIT0),   //reg[0ee6]#0 = 0
        //reg_hplugc_gpio_en
        _RVM1(0x0ee6, 1, BIT6),   //reg[0ee6]#6 = 1
        #define PAD_HOTPLUGC_PU1K (PAD_HOTPLUGA_IS_GPIO == GPIO_IN ? BIT6: 0)
        _RVM1(0x0e6d, PAD_HOTPLUGC_PU1K, BIT6),
    #endif

    #if(PAD_HOTPLUGD_IS_GPIO != GPIO_NONE)
        #define PAD_HOTPLUGD_OEN (PAD_HOTPLUGD_IS_GPIO == GPIO_IN ? BIT3: 0)
        #define PAD_HOTPLUGD_OUT (PAD_HOTPLUGD_IS_GPIO == GPIO_OUT_HIGH ? BIT7: 0)
        _RVM1(0x0e4e, PAD_HOTPLUGD_OUT, BIT7),
        _RVM1(0x0e4e, PAD_HOTPLUGD_OEN, BIT3),
        #define PAD_HOTPLUGD_PU1K (PAD_HOTPLUGA_IS_GPIO == GPIO_IN ? BIT7: 0)
        _RVM1(0x0e6d, PAD_HOTPLUGD_PU1K, BIT7),
    #endif

    #if(PAD_HOTPLUGA_HDMI20_5V_IS_GPIO != GPIO_NONE)
        #define PAD_HOTPLUGA_HDMI20_5V_OEN (PAD_HOTPLUGA_HDMI20_5V_IS_GPIO == GPIO_IN ? BIT5: 0)
        #define PAD_HOTPLUGA_HDMI20_5V_OUT (PAD_HOTPLUGA_HDMI20_5V_IS_GPIO == GPIO_OUT_HIGH ? BIT4: 0)
_MEMMAP_nonPM_01_,
        _RVM1(0x0218, PAD_HOTPLUGA_HDMI20_5V_OUT, BIT4),
        _RVM1(0x0218, PAD_HOTPLUGA_HDMI20_5V_OEN, BIT5),
    #endif

    #if(PAD_HOTPLUGB_HDMI20_5V_IS_GPIO != GPIO_NONE)
        #define PAD_HOTPLUGB_HDMI20_5V_OEN (PAD_HOTPLUGB_HDMI20_5V_IS_GPIO == GPIO_IN ? BIT5: 0)
        #define PAD_HOTPLUGB_HDMI20_5V_OUT (PAD_HOTPLUGB_HDMI20_5V_IS_GPIO == GPIO_OUT_HIGH ? BIT4: 0)
        _RVM1(0x0318, PAD_HOTPLUGB_HDMI20_5V_OUT, BIT4),
        _RVM1(0x0318, PAD_HOTPLUGB_HDMI20_5V_OEN, BIT5),
    #endif

    #if(PAD_HOTPLUGC_HDMI20_5V_IS_GPIO != GPIO_NONE)
        #define PAD_HOTPLUGC_HDMI20_5V_OEN (PAD_HOTPLUGC_HDMI20_5V_IS_GPIO == GPIO_IN ? BIT5: 0)
        #define PAD_HOTPLUGC_HDMI20_5V_OUT (PAD_HOTPLUGC_HDMI20_5V_IS_GPIO == GPIO_OUT_HIGH ? BIT4: 0)
        _RVM1(0x0418, PAD_HOTPLUGC_HDMI20_5V_OUT, BIT4),
        _RVM1(0x0418, PAD_HOTPLUGC_HDMI20_5V_OEN, BIT5),
    #endif

    #if(PAD_HOTPLUGD_HDMI20_5V_IS_GPIO != GPIO_NONE)
        #define PAD_HOTPLUGD_HDMI20_5V_OEN (PAD_HOTPLUGD_HDMI20_5V_IS_GPIO == GPIO_IN ? BIT5: 0)
        #define PAD_HOTPLUGD_HDMI20_5V_OUT (PAD_HOTPLUGD_HDMI20_5V_IS_GPIO == GPIO_OUT_HIGH ? BIT4: 0)
        _RVM1(0x0518, PAD_HOTPLUGD_HDMI20_5V_OUT, BIT4),
        _RVM1(0x0518, PAD_HOTPLUGD_HDMI20_5V_OEN, BIT5),
_MEMMAP_PM_,
    #endif

    #if(PAD_DDCDA_CK_IS_GPIO != GPIO_NONE)
        #define PAD_DDCDA_CK_OEN (PAD_DDCDA_CK_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_DDCDA_CK_OUT (PAD_DDCDA_CK_IS_GPIO == GPIO_OUT_HIGH ? BIT2: 0)
        _RVM1(0x0496, PAD_DDCDA_CK_OUT, BIT2),
        _RVM1(0x0496, PAD_DDCDA_CK_OEN, BIT1),
        //reg_ej_mode
        _RVM1(0x1e70, 0, BIT0),   //reg[1e70]#0 = 0b
        //reg_gpio2do_en
        _RVM1(0x0496, BIT7, BIT7),   //reg[0496]#7 = 1b
    #endif

    #if(PAD_DDCDA_DA_IS_GPIO != GPIO_NONE)
        #define PAD_DDCDA_DA_OEN (PAD_DDCDA_DA_IS_GPIO == GPIO_IN ? BIT5: 0)
        #define PAD_DDCDA_DA_OUT (PAD_DDCDA_DA_IS_GPIO == GPIO_OUT_HIGH ? BIT6: 0)
        _RVM1(0x0496, PAD_DDCDA_DA_OUT, BIT6),
        _RVM1(0x0496, PAD_DDCDA_DA_OEN, BIT5),
        //reg_ej_mode
        _RVM1(0x1e70, 0, BIT0),   //reg[1e70]#0 = 0b
        //reg_gpio2do_en
        _RVM1(0x0496, BIT7, BIT7),   //reg[0496]#7 = 1b
    #endif

    #if(PAD_DDCDB_CK_IS_GPIO != GPIO_NONE)
        #define PAD_DDCDB_CK_OEN (PAD_DDCDB_CK_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_DDCDB_CK_OUT (PAD_DDCDB_CK_IS_GPIO == GPIO_OUT_HIGH ? BIT2: 0)
        _RVM1(0x0497, PAD_DDCDB_CK_OUT, BIT2),
        _RVM1(0x0497, PAD_DDCDB_CK_OEN, BIT1),
        //reg_ej_mode
        _RVM1(0x1e70, 0, BIT0),   //reg[1e70]#0 = 0b
        //reg_gpio2d1_en
        _RVM1(0x0497, BIT7, BIT7),   //reg[0497]#7 = 1b
    #endif

    #if(PAD_DDCDB_DA_IS_GPIO != GPIO_NONE)
        #define PAD_DDCDB_DA_OEN (PAD_DDCDB_DA_IS_GPIO == GPIO_IN ? BIT5: 0)
        #define PAD_DDCDB_DA_OUT (PAD_DDCDB_DA_IS_GPIO == GPIO_OUT_HIGH ? BIT6: 0)
        _RVM1(0x0497, PAD_DDCDB_DA_OUT, BIT6),
        _RVM1(0x0497, PAD_DDCDB_DA_OEN, BIT5),
        //reg_ej_mode
        _RVM1(0x1e70, 0, BIT0),   //reg[1e70]#0 = 0b
        //reg_gpio2d1_en
        _RVM1(0x0497, BIT7, BIT7),   //reg[0497]#7 = 1b
    #endif

    #if(PAD_DDCDC_CK_IS_GPIO != GPIO_NONE)
        #define PAD_DDCDC_CK_OEN (PAD_DDCDC_CK_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_DDCDC_CK_OUT (PAD_DDCDC_CK_IS_GPIO == GPIO_OUT_HIGH ? BIT2: 0)
        _RVM1(0x0498, PAD_DDCDC_CK_OUT, BIT2),
        _RVM1(0x0498, PAD_DDCDC_CK_OEN, BIT1),
        //reg_gpio2d2_en
        _RVM1(0x0498, BIT7, BIT7),   //reg[0498]#7 = 1b
    #endif

    #if(PAD_DDCDC_DA_IS_GPIO != GPIO_NONE)
        #define PAD_DDCDC_DA_OEN (PAD_DDCDC_DA_IS_GPIO == GPIO_IN ? BIT5: 0)
        #define PAD_DDCDC_DA_OUT (PAD_DDCDC_DA_IS_GPIO == GPIO_OUT_HIGH ? BIT6: 0)
        _RVM1(0x0498, PAD_DDCDC_DA_OUT, BIT6),
        _RVM1(0x0498, PAD_DDCDC_DA_OEN, BIT5),
        //reg_gpio2d2_en
        _RVM1(0x0498, BIT7, BIT7),   //reg[0498]#7 = 1b
    #endif

    #if(PAD_DDCDD_CK_IS_GPIO != GPIO_NONE)
        #define PAD_DDCDD_CK_OEN (PAD_DDCDD_CK_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_DDCDD_CK_OUT (PAD_DDCDD_CK_IS_GPIO == GPIO_OUT_HIGH ? BIT2: 0)
        _RVM1(0x0499, PAD_DDCDD_CK_OUT, BIT2),
        _RVM1(0x0499, PAD_DDCDD_CK_OEN, BIT1),
        //reg_gpio2d3_en
        _RVM1(0x0499, BIT7, BIT7),   //reg[0499]#7 = 1b
    #endif

    #if(PAD_DDCDD_DA_IS_GPIO != GPIO_NONE)
        #define PAD_DDCDD_DA_OEN (PAD_DDCDD_DA_IS_GPIO == GPIO_IN ? BIT5: 0)
        #define PAD_DDCDD_DA_OUT (PAD_DDCDD_DA_IS_GPIO == GPIO_OUT_HIGH ? BIT6: 0)
        _RVM1(0x0499, PAD_DDCDD_DA_OUT, BIT6),
        _RVM1(0x0499, PAD_DDCDD_DA_OEN, BIT5),
        //reg_gpio2d3_en
        _RVM1(0x0499, BIT7, BIT7),   //reg[0499]#7 = 1b
    #endif

    #if(PAD_SAR0_IS_GPIO != GPIO_NONE)
        #define PAD_SAR0_OEN (PAD_SAR0_IS_GPIO == GPIO_IN ? BIT0: 0)
        #define PAD_SAR0_OUT (PAD_SAR0_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x1424, PAD_SAR0_OUT, BIT0),
        _RVM1(0x1423, PAD_SAR0_OEN, BIT0),
        //reg_sar_aisel[0]
        _RVM1(0x1422, 0, BIT0),   //reg[1422]#0 = 0b
    #endif

    #if(PAD_SAR1_IS_GPIO != GPIO_NONE)
        #define PAD_SAR1_OEN (PAD_SAR1_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_SAR1_OUT (PAD_SAR1_IS_GPIO == GPIO_OUT_HIGH ? BIT1: 0)
        _RVM1(0x1424, PAD_SAR1_OUT, BIT1),
        _RVM1(0x1423, PAD_SAR1_OEN, BIT1),
        //reg_sar_aisel[1]
        _RVM1(0x1422, 0, BIT1),   //reg[1422]#1 = 0b
    #endif

    #if(PAD_SAR2_IS_GPIO != GPIO_NONE)
        #define PAD_SAR2_OEN (PAD_SAR2_IS_GPIO == GPIO_IN ? BIT2: 0)
        #define PAD_SAR2_OUT (PAD_SAR2_IS_GPIO == GPIO_OUT_HIGH ? BIT2: 0)
        _RVM1(0x1424, PAD_SAR2_OUT, BIT2),
        _RVM1(0x1423, PAD_SAR2_OEN, BIT2),
        //reg_sar_aisel[2]
        _RVM1(0x1422, 0, BIT2),   //reg[1422]#2 = 0b
    #endif

    #if(PAD_SAR3_IS_GPIO != GPIO_NONE)
        #define PAD_SAR3_OEN (PAD_SAR3_IS_GPIO == GPIO_IN ? BIT3: 0)
        #define PAD_SAR3_OUT (PAD_SAR3_IS_GPIO == GPIO_OUT_HIGH ? BIT3: 0)
        _RVM1(0x1424, PAD_SAR3_OUT, BIT3),
        _RVM1(0x1423, PAD_SAR3_OEN, BIT3),
        //reg_sar_aisel[3]
        _RVM1(0x1422, 0, BIT3),   //reg[1422]#3 = 0b
    #endif

    #if(PAD_SAR4_IS_GPIO != GPIO_NONE)
        #define PAD_SAR4_OEN (PAD_SAR4_IS_GPIO == GPIO_IN ? BIT4: 0)
        #define PAD_SAR4_OUT (PAD_SAR4_IS_GPIO == GPIO_OUT_HIGH ? BIT4: 0)
        _RVM1(0x1424, PAD_SAR4_OUT, BIT4),
        _RVM1(0x1423, PAD_SAR4_OEN, BIT4),
        //reg_sar_aisel[4]
        _RVM1(0x1422, 0, BIT4),   //reg[1422]#4 = 0b
    #endif

    #if(PAD_VPLUGIN_IS_GPIO != GPIO_NONE)
        #define PAD_VPLUGIN_OEN (PAD_VPLUGIN_IS_GPIO == GPIO_IN ? BIT5: 0)
        #define PAD_VPLUGIN_OUT (PAD_VPLUGIN_IS_GPIO == GPIO_OUT_HIGH ? BIT5: 0)
        _RVM1(0x1424, PAD_VPLUGIN_OUT, BIT5),
        _RVM1(0x1423, PAD_VPLUGIN_OEN, BIT5),
        //reg_sar_aisel[5]
        _RVM1(0x1422, 0, BIT5),   //reg[1422]#5 = 0b
    #endif

    #if(PAD_WOL_INT_OUT_IS_GPIO != GPIO_NONE)
        #define PAD_WOL_INT_OUT_OEN (PAD_WOL_INT_OUT_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_WOL_INT_OUT_OUT (PAD_WOL_INT_OUT_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2e82, PAD_WOL_INT_OUT_OUT, BIT0),
        _RVM1(0x2e82, PAD_WOL_INT_OUT_OEN, BIT1),
        //reg_wol_is_gpio
        _RVM1(0x0e39, BIT1, BIT1),   //reg[0e39]#1 = 1b
    #endif

_MEMMAP_nonPM_,
    #if(PAD_DDCR_CK_IS_GPIO != GPIO_NONE)
        #define PAD_DDCR_CK_OEN (PAD_DDCR_CK_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_DDCR_CK_OUT (PAD_DDCR_CK_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2b87, PAD_DDCR_CK_OUT, BIT0),
        _RVM1(0x2b87, PAD_DDCR_CK_OEN, BIT1),
        //reg_test_in_mode[2:0]
        //_RVM1(0x1e24, 0, 0x07),   //reg[101e24]#2 ~ #0 = 000b
        //reg_test_out_mode[2:0]
        //_RVM1(0x1e24, 0, 0x70),   //reg[101e24]#6 ~ #4 = 000b
        //reg_ddcrmode[1:0]
        _RVM1(0x1eae, 0, BIT1 | BIT0),   //reg[101eae]#1 ~ #0 = 00b
        //reg_ddcrmode[1:0]
        _RVM1(0x1eae, 0, BIT1 | BIT0),   //reg[101eae]#1 ~ #0 = 00b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_DDCR_DA_IS_GPIO != GPIO_NONE)
        #define PAD_DDCR_DA_OEN (PAD_DDCR_DA_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_DDCR_DA_OUT (PAD_DDCR_DA_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2b86, PAD_DDCR_DA_OUT, BIT0),
        _RVM1(0x2b86, PAD_DDCR_DA_OEN, BIT1),
        //reg_test_in_mode[2:0]
        //_RVM1(0x1e24, 0, 0x07),   //reg[101e24]#2 ~ #0 = 000b
        //reg_test_out_mode[2:0]
        //_RVM1(0x1e24, 0, 0x70),   //reg[101e24]#6 ~ #4 = 000b
        //reg_ddcrmode[1:0]
        _RVM1(0x1eae, 0, BIT1 | BIT0),   //reg[101eae]#1 ~ #0 = 00b
        //reg_ddcrmode[1:0]
        _RVM1(0x1eae, 0, BIT1 | BIT0),   //reg[101eae]#1 ~ #0 = 00b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_DIM0_IS_GPIO != GPIO_NONE)
        #define PAD_DIM0_OEN (PAD_DIM0_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_DIM0_OUT (PAD_DIM0_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2bb1, PAD_DIM0_OUT, BIT0),
        _RVM1(0x2bb1, PAD_DIM0_OEN, BIT1),
        //reg_dim_config
        _RVM1(0x1ef6, 0, BIT0),   //reg[101ef6]#0 = 0b
        //reg_vx1gpi_osd_mode
        _RVM1(0x1e4a, 0, BIT1),   //reg[101e4a]#1 = 0b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_DIM1_IS_GPIO != GPIO_NONE)
        #define PAD_DIM1_OEN (PAD_DIM1_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_DIM1_OUT (PAD_DIM1_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2bb2, PAD_DIM1_OUT, BIT0),
        _RVM1(0x2bb2, PAD_DIM1_OEN, BIT1),
        //reg_dim_config
        _RVM1(0x1ef6, 0, BIT0),   //reg[101ef6]#0 = 0b
        //reg_vx1gpi_osd_mode
        _RVM1(0x1e4a, 0, BIT1),   //reg[101e4a]#1 = 0b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_DIM2_IS_GPIO != GPIO_NONE)
        #define PAD_DIM2_OEN (PAD_DIM2_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_DIM2_OUT (PAD_DIM2_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2bb3, PAD_DIM2_OUT, BIT0),
        _RVM1(0x2bb3, PAD_DIM2_OEN, BIT1),
        //reg_dim_config
        _RVM1(0x1ef6, 0, BIT0),   //reg[101ef6]#0 = 0b
        //reg_fifthuartmode[1:0]
        _RVM1(0x1e08, 0, BIT3 | BIT2),   //reg[101e08]#3 ~ #2 = 00b
        //reg_od5thuart[1:0]
        _RVM1(0x1eaa, 0, BIT5 | BIT4),   //reg[101eaa]#5 ~ #4 = 00b
        //reg_vx1gpi_mode
        _RVM1(0x1e4a, 0, BIT0),   //reg[101e4a]#0 = 0b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_DIM3_IS_GPIO != GPIO_NONE)
        #define PAD_DIM3_OEN (PAD_DIM3_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_DIM3_OUT (PAD_DIM3_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2bb4, PAD_DIM3_OUT, BIT0),
        _RVM1(0x2bb4, PAD_DIM3_OEN, BIT1),
        //reg_dim_config
        _RVM1(0x1ef6, 0, BIT0),   //reg[101ef6]#0 = 0b
        //reg_fifthuartmode[1:0]
        _RVM1(0x1e08, 0, BIT3 | BIT2),   //reg[101e08]#3 ~ #2 = 00b
        //reg_od5thuart[1:0]
        _RVM1(0x1eaa, 0, BIT5 | BIT4),   //reg[101eaa]#5 ~ #4 = 00b
        //reg_vx1gpi_mode
        _RVM1(0x1e4a, 0, BIT0),   //reg[101e4a]#0 = 0b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_GPIO0_IS_GPIO != GPIO_NONE)
        #define PAD_GPIO0_OEN (PAD_GPIO0_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_GPIO0_OUT (PAD_GPIO0_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2b00, PAD_GPIO0_OUT, BIT0),
        _RVM1(0x2b00, PAD_GPIO0_OEN, BIT1),
        //reg_test_out_mode[2:0]
        //_RVM1(0x1e24, 0, 0x70),   //reg[101e24]#6 ~ #4 = 000b
        //reg_pcm_config
        _RVM1(0x1ef6, 0, BIT1),   //reg[101ef6]#1 = 0b
        //reg_p1_enable[0]
        _RVM1(0x1ea4, 0, BIT0),   //reg[101ea4]#0 = 0b
        //reg_lg_earc_mode
        _RVM1(0x1e0b, 0, BIT7),   //reg[101e0b]#7 = 0b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_GPIO1_IS_GPIO != GPIO_NONE)
        #define PAD_GPIO1_OEN (PAD_GPIO1_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_GPIO1_OUT (PAD_GPIO1_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2b01, PAD_GPIO1_OUT, BIT0),
        _RVM1(0x2b01, PAD_GPIO1_OEN, BIT1),
        //reg_test_out_mode[2:0]
        //_RVM1(0x1e24, 0, 0x70),   //reg[101e24]#6 ~ #4 = 000b
        //reg_pcm_config
        _RVM1(0x1ef6, 0, BIT1),   //reg[101ef6]#1 = 0b
        //reg_p1_enable[1]
        _RVM1(0x1ea4, 0, BIT1),   //reg[101ea4]#1 = 0b
        //reg_lg_earc_mode
        _RVM1(0x1e0b, 0, BIT7),   //reg[101e0b]#7 = 0b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_GPIO2_IS_GPIO != GPIO_NONE)
        #define PAD_GPIO2_OEN (PAD_GPIO2_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_GPIO2_OUT (PAD_GPIO2_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2b02, PAD_GPIO2_OUT, BIT0),
        _RVM1(0x2b02, PAD_GPIO2_OEN, BIT1),
        //reg_test_out_mode[2:0]
        //_RVM1(0x1e24, 0, 0x70),   //reg[101e24]#6 ~ #4 = 000b
        //reg_pcm_config
        _RVM1(0x1ef6, 0, BIT1),   //reg[101ef6]#1 = 0b
        //reg_ld_spi2_config[1:0]
        _RVM1(0x1e9c, 0, BIT3 | BIT2),   //reg[101e9c]#3 ~ #2 = 00b
        //reg_ld_spi3_config[1:0]
        _RVM1(0x1e9c, 0, BIT5 | BIT4),   //reg[101e9c]#5 ~ #4 = 00b
        //reg_dim_config
        _RVM1(0x1ef6, 0, BIT0),   //reg[101ef6]#0 = 0b
        //reg_p1_enable[2]
        _RVM1(0x1ea4, 0, BIT2),   //reg[101ea4]#2 = 0b
        //reg_3dflagconfig[1:0]
        _RVM1(0x1eb3, 0, BIT7 | BIT6),   //reg[101eb3]#7 ~ #6 = 00b
        //reg_osd3dflag_config[1:0]
        _RVM1(0x1ef6, 0, BIT7 | BIT6),   //reg[101ef6]#7 ~ #6 = 00b
        //reg_usb30vctl_config[1:0]
        _RVM1(0x1e08, 0, BIT5 | BIT4),   //reg[101e08]#5 ~ #4 = 00b
        //reg_usb30vctl1_config[1:0]
        _RVM1(0x1e09, 0, BIT1 | BIT0),   //reg[101e09]#1 ~ #0 = 00b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_GPIO3_IS_GPIO != GPIO_NONE)
        #define PAD_GPIO3_OEN (PAD_GPIO3_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_GPIO3_OUT (PAD_GPIO3_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2b03, PAD_GPIO3_OUT, BIT0),
        _RVM1(0x2b03, PAD_GPIO3_OEN, BIT1),
        //reg_test_out_mode[2:0]
        //_RVM1(0x1e24, 0, 0x70),   //reg[101e24]#6 ~ #4 = 000b
        //reg_pcm_config
        _RVM1(0x1ef6, 0, BIT1),   //reg[101ef6]#1 = 0b
        //reg_ld_spi2_config[1:0]
        _RVM1(0x1e9c, 0, BIT3 | BIT2),   //reg[101e9c]#3 ~ #2 = 00b
        //reg_ld_spi3_config[1:0]
        _RVM1(0x1e9c, 0, BIT5 | BIT4),   //reg[101e9c]#5 ~ #4 = 00b
        //reg_dim_config
        _RVM1(0x1ef6, 0, BIT0),   //reg[101ef6]#0 = 0b
        //reg_seconduartmode[1:0]
        _RVM1(0x1e05, 0, BIT1 | BIT0),   //reg[101e05]#1 ~ #0 = 00b
        //reg_od2nduart[1:0]
        _RVM1(0x1ea9, 0, BIT1 | BIT0),   //reg[101ea9]#1 ~ #0 = 00b
        //reg_p1_enable[3]
        _RVM1(0x1ea4, 0, BIT3),   //reg[101ea4]#3 = 0b
        //reg_usb30vctl_config[1:0]
        _RVM1(0x1e08, 0, BIT5 | BIT4),   //reg[101e08]#5 ~ #4 = 00b
        //reg_usb30vctl1_config[1:0]
        _RVM1(0x1e09, 0, BIT1 | BIT0),   //reg[101e09]#1 ~ #0 = 00b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_GPIO4_IS_GPIO != GPIO_NONE)
        #define PAD_GPIO4_OEN (PAD_GPIO4_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_GPIO4_OUT (PAD_GPIO4_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2b04, PAD_GPIO4_OUT, BIT0),
        _RVM1(0x2b04, PAD_GPIO4_OEN, BIT1),
        //reg_test_out_mode[2:0]
        //_RVM1(0x1e24, 0, 0x70),   //reg[101e24]#6 ~ #4 = 000b
        //reg_ld_spi1_config[1:0]
        _RVM1(0x1e9c, 0, BIT1 | BIT0),   //reg[101e9c]#1 ~ #0 = 00b
        //reg_ld_spi3_config[1:0]
        _RVM1(0x1e9c, 0, BIT5 | BIT4),   //reg[101e9c]#5 ~ #4 = 00b
        //reg_dim_config
        _RVM1(0x1ef6, 0, BIT0),   //reg[101ef6]#0 = 0b
        //reg_seconduartmode[1:0]
        _RVM1(0x1e05, 0, BIT1 | BIT0),   //reg[101e05]#1 ~ #0 = 00b
        //reg_od2nduart[1:0]
        _RVM1(0x1ea9, 0, BIT1 | BIT0),   //reg[101ea9]#1 ~ #0 = 00b
        //reg_p1_enable[4]
        _RVM1(0x1ea4, 0, BIT4),   //reg[101ea4]#4 = 0b
        //reg_usb30vctl_config[1:0]
        _RVM1(0x1e08, 0, BIT5 | BIT4),   //reg[101e08]#5 ~ #4 = 00b
        //reg_usb30vctl1_config[1:0]
        _RVM1(0x1e09, 0, BIT1 | BIT0),   //reg[101e09]#1 ~ #0 = 00b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_GPIO5_IS_GPIO != GPIO_NONE)
        #define PAD_GPIO5_OEN (PAD_GPIO5_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_GPIO5_OUT (PAD_GPIO5_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2b05, PAD_GPIO5_OUT, BIT0),
        _RVM1(0x2b05, PAD_GPIO5_OEN, BIT1),
        //reg_test_out_mode[2:0]
        //_RVM1(0x1e24, 0, 0x70),   //reg[101e24]#6 ~ #4 = 000b
        //reg_ts4config[2:0]
        _RVM1(0x1e81, 0, 0x0E),   //reg[101e81]#3 ~ #1 = 000b
        //reg_ts4config[2:0]
        _RVM1(0x1e81, 0, 0x0E),   //reg[101e81]#3 ~ #1 = 000b
        //reg_ld_spi1_config[1:0]
        _RVM1(0x1e9c, 0, BIT1 | BIT0),   //reg[101e9c]#1 ~ #0 = 00b
        //reg_ld_spi3_config[1:0]
        _RVM1(0x1e9c, 0, BIT5 | BIT4),   //reg[101e9c]#5 ~ #4 = 00b
        //reg_dim_config
        _RVM1(0x1ef6, 0, BIT0),   //reg[101ef6]#0 = 0b
        //reg_fastuartmode[1:0]
        _RVM1(0x1e04, 0, BIT5 | BIT4),   //reg[101e04]#5 ~ #4 = 00b
        //reg_odfuart[1:0]
        _RVM1(0x1ea9, 0, BIT7 | BIT6),   //reg[101ea9]#7 ~ #6 = 00b
        //reg_p1_enable[5]
        _RVM1(0x1ea4, 0, BIT5),   //reg[101ea4]#5 = 0b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_GPIO6_IS_GPIO != GPIO_NONE)
        #define PAD_GPIO6_OEN (PAD_GPIO6_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_GPIO6_OUT (PAD_GPIO6_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2b06, PAD_GPIO6_OUT, BIT0),
        _RVM1(0x2b06, PAD_GPIO6_OEN, BIT1),
        //reg_test_out_mode[2:0]
        //_RVM1(0x1e24, 0, 0x70),   //reg[101e24]#6 ~ #4 = 000b
        //reg_ts4config[2:0]
        _RVM1(0x1e81, 0, 0x0E),   //reg[101e81]#3 ~ #1 = 000b
        //reg_ts4config[2:0]
        _RVM1(0x1e81, 0, 0x0E),   //reg[101e81]#3 ~ #1 = 000b
        //reg_fastuartmode[1:0]
        _RVM1(0x1e04, 0, BIT5 | BIT4),   //reg[101e04]#5 ~ #4 = 00b
        //reg_odfuart[1:0]
        _RVM1(0x1ea9, 0, BIT7 | BIT6),   //reg[101ea9]#7 ~ #6 = 00b
        //reg_p1_enable[6]
        _RVM1(0x1ea4, 0, BIT6),   //reg[101ea4]#6 = 0b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_GPIO7_IS_GPIO != GPIO_NONE)
        #define PAD_GPIO7_OEN (PAD_GPIO7_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_GPIO7_OUT (PAD_GPIO7_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2b07, PAD_GPIO7_OUT, BIT0),
        _RVM1(0x2b07, PAD_GPIO7_OEN, BIT1),
        //reg_test_out_mode[2:0]
        //_RVM1(0x1e24, 0, 0x70),   //reg[101e24]#6 ~ #4 = 000b
        //reg_ts4config[2:0]
        _RVM1(0x1e81, 0, 0x0E),   //reg[101e81]#3 ~ #1 = 000b
        //reg_ts4config[2:0]
        _RVM1(0x1e81, 0, 0x0E),   //reg[101e81]#3 ~ #1 = 000b
        //reg_seconduartmode[1:0]
        _RVM1(0x1e05, 0, BIT1 | BIT0),   //reg[101e05]#1 ~ #0 = 00b
        //reg_od2nduart[1:0]
        _RVM1(0x1ea9, 0, BIT1 | BIT0),   //reg[101ea9]#1 ~ #0 = 00b
        //reg_p1_enable[7]
        _RVM1(0x1ea4, 0, BIT7),   //reg[101ea4]#7 = 0b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_GPIO8_IS_GPIO != GPIO_NONE)
        #define PAD_GPIO8_OEN (PAD_GPIO8_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_GPIO8_OUT (PAD_GPIO8_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2b08, PAD_GPIO8_OUT, BIT0),
        _RVM1(0x2b08, PAD_GPIO8_OEN, BIT1),
        //reg_test_out_mode[2:0]
        //_RVM1(0x1e24, 0, 0x70),   //reg[101e24]#6 ~ #4 = 000b
        //reg_ts4config[2:0]
        _RVM1(0x1e81, 0, 0x0E),   //reg[101e81]#3 ~ #1 = 000b
        //reg_ts4config[2:0]
        _RVM1(0x1e81, 0, 0x0E),   //reg[101e81]#3 ~ #1 = 000b
        //reg_seconduartmode[1:0]
        _RVM1(0x1e05, 0, BIT1 | BIT0),   //reg[101e05]#1 ~ #0 = 00b
        //reg_fifthuartmode[1:0]
        _RVM1(0x1e08, 0, BIT3 | BIT2),   //reg[101e08]#3 ~ #2 = 00b
        //reg_od2nduart[1:0]
        _RVM1(0x1ea9, 0, BIT1 | BIT0),   //reg[101ea9]#1 ~ #0 = 00b
        //reg_od5thuart[1:0]
        _RVM1(0x1eaa, 0, BIT5 | BIT4),   //reg[101eaa]#5 ~ #4 = 00b
        //reg_pdtracectrl[1:0]
        _RVM1(0x1e9f, 0, BIT1 | BIT0),   //reg[101e9f]#1 ~ #0 = 00b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_GPIO9_IS_GPIO != GPIO_NONE)
        #define PAD_GPIO9_OEN (PAD_GPIO9_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_GPIO9_OUT (PAD_GPIO9_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2b09, PAD_GPIO9_OUT, BIT0),
        _RVM1(0x2b09, PAD_GPIO9_OEN, BIT1),
        //reg_thirduartmode[1:0]
        _RVM1(0x1e05, 0, BIT3 | BIT2),   //reg[101e05]#3 ~ #2 = 00b
        //reg_fifthuartmode[1:0]
        _RVM1(0x1e08, 0, BIT3 | BIT2),   //reg[101e08]#3 ~ #2 = 00b
        //reg_od3rduart[1:0]
        _RVM1(0x1ea9, 0, BIT3 | BIT2),   //reg[101ea9]#3 ~ #2 = 00b
        //reg_od5thuart[1:0]
        _RVM1(0x1eaa, 0, BIT5 | BIT4),   //reg[101eaa]#5 ~ #4 = 00b
        //reg_pdtracectrl[1:0]
        _RVM1(0x1e9f, 0, BIT1 | BIT0),   //reg[101e9f]#1 ~ #0 = 00b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_GPIO10_IS_GPIO != GPIO_NONE)
        #define PAD_GPIO10_OEN (PAD_GPIO10_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_GPIO10_OUT (PAD_GPIO10_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2b0a, PAD_GPIO10_OUT, BIT0),
        _RVM1(0x2b0a, PAD_GPIO10_OEN, BIT1),
        //reg_thirduartmode[1:0]
        _RVM1(0x1e05, 0, BIT3 | BIT2),   //reg[101e05]#3 ~ #2 = 00b
        //reg_od3rduart[1:0]
        _RVM1(0x1ea9, 0, BIT3 | BIT2),   //reg[101ea9]#3 ~ #2 = 00b
        //reg_pdtracectrl[1:0]
        _RVM1(0x1e9f, 0, BIT1 | BIT0),   //reg[101e9f]#1 ~ #0 = 00b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_GPIO11_IS_GPIO != GPIO_NONE)
        #define PAD_GPIO11_OEN (PAD_GPIO11_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_GPIO11_OUT (PAD_GPIO11_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2b0b, PAD_GPIO11_OUT, BIT0),
        _RVM1(0x2b0b, PAD_GPIO11_OEN, BIT1),
        //reg_fourthuartmode[1:0]
        _RVM1(0x1e04, 0, BIT7 | BIT6),   //reg[101e04]#7 ~ #6 = 00b
        //reg_od4thuart[1:0]
        _RVM1(0x1ea9, 0, BIT5 | BIT4),   //reg[101ea9]#5 ~ #4 = 00b
        //reg_pdtracectrl[1:0]
        _RVM1(0x1e9f, 0, BIT1 | BIT0),   //reg[101e9f]#1 ~ #0 = 00b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_GPIO12_IS_GPIO != GPIO_NONE)
        #define PAD_GPIO12_OEN (PAD_GPIO12_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_GPIO12_OUT (PAD_GPIO12_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2b0c, PAD_GPIO12_OUT, BIT0),
        _RVM1(0x2b0c, PAD_GPIO12_OEN, BIT1),
        //reg_fourthuartmode[1:0]
        _RVM1(0x1e04, 0, BIT7 | BIT6),   //reg[101e04]#7 ~ #6 = 00b
        //reg_od4thuart[1:0]
        _RVM1(0x1ea9, 0, BIT5 | BIT4),   //reg[101ea9]#5 ~ #4 = 00b
        //reg_pdtracectrl[1:0]
        _RVM1(0x1e9f, 0, BIT1 | BIT0),   //reg[101e9f]#1 ~ #0 = 00b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_GPIO13_IS_GPIO != GPIO_NONE)
        #define PAD_GPIO13_OEN (PAD_GPIO13_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_GPIO13_OUT (PAD_GPIO13_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2b0d, PAD_GPIO13_OUT, BIT0),
        _RVM1(0x2b0d, PAD_GPIO13_OEN, BIT1),
        //reg_fastuartmode[1:0]
        _RVM1(0x1e04, 0, BIT5 | BIT4),   //reg[101e04]#5 ~ #4 = 00b
        //reg_odfuart[1:0]
        _RVM1(0x1ea9, 0, BIT7 | BIT6),   //reg[101ea9]#7 ~ #6 = 00b
        //reg_pdtracectrl[1:0]
        _RVM1(0x1e9f, 0, BIT1 | BIT0),   //reg[101e9f]#1 ~ #0 = 00b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_GPIO14_IS_GPIO != GPIO_NONE)
        #define PAD_GPIO14_OEN (PAD_GPIO14_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_GPIO14_OUT (PAD_GPIO14_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2b0e, PAD_GPIO14_OUT, BIT0),
        _RVM1(0x2b0e, PAD_GPIO14_OEN, BIT1),
        //reg_fastuartmode[1:0]
        _RVM1(0x1e04, 0, BIT5 | BIT4),   //reg[101e04]#5 ~ #4 = 00b
        //reg_odfuart[1:0]
        _RVM1(0x1ea9, 0, BIT7 | BIT6),   //reg[101ea9]#7 ~ #6 = 00b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_GPIO15_IS_GPIO != GPIO_NONE)
        #define PAD_GPIO15_OEN (PAD_GPIO15_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_GPIO15_OUT (PAD_GPIO15_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2b0f, PAD_GPIO15_OUT, BIT0),
        _RVM1(0x2b0f, PAD_GPIO15_OEN, BIT1),
        //reg_i2smutemode[1:0]
        _RVM1(0x1e05, 0, BIT7 | BIT6),   //reg[101e05]#7 ~ #6 = 00b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_GPIO16_IS_GPIO != GPIO_NONE)
        #define PAD_GPIO16_OEN (PAD_GPIO16_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_GPIO16_OUT (PAD_GPIO16_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2b10, PAD_GPIO16_OUT, BIT0),
        _RVM1(0x2b10, PAD_GPIO16_OEN, BIT1),
        //reg_test_out_mode[2:0]
        //_RVM1(0x1e24, 0, 0x70),   //reg[101e24]#6 ~ #4 = 000b
        //reg_fifthuartmode[1:0]
        _RVM1(0x1e08, 0, BIT3 | BIT2),   //reg[101e08]#3 ~ #2 = 00b
        //reg_od5thuart[1:0]
        _RVM1(0x1eaa, 0, BIT5 | BIT4),   //reg[101eaa]#5 ~ #4 = 00b
        //reg_tserrout[1:0]
        _RVM1(0x1ec9, 0, BIT1 | BIT0),   //reg[101ec9]#1 ~ #0 = 00b
        //reg_tconconfig[4]
        _RVM1(0x1ea0, 0, BIT4),   //reg[101ea0]#4 = 0b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_GPIO17_IS_GPIO != GPIO_NONE)
        #define PAD_GPIO17_OEN (PAD_GPIO17_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_GPIO17_OUT (PAD_GPIO17_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2b11, PAD_GPIO17_OUT, BIT0),
        _RVM1(0x2b11, PAD_GPIO17_OEN, BIT1),
        //reg_test_out_mode[2:0]
        //_RVM1(0x1e24, 0, 0x70),   //reg[101e24]#6 ~ #4 = 000b
        //reg_tso_evd_mode[1:0]
        _RVM1(0x1e21, 0, BIT2 | BIT1),   //reg[101e21]#2 ~ #1 = 00b
        //reg_fifthuartmode[1:0]
        _RVM1(0x1e08, 0, BIT3 | BIT2),   //reg[101e08]#3 ~ #2 = 00b
        //reg_od5thuart[1:0]
        _RVM1(0x1eaa, 0, BIT5 | BIT4),   //reg[101eaa]#5 ~ #4 = 00b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_GPIO18_IS_GPIO != GPIO_NONE)
        #define PAD_GPIO18_OEN (PAD_GPIO18_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_GPIO18_OUT (PAD_GPIO18_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2b12, PAD_GPIO18_OUT, BIT0),
        _RVM1(0x2b12, PAD_GPIO18_OEN, BIT1),
        //reg_tso_evd_mode[1:0]
        _RVM1(0x1e21, 0, BIT2 | BIT1),   //reg[101e21]#2 ~ #1 = 00b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_GPIO19_IS_GPIO != GPIO_NONE)
        #define PAD_GPIO19_OEN (PAD_GPIO19_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_GPIO19_OUT (PAD_GPIO19_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2b13, PAD_GPIO19_OUT, BIT0),
        _RVM1(0x2b13, PAD_GPIO19_OEN, BIT1),
        //reg_agc_dbg
        _RVM1(0x1e9e, 0, BIT7),   //reg[101e9e]#7 = 0b
        //reg_tso_evd_mode[1:0]
        _RVM1(0x1e21, 0, BIT2 | BIT1),   //reg[101e21]#2 ~ #1 = 00b
        //reg_et_mode
        _RVM1(0x1edf, 0, BIT0),   //reg[101edf]#0 = 0b
        //reg_led_mode
        _RVM1(0x1eb4, 0, BIT4),   //reg[101eb4]#4 = 0b
        //reg_seconduartmode[1:0]
        _RVM1(0x1e05, 0, BIT1 | BIT0),   //reg[101e05]#1 ~ #0 = 00b
        //reg_od2nduart[1:0]
        _RVM1(0x1ea9, 0, BIT1 | BIT0),   //reg[101ea9]#1 ~ #0 = 00b
        //reg_miic_mode3[1:0]
        _RVM1(0x1edf, 0, BIT2 | BIT1),   //reg[101edf]#2 ~ #1 = 00b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_GPIO20_IS_GPIO != GPIO_NONE)
        #define PAD_GPIO20_OEN (PAD_GPIO20_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_GPIO20_OUT (PAD_GPIO20_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2b14, PAD_GPIO20_OUT, BIT0),
        _RVM1(0x2b14, PAD_GPIO20_OEN, BIT1),
        //reg_agc_dbg
        _RVM1(0x1e9e, 0, BIT7),   //reg[101e9e]#7 = 0b
        //reg_tso_evd_mode[1:0]
        _RVM1(0x1e21, 0, BIT2 | BIT1),   //reg[101e21]#2 ~ #1 = 00b
        //reg_et_mode
        _RVM1(0x1edf, 0, BIT0),   //reg[101edf]#0 = 0b
        //reg_led_mode
        _RVM1(0x1eb4, 0, BIT4),   //reg[101eb4]#4 = 0b
        //reg_seconduartmode[1:0]
        _RVM1(0x1e05, 0, BIT1 | BIT0),   //reg[101e05]#1 ~ #0 = 00b
        //reg_od2nduart[1:0]
        _RVM1(0x1ea9, 0, BIT1 | BIT0),   //reg[101ea9]#1 ~ #0 = 00b
        //reg_miic_mode3[1:0]
        _RVM1(0x1edf, 0, BIT2 | BIT1),   //reg[101edf]#2 ~ #1 = 00b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_GPIO21_IS_GPIO != GPIO_NONE)
        #define PAD_GPIO21_OEN (PAD_GPIO21_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_GPIO21_OUT (PAD_GPIO21_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2b15, PAD_GPIO21_OUT, BIT0),
        _RVM1(0x2b15, PAD_GPIO21_OEN, BIT1),
        //reg_tso_evd_mode[1:0]
        _RVM1(0x1e21, 0, BIT2 | BIT1),   //reg[101e21]#2 ~ #1 = 00b
        //reg_et_mode
        _RVM1(0x1edf, 0, BIT0),   //reg[101edf]#0 = 0b
        //reg_thirduartmode[1:0]
        _RVM1(0x1e05, 0, BIT3 | BIT2),   //reg[101e05]#3 ~ #2 = 00b
        //reg_od3rduart[1:0]
        _RVM1(0x1ea9, 0, BIT3 | BIT2),   //reg[101ea9]#3 ~ #2 = 00b
        //reg_ext_int[5]
        _RVM1(0x1ea5, 0, BIT5),   //reg[101ea5]#5 = 0b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_GPIO22_IS_GPIO != GPIO_NONE)
        #define PAD_GPIO22_OEN (PAD_GPIO22_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_GPIO22_OUT (PAD_GPIO22_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2b16, PAD_GPIO22_OUT, BIT0),
        _RVM1(0x2b16, PAD_GPIO22_OEN, BIT1),
        //reg_tso_evd_mode[1:0]
        _RVM1(0x1e21, 0, BIT2 | BIT1),   //reg[101e21]#2 ~ #1 = 00b
        //reg_et_mode
        _RVM1(0x1edf, 0, BIT0),   //reg[101edf]#0 = 0b
        //reg_thirduartmode[1:0]
        _RVM1(0x1e05, 0, BIT3 | BIT2),   //reg[101e05]#3 ~ #2 = 00b
        //reg_od3rduart[1:0]
        _RVM1(0x1ea9, 0, BIT3 | BIT2),   //reg[101ea9]#3 ~ #2 = 00b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_GPIO23_IS_GPIO != GPIO_NONE)
        #define PAD_GPIO23_OEN (PAD_GPIO23_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_GPIO23_OUT (PAD_GPIO23_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2b17, PAD_GPIO23_OUT, BIT0),
        _RVM1(0x2b17, PAD_GPIO23_OEN, BIT1),
        //reg_tso_evd_mode[1:0]
        _RVM1(0x1e21, 0, BIT2 | BIT1),   //reg[101e21]#2 ~ #1 = 00b
        //reg_et_mode
        _RVM1(0x1edf, 0, BIT0),   //reg[101edf]#0 = 0b
        //reg_fourthuartmode[1:0]
        _RVM1(0x1e04, 0, BIT7 | BIT6),   //reg[101e04]#7 ~ #6 = 00b
        //reg_od4thuart[1:0]
        _RVM1(0x1ea9, 0, BIT5 | BIT4),   //reg[101ea9]#5 ~ #4 = 00b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_GPIO24_IS_GPIO != GPIO_NONE)
        #define PAD_GPIO24_OEN (PAD_GPIO24_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_GPIO24_OUT (PAD_GPIO24_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2b18, PAD_GPIO24_OUT, BIT0),
        _RVM1(0x2b18, PAD_GPIO24_OEN, BIT1),
        //reg_tso_evd_mode[1:0]
        _RVM1(0x1e21, 0, BIT2 | BIT1),   //reg[101e21]#2 ~ #1 = 00b
        //reg_et_mode
        _RVM1(0x1edf, 0, BIT0),   //reg[101edf]#0 = 0b
        //reg_fourthuartmode[1:0]
        _RVM1(0x1e04, 0, BIT7 | BIT6),   //reg[101e04]#7 ~ #6 = 00b
        //reg_od4thuart[1:0]
        _RVM1(0x1ea9, 0, BIT5 | BIT4),   //reg[101ea9]#5 ~ #4 = 00b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_GPIO25_IS_GPIO != GPIO_NONE)
        #define PAD_GPIO25_OEN (PAD_GPIO25_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_GPIO25_OUT (PAD_GPIO25_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2b19, PAD_GPIO25_OUT, BIT0),
        _RVM1(0x2b19, PAD_GPIO25_OEN, BIT1),
        //reg_tso_evd_mode[1:0]
        _RVM1(0x1e21, 0, BIT2 | BIT1),   //reg[101e21]#2 ~ #1 = 00b
        //reg_et_mode
        _RVM1(0x1edf, 0, BIT0),   //reg[101edf]#0 = 0b
        //reg_mdio_mode
_MEMMAP_nonPM_15_,
        _RVM1(0x1e00, 0, BIT0),   //reg[151e00]#0 = 0b
        //reg_fastuartmode[1:0]
_MEMMAP_nonPM_,
        _RVM1(0x1e04, 0, BIT5 | BIT4),   //reg[101e04]#5 ~ #4 = 00b
        //reg_odfuart[1:0]
        _RVM1(0x1ea9, 0, BIT7 | BIT6),   //reg[101ea9]#7 ~ #6 = 00b
        //reg_diseqc_in_config[1:0]
        _RVM1(0x1ed0, 0, BIT3 | BIT2),   //reg[101ed0]#3 ~ #2 = 00b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_GPIO26_IS_GPIO != GPIO_NONE)
        #define PAD_GPIO26_OEN (PAD_GPIO26_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_GPIO26_OUT (PAD_GPIO26_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2b1a, PAD_GPIO26_OUT, BIT0),
        _RVM1(0x2b1a, PAD_GPIO26_OEN, BIT1),
        //reg_tso_evd_mode[1:0]
        _RVM1(0x1e21, 0, BIT2 | BIT1),   //reg[101e21]#2 ~ #1 = 00b
        //reg_et_mode
        _RVM1(0x1edf, 0, BIT0),   //reg[101edf]#0 = 0b
        //reg_mdio_mode
_MEMMAP_nonPM_15_,
        _RVM1(0x1e00, 0, BIT0),   //reg[151e00]#0 = 0b
        //reg_fastuartmode[1:0]
_MEMMAP_nonPM_,
        _RVM1(0x1e04, 0, BIT5 | BIT4),   //reg[101e04]#5 ~ #4 = 00b
        //reg_odfuart[1:0]
        _RVM1(0x1ea9, 0, BIT7 | BIT6),   //reg[101ea9]#7 ~ #6 = 00b
        //reg_diseqc_out_config[1:0]
        _RVM1(0x1ed0, 0, BIT5 | BIT4),   //reg[101ed0]#5 ~ #4 = 00b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_GPIO27_IS_GPIO != GPIO_NONE)
        #define PAD_GPIO27_OEN (PAD_GPIO27_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_GPIO27_OUT (PAD_GPIO27_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2b1b, PAD_GPIO27_OUT, BIT0),
        _RVM1(0x2b1b, PAD_GPIO27_OEN, BIT1),
        //reg_tso_evd_mode[1:0]
        _RVM1(0x1e21, 0, BIT2 | BIT1),   //reg[101e21]#2 ~ #1 = 00b
        //reg_et_mode
        _RVM1(0x1edf, 0, BIT0),   //reg[101edf]#0 = 0b
        //reg_ext_int[4]
        _RVM1(0x1ea5, 0, BIT4),   //reg[101ea5]#4 = 0b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_GPIO28_IS_GPIO != GPIO_NONE)
        #define PAD_GPIO28_OEN (PAD_GPIO28_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_GPIO28_OUT (PAD_GPIO28_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2ba0, PAD_GPIO28_OUT, BIT0),
        _RVM1(0x2ba0, PAD_GPIO28_OEN, BIT1),
        //reg_miic_mode0
        _RVM1(0x1edc, 0, BIT0),   //reg[101edc]#0 = 0b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_GPIO29_IS_GPIO != GPIO_NONE)
        #define PAD_GPIO29_OEN (PAD_GPIO29_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_GPIO29_OUT (PAD_GPIO29_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2ba1, PAD_GPIO29_OUT, BIT0),
        _RVM1(0x2ba1, PAD_GPIO29_OEN, BIT1),
        //reg_miic_mode0
        _RVM1(0x1edc, 0, BIT0),   //reg[101edc]#0 = 0b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_GPIO30_IS_GPIO != GPIO_NONE)
        #define PAD_GPIO30_OEN (PAD_GPIO30_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_GPIO30_OUT (PAD_GPIO30_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2ba2, PAD_GPIO30_OUT, BIT0),
        _RVM1(0x2ba2, PAD_GPIO30_OEN, BIT1),
        //reg_miic_mode4
        _RVM1(0x1ede, 0, BIT0),   //reg[101ede]#0 = 0b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_GPIO31_IS_GPIO != GPIO_NONE)
        #define PAD_GPIO31_OEN (PAD_GPIO31_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_GPIO31_OUT (PAD_GPIO31_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2ba3, PAD_GPIO31_OUT, BIT0),
        _RVM1(0x2ba3, PAD_GPIO31_OEN, BIT1),
        //reg_miic_mode4
        _RVM1(0x1ede, 0, BIT0),   //reg[101ede]#0 = 0b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_GPIO36_IS_GPIO != GPIO_NONE)
        #define PAD_GPIO36_OEN (PAD_GPIO36_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_GPIO36_OUT (PAD_GPIO36_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2ba4, PAD_GPIO36_OUT, BIT0),
        _RVM1(0x2ba4, PAD_GPIO36_OEN, BIT1),
        //reg_sixthuartmode[0]
        _RVM1(0x1e08, 0, BIT6),   //reg[101e08]#6 = 0b
        //reg_od6thuart[0]
        _RVM1(0x1eaa, 0, BIT6),   //reg[101eaa]#6 = 0b
        //reg_miic_mode1[1:0]
        _RVM1(0x1edc, 0, BIT2 | BIT1),   //reg[101edc]#2 ~ #1 = 00b
        //reg_miic_mode3[1:0]
        _RVM1(0x1edf, 0, BIT2 | BIT1),   //reg[101edf]#2 ~ #1 = 00b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_GPIO37_IS_GPIO != GPIO_NONE)
        #define PAD_GPIO37_OEN (PAD_GPIO37_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_GPIO37_OUT (PAD_GPIO37_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2ba5, PAD_GPIO37_OUT, BIT0),
        _RVM1(0x2ba5, PAD_GPIO37_OEN, BIT1),
        //reg_sixthuartmode[0]
        _RVM1(0x1e08, 0, BIT6),   //reg[101e08]#6 = 0b
        //reg_od6thuart[0]
        _RVM1(0x1eaa, 0, BIT6),   //reg[101eaa]#6 = 0b
        //reg_miic_mode1[1:0]
        _RVM1(0x1edc, 0, BIT2 | BIT1),   //reg[101edc]#2 ~ #1 = 00b
        //reg_miic_mode3[1:0]
        _RVM1(0x1edf, 0, BIT2 | BIT1),   //reg[101edf]#2 ~ #1 = 00b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_HDMIRX_ARCTX_IS_GPIO != GPIO_NONE)
        #define PAD_HDMIRX_ARCTX_OEN (PAD_HDMIRX_ARCTX_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_HDMIRX_ARCTX_OUT (PAD_HDMIRX_ARCTX_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
_MEMMAP_nonPM_11_,
        _RVM1(0x0320, PAD_HDMIRX_ARCTX_OUT, BIT0),
        _RVM1(0x0320, PAD_HDMIRX_ARCTX_OEN, BIT1),
        //reg_arc_gpio_en
        _RVM1(0x0320, BIT4, BIT4),   //reg[110320]#4 = 1b
        //reg_arc_mode
        _RVM1(0x0320, 0, BIT3),   //reg[110320]#3 = 0b
        //reg_allpad_in
_MEMMAP_nonPM_,
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_I2S_IN_BCK_IS_GPIO != GPIO_NONE)
        #define PAD_I2S_IN_BCK_OEN (PAD_I2S_IN_BCK_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_I2S_IN_BCK_OUT (PAD_I2S_IN_BCK_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2b37, PAD_I2S_IN_BCK_OUT, BIT0),
        _RVM1(0x2b37, PAD_I2S_IN_BCK_OEN, BIT1),
        //reg_test_out_mode[2:0]
        //_RVM1(0x1e24, 0, 0x70),   //reg[101e24]#6 ~ #4 = 000b
        //reg_i2sinconfig
        _RVM1(0x1eae, 0, BIT2),   //reg[101eae]#2 = 0b
        //reg_dspejtagmode[1:0]
        //_RVM1(0x1e07, 0, BIT1 | BIT0),   //reg[101e07]#1 ~ #0 = 00b
        //reg_thirduartmode[1:0]
        _RVM1(0x1e05, 0, BIT3 | BIT2),   //reg[101e05]#3 ~ #2 = 00b
        //reg_od3rduart[1:0]
        _RVM1(0x1ea9, 0, BIT3 | BIT2),   //reg[101ea9]#3 ~ #2 = 00b
        //reg_miic_mode2
        _RVM1(0x1edc, 0, BIT3),   //reg[101edc]#3 = 0b
        //reg_tconconfig[0]
        _RVM1(0x1ea0, 0, BIT0),   //reg[101ea0]#0 = 0b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_I2S_IN_SD_IS_GPIO != GPIO_NONE)
        #define PAD_I2S_IN_SD_OEN (PAD_I2S_IN_SD_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_I2S_IN_SD_OUT (PAD_I2S_IN_SD_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2b38, PAD_I2S_IN_SD_OUT, BIT0),
        _RVM1(0x2b38, PAD_I2S_IN_SD_OEN, BIT1),
        //reg_test_out_mode[2:0]
        //_RVM1(0x1e24, 0, 0x70),   //reg[101e24]#6 ~ #4 = 000b
        //reg_i2sinconfig
        _RVM1(0x1eae, 0, BIT2),   //reg[101eae]#2 = 0b
        //reg_dspejtagmode[1:0]
        //_RVM1(0x1e07, 0, BIT1 | BIT0),   //reg[101e07]#1 ~ #0 = 00b
        //reg_tserrout[1:0]
        _RVM1(0x1ec9, 0, BIT1 | BIT0),   //reg[101ec9]#1 ~ #0 = 00b
        //reg_miic_mode2
        _RVM1(0x1edc, 0, BIT3),   //reg[101edc]#3 = 0b
        //reg_tconconfig[2]
        _RVM1(0x1ea0, 0, BIT2),   //reg[101ea0]#2 = 0b
        //reg_3dflagconfig[1:0]
        _RVM1(0x1eb3, 0, BIT7 | BIT6),   //reg[101eb3]#7 ~ #6 = 00b
        //reg_osd3dflag_config[1:0]
        _RVM1(0x1ef6, 0, BIT7 | BIT6),   //reg[101ef6]#7 ~ #6 = 00b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_I2S_IN_WS_IS_GPIO != GPIO_NONE)
        #define PAD_I2S_IN_WS_OEN (PAD_I2S_IN_WS_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_I2S_IN_WS_OUT (PAD_I2S_IN_WS_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2b36, PAD_I2S_IN_WS_OUT, BIT0),
        _RVM1(0x2b36, PAD_I2S_IN_WS_OEN, BIT1),
        //reg_test_out_mode[2:0]
        //_RVM1(0x1e24, 0, 0x70),   //reg[101e24]#6 ~ #4 = 000b
        //reg_i2sinconfig
        _RVM1(0x1eae, 0, BIT2),   //reg[101eae]#2 = 0b
        //reg_mcujtagmode[1:0]
        //_RVM1(0x1ede, 0, BIT3 | BIT2),   //reg[101ede]#3 ~ #2 = 00b
        //reg_dspejtagmode[1:0]
        //_RVM1(0x1e07, 0, BIT1 | BIT0),   //reg[101e07]#1 ~ #0 = 00b
        //reg_thirduartmode[1:0]
        _RVM1(0x1e05, 0, BIT3 | BIT2),   //reg[101e05]#3 ~ #2 = 00b
        //reg_od3rduart[1:0]
        _RVM1(0x1ea9, 0, BIT3 | BIT2),   //reg[101ea9]#3 ~ #2 = 00b
        //reg_tconconfig[1]
        _RVM1(0x1ea0, 0, BIT1),   //reg[101ea0]#1 = 0b
        //reg_ext_int[6]
        _RVM1(0x1ea5, 0, BIT6),   //reg[101ea5]#6 = 0b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_I2S_OUT_BCK_IS_GPIO != GPIO_NONE)
        #define PAD_I2S_OUT_BCK_OEN (PAD_I2S_OUT_BCK_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_I2S_OUT_BCK_OUT (PAD_I2S_OUT_BCK_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2b3d, PAD_I2S_OUT_BCK_OUT, BIT0),
        _RVM1(0x2b3d, PAD_I2S_OUT_BCK_OEN, BIT1),
        //reg_test_in_mode[2:0]
        //_RVM1(0x1e24, 0, 0x07),   //reg[101e24]#2 ~ #0 = 000b
        //reg_test_out_mode[2:0]
        //_RVM1(0x1e24, 0, 0x70),   //reg[101e24]#6 ~ #4 = 000b
        //reg_i2soutconfig0
        _RVM1(0x1eae, 0, BIT4),   //reg[101eae]#4 = 0b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_I2S_OUT_MCK_IS_GPIO != GPIO_NONE)
        #define PAD_I2S_OUT_MCK_OEN (PAD_I2S_OUT_MCK_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_I2S_OUT_MCK_OUT (PAD_I2S_OUT_MCK_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2b3c, PAD_I2S_OUT_MCK_OUT, BIT0),
        _RVM1(0x2b3c, PAD_I2S_OUT_MCK_OEN, BIT1),
        //reg_test_in_mode[2:0]
        //_RVM1(0x1e24, 0, 0x07),   //reg[101e24]#2 ~ #0 = 000b
        //reg_test_out_mode[2:0]
        //_RVM1(0x1e24, 0, 0x70),   //reg[101e24]#6 ~ #4 = 000b
        //reg_i2soutconfig0
        _RVM1(0x1eae, 0, BIT4),   //reg[101eae]#4 = 0b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_I2S_OUT_SD_IS_GPIO != GPIO_NONE)
        #define PAD_I2S_OUT_SD_OEN (PAD_I2S_OUT_SD_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_I2S_OUT_SD_OUT (PAD_I2S_OUT_SD_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2b3e, PAD_I2S_OUT_SD_OUT, BIT0),
        _RVM1(0x2b3e, PAD_I2S_OUT_SD_OEN, BIT1),
        //reg_test_in_mode[2:0]
        //_RVM1(0x1e24, 0, 0x07),   //reg[101e24]#2 ~ #0 = 000b
        //reg_test_out_mode[2:0]
        //_RVM1(0x1e24, 0, 0x70),   //reg[101e24]#6 ~ #4 = 000b
        //reg_i2soutconfig1
        _RVM1(0x1eae, 0, BIT5),   //reg[101eae]#5 = 0b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_I2S_OUT_SD1_IS_GPIO != GPIO_NONE)
        #define PAD_I2S_OUT_SD1_OEN (PAD_I2S_OUT_SD1_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_I2S_OUT_SD1_OUT (PAD_I2S_OUT_SD1_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2b3f, PAD_I2S_OUT_SD1_OUT, BIT0),
        _RVM1(0x2b3f, PAD_I2S_OUT_SD1_OEN, BIT1),
        //reg_spdifoutconfig2
        _RVM1(0x1eb3, 0, BIT0),   //reg[101eb3]#0 = 0b
        //reg_i2soutconfig2
        _RVM1(0x1eb3, 0, BIT3),   //reg[101eb3]#3 = 0b
        //reg_mcujtagmode[1:0]
        //_RVM1(0x1ede, 0, BIT3 | BIT2),   //reg[101ede]#3 ~ #2 = 00b
        //reg_ext_int[7]
        _RVM1(0x1ea5, 0, BIT7),   //reg[101ea5]#7 = 0b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_I2S_OUT_SD2_IS_GPIO != GPIO_NONE)
        #define PAD_I2S_OUT_SD2_OEN (PAD_I2S_OUT_SD2_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_I2S_OUT_SD2_OUT (PAD_I2S_OUT_SD2_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2b40, PAD_I2S_OUT_SD2_OUT, BIT0),
        _RVM1(0x2b40, PAD_I2S_OUT_SD2_OEN, BIT1),
        //reg_i2soutconfig3
        _RVM1(0x1eb3, 0, BIT4),   //reg[101eb3]#4 = 0b
        //reg_mcujtagmode[1:0]
        //_RVM1(0x1ede, 0, BIT3 | BIT2),   //reg[101ede]#3 ~ #2 = 00b
        //reg_fourthuartmode[1:0]
        _RVM1(0x1e04, 0, BIT7 | BIT6),   //reg[101e04]#7 ~ #6 = 00b
        //reg_od4thuart[1:0]
        _RVM1(0x1ea9, 0, BIT5 | BIT4),   //reg[101ea9]#5 ~ #4 = 00b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_I2S_OUT_SD3_IS_GPIO != GPIO_NONE)
        #define PAD_I2S_OUT_SD3_OEN (PAD_I2S_OUT_SD3_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_I2S_OUT_SD3_OUT (PAD_I2S_OUT_SD3_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2b41, PAD_I2S_OUT_SD3_OUT, BIT0),
        _RVM1(0x2b41, PAD_I2S_OUT_SD3_OEN, BIT1),
        //reg_i2soutconfig4
        _RVM1(0x1eb3, 0, BIT5),   //reg[101eb3]#5 = 0b
        //reg_mcujtagmode[1:0]
        //_RVM1(0x1ede, 0, BIT3 | BIT2),   //reg[101ede]#3 ~ #2 = 00b
        //reg_fourthuartmode[1:0]
        _RVM1(0x1e04, 0, BIT7 | BIT6),   //reg[101e04]#7 ~ #6 = 00b
        //reg_od4thuart[1:0]
        _RVM1(0x1ea9, 0, BIT5 | BIT4),   //reg[101ea9]#5 ~ #4 = 00b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_I2S_OUT_WS_IS_GPIO != GPIO_NONE)
        #define PAD_I2S_OUT_WS_OEN (PAD_I2S_OUT_WS_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_I2S_OUT_WS_OUT (PAD_I2S_OUT_WS_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2b3b, PAD_I2S_OUT_WS_OUT, BIT0),
        _RVM1(0x2b3b, PAD_I2S_OUT_WS_OEN, BIT1),
        //reg_test_in_mode[2:0]
        //_RVM1(0x1e24, 0, 0x07),   //reg[101e24]#2 ~ #0 = 000b
        //reg_test_out_mode[2:0]
        //_RVM1(0x1e24, 0, 0x70),   //reg[101e24]#6 ~ #4 = 000b
        //reg_i2soutconfig0
        _RVM1(0x1eae, 0, BIT4),   //reg[101eae]#4 = 0b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_PCM2_CD_N_IS_GPIO != GPIO_NONE)
        #define PAD_PCM2_CD_N_OEN (PAD_PCM2_CD_N_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_PCM2_CD_N_OUT (PAD_PCM2_CD_N_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2b67, PAD_PCM2_CD_N_OUT, BIT0),
        _RVM1(0x2b67, PAD_PCM2_CD_N_OEN, BIT1),
        //reg_pcm_pe[33]
        _RVM1(0x1e16, BIT1, BIT1),   //reg[101e16]#1 = 1b
        //reg_pcm2ctrlconfig
        _RVM1(0x1ec8, 0, BIT3),   //reg[101ec8]#3 = 0b
        //reg_pcm2ctrlconfig_cd_n
        _RVM1(0x1e9e, 0, BIT0),   //reg[101e9e]#0 = 0b
        //reg_ext_int[3]
        _RVM1(0x1ea5, 0, BIT3),   //reg[101ea5]#3 = 0b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

        //[LMTASKWBS-71754][LM18A] CI Power switching (PCM_5V_CTL, CAM_CD1_N, CAM_CD2_N)--
        _RVM1(0x1e9e, 1, BIT0),   //reg[101e9e]#0 = 1b

    #if(PAD_PCM2_CE_N_IS_GPIO != GPIO_NONE)
        #define PAD_PCM2_CE_N_OEN (PAD_PCM2_CE_N_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_PCM2_CE_N_OUT (PAD_PCM2_CE_N_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2b63, PAD_PCM2_CE_N_OUT, BIT0),
        _RVM1(0x2b63, PAD_PCM2_CE_N_OEN, BIT1),
        //reg_pcm2ctrlconfig
        _RVM1(0x1ec8, 0, BIT3),   //reg[101ec8]#3 = 0b
        //reg_ext_int[0]
        _RVM1(0x1ea5, 0, BIT0),   //reg[101ea5]#0 = 0b
        //reg_sdio_config[1:0]
        _RVM1(0x1ef6, 0, BIT5 | BIT4),   //reg[101ef6]#5 ~ #4 = 00b
        //reg_sdio_config[1:0]
        _RVM1(0x1ef6, 0, BIT5 | BIT4),   //reg[101ef6]#5 ~ #4 = 00b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_PCM2_IRQA_N_IS_GPIO != GPIO_NONE)
        #define PAD_PCM2_IRQA_N_OEN (PAD_PCM2_IRQA_N_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_PCM2_IRQA_N_OUT (PAD_PCM2_IRQA_N_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2b64, PAD_PCM2_IRQA_N_OUT, BIT0),
        _RVM1(0x2b64, PAD_PCM2_IRQA_N_OEN, BIT1),
        //reg_pcm_pe[34]
        _RVM1(0x1e16, BIT2, BIT2),   //reg[101e16]#2 = 1b
        //reg_pcm2ctrlconfig
        _RVM1(0x1ec8, 0, BIT3),   //reg[101ec8]#3 = 0b
        //reg_tconconfig[5]
        _RVM1(0x1ea0, 0, BIT5),   //reg[101ea0]#5 = 0b
        //reg_ext_int[2]
        _RVM1(0x1ea5, 0, BIT2),   //reg[101ea5]#2 = 0b
        //reg_sdio_config[1:0]
        _RVM1(0x1ef6, 0, BIT5 | BIT4),   //reg[101ef6]#5 ~ #4 = 00b
        //reg_sdio_config[1:0]
        _RVM1(0x1ef6, 0, BIT5 | BIT4),   //reg[101ef6]#5 ~ #4 = 00b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_PCM2_RESET_IS_GPIO != GPIO_NONE)
        #define PAD_PCM2_RESET_OEN (PAD_PCM2_RESET_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_PCM2_RESET_OUT (PAD_PCM2_RESET_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2b66, PAD_PCM2_RESET_OUT, BIT0),
        _RVM1(0x2b66, PAD_PCM2_RESET_OEN, BIT1),
        //reg_pcm2ctrlconfig
        _RVM1(0x1ec8, 0, BIT3),   //reg[101ec8]#3 = 0b
        //reg_tconconfig[6]
        _RVM1(0x1ea0, 0, BIT6),   //reg[101ea0]#6 = 0b
        //reg_sd_config[1:0]
        _RVM1(0x1eb5, 0, BIT1 | BIT0),   //reg[101eb5]#1 ~ #0 = 00b
        //reg_sd_config[1:0]
        _RVM1(0x1eb5, 0, BIT1 | BIT0),   //reg[101eb5]#1 ~ #0 = 00b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_PCM2_WAIT_N_IS_GPIO != GPIO_NONE)
        #define PAD_PCM2_WAIT_N_OEN (PAD_PCM2_WAIT_N_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_PCM2_WAIT_N_OUT (PAD_PCM2_WAIT_N_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2b65, PAD_PCM2_WAIT_N_OUT, BIT0),
        _RVM1(0x2b65, PAD_PCM2_WAIT_N_OEN, BIT1),
        //reg_pcm_pe[35]
        _RVM1(0x1e16, BIT3, BIT3),   //reg[101e16]#3 = 1b
        //reg_pcm2ctrlconfig
        _RVM1(0x1ec8, 0, BIT3),   //reg[101ec8]#3 = 0b
        //reg_tconconfig[7]
        _RVM1(0x1ea0, 0, BIT7),   //reg[101ea0]#7 = 0b
        //reg_sd_config[1:0]
        _RVM1(0x1eb5, 0, BIT1 | BIT0),   //reg[101eb5]#1 ~ #0 = 00b
        //reg_sd_config[1:0]
        _RVM1(0x1eb5, 0, BIT1 | BIT0),   //reg[101eb5]#1 ~ #0 = 00b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_PCM_A0_IS_GPIO != GPIO_NONE)
        #define PAD_PCM_A0_OEN (PAD_PCM_A0_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_PCM_A0_OUT (PAD_PCM_A0_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2b5d, PAD_PCM_A0_OUT, BIT0),
        _RVM1(0x2b5d, PAD_PCM_A0_OEN, BIT1),
        //reg_pcm_pe[16]
        _RVM1(0x1e14, BIT0, BIT0),   //reg[101e14]#0 = 1b
        //reg_nand_mode[1:0]
        _RVM1(0x1ede, 0, BIT7 | BIT6),   //reg[101ede]#7 ~ #6 = 00b
        //reg_nand_mode[1:0]
        _RVM1(0x1ede, 0, BIT7 | BIT6),   //reg[101ede]#7 ~ #6 = 00b
        //reg_pcmadconfig
        _RVM1(0x1ec8, 0, BIT4),   //reg[101ec8]#4 = 0b
        //reg_ciadconfig
        _RVM1(0x1ec8, 0, BIT0),   //reg[101ec8]#0 = 0b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_PCM_A1_IS_GPIO != GPIO_NONE)
        #define PAD_PCM_A1_OEN (PAD_PCM_A1_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_PCM_A1_OUT (PAD_PCM_A1_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2b5c, PAD_PCM_A1_OUT, BIT0),
        _RVM1(0x2b5c, PAD_PCM_A1_OEN, BIT1),
        //reg_pcm_pe[17]
        _RVM1(0x1e14, BIT1, BIT1),   //reg[101e14]#1 = 1b
        //reg_test_in_mode[2:0]
        //_RVM1(0x1e24, 0, 0x07),   //reg[101e24]#2 ~ #0 = 000b
        //reg_nand_mode[1:0]
        _RVM1(0x1ede, 0, BIT7 | BIT6),   //reg[101ede]#7 ~ #6 = 00b
        //reg_nand_mode[1:0]
        _RVM1(0x1ede, 0, BIT7 | BIT6),   //reg[101ede]#7 ~ #6 = 00b
        //reg_pcmadconfig
        _RVM1(0x1ec8, 0, BIT4),   //reg[101ec8]#4 = 0b
        //reg_ciadconfig
        _RVM1(0x1ec8, 0, BIT0),   //reg[101ec8]#0 = 0b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_PCM_A2_IS_GPIO != GPIO_NONE)
        #define PAD_PCM_A2_OEN (PAD_PCM_A2_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_PCM_A2_OUT (PAD_PCM_A2_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2b5a, PAD_PCM_A2_OUT, BIT0),
        _RVM1(0x2b5a, PAD_PCM_A2_OEN, BIT1),
        //reg_pcm_pe[18]
        _RVM1(0x1e14, BIT2, BIT2),   //reg[101e14]#2 = 1b
        //reg_test_in_mode[2:0]
        //_RVM1(0x1e24, 0, 0x07),   //reg[101e24]#2 ~ #0 = 000b
        //reg_nand_mode[1:0]
        _RVM1(0x1ede, 0, BIT7 | BIT6),   //reg[101ede]#7 ~ #6 = 00b
        //reg_nand_mode[1:0]
        _RVM1(0x1ede, 0, BIT7 | BIT6),   //reg[101ede]#7 ~ #6 = 00b
        //reg_pcmadconfig
        _RVM1(0x1ec8, 0, BIT4),   //reg[101ec8]#4 = 0b
        //reg_ciadconfig
        _RVM1(0x1ec8, 0, BIT0),   //reg[101ec8]#0 = 0b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_PCM_A3_IS_GPIO != GPIO_NONE)
        #define PAD_PCM_A3_OEN (PAD_PCM_A3_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_PCM_A3_OUT (PAD_PCM_A3_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2b59, PAD_PCM_A3_OUT, BIT0),
        _RVM1(0x2b59, PAD_PCM_A3_OEN, BIT1),
        //reg_pcm_pe[19]
        _RVM1(0x1e14, BIT3, BIT3),   //reg[101e14]#3 = 1b
        //reg_test_in_mode[2:0]
        //_RVM1(0x1e24, 0, 0x07),   //reg[101e24]#2 ~ #0 = 000b
        //reg_test_out_mode[2:0]
        //_RVM1(0x1e24, 0, 0x70),   //reg[101e24]#6 ~ #4 = 000b
        //reg_nand_mode[1:0]
        _RVM1(0x1ede, 0, BIT7 | BIT6),   //reg[101ede]#7 ~ #6 = 00b
        //reg_nand_mode[1:0]
        _RVM1(0x1ede, 0, BIT7 | BIT6),   //reg[101ede]#7 ~ #6 = 00b
        //reg_pcmadconfig
        _RVM1(0x1ec8, 0, BIT4),   //reg[101ec8]#4 = 0b
        //reg_ciadconfig
        _RVM1(0x1ec8, 0, BIT0),   //reg[101ec8]#0 = 0b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_PCM_A4_IS_GPIO != GPIO_NONE)
        #define PAD_PCM_A4_OEN (PAD_PCM_A4_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_PCM_A4_OUT (PAD_PCM_A4_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2b58, PAD_PCM_A4_OUT, BIT0),
        _RVM1(0x2b58, PAD_PCM_A4_OEN, BIT1),
        //reg_pcm_pe[20]
        _RVM1(0x1e14, BIT4, BIT4),   //reg[101e14]#4 = 1b
        //reg_test_in_mode[2:0]
        //_RVM1(0x1e24, 0, 0x07),   //reg[101e24]#2 ~ #0 = 000b
        //reg_test_out_mode[2:0]
        //_RVM1(0x1e24, 0, 0x70),   //reg[101e24]#6 ~ #4 = 000b
        //reg_nand_mode[1:0]
        _RVM1(0x1ede, 0, BIT7 | BIT6),   //reg[101ede]#7 ~ #6 = 00b
        //reg_nand_mode[1:0]
        _RVM1(0x1ede, 0, BIT7 | BIT6),   //reg[101ede]#7 ~ #6 = 00b
        //reg_pcmadconfig
        _RVM1(0x1ec8, 0, BIT4),   //reg[101ec8]#4 = 0b
        //reg_ciadconfig
        _RVM1(0x1ec8, 0, BIT0),   //reg[101ec8]#0 = 0b
        //reg_sd_config[1:0]
        _RVM1(0x1eb5, 0, BIT1 | BIT0),   //reg[101eb5]#1 ~ #0 = 00b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_PCM_A5_IS_GPIO != GPIO_NONE)
        #define PAD_PCM_A5_OEN (PAD_PCM_A5_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_PCM_A5_OUT (PAD_PCM_A5_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2b56, PAD_PCM_A5_OUT, BIT0),
        _RVM1(0x2b56, PAD_PCM_A5_OEN, BIT1),
        //reg_pcm_pe[21]
        _RVM1(0x1e14, BIT5, BIT5),   //reg[101e14]#5 = 1b
        //reg_test_in_mode[2:0]
        //_RVM1(0x1e24, 0, 0x07),   //reg[101e24]#2 ~ #0 = 000b
        //reg_test_out_mode[2:0]
        //_RVM1(0x1e24, 0, 0x70),   //reg[101e24]#6 ~ #4 = 000b
        //reg_nand_mode[1:0]
        _RVM1(0x1ede, 0, BIT7 | BIT6),   //reg[101ede]#7 ~ #6 = 00b
        //reg_nand_mode[1:0]
        _RVM1(0x1ede, 0, BIT7 | BIT6),   //reg[101ede]#7 ~ #6 = 00b
        //reg_pcmadconfig
        _RVM1(0x1ec8, 0, BIT4),   //reg[101ec8]#4 = 0b
        //reg_ciadconfig
        _RVM1(0x1ec8, 0, BIT0),   //reg[101ec8]#0 = 0b
        //reg_sd_config[1:0]
        _RVM1(0x1eb5, 0, BIT1 | BIT0),   //reg[101eb5]#1 ~ #0 = 00b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_PCM_A6_IS_GPIO != GPIO_NONE)
        #define PAD_PCM_A6_OEN (PAD_PCM_A6_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_PCM_A6_OUT (PAD_PCM_A6_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2b55, PAD_PCM_A6_OUT, BIT0),
        _RVM1(0x2b55, PAD_PCM_A6_OEN, BIT1),
        //reg_pcm_pe[22]
        _RVM1(0x1e14, BIT6, BIT6),   //reg[101e14]#6 = 1b
        //reg_test_in_mode[2:0]
        //_RVM1(0x1e24, 0, 0x07),   //reg[101e24]#2 ~ #0 = 000b
        //reg_test_out_mode[2:0]
        //_RVM1(0x1e24, 0, 0x70),   //reg[101e24]#6 ~ #4 = 000b
        //reg_nand_mode[1:0]
        _RVM1(0x1ede, 0, BIT7 | BIT6),   //reg[101ede]#7 ~ #6 = 00b
        //reg_nand_mode[1:0]
        _RVM1(0x1ede, 0, BIT7 | BIT6),   //reg[101ede]#7 ~ #6 = 00b
        //reg_pcmadconfig
        _RVM1(0x1ec8, 0, BIT4),   //reg[101ec8]#4 = 0b
        //reg_ciadconfig
        _RVM1(0x1ec8, 0, BIT0),   //reg[101ec8]#0 = 0b
        //reg_sd_config[1:0]
        _RVM1(0x1eb5, 0, BIT1 | BIT0),   //reg[101eb5]#1 ~ #0 = 00b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_PCM_A7_IS_GPIO != GPIO_NONE)
        #define PAD_PCM_A7_OEN (PAD_PCM_A7_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_PCM_A7_OUT (PAD_PCM_A7_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2b54, PAD_PCM_A7_OUT, BIT0),
        _RVM1(0x2b54, PAD_PCM_A7_OEN, BIT1),
        //reg_pcm_pe[23]
        _RVM1(0x1e14, BIT7, BIT7),   //reg[101e14]#7 = 1b
        //reg_test_in_mode[2:0]
        //_RVM1(0x1e24, 0, 0x07),   //reg[101e24]#2 ~ #0 = 000b
        //reg_test_out_mode[2:0]
        //_RVM1(0x1e24, 0, 0x70),   //reg[101e24]#6 ~ #4 = 000b
        //reg_nand_mode[1:0]
        _RVM1(0x1ede, 0, BIT7 | BIT6),   //reg[101ede]#7 ~ #6 = 00b
        //reg_nand_mode[1:0]
        _RVM1(0x1ede, 0, BIT7 | BIT6),   //reg[101ede]#7 ~ #6 = 00b
        //reg_pcmadconfig
        _RVM1(0x1ec8, 0, BIT4),   //reg[101ec8]#4 = 0b
        //reg_ciadconfig
        _RVM1(0x1ec8, 0, BIT0),   //reg[101ec8]#0 = 0b
        //reg_sd_config[1:0]
        _RVM1(0x1eb5, 0, BIT1 | BIT0),   //reg[101eb5]#1 ~ #0 = 00b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_PCM_A8_IS_GPIO != GPIO_NONE)
        #define PAD_PCM_A8_OEN (PAD_PCM_A8_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_PCM_A8_OUT (PAD_PCM_A8_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2b4e, PAD_PCM_A8_OUT, BIT0),
        _RVM1(0x2b4e, PAD_PCM_A8_OEN, BIT1),
        //reg_pcm_pe[24]
        _RVM1(0x1e15, BIT0, BIT0),   //reg[101e15]#0 = 1b
        //reg_test_in_mode[2:0]
        //_RVM1(0x1e24, 0, 0x07),   //reg[101e24]#2 ~ #0 = 000b
        //reg_test_out_mode[2:0]
        //_RVM1(0x1e24, 0, 0x70),   //reg[101e24]#6 ~ #4 = 000b
        //reg_pcmadconfig
        _RVM1(0x1ec8, 0, BIT4),   //reg[101ec8]#4 = 0b
        //reg_ciadconfig
        _RVM1(0x1ec8, 0, BIT0),   //reg[101ec8]#0 = 0b
        //reg_sdio_config[1:0]
        _RVM1(0x1ef6, 0, BIT5 | BIT4),   //reg[101ef6]#5 ~ #4 = 00b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_PCM_A9_IS_GPIO != GPIO_NONE)
        #define PAD_PCM_A9_OEN (PAD_PCM_A9_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_PCM_A9_OUT (PAD_PCM_A9_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2b4c, PAD_PCM_A9_OUT, BIT0),
        _RVM1(0x2b4c, PAD_PCM_A9_OEN, BIT1),
        //reg_pcm_pe[25]
        _RVM1(0x1e15, BIT1, BIT1),   //reg[101e15]#1 = 1b
        //reg_test_in_mode[2:0]
        //_RVM1(0x1e24, 0, 0x07),   //reg[101e24]#2 ~ #0 = 000b
        //reg_test_out_mode[2:0]
        //_RVM1(0x1e24, 0, 0x70),   //reg[101e24]#6 ~ #4 = 000b
        //reg_pcmadconfig
        _RVM1(0x1ec8, 0, BIT4),   //reg[101ec8]#4 = 0b
        //reg_ciadconfig
        _RVM1(0x1ec8, 0, BIT0),   //reg[101ec8]#0 = 0b
        //reg_sdio_config[1:0]
        _RVM1(0x1ef6, 0, BIT5 | BIT4),   //reg[101ef6]#5 ~ #4 = 00b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_PCM_A10_IS_GPIO != GPIO_NONE)
        #define PAD_PCM_A10_OEN (PAD_PCM_A10_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_PCM_A10_OUT (PAD_PCM_A10_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2b48, PAD_PCM_A10_OUT, BIT0),
        _RVM1(0x2b48, PAD_PCM_A10_OEN, BIT1),
        //reg_pcm_pe[26]
        _RVM1(0x1e15, BIT2, BIT2),   //reg[101e15]#2 = 1b
        //reg_test_in_mode[2:0]
        //_RVM1(0x1e24, 0, 0x07),   //reg[101e24]#2 ~ #0 = 000b
        //reg_test_out_mode[2:0]
        //_RVM1(0x1e24, 0, 0x70),   //reg[101e24]#6 ~ #4 = 000b
        //reg_pcmadconfig
        _RVM1(0x1ec8, 0, BIT4),   //reg[101ec8]#4 = 0b
        //reg_ciadconfig
        _RVM1(0x1ec8, 0, BIT0),   //reg[101ec8]#0 = 0b
        //reg_sdio_config[1:0]
        _RVM1(0x1ef6, 0, BIT5 | BIT4),   //reg[101ef6]#5 ~ #4 = 00b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_PCM_A11_IS_GPIO != GPIO_NONE)
        #define PAD_PCM_A11_OEN (PAD_PCM_A11_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_PCM_A11_OUT (PAD_PCM_A11_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2b4a, PAD_PCM_A11_OUT, BIT0),
        _RVM1(0x2b4a, PAD_PCM_A11_OEN, BIT1),
        //reg_pcm_pe[27]
        _RVM1(0x1e15, BIT3, BIT3),   //reg[101e15]#3 = 1b
        //reg_test_in_mode[2:0]
        //_RVM1(0x1e24, 0, 0x07),   //reg[101e24]#2 ~ #0 = 000b
        //reg_test_out_mode[2:0]
        //_RVM1(0x1e24, 0, 0x70),   //reg[101e24]#6 ~ #4 = 000b
        //reg_pcmadconfig
        _RVM1(0x1ec8, 0, BIT4),   //reg[101ec8]#4 = 0b
        //reg_ciadconfig
        _RVM1(0x1ec8, 0, BIT0),   //reg[101ec8]#0 = 0b
        //reg_sdio_config[1:0]
        _RVM1(0x1ef6, 0, BIT5 | BIT4),   //reg[101ef6]#5 ~ #4 = 00b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_PCM_A12_IS_GPIO != GPIO_NONE)
        #define PAD_PCM_A12_OEN (PAD_PCM_A12_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_PCM_A12_OUT (PAD_PCM_A12_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2b53, PAD_PCM_A12_OUT, BIT0),
        _RVM1(0x2b53, PAD_PCM_A12_OEN, BIT1),
        //reg_pcm_pe[28]
        _RVM1(0x1e15, BIT4, BIT4),   //reg[101e15]#4 = 1b
        //reg_test_in_mode[2:0]
        //_RVM1(0x1e24, 0, 0x07),   //reg[101e24]#2 ~ #0 = 000b
        //reg_test_out_mode[2:0]
        //_RVM1(0x1e24, 0, 0x70),   //reg[101e24]#6 ~ #4 = 000b
        //reg_pcmadconfig
        _RVM1(0x1ec8, 0, BIT4),   //reg[101ec8]#4 = 0b
        //reg_ciadconfig
        _RVM1(0x1ec8, 0, BIT0),   //reg[101ec8]#0 = 0b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_PCM_A13_IS_GPIO != GPIO_NONE)
        #define PAD_PCM_A13_OEN (PAD_PCM_A13_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_PCM_A13_OUT (PAD_PCM_A13_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2b4f, PAD_PCM_A13_OUT, BIT0),
        _RVM1(0x2b4f, PAD_PCM_A13_OEN, BIT1),
        //reg_pcm_pe[29]
        _RVM1(0x1e15, BIT5, BIT5),   //reg[101e15]#5 = 1b
        //reg_test_in_mode[2:0]
        //_RVM1(0x1e24, 0, 0x07),   //reg[101e24]#2 ~ #0 = 000b
        //reg_test_out_mode[2:0]
        //_RVM1(0x1e24, 0, 0x70),   //reg[101e24]#6 ~ #4 = 000b
        //reg_pcmadconfig
        _RVM1(0x1ec8, 0, BIT4),   //reg[101ec8]#4 = 0b
        //reg_ciadconfig
        _RVM1(0x1ec8, 0, BIT0),   //reg[101ec8]#0 = 0b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_PCM_A14_IS_GPIO != GPIO_NONE)
        #define PAD_PCM_A14_OEN (PAD_PCM_A14_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_PCM_A14_OUT (PAD_PCM_A14_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2b50, PAD_PCM_A14_OUT, BIT0),
        _RVM1(0x2b50, PAD_PCM_A14_OEN, BIT1),
        //reg_pcm_pe[30]
        _RVM1(0x1e15, BIT6, BIT6),   //reg[101e15]#6 = 1b
        //reg_test_in_mode[2:0]
        //_RVM1(0x1e24, 0, 0x07),   //reg[101e24]#2 ~ #0 = 000b
        //reg_test_out_mode[2:0]
        //_RVM1(0x1e24, 0, 0x70),   //reg[101e24]#6 ~ #4 = 000b
        //reg_pcmadconfig
        _RVM1(0x1ec8, 0, BIT4),   //reg[101ec8]#4 = 0b
        //reg_ciadconfig
        _RVM1(0x1ec8, 0, BIT0),   //reg[101ec8]#0 = 0b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_PCM_CD_N_IS_GPIO != GPIO_NONE)
        #define PAD_PCM_CD_N_OEN (PAD_PCM_CD_N_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_PCM_CD_N_OUT (PAD_PCM_CD_N_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2b62, PAD_PCM_CD_N_OUT, BIT0),
        _RVM1(0x2b62, PAD_PCM_CD_N_OEN, BIT1),
        //reg_pcm_pe[32]
        _RVM1(0x1e16, BIT0, BIT0),   //reg[101e16]#0 = 1b
        //reg_test_in_mode[2:0]
        //_RVM1(0x1e24, 0, 0x07),   //reg[101e24]#2 ~ #0 = 000b
        //reg_pcmctrlconfig
        _RVM1(0x1ec8, 0, BIT5),   //reg[101ec8]#5 = 0b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_PCM_CE_N_IS_GPIO != GPIO_NONE)
        #define PAD_PCM_CE_N_OEN (PAD_PCM_CE_N_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_PCM_CE_N_OUT (PAD_PCM_CE_N_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2b47, PAD_PCM_CE_N_OUT, BIT0),
        _RVM1(0x2b47, PAD_PCM_CE_N_OEN, BIT1),
        //reg_pcm_pe[8]
        _RVM1(0x1e13, BIT0, BIT0),   //reg[101e13]#0 = 1b
        //reg_test_in_mode[2:0]
        //_RVM1(0x1e24, 0, 0x07),   //reg[101e24]#2 ~ #0 = 000b
        //reg_test_out_mode[2:0]
        //_RVM1(0x1e24, 0, 0x70),   //reg[101e24]#6 ~ #4 = 000b
        //reg_pcmctrlconfig
        _RVM1(0x1ec8, 0, BIT5),   //reg[101ec8]#5 = 0b
        //reg_cictrlconfig
        _RVM1(0x1ec8, 0, BIT1),   //reg[101ec8]#1 = 0b
        //reg_sm_config[1:0]
        _RVM1(0x1edc, 0, BIT5 | BIT4),   //reg[101edc]#5 ~ #4 = 00b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_PCM_D0_IS_GPIO != GPIO_NONE)
        #define PAD_PCM_D0_OEN (PAD_PCM_D0_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_PCM_D0_OUT (PAD_PCM_D0_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2b5e, PAD_PCM_D0_OUT, BIT0),
        _RVM1(0x2b5e, PAD_PCM_D0_OEN, BIT1),
        //reg_pcm_pe[0]
        _RVM1(0x1e12, BIT0, BIT0),   //reg[101e12]#0 = 1b
        //reg_test_in_mode[2:0]
        //_RVM1(0x1e24, 0, 0x07),   //reg[101e24]#2 ~ #0 = 000b
        //reg_test_out_mode[2:0]
        //_RVM1(0x1e24, 0, 0x70),   //reg[101e24]#6 ~ #4 = 000b
        //reg_pcmadconfig
        _RVM1(0x1ec8, 0, BIT4),   //reg[101ec8]#4 = 0b
        //reg_ciadconfig
        _RVM1(0x1ec8, 0, BIT0),   //reg[101ec8]#0 = 0b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_PCM_D1_IS_GPIO != GPIO_NONE)
        #define PAD_PCM_D1_OEN (PAD_PCM_D1_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_PCM_D1_OUT (PAD_PCM_D1_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2b5f, PAD_PCM_D1_OUT, BIT0),
        _RVM1(0x2b5f, PAD_PCM_D1_OEN, BIT1),
        //reg_pcm_pe[1]
        _RVM1(0x1e12, BIT1, BIT1),   //reg[101e12]#1 = 1b
        //reg_test_in_mode[2:0]
        //_RVM1(0x1e24, 0, 0x07),   //reg[101e24]#2 ~ #0 = 000b
        //reg_test_out_mode[2:0]
        //_RVM1(0x1e24, 0, 0x70),   //reg[101e24]#6 ~ #4 = 000b
        //reg_pcmadconfig
        _RVM1(0x1ec8, 0, BIT4),   //reg[101ec8]#4 = 0b
        //reg_ciadconfig
        _RVM1(0x1ec8, 0, BIT0),   //reg[101ec8]#0 = 0b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_PCM_D2_IS_GPIO != GPIO_NONE)
        #define PAD_PCM_D2_OEN (PAD_PCM_D2_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_PCM_D2_OUT (PAD_PCM_D2_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2b60, PAD_PCM_D2_OUT, BIT0),
        _RVM1(0x2b60, PAD_PCM_D2_OEN, BIT1),
        //reg_pcm_pe[2]
        _RVM1(0x1e12, BIT2, BIT2),   //reg[101e12]#2 = 1b
        //reg_test_in_mode[2:0]
        //_RVM1(0x1e24, 0, 0x07),   //reg[101e24]#2 ~ #0 = 000b
        //reg_test_out_mode[2:0]
        //_RVM1(0x1e24, 0, 0x70),   //reg[101e24]#6 ~ #4 = 000b
        //reg_pcmadconfig
        _RVM1(0x1ec8, 0, BIT4),   //reg[101ec8]#4 = 0b
        //reg_ciadconfig
        _RVM1(0x1ec8, 0, BIT0),   //reg[101ec8]#0 = 0b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_PCM_D3_IS_GPIO != GPIO_NONE)
        #define PAD_PCM_D3_OEN (PAD_PCM_D3_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_PCM_D3_OUT (PAD_PCM_D3_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2b42, PAD_PCM_D3_OUT, BIT0),
        _RVM1(0x2b42, PAD_PCM_D3_OEN, BIT1),
        //reg_pcm_pe[3]
        _RVM1(0x1e12, BIT3, BIT3),   //reg[101e12]#3 = 1b
        //reg_test_in_mode[2:0]
        //_RVM1(0x1e24, 0, 0x07),   //reg[101e24]#2 ~ #0 = 000b
        //reg_test_out_mode[2:0]
        //_RVM1(0x1e24, 0, 0x70),   //reg[101e24]#6 ~ #4 = 000b
        //reg_test_out_mode[2:0]
        //_RVM1(0x1e24, 0, 0x70),   //reg[101e24]#6 ~ #4 = 000b
        //reg_pcmadconfig
        _RVM1(0x1ec8, 0, BIT4),   //reg[101ec8]#4 = 0b
        //reg_ciadconfig
        _RVM1(0x1ec8, 0, BIT0),   //reg[101ec8]#0 = 0b
        //reg_mcujtagmode[1:0]
        //_RVM1(0x1ede, 0, BIT3 | BIT2),   //reg[101ede]#3 ~ #2 = 00b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_PCM_D4_IS_GPIO != GPIO_NONE)
        #define PAD_PCM_D4_OEN (PAD_PCM_D4_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_PCM_D4_OUT (PAD_PCM_D4_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2b43, PAD_PCM_D4_OUT, BIT0),
        _RVM1(0x2b43, PAD_PCM_D4_OEN, BIT1),
        //reg_pcm_pe[4]
        _RVM1(0x1e12, BIT4, BIT4),   //reg[101e12]#4 = 1b
        //reg_test_in_mode[2:0]
        //_RVM1(0x1e24, 0, 0x07),   //reg[101e24]#2 ~ #0 = 000b
        //reg_test_out_mode[2:0]
        //_RVM1(0x1e24, 0, 0x70),   //reg[101e24]#6 ~ #4 = 000b
        //reg_test_out_mode[2:0]
        //_RVM1(0x1e24, 0, 0x70),   //reg[101e24]#6 ~ #4 = 000b
        //reg_pcmadconfig
        _RVM1(0x1ec8, 0, BIT4),   //reg[101ec8]#4 = 0b
        //reg_ciadconfig
        _RVM1(0x1ec8, 0, BIT0),   //reg[101ec8]#0 = 0b
        //reg_mcujtagmode[1:0]
        //_RVM1(0x1ede, 0, BIT3 | BIT2),   //reg[101ede]#3 ~ #2 = 00b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_PCM_D5_IS_GPIO != GPIO_NONE)
        #define PAD_PCM_D5_OEN (PAD_PCM_D5_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_PCM_D5_OUT (PAD_PCM_D5_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2b44, PAD_PCM_D5_OUT, BIT0),
        _RVM1(0x2b44, PAD_PCM_D5_OEN, BIT1),
        //reg_pcm_pe[5]
        _RVM1(0x1e12, BIT5, BIT5),   //reg[101e12]#5 = 1b
        //reg_test_in_mode[2:0]
        //_RVM1(0x1e24, 0, 0x07),   //reg[101e24]#2 ~ #0 = 000b
        //reg_test_out_mode[2:0]
        //_RVM1(0x1e24, 0, 0x70),   //reg[101e24]#6 ~ #4 = 000b
        //reg_test_out_mode[2:0]
        //_RVM1(0x1e24, 0, 0x70),   //reg[101e24]#6 ~ #4 = 000b
        //reg_pcmadconfig
        _RVM1(0x1ec8, 0, BIT4),   //reg[101ec8]#4 = 0b
        //reg_ciadconfig
        _RVM1(0x1ec8, 0, BIT0),   //reg[101ec8]#0 = 0b
        //reg_mcujtagmode[1:0]
        //_RVM1(0x1ede, 0, BIT3 | BIT2),   //reg[101ede]#3 ~ #2 = 00b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_PCM_D6_IS_GPIO != GPIO_NONE)
        #define PAD_PCM_D6_OEN (PAD_PCM_D6_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_PCM_D6_OUT (PAD_PCM_D6_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2b45, PAD_PCM_D6_OUT, BIT0),
        _RVM1(0x2b45, PAD_PCM_D6_OEN, BIT1),
        //reg_pcm_pe[6]
        _RVM1(0x1e12, BIT6, BIT6),   //reg[101e12]#6 = 1b
        //reg_test_in_mode[2:0]
        //_RVM1(0x1e24, 0, 0x07),   //reg[101e24]#2 ~ #0 = 000b
        //reg_test_out_mode[2:0]
        //_RVM1(0x1e24, 0, 0x70),   //reg[101e24]#6 ~ #4 = 000b
        //reg_test_out_mode[2:0]
        //_RVM1(0x1e24, 0, 0x70),   //reg[101e24]#6 ~ #4 = 000b
        //reg_pcmadconfig
        _RVM1(0x1ec8, 0, BIT4),   //reg[101ec8]#4 = 0b
        //reg_ciadconfig
        _RVM1(0x1ec8, 0, BIT0),   //reg[101ec8]#0 = 0b
        //reg_mcujtagmode[1:0]
        //_RVM1(0x1ede, 0, BIT3 | BIT2),   //reg[101ede]#3 ~ #2 = 00b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_PCM_D7_IS_GPIO != GPIO_NONE)
        #define PAD_PCM_D7_OEN (PAD_PCM_D7_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_PCM_D7_OUT (PAD_PCM_D7_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2b46, PAD_PCM_D7_OUT, BIT0),
        _RVM1(0x2b46, PAD_PCM_D7_OEN, BIT1),
        //reg_pcm_pe[7]
        _RVM1(0x1e12, BIT7, BIT7),   //reg[101e12]#7 = 1b
        //reg_test_in_mode[2:0]
        //_RVM1(0x1e24, 0, 0x07),   //reg[101e24]#2 ~ #0 = 000b
        //reg_test_out_mode[2:0]
        //_RVM1(0x1e24, 0, 0x70),   //reg[101e24]#6 ~ #4 = 000b
        //reg_test_out_mode[2:0]
        //_RVM1(0x1e24, 0, 0x70),   //reg[101e24]#6 ~ #4 = 000b
        //reg_pcmadconfig
        _RVM1(0x1ec8, 0, BIT4),   //reg[101ec8]#4 = 0b
        //reg_ciadconfig
        _RVM1(0x1ec8, 0, BIT0),   //reg[101ec8]#0 = 0b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_PCM_IORD_N_IS_GPIO != GPIO_NONE)
        #define PAD_PCM_IORD_N_OEN (PAD_PCM_IORD_N_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_PCM_IORD_N_OUT (PAD_PCM_IORD_N_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2b4b, PAD_PCM_IORD_N_OUT, BIT0),
        _RVM1(0x2b4b, PAD_PCM_IORD_N_OEN, BIT1),
        //reg_pcm_pe[10]
        _RVM1(0x1e13, BIT2, BIT2),   //reg[101e13]#2 = 1b
        //reg_test_in_mode[2:0]
        //_RVM1(0x1e24, 0, 0x07),   //reg[101e24]#2 ~ #0 = 000b
        //reg_test_out_mode[2:0]
        //_RVM1(0x1e24, 0, 0x70),   //reg[101e24]#6 ~ #4 = 000b
        //reg_pcmctrlconfig
        _RVM1(0x1ec8, 0, BIT5),   //reg[101ec8]#5 = 0b
        //reg_cictrlconfig
        _RVM1(0x1ec8, 0, BIT1),   //reg[101ec8]#1 = 0b
        //reg_sm_config[1:0]
        _RVM1(0x1edc, 0, BIT5 | BIT4),   //reg[101edc]#5 ~ #4 = 00b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_PCM_IOWR_N_IS_GPIO != GPIO_NONE)
        #define PAD_PCM_IOWR_N_OEN (PAD_PCM_IOWR_N_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_PCM_IOWR_N_OUT (PAD_PCM_IOWR_N_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2b4d, PAD_PCM_IOWR_N_OUT, BIT0),
        _RVM1(0x2b4d, PAD_PCM_IOWR_N_OEN, BIT1),
        //reg_pcm_pe[11]
        _RVM1(0x1e13, BIT3, BIT3),   //reg[101e13]#3 = 1b
        //reg_test_in_mode[2:0]
        //_RVM1(0x1e24, 0, 0x07),   //reg[101e24]#2 ~ #0 = 000b
        //reg_test_out_mode[2:0]
        //_RVM1(0x1e24, 0, 0x70),   //reg[101e24]#6 ~ #4 = 000b
        //reg_pcmctrlconfig
        _RVM1(0x1ec8, 0, BIT5),   //reg[101ec8]#5 = 0b
        //reg_cictrlconfig
        _RVM1(0x1ec8, 0, BIT1),   //reg[101ec8]#1 = 0b
        //reg_sm_config[1:0]
        _RVM1(0x1edc, 0, BIT5 | BIT4),   //reg[101edc]#5 ~ #4 = 00b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_PCM_IRQA_N_IS_GPIO != GPIO_NONE)
        #define PAD_PCM_IRQA_N_OEN (PAD_PCM_IRQA_N_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_PCM_IRQA_N_OUT (PAD_PCM_IRQA_N_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2b52, PAD_PCM_IRQA_N_OUT, BIT0),
        _RVM1(0x2b52, PAD_PCM_IRQA_N_OEN, BIT1),
        //reg_pcm_pe[13]
        _RVM1(0x1e13, BIT5, BIT5),   //reg[101e13]#5 = 1b
        //reg_test_in_mode[2:0]
        //_RVM1(0x1e24, 0, 0x07),   //reg[101e24]#2 ~ #0 = 000b
        //reg_test_out_mode[2:0]
        //_RVM1(0x1e24, 0, 0x70),   //reg[101e24]#6 ~ #4 = 000b
        //reg_pcmctrlconfig
        _RVM1(0x1ec8, 0, BIT5),   //reg[101ec8]#5 = 0b
        //reg_cictrlconfig
        _RVM1(0x1ec8, 0, BIT1),   //reg[101ec8]#1 = 0b
        //reg_sm_config[1:0]
        _RVM1(0x1edc, 0, BIT5 | BIT4),   //reg[101edc]#5 ~ #4 = 00b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_PCM_OE_N_IS_GPIO != GPIO_NONE)
        #define PAD_PCM_OE_N_OEN (PAD_PCM_OE_N_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_PCM_OE_N_OUT (PAD_PCM_OE_N_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2b49, PAD_PCM_OE_N_OUT, BIT0),
        _RVM1(0x2b49, PAD_PCM_OE_N_OEN, BIT1),
        //reg_pcm_pe[9]
        _RVM1(0x1e13, BIT1, BIT1),   //reg[101e13]#1 = 1b
        //reg_test_in_mode[2:0]
        //_RVM1(0x1e24, 0, 0x07),   //reg[101e24]#2 ~ #0 = 000b
        //reg_test_out_mode[2:0]
        //_RVM1(0x1e24, 0, 0x70),   //reg[101e24]#6 ~ #4 = 000b
        //reg_pcmctrlconfig
        _RVM1(0x1ec8, 0, BIT5),   //reg[101ec8]#5 = 0b
        //reg_sm_config[1:0]
        _RVM1(0x1edc, 0, BIT5 | BIT4),   //reg[101edc]#5 ~ #4 = 00b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_PCM_REG_N_IS_GPIO != GPIO_NONE)
        #define PAD_PCM_REG_N_OEN (PAD_PCM_REG_N_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_PCM_REG_N_OUT (PAD_PCM_REG_N_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2b5b, PAD_PCM_REG_N_OUT, BIT0),
        _RVM1(0x2b5b, PAD_PCM_REG_N_OEN, BIT1),
        //reg_pcm_pe[15]
        _RVM1(0x1e13, BIT7, BIT7),   //reg[101e13]#7 = 1b
        //reg_test_in_mode[2:0]
        //_RVM1(0x1e24, 0, 0x07),   //reg[101e24]#2 ~ #0 = 000b
        //reg_pcmctrlconfig
        _RVM1(0x1ec8, 0, BIT5),   //reg[101ec8]#5 = 0b
        //reg_cictrlconfig
        _RVM1(0x1ec8, 0, BIT1),   //reg[101ec8]#1 = 0b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_PCM_RESET_IS_GPIO != GPIO_NONE)
        #define PAD_PCM_RESET_OEN (PAD_PCM_RESET_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_PCM_RESET_OUT (PAD_PCM_RESET_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2b61, PAD_PCM_RESET_OUT, BIT0),
        _RVM1(0x2b61, PAD_PCM_RESET_OEN, BIT1),
        //reg_pcm_pe[31]
        _RVM1(0x1e15, BIT7, BIT7),   //reg[101e15]#7 = 1b
        //reg_test_in_mode[2:0]
        //_RVM1(0x1e24, 0, 0x07),   //reg[101e24]#2 ~ #0 = 000b
        //reg_pcmctrlconfig
        _RVM1(0x1ec8, 0, BIT5),   //reg[101ec8]#5 = 0b
        //reg_cictrlconfig
        _RVM1(0x1ec8, 0, BIT1),   //reg[101ec8]#1 = 0b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_PCM_WAIT_N_IS_GPIO != GPIO_NONE)
        #define PAD_PCM_WAIT_N_OEN (PAD_PCM_WAIT_N_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_PCM_WAIT_N_OUT (PAD_PCM_WAIT_N_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2b57, PAD_PCM_WAIT_N_OUT, BIT0),
        _RVM1(0x2b57, PAD_PCM_WAIT_N_OEN, BIT1),
        //reg_pcm_pe[14]
        _RVM1(0x1e13, BIT6, BIT6),   //reg[101e13]#6 = 1b
        //reg_test_in_mode[2:0]
        //_RVM1(0x1e24, 0, 0x07),   //reg[101e24]#2 ~ #0 = 000b
        //reg_test_out_mode[2:0]
        //_RVM1(0x1e24, 0, 0x70),   //reg[101e24]#6 ~ #4 = 000b
        //reg_pcmctrlconfig
        _RVM1(0x1ec8, 0, BIT5),   //reg[101ec8]#5 = 0b
        //reg_cictrlconfig
        _RVM1(0x1ec8, 0, BIT1),   //reg[101ec8]#1 = 0b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_PCM_WE_N_IS_GPIO != GPIO_NONE)
        #define PAD_PCM_WE_N_OEN (PAD_PCM_WE_N_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_PCM_WE_N_OUT (PAD_PCM_WE_N_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2b51, PAD_PCM_WE_N_OUT, BIT0),
        _RVM1(0x2b51, PAD_PCM_WE_N_OEN, BIT1),
        //reg_pcm_pe[12]
        _RVM1(0x1e13, BIT4, BIT4),   //reg[101e13]#4 = 1b
        //reg_test_in_mode[2:0]
        //_RVM1(0x1e24, 0, 0x07),   //reg[101e24]#2 ~ #0 = 000b
        //reg_test_out_mode[2:0]
        //_RVM1(0x1e24, 0, 0x70),   //reg[101e24]#6 ~ #4 = 000b
        //reg_pcmctrlconfig
        _RVM1(0x1ec8, 0, BIT5),   //reg[101ec8]#5 = 0b
        //reg_sm_config[1:0]
        _RVM1(0x1edc, 0, BIT5 | BIT4),   //reg[101edc]#5 ~ #4 = 00b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_PWM0_IS_GPIO != GPIO_NONE)
        #define PAD_PWM0_OEN (PAD_PWM0_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_PWM0_OUT (PAD_PWM0_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2b88, PAD_PWM0_OUT, BIT0),
        _RVM1(0x2b88, PAD_PWM0_OEN, BIT1),
        //reg_vsense_pe
_MEMMAP_nonPM_11_,
        _RVM1(0x0321, BIT1, BIT1),   //reg[110321]#1 = 1b
        //reg_test_in_mode[2:0]
_MEMMAP_nonPM_,
        //_RVM1(0x1e24, 0, 0x07),   //reg[101e24]#2 ~ #0 = 000b
        //reg_test_out_mode[2:0]
        //_RVM1(0x1e24, 0, 0x70),   //reg[101e24]#6 ~ #4 = 000b
        //reg_pwm0_mode
        _RVM1(0x1ec8, 0, BIT2),   //reg[101ec8]#2 = 0b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_PWM1_IS_GPIO != GPIO_NONE)
        #define PAD_PWM1_OEN (PAD_PWM1_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_PWM1_OUT (PAD_PWM1_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2b89, PAD_PWM1_OUT, BIT0),
        _RVM1(0x2b89, PAD_PWM1_OEN, BIT1),
        //reg_test_in_mode[2:0]
        //_RVM1(0x1e24, 0, 0x07),   //reg[101e24]#2 ~ #0 = 000b
        //reg_test_out_mode[2:0]
        //_RVM1(0x1e24, 0, 0x70),   //reg[101e24]#6 ~ #4 = 000b
        //reg_pwm1_mode
        _RVM1(0x1ec8, 0, BIT6),   //reg[101ec8]#6 = 0b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_PWM2_IS_GPIO != GPIO_NONE)
        #define PAD_PWM2_OEN (PAD_PWM2_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_PWM2_OUT (PAD_PWM2_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2b8a, PAD_PWM2_OUT, BIT0),
        _RVM1(0x2b8a, PAD_PWM2_OEN, BIT1),
        //reg_test_in_mode[2:0]
        //_RVM1(0x1e24, 0, 0x07),   //reg[101e24]#2 ~ #0 = 000b
        //reg_test_out_mode[2:0]
        //_RVM1(0x1e24, 0, 0x70),   //reg[101e24]#6 ~ #4 = 000b
        //reg_pwm2_mode
        _RVM1(0x1ec8, 0, BIT7),   //reg[101ec8]#7 = 0b
        //reg_ire_mode[1:0]
        _RVM1(0x1edf, 0, BIT5 | BIT4),   //reg[101edf]#5 ~ #4 = 00b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_PWM3_IS_GPIO != GPIO_NONE)
        #define PAD_PWM3_OEN (PAD_PWM3_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_PWM3_OUT (PAD_PWM3_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2b8b, PAD_PWM3_OUT, BIT0),
        _RVM1(0x2b8b, PAD_PWM3_OEN, BIT1),
        //reg_test_in_mode[2:0]
        //_RVM1(0x1e24, 0, 0x07),   //reg[101e24]#2 ~ #0 = 000b
        //reg_test_out_mode[2:0]
        //_RVM1(0x1e24, 0, 0x70),   //reg[101e24]#6 ~ #4 = 000b
        //reg_pwm3_mode
        _RVM1(0x1ec9, 0, BIT4),   //reg[101ec9]#4 = 0b
        //reg_ire_mode[1:0]
        _RVM1(0x1edf, 0, BIT5 | BIT4),   //reg[101edf]#5 ~ #4 = 00b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_PWM4_IS_GPIO != GPIO_NONE)
        #define PAD_PWM4_OEN (PAD_PWM4_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_PWM4_OUT (PAD_PWM4_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2b8c, PAD_PWM4_OUT, BIT0),
        _RVM1(0x2b8c, PAD_PWM4_OEN, BIT1),
        //reg_test_out_mode[2:0]
        //_RVM1(0x1e24, 0, 0x70),   //reg[101e24]#6 ~ #4 = 000b
        //reg_i2smutemode[1:0]
        _RVM1(0x1e05, 0, BIT7 | BIT6),   //reg[101e05]#7 ~ #6 = 00b
        //reg_pwm4_mode
        _RVM1(0x1ec9, 0, BIT5),   //reg[101ec9]#5 = 0b
        //reg_ire_mode[1:0]
        _RVM1(0x1edf, 0, BIT5 | BIT4),   //reg[101edf]#5 ~ #4 = 00b
        //reg_tserrout[1:0]
        _RVM1(0x1ec9, 0, BIT1 | BIT0),   //reg[101ec9]#1 ~ #0 = 00b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_SPDIF_IN_IS_GPIO != GPIO_NONE)
        #define PAD_SPDIF_IN_OEN (PAD_SPDIF_IN_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_SPDIF_IN_OUT (PAD_SPDIF_IN_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2b39, PAD_SPDIF_IN_OUT, BIT0),
        _RVM1(0x2b39, PAD_SPDIF_IN_OEN, BIT1),
        //reg_test_in_mode[2:0]
        //_RVM1(0x1e24, 0, 0x07),   //reg[101e24]#2 ~ #0 = 000b
        //reg_test_out_mode[2:0]
        //_RVM1(0x1e24, 0, 0x70),   //reg[101e24]#6 ~ #4 = 000b
        //reg_spdifinconfig
        _RVM1(0x1eae, 0, BIT6),   //reg[101eae]#6 = 0b
        //reg_dspejtagmode[1:0]
        //_RVM1(0x1e07, 0, BIT1 | BIT0),   //reg[101e07]#1 ~ #0 = 00b
        //reg_tconconfig[3]
        _RVM1(0x1ea0, 0, BIT3),   //reg[101ea0]#3 = 0b
        //reg_3dflagconfig[1:0]
        _RVM1(0x1eb3, 0, BIT7 | BIT6),   //reg[101eb3]#7 ~ #6 = 00b
        //reg_osd3dflag_config[1:0]
        _RVM1(0x1ef6, 0, BIT7 | BIT6),   //reg[101ef6]#7 ~ #6 = 00b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_SPDIF_OUT_IS_GPIO != GPIO_NONE)
        #define PAD_SPDIF_OUT_OEN (PAD_SPDIF_OUT_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_SPDIF_OUT_OUT (PAD_SPDIF_OUT_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2b3a, PAD_SPDIF_OUT_OUT, BIT0),
        _RVM1(0x2b3a, PAD_SPDIF_OUT_OEN, BIT1),
        //reg_test_in_mode[2:0]
        //_RVM1(0x1e24, 0, 0x07),   //reg[101e24]#2 ~ #0 = 000b
        //reg_test_out_mode[2:0]
        //_RVM1(0x1e24, 0, 0x70),   //reg[101e24]#6 ~ #4 = 000b
        //reg_spdifoutconfig
        _RVM1(0x1eae, 0, BIT7),   //reg[101eae]#7 = 0b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_TGPIO0_IS_GPIO != GPIO_NONE)
        #define PAD_TGPIO0_OEN (PAD_TGPIO0_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_TGPIO0_OUT (PAD_TGPIO0_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2b8d, PAD_TGPIO0_OUT, BIT0),
        _RVM1(0x2b8d, PAD_TGPIO0_OEN, BIT1),
        //reg_test_out_mode[2:0]
        //_RVM1(0x1e24, 0, 0x70),   //reg[101e24]#6 ~ #4 = 000b
        //reg_test_out_mode[2:0]
        //_RVM1(0x1e24, 0, 0x70),   //reg[101e24]#6 ~ #4 = 000b
        //reg_freeze_tuner[1:0]
        _RVM1(0x1e0d, 0, BIT4 | BIT3),   //reg[101e0d]#4 ~ #3 = 00b
        //reg_vsync_vif_out_en
        _RVM1(0x1ea3, 0, BIT6),   //reg[101ea3]#6 = 0b
        //reg_mcujtagmode[1:0]
        //_RVM1(0x1ede, 0, BIT3 | BIT2),   //reg[101ede]#3 ~ #2 = 00b
        //reg_dspejtagmode[1:0]
        //_RVM1(0x1e07, 0, BIT1 | BIT0),   //reg[101e07]#1 ~ #0 = 00b
        //reg_miic_mode5
        _RVM1(0x1ede, 0, BIT1),   //reg[101ede]#1 = 0b
        //reg_diseqc_in_config[1:0]
        _RVM1(0x1ed0, 0, BIT3 | BIT2),   //reg[101ed0]#3 ~ #2 = 00b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_TGPIO1_IS_GPIO != GPIO_NONE)
        #define PAD_TGPIO1_OEN (PAD_TGPIO1_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_TGPIO1_OUT (PAD_TGPIO1_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2b8e, PAD_TGPIO1_OUT, BIT0),
        _RVM1(0x2b8e, PAD_TGPIO1_OEN, BIT1),
        //reg_test_out_mode[2:0]
        //_RVM1(0x1e24, 0, 0x70),   //reg[101e24]#6 ~ #4 = 000b
        //reg_test_out_mode[2:0]
        //_RVM1(0x1e24, 0, 0x70),   //reg[101e24]#6 ~ #4 = 000b
        //reg_freeze_tuner[1:0]
        _RVM1(0x1e0d, 0, BIT4 | BIT3),   //reg[101e0d]#4 ~ #3 = 00b
        //reg_mcujtagmode[1:0]
        //_RVM1(0x1ede, 0, BIT3 | BIT2),   //reg[101ede]#3 ~ #2 = 00b
        //reg_dspejtagmode[1:0]
        //_RVM1(0x1e07, 0, BIT1 | BIT0),   //reg[101e07]#1 ~ #0 = 00b
        //reg_miic_mode5
        _RVM1(0x1ede, 0, BIT1),   //reg[101ede]#1 = 0b
        //reg_diseqc_out_config[1:0]
        _RVM1(0x1ed0, 0, BIT5 | BIT4),   //reg[101ed0]#5 ~ #4 = 00b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_TGPIO2_IS_GPIO != GPIO_NONE)
        #define PAD_TGPIO2_OEN (PAD_TGPIO2_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_TGPIO2_OUT (PAD_TGPIO2_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2b8f, PAD_TGPIO2_OUT, BIT0),
        _RVM1(0x2b8f, PAD_TGPIO2_OEN, BIT1),
        //reg_test_out_mode[2:0]
        //_RVM1(0x1e24, 0, 0x70),   //reg[101e24]#6 ~ #4 = 000b
        //reg_mcujtagmode[1:0]
        //_RVM1(0x1ede, 0, BIT3 | BIT2),   //reg[101ede]#3 ~ #2 = 00b
        //reg_dspejtagmode[1:0]
        //_RVM1(0x1e07, 0, BIT1 | BIT0),   //reg[101e07]#1 ~ #0 = 00b
        //reg_miic_mode1[1:0]
        _RVM1(0x1edc, 0, BIT2 | BIT1),   //reg[101edc]#2 ~ #1 = 00b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_TGPIO3_IS_GPIO != GPIO_NONE)
        #define PAD_TGPIO3_OEN (PAD_TGPIO3_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_TGPIO3_OUT (PAD_TGPIO3_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2b90, PAD_TGPIO3_OUT, BIT0),
        _RVM1(0x2b90, PAD_TGPIO3_OEN, BIT1),
        //reg_test_out_mode[2:0]
        //_RVM1(0x1e24, 0, 0x70),   //reg[101e24]#6 ~ #4 = 000b
        //reg_mcujtagmode[1:0]
        //_RVM1(0x1ede, 0, BIT3 | BIT2),   //reg[101ede]#3 ~ #2 = 00b
        //reg_dspejtagmode[1:0]
        //_RVM1(0x1e07, 0, BIT1 | BIT0),   //reg[101e07]#1 ~ #0 = 00b
        //reg_miic_mode1[1:0]
        _RVM1(0x1edc, 0, BIT2 | BIT1),   //reg[101edc]#2 ~ #1 = 00b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_TS0_CLK_IS_GPIO != GPIO_NONE)
        #define PAD_TS0_CLK_OEN (PAD_TS0_CLK_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_TS0_CLK_OUT (PAD_TS0_CLK_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2b26, PAD_TS0_CLK_OUT, BIT0),
        _RVM1(0x2b26, PAD_TS0_CLK_OEN, BIT1),
        //reg_test_in_mode[2:0]
        //_RVM1(0x1e24, 0, 0x07),   //reg[101e24]#2 ~ #0 = 000b
        //reg_test_out_mode[2:0]
        //_RVM1(0x1e24, 0, 0x70),   //reg[101e24]#6 ~ #4 = 000b
        //reg_ts0config[2:0]
        _RVM1(0x1eaf, 0, 0x07),   //reg[101eaf]#2 ~ #0 = 000b
        //reg_ts0config[2:0]
        _RVM1(0x1eaf, 0, 0x07),   //reg[101eaf]#2 ~ #0 = 000b
        //reg_ts0config[2:0]
        _RVM1(0x1eaf, 0, 0x07),   //reg[101eaf]#2 ~ #0 = 000b
        //reg_ts0config[2:0]
        _RVM1(0x1eaf, 0, 0x07),   //reg[101eaf]#2 ~ #0 = 000b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_TS0_D0_IS_GPIO != GPIO_NONE)
        #define PAD_TS0_D0_OEN (PAD_TS0_D0_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_TS0_D0_OUT (PAD_TS0_D0_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2b1c, PAD_TS0_D0_OUT, BIT0),
        _RVM1(0x2b1c, PAD_TS0_D0_OEN, BIT1),
        //reg_test_in_mode[2:0]
        //_RVM1(0x1e24, 0, 0x07),   //reg[101e24]#2 ~ #0 = 000b
        //reg_test_out_mode[2:0]
        //_RVM1(0x1e24, 0, 0x70),   //reg[101e24]#6 ~ #4 = 000b
        //reg_ts0config[2:0]
        _RVM1(0x1eaf, 0, 0x07),   //reg[101eaf]#2 ~ #0 = 000b
        //reg_ts0config[2:0]
        _RVM1(0x1eaf, 0, 0x07),   //reg[101eaf]#2 ~ #0 = 000b
        //reg_ts0config[2:0]
        _RVM1(0x1eaf, 0, 0x07),   //reg[101eaf]#2 ~ #0 = 000b
        //reg_ts0config[2:0]
        _RVM1(0x1eaf, 0, 0x07),   //reg[101eaf]#2 ~ #0 = 000b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_TS0_D1_IS_GPIO != GPIO_NONE)
        #define PAD_TS0_D1_OEN (PAD_TS0_D1_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_TS0_D1_OUT (PAD_TS0_D1_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2b1d, PAD_TS0_D1_OUT, BIT0),
        _RVM1(0x2b1d, PAD_TS0_D1_OEN, BIT1),
        //reg_test_in_mode[2:0]
        //_RVM1(0x1e24, 0, 0x07),   //reg[101e24]#2 ~ #0 = 000b
        //reg_ts0config[2:0]
        _RVM1(0x1eaf, 0, 0x07),   //reg[101eaf]#2 ~ #0 = 000b
        //reg_sm_config[1:0]
        _RVM1(0x1edc, 0, BIT5 | BIT4),   //reg[101edc]#5 ~ #4 = 00b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_TS0_D2_IS_GPIO != GPIO_NONE)
        #define PAD_TS0_D2_OEN (PAD_TS0_D2_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_TS0_D2_OUT (PAD_TS0_D2_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2b1e, PAD_TS0_D2_OUT, BIT0),
        _RVM1(0x2b1e, PAD_TS0_D2_OEN, BIT1),
        //reg_test_in_mode[2:0]
        //_RVM1(0x1e24, 0, 0x07),   //reg[101e24]#2 ~ #0 = 000b
        //reg_ts0config[2:0]
        _RVM1(0x1eaf, 0, 0x07),   //reg[101eaf]#2 ~ #0 = 000b
        //reg_sm_config[1:0]
        _RVM1(0x1edc, 0, BIT5 | BIT4),   //reg[101edc]#5 ~ #4 = 00b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_TS0_D3_IS_GPIO != GPIO_NONE)
        #define PAD_TS0_D3_OEN (PAD_TS0_D3_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_TS0_D3_OUT (PAD_TS0_D3_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2b1f, PAD_TS0_D3_OUT, BIT0),
        _RVM1(0x2b1f, PAD_TS0_D3_OEN, BIT1),
        //reg_test_in_mode[2:0]
        //_RVM1(0x1e24, 0, 0x07),   //reg[101e24]#2 ~ #0 = 000b
        //reg_ts0config[2:0]
        _RVM1(0x1eaf, 0, 0x07),   //reg[101eaf]#2 ~ #0 = 000b
        //reg_sm_config[1:0]
        _RVM1(0x1edc, 0, BIT5 | BIT4),   //reg[101edc]#5 ~ #4 = 00b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_TS0_D4_IS_GPIO != GPIO_NONE)
        #define PAD_TS0_D4_OEN (PAD_TS0_D4_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_TS0_D4_OUT (PAD_TS0_D4_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2b20, PAD_TS0_D4_OUT, BIT0),
        _RVM1(0x2b20, PAD_TS0_D4_OEN, BIT1),
        //reg_test_in_mode[2:0]
        //_RVM1(0x1e24, 0, 0x07),   //reg[101e24]#2 ~ #0 = 000b
        //reg_ts0config[2:0]
        _RVM1(0x1eaf, 0, 0x07),   //reg[101eaf]#2 ~ #0 = 000b
        //reg_sm_config[1:0]
        _RVM1(0x1edc, 0, BIT5 | BIT4),   //reg[101edc]#5 ~ #4 = 00b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_TS0_D5_IS_GPIO != GPIO_NONE)
        #define PAD_TS0_D5_OEN (PAD_TS0_D5_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_TS0_D5_OUT (PAD_TS0_D5_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2b21, PAD_TS0_D5_OUT, BIT0),
        _RVM1(0x2b21, PAD_TS0_D5_OEN, BIT1),
        //reg_test_in_mode[2:0]
        //_RVM1(0x1e24, 0, 0x07),   //reg[101e24]#2 ~ #0 = 000b
        //reg_ts0config[2:0]
        _RVM1(0x1eaf, 0, 0x07),   //reg[101eaf]#2 ~ #0 = 000b
        //reg_sm_config[1:0]
        _RVM1(0x1edc, 0, BIT5 | BIT4),   //reg[101edc]#5 ~ #4 = 00b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_TS0_D6_IS_GPIO != GPIO_NONE)
        #define PAD_TS0_D6_OEN (PAD_TS0_D6_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_TS0_D6_OUT (PAD_TS0_D6_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2b22, PAD_TS0_D6_OUT, BIT0),
        _RVM1(0x2b22, PAD_TS0_D6_OEN, BIT1),
        //reg_test_in_mode[2:0]
        //_RVM1(0x1e24, 0, 0x07),   //reg[101e24]#2 ~ #0 = 000b
        //reg_ts0config[2:0]
        _RVM1(0x1eaf, 0, 0x07),   //reg[101eaf]#2 ~ #0 = 000b
        //reg_sm_config[1:0]
        _RVM1(0x1edc, 0, BIT5 | BIT4),   //reg[101edc]#5 ~ #4 = 00b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_TS0_D7_IS_GPIO != GPIO_NONE)
        #define PAD_TS0_D7_OEN (PAD_TS0_D7_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_TS0_D7_OUT (PAD_TS0_D7_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2b23, PAD_TS0_D7_OUT, BIT0),
        _RVM1(0x2b23, PAD_TS0_D7_OEN, BIT1),
        //reg_test_in_mode[2:0]
        //_RVM1(0x1e24, 0, 0x07),   //reg[101e24]#2 ~ #0 = 000b
        //reg_ts0config[2:0]
        _RVM1(0x1eaf, 0, 0x07),   //reg[101eaf]#2 ~ #0 = 000b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_TS0_SYNC_IS_GPIO != GPIO_NONE)
        #define PAD_TS0_SYNC_OEN (PAD_TS0_SYNC_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_TS0_SYNC_OUT (PAD_TS0_SYNC_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2b25, PAD_TS0_SYNC_OUT, BIT0),
        _RVM1(0x2b25, PAD_TS0_SYNC_OEN, BIT1),
        //reg_test_in_mode[2:0]
        //_RVM1(0x1e24, 0, 0x07),   //reg[101e24]#2 ~ #0 = 000b
        //reg_test_out_mode[2:0]
        //_RVM1(0x1e24, 0, 0x70),   //reg[101e24]#6 ~ #4 = 000b
        //reg_ts0config[2:0]
        _RVM1(0x1eaf, 0, 0x07),   //reg[101eaf]#2 ~ #0 = 000b
        //reg_ts0config[2:0]
        _RVM1(0x1eaf, 0, 0x07),   //reg[101eaf]#2 ~ #0 = 000b
        //reg_ts0config[2:0]
        _RVM1(0x1eaf, 0, 0x07),   //reg[101eaf]#2 ~ #0 = 000b
        //reg_ts0config[2:0]
        _RVM1(0x1eaf, 0, 0x07),   //reg[101eaf]#2 ~ #0 = 000b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_TS0_VLD_IS_GPIO != GPIO_NONE)
        #define PAD_TS0_VLD_OEN (PAD_TS0_VLD_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_TS0_VLD_OUT (PAD_TS0_VLD_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2b24, PAD_TS0_VLD_OUT, BIT0),
        _RVM1(0x2b24, PAD_TS0_VLD_OEN, BIT1),
        //reg_test_in_mode[2:0]
        //_RVM1(0x1e24, 0, 0x07),   //reg[101e24]#2 ~ #0 = 000b
        //reg_test_out_mode[2:0]
        //_RVM1(0x1e24, 0, 0x70),   //reg[101e24]#6 ~ #4 = 000b
        //reg_ts0config[2:0]
        _RVM1(0x1eaf, 0, 0x07),   //reg[101eaf]#2 ~ #0 = 000b
        //reg_ts0config[2:0]
        _RVM1(0x1eaf, 0, 0x07),   //reg[101eaf]#2 ~ #0 = 000b
        //reg_ts0config[2:0]
        _RVM1(0x1eaf, 0, 0x07),   //reg[101eaf]#2 ~ #0 = 000b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_TS1_CLK_IS_GPIO != GPIO_NONE)
        #define PAD_TS1_CLK_OEN (PAD_TS1_CLK_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_TS1_CLK_OUT (PAD_TS1_CLK_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2b27, PAD_TS1_CLK_OUT, BIT0),
        _RVM1(0x2b27, PAD_TS1_CLK_OEN, BIT1),
        //reg_lgd_rgbw_ts1[0]
        _RVM1(0x1e4c, 0, BIT0),   //reg[101e4c]#0 = 0b
        //reg_tconconfig_ts1[10]
        _RVM1(0x1e8b, 0, BIT2),   //reg[101e8b]#2 = 0b
        //reg_test_in_mode[2:0]
        //_RVM1(0x1e24, 0, 0x07),   //reg[101e24]#2 ~ #0 = 000b
        //reg_ts1config[2:0]
        _RVM1(0x1eaf, 0, 0x38),   //reg[101eaf]#5 ~ #3 = 000b
        //reg_ts1config[2:0]
        _RVM1(0x1eaf, 0, 0x38),   //reg[101eaf]#5 ~ #3 = 000b
        //reg_ts1config[2:0]
        _RVM1(0x1eaf, 0, 0x38),   //reg[101eaf]#5 ~ #3 = 000b
        //reg_ts1config[2:0]
        _RVM1(0x1eaf, 0, 0x38),   //reg[101eaf]#5 ~ #3 = 000b
        //reg_ts1config[2:0]
        _RVM1(0x1eaf, 0, 0x38),   //reg[101eaf]#5 ~ #3 = 000b
        //reg_ts_out_mode[2:0]
        _RVM1(0x1e80, 0, 0x70),   //reg[101e80]#6 ~ #4 = 000b
        //reg_ts_out_mode[2:0]
        _RVM1(0x1e80, 0, 0x70),   //reg[101e80]#6 ~ #4 = 000b
        //reg_ts_out_mode[2:0]
        _RVM1(0x1e80, 0, 0x70),   //reg[101e80]#6 ~ #4 = 000b
        //reg_ts_out_mode[2:0]
        _RVM1(0x1e80, 0, 0x70),   //reg[101e80]#6 ~ #4 = 000b
        //reg_ts_out_mode[2:0]
        _RVM1(0x1e80, 0, 0x70),   //reg[101e80]#6 ~ #4 = 000b
        //reg_pdtracectrl[1:0]
        _RVM1(0x1e9f, 0, BIT1 | BIT0),   //reg[101e9f]#1 ~ #0 = 00b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_TS1_D0_IS_GPIO != GPIO_NONE)
        #define PAD_TS1_D0_OEN (PAD_TS1_D0_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_TS1_D0_OUT (PAD_TS1_D0_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2b31, PAD_TS1_D0_OUT, BIT0),
        _RVM1(0x2b31, PAD_TS1_D0_OEN, BIT1),
        //reg_tconconfig_ts1[0]
        _RVM1(0x1e8a, 0, BIT0),   //reg[101e8a]#0 = 0b
        //reg_test_in_mode[2:0]
        //_RVM1(0x1e24, 0, 0x07),   //reg[101e24]#2 ~ #0 = 000b
        //reg_ts1config[2:0]
        _RVM1(0x1eaf, 0, 0x38),   //reg[101eaf]#5 ~ #3 = 000b
        //reg_ts1config[2:0]
        _RVM1(0x1eaf, 0, 0x38),   //reg[101eaf]#5 ~ #3 = 000b
        //reg_ts1config[2:0]
        _RVM1(0x1eaf, 0, 0x38),   //reg[101eaf]#5 ~ #3 = 000b
        //reg_ts1config[2:0]
        _RVM1(0x1eaf, 0, 0x38),   //reg[101eaf]#5 ~ #3 = 000b
        //reg_ts1config[2:0]
        _RVM1(0x1eaf, 0, 0x38),   //reg[101eaf]#5 ~ #3 = 000b
        //reg_ts_out_mode[2:0]
        _RVM1(0x1e80, 0, 0x70),   //reg[101e80]#6 ~ #4 = 000b
        //reg_ts_out_mode[2:0]
        _RVM1(0x1e80, 0, 0x70),   //reg[101e80]#6 ~ #4 = 000b
        //reg_ts_out_mode[2:0]
        _RVM1(0x1e80, 0, 0x70),   //reg[101e80]#6 ~ #4 = 000b
        //reg_ts_out_mode[2:0]
        _RVM1(0x1e80, 0, 0x70),   //reg[101e80]#6 ~ #4 = 000b
        //reg_ts_out_mode[2:0]
        _RVM1(0x1e80, 0, 0x70),   //reg[101e80]#6 ~ #4 = 000b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_TS1_D1_IS_GPIO != GPIO_NONE)
        #define PAD_TS1_D1_OEN (PAD_TS1_D1_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_TS1_D1_OUT (PAD_TS1_D1_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2b30, PAD_TS1_D1_OUT, BIT0),
        _RVM1(0x2b30, PAD_TS1_D1_OEN, BIT1),
        //reg_tconconfig_ts1[1]
        _RVM1(0x1e8a, 0, BIT1),   //reg[101e8a]#1 = 0b
        //reg_test_in_mode[2:0]
        //_RVM1(0x1e24, 0, 0x07),   //reg[101e24]#2 ~ #0 = 000b
        //reg_ts1config[2:0]
        _RVM1(0x1eaf, 0, 0x38),   //reg[101eaf]#5 ~ #3 = 000b
        //reg_ts1config[2:0]
        _RVM1(0x1eaf, 0, 0x38),   //reg[101eaf]#5 ~ #3 = 000b
        //reg_ts_out_mode[2:0]
        _RVM1(0x1e80, 0, 0x70),   //reg[101e80]#6 ~ #4 = 000b
        //reg_ts_out_mode[2:0]
        _RVM1(0x1e80, 0, 0x70),   //reg[101e80]#6 ~ #4 = 000b
        //reg_ts_out_mode[2:0]
        _RVM1(0x1e80, 0, 0x70),   //reg[101e80]#6 ~ #4 = 000b
        //reg_ts_out_mode[2:0]
        _RVM1(0x1e80, 0, 0x70),   //reg[101e80]#6 ~ #4 = 000b
        //reg_ts_out_mode[2:0]
        _RVM1(0x1e80, 0, 0x70),   //reg[101e80]#6 ~ #4 = 000b
        //reg_sm_config[1:0]
        _RVM1(0x1edc, 0, BIT5 | BIT4),   //reg[101edc]#5 ~ #4 = 00b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_TS1_D2_IS_GPIO != GPIO_NONE)
        #define PAD_TS1_D2_OEN (PAD_TS1_D2_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_TS1_D2_OUT (PAD_TS1_D2_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2b2f, PAD_TS1_D2_OUT, BIT0),
        _RVM1(0x2b2f, PAD_TS1_D2_OEN, BIT1),
        //reg_tconconfig_ts1[2]
        _RVM1(0x1e8a, 0, BIT2),   //reg[101e8a]#2 = 0b
        //reg_test_in_mode[2:0]
        //_RVM1(0x1e24, 0, 0x07),   //reg[101e24]#2 ~ #0 = 000b
        //reg_ts1config[2:0]
        _RVM1(0x1eaf, 0, 0x38),   //reg[101eaf]#5 ~ #3 = 000b
        //reg_ts1config[2:0]
        _RVM1(0x1eaf, 0, 0x38),   //reg[101eaf]#5 ~ #3 = 000b
        //reg_ts_out_mode[2:0]
        _RVM1(0x1e80, 0, 0x70),   //reg[101e80]#6 ~ #4 = 000b
        //reg_ts_out_mode[2:0]
        _RVM1(0x1e80, 0, 0x70),   //reg[101e80]#6 ~ #4 = 000b
        //reg_ts_out_mode[2:0]
        _RVM1(0x1e80, 0, 0x70),   //reg[101e80]#6 ~ #4 = 000b
        //reg_ts_out_mode[2:0]
        _RVM1(0x1e80, 0, 0x70),   //reg[101e80]#6 ~ #4 = 000b
        //reg_ts_out_mode[2:0]
        _RVM1(0x1e80, 0, 0x70),   //reg[101e80]#6 ~ #4 = 000b
        //reg_sm_config[1:0]
        _RVM1(0x1edc, 0, BIT5 | BIT4),   //reg[101edc]#5 ~ #4 = 00b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_TS1_D3_IS_GPIO != GPIO_NONE)
        #define PAD_TS1_D3_OEN (PAD_TS1_D3_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_TS1_D3_OUT (PAD_TS1_D3_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2b2e, PAD_TS1_D3_OUT, BIT0),
        _RVM1(0x2b2e, PAD_TS1_D3_OEN, BIT1),
        //reg_tconconfig_ts1[3]
        _RVM1(0x1e8a, 0, BIT3),   //reg[101e8a]#3 = 0b
        //reg_test_in_mode[2:0]
        //_RVM1(0x1e24, 0, 0x07),   //reg[101e24]#2 ~ #0 = 000b
        //reg_ts1config[2:0]
        _RVM1(0x1eaf, 0, 0x38),   //reg[101eaf]#5 ~ #3 = 000b
        //reg_ts1config[2:0]
        _RVM1(0x1eaf, 0, 0x38),   //reg[101eaf]#5 ~ #3 = 000b
        //reg_ts_out_mode[2:0]
        _RVM1(0x1e80, 0, 0x70),   //reg[101e80]#6 ~ #4 = 000b
        //reg_ts_out_mode[2:0]
        _RVM1(0x1e80, 0, 0x70),   //reg[101e80]#6 ~ #4 = 000b
        //reg_ts_out_mode[2:0]
        _RVM1(0x1e80, 0, 0x70),   //reg[101e80]#6 ~ #4 = 000b
        //reg_ts_out_mode[2:0]
        _RVM1(0x1e80, 0, 0x70),   //reg[101e80]#6 ~ #4 = 000b
        //reg_ts_out_mode[2:0]
        _RVM1(0x1e80, 0, 0x70),   //reg[101e80]#6 ~ #4 = 000b
        //reg_sm_config[1:0]
        _RVM1(0x1edc, 0, BIT5 | BIT4),   //reg[101edc]#5 ~ #4 = 00b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_TS1_D4_IS_GPIO != GPIO_NONE)
        #define PAD_TS1_D4_OEN (PAD_TS1_D4_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_TS1_D4_OUT (PAD_TS1_D4_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2b2d, PAD_TS1_D4_OUT, BIT0),
        _RVM1(0x2b2d, PAD_TS1_D4_OEN, BIT1),
        //reg_lgd_rgbw_ts1[1]
        _RVM1(0x1e4c, 0, BIT1),   //reg[101e4c]#1 = 0b
        //reg_tconconfig_ts1[4]
        _RVM1(0x1e8a, 0, BIT4),   //reg[101e8a]#4 = 0b
        //reg_test_in_mode[2:0]
        //_RVM1(0x1e24, 0, 0x07),   //reg[101e24]#2 ~ #0 = 000b
        //reg_ts1config[2:0]
        _RVM1(0x1eaf, 0, 0x38),   //reg[101eaf]#5 ~ #3 = 000b
        //reg_ts1config[2:0]
        _RVM1(0x1eaf, 0, 0x38),   //reg[101eaf]#5 ~ #3 = 000b
        //reg_ts_out_mode[2:0]
        _RVM1(0x1e80, 0, 0x70),   //reg[101e80]#6 ~ #4 = 000b
        //reg_ts_out_mode[2:0]
        _RVM1(0x1e80, 0, 0x70),   //reg[101e80]#6 ~ #4 = 000b
        //reg_ts_out_mode[2:0]
        _RVM1(0x1e80, 0, 0x70),   //reg[101e80]#6 ~ #4 = 000b
        //reg_ts_out_mode[2:0]
        _RVM1(0x1e80, 0, 0x70),   //reg[101e80]#6 ~ #4 = 000b
        //reg_ts_out_mode[2:0]
        _RVM1(0x1e80, 0, 0x70),   //reg[101e80]#6 ~ #4 = 000b
        //reg_ts2config[2:0]
        _RVM1(0x1eb5, 0, 0x70),   //reg[101eb5]#6 ~ #4 = 000b
        //reg_dspejtagmode[1:0]
        //_RVM1(0x1e07, 0, BIT1 | BIT0),   //reg[101e07]#1 ~ #0 = 00b
        //reg_sm_config[1:0]
        _RVM1(0x1edc, 0, BIT5 | BIT4),   //reg[101edc]#5 ~ #4 = 00b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_TS1_D5_IS_GPIO != GPIO_NONE)
        #define PAD_TS1_D5_OEN (PAD_TS1_D5_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_TS1_D5_OUT (PAD_TS1_D5_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2b2c, PAD_TS1_D5_OUT, BIT0),
        _RVM1(0x2b2c, PAD_TS1_D5_OEN, BIT1),
        //reg_lgd_rgbw_ts1[2]
        _RVM1(0x1e4c, 0, BIT2),   //reg[101e4c]#2 = 0b
        //reg_tconconfig_ts1[5]
        _RVM1(0x1e8a, 0, BIT5),   //reg[101e8a]#5 = 0b
        //reg_test_in_mode[2:0]
        //_RVM1(0x1e24, 0, 0x07),   //reg[101e24]#2 ~ #0 = 000b
        //reg_ts1config[2:0]
        _RVM1(0x1eaf, 0, 0x38),   //reg[101eaf]#5 ~ #3 = 000b
        //reg_ts1config[2:0]
        _RVM1(0x1eaf, 0, 0x38),   //reg[101eaf]#5 ~ #3 = 000b
        //reg_ts_out_mode[2:0]
        _RVM1(0x1e80, 0, 0x70),   //reg[101e80]#6 ~ #4 = 000b
        //reg_ts_out_mode[2:0]
        _RVM1(0x1e80, 0, 0x70),   //reg[101e80]#6 ~ #4 = 000b
        //reg_ts_out_mode[2:0]
        _RVM1(0x1e80, 0, 0x70),   //reg[101e80]#6 ~ #4 = 000b
        //reg_ts_out_mode[2:0]
        _RVM1(0x1e80, 0, 0x70),   //reg[101e80]#6 ~ #4 = 000b
        //reg_ts_out_mode[2:0]
        _RVM1(0x1e80, 0, 0x70),   //reg[101e80]#6 ~ #4 = 000b
        //reg_ts2config[2:0]
        _RVM1(0x1eb5, 0, 0x70),   //reg[101eb5]#6 ~ #4 = 000b
        //reg_dspejtagmode[1:0]
        //_RVM1(0x1e07, 0, BIT1 | BIT0),   //reg[101e07]#1 ~ #0 = 00b
        //reg_pdtracectrl[1:0]
        _RVM1(0x1e9f, 0, BIT1 | BIT0),   //reg[101e9f]#1 ~ #0 = 00b
        //reg_sm_config[1:0]
        _RVM1(0x1edc, 0, BIT5 | BIT4),   //reg[101edc]#5 ~ #4 = 00b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_TS1_D6_IS_GPIO != GPIO_NONE)
        #define PAD_TS1_D6_OEN (PAD_TS1_D6_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_TS1_D6_OUT (PAD_TS1_D6_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2b2b, PAD_TS1_D6_OUT, BIT0),
        _RVM1(0x2b2b, PAD_TS1_D6_OEN, BIT1),
        //reg_lgd_rgbw_ts1[3]
        _RVM1(0x1e4c, 0, BIT3),   //reg[101e4c]#3 = 0b
        //reg_tconconfig_ts1[6]
        _RVM1(0x1e8a, 0, BIT6),   //reg[101e8a]#6 = 0b
        //reg_test_in_mode[2:0]
        //_RVM1(0x1e24, 0, 0x07),   //reg[101e24]#2 ~ #0 = 000b
        //reg_ts1config[2:0]
        _RVM1(0x1eaf, 0, 0x38),   //reg[101eaf]#5 ~ #3 = 000b
        //reg_ts1config[2:0]
        _RVM1(0x1eaf, 0, 0x38),   //reg[101eaf]#5 ~ #3 = 000b
        //reg_ts_out_mode[2:0]
        _RVM1(0x1e80, 0, 0x70),   //reg[101e80]#6 ~ #4 = 000b
        //reg_ts_out_mode[2:0]
        _RVM1(0x1e80, 0, 0x70),   //reg[101e80]#6 ~ #4 = 000b
        //reg_ts_out_mode[2:0]
        _RVM1(0x1e80, 0, 0x70),   //reg[101e80]#6 ~ #4 = 000b
        //reg_ts_out_mode[2:0]
        _RVM1(0x1e80, 0, 0x70),   //reg[101e80]#6 ~ #4 = 000b
        //reg_ts_out_mode[2:0]
        _RVM1(0x1e80, 0, 0x70),   //reg[101e80]#6 ~ #4 = 000b
        //reg_ts2config[2:0]
        _RVM1(0x1eb5, 0, 0x70),   //reg[101eb5]#6 ~ #4 = 000b
        //reg_dspejtagmode[1:0]
        //_RVM1(0x1e07, 0, BIT1 | BIT0),   //reg[101e07]#1 ~ #0 = 00b
        //reg_pdtracectrl[1:0]
        _RVM1(0x1e9f, 0, BIT1 | BIT0),   //reg[101e9f]#1 ~ #0 = 00b
        //reg_sm_config[1:0]
        _RVM1(0x1edc, 0, BIT5 | BIT4),   //reg[101edc]#5 ~ #4 = 00b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_TS1_D7_IS_GPIO != GPIO_NONE)
        #define PAD_TS1_D7_OEN (PAD_TS1_D7_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_TS1_D7_OUT (PAD_TS1_D7_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2b2a, PAD_TS1_D7_OUT, BIT0),
        _RVM1(0x2b2a, PAD_TS1_D7_OEN, BIT1),
        //reg_tconconfig_ts1[7]
        _RVM1(0x1e8a, 0, BIT7),   //reg[101e8a]#7 = 0b
        //reg_test_in_mode[2:0]
        //_RVM1(0x1e24, 0, 0x07),   //reg[101e24]#2 ~ #0 = 000b
        //reg_ts1config[2:0]
        _RVM1(0x1eaf, 0, 0x38),   //reg[101eaf]#5 ~ #3 = 000b
        //reg_ts1config[2:0]
        _RVM1(0x1eaf, 0, 0x38),   //reg[101eaf]#5 ~ #3 = 000b
        //reg_ts_out_mode[2:0]
        _RVM1(0x1e80, 0, 0x70),   //reg[101e80]#6 ~ #4 = 000b
        //reg_ts_out_mode[2:0]
        _RVM1(0x1e80, 0, 0x70),   //reg[101e80]#6 ~ #4 = 000b
        //reg_ts_out_mode[2:0]
        _RVM1(0x1e80, 0, 0x70),   //reg[101e80]#6 ~ #4 = 000b
        //reg_ts_out_mode[2:0]
        _RVM1(0x1e80, 0, 0x70),   //reg[101e80]#6 ~ #4 = 000b
        //reg_ts_out_mode[2:0]
        _RVM1(0x1e80, 0, 0x70),   //reg[101e80]#6 ~ #4 = 000b
        //reg_ts2config[2:0]
        _RVM1(0x1eb5, 0, 0x70),   //reg[101eb5]#6 ~ #4 = 000b
        //reg_dspejtagmode[1:0]
        //_RVM1(0x1e07, 0, BIT1 | BIT0),   //reg[101e07]#1 ~ #0 = 00b
        //reg_pdtracectrl[1:0]
        _RVM1(0x1e9f, 0, BIT1 | BIT0),   //reg[101e9f]#1 ~ #0 = 00b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_TS1_SYNC_IS_GPIO != GPIO_NONE)
        #define PAD_TS1_SYNC_OEN (PAD_TS1_SYNC_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_TS1_SYNC_OUT (PAD_TS1_SYNC_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2b28, PAD_TS1_SYNC_OUT, BIT0),
        _RVM1(0x2b28, PAD_TS1_SYNC_OEN, BIT1),
        //reg_tconconfig_ts1[9]
        _RVM1(0x1e8b, 0, BIT1),   //reg[101e8b]#1 = 0b
        //reg_test_in_mode[2:0]
        //_RVM1(0x1e24, 0, 0x07),   //reg[101e24]#2 ~ #0 = 000b
        //reg_ts1config[2:0]
        _RVM1(0x1eaf, 0, 0x38),   //reg[101eaf]#5 ~ #3 = 000b
        //reg_ts1config[2:0]
        _RVM1(0x1eaf, 0, 0x38),   //reg[101eaf]#5 ~ #3 = 000b
        //reg_ts1config[2:0]
        _RVM1(0x1eaf, 0, 0x38),   //reg[101eaf]#5 ~ #3 = 000b
        //reg_ts1config[2:0]
        _RVM1(0x1eaf, 0, 0x38),   //reg[101eaf]#5 ~ #3 = 000b
        //reg_ts1config[2:0]
        _RVM1(0x1eaf, 0, 0x38),   //reg[101eaf]#5 ~ #3 = 000b
        //reg_ts_out_mode[2:0]
        _RVM1(0x1e80, 0, 0x70),   //reg[101e80]#6 ~ #4 = 000b
        //reg_ts_out_mode[2:0]
        _RVM1(0x1e80, 0, 0x70),   //reg[101e80]#6 ~ #4 = 000b
        //reg_ts_out_mode[2:0]
        _RVM1(0x1e80, 0, 0x70),   //reg[101e80]#6 ~ #4 = 000b
        //reg_ts_out_mode[2:0]
        _RVM1(0x1e80, 0, 0x70),   //reg[101e80]#6 ~ #4 = 000b
        //reg_ts_out_mode[2:0]
        _RVM1(0x1e80, 0, 0x70),   //reg[101e80]#6 ~ #4 = 000b
        //reg_pdtracectrl[1:0]
        _RVM1(0x1e9f, 0, BIT1 | BIT0),   //reg[101e9f]#1 ~ #0 = 00b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_TS1_VLD_IS_GPIO != GPIO_NONE)
        #define PAD_TS1_VLD_OEN (PAD_TS1_VLD_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_TS1_VLD_OUT (PAD_TS1_VLD_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2b29, PAD_TS1_VLD_OUT, BIT0),
        _RVM1(0x2b29, PAD_TS1_VLD_OEN, BIT1),
        //reg_lgd_rgbw_ts1[4]
        _RVM1(0x1e4c, 0, BIT4),   //reg[101e4c]#4 = 0b
        //reg_tconconfig_ts1[8]
        _RVM1(0x1e8b, 0, BIT0),   //reg[101e8b]#0 = 0b
        //reg_test_in_mode[2:0]
        //_RVM1(0x1e24, 0, 0x07),   //reg[101e24]#2 ~ #0 = 000b
        //reg_ts1config[2:0]
        _RVM1(0x1eaf, 0, 0x38),   //reg[101eaf]#5 ~ #3 = 000b
        //reg_ts1config[2:0]
        _RVM1(0x1eaf, 0, 0x38),   //reg[101eaf]#5 ~ #3 = 000b
        //reg_ts1config[2:0]
        _RVM1(0x1eaf, 0, 0x38),   //reg[101eaf]#5 ~ #3 = 000b
        //reg_ts1config[2:0]
        _RVM1(0x1eaf, 0, 0x38),   //reg[101eaf]#5 ~ #3 = 000b
        //reg_ts_out_mode[2:0]
        _RVM1(0x1e80, 0, 0x70),   //reg[101e80]#6 ~ #4 = 000b
        //reg_ts_out_mode[2:0]
        _RVM1(0x1e80, 0, 0x70),   //reg[101e80]#6 ~ #4 = 000b
        //reg_ts_out_mode[2:0]
        _RVM1(0x1e80, 0, 0x70),   //reg[101e80]#6 ~ #4 = 000b
        //reg_ts_out_mode[2:0]
        _RVM1(0x1e80, 0, 0x70),   //reg[101e80]#6 ~ #4 = 000b
        //reg_ts_out_mode[2:0]
        _RVM1(0x1e80, 0, 0x70),   //reg[101e80]#6 ~ #4 = 000b
        //reg_pdtracectrl[1:0]
        _RVM1(0x1e9f, 0, BIT1 | BIT0),   //reg[101e9f]#1 ~ #0 = 00b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_TS2_CLK_IS_GPIO != GPIO_NONE)
        #define PAD_TS2_CLK_OEN (PAD_TS2_CLK_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_TS2_CLK_OUT (PAD_TS2_CLK_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2b35, PAD_TS2_CLK_OUT, BIT0),
        _RVM1(0x2b35, PAD_TS2_CLK_OEN, BIT1),
        //reg_ts2config[2:0]
        _RVM1(0x1eb5, 0, 0x70),   //reg[101eb5]#6 ~ #4 = 000b
        //reg_ts2config[2:0]
        _RVM1(0x1eb5, 0, 0x70),   //reg[101eb5]#6 ~ #4 = 000b
        //reg_ts2config[2:0]
        _RVM1(0x1eb5, 0, 0x70),   //reg[101eb5]#6 ~ #4 = 000b
        //reg_ts3config[3:0]
        _RVM1(0x1ecf, 0, 0xF0),   //reg[101ecf]#7 ~ #4 = 0000b
        //reg_sd_config[1:0]
        _RVM1(0x1eb5, 0, BIT1 | BIT0),   //reg[101eb5]#1 ~ #0 = 00b
        //reg_sdio_config[1:0]
        _RVM1(0x1ef6, 0, BIT5 | BIT4),   //reg[101ef6]#5 ~ #4 = 00b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_TS2_D0_IS_GPIO != GPIO_NONE)
        #define PAD_TS2_D0_OEN (PAD_TS2_D0_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_TS2_D0_OUT (PAD_TS2_D0_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2b32, PAD_TS2_D0_OUT, BIT0),
        _RVM1(0x2b32, PAD_TS2_D0_OEN, BIT1),
        //reg_ts2config[2:0]
        _RVM1(0x1eb5, 0, 0x70),   //reg[101eb5]#6 ~ #4 = 000b
        //reg_ts2config[2:0]
        _RVM1(0x1eb5, 0, 0x70),   //reg[101eb5]#6 ~ #4 = 000b
        //reg_ts2config[2:0]
        _RVM1(0x1eb5, 0, 0x70),   //reg[101eb5]#6 ~ #4 = 000b
        //reg_ts3config[3:0]
        _RVM1(0x1ecf, 0, 0xF0),   //reg[101ecf]#7 ~ #4 = 0000b
        //reg_sd_config[1:0]
        _RVM1(0x1eb5, 0, BIT1 | BIT0),   //reg[101eb5]#1 ~ #0 = 00b
        //reg_sdio_config[1:0]
        _RVM1(0x1ef6, 0, BIT5 | BIT4),   //reg[101ef6]#5 ~ #4 = 00b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_TS2_D1_IS_GPIO != GPIO_NONE)
        #define PAD_TS2_D1_OEN (PAD_TS2_D1_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_TS2_D1_OUT (PAD_TS2_D1_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2bf2, PAD_TS2_D1_OUT, BIT0),
        _RVM1(0x2bf2, PAD_TS2_D1_OEN, BIT1),
        //reg_ts2config[2:0]
        _RVM1(0x1eb5, 0, 0x70),   //reg[101eb5]#6 ~ #4 = 000b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_TS2_D2_IS_GPIO != GPIO_NONE)
        #define PAD_TS2_D2_OEN (PAD_TS2_D2_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_TS2_D2_OUT (PAD_TS2_D2_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2bf3, PAD_TS2_D2_OUT, BIT0),
        _RVM1(0x2bf3, PAD_TS2_D2_OEN, BIT1),
        //reg_ts2config[2:0]
        _RVM1(0x1eb5, 0, 0x70),   //reg[101eb5]#6 ~ #4 = 000b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_TS2_D3_IS_GPIO != GPIO_NONE)
        #define PAD_TS2_D3_OEN (PAD_TS2_D3_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_TS2_D3_OUT (PAD_TS2_D3_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2bf4, PAD_TS2_D3_OUT, BIT0),
        _RVM1(0x2bf4, PAD_TS2_D3_OEN, BIT1),
        //reg_ts2config[2:0]
        _RVM1(0x1eb5, 0, 0x70),   //reg[101eb5]#6 ~ #4 = 000b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_TS2_D4_IS_GPIO != GPIO_NONE)
        #define PAD_TS2_D4_OEN (PAD_TS2_D4_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_TS2_D4_OUT (PAD_TS2_D4_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2bf5, PAD_TS2_D4_OUT, BIT0),
        _RVM1(0x2bf5, PAD_TS2_D4_OEN, BIT1),
        //reg_ts1config[2:0]
        _RVM1(0x1eaf, 0, 0x38),   //reg[101eaf]#5 ~ #3 = 000b
        //reg_ts2config[2:0]
        _RVM1(0x1eb5, 0, 0x70),   //reg[101eb5]#6 ~ #4 = 000b
        //reg_ts4config[2:0]
        _RVM1(0x1e81, 0, 0x0E),   //reg[101e81]#3 ~ #1 = 000b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_TS2_D5_IS_GPIO != GPIO_NONE)
        #define PAD_TS2_D5_OEN (PAD_TS2_D5_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_TS2_D5_OUT (PAD_TS2_D5_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2bf6, PAD_TS2_D5_OUT, BIT0),
        _RVM1(0x2bf6, PAD_TS2_D5_OEN, BIT1),
        //reg_ts1config[2:0]
        _RVM1(0x1eaf, 0, 0x38),   //reg[101eaf]#5 ~ #3 = 000b
        //reg_ts2config[2:0]
        _RVM1(0x1eb5, 0, 0x70),   //reg[101eb5]#6 ~ #4 = 000b
        //reg_ts4config[2:0]
        _RVM1(0x1e81, 0, 0x0E),   //reg[101e81]#3 ~ #1 = 000b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_TS2_D6_IS_GPIO != GPIO_NONE)
        #define PAD_TS2_D6_OEN (PAD_TS2_D6_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_TS2_D6_OUT (PAD_TS2_D6_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2bf7, PAD_TS2_D6_OUT, BIT0),
        _RVM1(0x2bf7, PAD_TS2_D6_OEN, BIT1),
        //reg_ts1config[2:0]
        _RVM1(0x1eaf, 0, 0x38),   //reg[101eaf]#5 ~ #3 = 000b
        //reg_ts2config[2:0]
        _RVM1(0x1eb5, 0, 0x70),   //reg[101eb5]#6 ~ #4 = 000b
        //reg_ts4config[2:0]
        _RVM1(0x1e81, 0, 0x0E),   //reg[101e81]#3 ~ #1 = 000b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_TS2_D7_IS_GPIO != GPIO_NONE)
        #define PAD_TS2_D7_OEN (PAD_TS2_D7_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_TS2_D7_OUT (PAD_TS2_D7_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2bf8, PAD_TS2_D7_OUT, BIT0),
        _RVM1(0x2bf8, PAD_TS2_D7_OEN, BIT1),
        //reg_ts1config[2:0]
        _RVM1(0x1eaf, 0, 0x38),   //reg[101eaf]#5 ~ #3 = 000b
        //reg_ts2config[2:0]
        _RVM1(0x1eb5, 0, 0x70),   //reg[101eb5]#6 ~ #4 = 000b
        //reg_ts4config[2:0]
        _RVM1(0x1e81, 0, 0x0E),   //reg[101e81]#3 ~ #1 = 000b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_TS2_SYNC_IS_GPIO != GPIO_NONE)
        #define PAD_TS2_SYNC_OEN (PAD_TS2_SYNC_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_TS2_SYNC_OUT (PAD_TS2_SYNC_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2b34, PAD_TS2_SYNC_OUT, BIT0),
        _RVM1(0x2b34, PAD_TS2_SYNC_OEN, BIT1),
        //reg_ts2config[2:0]
        _RVM1(0x1eb5, 0, 0x70),   //reg[101eb5]#6 ~ #4 = 000b
        //reg_ts2config[2:0]
        _RVM1(0x1eb5, 0, 0x70),   //reg[101eb5]#6 ~ #4 = 000b
        //reg_ts2config[2:0]
        _RVM1(0x1eb5, 0, 0x70),   //reg[101eb5]#6 ~ #4 = 000b
        //reg_ts3config[3:0]
        _RVM1(0x1ecf, 0, 0xF0),   //reg[101ecf]#7 ~ #4 = 0000b
        //reg_sd_config[1:0]
        _RVM1(0x1eb5, 0, BIT1 | BIT0),   //reg[101eb5]#1 ~ #0 = 00b
        //reg_sdio_config[1:0]
        _RVM1(0x1ef6, 0, BIT5 | BIT4),   //reg[101ef6]#5 ~ #4 = 00b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_TS2_VLD_IS_GPIO != GPIO_NONE)
        #define PAD_TS2_VLD_OEN (PAD_TS2_VLD_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_TS2_VLD_OUT (PAD_TS2_VLD_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2b33, PAD_TS2_VLD_OUT, BIT0),
        _RVM1(0x2b33, PAD_TS2_VLD_OEN, BIT1),
        //reg_ts2config[2:0]
        _RVM1(0x1eb5, 0, 0x70),   //reg[101eb5]#6 ~ #4 = 000b
        //reg_ts2config[2:0]
        _RVM1(0x1eb5, 0, 0x70),   //reg[101eb5]#6 ~ #4 = 000b
        //reg_ts2config[2:0]
        _RVM1(0x1eb5, 0, 0x70),   //reg[101eb5]#6 ~ #4 = 000b
        //reg_ts3config[3:0]
        _RVM1(0x1ecf, 0, 0xF0),   //reg[101ecf]#7 ~ #4 = 0000b
        //reg_sd_config[1:0]
        _RVM1(0x1eb5, 0, BIT1 | BIT0),   //reg[101eb5]#1 ~ #0 = 00b
        //reg_sdio_config[1:0]
        _RVM1(0x1ef6, 0, BIT5 | BIT4),   //reg[101ef6]#5 ~ #4 = 00b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_TS3_CLK_IS_GPIO != GPIO_NONE)
        #define PAD_TS3_CLK_OEN (PAD_TS3_CLK_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_TS3_CLK_OUT (PAD_TS3_CLK_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
_MEMMAP_nonPM_11_,
        _RVM1(0x030a, PAD_TS3_CLK_OUT, BIT0),
        _RVM1(0x030a, PAD_TS3_CLK_OEN, BIT1),
        //reg_chiptop_dummy_2[0]
_MEMMAP_nonPM_,
        _RVM1(0x1e3c, 0, BIT0),   //reg[101e3c]#0 = 0b
        //reg_tso_evd_mode[1:0]
        _RVM1(0x1e21, 0, BIT2 | BIT1),   //reg[101e21]#2 ~ #1 = 00b
        //reg_ts3config[3:0]
        _RVM1(0x1ecf, 0, 0xF0),   //reg[101ecf]#7 ~ #4 = 0000b
        //reg_ts3config[3:0]
        _RVM1(0x1ecf, 0, 0xF0),   //reg[101ecf]#7 ~ #4 = 0000b
        //reg_ts3config[3:0]
        _RVM1(0x1ecf, 0, 0xF0),   //reg[101ecf]#7 ~ #4 = 0000b
        //reg_ts3config[3:0]
        _RVM1(0x1ecf, 0, 0xF0),   //reg[101ecf]#7 ~ #4 = 0000b
        //reg_ts3config[3:0]
        _RVM1(0x1ecf, 0, 0xF0),   //reg[101ecf]#7 ~ #4 = 0000b
        //reg_ts3config[3:0]
        _RVM1(0x1ecf, 0, 0xF0),   //reg[101ecf]#7 ~ #4 = 0000b
        //reg_ts3config[3:0]
        _RVM1(0x1ecf, 0, 0xF0),   //reg[101ecf]#7 ~ #4 = 0000b
        //reg_ts3config[3:0]
        _RVM1(0x1ecf, 0, 0xF0),   //reg[101ecf]#7 ~ #4 = 0000b
        //reg_i2sout_in_tcon
        _RVM1(0x1e81, 0, BIT0),   //reg[101e81]#0 = 0b
        //reg_tconconfig[10]
        _RVM1(0x1ea1, 0, BIT2),   //reg[101ea1]#2 = 0b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_TS3_D0_IS_GPIO != GPIO_NONE)
        #define PAD_TS3_D0_OEN (PAD_TS3_D0_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_TS3_D0_OUT (PAD_TS3_D0_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
_MEMMAP_nonPM_11_,
        _RVM1(0x0300, PAD_TS3_D0_OUT, BIT0),
        _RVM1(0x0300, PAD_TS3_D0_OEN, BIT1),
        //reg_tso_evd_mode[1:0]
_MEMMAP_nonPM_,
        _RVM1(0x1e21, 0, BIT2 | BIT1),   //reg[101e21]#2 ~ #1 = 00b
        //reg_ts3config[3:0]
        _RVM1(0x1ecf, 0, 0xF0),   //reg[101ecf]#7 ~ #4 = 0000b
        //reg_ts3config[3:0]
        _RVM1(0x1ecf, 0, 0xF0),   //reg[101ecf]#7 ~ #4 = 0000b
        //reg_ts3config[3:0]
        _RVM1(0x1ecf, 0, 0xF0),   //reg[101ecf]#7 ~ #4 = 0000b
        //reg_ts3config[3:0]
        _RVM1(0x1ecf, 0, 0xF0),   //reg[101ecf]#7 ~ #4 = 0000b
        //reg_ts3config[3:0]
        _RVM1(0x1ecf, 0, 0xF0),   //reg[101ecf]#7 ~ #4 = 0000b
        //reg_ts3config[3:0]
        _RVM1(0x1ecf, 0, 0xF0),   //reg[101ecf]#7 ~ #4 = 0000b
        //reg_ts3config[3:0]
        _RVM1(0x1ecf, 0, 0xF0),   //reg[101ecf]#7 ~ #4 = 0000b
        //reg_ts3config[3:0]
        _RVM1(0x1ecf, 0, 0xF0),   //reg[101ecf]#7 ~ #4 = 0000b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_TS3_D1_IS_GPIO != GPIO_NONE)
        #define PAD_TS3_D1_OEN (PAD_TS3_D1_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_TS3_D1_OUT (PAD_TS3_D1_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
_MEMMAP_nonPM_11_,
        _RVM1(0x0301, PAD_TS3_D1_OUT, BIT0),
        _RVM1(0x0301, PAD_TS3_D1_OEN, BIT1),
        //reg_tso_evd_mode[1:0]
_MEMMAP_nonPM_,
        _RVM1(0x1e21, 0, BIT2 | BIT1),   //reg[101e21]#2 ~ #1 = 00b
        //reg_ts3config[3:0]
        _RVM1(0x1ecf, 0, 0xF0),   //reg[101ecf]#7 ~ #4 = 0000b
        //reg_ts3config[3:0]
        _RVM1(0x1ecf, 0, 0xF0),   //reg[101ecf]#7 ~ #4 = 0000b
        //reg_ts3config[3:0]
        _RVM1(0x1ecf, 0, 0xF0),   //reg[101ecf]#7 ~ #4 = 0000b
        //reg_ts3config[3:0]
        _RVM1(0x1ecf, 0, 0xF0),   //reg[101ecf]#7 ~ #4 = 0000b
        //reg_ts3config[3:0]
        _RVM1(0x1ecf, 0, 0xF0),   //reg[101ecf]#7 ~ #4 = 0000b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_TS3_D2_IS_GPIO != GPIO_NONE)
        #define PAD_TS3_D2_OEN (PAD_TS3_D2_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_TS3_D2_OUT (PAD_TS3_D2_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
_MEMMAP_nonPM_11_,
        _RVM1(0x0302, PAD_TS3_D2_OUT, BIT0),
        _RVM1(0x0302, PAD_TS3_D2_OEN, BIT1),
        //reg_tso_evd_mode[1:0]
_MEMMAP_nonPM_,
        _RVM1(0x1e21, 0, BIT2 | BIT1),   //reg[101e21]#2 ~ #1 = 00b
        //reg_ts3config[3:0]
        _RVM1(0x1ecf, 0, 0xF0),   //reg[101ecf]#7 ~ #4 = 0000b
        //reg_ts3config[3:0]
        _RVM1(0x1ecf, 0, 0xF0),   //reg[101ecf]#7 ~ #4 = 0000b
        //reg_ts3config[3:0]
        _RVM1(0x1ecf, 0, 0xF0),   //reg[101ecf]#7 ~ #4 = 0000b
        //reg_ts3config[3:0]
        _RVM1(0x1ecf, 0, 0xF0),   //reg[101ecf]#7 ~ #4 = 0000b
        //reg_ts3config[3:0]
        _RVM1(0x1ecf, 0, 0xF0),   //reg[101ecf]#7 ~ #4 = 0000b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_TS3_D3_IS_GPIO != GPIO_NONE)
        #define PAD_TS3_D3_OEN (PAD_TS3_D3_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_TS3_D3_OUT (PAD_TS3_D3_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
_MEMMAP_nonPM_11_,
        _RVM1(0x0303, PAD_TS3_D3_OUT, BIT0),
        _RVM1(0x0303, PAD_TS3_D3_OEN, BIT1),
        //reg_tso_evd_mode[1:0]
_MEMMAP_nonPM_,
        _RVM1(0x1e21, 0, BIT2 | BIT1),   //reg[101e21]#2 ~ #1 = 00b
        //reg_ts3config[3:0]
        _RVM1(0x1ecf, 0, 0xF0),   //reg[101ecf]#7 ~ #4 = 0000b
        //reg_ts3config[3:0]
        _RVM1(0x1ecf, 0, 0xF0),   //reg[101ecf]#7 ~ #4 = 0000b
        //reg_ts3config[3:0]
        _RVM1(0x1ecf, 0, 0xF0),   //reg[101ecf]#7 ~ #4 = 0000b
        //reg_ts3config[3:0]
        _RVM1(0x1ecf, 0, 0xF0),   //reg[101ecf]#7 ~ #4 = 0000b
        //reg_ts3config[3:0]
        _RVM1(0x1ecf, 0, 0xF0),   //reg[101ecf]#7 ~ #4 = 0000b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_TS3_D4_IS_GPIO != GPIO_NONE)
        #define PAD_TS3_D4_OEN (PAD_TS3_D4_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_TS3_D4_OUT (PAD_TS3_D4_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
_MEMMAP_nonPM_11_,
        _RVM1(0x0304, PAD_TS3_D4_OUT, BIT0),
        _RVM1(0x0304, PAD_TS3_D4_OEN, BIT1),
        //reg_chiptop_dummy_2[1]
_MEMMAP_nonPM_,
        _RVM1(0x1e3c, 0, BIT1),   //reg[101e3c]#1 = 0b
        //reg_tso_evd_mode[1:0]
        _RVM1(0x1e21, 0, BIT2 | BIT1),   //reg[101e21]#2 ~ #1 = 00b
        //reg_ts3config[3:0]
        _RVM1(0x1ecf, 0, 0xF0),   //reg[101ecf]#7 ~ #4 = 0000b
        //reg_ts3config[3:0]
        _RVM1(0x1ecf, 0, 0xF0),   //reg[101ecf]#7 ~ #4 = 0000b
        //reg_ts3config[3:0]
        _RVM1(0x1ecf, 0, 0xF0),   //reg[101ecf]#7 ~ #4 = 0000b
        //reg_ts3config[3:0]
        _RVM1(0x1ecf, 0, 0xF0),   //reg[101ecf]#7 ~ #4 = 0000b
        //reg_ts3config[3:0]
        _RVM1(0x1ecf, 0, 0xF0),   //reg[101ecf]#7 ~ #4 = 0000b
        //reg_i2sout_in_tcon
        _RVM1(0x1e81, 0, BIT0),   //reg[101e81]#0 = 0b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_TS3_D5_IS_GPIO != GPIO_NONE)
        #define PAD_TS3_D5_OEN (PAD_TS3_D5_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_TS3_D5_OUT (PAD_TS3_D5_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
_MEMMAP_nonPM_11_,
        _RVM1(0x0305, PAD_TS3_D5_OUT, BIT0),
        _RVM1(0x0305, PAD_TS3_D5_OEN, BIT1),
        //reg_chiptop_dummy_2[2]
_MEMMAP_nonPM_,
        _RVM1(0x1e3c, 0, BIT2),   //reg[101e3c]#2 = 0b
        //reg_tso_evd_mode[1:0]
        _RVM1(0x1e21, 0, BIT2 | BIT1),   //reg[101e21]#2 ~ #1 = 00b
        //reg_ts3config[3:0]
        _RVM1(0x1ecf, 0, 0xF0),   //reg[101ecf]#7 ~ #4 = 0000b
        //reg_ts3config[3:0]
        _RVM1(0x1ecf, 0, 0xF0),   //reg[101ecf]#7 ~ #4 = 0000b
        //reg_ts3config[3:0]
        _RVM1(0x1ecf, 0, 0xF0),   //reg[101ecf]#7 ~ #4 = 0000b
        //reg_ts3config[3:0]
        _RVM1(0x1ecf, 0, 0xF0),   //reg[101ecf]#7 ~ #4 = 0000b
        //reg_ts3config[3:0]
        _RVM1(0x1ecf, 0, 0xF0),   //reg[101ecf]#7 ~ #4 = 0000b
        //reg_i2sout_in_tcon
        _RVM1(0x1e81, 0, BIT0),   //reg[101e81]#0 = 0b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_TS3_D6_IS_GPIO != GPIO_NONE)
        #define PAD_TS3_D6_OEN (PAD_TS3_D6_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_TS3_D6_OUT (PAD_TS3_D6_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
_MEMMAP_nonPM_11_,
        _RVM1(0x0306, PAD_TS3_D6_OUT, BIT0),
        _RVM1(0x0306, PAD_TS3_D6_OEN, BIT1),
        //reg_chiptop_dummy_2[3]
_MEMMAP_nonPM_,
        _RVM1(0x1e3c, 0, BIT3),   //reg[101e3c]#3 = 0b
        //reg_tso_evd_mode[1:0]
        _RVM1(0x1e21, 0, BIT2 | BIT1),   //reg[101e21]#2 ~ #1 = 00b
        //reg_ts3config[3:0]
        _RVM1(0x1ecf, 0, 0xF0),   //reg[101ecf]#7 ~ #4 = 0000b
        //reg_ts3config[3:0]
        _RVM1(0x1ecf, 0, 0xF0),   //reg[101ecf]#7 ~ #4 = 0000b
        //reg_ts3config[3:0]
        _RVM1(0x1ecf, 0, 0xF0),   //reg[101ecf]#7 ~ #4 = 0000b
        //reg_ts3config[3:0]
        _RVM1(0x1ecf, 0, 0xF0),   //reg[101ecf]#7 ~ #4 = 0000b
        //reg_ts3config[3:0]
        _RVM1(0x1ecf, 0, 0xF0),   //reg[101ecf]#7 ~ #4 = 0000b
        //reg_i2sout_in_tcon
        _RVM1(0x1e81, 0, BIT0),   //reg[101e81]#0 = 0b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_TS3_D7_IS_GPIO != GPIO_NONE)
        #define PAD_TS3_D7_OEN (PAD_TS3_D7_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_TS3_D7_OUT (PAD_TS3_D7_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
_MEMMAP_nonPM_11_,
        _RVM1(0x0307, PAD_TS3_D7_OUT, BIT0),
        _RVM1(0x0307, PAD_TS3_D7_OEN, BIT1),
        //reg_tso_evd_mode[1:0]
_MEMMAP_nonPM_,
        _RVM1(0x1e21, 0, BIT2 | BIT1),   //reg[101e21]#2 ~ #1 = 00b
        //reg_ts3config[3:0]
        _RVM1(0x1ecf, 0, 0xF0),   //reg[101ecf]#7 ~ #4 = 0000b
        //reg_ts3config[3:0]
        _RVM1(0x1ecf, 0, 0xF0),   //reg[101ecf]#7 ~ #4 = 0000b
        //reg_ts3config[3:0]
        _RVM1(0x1ecf, 0, 0xF0),   //reg[101ecf]#7 ~ #4 = 0000b
        //reg_ts3config[3:0]
        _RVM1(0x1ecf, 0, 0xF0),   //reg[101ecf]#7 ~ #4 = 0000b
        //reg_ts3config[3:0]
        _RVM1(0x1ecf, 0, 0xF0),   //reg[101ecf]#7 ~ #4 = 0000b
        //reg_i2sout_in_tcon
        _RVM1(0x1e81, 0, BIT0),   //reg[101e81]#0 = 0b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_TS3_SYNC_IS_GPIO != GPIO_NONE)
        #define PAD_TS3_SYNC_OEN (PAD_TS3_SYNC_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_TS3_SYNC_OUT (PAD_TS3_SYNC_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
_MEMMAP_nonPM_11_,
        _RVM1(0x0309, PAD_TS3_SYNC_OUT, BIT0),
        _RVM1(0x0309, PAD_TS3_SYNC_OEN, BIT1),
        //reg_tso_evd_mode[1:0]
_MEMMAP_nonPM_,
        _RVM1(0x1e21, 0, BIT2 | BIT1),   //reg[101e21]#2 ~ #1 = 00b
        //reg_ts3config[3:0]
        _RVM1(0x1ecf, 0, 0xF0),   //reg[101ecf]#7 ~ #4 = 0000b
        //reg_ts3config[3:0]
        _RVM1(0x1ecf, 0, 0xF0),   //reg[101ecf]#7 ~ #4 = 0000b
        //reg_ts3config[3:0]
        _RVM1(0x1ecf, 0, 0xF0),   //reg[101ecf]#7 ~ #4 = 0000b
        //reg_ts3config[3:0]
        _RVM1(0x1ecf, 0, 0xF0),   //reg[101ecf]#7 ~ #4 = 0000b
        //reg_ts3config[3:0]
        _RVM1(0x1ecf, 0, 0xF0),   //reg[101ecf]#7 ~ #4 = 0000b
        //reg_ts3config[3:0]
        _RVM1(0x1ecf, 0, 0xF0),   //reg[101ecf]#7 ~ #4 = 0000b
        //reg_ts3config[3:0]
        _RVM1(0x1ecf, 0, 0xF0),   //reg[101ecf]#7 ~ #4 = 0000b
        //reg_ts3config[3:0]
        _RVM1(0x1ecf, 0, 0xF0),   //reg[101ecf]#7 ~ #4 = 0000b
        //reg_i2sout_in_tcon
        _RVM1(0x1e81, 0, BIT0),   //reg[101e81]#0 = 0b
        //reg_tconconfig[9]
        _RVM1(0x1ea1, 0, BIT1),   //reg[101ea1]#1 = 0b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_TS3_VLD_IS_GPIO != GPIO_NONE)
        #define PAD_TS3_VLD_OEN (PAD_TS3_VLD_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_TS3_VLD_OUT (PAD_TS3_VLD_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
_MEMMAP_nonPM_11_,
        _RVM1(0x0308, PAD_TS3_VLD_OUT, BIT0),
        _RVM1(0x0308, PAD_TS3_VLD_OEN, BIT1),
        //reg_chiptop_dummy_2[4]
_MEMMAP_nonPM_,
        _RVM1(0x1e3c, 0, BIT4),   //reg[101e3c]#4 = 0b
        //reg_tso_evd_mode[1:0]
        _RVM1(0x1e21, 0, BIT2 | BIT1),   //reg[101e21]#2 ~ #1 = 00b
        //reg_ts3config[3:0]
        _RVM1(0x1ecf, 0, 0xF0),   //reg[101ecf]#7 ~ #4 = 0000b
        //reg_ts3config[3:0]
        _RVM1(0x1ecf, 0, 0xF0),   //reg[101ecf]#7 ~ #4 = 0000b
        //reg_ts3config[3:0]
        _RVM1(0x1ecf, 0, 0xF0),   //reg[101ecf]#7 ~ #4 = 0000b
        //reg_ts3config[3:0]
        _RVM1(0x1ecf, 0, 0xF0),   //reg[101ecf]#7 ~ #4 = 0000b
        //reg_ts3config[3:0]
        _RVM1(0x1ecf, 0, 0xF0),   //reg[101ecf]#7 ~ #4 = 0000b
        //reg_ts3config[3:0]
        _RVM1(0x1ecf, 0, 0xF0),   //reg[101ecf]#7 ~ #4 = 0000b
        //reg_ts3config[3:0]
        _RVM1(0x1ecf, 0, 0xF0),   //reg[101ecf]#7 ~ #4 = 0000b
        //reg_i2sout_in_tcon
        _RVM1(0x1e81, 0, BIT0),   //reg[101e81]#0 = 0b
        //reg_tconconfig[8]
        _RVM1(0x1ea1, 0, BIT0),   //reg[101ea1]#0 = 0b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif

    #if(PAD_VSYNC_Like_IS_GPIO != GPIO_NONE)
        #define PAD_VSYNC_Like_OEN (PAD_VSYNC_Like_IS_GPIO == GPIO_IN ? BIT1: 0)
        #define PAD_VSYNC_Like_OUT (PAD_VSYNC_Like_IS_GPIO == GPIO_OUT_HIGH ? BIT0: 0)
        _RVM1(0x2bb0, PAD_VSYNC_Like_OUT, BIT0),
        _RVM1(0x2bb0, PAD_VSYNC_Like_OEN, BIT1),
        //reg_vsync_like_config[1:0]
        _RVM1(0x1eb4, 0, BIT7 | BIT6),   //reg[101eb4]#7 ~ #6 = 00b
        //reg_vsync_like_config[1:0]
        _RVM1(0x1eb4, 0, BIT7 | BIT6),   //reg[101eb4]#7 ~ #6 = 00b
        //reg_vsync_like_config[1:0]
        _RVM1(0x1eb4, 0, BIT7 | BIT6),   //reg[101eb4]#7 ~ #6 = 00b
        //reg_ext_int[1]
        _RVM1(0x1ea5, 0, BIT1),   //reg[101ea5]#1 = 0b
        //reg_allpad_in
        //_RVM1(0x1ea1, 0, BIT7),   //reg[101ea1]#7 = 0b
    #endif


//---------------------------------------------------------------------
// Pad Configuartion
//---------------------------------------------------------------------

    _MEMMAP_nonPM_,

//---------------------------------------------------------------------
// RF and IF AGC
#ifdef PADS_AGC_DBG
#if (PADS_AGC_DBG != CONFIG_PADMUX_UNKNOWN)
#define CONFIG_AGC_DBG_MODE1 ((PADS_AGC_DBG == CONFIG_PADMUX_MODE1) ? (0x01 << 7) : 0)
    _RVM1(0x1e9e, CONFIG_AGC_DBG_MODE1, BIT7),
#endif
#endif

// None
#ifdef PADS_PCM_CONFIG
#if (PADS_PCM_CONFIG != CONFIG_PADMUX_UNKNOWN)
#define CONFIG_PCM_CONFIG_MODE1 ((PADS_PCM_CONFIG == CONFIG_PADMUX_MODE1) ? (0x01 << 1) : 0)
    _RVM1(0x1ef6, CONFIG_PCM_CONFIG_MODE1, BIT1),
#endif
#endif

// Freeze Tuner
#ifdef PADS_FREEZE_TUNER
#if (PADS_FREEZE_TUNER != CONFIG_PADMUX_UNKNOWN)
#define CONFIG_FREEZE_TUNER_MODE1 ((PADS_FREEZE_TUNER == CONFIG_PADMUX_MODE1) ? (0x01 << 3) : \
                                   (PADS_FREEZE_TUNER == CONFIG_PADMUX_MODE2) ? (0x02 << 3) : 0)
    _RVM1(0x1e0d, CONFIG_FREEZE_TUNER_MODE1, BITMASK(4:3)),
#endif
#endif

// VIF VSYNC
#ifdef PADS_VSYNC_VIF_OUT_EN
#if (PADS_VSYNC_VIF_OUT_EN != CONFIG_PADMUX_UNKNOWN)
#define CONFIG_VSYNC_VIF_OUT_EN_MODE1 ((PADS_VSYNC_VIF_OUT_EN == CONFIG_PADMUX_MODE1) ? (0x01 << 6) : 0)
    _RVM1(0x1ea3, CONFIG_VSYNC_VIF_OUT_EN_MODE1, BIT6),
#endif
#endif

// 1p in
#ifdef PADS_TS0CONFIG
#if (PADS_TS0CONFIG != CONFIG_PADMUX_UNKNOWN)
#define CONFIG_TS0CONFIG_MODE1 ((PADS_TS0CONFIG == CONFIG_PADMUX_MODE1) ? (0x01 << 0) : \
                                (PADS_TS0CONFIG == CONFIG_PADMUX_MODE2) ? (0x02 << 0) : \
                                (PADS_TS0CONFIG == CONFIG_PADMUX_MODE3) ? (0x03 << 0) : \
                                (PADS_TS0CONFIG == CONFIG_PADMUX_MODE4) ? (0x04 << 0) : 0)
    _RVM1(0x1eaf, CONFIG_TS0CONFIG_MODE1, BITMASK(2:0)),
#endif
#endif

// 1p in
#ifdef PADS_TS1CONFIG
#if (PADS_TS1CONFIG != CONFIG_PADMUX_UNKNOWN)
#define CONFIG_TS1CONFIG_MODE1 ((PADS_TS1CONFIG == CONFIG_PADMUX_MODE1) ? (0x01 << 3) : \
                                (PADS_TS1CONFIG == CONFIG_PADMUX_MODE2) ? (0x02 << 3) : \
                                (PADS_TS1CONFIG == CONFIG_PADMUX_MODE3) ? (0x03 << 3) : \
                                (PADS_TS1CONFIG == CONFIG_PADMUX_MODE4) ? (0x04 << 3) : \
                                (PADS_TS1CONFIG == CONFIG_PADMUX_MODE5) ? (0x05 << 3) : \
                                (PADS_TS1CONFIG == CONFIG_PADMUX_MODE6) ? (0x06 << 3) : 0)
    _RVM1(0x1eaf, CONFIG_TS1CONFIG_MODE1, BITMASK(5:3)),
#endif
#endif

// ts 1p out
#ifdef PADS_TS_OUT_MODE
#if (PADS_TS_OUT_MODE != CONFIG_PADMUX_UNKNOWN)
#define CONFIG_TS_OUT_MODE_MODE1 ((PADS_TS_OUT_MODE == CONFIG_PADMUX_MODE1) ? (0x01 << 4) : \
                                  (PADS_TS_OUT_MODE == CONFIG_PADMUX_MODE2) ? (0x02 << 4) : \
                                  (PADS_TS_OUT_MODE == CONFIG_PADMUX_MODE3) ? (0x03 << 4) : \
                                  (PADS_TS_OUT_MODE == CONFIG_PADMUX_MODE4) ? (0x04 << 4) : \
                                  (PADS_TS_OUT_MODE == CONFIG_PADMUX_MODE5) ? (0x05 << 4) : 0)
    _RVM1(0x1e80, CONFIG_TS_OUT_MODE_MODE1, BITMASK(6:4)),
#endif
#endif


// None
#ifdef PADS_TSO_EVD_MODE
#if (PADS_TSO_EVD_MODE != CONFIG_PADMUX_UNKNOWN)
#define CONFIG_TSO_EVD_MODE_MODE1 ((PADS_TSO_EVD_MODE == CONFIG_PADMUX_MODE1) ? (0x01 << 1) : \
                                   (PADS_TSO_EVD_MODE == CONFIG_PADMUX_MODE2) ? (0x02 << 1) : 0)
    _RVM1(0x1e21, CONFIG_TSO_EVD_MODE_MODE1, BITMASK(2:1)),
#endif
#endif

// 1s in
#ifdef PADS_TS2CONFIG
#if (PADS_TS2CONFIG != CONFIG_PADMUX_UNKNOWN)
#define CONFIG_TS2CONFIG_MODE1 ((PADS_TS2CONFIG == CONFIG_PADMUX_MODE1) ? (0x01 << 4) : \
                                (PADS_TS2CONFIG == CONFIG_PADMUX_MODE2) ? (0x02 << 4) : \
                                (PADS_TS2CONFIG == CONFIG_PADMUX_MODE3) ? (0x03 << 4) : \
                                (PADS_TS2CONFIG == CONFIG_PADMUX_MODE4) ? (0x04 << 4) : 0)
    _RVM1(0x1eb5, CONFIG_TS2CONFIG_MODE1, BITMASK(6:4)),
#endif
#endif

// 1s in
#ifdef PADS_TS3CONFIG
#if (PADS_TS3CONFIG != CONFIG_PADMUX_UNKNOWN)
#define CONFIG_TS3CONFIG_MODE1 ((PADS_TS3CONFIG == CONFIG_PADMUX_MODE1) ? (0x01 << 4) : \
                                (PADS_TS3CONFIG == CONFIG_PADMUX_MODE2) ? (0x02 << 4) : \
                                (PADS_TS3CONFIG == CONFIG_PADMUX_MODE3) ? (0x03 << 4) : \
                                (PADS_TS3CONFIG == CONFIG_PADMUX_MODE4) ? (0x04 << 4) : \
                                (PADS_TS3CONFIG == CONFIG_PADMUX_MODE5) ? (0x05 << 4) : \
                                (PADS_TS3CONFIG == CONFIG_PADMUX_MODE6) ? (0x06 << 4) : \
                                (PADS_TS3CONFIG == CONFIG_PADMUX_MODE7) ? (0x07 << 4) : \
                                (PADS_TS3CONFIG == CONFIG_PADMUX_MODE8) ? (0x08 << 4) : \
                                (PADS_TS3CONFIG == CONFIG_PADMUX_MODE9) ? (0x09 << 4) : 0)
    _RVM1(0x1ecf, CONFIG_TS3CONFIG_MODE1, BITMASK(7:4)),
#endif
#endif

// ts4 = 1s in
#ifdef PADS_TS4CONFIG
#if (PADS_TS4CONFIG != CONFIG_PADMUX_UNKNOWN)
#define CONFIG_TS4CONFIG_MODE1 ((PADS_TS4CONFIG == CONFIG_PADMUX_MODE1) ? (0x01 << 1) : \
                                (PADS_TS4CONFIG == CONFIG_PADMUX_MODE3) ? (0x03 << 1) : \
                                (PADS_TS4CONFIG == CONFIG_PADMUX_MODE4) ? (0x04 << 1) : 0)
    _RVM1(0x1e81, CONFIG_TS4CONFIG_MODE1, BITMASK(3:1)),
#endif
#endif

// PCM CTRL
#ifdef PADS_PCMCTRLCONFIG
#if (PADS_PCMCTRLCONFIG != CONFIG_PADMUX_UNKNOWN)
#define CONFIG_PCMCTRLCONFIG_MODE1 ((PADS_PCMCTRLCONFIG == CONFIG_PADMUX_MODE1) ? (0x01 << 5) : 0)
    _RVM1(0x1ec8, CONFIG_PCMCTRLCONFIG_MODE1, BIT5),
#endif
#endif

// PCM  AD
#ifdef PADS_PCMADCONFIG
#if (PADS_PCMADCONFIG != CONFIG_PADMUX_UNKNOWN)
#define CONFIG_PCMADCONFIG_MODE1 ((PADS_PCMADCONFIG == CONFIG_PADMUX_MODE1) ? (0x01 << 4) : 0)
    _RVM1(0x1ec8, CONFIG_PCMADCONFIG_MODE1, BIT4),
#endif
#endif

// CIMAX Ctrl (None)
#ifdef PADS_CICTRLCONFIG
#if (PADS_CICTRLCONFIG != CONFIG_PADMUX_UNKNOWN)
#define CONFIG_CICTRLCONFIG_MODE1 ((PADS_CICTRLCONFIG == CONFIG_PADMUX_MODE1) ? (0x01 << 1) : 0)
    _RVM1(0x1ec8, CONFIG_CICTRLCONFIG_MODE1, BIT1),
#endif
#endif

// CIMAX AD (None)
#ifdef PADS_CIADCONFIG
#if (PADS_CIADCONFIG != CONFIG_PADMUX_UNKNOWN)
#define CONFIG_CIADCONFIG_MODE1 ((PADS_CIADCONFIG == CONFIG_PADMUX_MODE1) ? (0x01 << 0) : 0)
    _RVM1(0x1ec8, CONFIG_CIADCONFIG_MODE1, BIT0),
#endif
#endif

// PCM CTRL
#ifdef PADS_PCM2CTRLCONFIG
#if (PADS_PCM2CTRLCONFIG != CONFIG_PADMUX_UNKNOWN)
#define CONFIG_PCM2CTRLCONFIG_MODE1 ((PADS_PCM2CTRLCONFIG == CONFIG_PADMUX_MODE1) ? (0x01 << 3) : 0)
    _RVM1(0x1ec8, CONFIG_PCM2CTRLCONFIG_MODE1, BIT3),
#endif
#endif

// SPDIF OUT
#ifdef PADS_SPDIFOUTCONFIG
#if (PADS_SPDIFOUTCONFIG != CONFIG_PADMUX_UNKNOWN)
#define CONFIG_SPDIFOUTCONFIG_MODE1 ((PADS_SPDIFOUTCONFIG == CONFIG_PADMUX_MODE1) ? (0x01 << 7) : 0)
    _RVM1(0x1eae, CONFIG_SPDIFOUTCONFIG_MODE1, BIT7),
#endif
#endif

// SPDIF IN
#ifdef PADS_SPDIFINCONFIG
#if (PADS_SPDIFINCONFIG != CONFIG_PADMUX_UNKNOWN)
#define CONFIG_SPDIFINCONFIG_MODE1 ((PADS_SPDIFINCONFIG == CONFIG_PADMUX_MODE1) ? (0x01 << 6) : 0)
    _RVM1(0x1eae, CONFIG_SPDIFINCONFIG_MODE1, BIT6),
#endif
#endif

// 2nd SPDIF OUT
#ifdef PADS_SPDIFOUTCONFIG2
#if (PADS_SPDIFOUTCONFIG2 != CONFIG_PADMUX_UNKNOWN)
#define CONFIG_SPDIFOUTCONFIG2_MODE1 ((PADS_SPDIFOUTCONFIG2 == CONFIG_PADMUX_MODE1) ? (0x01 << 0) : 0)
    _RVM1(0x1eb3, CONFIG_SPDIFOUTCONFIG2_MODE1, BIT0),
#endif
#endif

// I2S In
#ifdef PADS_I2SINCONFIG
#if (PADS_I2SINCONFIG != CONFIG_PADMUX_UNKNOWN)
#define CONFIG_I2SINCONFIG_MODE1 ((PADS_I2SINCONFIG == CONFIG_PADMUX_MODE1) ? (0x01 << 2) : 0)
    _RVM1(0x1eae, CONFIG_I2SINCONFIG_MODE1, BIT2),
#endif
#endif

// I2S MUTE
#ifdef PADS_I2SMUTEMODE
#if (PADS_I2SMUTEMODE != CONFIG_PADMUX_UNKNOWN)
#define CONFIG_I2SMUTEMODE_MODE1 ((PADS_I2SMUTEMODE == CONFIG_PADMUX_MODE1) ? (0x01 << 6) : \
                                  (PADS_I2SMUTEMODE == CONFIG_PADMUX_MODE2) ? (0x02 << 6) : 0)
    _RVM1(0x1e05, CONFIG_I2SMUTEMODE_MODE1, BITMASK(7:6)),
#endif
#endif

// I2S Out All in Tcon
#ifdef PADS_I2SOUT_IN_TCON
#if (PADS_I2SOUT_IN_TCON != CONFIG_PADMUX_UNKNOWN)
#define CONFIG_I2SOUT_IN_TCON_MODE1 ((PADS_I2SOUT_IN_TCON == CONFIG_PADMUX_MODE1) ? (0x01 << 0) : 0)
    _RVM1(0x1e81, CONFIG_I2SOUT_IN_TCON_MODE1, BIT0),
#endif
#endif

// I2S Out CK/WS
#ifdef PADS_I2SOUTCONFIG0
#if (PADS_I2SOUTCONFIG0 != CONFIG_PADMUX_UNKNOWN)
#define CONFIG_I2SOUTCONFIG0_MODE1 ((PADS_I2SOUTCONFIG0 == CONFIG_PADMUX_MODE1) ? (0x01 << 4) : 0)
    _RVM1(0x1eae, CONFIG_I2SOUTCONFIG0_MODE1, BIT4),
#endif
#endif

// I2S Out SD0
#ifdef PADS_I2SOUTCONFIG1
#if (PADS_I2SOUTCONFIG1 != CONFIG_PADMUX_UNKNOWN)
#define CONFIG_I2SOUTCONFIG1_MODE1 ((PADS_I2SOUTCONFIG1 == CONFIG_PADMUX_MODE1) ? (0x01 << 5) : 0)
    _RVM1(0x1eae, CONFIG_I2SOUTCONFIG1_MODE1, BIT5),
#endif
#endif

// I2S Out SD1
#ifdef PADS_I2SOUTCONFIG2
#if (PADS_I2SOUTCONFIG2 != CONFIG_PADMUX_UNKNOWN)
#define CONFIG_I2SOUTCONFIG2_MODE1 ((PADS_I2SOUTCONFIG2 == CONFIG_PADMUX_MODE1) ? (0x01 << 3) : 0)
    _RVM1(0x1eb3, CONFIG_I2SOUTCONFIG2_MODE1, BIT3),
#endif
#endif

// I2S Out SD2
#ifdef PADS_I2SOUTCONFIG3
#if (PADS_I2SOUTCONFIG3 != CONFIG_PADMUX_UNKNOWN)
#define CONFIG_I2SOUTCONFIG3_MODE1 ((PADS_I2SOUTCONFIG3 == CONFIG_PADMUX_MODE1) ? (0x01 << 4) : 0)
    _RVM1(0x1eb3, CONFIG_I2SOUTCONFIG3_MODE1, BIT4),
#endif
#endif

// I2S Out SD3
#ifdef PADS_I2SOUTCONFIG4
#if (PADS_I2SOUTCONFIG4 != CONFIG_PADMUX_UNKNOWN)
#define CONFIG_I2SOUTCONFIG4_MODE1 ((PADS_I2SOUTCONFIG4 == CONFIG_PADMUX_MODE1) ? (0x01 << 5) : 0)
    _RVM1(0x1eb3, CONFIG_I2SOUTCONFIG4_MODE1, BIT5),
#endif
#endif

// RMII
#ifdef PADS_ET_MODE
#if (PADS_ET_MODE != CONFIG_PADMUX_UNKNOWN)
#define CONFIG_ET_MODE_MODE1 ((PADS_ET_MODE == CONFIG_PADMUX_MODE1) ? (0x01 << 0) : 0)
    _RVM1(0x1edf, CONFIG_ET_MODE_MODE1, BIT0),
#endif
#endif

// MDIO
#ifdef PADS_MDIO_MODE
#if (PADS_MDIO_MODE != CONFIG_PADMUX_UNKNOWN)
#define CONFIG_MDIO_MODE_MODE1 ((PADS_MDIO_MODE == CONFIG_PADMUX_MODE1) ? (0x01 << 0) : 0)
_MEMMAP_nonPM_15_,
    _RVM1(0x1e00, CONFIG_MDIO_MODE_MODE1, BIT0),
_MEMMAP_nonPM_,
#endif
#endif

// EPHY LED
#ifdef PADS_LED_MODE
#if (PADS_LED_MODE != CONFIG_PADMUX_UNKNOWN)
#define CONFIG_LED_MODE_MODE1 ((PADS_LED_MODE == CONFIG_PADMUX_MODE1) ? (0x01 << 4) : 0)
    _RVM1(0x1eb4, CONFIG_LED_MODE_MODE1, BIT4),
#endif
#endif

// LDM_SPI0
#ifdef PADS_VSYNC_LIKE_CONFIG
#if (PADS_VSYNC_LIKE_CONFIG != CONFIG_PADMUX_UNKNOWN)
#define CONFIG_VSYNC_LIKE_CONFIG_MODE ((PADS_VSYNC_LIKE_CONFIG == CONFIG_PADMUX_MODE1) ? (0x01 << 6) : \
                                       (PADS_VSYNC_LIKE_CONFIG == CONFIG_PADMUX_MODE2) ? (0x02 << 6) : \
                                       (PADS_VSYNC_LIKE_CONFIG == CONFIG_PADMUX_MODE3) ? (0x03 << 6) : 0)
    _RVM1(0x1eb4, CONFIG_VSYNC_LIKE_CONFIG_MODE, BITMASK(7:6)),
#endif
#endif

// 2-wired MSPI
#ifdef PADS_LD_SPI1_CONFIG
#if (PADS_LD_SPI1_CONFIG != CONFIG_PADMUX_UNKNOWN)
#define CONFIG_LD_SPI1_CONFIG_MODE ((PADS_LD_SPI1_CONFIG == CONFIG_PADMUX_MODE1) ? (0x01 << 0) : \
                                    (PADS_LD_SPI1_CONFIG == CONFIG_PADMUX_MODE2) ? (0x02 << 0) : 0)
    _RVM1(0x1e9c, CONFIG_LD_SPI1_CONFIG_MODE, BITMASK(1:0)),
#endif
#endif

// 2-wired MSPI
#ifdef PADS_LD_SPI2_CONFIG
#if (PADS_LD_SPI2_CONFIG != CONFIG_PADMUX_UNKNOWN)
#define CONFIG_LD_SPI2_CONFIG_MODE1 ((PADS_LD_SPI2_CONFIG == CONFIG_PADMUX_MODE1) ? (0x01 << 2) : \
                                     (PADS_LD_SPI2_CONFIG == CONFIG_PADMUX_MODE2) ? (0x02 << 2) : 0)
    _RVM1(0x1e9c, CONFIG_LD_SPI2_CONFIG_MODE1, BITMASK(3:2)),
#endif
#endif

// 4-wired MSPI
#ifdef PADS_LD_SPI3_CONFIG
#if (PADS_LD_SPI3_CONFIG != CONFIG_PADMUX_UNKNOWN)
#define CONFIG_LD_SPI3_CONFIG_MODE1 ((PADS_LD_SPI3_CONFIG == CONFIG_PADMUX_MODE1) ? (0x01 << 4) : \
                                     (PADS_LD_SPI3_CONFIG == CONFIG_PADMUX_MODE2) ? (0x02 << 4) : 0)
    _RVM1(0x1e9c, CONFIG_LD_SPI3_CONFIG_MODE1, BITMASK(5:4)),
#endif
#endif

// PWM dimming
#ifdef PADS_DIM_CONFIG
#if (PADS_DIM_CONFIG != CONFIG_PADMUX_UNKNOWN)
#define CONFIG_DIM_CONFIG_MODE1 ((PADS_DIM_CONFIG == CONFIG_PADMUX_MODE1) ? (0x01 << 0) : 0)
    _RVM1(0x1ef6, CONFIG_DIM_CONFIG_MODE1, BIT0),
#endif
#endif

// None
#ifdef PADS_DSPEJTAGMODE
#if (PADS_DSPEJTAGMODE != CONFIG_PADMUX_UNKNOWN)
#define CONFIG_DSPEJTAGMODE_MODE1 ((PADS_DSPEJTAGMODE == CONFIG_PADMUX_MODE1) ? (0x01 << 0) : \
                                   (PADS_DSPEJTAGMODE == CONFIG_PADMUX_MODE2) ? (0x02 << 0) : \
                                   (PADS_DSPEJTAGMODE == CONFIG_PADMUX_MODE3) ? (0x03 << 0) : 0)
    _RVM1(0x1e07, CONFIG_DSPEJTAGMODE_MODE1, BITMASK(1:0)),
#endif
#endif

// UART
#ifdef PADS_SECONDUARTMODE
#if (PADS_SECONDUARTMODE != CONFIG_PADMUX_UNKNOWN)
#define CONFIG_SECONDUARTMODE_MODE1 ((PADS_SECONDUARTMODE == CONFIG_PADMUX_MODE1) ? (0x01 << 0) : \
                                     (PADS_SECONDUARTMODE == CONFIG_PADMUX_MODE2) ? (0x02 << 0) : \
                                     (PADS_SECONDUARTMODE == CONFIG_PADMUX_MODE3) ? (0x03 << 0) : 0)
    _RVM1(0x1e05, CONFIG_SECONDUARTMODE_MODE1, BITMASK(1:0)),
#endif
#endif

// UART
#ifdef PADS_THIRDUARTMODE
#if (PADS_THIRDUARTMODE != CONFIG_PADMUX_UNKNOWN)
#define CONFIG_THIRDUARTMODE_MODE1 ((PADS_THIRDUARTMODE == CONFIG_PADMUX_MODE1) ? (0x01 << 2) : \
                                    (PADS_THIRDUARTMODE == CONFIG_PADMUX_MODE2) ? (0x02 << 2) : \
                                    (PADS_THIRDUARTMODE == CONFIG_PADMUX_MODE3) ? (0x03 << 2) : 0)
    _RVM1(0x1e05, CONFIG_THIRDUARTMODE_MODE1, BITMASK(3:2)),
#endif
#endif

// UART
#ifdef PADS_FOURTHUARTMODE
#if (PADS_FOURTHUARTMODE != CONFIG_PADMUX_UNKNOWN)
#define CONFIG_FOURTHUARTMODE_MODE1 ((PADS_FOURTHUARTMODE == CONFIG_PADMUX_MODE1) ? (0x01 << 6) : \
                                     (PADS_FOURTHUARTMODE == CONFIG_PADMUX_MODE2) ? (0x02 << 6) : \
                                     (PADS_FOURTHUARTMODE == CONFIG_PADMUX_MODE3) ? (0x03 << 6) : 0)
    _RVM1(0x1e04, CONFIG_FOURTHUARTMODE_MODE1, BITMASK(7:6)),
#endif
#endif

// UART
#ifdef PADS_FIFTHUARTMODE
#if (PADS_FIFTHUARTMODE != CONFIG_PADMUX_UNKNOWN)
#define CONFIG_FIFTHUARTMODE_MODE1 ((PADS_FIFTHUARTMODE == CONFIG_PADMUX_MODE1) ? (0x01 << 2) : \
                                    (PADS_FIFTHUARTMODE == CONFIG_PADMUX_MODE2) ? (0x02 << 2) : \
                                    (PADS_FIFTHUARTMODE == CONFIG_PADMUX_MODE3) ? (0x03 << 2) : 0)
    _RVM1(0x1e08, CONFIG_FIFTHUARTMODE_MODE1, BITMASK(3:2)),
#endif
#endif

// UART
#ifdef PADS_SIXTHUARTMODE
#if (PADS_SIXTHUARTMODE != CONFIG_PADMUX_UNKNOWN)
#define CONFIG_SIXTHUARTMODE_MODE1 ((PADS_SIXTHUARTMODE == CONFIG_PADMUX_MODE1) ? (0x01 << 6) : 0)
    _RVM1(0x1e08, CONFIG_SIXTHUARTMODE_MODE1, BIT6),
#endif
#endif

// Fast UART CTRL
#ifdef PADS_FASTUARTMODE
#if (PADS_FASTUARTMODE != CONFIG_PADMUX_UNKNOWN)
#define CONFIG_FASTUARTMODE_MODE1 ((PADS_FASTUARTMODE == CONFIG_PADMUX_MODE1) ? (0x01 << 4) : \
                                   (PADS_FASTUARTMODE == CONFIG_PADMUX_MODE2) ? (0x02 << 4) : \
                                   (PADS_FASTUARTMODE == CONFIG_PADMUX_MODE3) ? (0x03 << 4) : 0)
    _RVM1(0x1e04, CONFIG_FASTUARTMODE_MODE1, BITMASK(5:4)),
#endif
#endif

// OD UART
#ifdef PADS_OD2NDUART
#if (PADS_OD2NDUART != CONFIG_PADMUX_UNKNOWN)
#define CONFIG_OD2NDUART_MODE1 ((PADS_OD2NDUART == CONFIG_PADMUX_MODE1) ? (0x01 << 0) : \
                                (PADS_OD2NDUART == CONFIG_PADMUX_MODE2) ? (0x02 << 0) : \
                                (PADS_OD2NDUART == CONFIG_PADMUX_MODE3) ? (0x03 << 0) : 0)
    _RVM1(0x1ea9, CONFIG_OD2NDUART_MODE1, BITMASK(1:0)),
#endif
#endif

// OD UART
#ifdef PADS_OD3RDUART
#if (PADS_OD3RDUART != CONFIG_PADMUX_UNKNOWN)
#define CONFIG_OD3RDUART_MODE1 ((PADS_OD3RDUART == CONFIG_PADMUX_MODE1) ? (0x01 << 2) : \
                                (PADS_OD3RDUART == CONFIG_PADMUX_MODE2) ? (0x02 << 2) : \
                                (PADS_OD3RDUART == CONFIG_PADMUX_MODE3) ? (0x03 << 2) : 0)
    _RVM1(0x1ea9, CONFIG_OD3RDUART_MODE1, BITMASK(3:2)),
#endif
#endif

// OD UART
#ifdef PADS_OD4THUART
#if (PADS_OD4THUART != CONFIG_PADMUX_UNKNOWN)
#define CONFIG_OD4THUART_MODE1 ((PADS_OD4THUART == CONFIG_PADMUX_MODE1) ? (0x01 << 4) : \
                                (PADS_OD4THUART == CONFIG_PADMUX_MODE2) ? (0x02 << 4) : \
                                (PADS_OD4THUART == CONFIG_PADMUX_MODE3) ? (0x03 << 4) : 0)
    _RVM1(0x1ea9, CONFIG_OD4THUART_MODE1, BITMASK(5:4)),
#endif
#endif

// OD UART
#ifdef PADS_OD5THUART
#if (PADS_OD5THUART != CONFIG_PADMUX_UNKNOWN)
#define CONFIG_OD5THUART_MODE1 ((PADS_OD5THUART == CONFIG_PADMUX_MODE1) ? (0x01 << 4) : \
                                (PADS_OD5THUART == CONFIG_PADMUX_MODE2) ? (0x02 << 4) : \
                                (PADS_OD5THUART == CONFIG_PADMUX_MODE3) ? (0x03 << 4) : 0)
    _RVM1(0x1eaa, CONFIG_OD5THUART_MODE1, BITMASK(5:4)),
#endif
#endif

// OD UART
#ifdef PADS_OD6THUART
#if (PADS_OD6THUART != CONFIG_PADMUX_UNKNOWN)
#define CONFIG_OD6THUART_MODE1 ((PADS_OD6THUART == CONFIG_PADMUX_MODE1) ? (0x01 << 6) : 0)
    _RVM1(0x1eaa, CONFIG_OD6THUART_MODE1, BIT6),
#endif
#endif

//LG UART source select
#ifdef UART1_SRC_SEL
#if (UART1_SRC_SEL != CONFIG_PADMUX_UNKNOWN)
    _RVM1(0x1ea6, 0x44, BITMASK(7:0)),
#endif
#endif

// PWM[0]
#ifdef PADS_PWM0_MODE
#if (PADS_PWM0_MODE != CONFIG_PADMUX_UNKNOWN)
#define CONFIG_PWM0_MODE_MODE1 ((PADS_PWM0_MODE == CONFIG_PADMUX_MODE1) ? (0x01 << 2) : 0)
    _RVM1(0x1ec8, CONFIG_PWM0_MODE_MODE1, BIT2),
#endif
#endif

// PWM[1]
#ifdef PADS_PWM1_MODE
#if (PADS_PWM1_MODE != CONFIG_PADMUX_UNKNOWN)
#define CONFIG_PWM1_MODE_MODE1 ((PADS_PWM1_MODE == CONFIG_PADMUX_MODE1) ? (0x01 << 6) : 0)
    _RVM1(0x1ec8, CONFIG_PWM1_MODE_MODE1, BIT6),
#endif
#endif

// PWM[2]
#ifdef PADS_PWM2_MODE
#if (PADS_PWM2_MODE != CONFIG_PADMUX_UNKNOWN)
#define CONFIG_PWM2_MODE_MODE1 ((PADS_PWM2_MODE == CONFIG_PADMUX_MODE1) ? (0x01 << 7) : 0)
    _RVM1(0x1ec8, CONFIG_PWM2_MODE_MODE1, BIT7),
#endif
#endif

// PWM[3]
#ifdef PADS_PWM3_MODE
#if (PADS_PWM3_MODE != CONFIG_PADMUX_UNKNOWN)
#define CONFIG_PWM3_MODE_MODE1 ((PADS_PWM3_MODE == CONFIG_PADMUX_MODE1) ? (0x01 << 4) : 0)
    _RVM1(0x1ec9, CONFIG_PWM3_MODE_MODE1, BIT4),
#endif
#endif

// PWM[4]
#ifdef PADS_PWM4_MODE
#if (PADS_PWM4_MODE != CONFIG_PADMUX_UNKNOWN)
#define CONFIG_PWM4_MODE_MODE1 ((PADS_PWM4_MODE == CONFIG_PADMUX_MODE1) ? (0x01 << 5) : 0)
    _RVM1(0x1ec9, CONFIG_PWM4_MODE_MODE1, BIT5),
#endif
#endif

// VBY1 Video monitor
#ifdef PADS_VX1GPI_MODE
#if (PADS_VX1GPI_MODE != CONFIG_PADMUX_UNKNOWN)
#define CONFIG_VX1GPI_MODE_MODE1 ((PADS_VX1GPI_MODE == CONFIG_PADMUX_MODE1) ? (0x01 << 0) : 0)
    _RVM1(0x1e4a, CONFIG_VX1GPI_MODE_MODE1, BIT0),
#endif
#endif

// VBY1 OSD monitor
#ifdef PADS_VX1GPI_OSD_MODE
#if (PADS_VX1GPI_OSD_MODE != CONFIG_PADMUX_UNKNOWN)
#define CONFIG_VX1GPI_OSD_MODE_MODE1 ((PADS_VX1GPI_OSD_MODE == CONFIG_PADMUX_MODE1) ? (0x01 << 1) : 0)
    _RVM1(0x1e4a, CONFIG_VX1GPI_OSD_MODE_MODE1, BIT1),
#endif
#endif

// IRE_OUT
#ifdef PADS_IRE_MODE
#if (PADS_IRE_MODE != CONFIG_PADMUX_UNKNOWN)
#define CONFIG_IRE_MODE_MODE1 ((PADS_IRE_MODE == CONFIG_PADMUX_MODE1) ? (0x01 << 4) : \
                               (PADS_IRE_MODE == CONFIG_PADMUX_MODE2) ? (0x02 << 4) : \
                               (PADS_IRE_MODE == CONFIG_PADMUX_MODE3) ? (0x03 << 4) : 0)
    _RVM1(0x1edf, CONFIG_IRE_MODE_MODE1, BITMASK(5:4)),
#endif
#endif

// Tserr Out
#ifdef PADS_TSERROUT
#if (PADS_TSERROUT != CONFIG_PADMUX_UNKNOWN)
#define CONFIG_TSERROUT_MODE1 ((PADS_TSERROUT == CONFIG_PADMUX_MODE1) ? (0x01 << 0) : \
                               (PADS_TSERROUT == CONFIG_PADMUX_MODE2) ? (0x02 << 0) : \
                               (PADS_TSERROUT == CONFIG_PADMUX_MODE3) ? (0x03 << 0) : 0)
    _RVM1(0x1ec9, CONFIG_TSERROUT_MODE1, BITMASK(1:0)),
#endif
#endif

// DDCR mode
#ifdef PADS_DDCRMODE
#if (PADS_DDCRMODE != CONFIG_PADMUX_UNKNOWN)
#define CONFIG_DDCRMODE_MODE1 ((PADS_DDCRMODE == CONFIG_PADMUX_MODE1) ? (0x01 << 0) : \
                               (PADS_DDCRMODE == CONFIG_PADMUX_MODE2) ? (0x02 << 0) : 0)
    _RVM1(0x1eae, CONFIG_DDCRMODE_MODE1, BITMASK(1:0)),
#endif
#endif

// Normal MIIC
#ifdef PADS_MIIC_MODE0
#if (PADS_MIIC_MODE0 != CONFIG_PADMUX_UNKNOWN)
#define CONFIG_MIIC_MODE0_MODE1 ((PADS_MIIC_MODE0 == CONFIG_PADMUX_MODE1) ? (0x01 << 6) : 0)
_MEMMAP_PM_,

    _RVM1(0x1eae, CONFIG_MIIC_MODE0_MODE1, BITMASK(7:6)),
_MEMMAP_nonPM_,
#endif
#endif

// Normal MIIC
#ifdef PADS_MIIC_MODE1
#if (PADS_MIIC_MODE1 != CONFIG_PADMUX_UNKNOWN)
#define CONFIG_MIIC_MODE1_MODE1 ((PADS_MIIC_MODE1 == CONFIG_PADMUX_MODE1) ? (0x01 << 1) : \
                                 (PADS_MIIC_MODE1 == CONFIG_PADMUX_MODE2) ? (0x02 << 1) : 0)
    _RVM1(0x1edc, CONFIG_MIIC_MODE1_MODE1, BITMASK(2:1)),
#endif
#endif

// Normal MIIC
#ifdef PADS_MIIC_MODE2
#if (PADS_MIIC_MODE2 != CONFIG_PADMUX_UNKNOWN)
#define CONFIG_MIIC_MODE2_MODE1 ((PADS_MIIC_MODE2 == CONFIG_PADMUX_MODE1) ? (0x01 << 3) : 0)
    _RVM1(0x1edc, CONFIG_MIIC_MODE2_MODE1, BIT3),
#endif
#endif

// Normal MIIC
#ifdef PADS_MIIC_MODE3
#if (PADS_MIIC_MODE3 != CONFIG_PADMUX_UNKNOWN)
#define CONFIG_MIIC_MODE3_MODE1 ((PADS_MIIC_MODE3 == CONFIG_PADMUX_MODE1) ? (0x01 << 1) : \
                                 (PADS_MIIC_MODE3 == CONFIG_PADMUX_MODE2) ? (0x02 << 1) : 0)
    _RVM1(0x1edf, CONFIG_MIIC_MODE3_MODE1, BITMASK(2:1)),
#endif
#endif

// Normal MIIC
#ifdef PADS_MIIC_MODE4
#if (PADS_MIIC_MODE4 != CONFIG_PADMUX_UNKNOWN)
#define CONFIG_MIIC_MODE4_MODE1 ((PADS_MIIC_MODE4 == CONFIG_PADMUX_MODE1) ? (0x01 << 0) : 0)
    _RVM1(0x1ede, CONFIG_MIIC_MODE4_MODE1, BIT0),
#endif
#endif

// Normal MIIC
#ifdef PADS_MIIC_MODE5
#if (PADS_MIIC_MODE5 != CONFIG_PADMUX_UNKNOWN)
#define CONFIG_MIIC_MODE5_MODE1 ((PADS_MIIC_MODE5 == CONFIG_PADMUX_MODE1) ? (0x01 << 1) : 0)
    _RVM1(0x1ede, CONFIG_MIIC_MODE5_MODE1, BIT1),
#endif
#endif

// None
#ifdef PADS_PDTRACECTRL
#if (PADS_PDTRACECTRL != CONFIG_PADMUX_UNKNOWN)
#define CONFIG_PDTRACECTRL_MODE1 ((PADS_PDTRACECTRL == CONFIG_PADMUX_MODE1) ? (0x01 << 0) : \
                                  (PADS_PDTRACECTRL == CONFIG_PADMUX_MODE2) ? (0x02 << 0) : 0)
    _RVM1(0x1e9f, CONFIG_PDTRACECTRL_MODE1, BITMASK(1:0)),
#endif
#endif

// Smart Card PAD 1
#ifdef PADS_SM_CONFIG
#if (PADS_SM_CONFIG != CONFIG_PADMUX_UNKNOWN)
#define CONFIG_SM_CONFIG_MODE1 ((PADS_SM_CONFIG == CONFIG_PADMUX_MODE1) ? (0x01 << 4) : \
                                (PADS_SM_CONFIG == CONFIG_PADMUX_MODE2) ? (0x02 << 4) : \
                                (PADS_SM_CONFIG == CONFIG_PADMUX_MODE3) ? (0x03 << 4) : 0)
    _RVM1(0x1edc, CONFIG_SM_CONFIG_MODE1, BITMASK(5:4)),
#endif
#endif

// SD 2.0
#ifdef PADS_SD_CONFIG
#if (PADS_SD_CONFIG != CONFIG_PADMUX_UNKNOWN)
#define CONFIG_SD_CONFIG_MODE1 ((PADS_SD_CONFIG == CONFIG_PADMUX_MODE1) ? (0x01 << 0) : \
                                (PADS_SD_CONFIG == CONFIG_PADMUX_MODE2) ? (0x02 << 0) : \
                                (PADS_SD_CONFIG == CONFIG_PADMUX_MODE3) ? (0x03 << 0) : 0)
    _RVM1(0x1eb5, CONFIG_SD_CONFIG_MODE1, BITMASK(1:0)),
#endif
#endif

// SDIO
#ifdef PADS_SDIO_CONFIG
#if (PADS_SDIO_CONFIG != CONFIG_PADMUX_UNKNOWN)
#define CONFIG_SDIO_CONFIG_MODE1 ((PADS_SDIO_CONFIG == CONFIG_PADMUX_MODE1) ? (0x01 << 4) : \
                                  (PADS_SDIO_CONFIG == CONFIG_PADMUX_MODE2) ? (0x02 << 4) : \
                                  (PADS_SDIO_CONFIG == CONFIG_PADMUX_MODE3) ? (0x03 << 4) : 0)
    _RVM1(0x1ef6, CONFIG_SDIO_CONFIG_MODE1, BITMASK(5:4)),
#endif
#endif

// 3DFLAG mode
#ifdef PADS_3DFLAGCONFIG
#if (PADS_3DFLAGCONFIG != CONFIG_PADMUX_UNKNOWN)
#define CONFIG_3DFLAGCONFIG_MODE1 ((PADS_3DFLAGCONFIG == CONFIG_PADMUX_MODE1) ? (0x01 << 6) : \
                                   (PADS_3DFLAGCONFIG == CONFIG_PADMUX_MODE2) ? (0x02 << 6) : \
                                   (PADS_3DFLAGCONFIG == CONFIG_PADMUX_MODE3) ? (0x03 << 6) : 0)
    _RVM1(0x1eb3, CONFIG_3DFLAGCONFIG_MODE1, BITMASK(7:6)),
#endif
#endif

// 3DFLAG mode
#ifdef PADS_OSD3DFLAG_CONFIG
#if (PADS_OSD3DFLAG_CONFIG != CONFIG_PADMUX_UNKNOWN)
#define CONFIG_OSD3DFLAG_CONFIG_MODE1 ((PADS_OSD3DFLAG_CONFIG == CONFIG_PADMUX_MODE1) ? (0x01 << 6) : \
                                       (PADS_OSD3DFLAG_CONFIG == CONFIG_PADMUX_MODE2) ? (0x02 << 6) : \
                                       (PADS_OSD3DFLAG_CONFIG == CONFIG_PADMUX_MODE3) ? (0x03 << 6) : 0)
    _RVM1(0x1ef6, CONFIG_OSD3DFLAG_CONFIG_MODE1, BITMASK(7:6)),
#endif
#endif

// Through
#ifdef PADS_USB30VCTL_CONFIG
#if (PADS_USB30VCTL_CONFIG != CONFIG_PADMUX_UNKNOWN)
#define CONFIG_USB30VCTL_CONFIG_MODE1 ((PADS_USB30VCTL_CONFIG == CONFIG_PADMUX_MODE1) ? (0x01 << 4) : \
                                       (PADS_USB30VCTL_CONFIG == CONFIG_PADMUX_MODE2) ? (0x02 << 4) : \
                                       (PADS_USB30VCTL_CONFIG == CONFIG_PADMUX_MODE3) ? (0x03 << 4) : 0)
    _RVM1(0x1e08, CONFIG_USB30VCTL_CONFIG_MODE1, BITMASK(5:4)),
#endif
#endif

// Through
#ifdef PADS_USB30VCTL1_CONFIG
#if (PADS_USB30VCTL1_CONFIG != CONFIG_PADMUX_UNKNOWN)
#define CONFIG_USB30VCTL1_CONFIG_MODE1 ((PADS_USB30VCTL1_CONFIG == CONFIG_PADMUX_MODE1) ? (0x01 << 0) : \
                                        (PADS_USB30VCTL1_CONFIG == CONFIG_PADMUX_MODE2) ? (0x02 << 0) : \
                                        (PADS_USB30VCTL1_CONFIG == CONFIG_PADMUX_MODE3) ? (0x03 << 0) : 0)
    _RVM1(0x1e09, CONFIG_USB30VCTL1_CONFIG_MODE1, BITMASK(1:0)),
#endif
#endif

// ARC mode
#ifdef PADS_ARC_MODE
#if (PADS_ARC_MODE != CONFIG_PADMUX_UNKNOWN)
#define CONFIG_ARC_MODE_MODE1 ((PADS_ARC_MODE == CONFIG_PADMUX_MODE1) ? (0x01 << 3) : 0)
_MEMMAP_nonPM_11_,
    _RVM1(0x0320, CONFIG_ARC_MODE_MODE1, BIT3),
_MEMMAP_nonPM_,
#endif
#endif

// LGE EARC mode
#ifdef PADS_LG_EARC_MODE
#if (PADS_LG_EARC_MODE != CONFIG_PADMUX_UNKNOWN)
#define CONFIG_LG_EARC_MODE_MODE1 ((PADS_LG_EARC_MODE == CONFIG_PADMUX_MODE1) ? (0x01 << 7) : 0)
    _RVM1(0x1e0b, CONFIG_LG_EARC_MODE_MODE1, BIT7),
#endif
#endif

// DISEQC_IN
#ifdef PADS_DISEQC_IN_CONFIG
#if (PADS_DISEQC_IN_CONFIG != CONFIG_PADMUX_UNKNOWN)
#define CONFIG_DISEQC_IN_CONFIG_MODE1 ((PADS_DISEQC_IN_CONFIG == CONFIG_PADMUX_MODE1) ? (0x01 << 2) : \
                                       (PADS_DISEQC_IN_CONFIG == CONFIG_PADMUX_MODE2) ? (0x02 << 2) : 0)
    _RVM1(0x1ed0, CONFIG_DISEQC_IN_CONFIG_MODE1, BITMASK(3:2)),
#endif
#endif

// DISEQC_OUT
#ifdef PADS_DISEQC_OUT_CONFIG
#if (PADS_DISEQC_OUT_CONFIG != CONFIG_PADMUX_UNKNOWN)
#define CONFIG_DISEQC_OUT_CONFIG_MODE1 ((PADS_DISEQC_OUT_CONFIG == CONFIG_PADMUX_MODE1) ? (0x01 << 4) : \
                                        (PADS_DISEQC_OUT_CONFIG == CONFIG_PADMUX_MODE2) ? (0x02 << 4) : 0)
    _RVM1(0x1ed0, CONFIG_DISEQC_OUT_CONFIG_MODE1, BITMASK(5:4)),
#endif
#endif

    // Clear all pad in
    _RVM1(0x1EA1, 0, BIT7),

    _END_OF_TBL_,

//---------------------------------------------------------------------
// ISP_TOOL Write Protect
//---------------------------------------------------------------------
};

