//<MStar Software>
//******************************************************************************
// MStar Software
// Copyright (c) 2010 - 2012 MStar Semiconductor, Inc. All rights reserved.
// All software, firmware and related documentation herein () are
// intellectual property of MStar Semiconductor, Inc. () and protected by
// law, including, but not limited to, copyright law and international treaties.
// Any use, modification, reproduction, retransmission, or republication of all
// or part of MStar Software is expressly prohibited, unless prior written
// permission has been granted by MStar.
//
// By accessing, browsing and/or using MStar Software, you acknowledge that you
// have read, understood, and agree, to be bound by below terms () and to
// comply with all applicable laws and regulations:
//
// 1. MStar shall retain any and all right, ownership and interest to MStar
//    Software and any modification/derivatives thereof.
//    No right, ownership, or interest to MStar Software and any
//    modification/derivatives thereof is transferred to you under Terms.
//
// 2. You understand that MStar Software might include, incorporate or be
//    supplied together with third party`s software and the use of MStar
//    Software may require additional licenses from third parties.
//    Therefore, you hereby agree it is your sole responsibility to separately
//    obtain any and all third party right and license necessary for your use of
//    such third party`s software.
//
// 3. MStar Software and any modification/derivatives thereof shall be deemed as
//    MStar`s confidential information and you agree to keep MStar`s
//    confidential information in strictest confidence and not disclose to any
//    third party.
//
// 4. MStar Software is provided on an "AS IS" basis without warranties of any
//    kind. Any warranties are hereby expressly disclaimed by MStar, including
//    without limitation, any warranties of merchantability, non-infringement of
//    intellectual property rights, fitness for a particular purpose, error free
//    and in conformity with any international standard.  You agree to waive any
//    claim against MStar for any loss, damage, cost or expense that you may
//    incur related to your use of MStar Software.
//    In no event shall MStar be liable for any direct, indirect, incidental or
//    consequential damages, including without limitation, lost of profit or
//    revenues, lost or damage of data, and unauthorized system use.
//    You agree that this Section 4 shall still apply without being affected
//    even if MStar Software has been modified by MStar in accordance with your
//    request or instruction for your use, except otherwise agreed by both
//    parties in writing.
//
// 5. If requested, MStar may from time to time provide technical supports or
//    services in relation with MStar Software to you for your use of
//    MStar Software in conjunction with your or your customer`s product
//    ("; Services; ").
//    You understand and agree that, except otherwise agreed by both parties in
//    writing, Services are provided on an "AS IS" basis and the warranty
//    disclaimer set forth in Section 4 above shall apply.
//
// 6. Nothing contained herein shall be construed as by implication, estoppels
//    or otherwise:
//    (a) conferring any license or right to use MStar name, trademark, service
//        mark, symbol or any other identification;
//    (b) obligating MStar or any of its affiliates to furnish any person,
//        including without limitation, you and your customers, any assistance
//        of any kind whatsoever, or any information; or
//    (c) conferring any license or right under any intellectual property right.
//
// 7. These terms shall be governed by and construed in accordance with the laws
//    of Taiwan, R.O.C., excluding its conflict of law rules.
//    Any and all dispute arising out hereof or related hereto shall be finally
//    settled by arbitration referred to the Chinese Arbitration Association,
//    Taipei in accordance with the ROC Arbitration Law and the Arbitration
//    Rules of the Association by three (3) arbitrators appointed in accordance
//    with the said Rules.
//    The place of arbitration shall be in Taipei, Taiwan and the language shall
//    be English.
//    The arbitration award shall be final and binding to both parties.
//
//******************************************************************************
//<MStar Software>

#ifndef _MIU_MST288A_D01A_S_M7621_H_
#define _MIU_MST288A_D01A_S_M7621_H_

#if (ENABLE_MSTAR_BD_MST288A_D01A_S_M7621 == 1)
#if !defined(CONFIG_MIU0_DRAM_NONE)
const MS_REG_INIT MIU0_DDR_PreInit[] =
{
#if defined(CONFIG_MIU0_4X_MODE)
#if defined(CONFIG_MIU0_DDR4_2666)
    _RV32_2(0x110d36, 0x0400),
    _RV32_2(0x110d34, 0x2003),
    _RV32_2(0x110d30, 0x8000),
    _RV32_2(0x110d32, 0x0027),
    _RV32_2(0x110d8a, 0x0001),
#elif defined(CONFIG_MIU0_DDR3_2133)
    _RV32_2(0x110d36, 0x0400),
    _RV32_2(0x110d34, 0x2003),
    _RV32_2(0x110d30, 0x8000),
    _RV32_2(0x110d32, 0x0027),
    _RV32_2(0x110d8a, 0x0001),
#else
    #error "Invalid DRAM Setting"
#endif

#elif defined(CONFIG_MIU0_8X_MODE)
    #error "Not support DRAM 8X mode "
#else
    #error "Invalid DRAM Setting"
#endif

    _END_OF_TBL32_,
    MIU_VER
};

const MS_REG_INIT MIU0_DDR_Init[] =
{
#if defined(CONFIG_MIU0_4X_MODE)
#if defined(CONFIG_MIU0_DDR4_2666)
    //---------------------------
    //set DDR3_32_4X_CL14_2133
    //---------------------------
    _RV32_2(0x101202, 0x02a7),
    _RV32_2(0x101204, 0x0052),
    _RV32_2(0x101206, 0x0681),
    _RV32_2(0x101208, 0x24ee),
    _RV32_2(0x10120a, 0x3488),
    _RV32_2(0x10120c, 0xe80a),
    _RV32_2(0x10120e, 0xc0ab),
    _RV32_2(0x101210, 0x1124),
    _RV32_2(0x101212, 0x4004),
    _RV32_2(0x101214, 0x8028),
    _RV32_2(0x101216, 0xc000),
    _RV32_2(0x101228, 0x00c0),
    _RV32_2(0x152b22, 0x0003),
    _RV32_2(0x152b40, 0x0e0e),
    _RV32_2(0x152b42, 0x0824),
    _RV32_2(0x152b44, 0x3408),
    _RV32_2(0x152b46, 0x100a),
    _RV32_2(0x152b48, 0x0e08),
    _RV32_2(0x152b4a, 0x0604),
    _RV32_2(0x152b4c, 0x0528),
    _RV32_2(0x152b4e, 0x00ab),
    _RV32_2(0x152b50, 0xe000),
    _RV32_2(0x152b52, 0x0000),
    _RV32_2(0x152b54, 0x0e00),
    _RV32_2(0x152b80, 0x0000),
    _RV32_2(0x152b86, 0x0000),
    _RV32_2(0x152bfe, 0x0001),
    _RV32_2(0x161560, 0x0000),
    _RV32_2(0x161562, 0x0000),
    _RV32_2(0x161564, 0x0000),
    _RV32_2(0x161566, 0x0030),
    _RV32_2(0x161568, 0x5000),
    _RV32_2(0x16156a, 0x0028),
    _RV32_2(0x110d02, 0xaaaa),
    _RV32_2(0x110d04, 0x0008),
    _RV32_2(0x110d0e, 0x0099),
    _RV32_2(0x110d2e, 0x1111),
    _RV32_2(0x110db6, 0x0000),
    _RV32_2(0x110ddc, 0x0003),
    _RV32_2(0x110de0, 0x0011),
    _RV32_2(0x110de2, 0x0011),
    _RV32_2(0x110de4, 0x0010),
    _RV32_2(0x110de6, 0x1111),
    _RV32_2(0x110de8, 0x1111),
    _RV32_2(0x110dea, 0x1111),
    _RV32_2(0x110dec, 0x1111),
    _RV32_2(0x110dee, 0x1111),
    _RV32_2(0x110df0, 0x1111),
    _RV32_2(0x110df2, 0x0011),
    _RV32_2(0x151500, 0x0003),
    _RV32_2(0x151504, 0x0000),
    _RV32_2(0x151508, 0x0000),
    _RV32_2(0x15150a, 0x0000),
    _RV32_2(0x151510, 0x0000),
    _RV32_2(0x151512, 0x0000),
    _RV32_2(0x151514, 0x0707),
    _RV32_2(0x151516, 0x0707),
    _RV32_2(0x15151e, 0x0000),
    _RV32_2(0x151520, 0x0707),
    _RV32_2(0x151522, 0x0707),
    _RV32_2(0x151524, 0x0707),
    _RV32_2(0x151526, 0x0707),
    _RV32_2(0x151528, 0x0707),
    _RV32_2(0x15152a, 0x0707),
    _RV32_2(0x15152c, 0x0707),
    _RV32_2(0x15152e, 0x0707),
    _RV32_2(0x151530, 0x0707),
    _RV32_2(0x151532, 0x0707),
    _RV32_2(0x151534, 0x0707),
    _RV32_2(0x151536, 0x0707),
    _RV32_2(0x151538, 0x0707),
    _RV32_2(0x15153a, 0x0707),
    _RV32_2(0x15153c, 0x0707),
    _RV32_2(0x15153e, 0x0707),
    _RV32_2(0x151560, 0x0606),
    _RV32_2(0x151562, 0x0606),
    _RV32_2(0x151564, 0x0808),
    _RV32_2(0x151566, 0x0808),
    _RV32_2(0x151568, 0x0808),
    _RV32_2(0x15156a, 0x0808),
    _RV32_2(0x151580, 0x0101),
    _RV32_2(0x151582, 0x0101),
    _RV32_2(0x151584, 0x0101),
    _RV32_2(0x151586, 0x0101),
    _RV32_2(0x151588, 0x0101),
    _RV32_2(0x15158a, 0x0101),
    _RV32_2(0x15158c, 0x0101),
    _RV32_2(0x15158e, 0x0101),
    _RV32_2(0x151590, 0x0101),
    _RV32_2(0x151592, 0x0101),
    _RV32_2(0x151594, 0x0101),
    _RV32_2(0x151596, 0x0101),
    _RV32_2(0x151598, 0x0101),
    _RV32_2(0x15159a, 0x0101),
    _RV32_2(0x15159c, 0x0101),
    _RV32_2(0x15159e, 0x0101),
    //=====================================================================
    //pre inivec setting start
    //=====================================================================
    //-----------------
    //program DLL
    //-----------------
    _RV32_2(0x110d62, 0x007f),
    _RV32_2(0x110d64, 0xf000),
    _RV32_2(0x110d60, 0x00cb),
    _RV32_2(0x110d60, 0x00cf),
    _RV32_2(0x110d60, 0x00cb),
    _RV32_2(0x110d60, 0x00c3),
    _RV32_2(0x110d60, 0x00cb),
    _RV32_2(0x110d60, 0x00c3),
    _RV32_2(0x110d60, 0x00cb),
    _RV32_2(0x110d60, 0x00c2),
    _RV32_2(0x110d60, 0x00c0),
    _RV32_2(0x110d60, 0x33c8),
    _RV32_2(0x110d70, 0x0000),
    _RV32_2(0x110d98, 0x0000),
    _RV32_2(0x110d9a, 0x0000),
    _RV32_2(0x110d90, 0xf0f3),
    _RV32_2(0x110d70, 0x0800),
    //---------------------------
    //program dig general setting
    //---------------------------
    _RV32_2(0x152bde, 0x8000),
    _RV32_2(0x10122c, 0x8221),
    _RV32_2(0x1012fc, 0x61a1),
    //--------------------------------------
    //Multi bank function
    //--------------------------------------
    _RV32_2(0x152b8a, 0x0317),
    _RV32_2(0x152b80, 0x80f0),
    //---------------------
    // Default BW setting  
    //---------------------
    _RV32_2(0x310230, 0xc01d),
    _RV32_2(0x310270, 0xc01d),
    _RV32_2(0x3102b0, 0xc01d),
    _RV32_2(0x3102f0, 0xc01d),
    _RV32_2(0x310630, 0xc01d),
    _RV32_2(0x310670, 0x001d),
    _RV32_2(0x3106b0, 0x001d),
    //---------------------------
    //program ana general setting
    //---------------------------
    _RV32_2(0x110d70, 0x0800),
    //Driving setting
    //DRVN
    _RV32_2(0x1515a0, 0x1e1e),
    _RV32_2(0x1515a2, 0x1e1e),
    _RV32_2(0x1515a4, 0x1e1e),
    _RV32_2(0x1515a6, 0x1e1e),
    _RV32_2(0x1515a8, 0x1e1e),
    //DRVP
    _RV32_2(0x1515aa, 0x1e1e),
    _RV32_2(0x1515ac, 0x1e1e),
    _RV32_2(0x1515ae, 0x1e1e),
    _RV32_2(0x1515b0, 0x1e1e),
    _RV32_2(0x1515b2, 0x1e1e),
    //trig. lvl setting
    _RV32_2(0x110d6c, 0x0707),
    _RV32_2(0x110d6e, 0x0707),
    _RV32_2(0x110d74, 0x0707),
    _RV32_2(0x110d76, 0x0707),
    _RV32_2(0x110d1c, 0x0020),
    //---------------------------
    //ODT DIGCTRL setting
    //---------------------------
    _RV32_2(0x110d1a, 0x8022),
    _RV32_2(0x110d08, 0x003f),
    //=====================================================================
    // post inivec setting start
    //=====================================================================
    //---------------------------
    //disable GPIO
    //---------------------------
    _RV32_2(0x110d00, 0x2010),
    _RV32_2(0x110d00, 0x0000),
    _RV32_2(0x110d18, 0x0000),
    _RV32_2(0x110d7c, 0x0000),
    //---------------------------
    //Release ATOP PD mode       
    //---------------------------
    _RV32_2(0x110d54, 0xc000),
    //-----------
    // DQSM RST  
    //-----------
    _RV32_2(0x110d1e, 0x0005),
    _RV32_2(0x110d1e, 0x000f),
    _RV32_2(0x110d1e, 0x0005),
    //---------------------------
    //select Mapping
    //---------------------------
    _RV32_2(0x110d00, 0x0001),
    _RV32_2(0x101200, 0x0000),
#elif defined(CONFIG_MIU0_DDR3_2133)
    //---------------------------
    //set DDR3_32_4X_CL14_2133
    //---------------------------
    _RV32_2(0x101202, 0x02a7),
    _RV32_2(0x101204, 0x0052),
    _RV32_2(0x101206, 0x0681),
    _RV32_2(0x101208, 0x24ee),
    _RV32_2(0x10120a, 0x3488),
    _RV32_2(0x10120c, 0xe80a),
    _RV32_2(0x10120e, 0xc0ab),
    _RV32_2(0x101210, 0x1124),
    _RV32_2(0x101212, 0x4004),
    _RV32_2(0x101214, 0x8028),
    _RV32_2(0x101216, 0xc000),
    _RV32_2(0x101228, 0x00c0),
    _RV32_2(0x152b22, 0x0003),
    _RV32_2(0x152b40, 0x0e0e),
    _RV32_2(0x152b42, 0x0824),
    _RV32_2(0x152b44, 0x3408),
    _RV32_2(0x152b46, 0x100a),
    _RV32_2(0x152b48, 0x0e08),
    _RV32_2(0x152b4a, 0x0604),
    _RV32_2(0x152b4c, 0x0528),
    _RV32_2(0x152b4e, 0x00ab),
    _RV32_2(0x152b50, 0xe000),
    _RV32_2(0x152b52, 0x0000),
    _RV32_2(0x152b54, 0x0e00),
    _RV32_2(0x152b80, 0x0000),
    _RV32_2(0x152b86, 0x0000),
    _RV32_2(0x152bfe, 0x0001),
    _RV32_2(0x161560, 0x0000),
    _RV32_2(0x161562, 0x0000),
    _RV32_2(0x161564, 0x0000),
    _RV32_2(0x161566, 0x0030),
    _RV32_2(0x161568, 0x5000),
    _RV32_2(0x16156a, 0x0028),
    _RV32_2(0x110d02, 0xaaaa),
    _RV32_2(0x110d04, 0x0008),
    _RV32_2(0x110d0e, 0x0099),
    _RV32_2(0x110d2e, 0x1111),
    _RV32_2(0x110db6, 0x0000),
    _RV32_2(0x110ddc, 0x0003),
    _RV32_2(0x110de0, 0x0011),
    _RV32_2(0x110de2, 0x0011),
    _RV32_2(0x110de4, 0x0010),
    _RV32_2(0x110de6, 0x1111),
    _RV32_2(0x110de8, 0x1111),
    _RV32_2(0x110dea, 0x1111),
    _RV32_2(0x110dec, 0x1111),
    _RV32_2(0x110dee, 0x1111),
    _RV32_2(0x110df0, 0x1111),
    _RV32_2(0x110df2, 0x0011),
    _RV32_2(0x151500, 0x0003),
    _RV32_2(0x151504, 0x0000),
    _RV32_2(0x151508, 0x0000),
    _RV32_2(0x15150a, 0x0000),
    _RV32_2(0x151510, 0x0000),
    _RV32_2(0x151512, 0x0000),
    _RV32_2(0x151514, 0x0707),
    _RV32_2(0x151516, 0x0707),
    _RV32_2(0x15151e, 0x0000),
    _RV32_2(0x151520, 0x0707),
    _RV32_2(0x151522, 0x0707),
    _RV32_2(0x151524, 0x0707),
    _RV32_2(0x151526, 0x0707),
    _RV32_2(0x151528, 0x0707),
    _RV32_2(0x15152a, 0x0707),
    _RV32_2(0x15152c, 0x0707),
    _RV32_2(0x15152e, 0x0707),
    _RV32_2(0x151530, 0x0707),
    _RV32_2(0x151532, 0x0707),
    _RV32_2(0x151534, 0x0707),
    _RV32_2(0x151536, 0x0707),
    _RV32_2(0x151538, 0x0707),
    _RV32_2(0x15153a, 0x0707),
    _RV32_2(0x15153c, 0x0707),
    _RV32_2(0x15153e, 0x0707),
    _RV32_2(0x151560, 0x0606),
    _RV32_2(0x151562, 0x0606),
    _RV32_2(0x151564, 0x0808),
    _RV32_2(0x151566, 0x0808),
    _RV32_2(0x151568, 0x0808),
    _RV32_2(0x15156a, 0x0808),
    _RV32_2(0x151580, 0x0101),
    _RV32_2(0x151582, 0x0101),
    _RV32_2(0x151584, 0x0101),
    _RV32_2(0x151586, 0x0101),
    _RV32_2(0x151588, 0x0101),
    _RV32_2(0x15158a, 0x0101),
    _RV32_2(0x15158c, 0x0101),
    _RV32_2(0x15158e, 0x0101),
    _RV32_2(0x151590, 0x0101),
    _RV32_2(0x151592, 0x0101),
    _RV32_2(0x151594, 0x0101),
    _RV32_2(0x151596, 0x0101),
    _RV32_2(0x151598, 0x0101),
    _RV32_2(0x15159a, 0x0101),
    _RV32_2(0x15159c, 0x0101),
    _RV32_2(0x15159e, 0x0101),
    //=====================================================================
    //pre inivec setting start
    //=====================================================================
    //-----------------
    //program DLL
    //-----------------
    _RV32_2(0x110d62, 0x007f),
    _RV32_2(0x110d64, 0xf000),
    _RV32_2(0x110d60, 0x00cb),
    _RV32_2(0x110d60, 0x00cf),
    _RV32_2(0x110d60, 0x00cb),
    _RV32_2(0x110d60, 0x00c3),
    _RV32_2(0x110d60, 0x00cb),
    _RV32_2(0x110d60, 0x00c3),
    _RV32_2(0x110d60, 0x00cb),
    _RV32_2(0x110d60, 0x00c2),
    _RV32_2(0x110d60, 0x00c0),
    _RV32_2(0x110d60, 0x33c8),
    _RV32_2(0x110d70, 0x0000),
    _RV32_2(0x110d98, 0x0000),
    _RV32_2(0x110d9a, 0x0000),
    _RV32_2(0x110d90, 0xf0f3),
    _RV32_2(0x110d70, 0x0800),
    //---------------------------
    //program dig general setting
    //---------------------------
    _RV32_2(0x152bde, 0x8000),
    _RV32_2(0x10122c, 0x8221),
    _RV32_2(0x1012fc, 0x61a1),
    //--------------------------------------
    //Multi bank function
    //--------------------------------------
    _RV32_2(0x152b8a, 0x0317),
    _RV32_2(0x152b80, 0x80f0),
    //---------------------
    // Default BW setting  
    //---------------------
    _RV32_2(0x310230, 0xc01d),
    _RV32_2(0x310270, 0xc01d),
    _RV32_2(0x3102b0, 0xc01d),
    _RV32_2(0x3102f0, 0xc01d),
    _RV32_2(0x310630, 0xc01d),
    _RV32_2(0x310670, 0x001d),
    _RV32_2(0x3106b0, 0x001d),
    //---------------------------
    //program ana general setting
    //---------------------------
    _RV32_2(0x110d70, 0x0800),
    //Driving setting
    //DRVN
    _RV32_2(0x1515a0, 0x1e1e),
    _RV32_2(0x1515a2, 0x1e1e),
    _RV32_2(0x1515a4, 0x1e1e),
    _RV32_2(0x1515a6, 0x1e1e),
    _RV32_2(0x1515a8, 0x1e1e),
    //DRVP
    _RV32_2(0x1515aa, 0x1e1e),
    _RV32_2(0x1515ac, 0x1e1e),
    _RV32_2(0x1515ae, 0x1e1e),
    _RV32_2(0x1515b0, 0x1e1e),
    _RV32_2(0x1515b2, 0x1e1e),
    //trig. lvl setting
    _RV32_2(0x110d6c, 0x0707),
    _RV32_2(0x110d6e, 0x0707),
    _RV32_2(0x110d74, 0x0707),
    _RV32_2(0x110d76, 0x0707),
    _RV32_2(0x110d1c, 0x0020),
    //---------------------------
    //ODT DIGCTRL setting
    //---------------------------
    _RV32_2(0x110d1a, 0x8022),
    _RV32_2(0x110d08, 0x003f),
    //=====================================================================
    // post inivec setting start
    //=====================================================================
    //---------------------------
    //disable GPIO
    //---------------------------
    _RV32_2(0x110d00, 0x2010),
    _RV32_2(0x110d00, 0x0000),
    _RV32_2(0x110d18, 0x0000),
    _RV32_2(0x110d7c, 0x0000),
    //---------------------------
    //Release ATOP PD mode       
    //---------------------------
    _RV32_2(0x110d54, 0xc000),
    //-----------
    // DQSM RST  
    //-----------
    _RV32_2(0x110d1e, 0x0005),
    _RV32_2(0x110d1e, 0x000f),
    _RV32_2(0x110d1e, 0x0005),
    //---------------------------
    //select Mapping
    //---------------------------
    _RV32_2(0x110d00, 0x0001),
    _RV32_2(0x101200, 0x0000),
#else
    #error "Invalid DRAM Setting"
#endif

#elif defined(CONFIG_MIU0_8X_MODE)
    #error "Not support DRAM 8X mode "
#else
    #error "Invalid DRAM Setting"
#endif

    _END_OF_TBL32_,
    MIU_VER
};

#if defined(CONFIG_ENABLE_MIU_SSC)
const MS_REG_INIT MIU0_EnableSSC[] =
{
#if defined(CONFIG_MIU0_4X_MODE)
    #error "Not support DRAM 4X mode "
#elif defined(CONFIG_MIU0_8X_MODE)
    #error "Not support DRAM 8X mode "
#else
    #error "Invalid DRAM Setting"
#endif

    _END_OF_TBL32_,
    MIU_VER
};
#endif

#endif

#if !defined(CONFIG_MIU1_DRAM_NONE)
const MS_REG_INIT MIU1_DDR_PreInit[] =
{
#if defined(CONFIG_MIU1_4X_MODE)
#if defined(CONFIG_MIU1_DDR4_2666)
    _RV32_2(0x161636, 0x0400),
    _RV32_2(0x161634, 0x2003),
    _RV32_2(0x161630, 0x8000),
    _RV32_2(0x161632, 0x0027),
    _RV32_2(0x16168a, 0x0001),
#elif defined(CONFIG_MIU1_DDR3_2133)
    _RV32_2(0x161636, 0x0400),
    _RV32_2(0x161634, 0x2003),
    _RV32_2(0x161630, 0x8000),
    _RV32_2(0x161632, 0x0027),
    _RV32_2(0x16168a, 0x0001),
#else
    #error "Invalid DRAM Setting"
#endif

#elif defined(CONFIG_MIU1_8X_MODE)
    #error "Not support DRAM 8X mode "
#else
    #error "Invalid DRAM Setting"
#endif

    _END_OF_TBL32_,
    MIU_VER
};

const MS_REG_INIT MIU1_DDR_Init[] =
{
#if defined(CONFIG_MIU1_4X_MODE)
#if defined(CONFIG_MIU1_DDR4_2666)
    //---------------------------
    //set DDR3_32_4X_CL14_2133
    //---------------------------
    _RV32_2(0x100602, 0x02a7),
    _RV32_2(0x100604, 0x0052),
    _RV32_2(0x100606, 0x0681),
    _RV32_2(0x100608, 0x24ee),
    _RV32_2(0x10060a, 0x3488),
    _RV32_2(0x10060c, 0xe80a),
    _RV32_2(0x10060e, 0xc0ab),
    _RV32_2(0x100610, 0x1124),
    _RV32_2(0x100612, 0x4004),
    _RV32_2(0x100614, 0x8028),
    _RV32_2(0x100616, 0xc000),
    _RV32_2(0x100628, 0x00c0),
    _RV32_2(0x152c22, 0x0003),
    _RV32_2(0x152c40, 0x0e0e),
    _RV32_2(0x152c42, 0x0824),
    _RV32_2(0x152c44, 0x3408),
    _RV32_2(0x152c46, 0x100a),
    _RV32_2(0x152c48, 0x0e08),
    _RV32_2(0x152c4a, 0x0604),
    _RV32_2(0x152c4c, 0x0528),
    _RV32_2(0x152c4e, 0x00ab),
    _RV32_2(0x152c50, 0xe000),
    _RV32_2(0x152c52, 0x0000),
    _RV32_2(0x152c54, 0x0e00),
    _RV32_2(0x152c80, 0x0000),
    _RV32_2(0x152c86, 0x0000),
    _RV32_2(0x152cfe, 0x0001),
    _RV32_2(0x162260, 0x0000),
    _RV32_2(0x162262, 0x0000),
    _RV32_2(0x162264, 0x0000),
    _RV32_2(0x162266, 0x0030),
    _RV32_2(0x162268, 0x5000),
    _RV32_2(0x16226a, 0x0028),
    _RV32_2(0x161602, 0xaaaa),
    _RV32_2(0x161604, 0x0008),
    _RV32_2(0x16160e, 0x0099),
    _RV32_2(0x16162e, 0x1111),
    _RV32_2(0x1616b6, 0x0000),
    _RV32_2(0x1616dc, 0x0003),
    _RV32_2(0x1616e0, 0x0011),
    _RV32_2(0x1616e2, 0x0011),
    _RV32_2(0x1616e4, 0x0010),
    _RV32_2(0x1616e6, 0x1111),
    _RV32_2(0x1616e8, 0x1111),
    _RV32_2(0x1616ea, 0x1111),
    _RV32_2(0x1616ec, 0x1111),
    _RV32_2(0x1616ee, 0x1111),
    _RV32_2(0x1616f0, 0x1111),
    _RV32_2(0x1616f2, 0x0011),
    _RV32_2(0x151600, 0x0003),
    _RV32_2(0x151604, 0x0000),
    _RV32_2(0x151608, 0x0000),
    _RV32_2(0x15160a, 0x0000),
    _RV32_2(0x151610, 0x0000),
    _RV32_2(0x151612, 0x0000),
    _RV32_2(0x151614, 0x0707),
    _RV32_2(0x151616, 0x0707),
    _RV32_2(0x15161e, 0x0000),
    _RV32_2(0x151620, 0x0707),
    _RV32_2(0x151622, 0x0707),
    _RV32_2(0x151624, 0x0707),
    _RV32_2(0x151626, 0x0707),
    _RV32_2(0x151628, 0x0707),
    _RV32_2(0x15162a, 0x0707),
    _RV32_2(0x15162c, 0x0707),
    _RV32_2(0x15162e, 0x0707),
    _RV32_2(0x151630, 0x0707),
    _RV32_2(0x151632, 0x0707),
    _RV32_2(0x151634, 0x0707),
    _RV32_2(0x151636, 0x0707),
    _RV32_2(0x151638, 0x0707),
    _RV32_2(0x15163a, 0x0707),
    _RV32_2(0x15163c, 0x0707),
    _RV32_2(0x15163e, 0x0707),
    _RV32_2(0x151660, 0x0606),
    _RV32_2(0x151662, 0x0606),
    _RV32_2(0x151664, 0x0808),
    _RV32_2(0x151666, 0x0808),
    _RV32_2(0x151668, 0x0808),
    _RV32_2(0x15166a, 0x0808),
    _RV32_2(0x151680, 0x0101),
    _RV32_2(0x151682, 0x0101),
    _RV32_2(0x151684, 0x0101),
    _RV32_2(0x151686, 0x0101),
    _RV32_2(0x151688, 0x0101),
    _RV32_2(0x15168a, 0x0101),
    _RV32_2(0x15168c, 0x0101),
    _RV32_2(0x15168e, 0x0101),
    _RV32_2(0x151690, 0x0101),
    _RV32_2(0x151692, 0x0101),
    _RV32_2(0x151694, 0x0101),
    _RV32_2(0x151696, 0x0101),
    _RV32_2(0x151698, 0x0101),
    _RV32_2(0x15169a, 0x0101),
    _RV32_2(0x15169c, 0x0101),
    _RV32_2(0x15169e, 0x0101),
    //=====================================================================
    //pre inivec setting start
    //=====================================================================
    //-----------------
    //program DLL
    //-----------------
    _RV32_2(0x161662, 0x007f),
    _RV32_2(0x161664, 0xf000),
    _RV32_2(0x161660, 0x00cb),
    _RV32_2(0x161660, 0x00cf),
    _RV32_2(0x161660, 0x00cb),
    _RV32_2(0x161660, 0x00c3),
    _RV32_2(0x161660, 0x00cb),
    _RV32_2(0x161660, 0x00c3),
    _RV32_2(0x161660, 0x00cb),
    _RV32_2(0x161660, 0x00c2),
    _RV32_2(0x161660, 0x00c0),
    _RV32_2(0x161660, 0x33c8),
    _RV32_2(0x161670, 0x0000),
    _RV32_2(0x161698, 0x0000),
    _RV32_2(0x16169a, 0x0000),
    _RV32_2(0x161690, 0xf0f3),
    _RV32_2(0x161670, 0x0800),
    //---------------------------
    //program dig general setting
    //---------------------------
    _RV32_2(0x152cde, 0x8000),
    _RV32_2(0x10062c, 0x8221),
    _RV32_2(0x1006fc, 0x61a1),
    //--------------------------------------
    //Multi bank function
    //--------------------------------------
    _RV32_2(0x152c8a, 0x0317),
    _RV32_2(0x152c80, 0x80f0),
    //---------------------
    // Default BW setting  
    //---------------------
    _RV32_2(0x310330, 0xc01d),
    _RV32_2(0x310370, 0xc01d),
    _RV32_2(0x3103b0, 0xc01d),
    _RV32_2(0x3103f0, 0xc01d),
    _RV32_2(0x310730, 0xc01d),
    _RV32_2(0x310770, 0x001d),
    _RV32_2(0x3107b0, 0x001d),
    //---------------------------
    //program ana general setting
    //---------------------------
    _RV32_2(0x161670, 0x0800),
    //Driving setting
    //DRVN
    _RV32_2(0x1516a0, 0x1e1e),
    _RV32_2(0x1516a2, 0x1e1e),
    _RV32_2(0x1516a4, 0x1e1e),
    _RV32_2(0x1516a6, 0x1e1e),
    _RV32_2(0x1516a8, 0x1e1e),
    //DRVP
    _RV32_2(0x1516aa, 0x1e1e),
    _RV32_2(0x1516ac, 0x1e1e),
    _RV32_2(0x1516ae, 0x1e1e),
    _RV32_2(0x1516b0, 0x1e1e),
    _RV32_2(0x1516b2, 0x1e1e),
    //trig. lvl setting
    _RV32_2(0x16166c, 0x0707),
    _RV32_2(0x16166e, 0x0707),
    _RV32_2(0x161674, 0x0707),
    _RV32_2(0x161676, 0x0707),
    _RV32_2(0x16161c, 0x0020),
    _RV32_2(0x16161a, 0x8022),
    _RV32_2(0x161608, 0x003f),
    //=====================================================================
    // post inivec setting start
    //=====================================================================
    //---------------------------
    //disable GPIO
    //---------------------------
    _RV32_2(0x161600, 0x2010),
    _RV32_2(0x161600, 0x0000),
    _RV32_2(0x161618, 0x0000),
    _RV32_2(0x16167c, 0x0000),
    //---------------------------
    //Release ATOP PD mode       
    //---------------------------
    _RV32_2(0x161654, 0xc000),
    //-----------
    // DQSM RST  
    //-----------
    _RV32_2(0x16161e, 0x0005),
    _RV32_2(0x16161e, 0x000f),
    _RV32_2(0x16161e, 0x0005),
    //---------------------------
    //select Mapping
    //---------------------------
    _RV32_2(0x161600, 0x0004),
    _RV32_2(0x100600, 0x0000),
#elif defined(CONFIG_MIU1_DDR3_2133)
    //---------------------------
    //set DDR3_32_4X_CL14_2133
    //---------------------------
    _RV32_2(0x100602, 0x02a7),
    _RV32_2(0x100604, 0x0052),
    _RV32_2(0x100606, 0x0681),
    _RV32_2(0x100608, 0x24ee),
    _RV32_2(0x10060a, 0x3488),
    _RV32_2(0x10060c, 0xe80a),
    _RV32_2(0x10060e, 0xc0ab),
    _RV32_2(0x100610, 0x1124),
    _RV32_2(0x100612, 0x4004),
    _RV32_2(0x100614, 0x8028),
    _RV32_2(0x100616, 0xc000),
    _RV32_2(0x100628, 0x00c0),
    _RV32_2(0x152c22, 0x0003),
    _RV32_2(0x152c40, 0x0e0e),
    _RV32_2(0x152c42, 0x0824),
    _RV32_2(0x152c44, 0x3408),
    _RV32_2(0x152c46, 0x100a),
    _RV32_2(0x152c48, 0x0e08),
    _RV32_2(0x152c4a, 0x0604),
    _RV32_2(0x152c4c, 0x0528),
    _RV32_2(0x152c4e, 0x00ab),
    _RV32_2(0x152c50, 0xe000),
    _RV32_2(0x152c52, 0x0000),
    _RV32_2(0x152c54, 0x0e00),
    _RV32_2(0x152c80, 0x0000),
    _RV32_2(0x152c86, 0x0000),
    _RV32_2(0x152cfe, 0x0001),
    _RV32_2(0x162260, 0x0000),
    _RV32_2(0x162262, 0x0000),
    _RV32_2(0x162264, 0x0000),
    _RV32_2(0x162266, 0x0030),
    _RV32_2(0x162268, 0x5000),
    _RV32_2(0x16226a, 0x0028),
    _RV32_2(0x161602, 0xaaaa),
    _RV32_2(0x161604, 0x0008),
    _RV32_2(0x16160e, 0x0099),
    _RV32_2(0x16162e, 0x1111),
    _RV32_2(0x1616b6, 0x0000),
    _RV32_2(0x1616dc, 0x0003),
    _RV32_2(0x1616e0, 0x0011),
    _RV32_2(0x1616e2, 0x0011),
    _RV32_2(0x1616e4, 0x0010),
    _RV32_2(0x1616e6, 0x1111),
    _RV32_2(0x1616e8, 0x1111),
    _RV32_2(0x1616ea, 0x1111),
    _RV32_2(0x1616ec, 0x1111),
    _RV32_2(0x1616ee, 0x1111),
    _RV32_2(0x1616f0, 0x1111),
    _RV32_2(0x1616f2, 0x0011),
    _RV32_2(0x151600, 0x0003),
    _RV32_2(0x151604, 0x0000),
    _RV32_2(0x151608, 0x0000),
    _RV32_2(0x15160a, 0x0000),
    _RV32_2(0x151610, 0x0000),
    _RV32_2(0x151612, 0x0000),
    _RV32_2(0x151614, 0x0707),
    _RV32_2(0x151616, 0x0707),
    _RV32_2(0x15161e, 0x0000),
    _RV32_2(0x151620, 0x0707),
    _RV32_2(0x151622, 0x0707),
    _RV32_2(0x151624, 0x0707),
    _RV32_2(0x151626, 0x0707),
    _RV32_2(0x151628, 0x0707),
    _RV32_2(0x15162a, 0x0707),
    _RV32_2(0x15162c, 0x0707),
    _RV32_2(0x15162e, 0x0707),
    _RV32_2(0x151630, 0x0707),
    _RV32_2(0x151632, 0x0707),
    _RV32_2(0x151634, 0x0707),
    _RV32_2(0x151636, 0x0707),
    _RV32_2(0x151638, 0x0707),
    _RV32_2(0x15163a, 0x0707),
    _RV32_2(0x15163c, 0x0707),
    _RV32_2(0x15163e, 0x0707),
    _RV32_2(0x151660, 0x0606),
    _RV32_2(0x151662, 0x0606),
    _RV32_2(0x151664, 0x0808),
    _RV32_2(0x151666, 0x0808),
    _RV32_2(0x151668, 0x0808),
    _RV32_2(0x15166a, 0x0808),
    _RV32_2(0x151680, 0x0101),
    _RV32_2(0x151682, 0x0101),
    _RV32_2(0x151684, 0x0101),
    _RV32_2(0x151686, 0x0101),
    _RV32_2(0x151688, 0x0101),
    _RV32_2(0x15168a, 0x0101),
    _RV32_2(0x15168c, 0x0101),
    _RV32_2(0x15168e, 0x0101),
    _RV32_2(0x151690, 0x0101),
    _RV32_2(0x151692, 0x0101),
    _RV32_2(0x151694, 0x0101),
    _RV32_2(0x151696, 0x0101),
    _RV32_2(0x151698, 0x0101),
    _RV32_2(0x15169a, 0x0101),
    _RV32_2(0x15169c, 0x0101),
    _RV32_2(0x15169e, 0x0101),
    //=====================================================================
    //pre inivec setting start
    //=====================================================================
    //-----------------
    //program DLL
    //-----------------
    _RV32_2(0x161662, 0x007f),
    _RV32_2(0x161664, 0xf000),
    _RV32_2(0x161660, 0x00cb),
    _RV32_2(0x161660, 0x00cf),
    _RV32_2(0x161660, 0x00cb),
    _RV32_2(0x161660, 0x00c3),
    _RV32_2(0x161660, 0x00cb),
    _RV32_2(0x161660, 0x00c3),
    _RV32_2(0x161660, 0x00cb),
    _RV32_2(0x161660, 0x00c2),
    _RV32_2(0x161660, 0x00c0),
    _RV32_2(0x161660, 0x33c8),
    _RV32_2(0x161670, 0x0000),
    _RV32_2(0x161698, 0x0000),
    _RV32_2(0x16169a, 0x0000),
    _RV32_2(0x161690, 0xf0f3),
    _RV32_2(0x161670, 0x0800),
    //---------------------------
    //program dig general setting
    //---------------------------
    _RV32_2(0x152cde, 0x8000),
    _RV32_2(0x10062c, 0x8221),
    _RV32_2(0x1006fc, 0x61a1),
    //--------------------------------------
    //Multi bank function
    //--------------------------------------
    _RV32_2(0x152c8a, 0x0317),
    _RV32_2(0x152c80, 0x80f0),
    //---------------------
    // Default BW setting  
    //---------------------
    _RV32_2(0x310330, 0xc01d),
    _RV32_2(0x310370, 0xc01d),
    _RV32_2(0x3103b0, 0xc01d),
    _RV32_2(0x3103f0, 0xc01d),
    _RV32_2(0x310730, 0xc01d),
    _RV32_2(0x310770, 0x001d),
    _RV32_2(0x3107b0, 0x001d),
    //---------------------------
    //program ana general setting
    //---------------------------
    _RV32_2(0x161670, 0x0800),
    //Driving setting
    //DRVN
    _RV32_2(0x1516a0, 0x1e1e),
    _RV32_2(0x1516a2, 0x1e1e),
    _RV32_2(0x1516a4, 0x1e1e),
    _RV32_2(0x1516a6, 0x1e1e),
    _RV32_2(0x1516a8, 0x1e1e),
    //DRVP
    _RV32_2(0x1516aa, 0x1e1e),
    _RV32_2(0x1516ac, 0x1e1e),
    _RV32_2(0x1516ae, 0x1e1e),
    _RV32_2(0x1516b0, 0x1e1e),
    _RV32_2(0x1516b2, 0x1e1e),
    //trig. lvl setting
    _RV32_2(0x16166c, 0x0707),
    _RV32_2(0x16166e, 0x0707),
    _RV32_2(0x161674, 0x0707),
    _RV32_2(0x161676, 0x0707),
    _RV32_2(0x16161c, 0x0020),
    _RV32_2(0x16161a, 0x8022),
    _RV32_2(0x161608, 0x003f),
    //=====================================================================
    // post inivec setting start
    //=====================================================================
    //---------------------------
    //disable GPIO
    //---------------------------
    _RV32_2(0x161600, 0x2010),
    _RV32_2(0x161600, 0x0000),
    _RV32_2(0x161618, 0x0000),
    _RV32_2(0x16167c, 0x0000),
    //---------------------------
    //Release ATOP PD mode       
    //---------------------------
    _RV32_2(0x161654, 0xc000),
    //-----------
    // DQSM RST  
    //-----------
    _RV32_2(0x16161e, 0x0005),
    _RV32_2(0x16161e, 0x000f),
    _RV32_2(0x16161e, 0x0005),
    //---------------------------
    //select Mapping
    //---------------------------
    _RV32_2(0x161600, 0x0004),
    _RV32_2(0x100600, 0x0000),
#else
    #error "Invalid DRAM Setting"
#endif

#elif defined(CONFIG_MIU1_8X_MODE)
    #error "Not support DRAM 8X mode "
#else
    #error "Invalid DRAM Setting"
#endif

    _END_OF_TBL32_,
    MIU_VER
};

#if defined(CONFIG_ENABLE_MIU_SSC)
const MS_REG_INIT MIU1_EnableSSC[] =
{
#if defined(CONFIG_MIU1_4X_MODE)
    #error "Not support DRAM 4X mode "
#elif defined(CONFIG_MIU1_8X_MODE)
    #error "Not support DRAM 8X mode "
#else
    #error "Invalid DRAM Setting"
#endif

    _END_OF_TBL32_,
    MIU_VER
};
#endif

#endif

#endif

#endif
