////////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2006-2009 MStar Semiconductor, Inc.
// All rights reserved.
//
// Unless otherwise stipulated in writing, any and all information contained
// herein regardless in any format shall remain the sole proprietary of
// MStar Semiconductor Inc. and be kept in strict confidence
// ("MStar Confidential Information") by the recipient.
// Any unauthorized act including without limitation unauthorized disclosure,
// copying, use, reproduction, sale, distribution, modification, disassembling,
// reverse engineering and compiling of the contents of MStar Confidential
// Information is unlawful and strictly prohibited. MStar hereby reserves the
// rights to any and all damages, losses, costs and expenses resulting therefrom.
//
////////////////////////////////////////////////////////////////////////////////

//---------------------------------------------------------------------------

//---------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------------
//  Define
//-------------------------------------------------------------------------------------------------
#define MIU_RIU_REG_BASE                   0x1F000000

#define MIU0_RIU_DTOP                      0x1012
#define MIU1_RIU_DTOP                      0x1006
#define MIU2_RIU_DTOP                      0x1620
#define MIU0_RIU_ARB                       0x1615
#define MIU1_RIU_ARB                       0x1622
#define MIU2_RIU_ARB                       0x1623
#define MIU0_RIU_ARBB                      0x1520
#define MIU1_RIU_ARBB                      0x1521
#define MIU2_RIU_ARBB                      0x1522
#define MIU0_RIU_ATOP                      0x110d
#define MIU1_RIU_ATOP                      0x1616
#define MIU2_RIU_ATOP                      0x1621
#define MIU0_RIU_DTOP_E                    0x152b
#define MIU1_RIU_DTOP_E                    0x152c
#define MIU2_RIU_DTOP_E                    0x152d
#define MIU0_RIU_ARB_F                     0x3106
#define MIU1_RIU_ARB_F                     0x3107
#define MIU2_RIU_ARB_F                     0x3108
#define MIU0_RIU_ARB_E                     0x3102
#define MIU1_RIU_ARB_E                     0x3103
#define MIU2_RIU_ARB_E                     0x3104
#define MIU0_RIU_ATOP_E                    0x1515
#define MIU1_RIU_ATOP_E                    0x1516
#define MIU2_RIU_ATOP_E                    0x1517

#define EFUSE_RIU_BANK                     0x0020
#define REG_ATOP_REGION                    0x2F
#define DEBUG
//#define VREF_DQ_EN
//#define MIU_ENABLE_AUTO_WRITE_PHASE
#define MIU_ENABLE_AUTO_READ_PHASE

#define MIU_CHIP_VER_U01                    0
#define MIU_CHIP_VER_U02                    1

typedef unsigned char   u8;
typedef unsigned int   u16;
typedef unsigned long  u32;

#define MHal_MIU_ReadReg16(u32bank, u32reg ) *( ( volatile u16* ) (MIU_RIU_REG_BASE + (u32bank)*0x100*2 + ((u32reg) << 1)) )
#define MHal_MIU_WritReg16(u32bank, u32reg, u16val ) *( ( volatile u16* ) (MIU_RIU_REG_BASE + (u32bank)*0x100*2 + ((u32reg) << 1))  ) = (u16val)

#define MHal_EFUSE_ReadReg16(u32reg) *(( volatile u16* ) (MIU_RIU_REG_BASE + EFUSE_RIU_BANK*0x100*2 + ((u32reg) << 1)))
#define MHal_EFUSE_WritReg16(u32reg, u16val) *(( volatile u16* ) (MIU_RIU_REG_BASE + EFUSE_RIU_BANK*0x100*2 + ((u32reg) << 1))) = (u16val)



//-------------------------------------------------------------------------------------------------
//  Prototypes
//-------------------------------------------------------------------------------------------------
static void putn_ddr4( u8 n );
static void putk_ddr4( char c );
static void delayus_ddr4(u32 us);
//-------------------------------------------------------------------------------------------------
//  Local variables
//-------------------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------------
//  Functions
//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------
void putn_ddr4(u8 n)
{
    char c = '0' + n;

    *(volatile unsigned int*)(0x1F201300) = c;
}
//-------------------------------------------------------------------------------------------------
void putk_ddr4(char c)
{
   *(volatile unsigned int*)(0x1F201300) = c;
}

//-------------------------------------------------------------------------------------------------
void delayus_ddr4(u32 us)
{
    u16 u16RegVal0;

    u16RegVal0 = ((us* 12) & 0xffff);
    MHal_MIU_WritReg16(0x30, 0x24, u16RegVal0);

    u16RegVal0 = ((us* 12) >> 16);
    MHal_MIU_WritReg16(0x30, 0x26, u16RegVal0);

    u16RegVal0 = 0x0002;
    MHal_MIU_WritReg16(0x30, 0x20, u16RegVal0);

    do{
        u16RegVal0 = MHal_MIU_ReadReg16(0x30, 0x22);
    }while((u16RegVal0 & 0x0001) == 0);
}

//-------------------------------------------------------------------------------------------------
u8 BootRom_MiuPatch(void)
{
    u16 i = 0;
    u16 j = 0;
    u16 k = 0;
    u16 ll = 5;
    u16 miu0_Amin = 0, miu0_Amax = 0, miu1_Amin = 0, miu1_Amax = 0;
    u16 miu0_Bmin = 0, miu0_Bmax = 0, miu1_Bmin = 0, miu1_Bmax = 0;
    u16 u16RegVal0;
    u16 u16ChipVersion = 0;

    u16ChipVersion = MHal_MIU_ReadReg16(0x1e, 0x02);
    u16ChipVersion >>= 8;

    MHal_MIU_WritReg16(MIU0_RIU_DTOP, 0x1e, 0x0c00);
    MHal_MIU_WritReg16(MIU1_RIU_DTOP, 0x1e, 0x0c00);

    MHal_MIU_WritReg16(MIU0_RIU_ATOP, 0x62, 0x007f);
    MHal_MIU_WritReg16(MIU0_RIU_ATOP, 0x64, 0xf000);
    MHal_MIU_WritReg16(MIU0_RIU_ATOP, 0x60, 0x00cb);
    MHal_MIU_WritReg16(MIU0_RIU_ATOP, 0x60, 0x00cf);
    MHal_MIU_WritReg16(MIU0_RIU_ATOP, 0x60, 0x00cb);
    MHal_MIU_WritReg16(MIU0_RIU_ATOP, 0x60, 0x00c3);
    MHal_MIU_WritReg16(MIU0_RIU_ATOP, 0x60, 0x00cb);
    MHal_MIU_WritReg16(MIU0_RIU_ATOP, 0x60, 0x00c3);
    MHal_MIU_WritReg16(MIU0_RIU_ATOP, 0x60, 0x00cb);
    MHal_MIU_WritReg16(MIU0_RIU_ATOP, 0x60, 0x00c2);
    MHal_MIU_WritReg16(MIU0_RIU_ATOP, 0x60, 0x00c0);
    MHal_MIU_WritReg16(MIU0_RIU_ATOP, 0x60, 0x33c8);
    MHal_MIU_WritReg16(MIU0_RIU_ATOP, 0x70, 0x0000);
    MHal_MIU_WritReg16(MIU0_RIU_ATOP, 0x98, 0x0000);
    MHal_MIU_WritReg16(MIU0_RIU_ATOP, 0x9a, 0x0000);
    MHal_MIU_WritReg16(MIU0_RIU_ATOP, 0x90, 0xf0f3);
    MHal_MIU_WritReg16(MIU0_RIU_ATOP, 0x70, 0x0800);

    MHal_MIU_WritReg16(MIU1_RIU_ATOP, 0x62, 0x007f);
    MHal_MIU_WritReg16(MIU1_RIU_ATOP, 0x64, 0xf000);
    MHal_MIU_WritReg16(MIU1_RIU_ATOP, 0x60, 0x00cb);
    MHal_MIU_WritReg16(MIU1_RIU_ATOP, 0x60, 0x00cf);
    MHal_MIU_WritReg16(MIU1_RIU_ATOP, 0x60, 0x00cb);
    MHal_MIU_WritReg16(MIU1_RIU_ATOP, 0x60, 0x00c3);
    MHal_MIU_WritReg16(MIU1_RIU_ATOP, 0x60, 0x00cb);
    MHal_MIU_WritReg16(MIU1_RIU_ATOP, 0x60, 0x00c3);
    MHal_MIU_WritReg16(MIU1_RIU_ATOP, 0x60, 0x00cb);
    MHal_MIU_WritReg16(MIU1_RIU_ATOP, 0x60, 0x00c2);
    MHal_MIU_WritReg16(MIU1_RIU_ATOP, 0x60, 0x00c0);
    MHal_MIU_WritReg16(MIU1_RIU_ATOP, 0x60, 0x33c8);
    MHal_MIU_WritReg16(MIU1_RIU_ATOP, 0x70, 0x0000);
    MHal_MIU_WritReg16(MIU1_RIU_ATOP, 0x98, 0x0000);
    MHal_MIU_WritReg16(MIU1_RIU_ATOP, 0x9a, 0x0000);
    MHal_MIU_WritReg16(MIU1_RIU_ATOP, 0x90, 0xf0f3);
    MHal_MIU_WritReg16(MIU1_RIU_ATOP, 0x70, 0x0800);

    if (u16ChipVersion == MIU_CHIP_VER_U01)
    {
        MHal_MIU_WritReg16(MIU0_RIU_ATOP, 0x30, 0x47ae);
        MHal_MIU_WritReg16(MIU0_RIU_ATOP, 0x32, 0x0011);

        MHal_MIU_WritReg16(MIU1_RIU_ATOP, 0x30, 0x47ae);
        MHal_MIU_WritReg16(MIU1_RIU_ATOP, 0x32, 0x0011);
    }
    else
    {
        MHal_MIU_WritReg16(MIU0_RIU_ATOP, 0x30, 0xbdbe);
        MHal_MIU_WritReg16(MIU0_RIU_ATOP, 0x32, 0x0014);

        MHal_MIU_WritReg16(MIU1_RIU_ATOP, 0x30, 0xbdbe);
        MHal_MIU_WritReg16(MIU1_RIU_ATOP, 0x32, 0x0014);
    }

    delayus_ddr4(1000);

    u16RegVal0 = MHal_MIU_ReadReg16(MIU0_RIU_ATOP, 0x00);
    MHal_MIU_WritReg16(MIU0_RIU_ATOP, 0x00, u16RegVal0 | 0x40);

    u16RegVal0 = MHal_MIU_ReadReg16(MIU1_RIU_ATOP, 0x00);
    MHal_MIU_WritReg16(MIU1_RIU_ATOP, 0x00, u16RegVal0 | 0x40);
    delayus_ddr4(1000);

    for (i = 0; i < 32; i++)
    {
	if (i == 0)
	{
            miu0_Amin = miu0_Amax = MHal_MIU_ReadReg16(MIU0_RIU_ATOP, 0x66);
	    miu1_Amin = miu1_Amax = MHal_MIU_ReadReg16(MIU1_RIU_ATOP, 0x66);
	}
	else
	{
            u16RegVal0 = MHal_MIU_ReadReg16(MIU0_RIU_ATOP, 0x60);
            MHal_MIU_WritReg16(MIU0_RIU_ATOP, 0x60, u16RegVal0 | 0x02);
            u16RegVal0 = MHal_MIU_ReadReg16(MIU1_RIU_ATOP, 0x60);
            MHal_MIU_WritReg16(MIU1_RIU_ATOP, 0x60, u16RegVal0 | 0x02);
            
            delayus_ddr4(30);
            
            u16RegVal0 = MHal_MIU_ReadReg16(MIU0_RIU_ATOP, 0x60);
            MHal_MIU_WritReg16(MIU0_RIU_ATOP, 0x60, u16RegVal0 & (~0x02));
            u16RegVal0 = MHal_MIU_ReadReg16(MIU1_RIU_ATOP, 0x60);
            MHal_MIU_WritReg16(MIU1_RIU_ATOP, 0x60, u16RegVal0 & (~0x02));
	    
            delayus_ddr4(30);

	    u16RegVal0 = MHal_MIU_ReadReg16(MIU0_RIU_ATOP, 0x66);
	    if (u16RegVal0 > miu0_Amax)
	        miu0_Amax = u16RegVal0;
	    if (u16RegVal0 < miu0_Amin)
	        miu0_Amin = u16RegVal0;

            u16RegVal0 = MHal_MIU_ReadReg16(MIU1_RIU_ATOP, 0x66);
            if (u16RegVal0 > miu1_Amax)
                miu1_Amax = u16RegVal0;
            if (u16RegVal0 < miu1_Amin)
                miu1_Amin = u16RegVal0;
	}
        delayus_ddr4(10);
    }

    u16RegVal0 = MHal_MIU_ReadReg16(MIU0_RIU_ATOP, 0x00);
    MHal_MIU_WritReg16(MIU0_RIU_ATOP, 0x00, u16RegVal0 & ~0x40);

    u16RegVal0 = MHal_MIU_ReadReg16(MIU1_RIU_ATOP, 0x00);
    MHal_MIU_WritReg16(MIU1_RIU_ATOP, 0x00, u16RegVal0 & ~0x40);


    if (u16ChipVersion == MIU_CHIP_VER_U01)
    {
        MHal_MIU_WritReg16(MIU0_RIU_ATOP, 0x30, 0x8f5c);
        MHal_MIU_WritReg16(MIU0_RIU_ATOP, 0x32, 0x0022);

        MHal_MIU_WritReg16(MIU1_RIU_ATOP, 0x30, 0x8f5c);
        MHal_MIU_WritReg16(MIU1_RIU_ATOP, 0x32, 0x0022);
    }
    else
    {
        MHal_MIU_WritReg16(MIU0_RIU_ATOP, 0x30, 0x7b7c);
        MHal_MIU_WritReg16(MIU0_RIU_ATOP, 0x32, 0x0029);

        MHal_MIU_WritReg16(MIU1_RIU_ATOP, 0x30, 0x7b7c);
        MHal_MIU_WritReg16(MIU1_RIU_ATOP, 0x32, 0x0029);
    }

    delayus_ddr4(1000);

    MHal_MIU_WritReg16(MIU0_RIU_ATOP, 0x66, 0x0800);
    MHal_MIU_WritReg16(MIU1_RIU_ATOP, 0x66, 0x0800);

    MHal_MIU_WritReg16(MIU0_RIU_ATOP, 0x66, 0x0000);
    MHal_MIU_WritReg16(MIU1_RIU_ATOP, 0x66, 0x0000);

    for (i = 0; i < 32; i++)
    {
        if (i == 0)
        {
            miu0_Bmin = miu0_Bmax = MHal_MIU_ReadReg16(MIU0_RIU_ATOP, 0x66);
	    miu1_Bmin = miu1_Bmax = MHal_MIU_ReadReg16(MIU1_RIU_ATOP, 0x66);
        }
	else
	{
            u16RegVal0 = MHal_MIU_ReadReg16(MIU0_RIU_ATOP, 0x60);
            MHal_MIU_WritReg16(MIU0_RIU_ATOP, 0x60, u16RegVal0 | 0x02);
            u16RegVal0 = MHal_MIU_ReadReg16(MIU1_RIU_ATOP, 0x60);
            MHal_MIU_WritReg16(MIU1_RIU_ATOP, 0x60, u16RegVal0 | 0x02);
            
            delayus_ddr4(30);
            
            u16RegVal0 = MHal_MIU_ReadReg16(MIU0_RIU_ATOP, 0x60);
            MHal_MIU_WritReg16(MIU0_RIU_ATOP, 0x60, u16RegVal0 & (~0x02));
            u16RegVal0 = MHal_MIU_ReadReg16(MIU1_RIU_ATOP, 0x60);
            MHal_MIU_WritReg16(MIU1_RIU_ATOP, 0x60, u16RegVal0 & (~0x02));

            delayus_ddr4(30);

	    u16RegVal0 = MHal_MIU_ReadReg16(MIU0_RIU_ATOP, 0x66);
	    if (u16RegVal0 > miu0_Bmax)
	        miu0_Bmax = u16RegVal0;
	    if (u16RegVal0 < miu0_Bmin)
		miu0_Bmin = u16RegVal0;

	    u16RegVal0 = MHal_MIU_ReadReg16(MIU1_RIU_ATOP, 0x66);
            if (u16RegVal0 > miu1_Bmax)
	        miu1_Bmax = u16RegVal0;
	    if (u16RegVal0 < miu1_Bmin)
                miu1_Bmin = u16RegVal0;
	}
        delayus_ddr4(10);
    }

    MHal_MIU_WritReg16(0x1012, 0x80, miu0_Amax);
    MHal_MIU_WritReg16(0x1012, 0x82, miu0_Amin);
    MHal_MIU_WritReg16(0x1012, 0x84, miu0_Bmax);
    MHal_MIU_WritReg16(0x1012, 0x86, miu0_Bmin);
    MHal_MIU_WritReg16(0x1012, 0x88, miu1_Amax);
    MHal_MIU_WritReg16(0x1012, 0x8a, miu1_Amin);
    MHal_MIU_WritReg16(0x1012, 0x8c, miu1_Bmax);
    MHal_MIU_WritReg16(0x1012, 0x8e, miu1_Bmin);

    MHal_MIU_WritReg16(MIU0_RIU_DTOP, 0x1e, 0x0c01);
    MHal_MIU_WritReg16(MIU1_RIU_DTOP, 0x1e, 0x0c01);

    k = MHal_MIU_ReadReg16(0x1012, 0x90);
    if (k >= 28) {
        MHal_MIU_WritReg16(0x1012, 0x4e, 0xDEAD);
        return 1;
    } else if (k >= 12) {
        ll = 9;
    } else if (k >= 4) {
        ll = 7;
    } else {
        ll = 5;
    }

    if (miu0_Amax < miu0_Bmin && miu0_Bmin - miu0_Amax >= ll)
        j = 1;
    if (miu0_Amax > miu0_Bmin && miu0_Amax - miu0_Bmin >= ll)
        j = 1;
    if (miu0_Bmax < miu0_Amin && miu0_Amin - miu0_Bmax >= ll)
        j = 1;
    if (miu0_Bmax > miu0_Amin && miu0_Bmax - miu0_Amin >= ll)
        j = 1;
    if (miu1_Amax < miu1_Bmin && miu1_Bmin - miu1_Amax >= ll)
        j = 1;
    if (miu1_Amax > miu1_Bmin && miu1_Amax - miu1_Bmin >= ll)
        j = 1;
    if (miu1_Bmax < miu1_Amin && miu1_Amin - miu1_Bmax >= ll)
        j = 1;
    if (miu1_Bmax > miu1_Amin && miu1_Bmax - miu1_Amin >= ll)
        j = 1;

    if (j == 0)
    {
        return 1;
    }
    else
    {
        k = MHal_MIU_ReadReg16(0x1012, 0x90);
        MHal_MIU_WritReg16(0x1012, 0x90, k + 1);
        if (k >= 96) {
            //while (MHal_MIU_ReadReg16(0x1012, 0x90) != 0xBABE);
            //MHal_MIU_WritReg16(0x1012, 0x90, 0x0000);
        }
        MHal_MIU_WritReg16(MIU0_RIU_ATOP, 0x32, 0xc000);
        MHal_MIU_WritReg16(MIU1_RIU_ATOP, 0x32, 0xc000);
        return 0;
    }
}
