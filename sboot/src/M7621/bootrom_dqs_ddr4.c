////////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2006-2009 MStar Semiconductor, Inc.
// All rights reserved.
//
// Unless otherwise stipulated in writing, any and all information contained
// herein regardless in any format shall remain the sole proprietary of
// MStar Semiconductor Inc. and be kept in strict confidence
// ("MStar Confidential Information") by the recipient.
// Any unauthorized act including without limitation unauthorized disclosure,
// copying, use, reproduction, sale, distribution, modification, disassembling,
// reverse engineering and compiling of the contents of MStar Confidential
// Information is unlawful and strictly prohibited. MStar hereby reserves the
// rights to any and all damages, losses, costs and expenses resulting therefrom.
//
////////////////////////////////////////////////////////////////////////////////

//---------------------------------------------------------------------------

//---------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------------
//  Define
//-------------------------------------------------------------------------------------------------
#define MIU_RIU_REG_BASE                   0x1F000000

#define MIU0_RIU_DTOP                      0x1012
#define MIU1_RIU_DTOP                      0x1006
#define MIU2_RIU_DTOP                      0x1620
#define MIU0_RIU_ARB                       0x1615
#define MIU1_RIU_ARB                       0x1622
#define MIU2_RIU_ARB                       0x1623
#define MIU0_RIU_ARBB                      0x1520
#define MIU1_RIU_ARBB                      0x1521
#define MIU2_RIU_ARBB                      0x1522
#define MIU0_RIU_ATOP                      0x110d
#define MIU1_RIU_ATOP                      0x1616
#define MIU2_RIU_ATOP                      0x1621
#define MIU0_RIU_DTOP_E                    0x152b
#define MIU1_RIU_DTOP_E                    0x152c
#define MIU2_RIU_DTOP_E                    0x152d
#define MIU0_RIU_ARB_F                     0x3106
#define MIU1_RIU_ARB_F                     0x3107
#define MIU2_RIU_ARB_F                     0x3108
#define MIU0_RIU_ARB_E                     0x3102
#define MIU1_RIU_ARB_E                     0x3103
#define MIU2_RIU_ARB_E                     0x3104
#define MIU0_RIU_ATOP_E                    0x1515
#define MIU1_RIU_ATOP_E                    0x1516
#define MIU2_RIU_ATOP_E                    0x1517

#define EFUSE_RIU_BANK                     0x0020
#define REG_ATOP_REGION                    0x2F
#define DEBUG
//#define VREF_DQ_EN
//#define MIU_ENABLE_AUTO_WRITE_PHASE
#define MIU_ENABLE_AUTO_READ_PHASE

#define MIU_CHIP_VER_U01                    0
#define MIU_CHIP_VER_U02                    1

typedef unsigned char   u8;
typedef unsigned int   u16;
typedef unsigned long  u32;

#define MHal_MIU_ReadReg16(u32bank, u32reg ) *( ( volatile u16* ) (MIU_RIU_REG_BASE + (u32bank)*0x100*2 + ((u32reg) << 1)) )
#define MHal_MIU_WritReg16(u32bank, u32reg, u16val ) *( ( volatile u16* ) (MIU_RIU_REG_BASE + (u32bank)*0x100*2 + ((u32reg) << 1))  ) = (u16val)

#define MHal_EFUSE_ReadReg16(u32reg) *(( volatile u16* ) (MIU_RIU_REG_BASE + EFUSE_RIU_BANK*0x100*2 + ((u32reg) << 1)))
#define MHal_EFUSE_WritReg16(u32reg, u16val) *(( volatile u16* ) (MIU_RIU_REG_BASE + EFUSE_RIU_BANK*0x100*2 + ((u32reg) << 1))) = (u16val)



//-------------------------------------------------------------------------------------------------
//  Prototypes
//-------------------------------------------------------------------------------------------------
static void putn_ddr4( u8 n );
static void putk_ddr4( char c );
static void delayus_ddr4(u32 us);
void BootRom_MiuDdr4Dqs(u8 u8MiuDev);
static void single_cmd(u16 u16RegMiuDtop, u16 mrx, u8 CmdVal);
static u16 VrefDQ_train_loop(u16 u16RegMiuDtop, u16 u16RegMiuArb, u8 Indx, u8 step, u8 max);
//-------------------------------------------------------------------------------------------------
//  Local variables
//-------------------------------------------------------------------------------------------------
static const u32 VREF_VAL[10] = {0x1c, 0x1e, 0x20, 0x22,
                          0x24, 0x26, 0x28, 0x2a, 0x2c, 0x2e};

//-------------------------------------------------------------------------------------------------
//  Functions
//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------
void putn_ddr4(u8 n)
{
    char c = '0' + n;

    *(volatile unsigned int*)(0x1F201300) = c;
}
//-------------------------------------------------------------------------------------------------
void putk_ddr4(char c)
{
   *(volatile unsigned int*)(0x1F201300) = c;
}
//-------------------------------------------------------------------------------------------------
void single_cmd(u16 u16RegMiuDtop, u16 mrx, u8 CmdVal)
{
    MHal_MIU_WritReg16(u16RegMiuDtop, 0x18, mrx); //Pre-charge all command
    MHal_MIU_WritReg16(u16RegMiuDtop, 0x00, (MHal_MIU_ReadReg16(u16RegMiuDtop, 0x00) & ~0x1e00) | (CmdVal << 9));   //[12:9] : reg_single_cmd =  2 (Pre-charge)
    MHal_MIU_WritReg16(u16RegMiuDtop, 0x00, MHal_MIU_ReadReg16(u16RegMiuDtop, 0x00) | 0x0100);                      //[8] : reg_single_cmd_en = 1
    MHal_MIU_WritReg16(u16RegMiuDtop, 0x00, MHal_MIU_ReadReg16(u16RegMiuDtop, 0x00) & ~0x0100);                     //[8] : reg_single_cmd_en = 0
}
//-------------------------------------------------------------------------------------------------
//####################################################################################
//
//VrefDQ training loop procedure
//
//####################################################################################
u16 VrefDQ_train_loop(u16 u16RegMiuDtop, u16 u16RegMiuArb, u8 Indx, u8 step, u8 max)
{
    u16 loop;

    u16 u16temp1=0;
    u16 u16temp2=0;
    u16 u16data[3]={0,0,0};

    u8  u8temp;
    u8  u8temp1;
    u8  u8temp2;

    u8 loop_cnt=0;

    for (loop=Indx; loop<((max*step)+Indx); loop=loop+step)
    {
        if (loop > 10)
        {
            break;
        }
        MHal_MIU_WritReg16(u16RegMiuDtop, 0x00, MHal_MIU_ReadReg16(u16RegMiuDtop, 0x00) | 0x20);       //[05] : reg_auto_ref_off = 1

        //Enable & Change DQ Vref
        //Set MRx[6:0] = Choose VrefDQ value & Range
        single_cmd(u16RegMiuDtop, (MHal_MIU_ReadReg16(u16RegMiuDtop, 0x18) & ~0xff) | VREF_VAL[loop] | 0x80, 0x00); // = 1 (Enable VrefDQ training)
        delayus_ddr4(1); //wait 150ns for tVREFDQE

        single_cmd(u16RegMiuDtop, (MHal_MIU_ReadReg16(u16RegMiuDtop, 0x18) & ~0xff) | VREF_VAL[loop] | 0x80, 0x00); // = 1 (Enable VrefDQ training)
        delayus_ddr4(1); //wait 150ns for tVREFDQE

        //Enter DQ Vref training Mode.
        //MHal_MIU_WritReg16(u16RegMiuDtop, 0x00, MHal_MIU_ReadReg16(u16RegMiuDtop, 0x00) & ~0x20);       //[05] : reg_auto_ref_off = 0
        MHal_MIU_WritReg16(MIU0_RIU_ARB, 0x7c, 0x800b);
        MHal_MIU_WritReg16(u16RegMiuArb, 0x78, 0x0004);
        MHal_MIU_WritReg16(u16RegMiuArb, 0x78, 0x0000);                                                 //dpat_rst

        // Enable TX DQ deskew scan (by DPAT engine)
        MHal_MIU_WritReg16(u16RegMiuArb, 0x76, 0x0002);
        MHal_MIU_WritReg16(u16RegMiuArb, 0x78, 0x0013);

        //while ((MHal_MIU_ReadReg16(MIU0_RIU_ARB, 0x78) & 0x8000) == 0x0);
        while ((MHal_MIU_ReadReg16(u16RegMiuArb, 0x7e) & 0x000f) != 0x000d);

        MHal_MIU_WritReg16(u16RegMiuDtop, 0x00, MHal_MIU_ReadReg16(u16RegMiuDtop, 0x00) & ~0x20);           //[05] : reg_auto_ref_off = 0

        delayus_ddr4(1);
        while((MHal_MIU_ReadReg16(u16RegMiuArb, 0x78) & 0x8000) == 0x0);

        MHal_MIU_WritReg16(MIU0_RIU_ARB, 0x7c, 0xf004); //SWITCH dpat deb_out = min. margin * 64 + all dq pass windows
        delayus_ddr4(1);
        u16data[2]=u16data[1];
        u16data[1]=u16data[0];
        u16data[0]=MHal_MIU_ReadReg16(MIU0_RIU_ARB, 0x7e);
        u16temp1 = u16data[0] + (u16data[1] << 1) + u16data[2];
        if (u16temp1 >= u16temp2)
        {
            //if (u16temp1 > u16temp2)
            //{
            //    u8temp2=loop-1;
            //}
            u16temp2=u16temp1;
            u8temp1=loop-1;
        }
        #ifdef DEBUG
          MHal_MIU_WritReg16(u16RegMiuDtop, (0x80 + (loop_cnt<<1) ), (u16data[0]) );
          loop_cnt++;
        #endif
    }
    //u8temp = ((u8temp1 + u8temp2 + 1) >> 1);
    return u8temp1;
}
//-------------------------------------------------------------------------------------------------
void delayus_ddr4(u32 us)
{
    u16 u16RegVal0;

    u16RegVal0 = ((us* 12) & 0xffff);
    MHal_MIU_WritReg16(0x30, 0x24, u16RegVal0);

    u16RegVal0 = ((us* 12) >> 16);
    MHal_MIU_WritReg16(0x30, 0x26, u16RegVal0);

    u16RegVal0 = 0x0002;
    MHal_MIU_WritReg16(0x30, 0x20, u16RegVal0);

    do{
        u16RegVal0 = MHal_MIU_ReadReg16(0x30, 0x22);
    }while((u16RegVal0 & 0x0001) == 0);
}

//-------------------------------------------------------------------------------------------------
//####################################################################################
//
//DDR3 & DDR4 Auto-phase procedure
//
//####################################################################################
void BootRom_MiuDdr4Dqs(u8 u8MiuDev)
{
    u16 u16RegVal0;
#ifdef MIU_ENABLE_AUTO_WRITE_PHASE
    u16 u16RegVal1;
#endif
    u16 u16RegMiuArb;
    u16 u16RegMiuArbb;
    u16 u16RegMiuAtop;
    u16 u16RegMiuDtop;
    u16 u16RegMiuDtopE;
    u16 u16RegMiuArb_F;
    u16 u16RegMiuArb_E;
    u16 u16RegMiuAtop_E;
    u32 u32KcodeOffsetValue;
    u16 u16DQSMaxCenter[4];
    u16 u16KCodeOffset[4];
    u16 u16KCode;
    u8 i = 0;

    u16 u16DQPos0 = 0;
    u16 u16DQPos1 = 0;
    u16 u16DQPos2 = 0;
    u16 u16DQPos3 = 0;
    u16 u16DQPosRes = 0;

    u16 u16RegVal;

    if(u8MiuDev == 0)
    {
        u16RegMiuArb  = MIU0_RIU_ARB;
		u16RegMiuArbb = MIU0_RIU_ARBB;
        u16RegMiuAtop = MIU0_RIU_ATOP;
        u16RegMiuDtop = MIU0_RIU_DTOP;
        u16RegMiuDtopE = MIU0_RIU_DTOP_E;
        u16RegMiuArb_F  = MIU0_RIU_ARB_F;
        u16RegMiuArb_E  = MIU0_RIU_ARB_E;
        u16RegMiuAtop_E = MIU0_RIU_ATOP_E;
        MHal_MIU_WritReg16(MIU0_RIU_DTOP, 0xf8, 0x0000);
    }
    else if(u8MiuDev == 1)
    {
        u16RegMiuArb  = MIU1_RIU_ARB;
		u16RegMiuArbb = MIU1_RIU_ARBB;
        u16RegMiuAtop = MIU1_RIU_ATOP;
        u16RegMiuDtop = MIU1_RIU_DTOP;
        u16RegMiuDtopE = MIU1_RIU_DTOP_E;
        u16RegMiuArb_F  = MIU1_RIU_ARB_F;
        u16RegMiuArb_E  = MIU1_RIU_ARB_E;
        u16RegMiuAtop_E = MIU1_RIU_ATOP_E;
        MHal_MIU_WritReg16(MIU0_RIU_DTOP, 0xf8, 0x8000);
    }
    else
    {
        return;
    }

    //----------------------------------------------------------------
    //
    //Un-mask BIST
    //
    //----------------------------------------------------------------
    MHal_MIU_WritReg16(MIU0_RIU_DTOP,  0x1e, 0x8c08);
    MHal_MIU_WritReg16(u16RegMiuArb_E, 0x06, 0xfffe);
    MHal_MIU_WritReg16(u16RegMiuArb_E, 0x46, 0xffff);
    MHal_MIU_WritReg16(u16RegMiuArb_E, 0x86, 0xffff);
    MHal_MIU_WritReg16(u16RegMiuArb_E, 0xc6, 0xffff);
    MHal_MIU_WritReg16(u16RegMiuArb_F, 0x06, 0x7fff);
    MHal_MIU_WritReg16(u16RegMiuArb_F, 0x46, 0xffff);
    MHal_MIU_WritReg16(u16RegMiuArb_F, 0x86, 0xffff);
    MHal_MIU_WritReg16(u16RegMiuArbb,  0x4c, 0x60ef);
    MHal_MIU_WritReg16(u16RegMiuArb,   0xe6, 0xfffc);
	// wriu -b 0x1012ff  0x08 0x00

    u16RegVal0 = MHal_MIU_ReadReg16(u16RegMiuDtop, 0xfe);
    MHal_MIU_WritReg16(u16RegMiuDtop, 0xfe, u16RegVal0 & ~0x0800); //set reg_miu_test_off = 0

    //----------------------------------------------------------------
    //
    //Arbitration setting for auto-phase
    //
    //----------------------------------------------------------------

    // wriu -w 0x31061c  0x8000
    MHal_MIU_WritReg16(u16RegMiuArb_F, 0x1c, 0x8000);
    // wriu -w 0x15205c  0x0010
    MHal_MIU_WritReg16(u16RegMiuArbb, 0x5c, 0x0010);
    // wriu -b 0x161538  0x04 0x00
    u16RegVal0 = MHal_MIU_ReadReg16(u16RegMiuArb, 0x38);
    MHal_MIU_WritReg16(u16RegMiuArb, 0x38, u16RegVal0 & ~0x0004); //[2] : reg_miu_cmd_cut_in_en = 0

    // wriu -w 0x1615e2  0x0000
    MHal_MIU_WritReg16(u16RegMiuArb, 0xe2, 0x0000);

    // wriu -w 0x152b80  0x00f0
    MHal_MIU_WritReg16(u16RegMiuDtopE, 0x80, 0x00f0);
    // wriu -w 0x152b80  0x0010
    MHal_MIU_WritReg16(u16RegMiuDtopE, 0x80, 0x0010);
    // wriu -w 0x152b80  0x8010
    MHal_MIU_WritReg16(u16RegMiuDtopE, 0x80, 0x8010);

    //----------------------------------------------------------------
    //
    //MIU test pattern setting
    //
    //----------------------------------------------------------------


	// wriu -w 0x1012e0  0x0000 // This bank must be miu0
    MHal_MIU_WritReg16(MIU0_RIU_DTOP, 0xe0, 0x0000);
	// wriu -w 0x1012e2  0x0000 // This bank must be miu0
    MHal_MIU_WritReg16(MIU0_RIU_DTOP, 0xe2, 0x0000);
	// wriu -w 0x1012e4  0x0200 // This bank must be miu0
    MHal_MIU_WritReg16(MIU0_RIU_DTOP, 0xe4, 0x2000);
	// wriu -w 0x1012e6  0x0000 // This bank must be miu0
    MHal_MIU_WritReg16(MIU0_RIU_DTOP, 0xe6, 0x0000);

  //-- Multi-pat mode begin --
    MHal_MIU_WritReg16(MIU0_RIU_ARB, 0xcc, 0x0003);  // This bank must be miu0
    MHal_MIU_WritReg16(MIU0_RIU_ARB, 0xce, 0x0000);  // This bank must be miu0
    MHal_MIU_WritReg16(MIU0_RIU_ARB, 0xce, 0x0001);  // This bank must be miu0
  //-- Multi-pat mode end --

    //----------------------------------------------------------------
    //
    //DPAT pre-setting
    //
    //----------------------------------------------------------------

	// wriu -b 0x101207  0x10 0xff
    MHal_MIU_WritReg16(u16RegMiuDtop, 0x06, MHal_MIU_ReadReg16(u16RegMiuDtop, 0x06) | 0x1000); //[12] : reg_cke_always_on = 1
	// wriu -b 0x101200  0x20 0xff
    MHal_MIU_WritReg16(u16RegMiuDtop, 0x00, MHal_MIU_ReadReg16(u16RegMiuDtop, 0x00) | 0x20);   //[5] : reg_auto_ref_off = 1

	// wriu -b 0x152b8a  0x02 0x00
    u16RegVal0 = MHal_MIU_ReadReg16(u16RegMiuDtopE, 0x8a);
    MHal_MIU_WritReg16(u16RegMiuDtopE, 0x8a, u16RegVal0 & ~0x0302); //[1] : reg_precharge_in_idle_en = 0
                                                                    //[8] : reg_reorder_act_en       = 0
                                                                    //[9] : reg_reorder_pre_en       = 0
    MHal_MIU_WritReg16(u16RegMiuArb, 0x70, 0x0000);
	// wriu -w 0x1012e2  0x0000 // This bank must be miu0
    MHal_MIU_WritReg16(u16RegMiuArb, 0x72, 0x00a0);
	// wriu -w 0x1012e4  0x0200 // This bank must be miu0
    MHal_MIU_WritReg16(u16RegMiuArb, 0xa8, 0x0925);
	// wriu -w 0x1012e6  0x0000 // This bank must be miu0
    MHal_MIU_WritReg16(u16RegMiuArb, 0xaa, 0x0009);
    MHal_MIU_WritReg16(u16RegMiuArb, 0x74, 0x0d81);



#ifdef VREF_DQ_EN
    if((MHal_MIU_ReadReg16(u16RegMiuArb, 0x66) & 0x08) == 0x08)
    {
//====================================================================================
//
//
//VrefDQ training
//
//
//====================================================================================

        MHal_MIU_WritReg16(u16RegMiuDtop, 0x18, (MHal_MIU_ReadReg16(u16RegMiuArb, 0x64) & 0xff00) | (MHal_MIU_ReadReg16(u16RegMiuDtop, 0x18) & ~0xff00));       //Copy From 0x161565[7:0] to 0x101219[7:0]

        // Fine tune -->step :  +/- 2 codes twice
        u16RegVal = 4;//TDB
        u16RegVal = VrefDQ_train_loop(u16RegMiuDtop, u16RegMiuArb, u16RegVal-4, 1, 9);

        //SW need calculate Vref_Val = The Vref_val that have Max. minimum margin
        MHal_MIU_WritReg16(u16RegMiuArb, 0x78, 0x0000);
        MHal_MIU_WritReg16(u16RegMiuDtop, 0x00, MHal_MIU_ReadReg16(u16RegMiuDtop, 0x00) | 0x20); //[05] : reg_auto_ref_off = 1

        //Enable & Change DQ Vref
        //Set MRx[6:0] = Choose VrefDQ value & Range
        single_cmd(u16RegMiuDtop, (MHal_MIU_ReadReg16(u16RegMiuDtop, 0x18) & ~0xff) | VREF_VAL[u16RegVal]  | 0x80, 0x00);
        delayus_ddr4(1); //wait 150ns for tVREFDQE

        //Enable & Change DQ Vref
        //Set MRx[6:0] = Choose VrefDQ value & Range
        single_cmd(u16RegMiuDtop, (MHal_MIU_ReadReg16(u16RegMiuDtop, 0x18) & ~0xff) | VREF_VAL[u16RegVal]  | 0x80, 0x00);
        delayus_ddr4(1); //wait 150ns for tVREFDQE

        //Exit DQ Vref training Mode.
        single_cmd(u16RegMiuDtop, MHal_MIU_ReadReg16(u16RegMiuDtop, 0x18) & ~0x80, 0x00);                    // [7]= 0 (Disable VrefDQ training)
        delayus_ddr4(1); //wait 150ns for tVREFDQE

        //MHal_MIU_WritReg16(u16RegMiuDtop, 0x00, MHal_MIU_ReadReg16(u16RegMiuDtop, 0x00) & ~0x20);           //[05] : reg_auto_ref_off = 0
        //MHal_MIU_WritReg16(u16RegMiuDtop, 0x06, MHal_MIU_ReadReg16(u16RegMiuDtop, 0x06) & ~0x1000);         //[12] : reg_cke_always_on = 0
    }
#endif

#ifdef MIU_ENABLE_AUTO_WRITE_PHASE
//====================================================================================
//
//
//Write DQ phase training
//
//
//====================================================================================

	// wriu -w 0x16157c  0x800b // This bank must be miu0
    MHal_MIU_WritReg16(MIU0_RIU_ARB, 0x7c, 0x800b);
	// wriu -w 0x161578  0x0004
    MHal_MIU_WritReg16(u16RegMiuArb, 0x78, 0x0004);
	// wriu -w 0x161578  0x0000
    MHal_MIU_WritReg16(u16RegMiuArb, 0x78, 0x0000);
	// wriu -w 0x161576  0x0002
    MHal_MIU_WritReg16(u16RegMiuArb, 0x76, 0x0002);
	//// wriu -w 0x161578  0x0111
    //MHal_MIU_WritReg16(u16RegMiuArb, 0x78, 0x0111);

      // wriu -w 0x161578  0x0013
    MHal_MIU_WritReg16(u16RegMiuArb, 0x78, 0x0013);

    //while ((MHal_MIU_ReadReg16(MIU0_RIU_ARB, 0x78) & 0x8000) == 0x0);
    while ((MHal_MIU_ReadReg16(u16RegMiuArb, 0x7e) & 0x000f) != 0x000d);

	// wriu -w 0x101200  0x20 0x00
    MHal_MIU_WritReg16(u16RegMiuDtop, 0x00, MHal_MIU_ReadReg16(u16RegMiuDtop, 0x00) & ~0x20);           //[05] : reg_auto_ref_off = 0


    delayus_ddr4(1);

    while((MHal_MIU_ReadReg16(u16RegMiuArb, 0x78) & 0x8000) == 0x0);


	// wriu -w 0x110dc0  0x0004
    MHal_MIU_WritReg16(u16RegMiuAtop, 0xc0, 0x0004);
    delayus_ddr4(1);

	// rriu -c 0x110dc2 2 //reg_rdata
    u16RegVal0 = MHal_MIU_ReadReg16(u16RegMiuAtop, 0xc2);
	// wriu -w 0x151520  0xreg_rdata
    MHal_MIU_WritReg16(u16RegMiuAtop_E, 0x20, u16RegVal0);
	// rriu -c 0x110dc4 2 //reg_rdata
    u16RegVal0 = MHal_MIU_ReadReg16(u16RegMiuAtop, 0xc4);
	// wriu -w 0x151522  0xreg_rdata
    MHal_MIU_WritReg16(u16RegMiuAtop_E, 0x22, u16RegVal0);
	// rriu -c 0x110dc6 2 //reg_rdata
    u16RegVal0 = MHal_MIU_ReadReg16(u16RegMiuAtop, 0xc6);
	// wriu -w 0x151524  0xreg_rdata
    MHal_MIU_WritReg16(u16RegMiuAtop_E, 0x24, u16RegVal0);
	// rriu -c 0x110dc8 2 //reg_rdata
    u16RegVal0 = MHal_MIU_ReadReg16(u16RegMiuAtop, 0xc8);
	// wriu -w 0x151526  0xreg_rdata
    MHal_MIU_WritReg16(u16RegMiuAtop_E, 0x26, u16RegVal0);


	// wriu -w 0x110dc0  0x0005
    MHal_MIU_WritReg16(u16RegMiuAtop, 0xc0, 0x0005);
    delayus_ddr4(1);

	// rriu -c 0x110dc2 2 //reg_rdata
    u16RegVal0 = MHal_MIU_ReadReg16(u16RegMiuAtop, 0xc2);
	// wriu -w 0x151528  0xreg_rdata
    MHal_MIU_WritReg16(u16RegMiuAtop_E, 0x28, u16RegVal0);
	// rriu -c 0x110dc4 2 //reg_rdata
    u16RegVal0 = MHal_MIU_ReadReg16(u16RegMiuAtop, 0xc4);
	// wriu -w 0x15152a  0xreg_rdata
    MHal_MIU_WritReg16(u16RegMiuAtop_E, 0x2a, u16RegVal0);
	// rriu -c 0x110dc6 2 //reg_rdata
    u16RegVal0 = MHal_MIU_ReadReg16(u16RegMiuAtop, 0xc6);
	// wriu -w 0x15152c  0xreg_rdata
    MHal_MIU_WritReg16(u16RegMiuAtop_E, 0x2c, u16RegVal0);
	// rriu -c 0x110dc8 2 //reg_rdata
    u16RegVal0 = MHal_MIU_ReadReg16(u16RegMiuAtop, 0xc8);
	// wriu -w 0x15152e  0xreg_rdata
    MHal_MIU_WritReg16(u16RegMiuAtop_E, 0x2e, u16RegVal0);

	// wriu -w 0x110dc0  0x0006
    MHal_MIU_WritReg16(u16RegMiuAtop, 0xc0, 0x0006);
    delayus_ddr4(1);
	// rriu -c 0x110dc2 2 //reg_rdata
    u16RegVal0 = MHal_MIU_ReadReg16(u16RegMiuAtop, 0xc2);
	// wriu -w 0x151530  0xreg_rdata
    MHal_MIU_WritReg16(u16RegMiuAtop_E, 0x30, u16RegVal0);
	// rriu -c 0x110dc4 2 //reg_rdata
    u16RegVal0 = MHal_MIU_ReadReg16(u16RegMiuAtop, 0xc4);
	// wriu -w 0x151532  0xreg_rdata
    MHal_MIU_WritReg16(u16RegMiuAtop_E, 0x32, u16RegVal0);
	// rriu -c 0x110dc6 2 //reg_rdata
    u16RegVal0 = MHal_MIU_ReadReg16(u16RegMiuAtop, 0xc6);
	// wriu -w 0x151534  0xreg_rdata
    MHal_MIU_WritReg16(u16RegMiuAtop_E, 0x34, u16RegVal0);
	// rriu -c 0x110dc8 2 //reg_rdata
    u16RegVal0 = MHal_MIU_ReadReg16(u16RegMiuAtop, 0xc8);
	// wriu -w 0x151536  0xreg_rdata
    MHal_MIU_WritReg16(u16RegMiuAtop_E, 0x36, u16RegVal0);

	// wriu -w 0x110dc0  0x0007
    MHal_MIU_WritReg16(u16RegMiuAtop, 0xc0, 0x0007);
    delayus_ddr4(1);
	// rriu -c 0x110dc2 2 //reg_rdata
    u16RegVal0 = MHal_MIU_ReadReg16(u16RegMiuAtop, 0xc2);
	// wriu -w 0x151538  0xreg_rdata
    MHal_MIU_WritReg16(u16RegMiuAtop_E, 0x38, u16RegVal0);
	// rriu -c 0x110dc4 2 //reg_rdata
    u16RegVal0 = MHal_MIU_ReadReg16(u16RegMiuAtop, 0xc4);
	// wriu -w 0x15153a  0xreg_rdata
    MHal_MIU_WritReg16(u16RegMiuAtop_E, 0x3a, u16RegVal0);
	// rriu -c 0x110dc6 2 //reg_rdata
    u16RegVal0 = MHal_MIU_ReadReg16(u16RegMiuAtop, 0xc6);
	// wriu -w 0x15153c  0xreg_rdata
    MHal_MIU_WritReg16(u16RegMiuAtop_E, 0x3c, u16RegVal0);
	// rriu -c 0x110dc8 2 //reg_rdata
    u16RegVal0 = MHal_MIU_ReadReg16(u16RegMiuAtop, 0xc8);
	// wriu -w 0x15153e  0xreg_rdata
    MHal_MIU_WritReg16(u16RegMiuAtop_E, 0x3e, u16RegVal0);

//====================================================================================
//
//
//Write DQM phase training
//
//
//====================================================================================

    // wriu -w 0x101200  0x20 0x20
    MHal_MIU_WritReg16(u16RegMiuDtop, 0x00, MHal_MIU_ReadReg16(u16RegMiuDtop, 0x00) | 0x20); //[05] : reg_auto_ref_off = 1


    MHal_MIU_WritReg16(MIU0_RIU_DTOP, 0xbe, 0x0001);

    MHal_MIU_WritReg16(u16RegMiuAtop, 0x40, 0x0010);

    MHal_MIU_WritReg16(u16RegMiuArb, 0x78, 0x0004);

    MHal_MIU_WritReg16(u16RegMiuArb, 0x78, 0x0000);

    MHal_MIU_WritReg16(u16RegMiuArb, 0x76, 0x0002);

      // wriu -w 0x161578  0x0013
    MHal_MIU_WritReg16(u16RegMiuArb, 0x78, 0x0013);

    //while ((MHal_MIU_ReadReg16(MIU0_RIU_ARB, 0x78) & 0x8000) == 0x0);
    while ((MHal_MIU_ReadReg16(u16RegMiuArb, 0x7e) & 0x000f) != 0x000d);

	// wriu -w 0x101200  0x20 0x00
    MHal_MIU_WritReg16(u16RegMiuDtop, 0x00, MHal_MIU_ReadReg16(u16RegMiuDtop, 0x00) & ~0x20);           //[05] : reg_auto_ref_off = 0


    delayus_ddr4(1);

    while((MHal_MIU_ReadReg16(u16RegMiuArb, 0x78) & 0x8000) == 0x0);

    // wriu -w 0x110dc0  0x0004
    MHal_MIU_WritReg16(u16RegMiuAtop, 0xc0, 0x0004);
    delayus_ddr4(1);


	// rriu -c 0x110dca 2 //reg_rdata
    u16RegVal0 = MHal_MIU_ReadReg16(u16RegMiuAtop, 0xca);
	// wriu -w 0x151514  0xreg_rdata
    MHal_MIU_WritReg16(u16RegMiuAtop_E, 0x14, u16RegVal0);
	// rriu -c 0x110dcc 2 //reg_rdata

    u16RegVal0 = MHal_MIU_ReadReg16(u16RegMiuAtop, 0xcc);
	// wriu -w 0x151516  0xreg_rdata
    MHal_MIU_WritReg16(u16RegMiuAtop_E, 0x16, u16RegVal0);

    MHal_MIU_WritReg16(MIU0_RIU_DTOP, 0xbe, 0x0000);
    MHal_MIU_WritReg16(u16RegMiuAtop, 0x40, 0x0000);

#endif

#ifdef MIU_ENABLE_AUTO_READ_PHASE
//====================================================================================
//
//
//Read DQ deskew training
//
//
//====================================================================================
    // wriu -w 0x101200  0x20 0x20
    MHal_MIU_WritReg16(u16RegMiuDtop, 0x00, MHal_MIU_ReadReg16(u16RegMiuDtop, 0x00) | 0x20); //[05] : reg_auto_ref_off = 1

    // wriu -w 0x16157c  0x800b // This bank must be miu0
    MHal_MIU_WritReg16(MIU0_RIU_ARB, 0x7c, 0x800b);
    // wriu -w 0x110d70  0x0000
    MHal_MIU_WritReg16(u16RegMiuAtop, 0x70, 0x0000);
    // wriu -w 0x110d90  0xf0f0
    MHal_MIU_WritReg16(u16RegMiuAtop, 0x90, 0xf0f0);
    // wriu -w 0x110d70  0x0800
    MHal_MIU_WritReg16(u16RegMiuAtop, 0x70, 0x0800);
    // wriu -w 0x161578  0x0004
    MHal_MIU_WritReg16(u16RegMiuArb, 0x78, 0x0004);
    // wriu -w 0x161578  0x0000
    MHal_MIU_WritReg16(u16RegMiuArb, 0x78, 0x0000);
    // wriu -w 0x161576  0x0011
    MHal_MIU_WritReg16(u16RegMiuArb, 0x76, 0x0011);
    // wriu -w 0x161578  0x0111
    //MHal_MIU_WritReg16(u16RegMiuArb, 0x78, 0x0111);
    MHal_MIU_WritReg16(u16RegMiuArb, 0x78, 0x0013);

    //while ((MHal_MIU_ReadReg16(MIU0_RIU_ARB, 0x78) & 0x8000) == 0x0);
    while ((MHal_MIU_ReadReg16(u16RegMiuArb, 0x7e) & 0x000f) != 0x000d);

    // wriu -w 0x101200  0x20 0x00
    u16RegVal0 = MHal_MIU_ReadReg16(u16RegMiuDtop, 0x00);
    MHal_MIU_WritReg16(u16RegMiuDtop, 0x00, u16RegVal0 & ~0x20);
    delayus_ddr4(1);

    while((MHal_MIU_ReadReg16(u16RegMiuArb, 0x78) & 0x8000) == 0x0);

    // wriu -w 0x110dc0  0x0000
    MHal_MIU_WritReg16(u16RegMiuAtop, 0xc0, 0x0000);
    delayus_ddr4(1);

    // rriu -c 0x110dc2 2 //reg_rdata
    u16RegVal0 = MHal_MIU_ReadReg16(u16RegMiuAtop, 0xc2);
    // wriu -w 0x151580  0xreg_rdata
    MHal_MIU_WritReg16(u16RegMiuAtop_E, 0x80, u16RegVal0);
    // rriu -c 0x110dc4 2 //reg_rdata
    u16RegVal0 = MHal_MIU_ReadReg16(u16RegMiuAtop, 0xc4);
    // wriu -w 0x151582  0xreg_rdata
    MHal_MIU_WritReg16(u16RegMiuAtop_E, 0x82, u16RegVal0);
    // rriu -c 0x110dc6 2 //reg_rdata
    u16RegVal0 = MHal_MIU_ReadReg16(u16RegMiuAtop, 0xc6);
    // wriu -w 0x151584  0xreg_rdata
    MHal_MIU_WritReg16(u16RegMiuAtop_E, 0x84, u16RegVal0);
    // rriu -c 0x110dc8 2 //reg_rdata
    u16RegVal0 = MHal_MIU_ReadReg16(u16RegMiuAtop, 0xc8);
    // wriu -w 0x151586  0xreg_rdata
    MHal_MIU_WritReg16(u16RegMiuAtop_E, 0x86, u16RegVal0);

    // wriu -w 0x110dc0  0x0001
    MHal_MIU_WritReg16(u16RegMiuAtop, 0xc0, 0x0001);
    delayus_ddr4(1);

    // rriu -c 0x110dc2 2 //reg_rdata
    u16RegVal0 = MHal_MIU_ReadReg16(u16RegMiuAtop, 0xc2);
    // wriu -w 0x151588  0xreg_rdata
    MHal_MIU_WritReg16(u16RegMiuAtop_E, 0x88, u16RegVal0);
    // rriu -c 0x110dc4 2 //reg_rdata
    u16RegVal0 = MHal_MIU_ReadReg16(u16RegMiuAtop, 0xc4);
    // wriu -w 0x15158a  0xreg_rdata
    MHal_MIU_WritReg16(u16RegMiuAtop_E, 0x8a, u16RegVal0);
    // rriu -c 0x110dc6 2 //reg_rdata
    u16RegVal0 = MHal_MIU_ReadReg16(u16RegMiuAtop, 0xc6);
    // wriu -w 0x15158c  0xreg_rdata
    MHal_MIU_WritReg16(u16RegMiuAtop_E, 0x8c, u16RegVal0);
    // rriu -c 0x110dc8 2 //reg_rdata
    u16RegVal0 = MHal_MIU_ReadReg16(u16RegMiuAtop, 0xc8);
    // wriu -w 0x15158e  0xreg_rdata
    MHal_MIU_WritReg16(u16RegMiuAtop_E, 0x8e, u16RegVal0);


    // wriu -w 0x110dc0  0x0002
    MHal_MIU_WritReg16(u16RegMiuAtop, 0xc0, 0x0002);
    delayus_ddr4(1);
    // rriu -c 0x110dc2 2 //reg_rdata
    u16RegVal0 = MHal_MIU_ReadReg16(u16RegMiuAtop, 0xc2);
    // wriu -w 0x151590  0xreg_rdata
    MHal_MIU_WritReg16(u16RegMiuAtop_E, 0x90, u16RegVal0);
    // rriu -c 0x110dc4 2 //reg_rdata
    u16RegVal0 = MHal_MIU_ReadReg16(u16RegMiuAtop, 0xc4);
    // wriu -w 0x151592  0xreg_rdata
    MHal_MIU_WritReg16(u16RegMiuAtop_E, 0x92, u16RegVal0);
    // rriu -c 0x110dc6 2 //reg_rdata
    u16RegVal0 = MHal_MIU_ReadReg16(u16RegMiuAtop, 0xc6);
    // wriu -w 0x151594  0xreg_rdata
    MHal_MIU_WritReg16(u16RegMiuAtop_E, 0x94, u16RegVal0);
    // rriu -c 0x110dc8 2 //reg_rdata
    u16RegVal0 = MHal_MIU_ReadReg16(u16RegMiuAtop, 0xc8);
    // wriu -w 0x151596  0xreg_rdata
    MHal_MIU_WritReg16(u16RegMiuAtop_E, 0x96, u16RegVal0);

    // wriu -w 0x110dc0  0x0003
    MHal_MIU_WritReg16(u16RegMiuAtop, 0xc0, 0x0003);
    delayus_ddr4(1);
    // rriu -c 0x110dc2 2 //reg_rdata
    u16RegVal0 = MHal_MIU_ReadReg16(u16RegMiuAtop, 0xc2);
    // wriu -w 0x151598  0xreg_rdata
    MHal_MIU_WritReg16(u16RegMiuAtop_E, 0x98, u16RegVal0);
    // rriu -c 0x110dc4 2 //reg_rdata
    u16RegVal0 = MHal_MIU_ReadReg16(u16RegMiuAtop, 0xc4);
    // wriu -w 0x15159a  0xreg_rdata0000
    MHal_MIU_WritReg16(u16RegMiuAtop_E, 0x9a, u16RegVal0);
    // rriu -c 0x110dc6 2 //reg_rdata
    u16RegVal0 = MHal_MIU_ReadReg16(u16RegMiuAtop, 0xc6);
    // wriu -w 0x15159c  0xreg_rdata
    MHal_MIU_WritReg16(u16RegMiuAtop_E, 0x9c, u16RegVal0);
    // rriu -c 0x110dc8 2 //reg_rdata
    u16RegVal0 = MHal_MIU_ReadReg16(u16RegMiuAtop, 0xc8);
    // wriu -w 0x15159e  0xreg_rdata
    MHal_MIU_WritReg16(u16RegMiuAtop_E, 0x9e, u16RegVal0);

    //wriu -w 0x110dc0  0x0011
    MHal_MIU_WritReg16(u16RegMiuAtop, 0xc0, 0x0011);
    delayus_ddr4(1);
    //rriu -c 0x110dc2 2 //reg_rdata
    u16RegVal0 = MHal_MIU_ReadReg16(u16RegMiuAtop, 0xc2);
    //wriu -w 0x110d94  0xreg_rdata
    MHal_MIU_WritReg16(u16RegMiuAtop, 0x94, u16RegVal0);
    //rriu -c 0x110dc4 2 //reg_rdata
    u16RegVal0 = MHal_MIU_ReadReg16(u16RegMiuAtop, 0xc4);
    //wriu -w 0x110d96  0xreg_rdata
    MHal_MIU_WritReg16(u16RegMiuAtop, 0x96, u16RegVal0);

    // wriu -w 0x110dc0  0x0013
    MHal_MIU_WritReg16(u16RegMiuAtop, 0xc0, 0x0013);
    delayus_ddr4(1);
    // rriu -c 0x110dc2 2 //reg_rdata
    u16RegVal0 = MHal_MIU_ReadReg16(u16RegMiuAtop, 0xc2);
    // wriu -w 0x110da4  0xreg_rdata
    MHal_MIU_WritReg16(u16RegMiuAtop, 0xa4, u16RegVal0);
    // rriu -c 0x110dc4 2 //reg_rdata
    u16RegVal0 = MHal_MIU_ReadReg16(u16RegMiuAtop, 0xc4);
    // wriu -w 0x110da6  0xreg_rdata
    MHal_MIU_WritReg16(u16RegMiuAtop, 0xa6, u16RegVal0);
#endif

    /* STEP 6 : Disable DPAT engine & Set DQS Phase = 1/2* Kcode+offset (ratio mode) */
    // wait 1 // wait about 500ns for Dummy write only worst pattern BIST time
    delayus_ddr4(1);
    // wriu -w 0x110d70  0x0000
    MHal_MIU_WritReg16(u16RegMiuAtop, 0x70, 0x0000);
    // wriu -w 0x110d90  0xf0f3
    MHal_MIU_WritReg16(u16RegMiuAtop, 0x90, 0xf0f3);
    // wait 1  //Need wait up to 3.9us for one refresh
    delayus_ddr4(1);
    // wriu -w 0x161578  0x0000
    MHal_MIU_WritReg16(u16RegMiuArb, 0x78, 0x0000);
    // wriu -w 0x161574  0x0c60
    MHal_MIU_WritReg16(u16RegMiuArb, 0x74, 0x0d80);
    // wriu -w 0x110d70  0x0800
    MHal_MIU_WritReg16(u16RegMiuAtop, 0x70, 0x0800);

    //----------------------------------------------------------------
    //
    //Recovery settings
    //
    //----------------------------------------------------------------

    // wriu -w 0x1615ce  0x0000
    MHal_MIU_WritReg16(MIU0_RIU_ARB, 0xCE, 0x0000);
    // wriu -w 0x31061c  0x0000
    MHal_MIU_WritReg16(u16RegMiuArb_F, 0x1c, 0x0000);
    // wriu -w 0x15205c  0x0000
    MHal_MIU_WritReg16(u16RegMiuArbb, 0x40, 0x0000);
    // wriu -b 0x161538  0x04 0xff
    u16RegVal0 = MHal_MIU_ReadReg16(u16RegMiuArb, 0x38);
    MHal_MIU_WritReg16(u16RegMiuArb, 0x38, u16RegVal0 | 0x04);
    // wriu -b 0x161539  0x00 0xff
    //?
    // wriu -b 0x101200  0x20 0x00
    u16RegVal0 = MHal_MIU_ReadReg16(u16RegMiuDtop, 0x00);
    MHal_MIU_WritReg16(u16RegMiuDtop, 0x00, u16RegVal0 & ~0x20);
    // wriu -b 0x152b8a  0x02 0xff
    u16RegVal0 = MHal_MIU_ReadReg16(u16RegMiuDtopE, 0x8a);
    MHal_MIU_WritReg16(u16RegMiuDtopE, 0x8a, u16RegVal0 | 0x0302); //[1] : reg_precharge_in_idle_en = 1
                                                                   //[8] : reg_reorder_act_en       = 1
                                                                   //[9] : reg_reorder_pre_en       = 1
    // open bank
    // wriu -w 0x152b80  0x0010
    MHal_MIU_WritReg16(u16RegMiuDtopE, 0x80, 0x0010);
    // wriu -w 0x152b80  0x00f0
    MHal_MIU_WritReg16(u16RegMiuDtopE, 0x80, 0x00f0);
    // wriu -w 0x152b80  0x80f0
    MHal_MIU_WritReg16(u16RegMiuDtopE, 0x80, 0x80f0);

    /* Disable SI mode */
    //MHal_MIU_WritReg16(MIU0_RIU_DTOP, 0xd4, MHal_MIU_ReadReg16(MIU0_RIU_DTOP, 0xd4) & ~0x01);
#if 0
    putk_ddr4('M');
    putk_ddr4('I');
    putk_ddr4('U');
    putn_ddr4(u8MiuDev);
    putk_ddr4('_');
    putk_ddr4('D');
    putk_ddr4('Q');
    putk_ddr4('S');
    putk_ddr4('-');
    putk_ddr4('O');
    putk_ddr4('K');
    putk_ddr4('\n');
    putk_ddr4('\r');
#endif
}

