////////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2006-2009 MStar Semiconductor, Inc.
// All rights reserved.
//
// Unless otherwise stipulated in writing, any and all information contained
// herein regardless in any format shall remain the sole proprietary of
// MStar Semiconductor Inc. and be kept in strict confidence
// ("MStar Confidential Information") by the recipient.
// Any unauthorized act including without limitation unauthorized disclosure,
// copying, use, reproduction, sale, distribution, modification, disassembling,
// reverse engineering and compiling of the contents of MStar Confidential
// Information is unlawful and strictly prohibited. MStar hereby reserves the
// rights to any and all damages, losses, costs and expenses resulting therefrom.
//
////////////////////////////////////////////////////////////////////////////////

//---------------------------------------------------------------------------

//---------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------------
//  Define
//-------------------------------------------------------------------------------------------------
#define MIU_RIU_REG_BASE                   0x1F000000

typedef unsigned char   u8;
typedef unsigned int   u16;
typedef unsigned long  u32;

#define MHal_MIU_Scan_ReadReg16(u32bank, u32reg ) *( ( volatile u16* ) (MIU_RIU_REG_BASE + (u32bank)*0x100*2 + ((u32reg) << 1)) )
#define MHal_MIU_Scan_WritReg16(u32bank, u32reg, u16val ) *( ( volatile u16* ) (MIU_RIU_REG_BASE + (u32bank)*0x100*2 + ((u32reg) << 1))  ) = (u16val)



#define MIU_SCAN_SSO_MODE           0
#define MIU_SCAN_SSO_MODE_Pattern   0


#define MIU_SCAN_MIU0       0
#define MIU_SCAN_MIU1       1

#define MIU_SCAN_WRITE_PHASE 0
#define MIU_SCAN_READ_PHASE  1
#define MIU_SCAN_CMD_PHASE   2

#define MIU_SCAN_NOT_FULL_COMPLETE  0
#define MIU_SCAN_FULL_COMPLETE      1

#define MIU_SCAN_NOT_DONE   0
#define MIU_SCAN_DONE       1

#define MIU_SCAN_LOW_WORD   0
#define MIU_SCAN_HIGH_WORD  1

#define MIU_SCAN_SKEW_MINUS   0
#define MIU_SCAN_SKEW_NORMAL  1
#define MIU_SCAN_SKEW_PLUS    2

//#define MIU_SCAN_LOOP_COUNT 150

#define MIU_SCAN_RESULT_SIZE 80
#define MIU_SCAN_DEBUG 0

//-------------------------------------------------------------------------------------------------
//  Prototypes
//-------------------------------------------------------------------------------------------------
u8 Miu_Scan_ReadByte(u32 u32RegAddr);
u16 Miu_Scan_Read2Byte(u32 u32RegAddr);
void Miu_Scan_WriteByte(u32 u32RegAddr, u8 u8Val);
void Miu_Scan_Write2Byte(u32 u32RegAddr, u16 u16Val);
//bool Miu_Scan_ReadRegBit(u32 u32RegAddr, u8 u8Mask);
void Miu_Scan_WriteByteMask(u32 u32RegAddr, u8 u8Msk, u8 u8ValIn);


void putn_miu_scan( u8 n );
void putk_miu_scan( char c );
void delayus_miu_scan(u32 us);
#if !MIU_SCAN_SSO_MODE
void BootRom_BistInit(u16 BistMode);
#endif
void BootRom_WritePhase(u8 MiuDev, u32 MiuPhase);
void BootRom_ChangeSkew(u8 MiuDev, u16 OriSkew_L, u16 OriSkew_H);
void BootRom_SetBackWritePhase(u8 MiuDev, u16 OriSkew_L, u16 OriSkew_H);

void BootRom_MiuScan(void);

//-------------------------------------------------------------------------------------------------
//  Local variables
//-------------------------------------------------------------------------------------------------
u16 MIU0_Phase_L_default[8] = {0,};
u16 MIU0_Phase_H_default[8] = {0,};


u16 MIU0_Phase_L_Result[MIU_SCAN_RESULT_SIZE] = {0,};
u16 MIU0_Phase_H_Result[MIU_SCAN_RESULT_SIZE] = {0,};

u16 MIU1_Phase_L_default[8] = {0,};
u16 MIU1_Phase_H_default[8] = {0,};

u16 MIU1_Phase_L_Result[MIU_SCAN_RESULT_SIZE] = {0,};
u16 MIU1_Phase_H_Result[MIU_SCAN_RESULT_SIZE] = {0,};


u16 MIU0_OriSkew_L = 0, MIU1_OriSkew_L = 0;
u16 MIU0_OriSkew_H = 0, MIU1_OriSkew_H = 0;
u16 Count_ReadPahse = 0;

u16 CMD_MIU0_Ori_Skew = 0, CMD_MIU1_Ori_Skew = 0;
u16 CMD_MIU0_Ori_Phase = 0, CMD_MIU1_Ori_Phase = 0;


u16 LW_Result = 0, HW_Result = 0;

u32 MIU_SCAN_LOOP_COUNT = 0;
u8  SkipMiuTest = 0;
u8  DoReadPhase = 0;
u8  DoWritePhase = 0;
u8  DoCmdPhase = 0;

//u8 u8IsFirstTimeInit = 0;
//-------------------------------------------------------------------------------------------------
//  Functions
//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------
u8 Miu_Scan_ReadByte(u32 u32RegAddr)
{
    return ((volatile u8*)(MIU_RIU_REG_BASE))[(u32RegAddr << 1) - (u32RegAddr & 1)];
}

////////////////////////////////////////////////////////////////////////////////
/// @brief \b Function  \b Name: HAL_PM_Read2Byte
/// @brief \b Function  \b Description: read 2 Byte data
/// @param <IN>         \b u32RegAddr: register address
/// @param <OUT>        \b None :
/// @param <RET>        \b MS_U16
/// @param <GLOBAL>     \b None :
////////////////////////////////////////////////////////////////////////////////
u16 Miu_Scan_Read2Byte(u32 u32RegAddr)
{
    return *(( volatile unsigned int* )(MIU_RIU_REG_BASE + (u32RegAddr << 1)));

    //return ((volatile u16*)(MIU_RIU_REG_BASE))[u32RegAddr];
}

////////////////////////////////////////////////////////////////////////////////
/// @brief \b Function  \b Name: HAL_PM_WriteByte
/// @brief \b Function  \b Description: write 1 Byte data
/// @param <IN>         \b u32RegAddr: register address
/// @param <IN>         \b u8Val : 1 byte data
/// @param <OUT>        \b None :
/// @param <RET>        \b TRUE: Ok FALSE: Fail
/// @param <GLOBAL>     \b None :
////////////////////////////////////////////////////////////////////////////////
void Miu_Scan_WriteByte(u32 u32RegAddr, u8 u8Val)
{

    ((volatile u8*)(MIU_RIU_REG_BASE))[(u32RegAddr << 1) - (u32RegAddr & 1)] = u8Val;
    //return;
}

////////////////////////////////////////////////////////////////////////////////
/// @brief \b Function  \b Name: HAL_PM_Write2Byte
/// @brief \b Function  \b Description: write 2 Byte data
/// @param <IN>         \b u32RegAddr: register address
/// @param <IN>         \b u16Val : 2 byte data
/// @param <OUT>        \b None :
/// @param <RET>        \b TRUE: Ok FALSE: Fail
/// @param <GLOBAL>     \b None :
////////////////////////////////////////////////////////////////////////////////
void Miu_Scan_Write2Byte(u32 u32RegAddr, u16 u16Val)
{

    //((volatile u16*)(MIU_RIU_REG_BASE))[u32RegAddr] = u16Val;
    *(( volatile unsigned int* )(MIU_RIU_REG_BASE + (u32RegAddr << 1))) = u16Val;
    //return;
}


#if 0
////////////////////////////////////////////////////////////////////////////////
/// @brief \b Function  \b Name: HAL_PM_ReadRegBit
/// @brief \b Function  \b Description: write 1 Byte data
/// @param <IN>         \b u32RegAddr: register address
/// @param <IN>         \b u8Val : 1 byte data
/// @param <OUT>        \b None :
/// @param <RET>        \b TRUE: Ok FALSE: Fail
/// @param <GLOBAL>     \b None :
////////////////////////////////////////////////////////////////////////////////
bool Miu_Scan_ReadRegBit(u32 u32RegAddr, u8 u8Mask)
{
    u8 u8Val;

    u8Val = Miu_Scan_ReadByte(u32RegAddr);
    return (u8Val & u8Mask);
}
#endif


////////////////////////////////////////////////////////////////////////////////
/// @brief \b Function  \b Name: HAL_PM_WriteByteMask
/// @brief \b Function  \b Description: write 1 Byte data
/// @param <IN>         \b u32RegAddr: register address
/// @param <IN>         \b u8Val : 1 byte data
/// @param <OUT>        \b None :
/// @param <RET>        \b TRUE: Ok FALSE: Fail
/// @param <GLOBAL>     \b None :
////////////////////////////////////////////////////////////////////////////////
void Miu_Scan_WriteByteMask(u32 u32RegAddr, u8 u8Msk, u8 u8ValIn)
{
    u8 u8Val;

    u8Val = Miu_Scan_ReadByte(u32RegAddr);
    u8Val = (u8Val & ~(u8Msk)) | ((u8ValIn) & (u8Msk));
    Miu_Scan_WriteByte(u32RegAddr, u8Val);
    //return;
}

char getc_miu_scan(void)
{
    //Wait for Transmit-hold-register empty
    while (!(*(volatile unsigned int*)(0x1F201300 + (5<<3)) & 0x20));

    return *(volatile unsigned int*)(0x1F201300);
}


void putn_miu_scan(u8 n)
{
    char c = '0' + n;

    //Wait for Transmit-hold-register empty
    while (!(*(volatile unsigned int*)(0x1F201300 + (5<<3)) & 0x20));

    *(volatile unsigned int*)(0x1F201300) = c;
}
//-------------------------------------------------------------------------------------------------
void putk_miu_scan(char c)
{
    //Wait for Transmit-hold-register empty
    while (!(*(volatile unsigned int*)(0x1F201300 + (5<<3)) & 0x20));

   *(volatile unsigned int*)(0x1F201300) = c;
   //delayus_miu_scan(5000);
}
//-------------------------------------------------------------------------------------------------
void print_miu_scan(char input)
{
    u8 tmpHigh = 0;
    u8 tmpLow = 0;
    u8 tmpSize = 0;

    if(input >= 10)
    {
        tmpSize = input;
        tmpHigh = 0;
        tmpLow = 0;

        while(tmpSize >= 10)
        {
            tmpSize = tmpSize - 10;
            tmpHigh = tmpHigh + 1;
        }
        tmpLow = tmpSize;

            putn_miu_scan(tmpHigh);
            putn_miu_scan(tmpLow);
    }
    else
        putn_miu_scan(input);

}
//-------------------------------------------------------------------------------------------------
void delayus_miu_scan(u32 us)
{
    u16 u16RegVal0;

    u16RegVal0 = ((us* 12) & 0xffff);
    MHal_MIU_Scan_WritReg16(0x30, 0x24, u16RegVal0);

    u16RegVal0 = ((us* 12) >> 16);
    MHal_MIU_Scan_WritReg16(0x30, 0x26, u16RegVal0);

    u16RegVal0 = 0x0002;
    MHal_MIU_Scan_WritReg16(0x30, 0x20, u16RegVal0);

    do{
        u16RegVal0 = MHal_MIU_Scan_ReadReg16(0x30, 0x22);
    }while((u16RegVal0 & 0x0001) == 0);
}

//-------------------------------------------------------------------------------------------------

void BootRom_InitResult(void)
{
    u16 count = 0;

    for(count = 0; count < MIU_SCAN_RESULT_SIZE;count++)
    {
        MIU0_Phase_L_Result[count] = 0;
        MIU0_Phase_H_Result[count] = 0;
        MIU1_Phase_L_Result[count] = 0;
        MIU1_Phase_H_Result[count] = 0;
    }
}

void BootRom_OutputBitResult(u8 MiuDev, u8 ResultSize, u8 isHWord, u8 ScanSkew)
{
    u16 tmpByteResult = 0;
    u8 tmpIndex = 0, tmpBitIndex = 0;


    for(tmpIndex = 0; tmpIndex < ResultSize; tmpIndex++)
    {
        if(MiuDev == 0)
        {
            if(isHWord == 0)
            {
                tmpByteResult = MIU0_Phase_L_Result[tmpIndex];
                MIU0_Phase_L_Result[tmpIndex] = 0;
            }
            else
            {
                tmpByteResult = MIU0_Phase_H_Result[tmpIndex];
                MIU0_Phase_H_Result[tmpIndex] = 0;
            }
        }
        else
        {
            if(isHWord == 0)
            {
                tmpByteResult = MIU1_Phase_L_Result[tmpIndex];
                MIU1_Phase_L_Result[tmpIndex] = 0;
            }
            else
            {
                tmpByteResult = MIU1_Phase_H_Result[tmpIndex];
                MIU1_Phase_H_Result[tmpIndex] = 0;
            }
        }

        for(tmpBitIndex = 0; tmpBitIndex < 16; tmpBitIndex++)
        {
            if((tmpByteResult & (0x1 << tmpBitIndex )) == 0x0)
                putk_miu_scan('0');
            else
                putk_miu_scan('1');
        }

        putk_miu_scan(' ');
        putk_miu_scan(':');
        putk_miu_scan(' ');

        print_miu_scan(tmpIndex);


        if(ScanSkew != MIU_SCAN_SKEW_NORMAL)
        {
            putk_miu_scan(',');
            putk_miu_scan('S');

            if(ScanSkew == MIU_SCAN_SKEW_MINUS)
                putk_miu_scan('-');
            else
                putk_miu_scan('+');

            putk_miu_scan('1');
        }

        putk_miu_scan('\n');
        putk_miu_scan('\r');
    }

}

void BootRom_OutputByteResult(u8 MiuDev, u8 ResultSize, u8 isHWord)
{
    u16 tmpByteResult = 0;
    u8 tmpIndex = 0, tmpBitIndex = 0;

    for(tmpIndex = 0; tmpIndex < ResultSize; tmpIndex++)
    {
        if(MiuDev == 0)
        {
            if(isHWord == 0)
            {
                tmpByteResult = MIU0_Phase_L_Result[tmpIndex];
                MIU0_Phase_L_Result[tmpIndex] = 0;
            }
            else
            {
                tmpByteResult = MIU0_Phase_H_Result[tmpIndex];
                MIU0_Phase_H_Result[tmpIndex] = 0;
            }

        }
        else
        {
            if(isHWord == 0)
            {
                tmpByteResult = MIU1_Phase_L_Result[tmpIndex];
                MIU1_Phase_L_Result[tmpIndex] = 0;
            }
            else
            {
                tmpByteResult = MIU1_Phase_H_Result[tmpIndex];
                MIU1_Phase_H_Result[tmpIndex] = 0;
            }

        }

        if(tmpByteResult == 0x0)
            putk_miu_scan('0');
        else
            putk_miu_scan('1');

        putk_miu_scan(' ');
        putk_miu_scan(':');
        putk_miu_scan(' ');

        print_miu_scan(tmpIndex);

        putk_miu_scan('\n');
        putk_miu_scan('\r');
    }

}

void BootRom_OutputDQInfo(u8 isHWord, u8 DQ_Start, u8 DQ_End)
{
    u8 tmpHigh = 0;
    u8 tmpLow = 0;
    u8 tmpEnd = 0;

    if(DQ_End >= 10)
    {
        tmpEnd = DQ_End;

        while(tmpEnd >= 10)
        {
            tmpEnd = tmpEnd - 10;
            tmpHigh = tmpHigh + 1;
        }

        tmpLow = tmpEnd;
    }

    if(isHWord == MIU_SCAN_LOW_WORD)
    {
        putk_miu_scan('L');
        putk_miu_scan('o');
        putk_miu_scan('w');
    }
    else
    {
        putk_miu_scan('H');
        putk_miu_scan('i');
        putk_miu_scan('g');
        putk_miu_scan('h');
    }

    putk_miu_scan(' ');

    putk_miu_scan('W');
    putk_miu_scan('o');
    putk_miu_scan('r');
    putk_miu_scan('d');

    putk_miu_scan('\n');
    putk_miu_scan('\r');

    putk_miu_scan('D');
    putk_miu_scan('Q');
    putk_miu_scan('[');
    //putn_miu_scan(DQ_Start);
    putk_miu_scan('0');
    putk_miu_scan(']');
    putk_miu_scan('~');
    putk_miu_scan('D');
    putk_miu_scan('Q');
    putk_miu_scan('[');

#if 0
    if(DQ_End >= 10)
    {
        putn_miu_scan(tmpHigh);
        putn_miu_scan(tmpLow);
    }
    else
        putn_miu_scan(DQ_End);
#else
    putk_miu_scan('1');
    putk_miu_scan('5');
#endif


    putk_miu_scan(']');
    putk_miu_scan('\n');
    putk_miu_scan('\r');

}
void BootRom_OutputDefaultInfo(u8 MiuDev, u8 ScanCmd, u8 isHWord)
{
    char default_skew = 0;
    char default_phase = 0;
    u8 dq_counter = 0;

    u8 rx_phase_offset_byte0 = 0;
    u8 rx_phase_offset_byte1 = 0;
    u8 rx_phase_offset_byte2 = 0;
    u8 rx_phase_offset_byte3 = 0;
    u16 kcode_offset_byte0_byte1 = 0;
    u16 kcode_offset_byte2_byte3 = 0;

    u16 Rx_Dll_Deskew_L_default[8] = {0,};
    u16 Rx_Dll_Deskew_H_default[8] = {0,};

    if(ScanCmd == MIU_SCAN_WRITE_PHASE)
    {
        putk_miu_scan('W');
        putk_miu_scan('r');
        putk_miu_scan('i');
        putk_miu_scan('t');
        putk_miu_scan('e');
    }
    else if(ScanCmd == MIU_SCAN_READ_PHASE)
    {
        if(isHWord == MIU_SCAN_LOW_WORD)
        {
            putk_miu_scan('K');
            putk_miu_scan('c');
            putk_miu_scan('o');
            putk_miu_scan('d');
            putk_miu_scan('e');
            putk_miu_scan(' ');
            putk_miu_scan('V');
            putk_miu_scan('a');
            putk_miu_scan('l');
            putk_miu_scan('u');
            putk_miu_scan('e');
            putk_miu_scan(' ');
            putk_miu_scan(':');
            putk_miu_scan(' ');

            if(MiuDev == 0)
                Count_ReadPahse = Miu_Scan_Read2Byte(0x110d66);
            else
                Count_ReadPahse = Miu_Scan_Read2Byte(0x161666);

            print_miu_scan(Count_ReadPahse);
            putk_miu_scan('\n');
            putk_miu_scan('\r');
        }

        putk_miu_scan('R');
        putk_miu_scan('e');
        putk_miu_scan('a');
        putk_miu_scan('d');


        for(dq_counter=0;dq_counter < 8; dq_counter++)
        {
            MIU0_Phase_L_default[dq_counter] = 0;
            MIU0_Phase_H_default[dq_counter] = 0;
            MIU1_Phase_L_default[dq_counter] = 0;
            MIU1_Phase_H_default[dq_counter] = 0;
        }

        if(MiuDev == 0)
        {
            //read K-code offset
            //rriu -c 0x110d94 2 //[13:8] kcode_offset_byte1, [5:0] kcode_offset_byte0
            //rriu -c 0x110d96 2 //[13:8] kcode_offset_byte3, [5:0] kcode_offset_byte2
            kcode_offset_byte0_byte1 = Miu_Scan_Read2Byte(0x110d94);
            kcode_offset_byte2_byte3 = Miu_Scan_Read2Byte(0x110d96);

            //read LW rx_dll_deskew
            Rx_Dll_Deskew_L_default[0] = Miu_Scan_Read2Byte(0x151580);
            Rx_Dll_Deskew_L_default[1] = Miu_Scan_Read2Byte(0x151582);
            Rx_Dll_Deskew_L_default[2] = Miu_Scan_Read2Byte(0x151584);
            Rx_Dll_Deskew_L_default[3] = Miu_Scan_Read2Byte(0x151586);
            Rx_Dll_Deskew_L_default[4] = Miu_Scan_Read2Byte(0x151588);
            Rx_Dll_Deskew_L_default[5] = Miu_Scan_Read2Byte(0x15158a);
            Rx_Dll_Deskew_L_default[6] = Miu_Scan_Read2Byte(0x15158c);
            Rx_Dll_Deskew_L_default[7] = Miu_Scan_Read2Byte(0x15158e);


            //read HW rx_dll_deskew
            Rx_Dll_Deskew_H_default[0] = Miu_Scan_Read2Byte(0x151590);
            Rx_Dll_Deskew_H_default[1] = Miu_Scan_Read2Byte(0x151592);
            Rx_Dll_Deskew_H_default[2] = Miu_Scan_Read2Byte(0x151594);
            Rx_Dll_Deskew_H_default[3] = Miu_Scan_Read2Byte(0x151596);
            Rx_Dll_Deskew_H_default[4] = Miu_Scan_Read2Byte(0x151598);
            Rx_Dll_Deskew_H_default[5] = Miu_Scan_Read2Byte(0x15159a);
            Rx_Dll_Deskew_H_default[6] = Miu_Scan_Read2Byte(0x15159c);
            Rx_Dll_Deskew_H_default[7] = Miu_Scan_Read2Byte(0x15159e);
        }
        else
        {
            //read K-code offset
            //rriu -c 0x110d94 2 //[13:8] kcode_offset_byte1, [5:0] kcode_offset_byte0
            //rriu -c 0x110d96 2 //[13:8] kcode_offset_byte3, [5:0] kcode_offset_byte2
            kcode_offset_byte0_byte1 = Miu_Scan_Read2Byte(0x161694);
            kcode_offset_byte2_byte3 = Miu_Scan_Read2Byte(0x161696);

            //read LW rx_dll_deskew
            Rx_Dll_Deskew_L_default[0] = Miu_Scan_Read2Byte(0x151680);
            Rx_Dll_Deskew_L_default[1] = Miu_Scan_Read2Byte(0x151682);
            Rx_Dll_Deskew_L_default[2] = Miu_Scan_Read2Byte(0x151684);
            Rx_Dll_Deskew_L_default[3] = Miu_Scan_Read2Byte(0x151686);
            Rx_Dll_Deskew_L_default[4] = Miu_Scan_Read2Byte(0x151688);
            Rx_Dll_Deskew_L_default[5] = Miu_Scan_Read2Byte(0x15168a);
            Rx_Dll_Deskew_L_default[6] = Miu_Scan_Read2Byte(0x15168c);
            Rx_Dll_Deskew_L_default[7] = Miu_Scan_Read2Byte(0x15168e);
            //read HW rx_dll_deskew
            Rx_Dll_Deskew_H_default[0] = Miu_Scan_Read2Byte(0x151690);
            Rx_Dll_Deskew_H_default[1] = Miu_Scan_Read2Byte(0x151692);
            Rx_Dll_Deskew_H_default[2] = Miu_Scan_Read2Byte(0x151694);
            Rx_Dll_Deskew_H_default[3] = Miu_Scan_Read2Byte(0x151696);
            Rx_Dll_Deskew_H_default[4] = Miu_Scan_Read2Byte(0x151698);
            Rx_Dll_Deskew_H_default[5] = Miu_Scan_Read2Byte(0x15169a);
            Rx_Dll_Deskew_H_default[6] = Miu_Scan_Read2Byte(0x15169c);
            Rx_Dll_Deskew_H_default[7] = Miu_Scan_Read2Byte(0x15169e);
        }

        if(kcode_offset_byte0_byte1 & 0x0020)
            rx_phase_offset_byte0 = ((Count_ReadPahse >> 1) - (kcode_offset_byte0_byte1 & 0x001F));
       else
            rx_phase_offset_byte0 = ((Count_ReadPahse >> 1) + (kcode_offset_byte0_byte1 & 0x001F));

        if(kcode_offset_byte0_byte1 & 0x2000)
            rx_phase_offset_byte1 = ((Count_ReadPahse >> 1) - ((kcode_offset_byte0_byte1 & 0x1F00) >> 8));
        else
           rx_phase_offset_byte1 = ((Count_ReadPahse >> 1) + ((kcode_offset_byte0_byte1 & 0x1F00) >> 8));

       if(kcode_offset_byte2_byte3 & 0x0020)
           rx_phase_offset_byte2 = ((Count_ReadPahse >> 1) - (kcode_offset_byte2_byte3 & 0x001F));
       else
           rx_phase_offset_byte2 = ((Count_ReadPahse >> 1) + (kcode_offset_byte2_byte3 & 0x001F));

       if(kcode_offset_byte2_byte3 & 0x2000)
           rx_phase_offset_byte3 = ((Count_ReadPahse >> 1) - ((kcode_offset_byte2_byte3 & 0x1F00) >> 8));
       else
          rx_phase_offset_byte3 = ((Count_ReadPahse >> 1) + ((kcode_offset_byte2_byte3 & 0x1F00) >> 8));

#if 0
    if(MiuDev == 0)
    {
        MIU0_Phase_L_default[0] = rx_phase_offset_byte0 - (Rx_Dll_Deskew_L_default[0] & 0x001F);
        MIU0_Phase_L_default[0] += (rx_phase_offset_byte0 - ((Rx_Dll_Deskew_L_default[0] & 0x1F00)>>8)) << 8;
        MIU0_Phase_L_default[1] = rx_phase_offset_byte0 - (Rx_Dll_Deskew_L_default[1] & 0x001F);
        MIU0_Phase_L_default[1] += (rx_phase_offset_byte0 - ((Rx_Dll_Deskew_L_default[1] & 0x1F00)>>8)) << 8;
        MIU0_Phase_L_default[2] = rx_phase_offset_byte0 - (Rx_Dll_Deskew_L_default[2] & 0x001F);
        MIU0_Phase_L_default[2] += (rx_phase_offset_byte0 - ((Rx_Dll_Deskew_L_default[2] & 0x1F00)>>8)) << 8;
        MIU0_Phase_L_default[3] = rx_phase_offset_byte0 - (Rx_Dll_Deskew_L_default[3] & 0x001F);
        MIU0_Phase_L_default[3] += (rx_phase_offset_byte0 - ((Rx_Dll_Deskew_L_default[3] & 0x1F00)>>8)) << 8;
        MIU0_Phase_L_default[4] = rx_phase_offset_byte1 - (Rx_Dll_Deskew_L_default[4] & 0x001F);
        MIU0_Phase_L_default[4] += (rx_phase_offset_byte1 - ((Rx_Dll_Deskew_L_default[4] & 0x1F00)>>8)) << 8;
        MIU0_Phase_L_default[5] = rx_phase_offset_byte1 - (Rx_Dll_Deskew_L_default[5] & 0x001F);
        MIU0_Phase_L_default[5] += (rx_phase_offset_byte1 - ((Rx_Dll_Deskew_L_default[5] & 0x1F00)>>8)) << 8;
        MIU0_Phase_L_default[6] = rx_phase_offset_byte1 - (Rx_Dll_Deskew_L_default[6] & 0x001F);
        MIU0_Phase_L_default[6] += (rx_phase_offset_byte1 - ((Rx_Dll_Deskew_L_default[6] & 0x1F00)>>8)) << 8;
        MIU0_Phase_L_default[7] = rx_phase_offset_byte1 - (Rx_Dll_Deskew_L_default[7] & 0x001F);
        MIU0_Phase_L_default[7] += (rx_phase_offset_byte1 - ((Rx_Dll_Deskew_L_default[7] & 0x1F00)>>8)) << 8;

        MIU0_Phase_H_default[0] = rx_phase_offset_byte2 - (Rx_Dll_Deskew_H_default[0] & 0x001F);
        MIU0_Phase_H_default[0] += (rx_phase_offset_byte2 - ((Rx_Dll_Deskew_H_default[0] & 0x1F00)>>8)) << 8;
        MIU0_Phase_H_default[1] = rx_phase_offset_byte2 - (Rx_Dll_Deskew_H_default[1] & 0x001F);
        MIU0_Phase_H_default[1] += (rx_phase_offset_byte2 - ((Rx_Dll_Deskew_H_default[1] & 0x1F00)>>8)) << 8;
        MIU0_Phase_H_default[2] = rx_phase_offset_byte2 - (Rx_Dll_Deskew_H_default[2] & 0x001F);
        MIU0_Phase_H_default[2] += (rx_phase_offset_byte2 - ((Rx_Dll_Deskew_H_default[2] & 0x1F00)>>8)) << 8;
        MIU0_Phase_H_default[3] = rx_phase_offset_byte2 - (Rx_Dll_Deskew_H_default[3] & 0x001F);
        MIU0_Phase_H_default[3] += (rx_phase_offset_byte2 - ((Rx_Dll_Deskew_H_default[3] & 0x1F00)>>8)) << 8;
        MIU0_Phase_H_default[4] = rx_phase_offset_byte3 - (Rx_Dll_Deskew_H_default[4] & 0x001F);
        MIU0_Phase_H_default[4] += (rx_phase_offset_byte3 - ((Rx_Dll_Deskew_H_default[4] & 0x1F00)>>8)) << 8;
        MIU0_Phase_H_default[5] = rx_phase_offset_byte3 - (Rx_Dll_Deskew_H_default[5] & 0x001F);
        MIU0_Phase_H_default[5] += (rx_phase_offset_byte3 - ((Rx_Dll_Deskew_H_default[5] & 0x1F00)>>8)) << 8;
        MIU0_Phase_H_default[6] = rx_phase_offset_byte3 - (Rx_Dll_Deskew_H_default[6] & 0x001F);
        MIU0_Phase_H_default[6] += (rx_phase_offset_byte3 - ((Rx_Dll_Deskew_H_default[6] & 0x1F00)>>8)) << 8;
        MIU0_Phase_H_default[7] = rx_phase_offset_byte3 - (Rx_Dll_Deskew_H_default[7] & 0x001F);
        MIU0_Phase_H_default[7] += (rx_phase_offset_byte3 - ((Rx_Dll_Deskew_H_default[7] & 0x1F00)>>8)) << 8;
    }
    else
    {

        MIU1_Phase_L_default[0] = rx_phase_offset_byte0 - (Rx_Dll_Deskew_L_default[0] & 0x001F);
        MIU1_Phase_L_default[0] += (rx_phase_offset_byte0 - ((Rx_Dll_Deskew_L_default[0] & 0x1F00)>>8)) << 8;
        MIU1_Phase_L_default[1] = rx_phase_offset_byte0 - (Rx_Dll_Deskew_L_default[1] & 0x001F);
        MIU1_Phase_L_default[1] += (rx_phase_offset_byte0 - ((Rx_Dll_Deskew_L_default[1] & 0x1F00)>>8)) << 8;
        MIU1_Phase_L_default[2] = rx_phase_offset_byte0 - (Rx_Dll_Deskew_L_default[2] & 0x001F);
        MIU1_Phase_L_default[2] += (rx_phase_offset_byte0 - ((Rx_Dll_Deskew_L_default[2] & 0x1F00)>>8)) << 8;
        MIU1_Phase_L_default[3] = rx_phase_offset_byte0 - (Rx_Dll_Deskew_L_default[3] & 0x001F);
        MIU1_Phase_L_default[3] += (rx_phase_offset_byte0 - ((Rx_Dll_Deskew_L_default[3] & 0x1F00)>>8)) << 8;
        MIU1_Phase_L_default[4] = rx_phase_offset_byte1 - (Rx_Dll_Deskew_L_default[4] & 0x001F);
        MIU1_Phase_L_default[4] += (rx_phase_offset_byte1 - ((Rx_Dll_Deskew_L_default[4] & 0x1F00)>>8)) << 8;
        MIU1_Phase_L_default[5] = rx_phase_offset_byte1 - (Rx_Dll_Deskew_L_default[5] & 0x001F);
        MIU1_Phase_L_default[5] += (rx_phase_offset_byte1 - ((Rx_Dll_Deskew_L_default[5] & 0x1F00)>>8)) << 8;
        MIU1_Phase_L_default[6] = rx_phase_offset_byte1 - (Rx_Dll_Deskew_L_default[6] & 0x001F);
        MIU1_Phase_L_default[6] += (rx_phase_offset_byte1 - ((Rx_Dll_Deskew_L_default[6] & 0x1F00)>>8)) << 8;
        MIU1_Phase_L_default[7] = rx_phase_offset_byte1 - (Rx_Dll_Deskew_L_default[7] & 0x001F);
        MIU1_Phase_L_default[7] += (rx_phase_offset_byte1 - ((Rx_Dll_Deskew_L_default[7] & 0x1F00)>>8)) << 8;

        MIU1_Phase_H_default[0] = rx_phase_offset_byte2 - (Rx_Dll_Deskew_H_default[0] & 0x001F);
        MIU1_Phase_H_default[0] += (rx_phase_offset_byte2 - ((Rx_Dll_Deskew_H_default[0] & 0x1F00)>>8)) << 8;
        MIU1_Phase_H_default[1] = rx_phase_offset_byte2 - (Rx_Dll_Deskew_H_default[1] & 0x001F);
        MIU1_Phase_H_default[1] += (rx_phase_offset_byte2 - ((Rx_Dll_Deskew_H_default[1] & 0x1F00)>>8)) << 8;
        MIU1_Phase_H_default[2] = rx_phase_offset_byte2 - (Rx_Dll_Deskew_H_default[2] & 0x001F);
        MIU1_Phase_H_default[2] += (rx_phase_offset_byte2 - ((Rx_Dll_Deskew_H_default[2] & 0x1F00)>>8)) << 8;
        MIU1_Phase_H_default[3] = rx_phase_offset_byte2 - (Rx_Dll_Deskew_H_default[3] & 0x001F);
        MIU1_Phase_H_default[3] += (rx_phase_offset_byte2 - ((Rx_Dll_Deskew_H_default[3] & 0x1F00)>>8)) << 8;
        MIU1_Phase_H_default[4] = rx_phase_offset_byte3 - (Rx_Dll_Deskew_H_default[4] & 0x001F);
        MIU1_Phase_H_default[4] += (rx_phase_offset_byte3 - ((Rx_Dll_Deskew_H_default[4] & 0x1F00)>>8)) << 8;
        MIU1_Phase_H_default[5] = rx_phase_offset_byte3 - (Rx_Dll_Deskew_H_default[5] & 0x001F);
        MIU1_Phase_H_default[5] += (rx_phase_offset_byte3 - ((Rx_Dll_Deskew_H_default[5] & 0x1F00)>>8)) << 8;
        MIU1_Phase_H_default[6] = rx_phase_offset_byte3 - (Rx_Dll_Deskew_H_default[6] & 0x001F);
        MIU1_Phase_H_default[6] += (rx_phase_offset_byte3 - ((Rx_Dll_Deskew_H_default[6] & 0x1F00)>>8)) << 8;
        MIU1_Phase_H_default[7] = rx_phase_offset_byte3 - (Rx_Dll_Deskew_H_default[7] & 0x001F);
        MIU1_Phase_H_default[7] += (rx_phase_offset_byte3 - ((Rx_Dll_Deskew_H_default[7] & 0x1F00)>>8)) << 8;
    }
#else
        if(MiuDev == 0)
        {
            MIU0_Phase_L_default[0] = rx_phase_offset_byte0;
            MIU0_Phase_L_default[0] += (rx_phase_offset_byte0) << 8;
            MIU0_Phase_L_default[1] = rx_phase_offset_byte0;
            MIU0_Phase_L_default[1] += (rx_phase_offset_byte0) << 8;
            MIU0_Phase_L_default[2] = rx_phase_offset_byte0;
            MIU0_Phase_L_default[2] += (rx_phase_offset_byte0) << 8;
            MIU0_Phase_L_default[3] = rx_phase_offset_byte0;
            MIU0_Phase_L_default[3] += (rx_phase_offset_byte0) << 8;
            MIU0_Phase_L_default[4] = rx_phase_offset_byte1;
            MIU0_Phase_L_default[4] += (rx_phase_offset_byte1) << 8;
            MIU0_Phase_L_default[5] = rx_phase_offset_byte1;
            MIU0_Phase_L_default[5] += (rx_phase_offset_byte1) << 8;
            MIU0_Phase_L_default[6] = rx_phase_offset_byte1;
            MIU0_Phase_L_default[6] += (rx_phase_offset_byte1) << 8;
            MIU0_Phase_L_default[7] = rx_phase_offset_byte1;
            MIU0_Phase_L_default[7] += (rx_phase_offset_byte1) << 8;

            MIU0_Phase_H_default[0] = rx_phase_offset_byte2;
            MIU0_Phase_H_default[0] += (rx_phase_offset_byte2) << 8;
            MIU0_Phase_H_default[1] = rx_phase_offset_byte2;
            MIU0_Phase_H_default[1] += (rx_phase_offset_byte2) << 8;
            MIU0_Phase_H_default[2] = rx_phase_offset_byte2;
            MIU0_Phase_H_default[2] += (rx_phase_offset_byte2) << 8;
            MIU0_Phase_H_default[3] = rx_phase_offset_byte2;
            MIU0_Phase_H_default[3] += (rx_phase_offset_byte2) << 8;
            MIU0_Phase_H_default[4] = rx_phase_offset_byte3;
            MIU0_Phase_H_default[4] += (rx_phase_offset_byte3) << 8;
            MIU0_Phase_H_default[5] = rx_phase_offset_byte3;
            MIU0_Phase_H_default[5] += (rx_phase_offset_byte3) << 8;
            MIU0_Phase_H_default[6] = rx_phase_offset_byte3;
            MIU0_Phase_H_default[6] += (rx_phase_offset_byte3) << 8;
            MIU0_Phase_H_default[7] = rx_phase_offset_byte3;
            MIU0_Phase_H_default[7] += (rx_phase_offset_byte3) << 8;
        }
        else
        {
            MIU1_Phase_L_default[0] = rx_phase_offset_byte0;
            MIU1_Phase_L_default[0] += (rx_phase_offset_byte0) << 8;
            MIU1_Phase_L_default[1] = rx_phase_offset_byte0;
            MIU1_Phase_L_default[1] += (rx_phase_offset_byte0) << 8;
            MIU1_Phase_L_default[2] = rx_phase_offset_byte0;
            MIU1_Phase_L_default[2] += (rx_phase_offset_byte0) << 8;
            MIU1_Phase_L_default[3] = rx_phase_offset_byte0;
            MIU1_Phase_L_default[3] += (rx_phase_offset_byte0) << 8;
            MIU1_Phase_L_default[4] = rx_phase_offset_byte1;
            MIU1_Phase_L_default[4] += (rx_phase_offset_byte1) << 8;
            MIU1_Phase_L_default[5] = rx_phase_offset_byte1;
            MIU1_Phase_L_default[5] += (rx_phase_offset_byte1) << 8;
            MIU1_Phase_L_default[6] = rx_phase_offset_byte1;
            MIU1_Phase_L_default[6] += (rx_phase_offset_byte1) << 8;
            MIU1_Phase_L_default[7] = rx_phase_offset_byte1;
            MIU1_Phase_L_default[7] += (rx_phase_offset_byte1) << 8;

            MIU1_Phase_H_default[0] = rx_phase_offset_byte2;
            MIU1_Phase_H_default[0] += (rx_phase_offset_byte2) << 8;
            MIU1_Phase_H_default[1] = rx_phase_offset_byte2;
            MIU1_Phase_H_default[1] += (rx_phase_offset_byte2) << 8;
            MIU1_Phase_H_default[2] = rx_phase_offset_byte2;
            MIU1_Phase_H_default[2] += (rx_phase_offset_byte2) << 8;
            MIU1_Phase_H_default[3] = rx_phase_offset_byte2;
            MIU1_Phase_H_default[3] += (rx_phase_offset_byte2) << 8;
            MIU1_Phase_H_default[4] = rx_phase_offset_byte3;
            MIU1_Phase_H_default[4] += (rx_phase_offset_byte3) << 8;
            MIU1_Phase_H_default[5] = rx_phase_offset_byte3;
            MIU1_Phase_H_default[5] += (rx_phase_offset_byte3) << 8;
            MIU1_Phase_H_default[6] = rx_phase_offset_byte3;
            MIU1_Phase_H_default[6] += (rx_phase_offset_byte3) << 8;
            MIU1_Phase_H_default[7] = rx_phase_offset_byte3;
            MIU1_Phase_H_default[7] += (rx_phase_offset_byte3) << 8;
         }
#endif
    }
    else //if(ScanCmd == MIU_SCAN_CMD_PHASE)
    {
        putk_miu_scan('C');
        putk_miu_scan('m');
        putk_miu_scan('d');
    }

    putk_miu_scan(' ');
    if(ScanCmd != MIU_SCAN_CMD_PHASE)
    {
        putk_miu_scan('D');
        putk_miu_scan('Q');
        putk_miu_scan(' ');
    }
    putk_miu_scan('f');
    putk_miu_scan('o');
    putk_miu_scan('r');
    putk_miu_scan(' ');
    putk_miu_scan('M');
    putk_miu_scan('I');
    putk_miu_scan('U');
    putn_miu_scan(MiuDev);
    putk_miu_scan('\r');
    putk_miu_scan('\n');

    if(isHWord == MIU_SCAN_LOW_WORD)
    {
        putk_miu_scan('L');
        putk_miu_scan('o');
        putk_miu_scan('w');
    }
    else
    {
        putk_miu_scan('H');
        putk_miu_scan('i');
        putk_miu_scan('g');
        putk_miu_scan('h');
    }


    putk_miu_scan(' ');

    putk_miu_scan('W');
    putk_miu_scan('o');
    putk_miu_scan('r');
    putk_miu_scan('d');
    putk_miu_scan('\n');
    putk_miu_scan('\r');

    if(ScanCmd == MIU_SCAN_CMD_PHASE)
    {
        putk_miu_scan('s');
        putk_miu_scan('k');
        putk_miu_scan('e');
        putk_miu_scan('w');
        putk_miu_scan('+');

        if(isHWord == MIU_SCAN_LOW_WORD)
        {
            if(MiuDev == 0)
                default_skew = (CMD_MIU0_Ori_Skew & 0x0007);
            else
                default_skew = (CMD_MIU1_Ori_Skew & 0x0007);
        }
        else
        {
            if(MiuDev == 0)
                default_skew = (CMD_MIU0_Ori_Skew & 0x0070) >> 4;
            else
                default_skew = (CMD_MIU1_Ori_Skew & 0x0070) >> 4;
        }

        //default_skew = default_skew - 1;
        putn_miu_scan(default_skew);
        putk_miu_scan(',');

        if(isHWord == MIU_SCAN_LOW_WORD)
        {
            if(MiuDev == 0)
                default_phase = CMD_MIU0_Ori_Phase & 0x001f;
            else
                default_phase = CMD_MIU1_Ori_Phase & 0x001f;
        }
        else
        {
            if(MiuDev == 0)
                default_phase = (CMD_MIU0_Ori_Phase & 0x1f00) >> 8;
            else
                default_phase = (CMD_MIU1_Ori_Phase & 0x1f00) >> 8;
        }

        print_miu_scan(default_phase);

        putk_miu_scan('\r');
        putk_miu_scan('\n');
    }
    else
    {
        for(dq_counter=0;dq_counter < 16; dq_counter++)
        {
            putk_miu_scan('D');
            putk_miu_scan('Q');
            putk_miu_scan('[');

            print_miu_scan(dq_counter);

            putk_miu_scan(']');
            putk_miu_scan(' ');
            putk_miu_scan(':');
            putk_miu_scan(' ');

            if(ScanCmd == MIU_SCAN_WRITE_PHASE)
            {
	        putk_miu_scan('s');
    	    	putk_miu_scan('k');
		putk_miu_scan('e');
		putk_miu_scan('w');
		putk_miu_scan('+');
		if(isHWord == MIU_SCAN_LOW_WORD)
    	    	{
        	    if(dq_counter < 4)
		    {
		        if(MiuDev == 0)
			    default_skew =  (MIU0_OriSkew_L & 0x000f) ;
			else
			    default_skew =  (MIU1_OriSkew_L & 0x000f) ;
                    }
		    else if (dq_counter < 8)
		    {
		        if(MiuDev == 0)
		  	    default_skew =  (MIU0_OriSkew_L & 0x00f0) >> 4;
			else
			    default_skew =  (MIU1_OriSkew_L & 0x00f0) >> 4;
		    }
		    else if (dq_counter < 12)
		    {
		        if(MiuDev == 0)
		  	    default_skew =  (MIU0_OriSkew_L & 0x0f00) >> 8;
			else
			    default_skew =  (MIU1_OriSkew_L & 0x0f00) >> 8;
		    }
		    else if (dq_counter < 16)
		    {
 			if(MiuDev == 0)
		  	    default_skew =  (MIU0_OriSkew_L & 0xf000) >> 12;
			else
			    default_skew =  (MIU1_OriSkew_L & 0xf000) >> 12;
		    }
    	    	}
		else
                {

		    if(dq_counter < 4)
		    {
			if(MiuDev == 0)
			   default_skew =  (MIU0_OriSkew_H & 0x000f) ;
			else
			   default_skew =  (MIU1_OriSkew_H & 0x000f) ;
		    }
		    else if (dq_counter < 8)
		    {
		        if(MiuDev == 0)
		  	    default_skew =  (MIU0_OriSkew_H & 0x00f0) >> 4;
			else
			    default_skew =  (MIU1_OriSkew_H & 0x00f0) >> 4;
		    }
		    else if (dq_counter < 12)
		    {
		        if(MiuDev == 0)
		  	    default_skew =  (MIU0_OriSkew_H & 0x0f00) >> 8;
			else
			    default_skew =  (MIU1_OriSkew_H & 0x0f00) >> 8;
		    }
		    else if (dq_counter < 16)
		    {
			if(MiuDev == 0)
		 	    default_skew =  (MIU0_OriSkew_H & 0xf000) >> 12;
			else
			    default_skew =  (MIU1_OriSkew_H & 0xf000) >> 12;
		    }
	        }
		default_skew = default_skew - 1;
	  	putn_miu_scan(default_skew);
		putk_miu_scan(',');

	}

	if(isHWord == MIU_SCAN_LOW_WORD)
	{
    	    if(MiuDev == 0)
            {
		if((dq_counter%2) == 0)
			default_phase = MIU0_Phase_L_default[(dq_counter >> 1)] & 0x00ff;
		else
			default_phase = (MIU0_Phase_L_default[(dq_counter >> 1)] & 0xff00) >> 8;
            }
	    else
	    {
		if((dq_counter%2) == 0)
			default_phase = MIU1_Phase_L_default[(dq_counter >> 1)] & 0x00ff;
		else
			default_phase = (MIU1_Phase_L_default[(dq_counter >> 1)] & 0xff00) >> 8;
	    }
	}
	else
	{
            if(MiuDev == 0)
	    {
		if((dq_counter%2) == 0)
			default_phase = MIU0_Phase_H_default[(dq_counter  >> 1)] & 0x00ff;
		else
			default_phase = (MIU0_Phase_H_default[(dq_counter >> 1)] & 0xff00) >> 8;
	    }
	    else
	    {
		if((dq_counter%2) == 0)
			default_phase = MIU1_Phase_H_default[(dq_counter >> 1)] & 0x00ff;
		else
			default_phase = (MIU1_Phase_H_default[(dq_counter >> 1)] & 0xff00) >> 8;
	    }
	}

	print_miu_scan(default_phase);

	putk_miu_scan('\r');
	putk_miu_scan('\n');
    }

   }

}



void BootRom_OutputInfo(u8 MiuDev, u8 ScanCmd, u8 isHWord, u8 ScanSkew, u8 isDone, u8 isComplete)
{
    if(isComplete == MIU_SCAN_FULL_COMPLETE)
    {
        putk_miu_scan('F');
        putk_miu_scan('u');
        putk_miu_scan('l');
        putk_miu_scan('l');
        putk_miu_scan(' ');
    }


    putk_miu_scan('S');
    putk_miu_scan('c');
    putk_miu_scan('a');
    putk_miu_scan('n');
    putk_miu_scan(' ');

    if(isComplete == MIU_SCAN_FULL_COMPLETE)
    {
            putk_miu_scan('C');
            putk_miu_scan('o');
            putk_miu_scan('m');
            putk_miu_scan('p');
            putk_miu_scan('l');
            putk_miu_scan('e');
            putk_miu_scan('t');
            putk_miu_scan('e');
    }
    else
    {
        if(isDone == MIU_SCAN_NOT_DONE)
        {
            putk_miu_scan('S');
            putk_miu_scan('t');
            putk_miu_scan('a');
            putk_miu_scan('r');
            putk_miu_scan('t');
        }
        else
        {
            putk_miu_scan('D');
            putk_miu_scan('o');
            putk_miu_scan('n');
            putk_miu_scan('e');
        }
    }

    putk_miu_scan(' ');

    putk_miu_scan('M');
    putk_miu_scan('I');
    putk_miu_scan('U');
    putn_miu_scan(MiuDev);
    putk_miu_scan(' ');

    if(ScanCmd == MIU_SCAN_WRITE_PHASE)
    {
        putk_miu_scan('W');
        putk_miu_scan('r');
        putk_miu_scan('i');
        putk_miu_scan('t');
        putk_miu_scan('e');

    }
    else if(ScanCmd == MIU_SCAN_READ_PHASE)
    {
        putk_miu_scan('R');
        putk_miu_scan('e');
        putk_miu_scan('a');
        putk_miu_scan('d');

    }
    else
    {
        putk_miu_scan('C');
        putk_miu_scan('m');
        putk_miu_scan('d');
    }

    putk_miu_scan(' ');

    putk_miu_scan('P');
    putk_miu_scan('h');
    putk_miu_scan('a');
    putk_miu_scan('s');
    putk_miu_scan('e');

    putk_miu_scan(' ');


    //if(isHWord == MIU_SCAN_LOW_WORD)
    //{
    //    putk_miu_scan("L");
    //    putk_miu_scan("o");
    //    putk_miu_scan("w");
    //}
    //else
    //{
    //    putk_miu_scan("H");
    //    putk_miu_scan("i");
    //    putk_miu_scan("g");
    //    putk_miu_scan("h");
    //}

    //putk_miu_scan("-");

    //putk_miu_scan('W');
    //putk_miu_scan('o');
    //putk_miu_scan('r');
    //putk_miu_scan('d');

    if(ScanSkew != MIU_SCAN_SKEW_NORMAL)
    {
        putk_miu_scan(' ');
        putk_miu_scan('S');
        putk_miu_scan('k');
        putk_miu_scan('e');
        putk_miu_scan('w');
        putk_miu_scan(' ');

        if(ScanSkew == MIU_SCAN_SKEW_MINUS)
            putk_miu_scan('-');
        else
            putk_miu_scan('+');

        putk_miu_scan(' ');
        putk_miu_scan('1');
    }

    putk_miu_scan('\n');
    putk_miu_scan('\r');

}


#if !MIU_SCAN_SSO_MODE
void BootRom_BistInit(u16 BistMode)
{

    //##### Disable protect setting ####
    Miu_Scan_WriteByteMask(0x1012ff, 0x08, 0x00); //[11] : reg_miu_test_off = 0

    //#### set BIST ####
    //[3:1] : Set BIST pattern mode
    //                      //[4]   : reg_test_loop
    //                      //@2017.07.04 : update BIST pattern : 7-mode, address toggle, Loop mode
    Miu_Scan_Write2Byte(0x1012e0, BistMode);

    //Memory BIST start address (S/W Jack will provide the non-use memory partition for BIST testing)
    Miu_Scan_Write2Byte(0x1012e2, 0x0000);

    Miu_Scan_Write2Byte(0x1012e4, 0x0200); //BIST test length (512K+16Byte)
    Miu_Scan_Write2Byte(0x1012e8, 0x00ff); //Test Data (valid when using BIST 3-mode)
}
#endif

#if MIU_SCAN_SSO_MODE
void BootRom_Pattern()
{
    u16 Count = 0;
    u32 Data = 0;

    //Miu_Scan_BDMAMoveData(0,&PRBS_Pattern,0,0x20000000,256);
    for(Count = 0; Count < 128; Count++)
     *(( volatile unsigned int* )(0x20000000 + Count*4)) = PRBS_Pattern[Count];

    for(Count = 0; Count < 128; Count++)
    {

        Data = *((volatile unsigned int* )(0x20000000 + Count*4));

	LW_Result = ((Data & 0xFFFF) ^ (PRBS_Pattern[Count]&0xFFFF));
	HW_Result = (((Data >> 16) & 0xFFFF) ^ ((PRBS_Pattern[Count]>>16)&0xFFFF));

    }
}
#endif

void BootRom_WritePhase(u8 MiuDev, u32 MiuPhase)
{
    u32 PhaseIdx = 0, loopCount = 0;
    u16 BistModeCount = 0;
    PhaseIdx = 0x0101 * MiuPhase;

    for(loopCount = 0; loopCount < MIU_SCAN_LOOP_COUNT; loopCount++)
    {
        //####### Mask all expect cmd
        if(MiuDev == 0)
            Miu_Scan_Write2Byte(0x1615e6,  0x00fe);
        else
            Miu_Scan_Write2Byte(0x1622e6,  0x00fe);

        delayus_miu_scan(1200); //wait 1ms

        if(MiuDev == 0)
        {
            //--- LW Write Phase setting ---
            Miu_Scan_Write2Byte(0x151520, PhaseIdx);
            Miu_Scan_Write2Byte(0x151522, PhaseIdx);
            Miu_Scan_Write2Byte(0x151524, PhaseIdx);
            Miu_Scan_Write2Byte(0x151526, PhaseIdx);
            Miu_Scan_Write2Byte(0x151528, PhaseIdx);
            Miu_Scan_Write2Byte(0x15152a, PhaseIdx);
            Miu_Scan_Write2Byte(0x15152c, PhaseIdx);
            Miu_Scan_Write2Byte(0x15152e, PhaseIdx);

        //--- HW Write Phase setting ---
            Miu_Scan_Write2Byte(0x151530, PhaseIdx);
            Miu_Scan_Write2Byte(0x151532, PhaseIdx);
            Miu_Scan_Write2Byte(0x151534, PhaseIdx);
            Miu_Scan_Write2Byte(0x151536, PhaseIdx);
            Miu_Scan_Write2Byte(0x151538, PhaseIdx);
            Miu_Scan_Write2Byte(0x15153a, PhaseIdx);
            Miu_Scan_Write2Byte(0x15153c, PhaseIdx);
            Miu_Scan_Write2Byte(0x15153e, PhaseIdx);
        }
        else
        {
            //--- LW Write Phase setting ---
            Miu_Scan_Write2Byte(0x151620, PhaseIdx);
            Miu_Scan_Write2Byte(0x151622, PhaseIdx);
            Miu_Scan_Write2Byte(0x151624, PhaseIdx);
            Miu_Scan_Write2Byte(0x151626, PhaseIdx);
            Miu_Scan_Write2Byte(0x151628, PhaseIdx);
            Miu_Scan_Write2Byte(0x15162a, PhaseIdx);
            Miu_Scan_Write2Byte(0x15162c, PhaseIdx);
            Miu_Scan_Write2Byte(0x15162e, PhaseIdx);

            //--- HW Write Phase setting ---
            Miu_Scan_Write2Byte(0x151630, PhaseIdx);
            Miu_Scan_Write2Byte(0x151632, PhaseIdx);
            Miu_Scan_Write2Byte(0x151634, PhaseIdx);
            Miu_Scan_Write2Byte(0x151636, PhaseIdx);
            Miu_Scan_Write2Byte(0x151638, PhaseIdx);
            Miu_Scan_Write2Byte(0x15163a, PhaseIdx);
            Miu_Scan_Write2Byte(0x15163c, PhaseIdx);
            Miu_Scan_Write2Byte(0x15163e, PhaseIdx);
        }
        //####### Un-Mask all
        if(MiuDev == 0)
            Miu_Scan_Write2Byte(0x1615e6,  0x0000);
        else
            Miu_Scan_Write2Byte(0x1622e6,  0x0000);

#if MIU_SCAN_SSO_MODE
        BootRom_Pattern();
#else
        for(BistModeCount = 0x10;BistModeCount <= 0x16; BistModeCount+= 0x02)
        {

            BootRom_BistInit(BistModeCount);
            delayus_miu_scan(1200);
            //####### Enable BIST
            Miu_Scan_WriteByteMask(0x1012e0, 0x01, 0x01); //

            delayus_miu_scan(1200);

            //####### Check pass/Fail

            LW_Result |= Miu_Scan_Read2Byte(0x1012a0); //bit_fail:LW [N] --> Bit N PASS/FAIL 0:PASS ; 1:FAIL
            HW_Result |= Miu_Scan_Read2Byte(0x1012a2); //bit_fail:HW [N] --> Bit N PASS/FAIL 0:PASS ; 1:FAIL

            //Disable BIST
            Miu_Scan_WriteByteMask(0x1012e0, 0x01, 0x00);
            delayus_miu_scan(1200);
        }
#endif
    }
    if(MiuDev == 0)
    {
        MIU0_Phase_L_Result[MiuPhase] = LW_Result;
        MIU0_Phase_H_Result[MiuPhase] = HW_Result;
    }
    else
    {
        MIU1_Phase_L_Result[MiuPhase] = LW_Result;
        MIU1_Phase_H_Result[MiuPhase] = HW_Result;
    }
}

void BootRom_ChangeSkew(u8 MiuDev, u16 OriSkew_L, u16 OriSkew_H)
{
    if(MiuDev == 0)
    {
        //####### Mask all expect cmd
        Miu_Scan_Write2Byte(0x1615e6,  0x00fe);
        delayus_miu_scan(1200); //wait 1ms
        Miu_Scan_Write2Byte(0x110dec, OriSkew_L + 0x1111);
        Miu_Scan_Write2Byte(0x110dee, OriSkew_H + 0x1111);
        //####### Un-Mask all
        Miu_Scan_Write2Byte(0x1615e6,  0x0000);
    }
    else
    {
        //####### Mask all expect cmd
        Miu_Scan_Write2Byte(0x1622e6,  0x00fe);
        delayus_miu_scan(1200); //wait 1ms
        Miu_Scan_Write2Byte(0x1616ec, OriSkew_L + 0x1111);
        Miu_Scan_Write2Byte(0x1616ee, OriSkew_H + 0x1111);
        //####### Un-Mask all
        Miu_Scan_Write2Byte(0x1622e6,  0x0000);
    }

}

void BootRom_SetBackWritePhase(u8 MiuDev, u16 OriSkew_L, u16 OriSkew_H)
{

    if(MiuDev == 0)
    {
        //####### Mask all expect cmd
        Miu_Scan_Write2Byte(0x1615e6,  0x00fe);

        delayus_miu_scan(1200); //wait 1ms

        //Original Skew
        Miu_Scan_Write2Byte(0x110dec, OriSkew_L); // 0x110dec[15:0] = {org_dq_byte3_skew  ,org_dq_byte2_skew  ,org_dq_byte1_skew  ,org_dq_byte0_skew }
        Miu_Scan_Write2Byte(0x110dee, OriSkew_H); // 0x110dee[15:0] = {org_oen_byte3_skew ,org_oen_byte2_skew ,org_oen_byte1_skew ,org_oen_byte0_skew}


        //Original Phase
        //--- LW Write Phase setting ---
        Miu_Scan_Write2Byte(0x151520, MIU0_Phase_L_default[0]);
        Miu_Scan_Write2Byte(0x151522, MIU0_Phase_L_default[1]);
        Miu_Scan_Write2Byte(0x151524, MIU0_Phase_L_default[2]);
        Miu_Scan_Write2Byte(0x151526, MIU0_Phase_L_default[3]);
        Miu_Scan_Write2Byte(0x151528, MIU0_Phase_L_default[4]);
        Miu_Scan_Write2Byte(0x15152a, MIU0_Phase_L_default[5]);
        Miu_Scan_Write2Byte(0x15152c, MIU0_Phase_L_default[6]);
        Miu_Scan_Write2Byte(0x15152e, MIU0_Phase_L_default[7]);

        //--- HW Write Phase setting ---
        Miu_Scan_Write2Byte(0x151530, MIU0_Phase_H_default[0]);
        Miu_Scan_Write2Byte(0x151532, MIU0_Phase_H_default[1]);
        Miu_Scan_Write2Byte(0x151534, MIU0_Phase_H_default[2]);
        Miu_Scan_Write2Byte(0x151536, MIU0_Phase_H_default[3]);
        Miu_Scan_Write2Byte(0x151538, MIU0_Phase_H_default[4]);
        Miu_Scan_Write2Byte(0x15153a, MIU0_Phase_H_default[5]); //0620
        Miu_Scan_Write2Byte(0x15153c, MIU0_Phase_H_default[6]); //0620
        Miu_Scan_Write2Byte(0x15153e, MIU0_Phase_H_default[7]);//0620

        //####### Un-Mask all
        Miu_Scan_Write2Byte(0x1615e6,  0x0000);
    }
    else
    {
        //####### Mask all expect cmd
        Miu_Scan_Write2Byte(0x1622e6,  0x00fe);

        delayus_miu_scan(1200); //wait 1ms

        //Original Skew
        Miu_Scan_Write2Byte(0x1616ec, OriSkew_L); // 0x1616ec[15:0] = {org_dq_byte3_skew  ,org_dq_byte2_skew  ,org_dq_byte1_skew  ,org_dq_byte0_skew }
        Miu_Scan_Write2Byte(0x1616ee, OriSkew_H); // 0x1616ee[15:0] = {org_oen_byte3_skew ,org_oen_byte2_skew ,org_oen_byte1_skew ,org_oen_byte0_skew}


        //Original Phase
        //--- LW Write Phase setting ---
        Miu_Scan_Write2Byte(0x151620, MIU1_Phase_L_default[0]);
        Miu_Scan_Write2Byte(0x151622, MIU1_Phase_L_default[1]);
        Miu_Scan_Write2Byte(0x151624, MIU1_Phase_L_default[2]);
        Miu_Scan_Write2Byte(0x151626, MIU1_Phase_L_default[3]);
        Miu_Scan_Write2Byte(0x151628, MIU1_Phase_L_default[4]);
        Miu_Scan_Write2Byte(0x15162a, MIU1_Phase_L_default[5]);
        Miu_Scan_Write2Byte(0x15162c, MIU1_Phase_L_default[6]);
        Miu_Scan_Write2Byte(0x15162e, MIU1_Phase_L_default[7]);

        //--- HW Write Phase setting ---
        Miu_Scan_Write2Byte(0x151630, MIU1_Phase_H_default[0]);
        Miu_Scan_Write2Byte(0x151632, MIU1_Phase_H_default[1]);
        Miu_Scan_Write2Byte(0x151634, MIU1_Phase_H_default[2]);
        Miu_Scan_Write2Byte(0x151636, MIU1_Phase_H_default[3]);
        Miu_Scan_Write2Byte(0x151638, MIU1_Phase_H_default[4]);
        Miu_Scan_Write2Byte(0x15163a, MIU1_Phase_H_default[5]);
        Miu_Scan_Write2Byte(0x15163c, MIU1_Phase_H_default[6]);
        Miu_Scan_Write2Byte(0x15163e, MIU1_Phase_H_default[7]);

        //####### Un-Mask all
        Miu_Scan_Write2Byte(0x1622e6,  0x0000);
    }

}

//Set Read phase control path to RIU control
void BootRom_SetReadPhaseControl(u8 MiuDev)
{
    if(MiuDev == 0)
    {
        //####### Mask all expect cmd
        Miu_Scan_Write2Byte(0x1615e6,  0x00fe);

        delayus_miu_scan(1200); //wait 1ms

        //gated control
        Miu_Scan_WriteByteMask(0x110d71, 0x08, 0x00); //[11]: set to 0

        Miu_Scan_WriteByteMask(0x110d90, 0x03, 0x00); //[1:0] = 0 Disable the hardware auto read DQS phase

        //gated control
        Miu_Scan_WriteByteMask(0x110d71, 0x08, 0x08); //[11]: set to 1

        //####### Un-Mask all
        Miu_Scan_Write2Byte(0x1615e6, 0x0000);
    }
    else
    {
        //####### Mask all expect cmd
        Miu_Scan_Write2Byte(0x1622e6, 0x00fe);

        delayus_miu_scan(1200); //wait 1ms

        //gated control
        Miu_Scan_WriteByteMask(0x161671, 0x08, 0x00); //[11]: set to 0

        Miu_Scan_WriteByteMask(0x161690, 0x03, 0x00); //[1:0] = 0 Disable the hardware auto read DQS phase

        //gated control
        Miu_Scan_WriteByteMask(0x161671, 0x08, 0x08); //[11]: set to 1

        //####### Un-Mask all
        Miu_Scan_Write2Byte(0x1622e6, 0x0000);
    }



}

void BootRom_ReadPhase(u8 MiuDev, u32 MiuPhase)
{
    u32 PhaseIdx = 0, loopCount = 0;
    u16 BistModeCount = 0;

    PhaseIdx = 0x0101 * MiuPhase;

    for(loopCount = 0; loopCount < MIU_SCAN_LOOP_COUNT; loopCount++)
    {
        if(MiuDev == 0)
        {
            //####### Mask all expect cmd
            Miu_Scan_Write2Byte(0x1615e6, 0x00fe);

            delayus_miu_scan(1200); //wait 1ms

            Miu_Scan_WriteByteMask(0x110d71, 0x08, 0x00); //gated control //[11]: set to 0
            Miu_Scan_Write2Byte(0x151564, PhaseIdx); //<----------------------------------------------- LW Read DQS phase
            Miu_Scan_Write2Byte(0x151566, PhaseIdx); //<----------------------------------------------- HW Read DQS phase
            Miu_Scan_Write2Byte(0x151568, PhaseIdx); //<----------------------------------------------- LW Read DQS phase
            Miu_Scan_Write2Byte(0x15156a, PhaseIdx); //<----------------------------------------------- HW Read DQS phase
            Miu_Scan_WriteByteMask(0x110d71, 0x08, 0x08); //gated control //[11]: set to 1

            //####### Un-Mask all
            Miu_Scan_Write2Byte(0x1615e6, 0x0000);
        }
        else
        {
            //####### Mask all expect cmd
            Miu_Scan_Write2Byte(0x1622e6, 0x00fe);

            delayus_miu_scan(1200); //wait 1ms

            Miu_Scan_WriteByteMask(0x161671, 0x08, 0x00); //gated control //[11]: set to 0
            Miu_Scan_Write2Byte(0x151664, PhaseIdx); //<----------------------------------------------- LW Read DQS phase
            Miu_Scan_Write2Byte(0x151666, PhaseIdx); //<----------------------------------------------- HW Read DQS phase
            Miu_Scan_Write2Byte(0x151668, PhaseIdx); //<----------------------------------------------- LW Read DQS phase
            Miu_Scan_Write2Byte(0x15166a, PhaseIdx); //<----------------------------------------------- HW Read DQS phase
            Miu_Scan_WriteByteMask(0x161671, 0x08, 0x08); //gated control //[11]: set to 1

            //####### Un-Mask all
            Miu_Scan_Write2Byte(0x1622e6, 0x0000);
        }

#if MIU_SCAN_SSO_MODE
        BootRom_Pattern();
#else
		for(BistModeCount = 0x10;BistModeCount <= 0x16; BistModeCount+= 0x02)
		{
			BootRom_BistInit(BistModeCount);

        	//####### Enable BIST
			Miu_Scan_WriteByteMask(0x1012e0, 0x01, 0x01); //

        	delayus_miu_scan(1200);

        	//####### Check pass/Fail

       		LW_Result |= Miu_Scan_Read2Byte(0x1012a4); //bit_fail:LW [N] --> Bit N PASS/FAIL 0:PASS ; 1:FAIL
        	HW_Result |= Miu_Scan_Read2Byte(0x1012a6); //bit_fail:HW [N] --> Bit N PASS/FAIL 0:PASS ; 1:FAIL

			//Disable BIST
			Miu_Scan_WriteByteMask(0x1012e0, 0x01, 0x00);
		}
#endif
    }
    if(MiuDev == 0)
    {
        MIU0_Phase_L_Result[MiuPhase] = LW_Result;
        MIU0_Phase_H_Result[MiuPhase] = HW_Result;
    }
    else
    {
        MIU1_Phase_L_Result[MiuPhase] = LW_Result;
        MIU1_Phase_H_Result[MiuPhase] = HW_Result;
    }

        //SW log & display DQ0-31 pass/Fail info.

}


void BootRom_SetBackReadPhase(u8 MiuDev)
{

    if(MiuDev == 0)
    {
        //####### Mask all expect cmd
        Miu_Scan_Write2Byte(0x1615e6, 0x00fe);

        delayus_miu_scan(1200); //wait 1ms

        Miu_Scan_WriteByteMask(0x110d71, 0x08, 0x00); //gated control //[11]: set to 0

        Miu_Scan_WriteByteMask(0x110d90, 0x03, 0x03); //[1:0] = 2'b11 Enable the hardware auto read DQS phase

        Miu_Scan_WriteByteMask(0x110d71, 0x08, 0x08); //gated control //[11]: set to 1

        //####### Un-Mask all
        Miu_Scan_Write2Byte(0x1615e6, 0x0000);
    }
    else
    {
        //####### Mask all expect cmd
        Miu_Scan_Write2Byte(0x1622e6, 0x00fe);

        delayus_miu_scan(1200); //wait 1ms

        Miu_Scan_WriteByteMask(0x161671, 0x08, 0x00); //gated control //[11]: set to 0

        Miu_Scan_WriteByteMask(0x161690, 0x03, 0x03); //[1:0] = 2'b11 Enable the hardware auto read DQS phase

        Miu_Scan_WriteByteMask(0x161671, 0x08, 0x08); //gated control //[11]: set to 1

        //####### Un-Mask all
        Miu_Scan_Write2Byte(0x1622e6, 0x0000);

    }

}

void BootRom_MIU_ReInit(u8 MiuDev)
{
    delayus_miu_scan(1200); // wait 1 ms
    //####### SW Reset
    Miu_Scan_Write2Byte(0x10121e,0x8009);
    Miu_Scan_Write2Byte(0x10061e,0x8001);
    delayus_miu_scan(1200); // wait 1 ms

    //Refine SW reset sequence
    if(MiuDev)
    {
        Miu_Scan_Write2Byte(0x100600, 0x0000);
    }
    else
    {
        Miu_Scan_Write2Byte(0x101200, 0x0000);
    }
    //####### SW Reset
    delayus_miu_scan(1200); // wait 1 ms
    Miu_Scan_Write2Byte(0x10121e,0x8008);
    Miu_Scan_Write2Byte(0x10061e,0x8000);
    delayus_miu_scan(1200); // wait 1 ms


    if(MiuDev)
    {
        //---------------------
        // begin init DRAM
        //---------------------
        Miu_Scan_Write2Byte(0x100600, 0x0000);
        //-----Wait 200us, (keep DRAM RESET = 0)-----
        delayus_miu_scan(1200);
        Miu_Scan_Write2Byte(0x100600, 0x0008);
        Miu_Scan_Write2Byte(0x100600, 0x000c);
        //-----Wait 500us, (keep DRAM RESET = 1 , CKE = 0)-----
        delayus_miu_scan(1200);
        Miu_Scan_Write2Byte(0x100600, 0x000e);
        //-----Wait tXPR=400ns-----
        delayus_miu_scan(1200);
        //--------Initial DRAM start here!!!
        Miu_Scan_Write2Byte(0x100600, 0x001f);
        //-----Wait init done-----
        delayus_miu_scan(1200);
        //--------Initial Done
        //--------DDR2 wait 400ns for tXPR(?), DDR3 wait 512T for tZQinit
        delayus_miu_scan(1200);

    }
    else
    {
        //---------------------
        // begin init DRAM
        //---------------------
        Miu_Scan_Write2Byte(0x101200, 0x0000);
        //-----Wait 200us, (keep DRAM RESET = 0)-----
        delayus_miu_scan(1200);
        Miu_Scan_Write2Byte(0x101200, 0x0008);
        Miu_Scan_Write2Byte(0x101200, 0x000c);
        //-----Wait 500us, (keep DRAM RESET = 1 , CKE = 0)-----
        delayus_miu_scan(1200);
        Miu_Scan_Write2Byte(0x101200, 0x000e);
        //-----Wait tXPR=400ns-----
        delayus_miu_scan(1200);
        //--------Initial DRAM start here!!!
        Miu_Scan_Write2Byte(0x101200, 0x001f);
        //-----Wait init done-----
        delayus_miu_scan(1200);
        //--------Initial Done
        //--------DDR2 wait 400ns for tXPR(?), DDR3 wait 512T for tZQinit
        delayus_miu_scan(1200);
    }

    //####### SW Reset
    //Miu_Scan_Write2Byte(0x10061e,0x8000);
    //Miu_Scan_Write2Byte(0x10121e,0x8008);

    delayus_miu_scan(1200); // wait 1 ms
}

void BootRom_CmdPhase(u8 MiuDev, u16 New_Miu_Skew, u8 MiuPhase)
{
    u32 PhaseIdx = 0, loopCount =0;
    u16 BistModeCount = 0;


    PhaseIdx = 0x0101 * MiuPhase;

    //Change phase and reinit MIU
    {
        //####### Mask all expect cmd
        if(MiuDev == 0)
            Miu_Scan_Write2Byte(0x1615e6,  0x00fe);
        else
            Miu_Scan_Write2Byte(0x1622e6,  0x00fe);

        delayus_miu_scan(1200); //wait 1ms


        if(MiuDev == 0)
        {
            //Change CKO phase
            Miu_Scan_Write2Byte(0x15150c, PhaseIdx);          //[12:8] : reg_clkph_cko1 = 0 for HW, [4:0] : reg_clkph_cko0 = 0 for LW
            Miu_Scan_WriteByte(0x110de0, New_Miu_Skew);       // [6:4] : reg_cko1_skew  = 0 for HW, [2:0] : reg_cko0_skew  = 0 for LW

        }
        else
        {
            //Change CKO phase
            Miu_Scan_Write2Byte(0x15160c, PhaseIdx);          //[12:8] : reg_clkph_cko1 = 0 for HW, [4:0] : reg_clkph_cko0 = 0 for LW
            Miu_Scan_WriteByte(0x1616e0, New_Miu_Skew); 	// [6:4] : reg_cko1_skew  = 0 for HW, [2:0] : reg_cko0_skew  = 0 for LW
        }

        delayus_miu_scan(1200); //wait 1ms


        //Re-init MIU
        //if (u8IsFirstTimeInit)
        {
            BootRom_MIU_ReInit(MiuDev);
            delayus_miu_scan(1200); //wait 1ms
            //u8IsFirstTimeInit = 0;
        }

        //####### Un-Mask all
        if(MiuDev == 0)
            Miu_Scan_Write2Byte(0x1615e6,  0x0000);
        else
            Miu_Scan_Write2Byte(0x1622e6,  0x0000);

        delayus_miu_scan(1200); //wait 1ms
    }

    for(loopCount = 0; loopCount < MIU_SCAN_LOOP_COUNT; loopCount++)
    {

#if MIU_SCAN_SSO_MODE
        BootRom_Pattern();
#else
        for(BistModeCount = 0x10;BistModeCount <= 0x16; BistModeCount+= 0x02)
        {

            BootRom_BistInit(BistModeCount);

            delayus_miu_scan(1200); //wait 1ms
            //####### Enable BIST
            Miu_Scan_WriteByteMask(0x1012e0, 0x01, 0x01); //

            delayus_miu_scan(1200);

            //####### Check pass/Fail

            LW_Result |= Miu_Scan_Read2Byte(0x1012a0);  //bit_fail:LW [N] --> Bit N PASS/FAIL 0:PASS ; 1:FAIL
            HW_Result |= Miu_Scan_Read2Byte(0x1012a2);  //bit_fail:HW [N] --> Bit N PASS/FAIL 0:PASS ; 1:FAIL

            //Disable BIST
            Miu_Scan_WriteByteMask(0x1012e0, 0x01, 0x00);
            delayus_miu_scan(1200); //wait 1ms
        }

        if(LW_Result != 0 || HW_Result != 0)
        {
            //BootRom_MIU_ReInit(MiuDev);
            break;
        }
#endif
    }

    if(MiuDev == 0)
    {
        MIU0_Phase_L_Result[MiuPhase] = LW_Result;
        MIU0_Phase_H_Result[MiuPhase] = HW_Result;
    }
    else
    {
        MIU1_Phase_L_Result[MiuPhase] = LW_Result;
        MIU1_Phase_H_Result[MiuPhase] = HW_Result;
    }
        //SW log & display all Byte pass/Fail info.
}

void BootRom_Restore_CmdPhase(u8 MiuDev, u16 Ori_Miu_Skew, u16 Ori_MiuPhase)
{

    //####### Mask all expect cmd
    if(MiuDev == 0)
        Miu_Scan_Write2Byte(0x1615e6,  0x00fe);
    else
        Miu_Scan_Write2Byte(0x1622e6,  0x00fe);

    delayus_miu_scan(1200); //wait 1ms

    if(MiuDev == 0)
    {
        //Change CKO phase
        Miu_Scan_WriteByte(0x110de0, Ori_Miu_Skew);  	// [6:4] : reg_cko1_skew  = 0 for HW, [2:0] : reg_cko0_skew  = 0 for LW
        Miu_Scan_Write2Byte(0x15150c, Ori_MiuPhase);          //[12:8] : reg_clkph_cko1 = 0 for HW, [4:0] : reg_clkph_cko0 = 0 for LW

    }
    else
    {
        //Change CKO phase
        Miu_Scan_WriteByte(0x1616e0, Ori_Miu_Skew); 	// [6:4] : reg_cko1_skew  = 0 for HW, [2:0] : reg_cko0_skew  = 0 for LW
        Miu_Scan_Write2Byte(0x15160c, Ori_MiuPhase);          //[12:8] : reg_clkph_cko1 = 0 for HW, [4:0] : reg_clkph_cko0 = 0 for LW
    }

    //Re-init DDR
    BootRom_MIU_ReInit(MiuDev);

    //####### Un-Mask all
    if(MiuDev == 0)
        Miu_Scan_Write2Byte(0x1615e6,  0x0000);
    else
        Miu_Scan_Write2Byte(0x1622e6,  0x0000);


}

void BoorRom_InformationCheck(void)
{
    char tmpInput = 0;

    putk_miu_scan('M');
    putk_miu_scan('I');
    putk_miu_scan('U');
    putk_miu_scan('_');
    putk_miu_scan('S');
    putk_miu_scan('C');
    putk_miu_scan('A');
    putk_miu_scan('N');
    putk_miu_scan('-');
    putk_miu_scan('\n');
    putk_miu_scan('\r');

    putk_miu_scan('I');
    putk_miu_scan('N');
    putk_miu_scan('P');
    putk_miu_scan('U');
    putk_miu_scan('T');
    putk_miu_scan('[');
    putk_miu_scan('S');
    putk_miu_scan(']');
    putk_miu_scan(':');


    while(1)
    {
        tmpInput = getc_miu_scan();
        if(tmpInput == 'S')
        {
            DoReadPhase = 1;
            DoWritePhase = 1;
            DoCmdPhase = 1;
            break;
        }

        if(tmpInput == 'P')
        {
            SkipMiuTest = 1;
            break;
        }

#if MIU_SCAN_DEBUG
        if(tmpInput == 'R')
        {
            DoReadPhase = 1;
            break;
        }
        else if(tmpInput == 'W')
        {
            DoWritePhase = 1;
            break;
        }
        else if(tmpInput == 'C')
        {
            DoCmdPhase = 1;
            break;
        }
#endif
    }

    if(SkipMiuTest == 0)
    {

        putk_miu_scan('S');
        putk_miu_scan('\n');
        putk_miu_scan('\r');

        putk_miu_scan('T');
        putk_miu_scan('E');
        putk_miu_scan('S');
        putk_miu_scan('T');
        putk_miu_scan(' ');
        putk_miu_scan('T');
        putk_miu_scan('I');
        putk_miu_scan('M');
        putk_miu_scan('E');
        putk_miu_scan('S');
        putk_miu_scan(':');
        putk_miu_scan(' ');


        putk_miu_scan('[');
        putk_miu_scan('1');
        putk_miu_scan(']');
        putk_miu_scan('=');
        putk_miu_scan('>');
        putk_miu_scan('1');
        putk_miu_scan('5');
        putk_miu_scan('0');
        putk_miu_scan(',');

        putk_miu_scan('[');
        putk_miu_scan('2');
        putk_miu_scan(']');
        putk_miu_scan('=');
        putk_miu_scan('>');
        putk_miu_scan('1');
        putk_miu_scan('0');
        putk_miu_scan('0');
        putk_miu_scan('0');
        putk_miu_scan(',');

        putk_miu_scan('[');
        putk_miu_scan('3');
        putk_miu_scan(']');
        putk_miu_scan('=');
        putk_miu_scan('>');
        putk_miu_scan('6');
        putk_miu_scan('0');
        putk_miu_scan('0');
        putk_miu_scan('0');
        putk_miu_scan(' ');

        putk_miu_scan(':');


        tmpInput = 0;

        while(1)
        {
            tmpInput = getc_miu_scan();
            if(tmpInput == '1')
            {
                MIU_SCAN_LOOP_COUNT = 150;

                putk_miu_scan('1');
                putk_miu_scan('5');
                putk_miu_scan('0');

                break;
            }

            if(tmpInput == '2')
            {
                MIU_SCAN_LOOP_COUNT = 1000;

                putk_miu_scan('1');
                putk_miu_scan('0');
                putk_miu_scan('0');
                putk_miu_scan('0');

                break;
            }

            if(tmpInput == '3')
            {
                MIU_SCAN_LOOP_COUNT = 6000;

                putk_miu_scan('6');
                putk_miu_scan('0');
                putk_miu_scan('0');
                putk_miu_scan('0');

                break;
            }
        }
    }
    else
    {
        putk_miu_scan('P');
    }

    putk_miu_scan('\n');
    putk_miu_scan('\r');


}

void BootRom_MiuScan(void)
{
    u8 Count_WritePhase = 0, Count_CmdPhase = 0;
    u16 ReadPhase_index = 0;

    SkipMiuTest = 0;
    MIU_SCAN_LOOP_COUNT = 50;
    BoorRom_InformationCheck();
//  BootRom_OutputInfo(MIU_SCAN_MIU0,           MIU_SCAN_WRITE_PHASE,   MIU_SCAN_LOW_WORD,
//                        MIU_SCAN_SKEW_NORMAL,    MIU_SCAN_NOT_DONE,      MIU_SCAN_NOT_FULL_COMPLETE);

#if 0
    Miu_Scan_Write2Byte(0x103380, 0x1111);
    Miu_Scan_Write2Byte(0x103384, 0x1111);



    if(Miu_Scan_Read2Byte(0x103380) == 0x1111)
        putk_miu_scan('O');
    else
        putk_miu_scan('N');

    putk_miu_scan('\n');
    putk_miu_scan('\r');

    Miu_Scan_Write2Byte(0x103382, Miu_Scan_ReadByte(0x103380));
    Miu_Scan_WriteByte(0x103383, 0x99);

    Miu_Scan_WriteByteMask(0x103384, 0x1, 0);
    Miu_Scan_WriteByteMask(0x103385, 0x1, 0);

    //while(1);

    if(Miu_Scan_ReadByte(0x103380) == 0x11)
        putk_miu_scan('O');
    else
        putk_miu_scan('N');

    putk_miu_scan('\n');
    putk_miu_scan('\r');

    if(Miu_Scan_ReadByte(0x103381) == 0x11)
        putk_miu_scan('O');
    else
        putk_miu_scan('N');

    putk_miu_scan('\n');
    putk_miu_scan('\r');

    Miu_Scan_WriteByteMask(0x103380, 0x1, 0);

    if(Miu_Scan_Read2Byte(0x103380) == 0x1110)
        putk_miu_scan('O');
    else
        putk_miu_scan('N');

    putk_miu_scan('\n');
    putk_miu_scan('\r');

    Miu_Scan_WriteByteMask(0x103380, 0x10, 0);

    if(Miu_Scan_Read2Byte(0x103380) == 0x1100)
        putk_miu_scan('O');
    else
        putk_miu_scan('N');

    putk_miu_scan('\n');
    putk_miu_scan('\r');

    Miu_Scan_WriteByteMask(0x103381, 0x01, 0);

    if(Miu_Scan_Read2Byte(0x103380) == 0x1000)
        putk_miu_scan('O');
    else
        putk_miu_scan('N');

    putk_miu_scan('\n');
    putk_miu_scan('\r');

    Miu_Scan_WriteByteMask(0x103381, 0x10, 0);

    if(Miu_Scan_Read2Byte(0x103380) == 0x0000)
        putk_miu_scan('O');
    else
        putk_miu_scan('N');

    putk_miu_scan('\n');
    putk_miu_scan('\r');
#endif


    if(SkipMiuTest == 0)
    {
            //Save MIU0 original skew
            MIU0_OriSkew_L = Miu_Scan_Read2Byte(0x110dec);
            MIU0_OriSkew_H = Miu_Scan_Read2Byte(0x110dee);
            //Save MIU0/1 default Phase
            //MIU0 Original Phase
            //--- Read Write Phase setting ---
            MIU0_Phase_L_default[0] = Miu_Scan_Read2Byte(0x151520);
            MIU0_Phase_L_default[1] = Miu_Scan_Read2Byte(0x151522);
            MIU0_Phase_L_default[2] = Miu_Scan_Read2Byte(0x151524);
            MIU0_Phase_L_default[3] = Miu_Scan_Read2Byte(0x151526);
            MIU0_Phase_L_default[4] = Miu_Scan_Read2Byte(0x151528);
            MIU0_Phase_L_default[5] = Miu_Scan_Read2Byte(0x15152a);
            MIU0_Phase_L_default[6] = Miu_Scan_Read2Byte(0x15152c);
            MIU0_Phase_L_default[7] = Miu_Scan_Read2Byte(0x15152e);

            //--- HW Write Phase setting ---
            MIU0_Phase_H_default[0] = Miu_Scan_Read2Byte(0x151530);
            MIU0_Phase_H_default[1] = Miu_Scan_Read2Byte(0x151532);
            MIU0_Phase_H_default[2] = Miu_Scan_Read2Byte(0x151534);
            MIU0_Phase_H_default[3] = Miu_Scan_Read2Byte(0x151536);
            MIU0_Phase_H_default[4] = Miu_Scan_Read2Byte(0x151538);
            MIU0_Phase_H_default[5] = Miu_Scan_Read2Byte(0x15153a); //0620
            MIU0_Phase_H_default[6] = Miu_Scan_Read2Byte(0x15153c); //0620
            MIU0_Phase_H_default[7] = Miu_Scan_Read2Byte(0x15153e); //0620

            //Save MIU1 original skew
            MIU1_OriSkew_L = Miu_Scan_Read2Byte(0x1616ec);
            MIU1_OriSkew_H = Miu_Scan_Read2Byte(0x1616ee);

            //MIU1 Original Phase
            //--- Read Write Phase setting ---
            MIU1_Phase_L_default[0] = Miu_Scan_Read2Byte(0x151620);
            MIU1_Phase_L_default[1] = Miu_Scan_Read2Byte(0x151622);
            MIU1_Phase_L_default[2] = Miu_Scan_Read2Byte(0x151624);
            MIU1_Phase_L_default[3] = Miu_Scan_Read2Byte(0x151626);
            MIU1_Phase_L_default[4] = Miu_Scan_Read2Byte(0x151628);
            MIU1_Phase_L_default[5] = Miu_Scan_Read2Byte(0x15162a);
            MIU1_Phase_L_default[6] = Miu_Scan_Read2Byte(0x15162c);
            MIU1_Phase_L_default[7] = Miu_Scan_Read2Byte(0x15162e);

            //--- HW Write Phase setting ---
            MIU1_Phase_H_default[0] = Miu_Scan_Read2Byte(0x151630);
            MIU1_Phase_H_default[1] = Miu_Scan_Read2Byte(0x151632);
            MIU1_Phase_H_default[2] = Miu_Scan_Read2Byte(0x151634);
            MIU1_Phase_H_default[3] = Miu_Scan_Read2Byte(0x151636);
            MIU1_Phase_H_default[4] = Miu_Scan_Read2Byte(0x151638);
            MIU1_Phase_H_default[5] = Miu_Scan_Read2Byte(0x15163a); //0620
            MIU1_Phase_H_default[6] = Miu_Scan_Read2Byte(0x15163c); //0620
            MIU1_Phase_H_default[7] = Miu_Scan_Read2Byte(0x15163e); //0620

    if (DoWritePhase == 1)
    {
        //=============================================================//
        //==============MIU0 Write Phase==================================//

        //Print Default setting
        BootRom_OutputDefaultInfo(MIU_SCAN_MIU0, MIU_SCAN_WRITE_PHASE, MIU_SCAN_LOW_WORD);
        BootRom_OutputDefaultInfo(MIU_SCAN_MIU0, MIU_SCAN_WRITE_PHASE, MIU_SCAN_HIGH_WORD);

        BootRom_OutputInfo(MIU_SCAN_MIU0,           MIU_SCAN_WRITE_PHASE,   MIU_SCAN_LOW_WORD,
                           MIU_SCAN_SKEW_NORMAL,    MIU_SCAN_NOT_DONE,      MIU_SCAN_NOT_FULL_COMPLETE);

        BootRom_InitResult();
        //sel miu0
        Miu_Scan_Write2Byte(0x1012f8, 0x0000);
        //MIU0 Write Phase
        for(Count_WritePhase = 0; Count_WritePhase < 19; Count_WritePhase++)
        {
            LW_Result = 0;
            HW_Result = 0;
            BootRom_WritePhase(MIU_SCAN_MIU0, Count_WritePhase);
            putk_miu_scan('.');
        }
        putk_miu_scan('\n');
        putk_miu_scan('\r');

        BootRom_OutputInfo(MIU_SCAN_MIU0,           MIU_SCAN_WRITE_PHASE,   MIU_SCAN_LOW_WORD,
                           MIU_SCAN_SKEW_NORMAL,    MIU_SCAN_DONE,          MIU_SCAN_NOT_FULL_COMPLETE);
        BootRom_OutputDQInfo(MIU_SCAN_LOW_WORD, 0, 18); //print DQ range
        BootRom_OutputBitResult(MIU_SCAN_MIU0, 19, MIU_SCAN_LOW_WORD, MIU_SCAN_SKEW_NORMAL);  //print L word result
        BootRom_OutputDQInfo(MIU_SCAN_HIGH_WORD, 0, 18); //print DQ range
        BootRom_OutputBitResult(MIU_SCAN_MIU0, 19, MIU_SCAN_HIGH_WORD, MIU_SCAN_SKEW_NORMAL);  //print H word result

        BootRom_OutputInfo(MIU_SCAN_MIU0,       MIU_SCAN_WRITE_PHASE,   MIU_SCAN_LOW_WORD,
                           MIU_SCAN_SKEW_PLUS,  MIU_SCAN_NOT_DONE,      MIU_SCAN_NOT_FULL_COMPLETE);

        BootRom_InitResult();
        //Change MIU0 Skew
        BootRom_ChangeSkew(MIU_SCAN_MIU0, MIU0_OriSkew_L, MIU0_OriSkew_H);
        //MIU0 Write Phase for skew
        for(Count_WritePhase = 0; Count_WritePhase < 19; Count_WritePhase++)
        {
            LW_Result = 0;
            HW_Result = 0;
            BootRom_WritePhase(MIU_SCAN_MIU0, Count_WritePhase);
            putk_miu_scan('.');
        }
        putk_miu_scan('\n');
        putk_miu_scan('\r');

        //Set MIU0 Back Write phase to original phase
        BootRom_SetBackWritePhase(MIU_SCAN_MIU0, MIU0_OriSkew_L, MIU0_OriSkew_H);

        BootRom_OutputInfo(MIU_SCAN_MIU0,       MIU_SCAN_WRITE_PHASE,   MIU_SCAN_LOW_WORD,
                           MIU_SCAN_SKEW_PLUS,  MIU_SCAN_DONE,          MIU_SCAN_NOT_FULL_COMPLETE);
        BootRom_OutputDQInfo(MIU_SCAN_LOW_WORD, 0, 18); //print DQ range
        BootRom_OutputBitResult(MIU_SCAN_MIU0, 19, MIU_SCAN_LOW_WORD, MIU_SCAN_SKEW_PLUS);  //print L word result
        BootRom_OutputDQInfo(MIU_SCAN_HIGH_WORD, 0, 18); //print DQ range
        BootRom_OutputBitResult(MIU_SCAN_MIU0, 19, MIU_SCAN_HIGH_WORD, MIU_SCAN_SKEW_PLUS);  //print H word result
        BootRom_OutputInfo(MIU_SCAN_MIU0,       MIU_SCAN_WRITE_PHASE,   MIU_SCAN_LOW_WORD,
                           MIU_SCAN_SKEW_PLUS,  MIU_SCAN_DONE,          MIU_SCAN_FULL_COMPLETE);


        //=============================================================//
        //==============MIU1 Write Phase==================================//
        //Print Default setting
        BootRom_OutputDefaultInfo(MIU_SCAN_MIU1, MIU_SCAN_WRITE_PHASE, MIU_SCAN_LOW_WORD);
        BootRom_OutputDefaultInfo(MIU_SCAN_MIU1, MIU_SCAN_WRITE_PHASE, MIU_SCAN_HIGH_WORD);
        //MIU1 Write Phase
        //Step 0 : bist init
        BootRom_OutputInfo(MIU_SCAN_MIU1,           MIU_SCAN_WRITE_PHASE,   MIU_SCAN_LOW_WORD,
                           MIU_SCAN_SKEW_NORMAL,    MIU_SCAN_NOT_DONE,      MIU_SCAN_NOT_FULL_COMPLETE);

        BootRom_InitResult();
        //sel miu1
        Miu_Scan_Write2Byte(0x1012f8, 0x8000);
        //MIU1 Write Phase
        for(Count_WritePhase = 0; Count_WritePhase < 19; Count_WritePhase++)
        {
            LW_Result = 0;
            HW_Result = 0;
            BootRom_WritePhase(1, Count_WritePhase);
            putk_miu_scan('.');
        }
        putk_miu_scan('\n');
        putk_miu_scan('\r');

        BootRom_OutputInfo(MIU_SCAN_MIU1,           MIU_SCAN_WRITE_PHASE,   MIU_SCAN_LOW_WORD,
                           MIU_SCAN_SKEW_NORMAL,    MIU_SCAN_DONE,          MIU_SCAN_NOT_FULL_COMPLETE);
        BootRom_OutputDQInfo(MIU_SCAN_LOW_WORD, 0, 18); //print DQ range
        BootRom_OutputBitResult(MIU_SCAN_MIU1, 19, MIU_SCAN_LOW_WORD, MIU_SCAN_SKEW_NORMAL);  //print L word result
        BootRom_OutputDQInfo(MIU_SCAN_HIGH_WORD, 0, 18); //print DQ range
        BootRom_OutputBitResult(MIU_SCAN_MIU1, 19, MIU_SCAN_HIGH_WORD, MIU_SCAN_SKEW_NORMAL);  //print H word result



        BootRom_OutputInfo(MIU_SCAN_MIU1,       MIU_SCAN_WRITE_PHASE,   MIU_SCAN_LOW_WORD,
                           MIU_SCAN_SKEW_PLUS,  MIU_SCAN_NOT_DONE,      MIU_SCAN_NOT_FULL_COMPLETE);

        BootRom_InitResult();
        //Change MIU1 Skew
        BootRom_ChangeSkew(MIU_SCAN_MIU1, MIU1_OriSkew_L, MIU1_OriSkew_H);
        //MIU1 Write Phase for skew
        for(Count_WritePhase = 0; Count_WritePhase < 19; Count_WritePhase++)
        {
            LW_Result = 0;
            HW_Result = 0;
            BootRom_WritePhase(MIU_SCAN_MIU1, Count_WritePhase);
            putk_miu_scan('.');
        }

        putk_miu_scan('\n');
        putk_miu_scan('\r');

        //Set MIU1 Back Write phase to original phase
        BootRom_SetBackWritePhase(MIU_SCAN_MIU1, MIU1_OriSkew_L, MIU1_OriSkew_H);

        BootRom_OutputInfo(MIU_SCAN_MIU1,       MIU_SCAN_WRITE_PHASE,   MIU_SCAN_LOW_WORD,
                           MIU_SCAN_SKEW_PLUS,  MIU_SCAN_DONE,          MIU_SCAN_NOT_FULL_COMPLETE);
        BootRom_OutputDQInfo(MIU_SCAN_LOW_WORD, 0, 18); //print DQ range
        BootRom_OutputBitResult(MIU_SCAN_MIU1, 19, 0, MIU_SCAN_SKEW_PLUS);  //print L word result
        BootRom_OutputDQInfo(MIU_SCAN_HIGH_WORD, 0, 18); //print DQ range
        BootRom_OutputBitResult(MIU_SCAN_MIU1, 19, 1, MIU_SCAN_SKEW_PLUS);  //print H word result
        BootRom_OutputInfo(MIU_SCAN_MIU1,       MIU_SCAN_WRITE_PHASE,   MIU_SCAN_LOW_WORD,
                           MIU_SCAN_SKEW_PLUS,  MIU_SCAN_DONE,          MIU_SCAN_FULL_COMPLETE);

    }

        //=========================================================================================================
        //Read DQS phase scan
        //=========================================================================================================

        //=============================================================//
        //==============MIU0 Read Phase==================================//
        //MIU0 Read Phase
        //Step 0 : bist init

    if (DoReadPhase == 1)
    {

        //sel miu0
        Miu_Scan_Write2Byte(0x1012f8, 0x0000);

        Count_ReadPahse = Miu_Scan_Read2Byte(0x110d66); //read K-code value , Max. Phase = kcode + 10

        if(Count_ReadPahse > 70) //max array is 80
            Count_ReadPahse = 70;

        //Print Default setting
        BootRom_OutputDefaultInfo(MIU_SCAN_MIU0, MIU_SCAN_READ_PHASE, MIU_SCAN_LOW_WORD);
        BootRom_OutputDefaultInfo(MIU_SCAN_MIU0, MIU_SCAN_READ_PHASE, MIU_SCAN_HIGH_WORD);

        BootRom_OutputInfo(MIU_SCAN_MIU0,           MIU_SCAN_READ_PHASE,   MIU_SCAN_LOW_WORD,
                           MIU_SCAN_SKEW_NORMAL,    MIU_SCAN_NOT_DONE,      MIU_SCAN_NOT_FULL_COMPLETE);

        BootRom_InitResult();
        BootRom_SetReadPhaseControl(MIU_SCAN_MIU0); //MIU0 Read Control
        //MIU0 Read Phase
        for(ReadPhase_index = 0; ReadPhase_index < Count_ReadPahse+10; ReadPhase_index++)
        {
            LW_Result = 0;
            HW_Result = 0;
            BootRom_ReadPhase(MIU_SCAN_MIU0, ReadPhase_index);
            putk_miu_scan('.');
            //BootRom_OutputBitResult(0, 1, 0); //print L word result
            //BootRom_OutputBitResult(0, 1, 1); //print L word result
        }
        putk_miu_scan('\n');
        putk_miu_scan('\r');

        //Set Back MIU0 Read phase to original phase
        BootRom_SetBackReadPhase(0);

        BootRom_OutputInfo(MIU_SCAN_MIU0,           MIU_SCAN_READ_PHASE,   MIU_SCAN_LOW_WORD,
                           MIU_SCAN_SKEW_NORMAL,    MIU_SCAN_DONE,          MIU_SCAN_NOT_FULL_COMPLETE);
        BootRom_OutputDQInfo(MIU_SCAN_LOW_WORD, 0, Count_ReadPahse+10); //print DQ range
        BootRom_OutputBitResult(MIU_SCAN_MIU0, Count_ReadPahse+10, 0, MIU_SCAN_SKEW_NORMAL);  //print L word result
        BootRom_OutputDQInfo(MIU_SCAN_HIGH_WORD, 0, Count_ReadPahse+10); //print DQ range
        BootRom_OutputBitResult(MIU_SCAN_MIU0, Count_ReadPahse+10, 1, MIU_SCAN_SKEW_NORMAL);  //print H word result
        BootRom_OutputInfo(MIU_SCAN_MIU0,           MIU_SCAN_READ_PHASE,   MIU_SCAN_LOW_WORD,
                           MIU_SCAN_SKEW_NORMAL,    MIU_SCAN_DONE,         MIU_SCAN_FULL_COMPLETE);


        //=============================================================//
        //==============MIU1 Read Phase==================================//
        //MIU1 Read Phase

        //sel miu1
        Miu_Scan_Write2Byte(0x1012f8, 0x8000);

        Count_ReadPahse = Miu_Scan_Read2Byte(0x161666); //read K-code value , Max. Phase = kcode + 10

        if(Count_ReadPahse > 70) //max array is 80
            Count_ReadPahse = 70;

        //Print Default setting
        BootRom_OutputDefaultInfo(MIU_SCAN_MIU1, MIU_SCAN_READ_PHASE, MIU_SCAN_LOW_WORD);
        BootRom_OutputDefaultInfo(MIU_SCAN_MIU1, MIU_SCAN_READ_PHASE, MIU_SCAN_HIGH_WORD);
        //Step 0 : bist init
        BootRom_OutputInfo(MIU_SCAN_MIU1,           MIU_SCAN_READ_PHASE,   MIU_SCAN_LOW_WORD,
                           MIU_SCAN_SKEW_NORMAL,    MIU_SCAN_NOT_DONE,     MIU_SCAN_NOT_FULL_COMPLETE);

        BootRom_InitResult();
        BootRom_SetReadPhaseControl(1); //MIU1 Read Control
        //MIU1 Read Phase
        for(ReadPhase_index = 0; ReadPhase_index < Count_ReadPahse+10; ReadPhase_index++)
        {
            LW_Result = 0;
            HW_Result = 0;
            BootRom_ReadPhase(1, ReadPhase_index);
            putk_miu_scan('.');
            //BootRom_OutputBitResult(1, 1, 0); //print L word result
            //BootRom_OutputBitResult(1, 1, 1); //print L word result
        }
        putk_miu_scan('\n');
        putk_miu_scan('\r');

        //Set Back MIU1 Read phase to original phase
        BootRom_SetBackReadPhase(1);

        BootRom_OutputInfo(MIU_SCAN_MIU1,           MIU_SCAN_READ_PHASE,   MIU_SCAN_LOW_WORD,
                           MIU_SCAN_SKEW_NORMAL,    MIU_SCAN_DONE,         MIU_SCAN_NOT_FULL_COMPLETE);

        BootRom_OutputDQInfo(MIU_SCAN_LOW_WORD, 0, Count_ReadPahse+10); //print DQ range
        BootRom_OutputBitResult(MIU_SCAN_MIU1, Count_ReadPahse+10, 0, MIU_SCAN_SKEW_NORMAL);  //print L word result
        BootRom_OutputDQInfo(MIU_SCAN_HIGH_WORD, 0, Count_ReadPahse+10); //print DQ range
        BootRom_OutputBitResult(MIU_SCAN_MIU1, Count_ReadPahse+10, 1, MIU_SCAN_SKEW_NORMAL);  //print H word result
        BootRom_OutputInfo(MIU_SCAN_MIU1,           MIU_SCAN_READ_PHASE,   MIU_SCAN_LOW_WORD,
                           MIU_SCAN_SKEW_NORMAL,    MIU_SCAN_DONE,         MIU_SCAN_FULL_COMPLETE);

    }


        //=========================================================================================================
        //CMD phase scan
        //=========================================================================================================
        //==============MIU0 CMD Phase Scan==================================//
        //MIU0 CMD Phase
        //Step 0 : bist init

      //  BootRom_OutputInfo(MIU_SCAN_MIU0,           MIU_SCAN_CMD_PHASE,     MIU_SCAN_LOW_WORD,
       //                    MIU_SCAN_SKEW_MINUS,     MIU_SCAN_NOT_DONE,      MIU_SCAN_NOT_FULL_COMPLETE);
    if (DoCmdPhase == 1)
    {
        //sel miu0
        Miu_Scan_Write2Byte(0x1012f8, 0x0000);

        //Read MIU0 Ori-Skew
        CMD_MIU0_Ori_Skew = Miu_Scan_Read2Byte(0x110de0); //read original cko skew ,  [6:4] : reg_cko1_skew  for HW, [2:0] : reg_cko0_skew  for LW
        CMD_MIU0_Ori_Phase = Miu_Scan_Read2Byte(0x15150c); //read original cko phase, [12:8] : reg_clkph_cko1 for HW, [4:0] : reg_clkph_cko0 for LW


        BootRom_OutputDefaultInfo(MIU_SCAN_MIU0, MIU_SCAN_CMD_PHASE, MIU_SCAN_LOW_WORD);
        BootRom_OutputDefaultInfo(MIU_SCAN_MIU0, MIU_SCAN_CMD_PHASE, MIU_SCAN_HIGH_WORD);

        BootRom_OutputInfo(MIU_SCAN_MIU0,           MIU_SCAN_CMD_PHASE,   MIU_SCAN_LOW_WORD,
                           MIU_SCAN_SKEW_MINUS,    MIU_SCAN_NOT_DONE,     MIU_SCAN_NOT_FULL_COMPLETE);

        BootRom_InitResult();
        //MIU0 CMD Skew-1
        for(Count_CmdPhase = 19; Count_CmdPhase > (CMD_MIU0_Ori_Phase & 0x00FF); Count_CmdPhase--)
        {
            LW_Result = 0;
            HW_Result = 0;
            //if (Count_CmdPhase == 19)
            //    u8IsFirstTimeInit = 1;
            BootRom_CmdPhase(MIU_SCAN_MIU0, CMD_MIU0_Ori_Skew - 0x11, Count_CmdPhase-1);
            putk_miu_scan('.');
        }

        putk_miu_scan('\n');
        putk_miu_scan('\r');


        BootRom_Restore_CmdPhase(MIU_SCAN_MIU0, CMD_MIU0_Ori_Skew, CMD_MIU0_Ori_Phase);

        BootRom_OutputInfo(MIU_SCAN_MIU0,           MIU_SCAN_CMD_PHASE,     MIU_SCAN_LOW_WORD,
                           MIU_SCAN_SKEW_MINUS,     MIU_SCAN_DONE,          MIU_SCAN_NOT_FULL_COMPLETE);
        BootRom_OutputDQInfo(MIU_SCAN_LOW_WORD, 0, 18); //print DQ range
        BootRom_OutputByteResult(MIU_SCAN_MIU0, 19, MIU_SCAN_LOW_WORD); //print L word result
        BootRom_OutputDQInfo(MIU_SCAN_HIGH_WORD, 0, 18); //print DQ range
        BootRom_OutputByteResult(MIU_SCAN_MIU0, 19, MIU_SCAN_HIGH_WORD); //print H word result




        BootRom_OutputInfo(MIU_SCAN_MIU0,           MIU_SCAN_CMD_PHASE,     MIU_SCAN_LOW_WORD,
                           MIU_SCAN_SKEW_NORMAL,    MIU_SCAN_NOT_DONE,      MIU_SCAN_NOT_FULL_COMPLETE);

        BootRom_InitResult();
        //MIU0 CMD Skew+0
        for(Count_CmdPhase = 0; Count_CmdPhase < 19; Count_CmdPhase++)
        {
            LW_Result = 0;
            HW_Result = 0;
            BootRom_CmdPhase(MIU_SCAN_MIU0, CMD_MIU0_Ori_Skew, Count_CmdPhase);
            putk_miu_scan('.');
        }
        putk_miu_scan('\n');
        putk_miu_scan('\r');

        BootRom_Restore_CmdPhase(MIU_SCAN_MIU0, CMD_MIU0_Ori_Skew, CMD_MIU0_Ori_Phase);


        BootRom_OutputInfo(MIU_SCAN_MIU0,           MIU_SCAN_CMD_PHASE,     MIU_SCAN_LOW_WORD,
                           MIU_SCAN_SKEW_NORMAL,    MIU_SCAN_DONE,          MIU_SCAN_NOT_FULL_COMPLETE);
        BootRom_OutputDQInfo(MIU_SCAN_LOW_WORD, 0, 18); //print DQ range
        BootRom_OutputByteResult(MIU_SCAN_MIU0, 19, MIU_SCAN_LOW_WORD); //print L word result
        BootRom_OutputDQInfo(MIU_SCAN_HIGH_WORD, 0, 18); //print DQ range
        BootRom_OutputByteResult(MIU_SCAN_MIU0, 19, MIU_SCAN_HIGH_WORD); //print H word result


        BootRom_OutputInfo(MIU_SCAN_MIU0,           MIU_SCAN_CMD_PHASE,     MIU_SCAN_LOW_WORD,
                           MIU_SCAN_SKEW_PLUS,      MIU_SCAN_NOT_DONE,      MIU_SCAN_NOT_FULL_COMPLETE);


        BootRom_InitResult();
        //MIU0 CMD Skew+1
        for(Count_CmdPhase = 0; Count_CmdPhase < (CMD_MIU0_Ori_Phase & 0x00FF)+1; Count_CmdPhase++)
        {
            LW_Result = 0;
            HW_Result = 0;
            BootRom_CmdPhase(MIU_SCAN_MIU0, CMD_MIU0_Ori_Skew + 0x11, Count_CmdPhase);
            putk_miu_scan('.');
        }

        putk_miu_scan('\n');
        putk_miu_scan('\r');

        BootRom_Restore_CmdPhase(MIU_SCAN_MIU0, CMD_MIU0_Ori_Skew, CMD_MIU0_Ori_Phase);

        BootRom_OutputInfo(MIU_SCAN_MIU0,           MIU_SCAN_CMD_PHASE,     MIU_SCAN_LOW_WORD,
                           MIU_SCAN_SKEW_PLUS,      MIU_SCAN_DONE,          MIU_SCAN_NOT_FULL_COMPLETE);
        BootRom_OutputDQInfo(MIU_SCAN_LOW_WORD, 0, (CMD_MIU0_Ori_Phase & 0x00FF)+1); //print DQ range
        BootRom_OutputByteResult(MIU_SCAN_MIU0, 1, MIU_SCAN_LOW_WORD); //print L word result
        BootRom_OutputDQInfo(MIU_SCAN_HIGH_WORD, 0, (CMD_MIU0_Ori_Phase & 0x00FF)+1); //print DQ range
        BootRom_OutputByteResult(MIU_SCAN_MIU0, 1, MIU_SCAN_HIGH_WORD); //print H word result
        BootRom_OutputInfo(MIU_SCAN_MIU0,           MIU_SCAN_CMD_PHASE,     MIU_SCAN_LOW_WORD,
                           MIU_SCAN_SKEW_PLUS,      MIU_SCAN_DONE,          MIU_SCAN_FULL_COMPLETE);


        //==============MIU1 CMD Phase Scan==================================//
        //MIU1 CMD Phase
        //Step 0 : bist init

        //sel miu1
        Miu_Scan_Write2Byte(0x1012f8, 0x8000);

        //Read MIU1 Ori-Skew
        CMD_MIU1_Ori_Skew = Miu_Scan_Read2Byte(0x1616e0); //read original cko skew ,  [6:4] : reg_cko1_skew  for HW, [2:0] : reg_cko0_skew  for LW
        CMD_MIU1_Ori_Phase = Miu_Scan_Read2Byte(0x15160c); //read original cko phase, [12:8] : reg_clkph_cko1 for HW, [4:0] : reg_clkph_cko0 for LW

        BootRom_OutputDefaultInfo(MIU_SCAN_MIU1, MIU_SCAN_CMD_PHASE, MIU_SCAN_LOW_WORD);
        BootRom_OutputDefaultInfo(MIU_SCAN_MIU1, MIU_SCAN_CMD_PHASE, MIU_SCAN_HIGH_WORD);

        BootRom_OutputInfo(MIU_SCAN_MIU1,           MIU_SCAN_CMD_PHASE,   MIU_SCAN_LOW_WORD,
                           MIU_SCAN_SKEW_MINUS,    MIU_SCAN_NOT_DONE,     MIU_SCAN_NOT_FULL_COMPLETE);


        BootRom_InitResult();
        //MIU1 CMD Skew-1
        for(Count_CmdPhase = 19; Count_CmdPhase > (CMD_MIU1_Ori_Phase & 0x00FF); Count_CmdPhase--)
        {
            LW_Result = 0;
            HW_Result = 0;
            //if (Count_CmdPhase == 19)
            //    u8IsFirstTimeInit = 1;
            BootRom_CmdPhase(MIU_SCAN_MIU1, CMD_MIU1_Ori_Skew - 0x11, Count_CmdPhase-1);
            putk_miu_scan('.');
        }

        putk_miu_scan('\n');
        putk_miu_scan('\r');

        BootRom_Restore_CmdPhase(MIU_SCAN_MIU1, CMD_MIU1_Ori_Skew, CMD_MIU1_Ori_Phase);

        BootRom_OutputInfo(MIU_SCAN_MIU1,           MIU_SCAN_CMD_PHASE,     MIU_SCAN_LOW_WORD,
                           MIU_SCAN_SKEW_MINUS,     MIU_SCAN_DONE,          MIU_SCAN_NOT_FULL_COMPLETE);
        BootRom_OutputDQInfo(MIU_SCAN_LOW_WORD, 0, 18); //print DQ range
        BootRom_OutputByteResult(MIU_SCAN_MIU1, 19, MIU_SCAN_LOW_WORD); //print L word result
        BootRom_OutputDQInfo(MIU_SCAN_HIGH_WORD, 0, 18); //print DQ range
        BootRom_OutputByteResult(MIU_SCAN_MIU1, 19, MIU_SCAN_HIGH_WORD); //print H word result
        BootRom_OutputInfo(MIU_SCAN_MIU1,           MIU_SCAN_CMD_PHASE,     MIU_SCAN_LOW_WORD,
                           MIU_SCAN_SKEW_NORMAL,    MIU_SCAN_NOT_DONE,      MIU_SCAN_NOT_FULL_COMPLETE);

        BootRom_InitResult();
        //MIU1 CMD Skew
        for(Count_CmdPhase = 0; Count_CmdPhase < 19; Count_CmdPhase++)
        {
            LW_Result = 0;
            HW_Result = 0;
            BootRom_CmdPhase(MIU_SCAN_MIU1, CMD_MIU1_Ori_Skew, Count_CmdPhase);
            putk_miu_scan('.');
        }

        putk_miu_scan('\n');
        putk_miu_scan('\r');

        BootRom_Restore_CmdPhase(MIU_SCAN_MIU1, CMD_MIU1_Ori_Skew, CMD_MIU1_Ori_Phase);

        BootRom_OutputInfo(MIU_SCAN_MIU1,           MIU_SCAN_CMD_PHASE,     MIU_SCAN_LOW_WORD,
                           MIU_SCAN_SKEW_NORMAL,    MIU_SCAN_DONE,      MIU_SCAN_NOT_FULL_COMPLETE);
        BootRom_OutputDQInfo(MIU_SCAN_LOW_WORD, 0, 18); //print DQ range
        BootRom_OutputByteResult(MIU_SCAN_MIU1, 19, MIU_SCAN_LOW_WORD); //print L word result
        BootRom_OutputDQInfo(MIU_SCAN_HIGH_WORD, 0, 18); //print DQ range
        BootRom_OutputByteResult(MIU_SCAN_MIU1, 19, MIU_SCAN_HIGH_WORD); //print H word result
        BootRom_OutputInfo(MIU_SCAN_MIU1,         MIU_SCAN_CMD_PHASE,     MIU_SCAN_LOW_WORD,
                           MIU_SCAN_SKEW_PLUS,    MIU_SCAN_NOT_DONE,      MIU_SCAN_NOT_FULL_COMPLETE);

        BootRom_InitResult();
        //MIU1 CMD Skew+1
        for(Count_CmdPhase = 0; Count_CmdPhase < (CMD_MIU1_Ori_Phase & 0x00FF)+1; Count_CmdPhase++)
        {
            LW_Result = 0;
            HW_Result = 0;
            BootRom_CmdPhase(MIU_SCAN_MIU1, CMD_MIU1_Ori_Skew + 0x11, Count_CmdPhase);
            putk_miu_scan('.');
        }
        putk_miu_scan('\n');
        putk_miu_scan('\r');

        BootRom_Restore_CmdPhase(MIU_SCAN_MIU1, CMD_MIU1_Ori_Skew, CMD_MIU1_Ori_Phase);

        BootRom_OutputInfo(MIU_SCAN_MIU1,         MIU_SCAN_CMD_PHASE,     MIU_SCAN_LOW_WORD,
                           MIU_SCAN_SKEW_PLUS,    MIU_SCAN_DONE,          MIU_SCAN_NOT_FULL_COMPLETE);
        BootRom_OutputDQInfo(MIU_SCAN_LOW_WORD, 0, (CMD_MIU1_Ori_Phase & 0x00FF)+1); //print DQ range
        BootRom_OutputByteResult(MIU_SCAN_MIU1, 1, MIU_SCAN_LOW_WORD); //print L word result
        BootRom_OutputDQInfo(MIU_SCAN_HIGH_WORD, 0, (CMD_MIU1_Ori_Phase & 0x00FF)+1); //print DQ range
        BootRom_OutputByteResult(MIU_SCAN_MIU1, 1, MIU_SCAN_HIGH_WORD); //print H word result
        BootRom_OutputInfo(MIU_SCAN_MIU1,         MIU_SCAN_CMD_PHASE,     MIU_SCAN_LOW_WORD,
                           MIU_SCAN_SKEW_PLUS,    MIU_SCAN_DONE,          MIU_SCAN_FULL_COMPLETE);

    }

#if (MIU_SCAN_DEBUG)
    while (1)
    {;}
#endif

    }
}


