#!/bin/bash
#Convert PM51 bin file to dat file
PM51_DAT_FOLDER=../MstarCustomer/include
PM51_DAT_FILE=./$PM51_DAT_FOLDER/pmlite.dat

if [ -r PM51_M7621.bin ]; then
    ./bin2hex PM51_M7621.bin > $PM51_DAT_FILE
        sed -i '1d' ${PM51_DAT_FILE}
    echo "build PM51 dat file OK"
fi

rm -rf ./*.bin

echo "-- build PM51 dat file OK --"
