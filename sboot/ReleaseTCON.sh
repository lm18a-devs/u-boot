#!/bin/bash
#Convert TCON bin file to dat file
TCON_DAT_FOLDER=../MstarCore/include
TCON_DAT_43INCH_FILE=./$TCON_DAT_FOLDER/tcon_43inch.dat
TCON_DAT_49INCH_FILE=./$TCON_DAT_FOLDER/tcon_49inch.dat
TCON_DAT_55INCH_FILE=./$TCON_DAT_FOLDER/tcon_55inch.dat
TCON_DAT_65INCH_FILE=./$TCON_DAT_FOLDER/tcon_65inch.dat

if [ -r TCON_*_43inch.bin ]; then
    ./bin2hex TCON_*_43inch.bin > $TCON_DAT_43INCH_FILE
	sed -i '1d' ${TCON_DAT_43INCH_FILE}
    echo "build 43 inch TCON dat file OK" 
fi 
if [ -r TCON_*_49inch.bin ]; then
    ./bin2hex TCON_*_49inch.bin > $TCON_DAT_49INCH_FILE
	sed -i '1d' ${TCON_DAT_49INCH_FILE}
    echo "build 49 inch TCON dat file OK" 
fi 
if [ -r TCON_*_55inch.bin ]; then
    ./bin2hex TCON_*_55inch.bin > $TCON_DAT_55INCH_FILE
	sed -i '1d' ${TCON_DAT_55INCH_FILE}
    echo "build 55 inch TCON dat file OK" 
fi 
if [ -r TCON_*_65inch.bin ]; then
    ./bin2hex TCON_*_65inch.bin > $TCON_DAT_65INCH_FILE
	sed -i '1d' ${TCON_DAT_65INCH_FILE}
    echo "build 65 inch TCON dat file OK" 
fi 

rm -rf ./*.bin

echo "-- build TCON dat file OK --"
