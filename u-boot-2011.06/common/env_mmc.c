/*
 * (C) Copyright 2008-2011 Freescale Semiconductor, Inc.
 *
 * See file CREDITS for list of people who contributed to this
 * project.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */

/* #define DEBUG */

#include <common.h>

#include <command.h>
#include <environment.h>
#include <linux/stddef.h>
#include <malloc.h>
#include <mmc.h>
#include <search.h>
#include <errno.h>
#include <MsSystem.h>

#include "drv_eMMC.h"
#include "../disk/part_emmc.h"

/* references to names in env_common.c */
extern uchar default_environment[];

char *env_name_spec = "MMC";

extern ulong emmc_boot_read(struct mmc *mmc, int prtno, int dev_num, ulong start, lbaint_t blkcnt, unsigned long *dst);
extern ulong emmc_boot_write(struct mmc *mmc, int prtno, int dev_num, ulong start, lbaint_t blkcnt, unsigned long *src);

#ifdef ENV_IS_EMBEDDED
extern uchar environment[];
env_t *env_ptr = (env_t *)(&environment[0]);
#else /* ! ENV_IS_EMBEDDED */
env_t *env_ptr = NULL;
#endif /* ENV_IS_EMBEDDED */

/* local functions */
#if !defined(ENV_IS_EMBEDDED)
static void use_default(void);
#endif

DECLARE_GLOBAL_DATA_PTR;

#if !defined(CONFIG_ENV_OFFSET)
#define CONFIG_ENV_OFFSET 0
#endif

static int __mmc_get_env_addr(struct mmc *mmc, u32 *env_addr)
{
    *env_addr = CONFIG_ENV_OFFSET;
    return 0;
}
__attribute__((weak, alias("__mmc_get_env_addr")))
int mmc_get_env_addr(struct mmc *mmc, u32 *env_addr);


uchar env_get_char_spec(int index)
{
    return *((uchar *)(gd->env_addr + index));
}

int env_init(void)
{
    /* use default */
    gd->env_addr = (ulong)&default_environment[0];
    gd->env_valid = 1;

    return 0;
}

int init_mmc_for_env(struct mmc *mmc)
{
    if (!mmc) {
        puts("No MMC card found\n");
        return -1;
    }

    //if (mmc_init(mmc)) {
    if(eMMC_Init()){
        puts("MMC init failed\n");
        return  -1;
    }

    return 0;
}

#ifdef CONFIG_CMD_SAVEENV

inline int write_env(struct mmc *mmc, unsigned long size,
			unsigned long offset, const void *buffer)
{
    uint blk_start, blk_cnt, n;//, total_size;

    blk_cnt   = ALIGN(size, 512) / 512;
    blk_start = ENV_NVM_OFFSET>>9;
    n = eMMC_WriteData((U8*)buffer, blk_cnt << 9, blk_start);

    return (n == 0) ? 0 : -1;
}

int saveenv(void)
{
    env_t *tmp_env1 = NULL;
    ssize_t	len;
    char *res=NULL;

    u32 offset = 0;

    tmp_env1 = (env_t *)malloc(CONFIG_ENV_SIZE);

    if (!tmp_env1) {
        set_default_env("!malloc() failed");
        return 1;
    }

    res = (char *)tmp_env1->data;
    len = hexport_r(&env_htab, '\0', &res, ENV_SIZE);
    if (len < 0) {
        error("Cannot export environment: errno = %d\n", errno);
        return 1;
    }
    tmp_env1->crc   = crc32(0, tmp_env1->data, ENV_SIZE);
    printf("Writing to MMC(%d)... ", CONFIG_SYS_MMC_ENV_DEV);
    if (write_env(NULL, CONFIG_ENV_SIZE, offset, (u_char *)tmp_env1)) {
        puts("failed to save env\n");
    }

    remake_hib();
    puts("done\n");
    return 0;
}
#endif /* CONFIG_CMD_SAVEENV */

inline int read_env(struct mmc *mmc, unsigned long size,
			unsigned long offset, const void *buffer)
{
    uint blk_start, blk_cnt, n;
    blk_cnt   = ALIGN(size, 512) / 512;

    blk_start  = ENV_NVM_OFFSET >> 9;
    n = eMMC_ReadData((U8*)buffer, blk_cnt << 9, blk_start);
    return (n == 0) ? 0 : -1;
}

void env_relocate_spec(void)
{
#if !defined(ENV_IS_EMBEDDED)
    char buf[CONFIG_ENV_SIZE];

    env_t *ep;
    //struct mmc *mmc = find_mmc_device(CONFIG_SYS_MMC_ENV_DEV);
    u32 crc, offset = 0;
    int ret;

    printf("Reading from MMC(%d)... ", CONFIG_SYS_MMC_ENV_DEV);
    ret = read_env(NULL, CONFIG_ENV_SIZE, offset, buf);

    ep = (env_t *)buf;
    crc = crc32(0, ep->data, ENV_SIZE);


    if ((crc != ep->crc) || (ret == -1))
    {
        use_default();
        return;
    }

    env_import(buf, 1);
#endif
}

#if !defined(ENV_IS_EMBEDDED)
static void use_default()
{
    set_default_env(NULL);
}
#endif
