////////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2006-2009 MStar Semiconductor, Inc.
// All rights reserved.
//
// Unless otherwise stipulated in writing, any and all information contained
// herein regardless in any format shall remain the sole proprietary of
// MStar Semiconductor Inc. and be kept in strict confidence
// ("MStar Confidential Information") by the recipient.
// Any unauthorized act including without limitation unauthorized disclosure,
// copying, use, reproduction, sale, distribution, modification, disassembling,
// reverse engineering and compiling of the contents of MStar Confidential
// Information is unlawful and strictly prohibited. MStar hereby reserves the
// rights to any and all damages, losses, costs and expenses resulting therefrom.
//
////////////////////////////////////////////////////////////////////////////////

#define SCA_TOOL_VERSION            "SN SCA V1.1.10 "

////////////////////////////////////////////////////////////////////////////////
// DRAM memory map
//
// Every Module Memory Mapping need 4 define,
// and check code in "msAPI_Memory_DumpMemoryMap"
// 1. XXX_AVAILABLE : For get avaialble memory start address
// 2. XXX_ADR       : Real Address with Alignment
// 3. XXX_GAP_CHK   : For Check Memory Gap, for avoid memory waste
// 4. XXX_LEN       : For the Memory size of this Module usage
////////////////////////////////////////////////////////////////////////////////
#define ENABLE_MIU_1                1
#define ENABLE_MIU_2                0
#define MIU_DRAM_LEN                0x0030000000
#define MIU_DRAM_LEN0               0x0020000000
#define MIU_DRAM_LEN1               0x0010000000
#define MIU_DRAM_LEN2               0x0000000000
#define MIU_INTERVAL                0x0080000000
#define CPU_ALIGN                   0x0000001000

////////////////////////////////////////////////////////////////////////////////
//MIU SETTING
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
//MEMORY TYPE
////////////////////////////////////////////////////////////////////////////////
#define MIU0                        (0x0000)
#define MIU1                        (0x0001)
#define MIU2                        (0x0002)

#define TYPE_NONE                   (0x0000 << 2)

#define UNCACHE                     (0x0001 << 2)
#define REMAPPING_TO_USER_SPACE     (0x0002 << 2)
#define CACHE                       (0x0004 << 2)
#define NONCACHE_BUFFERABLE         (0x0008 << 2)


#define CMA19                       (0x0003)
//MIU_0_START
/* DUMMY_MEM for cobuffer draw */
#define DUMMY_MEM_LAYER                                        0
#define DUMMY_MEM_AVAILABLE                                    0x0000000000
#define DUMMY_MEM_ADR                                          0x0000000000  //Alignment 0x01000
#define DUMMY_MEM_GAP_CHK                                      0x0000000000
#define DUMMY_MEM_LEN                                          0x0000200000
#define DUMMY_MEM_MEMORY_TYPE                                  (MIU0 | TYPE_NONE | UNCACHE | TYPE_NONE)
#define DUMMY_MEM_CMA_HID                                      0

/* BIN_MEM   */
#define BIN_MEM_LAYER                                          1
#define BIN_MEM_AVAILABLE                                      0x0000000000
#define BIN_MEM_ADR                                            0x0000000000  //Alignment 0x01000
#define BIN_MEM_GAP_CHK                                        0x0000000000
#define BIN_MEM_LEN                                            0x0000100000
#define BIN_MEM_MEMORY_TYPE                                    (MIU0 | TYPE_NONE | UNCACHE | TYPE_NONE)
#define BIN_MEM_CMA_HID                                        0

/* MAILBOX 40KB */
#define MAILBOX_LAYER                                          1
#define MAILBOX_AVAILABLE                                      0x0000100000
#define MAILBOX_ADR                                            0x0000100000  //Alignment 0x01000
#define MAILBOX_GAP_CHK                                        0x0000000000
#define MAILBOX_LEN                                            0x0000100000
#define MAILBOX_MEMORY_TYPE                                    (MIU0 | TYPE_NONE | UNCACHE | TYPE_NONE)
#define MAILBOX_CMA_HID                                        0

/* LINUX_MEM 408MB */
#define LINUX_MEM_LAYER                                        0
#define LINUX_MEM_AVAILABLE                                    0x0000200000
#define LINUX_MEM_ADR                                          0x0000200000  //Alignment 0x02000
#define LINUX_MEM_GAP_CHK                                      0x0000000000
#define LINUX_MEM_LEN                                          0x0018C00000
#define LINUX_MEM_MEMORY_TYPE                                  (MIU0 | TYPE_NONE | UNCACHE | TYPE_NONE)
#define LINUX_MEM_CMA_HID                                      0

/* EMAC_MEM 1MB */
#define EMAC_MEM_LAYER                                         0
#define EMAC_MEM_AVAILABLE                                     0x0018E00000
#define EMAC_MEM_ADR                                           0x0018E00000  //Alignment 0x01000
#define EMAC_MEM_GAP_CHK                                       0x0000000000
#define EMAC_MEM_LEN                                           0x0000100000
#define EMAC_MEM_MEMORY_TYPE                                   (MIU0 | TYPE_NONE | UNCACHE | TYPE_NONE)
#define EMAC_MEM_CMA_HID                                       0

/* VE_DIPW_BUF   */
#define VE_DIPW_BUF_LAYER                                      0
#define VE_DIPW_BUF_AVAILABLE                                  0x0018F00000
#define VE_DIPW_BUF_ADR                                        0x0018F00000  //Alignment 0x100000
#define VE_DIPW_BUF_GAP_CHK                                    0x0000000000
#define VE_DIPW_BUF_LEN                                        0x0000333000
#define VE_DIPW_BUF_MEMORY_TYPE                                (MIU0 | TYPE_NONE | UNCACHE | TYPE_NONE)
#define VE_DIPW_BUF_CMA_HID                                    0

/* MFE_BUF   */
#define MFE_BUF_LAYER                                          0
#define MFE_BUF_AVAILABLE                                      0x0019233000
#define MFE_BUF_ADR                                            0x0019233000  //Alignment 0x01000
#define MFE_BUF_GAP_CHK                                        0x0000000000
#define MFE_BUF_LEN                                            0x0000300000
#define MFE_BUF_MEMORY_TYPE                                    (MIU0 | TYPE_NONE | UNCACHE | TYPE_NONE)
#define MFE_BUF_CMA_HID                                        0

/* MFE_OUT_BUF   */
#define MFE_OUT_BUF_LAYER                                      0
#define MFE_OUT_BUF_AVAILABLE                                  0x0019533000
#define MFE_OUT_BUF_ADR                                        0x0019533000  //Alignment 0x01000
#define MFE_OUT_BUF_GAP_CHK                                    0x0000000000
#define MFE_OUT_BUF_LEN                                        0x0000267000
#define MFE_OUT_BUF_MEMORY_TYPE                                (MIU0 | TYPE_NONE | UNCACHE | TYPE_NONE)
#define MFE_OUT_BUF_CMA_HID                                    0

/* VIDEO_IN_CAPTURE_TMP   */
#define VIDEO_IN_CAPTURE_TMP_LAYER                             0
#define VIDEO_IN_CAPTURE_TMP_AVAILABLE                         0x001979A000
#define VIDEO_IN_CAPTURE_TMP_ADR                               0x001979A000  //Alignment 0x01000
#define VIDEO_IN_CAPTURE_TMP_GAP_CHK                           0x0000000000
#define VIDEO_IN_CAPTURE_TMP_LEN                               0x0000400000
#define VIDEO_IN_CAPTURE_TMP_MEMORY_TYPE                       (MIU0 | TYPE_NONE | UNCACHE | TYPE_NONE)
#define VIDEO_IN_CAPTURE_TMP_CMA_HID                           0

/* PVR_THUMBNAIL_DECODE_BUFFER 16MB */
#define PVR_THUMBNAIL_DECODE_BUFFER_LAYER                      1
#define PVR_THUMBNAIL_DECODE_BUFFER_AVAILABLE                  0x001979A000
#define PVR_THUMBNAIL_DECODE_BUFFER_ADR                        0x001979A000  //Alignment 0x01000
#define PVR_THUMBNAIL_DECODE_BUFFER_GAP_CHK                    0x0000000000
#define PVR_THUMBNAIL_DECODE_BUFFER_LEN                        0x0000000000
#define PVR_THUMBNAIL_DECODE_BUFFER_MEMORY_TYPE                (MIU0 | TYPE_NONE | UNCACHE | TYPE_NONE)
#define PVR_THUMBNAIL_DECODE_BUFFER_CMA_HID                    0

/* XIP_MEM   */
#define XIP_MEM_LAYER                                          0
#define XIP_MEM_AVAILABLE                                      0x0019B9A000
#define XIP_MEM_ADR                                            0x0019B9A000  //Alignment 0x01000
#define XIP_MEM_GAP_CHK                                        0x0000000000
#define XIP_MEM_LEN                                            0x0000000000
#define XIP_MEM_MEMORY_TYPE                                    (MIU0 | TYPE_NONE | UNCACHE | TYPE_NONE)
#define XIP_MEM_CMA_HID                                        0

/* SECURE_TSP   */
#define SECURE_TSP_LAYER                                       0
#define SECURE_TSP_AVAILABLE                                   0x0019B9A000
#define SECURE_TSP_ADR                                         0x0019B9A000  //Alignment 0x01000
#define SECURE_TSP_GAP_CHK                                     0x0000000000
#define SECURE_TSP_LEN                                         0x0000000000
#define SECURE_TSP_MEMORY_TYPE                                 (MIU0 | TYPE_NONE | UNCACHE | TYPE_NONE)
#define SECURE_TSP_CMA_HID                                     0

/* MAD_BASE_BUFFER 6.5MB : R2 + SE + DEC + COMMON */
#define MAD_BASE_BUFFER_LAYER                                  0
#define MAD_BASE_BUFFER_AVAILABLE                              0x0019B9A000
#define MAD_BASE_BUFFER_ADR                                    0x0019B9A000  //Alignment 0x01000
#define MAD_BASE_BUFFER_GAP_CHK                                0x0000000000
#define MAD_BASE_BUFFER_LEN                                    0x0000E00000
#define MAD_BASE_BUFFER_MEMORY_TYPE                            (MIU0 | TYPE_NONE | UNCACHE | TYPE_NONE)
#define MAD_BASE_BUFFER_CMA_HID                                0

/* VD_3DCOMB 4MB */
#define VD_3DCOMB_LAYER                                        0
#define VD_3DCOMB_AVAILABLE                                    0x001A99A000
#define VD_3DCOMB_ADR                                          0x001A99A000  //Alignment 0x01000
#define VD_3DCOMB_GAP_CHK                                      0x0000000000
#define VD_3DCOMB_LEN                                          0x0000400000
#define VD_3DCOMB_MEMORY_TYPE                                  (MIU0 | TYPE_NONE | UNCACHE | TYPE_NONE)
#define VD_3DCOMB_CMA_HID                                      0

/* G3D_TEXTURE0_BUF 16MB */
#define G3D_TEXTURE0_BUF_LAYER                                 0
#define G3D_TEXTURE0_BUF_AVAILABLE                             0x001AD9A000
#define G3D_TEXTURE0_BUF_ADR                                   0x001AD9A000  //Alignment 0x01000
#define G3D_TEXTURE0_BUF_GAP_CHK                               0x0000000000
#define G3D_TEXTURE0_BUF_LEN                                   0x0000000000
#define G3D_TEXTURE0_BUF_MEMORY_TYPE                           (MIU0 | TYPE_NONE | UNCACHE | TYPE_NONE)
#define G3D_TEXTURE0_BUF_CMA_HID                               0

/* LINUX_LOW_MEM_RESERVE To seperate the buffer could run in high memory */
#define LINUX_LOW_MEM_RESERVE_LAYER                            0
#define LINUX_LOW_MEM_RESERVE_AVAILABLE                        0x001AD9A000
#define LINUX_LOW_MEM_RESERVE_ADR                              0x001AD9A000  //Alignment 0x01000
#define LINUX_LOW_MEM_RESERVE_GAP_CHK                          0x0000000000
#define LINUX_LOW_MEM_RESERVE_LEN                              0x0000000000
#define LINUX_LOW_MEM_RESERVE_MEMORY_TYPE                      (MIU0 | TYPE_NONE | UNCACHE | TYPE_NONE)
#define LINUX_LOW_MEM_RESERVE_CMA_HID                          0

/* VE_FRAMEBUFFER 720 */
#define VE_FRAMEBUFFER_LAYER                                   0
#define VE_FRAMEBUFFER_AVAILABLE                               0x001AD9A000
#define VE_FRAMEBUFFER_ADR                                     0x001AD9A000  //Alignment 0x01000
#define VE_FRAMEBUFFER_GAP_CHK                                 0x0000000000
#define VE_FRAMEBUFFER_LEN                                     0x0000200000
#define VE_FRAMEBUFFER_MEMORY_TYPE                             (MIU0 | TYPE_NONE | UNCACHE | TYPE_NONE)
#define VE_FRAMEBUFFER_CMA_HID                                 0

/* TTX_BUF 200KB is enough, but MVD co-buf here for callback data, so need 256KB */
#define TTX_BUF_LAYER                                          0
#define TTX_BUF_AVAILABLE                                      0x001AF9A000
#define TTX_BUF_ADR                                            0x001AF9A000  //Alignment 0x01000
#define TTX_BUF_GAP_CHK                                        0x0000000000
#define TTX_BUF_LEN                                            0x0000000000
#define TTX_BUF_MEMORY_TYPE                                    (MIU0 | TYPE_NONE | UNCACHE | TYPE_NONE)
#define TTX_BUF_CMA_HID                                        0

/* PVR_DOWNLOAD_BUFFER 6MB */
#define PVR_DOWNLOAD_BUFFER_LAYER                              0
#define PVR_DOWNLOAD_BUFFER_AVAILABLE                          0x001AF9A000
#define PVR_DOWNLOAD_BUFFER_ADR                                0x001AF9A000  //Alignment 0x01000
#define PVR_DOWNLOAD_BUFFER_GAP_CHK                            0x0000000000
#define PVR_DOWNLOAD_BUFFER_LEN                                0x0000900000
#define PVR_DOWNLOAD_BUFFER_MEMORY_TYPE                        (MIU0 | TYPE_NONE | UNCACHE | TYPE_NONE)
#define PVR_DOWNLOAD_BUFFER_CMA_HID                            0

/* PVR_UPLOAD_BUFFER 1152KB */
#define PVR_UPLOAD_BUFFER_LAYER                                0
#define PVR_UPLOAD_BUFFER_AVAILABLE                            0x001B89A000
#define PVR_UPLOAD_BUFFER_ADR                                  0x001B89A000  //Alignment 0x01000
#define PVR_UPLOAD_BUFFER_GAP_CHK                              0x0000000000
#define PVR_UPLOAD_BUFFER_LEN                                  0x0000120000
#define PVR_UPLOAD_BUFFER_MEMORY_TYPE                          (MIU0 | TYPE_NONE | UNCACHE | TYPE_NONE)
#define PVR_UPLOAD_BUFFER_CMA_HID                              0

/* TSP_BUF 2.5MB */
#define TSP_BUF_LAYER                                          0
#define TSP_BUF_AVAILABLE                                      0x001B9BA000
#define TSP_BUF_ADR                                            0x001B9BA000  //Alignment 0x01000
#define TSP_BUF_GAP_CHK                                        0x0000000000
#define TSP_BUF_LEN                                            0x0000500000
#define TSP_BUF_MEMORY_TYPE                                    (MIU0 | TYPE_NONE | UNCACHE | TYPE_NONE)
#define TSP_BUF_CMA_HID                                        0

/* VQ_BUF 1MB */
#define VQ_BUF_LAYER                                           0
#define VQ_BUF_AVAILABLE                                       0x001BEBA000
#define VQ_BUF_ADR                                             0x001BEBA000  //Alignment 0x01000
#define VQ_BUF_GAP_CHK                                         0x0000000000
#define VQ_BUF_LEN                                             0x0000100000
#define VQ_BUF_MEMORY_TYPE                                     (MIU0 | TYPE_NONE | UNCACHE | TYPE_NONE)
#define VQ_BUF_CMA_HID                                         0

/* DIP 4KB */
#define DIP_LAYER                                              0
#define DIP_AVAILABLE                                          0x001BFBA000
#define DIP_ADR                                                0x001BFBA000  //Alignment 0x01000
#define DIP_GAP_CHK                                            0x0000000000
#define DIP_LEN                                                0x0000000000
#define DIP_MEMORY_TYPE                                        (MIU0 | TYPE_NONE | UNCACHE | TYPE_NONE)
#define DIP_CMA_HID                                            0

/* VDEC_SW_DETILE_BUF   */
#define VDEC_SW_DETILE_BUF_LAYER                               0
#define VDEC_SW_DETILE_BUF_AVAILABLE                           0x001BFBA000
#define VDEC_SW_DETILE_BUF_ADR                                 0x001BFBA000  //Alignment 0x01000
#define VDEC_SW_DETILE_BUF_GAP_CHK                             0x0000000000
#define VDEC_SW_DETILE_BUF_LEN                                 0x0000000000
#define VDEC_SW_DETILE_BUF_MEMORY_TYPE                         (MIU0 | TYPE_NONE | UNCACHE | TYPE_NONE)
#define VDEC_SW_DETILE_BUF_CMA_HID                             0

/* MPOOL 70MB */
#define MPOOL_LAYER                                            0
#define MPOOL_AVAILABLE                                        0x001BFBA000
#define MPOOL_ADR                                              0x001BFBA000  //Alignment 0x01000
#define MPOOL_GAP_CHK                                          0x0000000000
#define MPOOL_LEN                                              0x0000000000
#define MPOOL_MEMORY_TYPE                                      (MIU0 | TYPE_NONE | UNCACHE | TYPE_NONE)
#define MPOOL_CMA_HID                                          0

/* PNG_OUTPUT_BUF 0MB */
#define PNG_OUTPUT_BUF_LAYER                                   0
#define PNG_OUTPUT_BUF_AVAILABLE                               0x001BFBA000
#define PNG_OUTPUT_BUF_ADR                                     0x001BFBA000  //Alignment 0x01000
#define PNG_OUTPUT_BUF_GAP_CHK                                 0x0000000000
#define PNG_OUTPUT_BUF_LEN                                     0x0000000000
#define PNG_OUTPUT_BUF_MEMORY_TYPE                             (MIU0 | TYPE_NONE | UNCACHE | TYPE_NONE)
#define PNG_OUTPUT_BUF_CMA_HID                                 0

/* PNG_INPUT_BUF 0MB */
#define PNG_INPUT_BUF_LAYER                                    0
#define PNG_INPUT_BUF_AVAILABLE                                0x001BFBA000
#define PNG_INPUT_BUF_ADR                                      0x001BFBA000  //Alignment 0x01000
#define PNG_INPUT_BUF_GAP_CHK                                  0x0000000000
#define PNG_INPUT_BUF_LEN                                      0x0000000000
#define PNG_INPUT_BUF_MEMORY_TYPE                              (MIU0 | TYPE_NONE | UNCACHE | TYPE_NONE)
#define PNG_INPUT_BUF_CMA_HID                                  0

/* VDEC_GN_IAP 14MB */
#define VDEC_GN_IAP_LAYER                                      0
#define VDEC_GN_IAP_AVAILABLE                                  0x001BFBA000
#define VDEC_GN_IAP_ADR                                        0x001BFBA000  //Alignment 0x01000
#define VDEC_GN_IAP_GAP_CHK                                    0x0000000000
#define VDEC_GN_IAP_LEN                                        0x0000000000
#define VDEC_GN_IAP_MEMORY_TYPE                                (MIU0 | TYPE_NONE | UNCACHE | TYPE_NONE)
#define VDEC_GN_IAP_CMA_HID                                    0

/* VBI_1_BUFFER   */
#define VBI_1_BUFFER_LAYER                                     0
#define VBI_1_BUFFER_AVAILABLE                                 0x001BFBA000
#define VBI_1_BUFFER_ADR                                       0x001BFBA000  //Alignment 0x01000
#define VBI_1_BUFFER_GAP_CHK                                   0x0000000000
#define VBI_1_BUFFER_LEN                                       0x0000100000
#define VBI_1_BUFFER_MEMORY_TYPE                               (MIU0 | TYPE_NONE | UNCACHE | TYPE_NONE)
#define VBI_1_BUFFER_CMA_HID                                   0

/* VBI_2_BUFFER 1MB for SCART OUT; 0 for none */
#define VBI_2_BUFFER_LAYER                                     0
#define VBI_2_BUFFER_AVAILABLE                                 0x001C0BA000
#define VBI_2_BUFFER_ADR                                       0x001C0BA000  //Alignment 0x01000
#define VBI_2_BUFFER_GAP_CHK                                   0x0000000000
#define VBI_2_BUFFER_LEN                                       0x0000100000
#define VBI_2_BUFFER_MEMORY_TYPE                               (MIU0 | TYPE_NONE | UNCACHE | TYPE_NONE)
#define VBI_2_BUFFER_CMA_HID                                   0

/* PIU_BUF   */
#define PIU_BUF_LAYER                                          0
#define PIU_BUF_AVAILABLE                                      0x001C1BA000
#define PIU_BUF_ADR                                            0x001C1BA000  //Alignment 0x01000
#define PIU_BUF_GAP_CHK                                        0x0000000000
#define PIU_BUF_LEN                                            0x0000000000
#define PIU_BUF_MEMORY_TYPE                                    (MIU0 | TYPE_NONE | UNCACHE | TYPE_NONE)
#define PIU_BUF_CMA_HID                                        0

/* T3D_DMA_CLIENT9_BUF   */
#define T3D_DMA_CLIENT9_BUF_LAYER                              0
#define T3D_DMA_CLIENT9_BUF_AVAILABLE                          0x001C1BA000
#define T3D_DMA_CLIENT9_BUF_ADR                                0x001C1BA000  //Alignment 0x01000
#define T3D_DMA_CLIENT9_BUF_GAP_CHK                            0x0000000000
#define T3D_DMA_CLIENT9_BUF_LEN                                0x0000000000
#define T3D_DMA_CLIENT9_BUF_MEMORY_TYPE                        (MIU0 | TYPE_NONE | UNCACHE | TYPE_NONE)
#define T3D_DMA_CLIENT9_BUF_CMA_HID                            0

/* DUMMY   */
#define DUMMY_LAYER                                            0
#define DUMMY_AVAILABLE                                        0x001C1BA000
#define DUMMY_ADR                                              0x001C1BA000  //Alignment 0x01000
#define DUMMY_GAP_CHK                                          0x0000000000
#define DUMMY_LEN                                              0x0000000000
#define DUMMY_MEMORY_TYPE                                      (MIU0 | TYPE_NONE | TYPE_NONE | TYPE_NONE)
#define DUMMY_CMA_HID                                          0

/* SCALER_DNR_BUF   */
#define SCALER_DNR_BUF_LAYER                                   0
#define SCALER_DNR_BUF_AVAILABLE                               0x001C1BA000
#define SCALER_DNR_BUF_ADR                                     0x001C1BA000  //Alignment 0x01000
#define SCALER_DNR_BUF_GAP_CHK                                 0x0000000000
#define SCALER_DNR_BUF_LEN                                     0x0001800000
#define SCALER_DNR_BUF_MEMORY_TYPE                             (MIU0 | TYPE_NONE | TYPE_NONE | TYPE_NONE)
#define SCALER_DNR_BUF_CMA_HID                                 0

/* SCALER_DNR_W_BARRIER   */
#define SCALER_DNR_W_BARRIER_LAYER                             0
#define SCALER_DNR_W_BARRIER_AVAILABLE                         0x001D9BA000
#define SCALER_DNR_W_BARRIER_ADR                               0x001D9BA000  //Alignment 0x01000
#define SCALER_DNR_W_BARRIER_GAP_CHK                           0x0000000000
#define SCALER_DNR_W_BARRIER_LEN                               0x0000000000
#define SCALER_DNR_W_BARRIER_MEMORY_TYPE                       (MIU0 | TYPE_NONE | TYPE_NONE | TYPE_NONE)
#define SCALER_DNR_W_BARRIER_CMA_HID                           0

/* SCALER_PIP_BUF   */
#define SCALER_PIP_BUF_LAYER                                   0
#define SCALER_PIP_BUF_AVAILABLE                               0x001D9BA000
#define SCALER_PIP_BUF_ADR                                     0x001D9BA000  //Alignment 0x01000
#define SCALER_PIP_BUF_GAP_CHK                                 0x0000000000
#define SCALER_PIP_BUF_LEN                                     0x0000700000
#define SCALER_PIP_BUF_MEMORY_TYPE                             (MIU0 | TYPE_NONE | TYPE_NONE | TYPE_NONE)
#define SCALER_PIP_BUF_CMA_HID                                 0

/* SCALER_MAIN_FRCM_BUFFER   */
#define SCALER_MAIN_FRCM_BUFFER_LAYER                          0
#define SCALER_MAIN_FRCM_BUFFER_AVAILABLE                      0x001E0BA000
#define SCALER_MAIN_FRCM_BUFFER_ADR                            0x001E0BA000  //Alignment 0x01000
#define SCALER_MAIN_FRCM_BUFFER_GAP_CHK                        0x0000000000
#define SCALER_MAIN_FRCM_BUFFER_LEN                            0x0000000000
#define SCALER_MAIN_FRCM_BUFFER_MEMORY_TYPE                    (MIU0 | TYPE_NONE | TYPE_NONE | TYPE_NONE)
#define SCALER_MAIN_FRCM_BUFFER_CMA_HID                        0

/* SC_SUB_FRCM   */
#define SC_SUB_FRCM_LAYER                                      0
#define SC_SUB_FRCM_AVAILABLE                                  0x001E0BA000
#define SC_SUB_FRCM_ADR                                        0x001E0BA000  //Alignment 0x01000
#define SC_SUB_FRCM_GAP_CHK                                    0x0000000000
#define SC_SUB_FRCM_LEN                                        0x0000000000
#define SC_SUB_FRCM_MEMORY_TYPE                                (MIU0 | TYPE_NONE | TYPE_NONE | TYPE_NONE)
#define SC_SUB_FRCM_CMA_HID                                    0

/* SCALER_MCDI_ME1_BUFFER   */
#define SCALER_MCDI_ME1_BUFFER_LAYER                           0
#define SCALER_MCDI_ME1_BUFFER_AVAILABLE                       0x001E0BA000
#define SCALER_MCDI_ME1_BUFFER_ADR                             0x001E0BA000  //Alignment 0x01000
#define SCALER_MCDI_ME1_BUFFER_GAP_CHK                         0x0000000000
#define SCALER_MCDI_ME1_BUFFER_LEN                             0x0000000000
#define SCALER_MCDI_ME1_BUFFER_MEMORY_TYPE                     (MIU0 | TYPE_NONE | TYPE_NONE | TYPE_NONE)
#define SCALER_MCDI_ME1_BUFFER_CMA_HID                         0

/* SCALER_MCDI_ME2_BUFFER   */
#define SCALER_MCDI_ME2_BUFFER_LAYER                           0
#define SCALER_MCDI_ME2_BUFFER_AVAILABLE                       0x001E0BA000
#define SCALER_MCDI_ME2_BUFFER_ADR                             0x001E0BA000  //Alignment 0x01000
#define SCALER_MCDI_ME2_BUFFER_GAP_CHK                         0x0000000000
#define SCALER_MCDI_ME2_BUFFER_LEN                             0x0000000000
#define SCALER_MCDI_ME2_BUFFER_MEMORY_TYPE                     (MIU0 | TYPE_NONE | TYPE_NONE | TYPE_NONE)
#define SCALER_MCDI_ME2_BUFFER_CMA_HID                         0

/* SC_OD   */
#define SC_OD_LAYER                                            0
#define SC_OD_AVAILABLE                                        0x001E0BA000
#define SC_OD_ADR                                              0x001E0BA000  //Alignment 0x01000
#define SC_OD_GAP_CHK                                          0x0000000000
#define SC_OD_LEN                                              0x0000400000
#define SC_OD_MEMORY_TYPE                                      (MIU0 | TYPE_NONE | TYPE_NONE | TYPE_NONE)
#define SC_OD_CMA_HID                                          0

/* SC_BUF   */
#define SC_BUF_LAYER                                           0
#define SC_BUF_AVAILABLE                                       0x001E4BA000
#define SC_BUF_ADR                                             0x001E4BA000  //Alignment 0x01000
#define SC_BUF_GAP_CHK                                         0x0000000000
#define SC_BUF_LEN                                             0x0000000000
#define SC_BUF_MEMORY_TYPE                                     (MIU0 | TYPE_NONE | TYPE_NONE | TYPE_NONE)
#define SC_BUF_CMA_HID                                         0

/* SCALER_DYNAMIC_XC_BUFFER   */
#define SCALER_DYNAMIC_XC_BUFFER_LAYER                         0
#define SCALER_DYNAMIC_XC_BUFFER_AVAILABLE                     0x001E4BA000
#define SCALER_DYNAMIC_XC_BUFFER_ADR                           0x001E4BA000  //Alignment 0x01000
#define SCALER_DYNAMIC_XC_BUFFER_GAP_CHK                       0x0000000000
#define SCALER_DYNAMIC_XC_BUFFER_LEN                           0x0000100000
#define SCALER_DYNAMIC_XC_BUFFER_MEMORY_TYPE                   (MIU0 | TYPE_NONE | TYPE_NONE | TYPE_NONE)
#define SCALER_DYNAMIC_XC_BUFFER_CMA_HID                       0

/* SCALER_MLOAD_BUFFER   */
#define SCALER_MLOAD_BUFFER_LAYER                              0
#define SCALER_MLOAD_BUFFER_AVAILABLE                          0x001E5BA000
#define SCALER_MLOAD_BUFFER_ADR                                0x001E5BA000  //Alignment 0x01000
#define SCALER_MLOAD_BUFFER_GAP_CHK                            0x0000000000
#define SCALER_MLOAD_BUFFER_LEN                                0x0000000000
#define SCALER_MLOAD_BUFFER_MEMORY_TYPE                        (MIU0 | TYPE_NONE | TYPE_NONE | TYPE_NONE)
#define SCALER_MLOAD_BUFFER_CMA_HID                            0

/* TWOTO3D_DEPTH_DETECT_BUF   */
#define TWOTO3D_DEPTH_DETECT_BUF_LAYER                         0
#define TWOTO3D_DEPTH_DETECT_BUF_AVAILABLE                     0x001E5BA000
#define TWOTO3D_DEPTH_DETECT_BUF_ADR                           0x001E5BA000  //Alignment 0x01000
#define TWOTO3D_DEPTH_DETECT_BUF_GAP_CHK                       0x0000000000
#define TWOTO3D_DEPTH_DETECT_BUF_LEN                           0x0000000000
#define TWOTO3D_DEPTH_DETECT_BUF_MEMORY_TYPE                   (MIU0 | TYPE_NONE | TYPE_NONE | TYPE_NONE)
#define TWOTO3D_DEPTH_DETECT_BUF_CMA_HID                       0

/* TWOTO3D_DEPTH_RENDER_BUF   */
#define TWOTO3D_DEPTH_RENDER_BUF_LAYER                         0
#define TWOTO3D_DEPTH_RENDER_BUF_AVAILABLE                     0x001E5BA000
#define TWOTO3D_DEPTH_RENDER_BUF_ADR                           0x001E5BA000  //Alignment 0x01000
#define TWOTO3D_DEPTH_RENDER_BUF_GAP_CHK                       0x0000000000
#define TWOTO3D_DEPTH_RENDER_BUF_LEN                           0x0000000000
#define TWOTO3D_DEPTH_RENDER_BUF_MEMORY_TYPE                   (MIU0 | TYPE_NONE | TYPE_NONE | TYPE_NONE)
#define TWOTO3D_DEPTH_RENDER_BUF_CMA_HID                       0

/* SC2_MAIN_FB   */
#define SC2_MAIN_FB_LAYER                                      0
#define SC2_MAIN_FB_AVAILABLE                                  0x001E5BA000
#define SC2_MAIN_FB_ADR                                        0x001E5BA000  //Alignment 0x01000
#define SC2_MAIN_FB_GAP_CHK                                    0x0000000000
#define SC2_MAIN_FB_LEN                                        0x0000000000
#define SC2_MAIN_FB_MEMORY_TYPE                                (MIU0 | TYPE_NONE | TYPE_NONE | TYPE_NONE)
#define SC2_MAIN_FB_CMA_HID                                    0

/* LD_BUFFER   */
#define LD_BUFFER_LAYER                                        0
#define LD_BUFFER_AVAILABLE                                    0x001E5BA000
#define LD_BUFFER_ADR                                          0x001E5BA000  //Alignment 0x01000
#define LD_BUFFER_GAP_CHK                                      0x0000000000
#define LD_BUFFER_LEN                                          0x0000100000
#define LD_BUFFER_MEMORY_TYPE                                  (MIU0 | TYPE_NONE | TYPE_NONE | TYPE_NONE)
#define LD_BUFFER_CMA_HID                                      0

/* ISDBT_TDI 7MB */
#define ISDBT_TDI_LAYER                                        0
#define ISDBT_TDI_AVAILABLE                                    0x001E6BA000
#define ISDBT_TDI_ADR                                          0x001E6BA000  //Alignment 0x01000
#define ISDBT_TDI_GAP_CHK                                      0x0000000000
#define ISDBT_TDI_LEN                                          0x0000000000
#define ISDBT_TDI_MEMORY_TYPE                                  (MIU0 | TYPE_NONE | UNCACHE | TYPE_NONE)
#define ISDBT_TDI_CMA_HID                                      0

/* DTMB_TDI 7MB */
#define DTMB_TDI_LAYER                                         1
#define DTMB_TDI_AVAILABLE                                     0x001E6BA000
#define DTMB_TDI_ADR                                           0x001E6BA000  //Alignment 0x01000
#define DTMB_TDI_GAP_CHK                                       0x0000000000
#define DTMB_TDI_LEN                                           0x0000000000
#define DTMB_TDI_MEMORY_TYPE                                   (MIU0 | TYPE_NONE | UNCACHE | TYPE_NONE)
#define DTMB_TDI_CMA_HID                                       0

/* NFDRM_NUTTX_MEM 16MB, 64K alignment */
#define NFDRM_NUTTX_MEM_LAYER                                  0
#define NFDRM_NUTTX_MEM_AVAILABLE                              0x001E6BA000
#define NFDRM_NUTTX_MEM_ADR                                    0x001E700000  //Alignment 0x100000
#define NFDRM_NUTTX_MEM_GAP_CHK                                0x0000046000
#define NFDRM_NUTTX_MEM_LEN                                    0x0001000000
#define NFDRM_NUTTX_MEM_MEMORY_TYPE                            (MIU0 | TYPE_NONE | UNCACHE | TYPE_NONE)
#define NFDRM_NUTTX_MEM_CMA_HID                                0

/* NFDRM_HW_AES_BUF 768KB */
#define NFDRM_HW_AES_BUF_LAYER                                 0
#define NFDRM_HW_AES_BUF_AVAILABLE                             0x001F700000
#define NFDRM_HW_AES_BUF_ADR                                   0x001F700000  //Alignment 0x100000
#define NFDRM_HW_AES_BUF_GAP_CHK                               0x0000000000
#define NFDRM_HW_AES_BUF_LEN                                   0x0000100000
#define NFDRM_HW_AES_BUF_MEMORY_TYPE                           (MIU0 | TYPE_NONE | UNCACHE | TYPE_NONE)
#define NFDRM_HW_AES_BUF_CMA_HID                               0

/* HW_SECURE_BUFFER   */
#define HW_SECURE_BUFFER_LAYER                                 0
#define HW_SECURE_BUFFER_AVAILABLE                             0x001F800000
#define HW_SECURE_BUFFER_ADR                                   0x001F800000  //Alignment 0x100000
#define HW_SECURE_BUFFER_GAP_CHK                               0x0000000000
#define HW_SECURE_BUFFER_LEN                                   0x0000400000
#define HW_SECURE_BUFFER_MEMORY_TYPE                           (MIU0 | TYPE_NONE | UNCACHE | TYPE_NONE)
#define HW_SECURE_BUFFER_CMA_HID                               0

/* SECURE_SHM   */
#define SECURE_SHM_LAYER                                       0
#define SECURE_SHM_AVAILABLE                                   0x001FC00000
#define SECURE_SHM_ADR                                         0x001FC00000  //Alignment 0x100000
#define SECURE_SHM_GAP_CHK                                     0x0000000000
#define SECURE_SHM_LEN                                         0x0000200000
#define SECURE_SHM_MEMORY_TYPE                                 (MIU0 | TYPE_NONE | UNCACHE | TYPE_NONE)
#define SECURE_SHM_CMA_HID                                     0

/* SECURE_UPDATE_AREA   */
#define SECURE_UPDATE_AREA_LAYER                               0
#define SECURE_UPDATE_AREA_AVAILABLE                           0x001FE00000
#define SECURE_UPDATE_AREA_ADR                                 0x001FE00000  //Alignment 0x100000
#define SECURE_UPDATE_AREA_GAP_CHK                             0x0000000000
#define SECURE_UPDATE_AREA_LEN                                 0x0000100000
#define SECURE_UPDATE_AREA_MEMORY_TYPE                         (MIU0 | TYPE_NONE | UNCACHE | TYPE_NONE)
#define SECURE_UPDATE_AREA_CMA_HID                             0

/* HWI2C_DMA 1MB */
#define HWI2C_DMA_LAYER                                        0
#define HWI2C_DMA_AVAILABLE                                    0x001FF00000
#define HWI2C_DMA_ADR                                          0x001FF00000  //Alignment 0x100000
#define HWI2C_DMA_GAP_CHK                                      0x0000000000
#define HWI2C_DMA_LEN                                          0x0000100000
#define HWI2C_DMA_MEMORY_TYPE                                  (MIU0 | TYPE_NONE | UNCACHE | TYPE_NONE)
#define HWI2C_DMA_CMA_HID                                      0

//MIU_1_START
/* GOP_FRAMEBUFFER 40MB */
#define GOP_FRAMEBUFFER_LAYER                                  0
#define GOP_FRAMEBUFFER_AVAILABLE                              0x0000000000
#define GOP_FRAMEBUFFER_ADR                                    0x0000000000  //Alignment 0x01000
#define GOP_FRAMEBUFFER_GAP_CHK                                0x0000000000
#define GOP_FRAMEBUFFER_LEN                                    0x0002800000
#define GOP_FRAMEBUFFER_MEMORY_TYPE                            (MIU1 | TYPE_NONE | UNCACHE | TYPE_NONE)
#define GOP_FRAMEBUFFER_CMA_HID                                0

/* DUAL_STREAM_DUMY 53MB */
#define DUAL_STREAM_DUMY_LAYER                                 0
#define DUAL_STREAM_DUMY_AVAILABLE                             0x0002800000
#define DUAL_STREAM_DUMY_ADR                                   0x0002800000  //Alignment 0x01000
#define DUAL_STREAM_DUMY_GAP_CHK                               0x0000000000
#define DUAL_STREAM_DUMY_LEN                                   0x0003500000
#define DUAL_STREAM_DUMY_MEMORY_TYPE                           (MIU1 | TYPE_NONE | UNCACHE | TYPE_NONE)
#define DUAL_STREAM_DUMY_CMA_HID                               0

/* VDEC_CPU 2MB */
#define VDEC_CPU_LAYER                                         1
#define VDEC_CPU_AVAILABLE                                     0x0002800000
#define VDEC_CPU_ADR                                           0x0002800000  //Alignment 0x01000
#define VDEC_CPU_GAP_CHK                                       0x0000000000
#define VDEC_CPU_LEN                                           0x0000200000
#define VDEC_CPU_MEMORY_TYPE                                   (MIU1 | TYPE_NONE | UNCACHE | TYPE_NONE)
#define VDEC_CPU_CMA_HID                                       0

/* VDEC_BITSTREAM 8MB */
#define VDEC_BITSTREAM_LAYER                                   1
#define VDEC_BITSTREAM_AVAILABLE                               0x0002A00000
#define VDEC_BITSTREAM_ADR                                     0x0002A00000  //Alignment 0x01000
#define VDEC_BITSTREAM_GAP_CHK                                 0x0000000000
#define VDEC_BITSTREAM_LEN                                     0x0000800000
#define VDEC_BITSTREAM_MEMORY_TYPE                             (MIU1 | TYPE_NONE | UNCACHE | TYPE_NONE)
#define VDEC_BITSTREAM_CMA_HID                                 0

/* VDEC_N1_BITSTREAM 0MB */
#define VDEC_N1_BITSTREAM_LAYER                                1
#define VDEC_N1_BITSTREAM_AVAILABLE                            0x0003200000
#define VDEC_N1_BITSTREAM_ADR                                  0x0003200000  //Alignment 0x01000
#define VDEC_N1_BITSTREAM_GAP_CHK                              0x0000000000
#define VDEC_N1_BITSTREAM_LEN                                  0x0000000000
#define VDEC_N1_BITSTREAM_MEMORY_TYPE                          (MIU1 | TYPE_NONE | UNCACHE | TYPE_NONE)
#define VDEC_N1_BITSTREAM_CMA_HID                              0

/* VDEC_N2_BITSTREAM 0MB */
#define VDEC_N2_BITSTREAM_LAYER                                1
#define VDEC_N2_BITSTREAM_AVAILABLE                            0x0003200000
#define VDEC_N2_BITSTREAM_ADR                                  0x0003200000  //Alignment 0x01000
#define VDEC_N2_BITSTREAM_GAP_CHK                              0x0000000000
#define VDEC_N2_BITSTREAM_LEN                                  0x0000000000
#define VDEC_N2_BITSTREAM_MEMORY_TYPE                          (MIU1 | TYPE_NONE | UNCACHE | TYPE_NONE)
#define VDEC_N2_BITSTREAM_CMA_HID                              0

/* VDEC_FRAMEBUFFER 43MB */
#define VDEC_FRAMEBUFFER_LAYER                                 1
#define VDEC_FRAMEBUFFER_AVAILABLE                             0x0003200000
#define VDEC_FRAMEBUFFER_ADR                                   0x0003200000  //Alignment 0x01000
#define VDEC_FRAMEBUFFER_GAP_CHK                               0x0000000000
#define VDEC_FRAMEBUFFER_LEN                                   0x0002B00000
#define VDEC_FRAMEBUFFER_MEMORY_TYPE                           (MIU1 | TYPE_NONE | UNCACHE | TYPE_NONE)
#define VDEC_FRAMEBUFFER_CMA_HID                               0

/* VDEC_SUB_FRAMEBUFFER VDEC_SUB_FRAMEBUFFER 38MB */
#define VDEC_SUB_FRAMEBUFFER_LAYER                             1
#define VDEC_SUB_FRAMEBUFFER_AVAILABLE                         0x0005D00000
#define VDEC_SUB_FRAMEBUFFER_ADR                               0x0005D00000  //Alignment 0x01000
#define VDEC_SUB_FRAMEBUFFER_GAP_CHK                           0x0000000000
#define VDEC_SUB_FRAMEBUFFER_LEN                               0x0000000000
#define VDEC_SUB_FRAMEBUFFER_MEMORY_TYPE                       (MIU1 | TYPE_NONE | UNCACHE | TYPE_NONE)
#define VDEC_SUB_FRAMEBUFFER_CMA_HID                           0

/* SVD_CPU 2MB */
#define SVD_CPU_LAYER                                          2
#define SVD_CPU_AVAILABLE                                      0x0002800000
#define SVD_CPU_ADR                                            0x0002800000  //Alignment 0x01000
#define SVD_CPU_GAP_CHK                                        0x0000000000
#define SVD_CPU_LEN                                            0x0000200000
#define SVD_CPU_MEMORY_TYPE                                    (MIU1 | TYPE_NONE | UNCACHE | TYPE_NONE)
#define SVD_CPU_CMA_HID                                        0

/* VDEC_DUMMY 8MB */
#define VDEC_DUMMY_LAYER                                       2
#define VDEC_DUMMY_AVAILABLE                                   0x0002A00000
#define VDEC_DUMMY_ADR                                         0x0002A00000  //Alignment 0x01000
#define VDEC_DUMMY_GAP_CHK                                     0x0000000000
#define VDEC_DUMMY_LEN                                         0x0000800000
#define VDEC_DUMMY_MEMORY_TYPE                                 (MIU1 | TYPE_NONE | UNCACHE | TYPE_NONE)
#define VDEC_DUMMY_CMA_HID                                     0

/* VDEC_SUB_BITSTREAM 8MB */
#define VDEC_SUB_BITSTREAM_LAYER                               2
#define VDEC_SUB_BITSTREAM_AVAILABLE                           0x0003200000
#define VDEC_SUB_BITSTREAM_ADR                                 0x0003200000  //Alignment 0x01000
#define VDEC_SUB_BITSTREAM_GAP_CHK                             0x0000000000
#define VDEC_SUB_BITSTREAM_LEN                                 0x0000800000
#define VDEC_SUB_BITSTREAM_MEMORY_TYPE                         (MIU1 | TYPE_NONE | UNCACHE | TYPE_NONE)
#define VDEC_SUB_BITSTREAM_CMA_HID                             0

/* JPD_FRAMEBUFFER 30MB */
#define JPD_FRAMEBUFFER_LAYER                                  2
#define JPD_FRAMEBUFFER_AVAILABLE                              0x0003A00000
#define JPD_FRAMEBUFFER_ADR                                    0x0003A00000  //Alignment 0x01000
#define JPD_FRAMEBUFFER_GAP_CHK                                0x0000000000
#define JPD_FRAMEBUFFER_LEN                                    0x0000800000
#define JPD_FRAMEBUFFER_MEMORY_TYPE                            (MIU1 | TYPE_NONE | UNCACHE | TYPE_NONE)
#define JPD_FRAMEBUFFER_CMA_HID                                0

/* JPD_OUTPUT_BUF 16MB */
#define JPD_OUTPUT_BUF_LAYER                                   2
#define JPD_OUTPUT_BUF_AVAILABLE                               0x0004200000
#define JPD_OUTPUT_BUF_ADR                                     0x0004200000  //Alignment 0x01000
#define JPD_OUTPUT_BUF_GAP_CHK                                 0x0000000000
#define JPD_OUTPUT_BUF_LEN                                     0x0001000000
#define JPD_OUTPUT_BUF_MEMORY_TYPE                             (MIU1 | TYPE_NONE | UNCACHE | TYPE_NONE)
#define JPD_OUTPUT_BUF_CMA_HID                                 0

/* NFDRM_VDEC_SHARE_MEM 1MB */
#define NFDRM_VDEC_SHARE_MEM_LAYER                             0
#define NFDRM_VDEC_SHARE_MEM_AVAILABLE                         0x0005D00000
#define NFDRM_VDEC_SHARE_MEM_ADR                               0x0005D00000  //Alignment 0x01000
#define NFDRM_VDEC_SHARE_MEM_GAP_CHK                           0x0000000000
#define NFDRM_VDEC_SHARE_MEM_LEN                               0x0000100000
#define NFDRM_VDEC_SHARE_MEM_MEMORY_TYPE                       (MIU1 | TYPE_NONE | UNCACHE | TYPE_NONE)
#define NFDRM_VDEC_SHARE_MEM_CMA_HID                           0

/* JPD_THUMBNAIL 0MB */
#define JPD_THUMBNAIL_LAYER                                    0
#define JPD_THUMBNAIL_AVAILABLE                                0x0005E00000
#define JPD_THUMBNAIL_ADR                                      0x0005E00000  //Alignment 0x01000
#define JPD_THUMBNAIL_GAP_CHK                                  0x0000000000
#define JPD_THUMBNAIL_LEN                                      0x0000000000
#define JPD_THUMBNAIL_MEMORY_TYPE                              (MIU1 | TYPE_NONE | UNCACHE | TYPE_NONE)
#define JPD_THUMBNAIL_CMA_HID                                  0

/* POSD0 0MB */
#define POSD0_LAYER                                            0
#define POSD0_AVAILABLE                                        0x0005E00000
#define POSD0_ADR                                              0x0005E00000  //Alignment 0x01000
#define POSD0_GAP_CHK                                          0x0000000000
#define POSD0_LEN                                              0x0000000000
#define POSD0_MEMORY_TYPE                                      (MIU1 | TYPE_NONE | UNCACHE | TYPE_NONE)
#define POSD0_CMA_HID                                          0

/* POSD1 0 */
#define POSD1_LAYER                                            0
#define POSD1_AVAILABLE                                        0x0005E00000
#define POSD1_ADR                                              0x0005E00000  //Alignment 0x01000
#define POSD1_GAP_CHK                                          0x0000000000
#define POSD1_LEN                                              0x0000000000
#define POSD1_MEMORY_TYPE                                      (MIU1 | TYPE_NONE | UNCACHE | TYPE_NONE)
#define POSD1_CMA_HID                                          0

/* AUDIO_CLIP 4MB */
#define AUDIO_CLIP_LAYER                                       0
#define AUDIO_CLIP_AVAILABLE                                   0x0005E00000
#define AUDIO_CLIP_ADR                                         0x0005E00000  //Alignment 0x01000
#define AUDIO_CLIP_GAP_CHK                                     0x0000000000
#define AUDIO_CLIP_LEN                                         0x0000400000
#define AUDIO_CLIP_MEMORY_TYPE                                 (MIU1 | TYPE_NONE | UNCACHE | TYPE_NONE)
#define AUDIO_CLIP_CMA_HID                                     0

/* LD_BUF 0MB */
#define LD_BUF_LAYER                                           0
#define LD_BUF_AVAILABLE                                       0x0006200000
#define LD_BUF_ADR                                             0x0006200000  //Alignment 0x01000
#define LD_BUF_GAP_CHK                                         0x0000000000
#define LD_BUF_LEN                                             0x0000000000
#define LD_BUF_MEMORY_TYPE                                     (MIU1 | TYPE_NONE | UNCACHE | TYPE_NONE)
#define LD_BUF_CMA_HID                                         0

/* BB 0KB */
#define BB_LAYER                                               0
#define BB_AVAILABLE                                           0x0006200000
#define BB_ADR                                                 0x0006200000  //Alignment 0x01000
#define BB_GAP_CHK                                             0x0000000000
#define BB_LEN                                                 0x0000000000
#define BB_MEMORY_TYPE                                         (MIU1 | TYPE_NONE | UNCACHE | TYPE_NONE)
#define BB_CMA_HID                                             0

/* DEBUG 0KB */
#define DEBUG_LAYER                                            0
#define DEBUG_AVAILABLE                                        0x0006200000
#define DEBUG_ADR                                              0x0006200000  //Alignment 0x01000
#define DEBUG_GAP_CHK                                          0x0000000000
#define DEBUG_LEN                                              0x0000000000
#define DEBUG_MEMORY_TYPE                                      (MIU1 | TYPE_NONE | UNCACHE | TYPE_NONE)
#define DEBUG_CMA_HID                                          0

/* BT_POOL 0MB */
#define BT_POOL_LAYER                                          0
#define BT_POOL_AVAILABLE                                      0x0006200000
#define BT_POOL_ADR                                            0x0006200000  //Alignment 0x01000
#define BT_POOL_GAP_CHK                                        0x0000000000
#define BT_POOL_LEN                                            0x0000000000
#define BT_POOL_MEMORY_TYPE                                    (MIU1 | TYPE_NONE | UNCACHE | TYPE_NONE)
#define BT_POOL_CMA_HID                                        0

/* APVR_BUF 1MB */
#define APVR_BUF_LAYER                                         0
#define APVR_BUF_AVAILABLE                                     0x0006200000
#define APVR_BUF_ADR                                           0x0006200000  //Alignment 0x01000
#define APVR_BUF_GAP_CHK                                       0x0000000000
#define APVR_BUF_LEN                                           0x0000000000
#define APVR_BUF_MEMORY_TYPE                                   (MIU1 | TYPE_NONE | UNCACHE | TYPE_NONE)
#define APVR_BUF_CMA_HID                                       0

/* VIDEO_CONF 1.35MB(720x480x2x2) */
#define VIDEO_CONF_LAYER                                       0
#define VIDEO_CONF_AVAILABLE                                   0x0006200000
#define VIDEO_CONF_ADR                                         0x0006200000  //Alignment 0x01000
#define VIDEO_CONF_GAP_CHK                                     0x0000000000
#define VIDEO_CONF_LEN                                         0x0000000000
#define VIDEO_CONF_MEMORY_TYPE                                 (MIU1 | TYPE_NONE | UNCACHE | TYPE_NONE)
#define VIDEO_CONF_CMA_HID                                     0

/* G3D_CMD_Q_BUF 4MB */
#define G3D_CMD_Q_BUF_LAYER                                    0
#define G3D_CMD_Q_BUF_AVAILABLE                                0x0006200000
#define G3D_CMD_Q_BUF_ADR                                      0x0006200000  //Alignment 0x01000
#define G3D_CMD_Q_BUF_GAP_CHK                                  0x0000000000
#define G3D_CMD_Q_BUF_LEN                                      0x0000000000
#define G3D_CMD_Q_BUF_MEMORY_TYPE                              (MIU1 | TYPE_NONE | UNCACHE | TYPE_NONE)
#define G3D_CMD_Q_BUF_CMA_HID                                  0

/* G3D_VERTEX_BUF 16MB */
#define G3D_VERTEX_BUF_LAYER                                   0
#define G3D_VERTEX_BUF_AVAILABLE                               0x0006200000
#define G3D_VERTEX_BUF_ADR                                     0x0006200000  //Alignment 0x01000
#define G3D_VERTEX_BUF_GAP_CHK                                 0x0000000000
#define G3D_VERTEX_BUF_LEN                                     0x0000000000
#define G3D_VERTEX_BUF_MEMORY_TYPE                             (MIU1 | TYPE_NONE | UNCACHE | TYPE_NONE)
#define G3D_VERTEX_BUF_CMA_HID                                 0

/* G3D_COLOR_BUF 16MB */
#define G3D_COLOR_BUF_LAYER                                    0
#define G3D_COLOR_BUF_AVAILABLE                                0x0006200000
#define G3D_COLOR_BUF_ADR                                      0x0006200000  //Alignment 0x01000
#define G3D_COLOR_BUF_GAP_CHK                                  0x0000000000
#define G3D_COLOR_BUF_LEN                                      0x0000000000
#define G3D_COLOR_BUF_MEMORY_TYPE                              (MIU1 | TYPE_NONE | UNCACHE | TYPE_NONE)
#define G3D_COLOR_BUF_CMA_HID                                  0

/* G3D_TEXTURE1_BUF 32MB */
#define G3D_TEXTURE1_BUF_LAYER                                 0
#define G3D_TEXTURE1_BUF_AVAILABLE                             0x0006200000
#define G3D_TEXTURE1_BUF_ADR                                   0x0006200000  //Alignment 0x01000
#define G3D_TEXTURE1_BUF_GAP_CHK                               0x0000000000
#define G3D_TEXTURE1_BUF_LEN                                   0x0000000000
#define G3D_TEXTURE1_BUF_MEMORY_TYPE                           (MIU1 | TYPE_NONE | UNCACHE | TYPE_NONE)
#define G3D_TEXTURE1_BUF_CMA_HID                               0

/* GPD_ES_BUF 2MB */
#define GPD_ES_BUF_LAYER                                       0
#define GPD_ES_BUF_AVAILABLE                                   0x0006200000
#define GPD_ES_BUF_ADR                                         0x0006200000  //Alignment 0x01000
#define GPD_ES_BUF_GAP_CHK                                     0x0000000000
#define GPD_ES_BUF_LEN                                         0x0000000000
#define GPD_ES_BUF_MEMORY_TYPE                                 (MIU1 | TYPE_NONE | UNCACHE | TYPE_NONE)
#define GPD_ES_BUF_CMA_HID                                     0

/* GPD_OUTPUT_BUF 16MB */
#define GPD_OUTPUT_BUF_LAYER                                   0
#define GPD_OUTPUT_BUF_AVAILABLE                               0x0006200000
#define GPD_OUTPUT_BUF_ADR                                     0x0006200000  //Alignment 0x01000
#define GPD_OUTPUT_BUF_GAP_CHK                                 0x0000000000
#define GPD_OUTPUT_BUF_LEN                                     0x0000000000
#define GPD_OUTPUT_BUF_MEMORY_TYPE                             (MIU1 | TYPE_NONE | UNCACHE | TYPE_NONE)
#define GPD_OUTPUT_BUF_CMA_HID                                 0

/* GOP_REGDMA 1MB */
#define GOP_REGDMA_LAYER                                       0
#define GOP_REGDMA_AVAILABLE                                   0x0006200000
#define GOP_REGDMA_ADR                                         0x0006200000  //Alignment 0x01000
#define GOP_REGDMA_GAP_CHK                                     0x0000000000
#define GOP_REGDMA_LEN                                         0x0000100000
#define GOP_REGDMA_MEMORY_TYPE                                 (MIU1 | TYPE_NONE | UNCACHE | TYPE_NONE)
#define GOP_REGDMA_CMA_HID                                     0

/* GFX_VQ_BUFFER 0MB */
#define GFX_VQ_BUFFER_LAYER                                    0
#define GFX_VQ_BUFFER_AVAILABLE                                0x0006300000
#define GFX_VQ_BUFFER_ADR                                      0x0006300000  //Alignment 0x01000
#define GFX_VQ_BUFFER_GAP_CHK                                  0x0000000000
#define GFX_VQ_BUFFER_LEN                                      0x0000000000
#define GFX_VQ_BUFFER_MEMORY_TYPE                              (MIU1 | TYPE_NONE | UNCACHE | TYPE_NONE)
#define GFX_VQ_BUFFER_CMA_HID                                  0

/* LX_MEM1 157MB */
#define LX_MEM1_LAYER                                          0
#define LX_MEM1_AVAILABLE                                      0x0006300000
#define LX_MEM1_ADR                                            0x0006300000  //Alignment 0x01000
#define LX_MEM1_GAP_CHK                                        0x0000000000
#define LX_MEM1_LEN                                            0x0009D00000
#define LX_MEM1_MEMORY_TYPE                                    (MIU1 | TYPE_NONE | UNCACHE | TYPE_NONE)
#define LX_MEM1_CMA_HID                                        0

//MIU_END

#define MIU0_END_ADR                                           0x0020000000
#define MIU1_END_ADR                                           0x0010000000
#define MMAP_COUNT                                             0x0000000059

#define TEST_4K_ALIGN                   1

/* CHK_VALUE = 154908392 */

