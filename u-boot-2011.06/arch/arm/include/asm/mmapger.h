#ifndef _MMAPGER__H
#define _MMAPGER__H


#ifndef CONFIG_MSTAR_CMA
#define MMAP_ID_MAX 103
#else
#define MMAP_ID_MAX 102
#endif

typedef enum {
    DUMMY_MEM,
    LINUX_MEM,
    EMAC_MEM,
    XIP_MEM,
    NFDRM_NUTTX_MEM,
    NFDRM_HW_AES_BUF,
    HW_SECURE_BUFFER,
    SECURE_SHM,
    SECURE_UPDATE_AREA,
    SECURE_TSP,
    BIN_MEM,
    MAD_BASE_BUFFER,
    VD_3DCOMB,
    G3D_TEXTURE0_BUF,
    LINUX_LOW_MEM_RESERVE,
    VE_FRAMEBUFFER,
    TTX_BUF,
    PVR_DOWNLOAD_BUFFER,
    PVR_UPLOAD_BUFFER,
    TSP_BUF,
    VQ_BUF,
    MAILBOX,
    DIP,
    MPOOL,
    PNG_OUTPUT_BUF,
    PNG_INPUT_BUF,
    VDEC_GN_IAP,
    GOP_FRAMEBUFFER,
    GOP_REGDMA,
    GFX_VQ_BUFFER,
    VBI_1_BUFFER,
    VBI_2_BUFFER,
    LD_BUFFER2,
    PIU_BUF,
    DUMMY,
    DUAL_STREAM_DUMY,
    VDEC_CPU,
    SVD_CPU,
    VDEC_BITSTREAM,
    VDEC_SUB_BITSTREAM,
    VDEC_N1_BITSTREAM,
    VDEC_N2_BITSTREAM,
    VDEC_FRAMEBUFFER,
    VDEC_SUB_FRAMEBUFFER,
    NFDRM_VDEC_SHARE_MEM,
    JPD_THUMBNAIL,
    POSD0,
    POSD1,
    AUDIO_CLIP,
    PVR_THUMBNAIL_DECODE_BUFFER,
    LD_BUF,
    BB,
    DEBUG,
    BT_POOL,
    APVR_BUF,
    VIDEO_CONF,
    G3D_CMD_Q_BUF,
    G3D_VERTEX_BUF,
    G3D_COLOR_BUF,
    G3D_TEXTURE1_BUF,
    GPD_ES_BUF,
    GPD_OUTPUT_BUF,
    ISDBT_TDI,
    VE_DIPW_BUF,
    MFE_BUF,
    MFE_OUT_BUF,
    VIDEO_IN_CAPTURE_TMP,
    LX_MEM1,
    DTMB_TDI,
    SCALER_DNR_BUF,
    SCALER_DNR_W_BARRIER,
    SCALER_PIP_BUF,
    SCALER_MAIN_FRCM_BUFFER,
    SC_SUB_FRCM,
    SCALER_MCDI_ME1_BUFFER,
    SCALER_MCDI_ME2_BUFFER,
    SC_OD,
    SC_BUF,
    SCALER_MLOAD_BUFFER,
    SCALER_DYNAMIC_XC_BUFFER,
    TWOTO3D_DEPTH_DETECT_BUF,
    TWOTO3D_DEPTH_RENDER_BUF,
    T3D_DMA_CLIENT9_BUF,
    SC2_MAIN_FB,
    VDEC_DUMMY,
    JPD_FRAMEBUFFER,
    E_MMAP_ID_VDEC_FRAMEBUFFER_BALANCE_BW,
    HWI2C_DMA,
    E_MMAP_ID_FRC_R2,
    E_MMAP_ID_XC_DOLBY_HDR,
    E_MMAP_ID_CFD_MLOAD,
    E_MMAP_ID_XC_MLOAD2,
    E_MMAP_ID_XC_DMA,
    E_MMAP_ID_XC_FRC_R,
    E_MMAP_ID_XC_FRC_L,
    E_MMAP_ID_XC_FRC_PQ,
    E_MMAP_ID_XC_AUTODOWNLOAD,
    E_MMAP_ID_XC_AUTODOWNLOAD_XVYCC,
    E_MMAP_ID_XC_AUTODOWNLOAD_ODGAMMA,
    E_MMAP_ID_XC_VIRTUAL_REGISTER,
    E_MMAP_ID_XC_DUMMY,
#ifndef CONFIG_MSTAR_CMA
    CIPLUS_DOWNLOAD,
    CIPLUS_UPLOAD,
#else
    CIPLUS_BUFFER,
#endif


    } MMAPGER_ID;


typedef struct MMapInfo_s
{
    unsigned int u32MemAvailableAddr;
    unsigned int u32MemAddr;
    unsigned int u32MemGapCheckAddr;
    unsigned int u32MemLength;
    unsigned int u32MemType;
} MMapInfo_t;


typedef struct MiuInfo_s
{
    unsigned int u32EnableMiu_1;
    unsigned int u32EnableMiu_2;
    unsigned int u32MiuDramLen;
    unsigned int u32MiuDramLen_0;
    unsigned int u32MiuDramLen_1;
    unsigned int u32MiuDramLen_2;
    unsigned int u32MiuInterval;
    unsigned int u32CpuAlign;
    unsigned int u32Miu0EndAddr;
    unsigned int u32Miu1EndAddr;
    unsigned int u32MmapCount;

} MiuInfo_t;

#ifdef CONFIG_MSTAR_CMA
#define CMA_MMAP_ID_MAX 5

typedef struct CMA_MMapInfo_s
{
    char *pCMA_name;
    unsigned int u32MemAddr;
    unsigned int u32MemLength;
    unsigned int u32MemType;
    unsigned int u32CMA_HID;
} CMA_MMapInfo_t;
#endif /* CONFIG_MSTAR_CMA */

#endif /* _MMAPGER__H */
