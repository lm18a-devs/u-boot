/*
 * (C) Copyright 2002
 * Sysgo Real-Time Solutions, GmbH <www.elinos.com>
 * Marius Groeger <mgroeger@sysgo.de>
 *
 * Copyright (C) 2001  Erik Mouw (J.A.K.Mouw@its.tudelft.nl)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

#include <common.h>
#include <command.h>
#include <image.h>
#include <u-boot/zlib.h>
#include <asm/byteorder.h>
#include <fdt.h>
#include <libfdt.h>
#include <fdt_support.h>
#include <MsApiMiu.h>
#include "MsEnvironment.h"
#include <cmnio_type.h> // nvmdb
#include <MsEnvironment.h>
#include <cmd_micom.h>
#include "drvSYS.h"
#include <version.h>
#include <swum.h>

#ifdef CFG_LG_CHG
#include <lg_modeldef.h>
#include <mmc.h>
#endif

#ifdef CONFIG_HIBERNATION
#include <cmd_resume.h>
#endif

#ifdef CONFIG_CUSTOM_TAG
#include <asm/mmapger.h>
#include <CusCmnio.h>
extern MMapInfo_t MMAPGer[MMAP_ID_MAX];
extern MiuInfo_t  MiuInfo;
extern MODELOPT_T gModelOpt;
#endif

#if defined(LG_CHG)
#ifdef CONFIG_SECURITY_BOOT
#include <MsSecureBoot.h>
#endif
#endif
#ifdef CONFIG_MSTAR_CMA
extern CMA_MMapInfo_t CMAInfo[CMA_MMAP_ID_MAX];
#endif

#define NFS_PARAMETER           "nolock,tcp,rsize=4096,wsize=4096"

DECLARE_GLOBAL_DATA_PTR;

#if defined (CONFIG_SETUP_MEMORY_TAGS) || \
    defined (CONFIG_CMDLINE_TAG) || \
    defined (CONFIG_INITRD_TAG) || \
    defined (CONFIG_SERIAL_TAG) || \
    defined (CONFIG_REVISION_TAG)
static void setup_start_tag (bd_t *bd);

# ifdef CONFIG_SETUP_MEMORY_TAGS
static void setup_memory_tags (bd_t *bd);
# endif
static void setup_commandline_tag (bd_t *bd, char *commandline);

# ifdef CONFIG_CUSTOM_TAG
static void setup_custom_tag(bd_t *bd);
# endif

# ifdef CONFIG_INITRD_TAG
static void setup_initrd_tag (bd_t *bd, ulong initrd_start,
                  ulong initrd_end);
# endif
static void setup_end_tag (bd_t *bd);

static struct tag *params;
#endif /* CONFIG_SETUP_MEMORY_TAGS || CONFIG_CMDLINE_TAG || CONFIG_INITRD_TAG */

static ulong get_sp(void);
#if defined(CONFIG_OF_LIBFDT)
static int bootm_linux_fdt(int machid, bootm_headers_t *images);
#endif

#ifdef CFG_LG_CHG
#define arg_next(p)             (char *)((unsigned long)p+strlen(p))

extern uint32_t appxip_len;             /* app xip length */
extern uint32_t fontxip_len;            /* font xip length */

extern char strModelOpt[];
extern char strHWOption[];
extern UINT32 gToolOpt[];
extern MODEL_INFO_DB_T gModelInfoDB;
extern char default_env[DEFAULTENV_MAX_NUM][DEFAULTENV_MAX_LENGTH];

#if 0 //USE_LG_SNAPSHOT_BOOT
int check_snapshot_bootmode(void);
#endif

// zzindda
#if 0
extern char bPortProtect;
#endif

extern int SetPm2Sram(void);

extern char * get_blkdev_name(void);
extern int get_blkdev_idx(const char *name);
//extern char buildVers[];
extern uint8_t gDispType;
extern unsigned char first_boot_ver[];
extern const char second_boot_ver[];
// zzindda
#if 0
extern int getFullVerifyOTP(void);
#endif

void linux_param_set(char *kargs)
{
    char *user_args     = getenv ("bootargs");
    char *bootmode      = getenv("bootmode");
    char *init_file     = getenv("initfile");
    char *mac, *print, *ssc_enable;
    TOOL_OPTION1_T toolOpt1;
    int DebugStatus = DDI_NVM_GetDebugStatus();
    int swumode = get_swumode();
    int defaultenv_index=0;
    unsigned int soc_rev = MDrv_SYS_GetChipRev();
    DDI_NVM_GetToolOpt1(&toolOpt1);

#ifdef SIGN_USE_PARTIAL
    LDR_ENV_T* prLdrEnv = (LDR_ENV_T*)0xfb005000;
#endif

    if(!strcmp(bootmode, "user"))
    {
    #ifdef USING_SIMPLE_MMAP

        // for default cmdline
        sprintf(arg_next(kargs), "%s ", user_args);

        // for rootfs waiting in kernel
        sprintf(arg_next(kargs), "rootwait ");

        #if 0
            sprintf(arg_next(kargs), "console=ttyS0,115200 ");  // webos - kernel print
        #endif

        // print on,off
        print = getenv("print");
        if ((!strncmp(getenv("print"), "off", 3)) || (DebugStatus != DEBUG_LEVEL))
        {
            printf("SILENT MODE!\n");
            sprintf(arg_next(kargs),"%s ", "quiet loglevel=0");
        }

        // for ethaddr
        mac = getenv("ethaddr");
        sprintf(arg_next(kargs), "macadr=%s ", mac);

        #if 0
            sprintf(arg_next(kargs), "ethaddr=%s ",mac);    // webos
        #endif

        // for boot version
        //sprintf(arg_next(kargs), "bver=%s ", buildVers);  // netcast - boot version
        sprintf(arg_next(kargs), "sver=%s ", first_boot_ver);   // webos - 1st boot version
        sprintf(arg_next(kargs), "bver=%s ", second_boot_ver);   // webos - 2nd boot version

        // for snapshot boot

        #ifdef CONFIG_HIBERNATION
            if((check_snapshot_mode() == LGSNAP_MAKING_IMAGE))
            {
                sprintf(arg_next(kargs), "snapshot ");
            }
            sprintf(arg_next(kargs), "resume=/dev/%s%d ", get_blkdev_name(), get_blkdev_idx(SNAP_PART_NAME));
        #endif

        //for Spread Spectrum option
        ssc_enable = getenv("ssc_enable");
        sprintf(arg_next(kargs), "ssc_enable=%s ", ssc_enable);

        // for model option.
        sprintf(arg_next(kargs), "modelopt=%s ", strModelOpt);

        // for HW option.
        sprintf(arg_next(kargs), "hwopt=%s ", strHWOption); // webos - to parsing hw option in tvservice, boardinfo

        // for Tool option.
        #if 0
            sprintf(arg_next(kargs), "ToolOpt=%d:%d:%d:%d:%d:%d:%d ", gToolOpt[0], gToolOpt[1], gToolOpt[2], gToolOpt[3], gToolOpt[4], gToolOpt[5], gToolOpt[6]);   // webos
        #endif

        #if 0
            DDI_NVM_Read(MODEL_INFO_DB_BASE + (UINT32)&(gModelInfoDB.validMark) - (UINT32)&gModelInfoDB, \
                            sizeof(gModelInfoDB.validMark), (UINT8 *)&(gModelInfoDB.validMark));
            if(gModelInfoDB.validMark != 0xffffffff)
            {
                    DDI_NVM_Read(MODEL_INFO_DB_BASE + (UINT32)&(gModelInfoDB.aModelName) - (UINT32)&gModelInfoDB, \
                                    sizeof(gModelInfoDB.aModelName), (UINT8 *)&(gModelInfoDB.aModelName));

                    DDI_NVM_Read(MODEL_INFO_DB_BASE + (UINT32)&(gModelInfoDB.aSerialNum) - (UINT32)&gModelInfoDB,   \
                                    sizeof(gModelInfoDB.aSerialNum), (UINT8 *)&(gModelInfoDB.aSerialNum));

                    DDI_NVM_Read(MODEL_INFO_DB_BASE + (UINT32)&(gModelInfoDB.group_code) - (UINT32)&gModelInfoDB,   \
                                    sizeof(gModelInfoDB.group_code), (UINT8 *)&(gModelInfoDB.group_code));
            }

            sprintf(arg_next(kargs), "countryGrp=%d ", gModelInfoDB.group_code);
            sprintf(arg_next(kargs), "debugMode=%d ", DDI_NVM_GetDebugStatus());
            sprintf(arg_next(kargs), "modelName=%s ", gModelInfoDB.aModelName);
            sprintf(arg_next(kargs), "serialNum=%s ", gModelInfoDB.aSerialNum);
        #endif

        // for mstar
        sprintf(arg_next(kargs), "NR_BANK=0x0 ");

        #ifdef LG_CHG
            char linux_mem[40] = {0};
            sprintf(arg_next(kargs), "vmalloc=804M ");
            sprintf(linux_mem, "LX_MEM=0x%lx ",MMAPGer[LINUX_MEM].u32MemLength);
            sprintf(arg_next(kargs),linux_mem);

            sprintf(arg_next(kargs), "EMAC_MEM=0x%lx ",MMAPGer[EMAC_MEM].u32MemLength);
            sprintf(arg_next(kargs), "DRAM_LEN=0x%lx ",MiuInfo.u32MiuDramLen);
            memset(linux_mem,0,40);
            if((MMAPGer[LX_MEM1].u32MemType & 0x3) == 0x0)            //MIU0
                sprintf(linux_mem, "LX_MEM2=0x%lx,0x%lx ",MMAPGer[LX_MEM1].u32MemAddr+ 0x20000000,MMAPGer[LX_MEM1].u32MemLength);
            else if((MMAPGer[LX_MEM1].u32MemType & 0x3) == 0x1)       //MIU1
                sprintf(linux_mem, "LX_MEM2=0x%lx,0x%lx ",MMAPGer[LX_MEM1].u32MemAddr+ 0xA0000000,MMAPGer[LX_MEM1].u32MemLength);
            //sprintf(linux_mem, "LX_MEM2=0x%lx,0x%lx ",0xA0000000,0);
            sprintf(arg_next(kargs),linux_mem);

            memset(linux_mem,0,40);
            sprintf(linux_mem, "LX_MEM3=0x%lx,0x%lx ",0 + 0xE0000000,0);//LX_MEM2_LEN);
            sprintf(arg_next(kargs),linux_mem);

            sprintf(arg_next(kargs), "DRAM_SIZE1=0x%lx ",MiuInfo.u32MiuDramLen_0);
            sprintf(arg_next(kargs), "DRAM_SIZE2=0x%lx ",MiuInfo.u32MiuDramLen_1);
            sprintf(arg_next(kargs), "KERNEL_PROTECT ");
            //sprintf(arg_next(kargs), "BB_ADDR=0xFFBF000 ");
            //sprintf(arg_next(kargs), "PM51_ADDR=0x3FC00000 ");
            //sprintf(arg_next(kargs), "PM51_LEN=0x10000 ");
        #endif
        sprintf(arg_next(kargs), "sar_hw_opt=%s ", getenv ("sar_hw_opt"));
        sprintf(arg_next(kargs), "tcontype=%s ", getenv ("tcontype"));
	if (getenv ("enable_tcon_panel") != NULL)
	{
            sprintf(arg_next(kargs), "enable_tcon_panel=%s ", getenv ("enable_tcon_panel"));
	}
        // end
        sprintf(arg_next(kargs), "cmdEnd"); // End of Command line
    #else
        //default bootargs : default no args. So you can add.
        sprintf(arg_next(kargs), "%s", user_args);
    #endif
    }
    else // bootmode != user
    {
        #ifdef USING_SIMPLE_MMAP
        if(!strcmp(bootmode, "nfs"))
        #else
        if(!strcmp(bootmode, "webos"))
        #endif
        {
            char *dhcp      = getenv("dhcp");
            char *nfsroot   = getenv("nfsroot");
            char *nfsserver = getenv("nfsserver");

            sprintf(arg_next(kargs), "root=/dev/nfs rw nfsroot=%s:%s,%s ",
                            nfsserver,
                            (nfsroot)?nfsroot:"root",
                            NFS_PARAMETER);

            if(dhcp != NULL && !strcmp(dhcp, "on"))
            {
                sprintf(arg_next(kargs), "ip=dhcp ");
            }
            else
            {
                char *ipaddr    = getenv("ipaddr");
                char *serverip  = getenv("serverip");
                char *gatewayip = getenv("gatewayip");
                char *netmask   = getenv("netmask");

                sprintf(arg_next(kargs), "ip=%s:%s:%s:%s::eth0:off ", ipaddr, serverip, gatewayip, netmask);
            }
        }
        else // bootmode == auto
        {
            char root_dev[32] = {0, };
            struct partition_info *pi = NULL;

            if(swumode)
            {
                pi = get_used_partition("swue");
                sprintf(root_dev, "/dev/%s%d ",  get_blkdev_name(), get_blkdev_idx("swue"));
            }
            else
            {
                pi = get_used_partition("rootfs");
                sprintf(root_dev, "/dev/%s%d ",  get_blkdev_name(), get_blkdev_idx("rootfs"));
            }

            if (!pi) {
                printf("Invalid rootfs partition\n");
                return -1;
            }

            sprintf(arg_next(kargs), "root=%s ro %s rootfstype=%s ",
                            root_dev,
                            (init_file)? init_file:"",
                            "squashfs");
            // Internal Micom
            if(DDI_CMNIO_GetMicomType())
            {
                printf("Internal Micom Mode!\n");
                sprintf(arg_next(kargs),"%s ", "PM");
            }
        }

#ifdef CONFIG_MSTAR_CMA
        unsigned int counter=0;
        for(counter = 0 ; counter < ARRAY_SIZE(CMAInfo) ; counter++) {
            if(!CMAInfo[counter].u32CMA_HID || !CMAInfo[counter].u32MemLength)
                continue;
            sprintf(arg_next(kargs),
                "CMA%d=%s,miu=%d,hid=%d,sz=0x%lx,st=0x%lx ",
                counter,
                CMAInfo[counter].pCMA_name,
                CMAInfo[counter].u32MemType & 0x0001,
                CMAInfo[counter].u32CMA_HID,
                CMAInfo[counter].u32MemLength,
                CMAInfo[counter].u32MemAddr);
        }
#endif /* CONFIG_MSTAR_CMA */

        mac = getenv("ethaddr");
        sprintf(arg_next(kargs), "ethaddr=%s ",mac);

        sprintf(arg_next(kargs), "mmcoops=dump wdtlog=dump@1M ");

        // snapshot auto test
        if ((!strncmp(getenv("SST"), "on", 2)))
        {
            printf("snapshot auto test mode!\n");
            sprintf(arg_next(kargs),"%s ", "SST");
        }
        else    // disable print only snashot test disabled, to check debug msg in snapshot test mode.
        {
            print = getenv("print");
            if ((!strncmp(getenv("print"), "off", 3)) || (DebugStatus != DEBUG_LEVEL))
            {
                printf("SILENT MODE!\n");
                sprintf(arg_next(kargs),"%s ", "quiet loglevel=0");
            }
        }

        // for FHD module.
        if ((!strncmp(getenv("FHD"), "on", 2)))
        {
            printf("FHD MODE!\n");
            sprintf(arg_next(kargs),"%s ", "FHD");
        }

        if(swumode)
        {
            sprintf(arg_next(kargs), "swumode ");
        }

        sprintf(arg_next(kargs), "console=ttyS0,115200 "); // to print kernel log
        sprintf(arg_next(kargs), "rootwait ");
        //sprintf(arg_next(kargs), "bver=%s ", buildVers);
        char *ssc_enable = getenv("ssc_enable");
        sprintf(arg_next(kargs), "ssc_enable=%s ", ssc_enable);

        // for model option.
        sprintf(arg_next(kargs), "modelopt=%s ", strModelOpt);
        // for HW option.
        sprintf(arg_next(kargs), "hwopt=%s ", strHWOption); // to parsing hw option in tvservice, boardinfo
        // for Tool option.
        sprintf(arg_next(kargs), "ToolOpt=%u:%u:%u:%u:%u:%u:%u:%u:%u ", gToolOpt[0], gToolOpt[1], gToolOpt[2], gToolOpt[3], gToolOpt[4], gToolOpt[5], gToolOpt[6], gToolOpt[7], gToolOpt[8]);

        DDI_NVM_Read(MODEL_INFO_DB_BASE + (UINT32)&(gModelInfoDB.validMark) - (UINT32)&gModelInfoDB, \
                        sizeof(gModelInfoDB.validMark), (UINT8 *)&(gModelInfoDB.validMark));
        if(gModelInfoDB.validMark != 0xffffffff)
        {
                DDI_NVM_Read(MODEL_INFO_DB_BASE + (UINT32)&(gModelInfoDB.aModelName) - (UINT32)&gModelInfoDB, \
                                sizeof(gModelInfoDB.aModelName), (UINT8 *)&(gModelInfoDB.aModelName));

                DDI_NVM_Read(MODEL_INFO_DB_BASE + (UINT32)&(gModelInfoDB.aSerialNum) - (UINT32)&gModelInfoDB,   \
                                sizeof(gModelInfoDB.aSerialNum), (UINT8 *)&(gModelInfoDB.aSerialNum));

                DDI_NVM_Read(MODEL_INFO_DB_BASE + (UINT32)&(gModelInfoDB.group_code) - (UINT32)&gModelInfoDB,   \
                                sizeof(gModelInfoDB.group_code), (UINT8 *)&(gModelInfoDB.group_code));
        }

        sprintf(arg_next(kargs), "countryGrp=%d ", gModelInfoDB.group_code);
        sprintf(arg_next(kargs), "debugMode=%d ", DebugStatus);
        sprintf(arg_next(kargs), "modelName=%s ", gModelInfoDB.aModelName);
        sprintf(arg_next(kargs), "serialNum=%s ", gModelInfoDB.aSerialNum);
        sprintf(arg_next(kargs), "devtmpfs.mount=1 ");

        sprintf(arg_next(kargs), "chip=LM18AA0 ");

        if(get_emmc_klog() == 1)
            sprintf(arg_next(kargs), "emmc_log=1 ");

        // for mstar
        sprintf(arg_next(kargs), "NR_BANK=0x0 ");

#ifdef LG_CHG
        char linux_mem[40] = {0};
        sprintf(arg_next(kargs), "vmalloc=804M ");
        sprintf(linux_mem, "LX_MEM=0x%lx ",MMAPGer[LINUX_MEM].u32MemLength);
        sprintf(arg_next(kargs),linux_mem);

        sprintf(arg_next(kargs), "EMAC_MEM=0x%lx ",MMAPGer[EMAC_MEM].u32MemLength);
        sprintf(arg_next(kargs), "DRAM_LEN=0x%lx ",MiuInfo.u32MiuDramLen);
        memset(linux_mem,0,40);
        if((MMAPGer[LX_MEM1].u32MemType & 0x3) == 0x0)            //MIU0
            sprintf(linux_mem, "LX_MEM2=0x%lx,0x%lx ",MMAPGer[LX_MEM1].u32MemAddr+ 0x20000000,MMAPGer[LX_MEM1].u32MemLength);
        else if((MMAPGer[LX_MEM1].u32MemType & 0x3) == 0x1)       //MIU1
            sprintf(linux_mem, "LX_MEM2=0x%lx,0x%lx ",MMAPGer[LX_MEM1].u32MemAddr+ 0xA0000000,MMAPGer[LX_MEM1].u32MemLength);
        //sprintf(linux_mem, "LX_MEM2=0x%lx,0x%lx ",0xA0000000,0);
        sprintf(arg_next(kargs),linux_mem);

        memset(linux_mem,0,40);
        sprintf(linux_mem, "LX_MEM3=0x%lx,0x%lx ",0 + 0xE0000000,0);//LX_MEM2_LEN);
        sprintf(arg_next(kargs),linux_mem);

        sprintf(arg_next(kargs), "DRAM_SIZE1=0x%lx ",MiuInfo.u32MiuDramLen_0);
        sprintf(arg_next(kargs), "DRAM_SIZE2=0x%lx ",MiuInfo.u32MiuDramLen_1);
        sprintf(arg_next(kargs), "KERNEL_PROTECT ");
        //sprintf(arg_next(kargs), "BB_ADDR=0xFFBF000 ");
        //sprintf(arg_next(kargs), "PM51_ADDR=0x3FC00000 ");
        //sprintf(arg_next(kargs), "PM51_LEN=0x10000 ");
#endif

#ifdef CONFIG_HIBERNATION
        if((check_snapshot_mode() == LGSNAP_MAKING_IMAGE))
        {
            sprintf(arg_next(kargs), "snapshot ");
        }
        sprintf(arg_next(kargs), "resume=/dev/%s%d ", get_blkdev_name(), get_blkdev_idx(SNAP_PART_NAME));

        {
        //zzindda
        #ifdef USING_SIMPLE_MMAP

        #else
            unsigned char art = DDI_NVM_GetSnapShotART();
            if( art == 'A' )
                sprintf(arg_next(kargs), "art=auto art_period=20 ");
            else if( art == 'R' )
                sprintf(arg_next(kargs), "art=resume art_period=20 ");
            else if( art == 'M' )
                sprintf(arg_next(kargs), "art=making art_period=20 ");
        #endif
        }
#endif

#ifndef USING_SIMPLE_MMAP
	if(DDI_NVM_GetCRIUEnable())
		sprintf(arg_next(kargs), "criu_enabled ");
#endif

#if 0
        lock_time = getenv("lock_time");
        sprintf(arg_next(kargs), "lock_time=%s ", lock_time);

        bootmode = getenv("bootmode");
        sprintf(arg_next(kargs), "bootmode=%s ", bootmode);

        if (bootmode != NULL && strcmp(bootmode,"nfs") == 0)
            sprintf(arg_next(kargs),"init=/sbin/init ");
        // zzindda
        #if 0
        else
        {
            if(check_snapshot_mode() == LGSNAP_MAKING_IMAGE)
                sprintf(arg_next(kargs),"lgsnap ");
        }

        if(swumode) {
            sprintf(arg_next(kargs), "swumode ");
        }
        #endif

        /*
         * ADD "appxip=XXX,XXX fontxip=XXX,XXX"
         */
        if (appxip_len > 0) {
            appxip_addr = APPXIP_LOADADDR;
        } else {
            appxip_addr = 0;
        }

        if (fontxip_len > 0) {
            if (appxip_addr > 0)
                fontxip_addr = appxip_addr - fontxip_len;
            else
                fontxip_addr = APPXIP_LOADADDR;
        } else {
            fontxip_addr = 0;
        }

        xip_size_mb = (appxip_len ? (appxip_len >> 20) : 0) + (fontxip_len ? (fontxip_len >> 20) : 0);
        sprintf( arg_next(kargs), "appxip=0x%x,0x%x fontxip=0x%x,0x%x ", appxip_addr, appxip_len,  fontxip_addr, fontxip_len);

        lserverip   = getenv("lserverip");
        serverip    = getenv("serverip");
        ipaddr      = getenv("ipaddr");
        gatewayip   = getenv("gatewayip");
        netmask     = getenv("netmask");
        sprintf(arg_next(kargs), "ip=%s:%s:%s:%s:%s ", lserverip, serverip, ipaddr, gatewayip, netmask);

        mac         = getenv("ethaddr");
        sprintf(arg_next(kargs),"macadr=%s ", mac);

        print       = getenv("print");
        if ((!(strncmp(getenv("print"),"off",3))) || (DDI_NVM_GetDebugStatus() != DEBUG_LEVEL))
        {
            printf("SILENT MODE!\n");
            sprintf(arg_next(kargs),"%s ", "quiet loglevel=0"); // silent mode
        }

        sprintf(arg_next(kargs),"modelopt=%s ", strModelOpt);
#endif

        if (DebugStatus == RELEASE_LEVEL)
            sprintf(arg_next(kargs),"portProtection ");

#if 0
        malimem     = getenv("malimem");
        sprintf(arg_next(kargs), "malimem=%s ", malimem);

        umpmem      = getenv("umpmem");
        sprintf(arg_next(kargs), "umpmem=%s ", umpmem);
        lpj         = getenv("lpj");
        sprintf(arg_next(kargs), "lpj=%s ", lpj);
#else
        /* set lpj value directly */
        /* MTK5369 200 HZ case */
        //sprintf(arg_next(kargs), "lpj=4440064 ");
        /* MTK5398 case */
        //zzindda
        #if 0
        sprintf(arg_next(kargs), "lpj=4780032 ");
        #endif
        /* MTK5369 1000 HZ case */
        //sprintf(arg_next(kargs), "lpj=886784 ");
#endif

    // zzindda
#if 0

        /* set LCD PDP module */
        sprintf(arg_next(kargs), "%s ", (gDispType==2)?"pdpmodules":"lcdmodules");


#ifdef CC_MT53XX_SUPPORT_2G_DRAM
        printf("TOTAL_MEM_SIZE = 0x%x \n",TOTAL_MEM_SIZE);
        printf("kmemsize = 0x%x \n",kmemsize);
        printf("fbdfbsize = 0x%x \n",fbdfbsize);
        printf("ALIGN(TOTAL_MEM_SIZE - kmemsize - fbdfbsize, 0x800000)/0x100000 = %d \n",ALIGN(TOTAL_MEM_SIZE - kmemsize - fbdfbsize, 0x800000)/0x100000);
        printf("ALIGN(fbdfbsize, 0x800000)/0x100000 = %d \n",ALIGN(fbdfbsize, 0x800000)/0x100000);
        sprintf(arg_next(kargs), "vmalloc=%dMB ", ALIGN(TOTAL_MEM_SIZE - kmemsize - fbdfbsize, 0x800000)/0x100000/*FBM*/ + ALIGN(fbdfbsize, 0x800000)/0x100000/*FB+DFB*/ + 256/*0xf0000000--end*/ + 16/*DFB*/ + 40/*kernel usage*/ + 16/*alignment reserved*/);
#endif

#ifdef CC_SIGN_DIST
        sprintf(arg_next(kargs), "par_init ");
#endif

        //parse mtk chip revision
        soc_rev = (unsigned int)BSP_GetIc5369Version();
        if(soc_rev < IC_VER_5396_AC)
            sprintf(arg_next(kargs), "soc_rev=A0 ");
        else
            sprintf(arg_next(kargs), "soc_rev=B0 ");

        if (DDI_NVM_GetDebugStatus() == RELEASE_LEVEL)
            sprintf(arg_next(kargs),"panic=1 ");
        else
            sprintf(arg_next(kargs),"panic=0 ");

#ifdef SIGN_USE_PARTIAL
        if ( getFullVerifyOTP() )
        {
            sprintf(arg_next(kargs),"fullverify ");
        }
        else
        {
            //copy pub key to mem
            memcpy((void *)0x4000000+appxip_len-sizeof(prLdrEnv->au4CustKey) /*tail of appxip*/, (void *)prLdrEnv->au4CustKey, sizeof(prLdrEnv->au4CustKey));
        }
#endif

#endif
        //printf("emmc capacity:= 0x%01x%08x \n", U64_UPPER(emmc_info[0].capacity), U64_LOWER(emmc_info[0].capacity));
        struct mmc *mmc = find_mmc_device(0);
        if(mmc->has_init)
        {
            sprintf(arg_next(kargs), "emmc_size=0x%01lx%08lx ", U64_UPPER(mmc->capacity), U64_LOWER(mmc->capacity));
        }
        else printf("not finished eMMC init\n");

        do {
                if(default_env[defaultenv_index][0])
                    sprintf(arg_next(kargs), "%s ", default_env[defaultenv_index]);
                defaultenv_index++;

        } while(defaultenv_index < DEFAULTENV_MAX);

        if(MICOM_IsPowerOnly() || !DDI_NVM_GetInstopStatus())
            sprintf(arg_next(kargs), "factory ");

        if(MICOM_IsPowerOnly())
            sprintf(arg_next(kargs), "%s ", "pwrOnly");

        sprintf(arg_next(kargs), "sver=%s ", first_boot_ver);	// 1st Boot version
        sprintf(arg_next(kargs), "bver=%s ", second_boot_ver);	// 2nd Boot version

		if(toolOpt1.flags.eModelModuleType == 7)
        {
        	sprintf(arg_next(kargs), "innolux=%d ", 1);	// innolux panel
        }
		else
		{
			sprintf(arg_next(kargs), "innolux=%d ", 0);	// non-innolux panel
		}

#if defined(LG_CHG)
#ifdef CONFIG_SECURITY_BOOT
    if ( DDI_NVM_GetFullVerifyFlag() )
    {
        printf("verify_done = 0x%x \n",verify_done);
        sprintf(arg_next(kargs), "fullverify ");
        if ( verify_done == (VERIFY_APPS_DONE | VERIFY_TZFW_DONE) )
        {
            printf("although current fullverify was ON, all apps and kernel is verified in full, we set full verify flag off \n");
            DDI_NVM_SetFullVerifyFlag(0);
        }
    }
#else
    sprintf(arg_next(kargs), "fullverify ");
#endif
#endif
        sprintf(arg_next(kargs), "sar_hw_opt=%s ", getenv ("sar_hw_opt"));
        sprintf(arg_next(kargs), "tcontype=%s ", getenv ("tcontype"));
        if (getenv ("enable_tcon_panel") != NULL)
	{
            sprintf(arg_next(kargs), "enable_tcon_panel=%s ", getenv ("enable_tcon_panel"));
	}

        sprintf(arg_next(kargs), "cmdEnd"); // End of Command line
    }// bootmode == user


}


#endif

void arch_lmb_reserve(struct lmb *lmb)
{
    ulong sp;

    /*
     * Booting a (Linux) kernel image
     *
     * Allocate space for command line and board info - the
     * address should be as high as possible within the reach of
     * the kernel (see CONFIG_SYS_BOOTMAPSZ settings), but in unused
     * memory, which means far enough below the current stack
     * pointer.
     */
    sp = get_sp();
    debug("## Current stack ends at 0x%08lx ", sp);

    /* adjust sp by 1K to be safe */
    sp -= 1024;
    lmb_reserve(lmb, sp,
            gd->bd->bi_dram[0].start + gd->bd->bi_dram[0].size - sp);
}

#if 0
// MStar start
#define BOOTTIME_SBOOT_STR "BOOTTIME_SBOOT"
#define BOOTTIME_UBOOT_STR "BOOTTIME_UBOOT"
#include <stdlib.h>
#include <malloc.h>
#include "MsEnvironment.h"

unsigned long G_MS_BOOTTIME_SBOOT=1; // global variable for storing the boot time used in sboot (ms)
unsigned long G_MS_BOOTTIME_UBOOT=1; // global variable for storing the boot time used in sboot (ms)

static void _boottime_set_to_env(void)
{
    //extern unsigned long G_MS_BOOTTIME_SBOOT; // global variable for storing the boot time used in sboot (ms)
    //extern unsigned long G_MS_BOOTTIME_UBOOT; // global variable for storing the boot time used in sboot (ms)
    extern int snprintf(char *str, size_t size, const char *fmt, ...);
    const char *strBootArg = "bootargs";
    char *strEnv = NULL;
    char *strPrevEnv = NULL;
    const int u32EnvSize = 32;

    {
        unsigned int PiuTick = *(volatile unsigned int *)(0x1f006090);
        PiuTick |= (*(volatile unsigned int *)(0x1f006094)) << 16;
        unsigned int PiuTime = PiuTick / 12000;
        G_MS_BOOTTIME_UBOOT = (unsigned long)PiuTime;
    }

    strEnv = malloc(u32EnvSize);
    if(strEnv  != NULL)
    {
        strPrevEnv = getenv((char*)strBootArg);
        if(strPrevEnv != NULL)
        {
            memset(strEnv , 0, u32EnvSize);
            snprintf(strEnv , u32EnvSize-1, "%s=%lu", BOOTTIME_SBOOT_STR, G_MS_BOOTTIME_SBOOT);
            if(0 != set_bootargs_cfg((char*)strBootArg, strEnv, 1))
            {
                printf("%s: Error: set_bootargs_cfg failed at %d\n", __func__, __LINE__);
            }

            //NOTE: getenv again because if has been updated
            strPrevEnv = getenv((char*)strBootArg);
            if(strPrevEnv != NULL)
            {
                memset(strEnv , 0,u32EnvSize);
                snprintf(strEnv , u32EnvSize-1, "%s=%lu", BOOTTIME_UBOOT_STR, G_MS_BOOTTIME_UBOOT);
                if(0 != set_bootargs_cfg((char*)strBootArg, strEnv, 1))
                {
                    printf("%s: Error: set_bootargs_cfg failed at %d\n", __func__, __LINE__);
                }
            }
        }

        free(strEnv );
    }
}
// MStar end
#endif
static void announce_and_cleanup(void)
{
    char* s = NULL;
    char* t = NULL;

    /* checkpoint for autotest boottime, plz dont remove it */
    unsigned int PiuTick = *(volatile unsigned int *)(0x1f006090);
    PiuTick |= (*(volatile unsigned int *)(0x1f006094)) << 16;
    unsigned int PiuTime = PiuTick / 12000;
    printf("[AT][MB][start kr][%u]\n", PiuTime);

    printf("\nStarting kernel   ...\n\n");

    #ifdef STR_FOR_AGATE
        *(volatile unsigned int*)(0x1f206700) = 0xdfdf;
    #else
        extern void MsOS_DisableAllInterrupts(void);
        MsOS_DisableAllInterrupts();
    #endif

    s = getenv("sync_miuprot");
    t = getenv("KERNEL_PROTECT");

#if 0// fixe me, hank
    if(!(strcmp(s, "2")))
    {
        MsApi_MiuProtect();
    }
    else if ((!(strcmp(s, "1"))) || (t != NULL))
    {
        MsApi_kernelProtect();
        s = getenv("kernelProtectBist");
        if (s != NULL)
        {
            MsApi_kernelProtectBist();
        }
    }
#endif

#ifdef CONFIG_USB_DEVICE
    {
        extern void udc_disconnect(void);
        udc_disconnect();
    }
#endif
    cleanup_before_linux();
}

#if defined(CONFIG_ARMV7_NONSEC) || defined(CONFIG_ARMV7_VIRT)
static void do_nonsec_virt_switch(void)
{
#if defined(CONFIG_ARMV7_NONSEC) || defined(CONFIG_ARMV7_VIRT)
	if (armv7_switch_nonsec() == 0)
#ifdef CONFIG_ARMV7_VIRT
	if (armv7_switch_hyp() == 0)
		debug("entered HYP mode\n");
#else
	debug("entered non-secure state\n");
#endif
#endif
}
#endif

int do_bootm_linux(int flag, int argc, char *argv[], bootm_headers_t *images)
{
    bd_t    *bd = gd->bd;
    char    *s;
    char    str[64] = {0,};
    int machid = bd->bi_arch_number;
    void    (*kernel_entry)(int zero, int arch, uint params);

#ifdef CONFIG_CMDLINE_TAG
#ifdef CFG_LG_CHG
    char *kargs[CONFIG_SYS_CBSIZE] = {0,};
#else
    char *commandline = getenv ("bootargs");
#endif
#endif
    char *kernel_protect = getenv("KERNEL_PROTECT");

#ifdef CFG_LG_CHG
        //Set Kernel protect
        if(kernel_protect == NULL)
        {
            //setenv("KERNEL_PROTECT", " DRAM_SIZE1=0x20000000 DRAM_SIZE2=0x20000000");
            sprintf(str, " DRAM_SIZE1=0x%lx DRAM_SIZE2=0x%lx", MiuInfo.u32MiuDramLen_0, MiuInfo.u32MiuDramLen_1);
			//printf("u32MiuDramLen_0=%x,u32MiuDramLen_1=%x,u32MiuDramLen_2=%x\n",MiuInfo.u32MiuDramLen_0,MiuInfo.u32MiuDramLen_1,MiuInfo.u32MiuDramLen_2);
			setenv("KERNEL_PROTECT", str);
            saveenv();
            printf("ENABLE KERNEL_PROTECT \n\n");
        }

        linux_param_set(kargs);
        printf("cmdline = %s \n", kargs);
#endif
    {
        // cyli add to measure booting time
        unsigned int u32Timer_b, u32Timer_c;
        u32Timer_c = (*((volatile unsigned int *)0x1f006090)) | ((*((volatile unsigned int *)0x1f006094)) << 16);
        u32Timer_b = (*((volatile unsigned int *)0x1f2067B8)) | ((*((volatile unsigned int *)0x1f2067BC)) << 16);
        //printf("u32Timer_b = 0x%08x, u32Timer_c = 0x%08x\n", u32Timer_b, u32Timer_c);
        printf("\033[0;31m[time] sboot takes %d.%03dms, mboot takes %d.%03dms \033[0m\n", u32Timer_b/12000, (u32Timer_b/12)%1000,
                                     (u32Timer_c - u32Timer_b)/12000, ((u32Timer_c - u32Timer_b)/12)%1000);
    }

    if ((flag != 0) && (flag != BOOTM_STATE_OS_GO))
        return 1;

    s = getenv ("machid");
    if (s) {
        machid = simple_strtoul (s, NULL, 16);
        printf ("Using machid 0x%x from environment\n", machid);
    }

    show_boot_progress (15);

#ifdef CONFIG_OF_LIBFDT
    if (images->ft_len)
        return bootm_linux_fdt(machid, images);
#endif

    kernel_entry = (void (*)(int, int, uint))images->ep;

    debug ("## Transferring control to Linux (at address %08lx) ...\n",
           (ulong) kernel_entry);

//MStar start
    _boottime_set_to_env();
#ifdef CONFIG_CMDLINE_TAG
#ifdef CFG_LG_CHG
#else
    commandline = getenv ("bootargs"); // NOTE: should get agaig, becuase the env has been updated
#endif
#endif
//MStar end
#if defined (CONFIG_SETUP_MEMORY_TAGS) || \
    defined (CONFIG_CMDLINE_TAG) || \
    defined (CONFIG_INITRD_TAG) || \
    defined (CONFIG_SERIAL_TAG) || \
    defined (CONFIG_REVISION_TAG)
    setup_start_tag (bd);
#ifdef CONFIG_SERIAL_TAG
    setup_serial_tag (&params);
#endif
#ifdef CONFIG_REVISION_TAG
    setup_revision_tag (&params);
#endif
#ifdef CONFIG_SETUP_MEMORY_TAGS
    setup_memory_tags (bd);
#endif
#ifdef CONFIG_CMDLINE_TAG
#ifdef CFG_LG_CHG
    setup_commandline_tag (bd, kargs);
#else
    setup_commandline_tag (bd, commandline);
#endif

#ifdef CONFIG_CUSTOM_TAG
    setup_custom_tag(bd);
#endif

#endif
#ifdef CONFIG_INITRD_TAG
    if (images->rd_start && images->rd_end)
        setup_initrd_tag (bd, images->rd_start, images->rd_end);
#endif
    setup_end_tag(bd);
#endif

	display_boottime_log();
    announce_and_cleanup();

#if defined (CONFIG_TRUSTZONE_ENABLE)
        #include "MsBoot.h"
        do_bootNuttx();
#endif
#if defined (CONFIG_ARMv8_64BIT_KERNEL)
    //there is no atags pointer within 64 bit kernel, so we need to pass bootargs to kernel
    char* __cmdline = bd->bi_boot_params;
    //cmd_ptr is used to save pointer of command line
    unsigned long cmd_ptr = (unsigned long)bd->bi_boot_params;
    strncpy(__cmdline,commandline,strlen(commandline));
    printf("set 64 bit mode \n");

    extern ulong ramdisk_start,ramdisk_len;

    __asm__ __volatile__(
    "ldr  r3, [%0]  \n\t"
    "ldr  r4, [%1]  \n\t"
    "ldr  r2, [%2]  \n\t"
    :
    : "r"(&ramdisk_start),"r"(&ramdisk_len),"r"(&cmd_ptr)
    : "r3","r4","r2"
    );

    __asm__ __volatile__("smc #0": );
#else
#if defined(CONFIG_ARMV7_NONSEC) || defined(CONFIG_ARMV7_VIRT)
	do_nonsec_virt_switch();
#endif //CONFIG_ARMV7_NONSEC || CONFIG_ARMV7_VIRT

//#if defined(CONFIG_MULTICORES_PLATFORM)
#if defined(CONFIG_MSTAR_MUNICH) || defined(CONFIG_MSTAR_MUSTANG)
    __asm__ volatile(
                        /* ACTLR.SMP = 0 */
                        "mrc    p15, 0, r0, c1, c0, 1\n"
                        "bic    r0, r0, #(0x01 << 6)\n"
                        "mcr    p15, 0, r0, c1, c0, 1\n"
             );
#endif
//#endif

	kernel_entry(0, machid, bd->bi_boot_params);
	/* does not return */
#endif
    return 1;
}

#if defined(CONFIG_OF_LIBFDT)
static int fixup_memory_node(void *blob)
{
    bd_t    *bd = gd->bd;
    int bank;
    u64 start[CONFIG_NR_DRAM_BANKS];
    u64 size[CONFIG_NR_DRAM_BANKS];

    for (bank = 0; bank < CONFIG_NR_DRAM_BANKS; bank++) {
        start[bank] = bd->bi_dram[bank].start;
        size[bank] = bd->bi_dram[bank].size;
    }

    return fdt_fixup_memory_banks(blob, start, size, CONFIG_NR_DRAM_BANKS);
}

static int bootm_linux_fdt(int machid, bootm_headers_t *images)
{
    ulong rd_len;
    void (*kernel_entry)(int zero, int dt_machid, void *dtblob);
    ulong of_size = images->ft_len;
    char **of_flat_tree = &images->ft_addr;
    ulong *initrd_start = &images->initrd_start;
    ulong *initrd_end = &images->initrd_end;
    struct lmb *lmb = &images->lmb;
    int ret;

    kernel_entry = (void (*)(int, int, void *))images->ep;

    boot_fdt_add_mem_rsv_regions(lmb, *of_flat_tree);

    rd_len = images->rd_end - images->rd_start;
    ret = boot_ramdisk_high(lmb, images->rd_start, rd_len,
                initrd_start, initrd_end);
    if (ret)
        return ret;

    ret = boot_relocate_fdt(lmb, of_flat_tree, &of_size);
    if (ret)
        return ret;

    debug("## Transferring control to Linux (at address %08lx) ...\n",
           (ulong) kernel_entry);

    fdt_chosen(*of_flat_tree, 1);

    fixup_memory_node(*of_flat_tree);

    fdt_initrd(*of_flat_tree, *initrd_start, *initrd_end, 1);

    announce_and_cleanup();

    kernel_entry(0, machid, *of_flat_tree);
    /* does not return */

    return 1;
}
#endif

#if defined (CONFIG_SETUP_MEMORY_TAGS) || \
    defined (CONFIG_CMDLINE_TAG) || \
    defined (CONFIG_INITRD_TAG) || \
    defined (CONFIG_SERIAL_TAG) || \
    defined (CONFIG_REVISION_TAG)
static void setup_start_tag (bd_t *bd)
{
    params = (struct tag *) bd->bi_boot_params;

    params->hdr.tag = ATAG_CORE;
    params->hdr.size = tag_size (tag_core);

    params->u.core.flags = 0;
    params->u.core.pagesize = 0;
    params->u.core.rootdev = 0;

    params = tag_next (params);
}


#ifdef CONFIG_SETUP_MEMORY_TAGS
static void setup_memory_tags (bd_t *bd)
{
    int i;

    for (i = 0; i < CONFIG_NR_DRAM_BANKS; i++) {
        params->hdr.tag = ATAG_MEM;
        params->hdr.size = tag_size (tag_mem32);

        params->u.mem.start = bd->bi_dram[i].start;
        params->u.mem.size = bd->bi_dram[i].size;

        params = tag_next (params);
    }
}
#endif /* CONFIG_SETUP_MEMORY_TAGS */


static void setup_commandline_tag (bd_t *bd, char *commandline)
{
    char *p;

    if (!commandline)
        return;

    /* eat leading white space */
    for (p = commandline; *p == ' '; p++);

    /* skip non-existent command lines so the kernel will still
     * use its default command line.
     */
    if (*p == '\0')
        return;

    params->hdr.tag = ATAG_CMDLINE;
    params->hdr.size =
        (sizeof (struct tag_header) + strlen (p) + 1 + 4) >> 2;

    strcpy (params->u.cmdline.cmdline, p);

    params = tag_next (params);
}


#ifdef CONFIG_CUSTOM_TAG


static void setup_custom_tag(bd_t *bd) {

    params->hdr.tag = ATAG_CUSTOM;
    params->hdr.size = tag_size(tag_custom);

	//printf("miu_dram_len=%x,miu_dram_len0=%x,miu_dram_len1=%x,miu_dram_len2=%x,u32EnableMiu_1=%x,u32MiuInterval=%x,u32MmapCount=%x\n",MiuInfo.u32MiuDramLen,MiuInfo.u32MiuDramLen_0,MiuInfo.u32MiuDramLen_1,MiuInfo.u32MiuDramLen_2,MiuInfo.u32EnableMiu_1,MiuInfo.u32MiuInterval,MiuInfo.u32MmapCount);

    params->u.custom.miu_dram_len  = MiuInfo.u32MiuDramLen;
    params->u.custom.miu_dram_len0 = MiuInfo.u32MiuDramLen_0;
    params->u.custom.miu_dram_len1 = MiuInfo.u32MiuDramLen_1;
    params->u.custom.miu_dram_len2 = MiuInfo.u32MiuDramLen_2;
    params->u.custom.enable_miu = MiuInfo.u32EnableMiu_1;
    params->u.custom.miu_interval = MiuInfo.u32MiuInterval;
//    params->u.custom.miu2_interval = MIU_INTERVAL2;
    params->u.custom.mmap_count = MiuInfo.u32MmapCount;
    memset(params->u.custom.mmapger, 0, sizeof(MMAPGer));
    memcpy((void*)params->u.custom.mmapger, (void *)MMAPGer, sizeof(MMAPGer));

#if 0
    printf("setup_custom_tag tag_size=%d sizeof(tag_custom)=%d sizeof(MMAPGer)=%d\n", tag_size(tag_custom), sizeof(struct tag_custom), sizeof(MMAPGer));
    int i = 0;
    for(i = 0; i< MMAP_ID_MAX; i++)
   {
       printf("%s(),%d, type = %d, ,u32MemAvailableAddr = 0x%x,u32MemAddr = 0x%x,u32MemGapCheckAddr = 0x%x,u32MemLength = 0x%x,u32MemType = 0x%x \n",__FUNCTION__,__LINE__,i,MMAPGer[i].u32MemAvailableAddr,MMAPGer[i].u32MemAddr,MMAPGer[i].u32MemGapCheckAddr,MMAPGer[i].u32MemLength,MMAPGer[i].u32MemType);
   }
#endif
    params = tag_next (params);
}
#endif

#ifdef CONFIG_INITRD_TAG
static void setup_initrd_tag (bd_t *bd, ulong initrd_start, ulong initrd_end)
{
    /* an ATAG_INITRD node tells the kernel where the compressed
     * ramdisk can be found. ATAG_RDIMG is a better name, actually.
     */
    params->hdr.tag = ATAG_INITRD2;
    params->hdr.size = tag_size (tag_initrd);

    params->u.initrd.start = initrd_start;
    params->u.initrd.size = initrd_end - initrd_start;

    params = tag_next (params);
}
#endif /* CONFIG_INITRD_TAG */

#ifdef CONFIG_SERIAL_TAG
void setup_serial_tag (struct tag **tmp)
{
    struct tag *params = *tmp;
    struct tag_serialnr serialnr;
    void get_board_serial(struct tag_serialnr *serialnr);

    get_board_serial(&serialnr);
    params->hdr.tag = ATAG_SERIAL;
    params->hdr.size = tag_size (tag_serialnr);
    params->u.serialnr.low = serialnr.low;
    params->u.serialnr.high= serialnr.high;
    params = tag_next (params);
    *tmp = params;
}
#endif

#ifdef CONFIG_REVISION_TAG
void setup_revision_tag(struct tag **in_params)
{
    u32 rev = 0;
    u32 get_board_rev(void);

    rev = get_board_rev();
    params->hdr.tag = ATAG_REVISION;
    params->hdr.size = tag_size (tag_revision);
    params->u.revision.rev = rev;
    params = tag_next (params);
}
#endif  /* CONFIG_REVISION_TAG */

static void setup_end_tag (bd_t *bd)
{
    params->hdr.tag = ATAG_NONE;
    params->hdr.size = 0;
}
#endif /* CONFIG_SETUP_MEMORY_TAGS || CONFIG_CMDLINE_TAG || CONFIG_INITRD_TAG */

static ulong get_sp(void)
{
    ulong ret;

    asm("mov %0, sp" : "=r"(ret) : );
    return ret;
}

void display_boottime_log(void)
{
	char*	timelog = getenv("timelog");

	if(!strcmp("on", timelog))
	{
		enable_console();
	    {
	        // cyli add to measure booting time
	        unsigned int u32Timer_b, u32Timer_c;
	        u32Timer_c = (*((volatile unsigned int *)0x1f006090)) | ((*((volatile unsigned int *)0x1f006094)) << 16);
                u32Timer_b = (*((volatile unsigned int *)0x1f2067B8)) | ((*((volatile unsigned int *)0x1f2067BC)) << 16);
                //printf("u32Timer_b = 0x%08x, u32Timer_c = 0x%08x\n", u32Timer_b, u32Timer_c);
	        printf("\033[0;31m[time] sboot takes %d.%03dms, mboot takes %d.%03dms \033[0m\n", u32Timer_b/12000, (u32Timer_b/12)%1000,
	                                     (u32Timer_c - u32Timer_b)/12000, ((u32Timer_c - u32Timer_b)/12)%1000);
	    }
		disable_console();
	}
}

