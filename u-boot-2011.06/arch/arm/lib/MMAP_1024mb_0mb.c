//<MStar Software>
//******************************************************************************
// MStar Software
// Copyright (c) 2010 - 2012 MStar Semiconductor, Inc. All rights reserved.
// All software, firmware and related documentation herein ("MStar Software") are
// intellectual property of MStar Semiconductor, Inc. ("MStar") and protected by
// law, including, but not limited to, copyright law and international treaties.
// Any use, modification, reproduction, retransmission, or republication of all
// or part of MStar Software is expressly prohibited, unless prior written
// permission has been granted by MStar.
//
// By accessing, browsing and/or using MStar Software, you acknowledge that you
// have read, understood, and agree, to be bound by below terms ("Terms") and to
// comply with all applicable laws and regulations:
//
// 1. MStar shall retain any and all right, ownership and interest to MStar
//    Software and any modification/derivatives thereof.
//    No right, ownership, or interest to MStar Software and any
//    modification/derivatives thereof is transferred to you under Terms.
//
// 2. You understand that MStar Software might include, incorporate or be
//    supplied together with third party`s software and the use of MStar
//    Software may require additional licenses from third parties.
//    Therefore, you hereby agree it is your sole responsibility to separately
//    obtain any and all third party right and license necessary for your use of
//    such third party`s software.
//
// 3. MStar Software and any modification/derivatives thereof shall be deemed as
//    MStar`s confidential information and you agree to keep MStar`s
//    confidential information in strictest confidence and not disclose to any
//    third party.
//
// 4. MStar Software is provided on an "AS IS" basis without warranties of any
//    kind. Any warranties are hereby expressly disclaimed by MStar, including
//    without limitation, any warranties of merchantability, non-infringement of
//    intellectual property rights, fitness for a particular purpose, error free
//    and in conformity with any international standard.  You agree to waive any
//    claim against MStar for any loss, damage, cost or expense that you may
//    incur related to your use of MStar Software.
//    In no event shall MStar be liable for any direct, indirect, incidental or
//    consequential damages, including without limitation, lost of profit or
//    revenues, lost or damage of data, and unauthorized system use.
//    You agree that this Section 4 shall still apply without being affected
//    even if MStar Software has been modified by MStar in accordance with your
//    request or instruction for your use, except otherwise agreed by both
//    parties in writing.
//
// 5. If requested, MStar may from time to time provide technical supports or
//    services in relation with MStar Software to you for your use of
//    MStar Software in conjunction with your or your customer`s product
//    ("Services").
//    You understand and agree that, except otherwise agreed by both parties in
//    writing, Services are provided on an "AS IS" basis and the warranty
//    disclaimer set forth in Section 4 above shall apply.
//
// 6. Nothing contained herein shall be construed as by implication, estoppels
//    or otherwise:
//    (a) conferring any license or right to use MStar name, trademark, service
//        mark, symbol or any other identification;
//    (b) obligating MStar or any of its affiliates to furnish any person,
//        including without limitation, you and your customers, any assistance
//        of any kind whatsoever, or any information; or
//    (c) conferring any license or right under any intellectual property right.
//
// 7. These terms shall be governed by and construed in accordance with the laws
//    of Taiwan, R.O.C., excluding its conflict of law rules.
//    Any and all dispute arising out hereof or related hereto shall be finally
//    settled by arbitration referred to the Chinese Arbitration Association,
//    Taipei in accordance with the ROC Arbitration Law and the Arbitration
//    Rules of the Association by three (3) arbitrators appointed in accordance
//    with the said Rules.
//    The place of arbitration shall be in Taipei, Taiwan and the language shall
//    be English.
//    The arbitration award shall be final and binding to both parties.
//
//******************************************************************************
//<MStar Software>

#ifdef CONFIG_MSTAR_CMA
#include <mmap_hwopt_1024_0_cma.h>
#else
#include "../include/asm/mmap_hwopt_1024_0.h"
#endif
#include "../include/asm/mmapger.h"

MiuInfo_t __miu_1024mb_0mb =
{
    ENABLE_MIU_1,
	ENABLE_MIU_2,
	MIU_DRAM_LEN,
	MIU_DRAM_LEN0,
	MIU_DRAM_LEN1,
	MIU_DRAM_LEN2,
	MIU_INTERVAL,
	CPU_ALIGN,
	MIU0_END_ADR,
	0,   //MIU1_END_ADR,
    MMAP_COUNT
};

MMapInfo_t __mmap_1024mb_0mb[MMAP_ID_MAX] =
{
     { DUMMY_MEM_AVAILABLE,
        DUMMY_MEM_ADR,
        DUMMY_MEM_GAP_CHK,
        DUMMY_MEM_LEN,
        DUMMY_MEM_MEMORY_TYPE },

    { LINUX_MEM_AVAILABLE,
        LINUX_MEM_ADR,
        LINUX_MEM_GAP_CHK,
        LINUX_MEM_LEN,
        LINUX_MEM_MEMORY_TYPE },

    { EMAC_MEM_AVAILABLE,
        EMAC_MEM_ADR,
        EMAC_MEM_GAP_CHK,
        EMAC_MEM_LEN,
        EMAC_MEM_MEMORY_TYPE },

    { XIP_MEM_AVAILABLE,
        XIP_MEM_ADR,
        XIP_MEM_GAP_CHK,
        XIP_MEM_LEN,
        XIP_MEM_MEMORY_TYPE },

    { NFDRM_NUTTX_MEM_AVAILABLE,
        NFDRM_NUTTX_MEM_ADR,
        NFDRM_NUTTX_MEM_GAP_CHK,
        NFDRM_NUTTX_MEM_LEN,
        NFDRM_NUTTX_MEM_MEMORY_TYPE },

    { NFDRM_HW_AES_BUF_AVAILABLE,
        NFDRM_HW_AES_BUF_ADR,
        NFDRM_HW_AES_BUF_GAP_CHK,
        NFDRM_HW_AES_BUF_LEN,
        NFDRM_HW_AES_BUF_MEMORY_TYPE },

    { HW_SECURE_BUFFER_AVAILABLE,
        HW_SECURE_BUFFER_ADR,
        HW_SECURE_BUFFER_GAP_CHK,
        HW_SECURE_BUFFER_LEN,
        HW_SECURE_BUFFER_MEMORY_TYPE },

    { SECURE_SHM_AVAILABLE,
        SECURE_SHM_ADR,
        SECURE_SHM_GAP_CHK,
        SECURE_SHM_LEN,
        SECURE_SHM_MEMORY_TYPE },

    { SECURE_UPDATE_AREA_AVAILABLE,
        SECURE_UPDATE_AREA_ADR,
        SECURE_UPDATE_AREA_GAP_CHK,
        SECURE_UPDATE_AREA_LEN,
        SECURE_UPDATE_AREA_MEMORY_TYPE },

    { SECURE_TSP_AVAILABLE,
        SECURE_TSP_ADR,
        SECURE_TSP_GAP_CHK,
        SECURE_TSP_LEN,
        SECURE_TSP_MEMORY_TYPE },

    { BIN_MEM_AVAILABLE,
        BIN_MEM_ADR,
        BIN_MEM_GAP_CHK,
        BIN_MEM_LEN,
        BIN_MEM_MEMORY_TYPE },

    { MAD_BASE_BUFFER_AVAILABLE,
        MAD_BASE_BUFFER_ADR,
        MAD_BASE_BUFFER_GAP_CHK,
        MAD_BASE_BUFFER_LEN,
        MAD_BASE_BUFFER_MEMORY_TYPE },

    { VD_3DCOMB_AVAILABLE,
        VD_3DCOMB_ADR,
        VD_3DCOMB_GAP_CHK,
        VD_3DCOMB_LEN,
        VD_3DCOMB_MEMORY_TYPE },

    { G3D_TEXTURE0_BUF_AVAILABLE,
        G3D_TEXTURE0_BUF_ADR,
        G3D_TEXTURE0_BUF_GAP_CHK,
        G3D_TEXTURE0_BUF_LEN,
        G3D_TEXTURE0_BUF_MEMORY_TYPE },

    { LINUX_LOW_MEM_RESERVE_AVAILABLE,
        LINUX_LOW_MEM_RESERVE_ADR,
        LINUX_LOW_MEM_RESERVE_GAP_CHK,
        LINUX_LOW_MEM_RESERVE_LEN,
        LINUX_LOW_MEM_RESERVE_MEMORY_TYPE },

    { VE_FRAMEBUFFER_AVAILABLE,
        VE_FRAMEBUFFER_ADR,
        VE_FRAMEBUFFER_GAP_CHK,
        VE_FRAMEBUFFER_LEN,
        VE_FRAMEBUFFER_MEMORY_TYPE },

    { TTX_BUF_AVAILABLE,
        TTX_BUF_ADR,
        TTX_BUF_GAP_CHK,
        TTX_BUF_LEN,
        TTX_BUF_MEMORY_TYPE },

    { PVR_DOWNLOAD_BUFFER_AVAILABLE,
        PVR_DOWNLOAD_BUFFER_ADR,
        PVR_DOWNLOAD_BUFFER_GAP_CHK,
        PVR_DOWNLOAD_BUFFER_LEN,
        PVR_DOWNLOAD_BUFFER_MEMORY_TYPE },

    { PVR_UPLOAD_BUFFER_AVAILABLE,
        PVR_UPLOAD_BUFFER_ADR,
        PVR_UPLOAD_BUFFER_GAP_CHK,
        PVR_UPLOAD_BUFFER_LEN,
        PVR_UPLOAD_BUFFER_MEMORY_TYPE },

    { TSP_BUF_AVAILABLE,
        TSP_BUF_ADR,
        TSP_BUF_GAP_CHK,
        TSP_BUF_LEN,
        TSP_BUF_MEMORY_TYPE },

    { VQ_BUF_AVAILABLE,
        VQ_BUF_ADR,
        VQ_BUF_GAP_CHK,
        VQ_BUF_LEN,
        VQ_BUF_MEMORY_TYPE },

    { MAILBOX_AVAILABLE,
        MAILBOX_ADR,
        MAILBOX_GAP_CHK,
        MAILBOX_LEN,
        MAILBOX_MEMORY_TYPE },

    { DIP_AVAILABLE,
        DIP_ADR,
        DIP_GAP_CHK,
        DIP_LEN,
        DIP_MEMORY_TYPE },

    { MPOOL_AVAILABLE,
        MPOOL_ADR,
        MPOOL_GAP_CHK,
        MPOOL_LEN,
        MPOOL_MEMORY_TYPE },

    { PNG_OUTPUT_BUF_AVAILABLE,
        PNG_OUTPUT_BUF_ADR,
        PNG_OUTPUT_BUF_GAP_CHK,
        PNG_OUTPUT_BUF_LEN,
        PNG_OUTPUT_BUF_MEMORY_TYPE },

    { PNG_INPUT_BUF_AVAILABLE,
        PNG_INPUT_BUF_ADR,
        PNG_INPUT_BUF_GAP_CHK,
        PNG_INPUT_BUF_LEN,
        PNG_INPUT_BUF_MEMORY_TYPE },

    { VDEC_GN_IAP_AVAILABLE,
        VDEC_GN_IAP_ADR,
        VDEC_GN_IAP_GAP_CHK,
        VDEC_GN_IAP_LEN,
        VDEC_GN_IAP_MEMORY_TYPE },

    { GOP_FRAMEBUFFER_AVAILABLE,
        GOP_FRAMEBUFFER_ADR,
        GOP_FRAMEBUFFER_GAP_CHK,
        GOP_FRAMEBUFFER_LEN,
        GOP_FRAMEBUFFER_MEMORY_TYPE },

    { GOP_REGDMA_AVAILABLE,
        GOP_REGDMA_ADR,
        GOP_REGDMA_GAP_CHK,
        GOP_REGDMA_LEN,
        GOP_REGDMA_MEMORY_TYPE },

    { GFX_VQ_BUFFER_AVAILABLE,
        GFX_VQ_BUFFER_ADR,
        GFX_VQ_BUFFER_GAP_CHK,
        GFX_VQ_BUFFER_LEN,
        GFX_VQ_BUFFER_MEMORY_TYPE },

    { VBI_1_BUFFER_AVAILABLE,
        VBI_1_BUFFER_ADR,
        VBI_1_BUFFER_GAP_CHK,
        VBI_1_BUFFER_LEN,
        VBI_1_BUFFER_MEMORY_TYPE },

    { VBI_2_BUFFER_AVAILABLE,
        VBI_2_BUFFER_ADR,
        VBI_2_BUFFER_GAP_CHK,
        VBI_2_BUFFER_LEN,
        VBI_2_BUFFER_MEMORY_TYPE },

    { LD_BUFFER_AVAILABLE,
        LD_BUFFER_ADR,
        LD_BUFFER_GAP_CHK,
        LD_BUFFER_LEN,
        LD_BUFFER_MEMORY_TYPE },

    { PIU_BUF_AVAILABLE,
        PIU_BUF_ADR,
        PIU_BUF_GAP_CHK,
        PIU_BUF_LEN,
        PIU_BUF_MEMORY_TYPE },

    { DUMMY_AVAILABLE,
        DUMMY_ADR,
        DUMMY_GAP_CHK,
        DUMMY_LEN,
        DUMMY_MEMORY_TYPE },

    { DUAL_STREAM_DUMY_AVAILABLE,
        DUAL_STREAM_DUMY_ADR,
        DUAL_STREAM_DUMY_GAP_CHK,
        DUAL_STREAM_DUMY_LEN,
        DUAL_STREAM_DUMY_MEMORY_TYPE },

    { VDEC_CPU_AVAILABLE,
        VDEC_CPU_ADR,
        VDEC_CPU_GAP_CHK,
        VDEC_CPU_LEN,
        VDEC_CPU_MEMORY_TYPE },

    { SVD_CPU_AVAILABLE,
        SVD_CPU_ADR,
        SVD_CPU_GAP_CHK,
        SVD_CPU_LEN,
        SVD_CPU_MEMORY_TYPE },

    { VDEC_BITSTREAM_AVAILABLE,
        VDEC_BITSTREAM_ADR,
        VDEC_BITSTREAM_GAP_CHK,
        VDEC_BITSTREAM_LEN,
        VDEC_BITSTREAM_MEMORY_TYPE },

    { VDEC_SUB_BITSTREAM_AVAILABLE,
        VDEC_SUB_BITSTREAM_ADR,
        VDEC_SUB_BITSTREAM_GAP_CHK,
        VDEC_SUB_BITSTREAM_LEN,
        VDEC_SUB_BITSTREAM_MEMORY_TYPE },

    { VDEC_N1_BITSTREAM_AVAILABLE,
        VDEC_N1_BITSTREAM_ADR,
        VDEC_N1_BITSTREAM_GAP_CHK,
        VDEC_N1_BITSTREAM_LEN,
        VDEC_N1_BITSTREAM_MEMORY_TYPE },

    { VDEC_N2_BITSTREAM_AVAILABLE,
        VDEC_N2_BITSTREAM_ADR,
        VDEC_N2_BITSTREAM_GAP_CHK,
        VDEC_N2_BITSTREAM_LEN,
        VDEC_N2_BITSTREAM_MEMORY_TYPE },

    { VDEC_FRAMEBUFFER_AVAILABLE,
        VDEC_FRAMEBUFFER_ADR,
        VDEC_FRAMEBUFFER_GAP_CHK,
        VDEC_FRAMEBUFFER_LEN,
        VDEC_FRAMEBUFFER_MEMORY_TYPE },

    { VDEC_SUB_FRAMEBUFFER_AVAILABLE,
        VDEC_SUB_FRAMEBUFFER_ADR,
        VDEC_SUB_FRAMEBUFFER_GAP_CHK,
        VDEC_SUB_FRAMEBUFFER_LEN,
        VDEC_SUB_FRAMEBUFFER_MEMORY_TYPE },

    { NFDRM_VDEC_SHARE_MEM_AVAILABLE,
        NFDRM_VDEC_SHARE_MEM_ADR,
        NFDRM_VDEC_SHARE_MEM_GAP_CHK,
        NFDRM_VDEC_SHARE_MEM_LEN,
        NFDRM_VDEC_SHARE_MEM_MEMORY_TYPE },

    { JPD_THUMBNAIL_AVAILABLE,
        JPD_THUMBNAIL_ADR,
        JPD_THUMBNAIL_GAP_CHK,
        JPD_THUMBNAIL_LEN,
        JPD_THUMBNAIL_MEMORY_TYPE },

    { POSD0_AVAILABLE,
        POSD0_ADR,
        POSD0_GAP_CHK,
        POSD0_LEN,
        POSD0_MEMORY_TYPE },

    { POSD1_AVAILABLE,
        POSD1_ADR,
        POSD1_GAP_CHK,
        POSD1_LEN,
        POSD1_MEMORY_TYPE },

    { AUDIO_CLIP_AVAILABLE,
        AUDIO_CLIP_ADR,
        AUDIO_CLIP_GAP_CHK,
        AUDIO_CLIP_LEN,
        AUDIO_CLIP_MEMORY_TYPE },

    { PVR_THUMBNAIL_DECODE_BUFFER_AVAILABLE,
        PVR_THUMBNAIL_DECODE_BUFFER_ADR,
        PVR_THUMBNAIL_DECODE_BUFFER_GAP_CHK,
        PVR_THUMBNAIL_DECODE_BUFFER_LEN,
        PVR_THUMBNAIL_DECODE_BUFFER_MEMORY_TYPE },

    { LD_BUF_AVAILABLE,
        LD_BUF_ADR,
        LD_BUF_GAP_CHK,
        LD_BUF_LEN,
        LD_BUF_MEMORY_TYPE },

    { BB_AVAILABLE,
        BB_ADR,
        BB_GAP_CHK,
        BB_LEN,
        BB_MEMORY_TYPE },

    { DEBUG_AVAILABLE,
        DEBUG_ADR,
        DEBUG_GAP_CHK,
        DEBUG_LEN,
        DEBUG_MEMORY_TYPE },

    { BT_POOL_AVAILABLE,
        BT_POOL_ADR,
        BT_POOL_GAP_CHK,
        BT_POOL_LEN,
        BT_POOL_MEMORY_TYPE },

    { APVR_BUF_AVAILABLE,
        APVR_BUF_ADR,
        APVR_BUF_GAP_CHK,
        APVR_BUF_LEN,
        APVR_BUF_MEMORY_TYPE },

    { VIDEO_CONF_AVAILABLE,
        VIDEO_CONF_ADR,
        VIDEO_CONF_GAP_CHK,
        VIDEO_CONF_LEN,
        VIDEO_CONF_MEMORY_TYPE },

    { G3D_CMD_Q_BUF_AVAILABLE,
        G3D_CMD_Q_BUF_ADR,
        G3D_CMD_Q_BUF_GAP_CHK,
        G3D_CMD_Q_BUF_LEN,
        G3D_CMD_Q_BUF_MEMORY_TYPE },

    { G3D_VERTEX_BUF_AVAILABLE,
        G3D_VERTEX_BUF_ADR,
        G3D_VERTEX_BUF_GAP_CHK,
        G3D_VERTEX_BUF_LEN,
        G3D_VERTEX_BUF_MEMORY_TYPE },

    { G3D_COLOR_BUF_AVAILABLE,
        G3D_COLOR_BUF_ADR,
        G3D_COLOR_BUF_GAP_CHK,
        G3D_COLOR_BUF_LEN,
        G3D_COLOR_BUF_MEMORY_TYPE },

    { G3D_TEXTURE1_BUF_AVAILABLE,
        G3D_TEXTURE1_BUF_ADR,
        G3D_TEXTURE1_BUF_GAP_CHK,
        G3D_TEXTURE1_BUF_LEN,
        G3D_TEXTURE1_BUF_MEMORY_TYPE },

    { GPD_ES_BUF_AVAILABLE,
        GPD_ES_BUF_ADR,
        GPD_ES_BUF_GAP_CHK,
        GPD_ES_BUF_LEN,
        GPD_ES_BUF_MEMORY_TYPE },

    { GPD_OUTPUT_BUF_AVAILABLE,
        GPD_OUTPUT_BUF_ADR,
        GPD_OUTPUT_BUF_GAP_CHK,
        GPD_OUTPUT_BUF_LEN,
        GPD_OUTPUT_BUF_MEMORY_TYPE },

    { ISDBT_TDI_AVAILABLE,
        ISDBT_TDI_ADR,
        ISDBT_TDI_GAP_CHK,
        ISDBT_TDI_LEN,
        ISDBT_TDI_MEMORY_TYPE },

    { VE_DIPW_BUF_AVAILABLE,
        VE_DIPW_BUF_ADR,
        VE_DIPW_BUF_GAP_CHK,
        VE_DIPW_BUF_LEN,
        VE_DIPW_BUF_MEMORY_TYPE },

    { MFE_BUF_AVAILABLE,
        MFE_BUF_ADR,
        MFE_BUF_GAP_CHK,
        MFE_BUF_LEN,
        MFE_BUF_MEMORY_TYPE },

    { MFE_OUT_BUF_AVAILABLE,
        MFE_OUT_BUF_ADR,
        MFE_OUT_BUF_GAP_CHK,
        MFE_OUT_BUF_LEN,
        MFE_OUT_BUF_MEMORY_TYPE },

    { VIDEO_IN_CAPTURE_TMP_AVAILABLE,
        VIDEO_IN_CAPTURE_TMP_ADR,
        VIDEO_IN_CAPTURE_TMP_GAP_CHK,
        VIDEO_IN_CAPTURE_TMP_LEN,
        VIDEO_IN_CAPTURE_TMP_MEMORY_TYPE },

    { LX_MEM1_AVAILABLE,
        LX_MEM1_ADR,
        LX_MEM1_GAP_CHK,
        LX_MEM1_LEN,
        LX_MEM1_MEMORY_TYPE },

{
        DTMB_TDI_AVAILABLE,
        DTMB_TDI_ADR,
        DTMB_TDI_GAP_CHK,
        DTMB_TDI_LEN,
        DTMB_TDI_MEMORY_TYPE},

    { SCALER_DNR_BUF_AVAILABLE,
        SCALER_DNR_BUF_ADR,
        SCALER_DNR_BUF_GAP_CHK,
        SCALER_DNR_BUF_LEN,
        SCALER_DNR_BUF_MEMORY_TYPE },

    { SCALER_DNR_W_BARRIER_AVAILABLE,
        SCALER_DNR_W_BARRIER_ADR,
        SCALER_DNR_W_BARRIER_GAP_CHK,
        SCALER_DNR_W_BARRIER_LEN,
        SCALER_DNR_W_BARRIER_MEMORY_TYPE },

    { SCALER_PIP_BUF_AVAILABLE,
        SCALER_PIP_BUF_ADR,
        SCALER_PIP_BUF_GAP_CHK,
        SCALER_PIP_BUF_LEN,
        SCALER_PIP_BUF_MEMORY_TYPE },

    { SCALER_MAIN_FRCM_BUFFER_AVAILABLE,
        SCALER_MAIN_FRCM_BUFFER_ADR,
        SCALER_MAIN_FRCM_BUFFER_GAP_CHK,
        SCALER_MAIN_FRCM_BUFFER_LEN,
        SCALER_MAIN_FRCM_BUFFER_MEMORY_TYPE },

    { SC_SUB_FRCM_AVAILABLE,
        SC_SUB_FRCM_ADR,
        SC_SUB_FRCM_GAP_CHK,
        SC_SUB_FRCM_LEN,
        SC_SUB_FRCM_MEMORY_TYPE },

    { SCALER_MCDI_ME1_BUFFER_AVAILABLE,
        SCALER_MCDI_ME1_BUFFER_ADR,
        SCALER_MCDI_ME1_BUFFER_GAP_CHK,
        SCALER_MCDI_ME1_BUFFER_LEN,
        SCALER_MCDI_ME1_BUFFER_MEMORY_TYPE },

    { SCALER_MCDI_ME2_BUFFER_AVAILABLE,
        SCALER_MCDI_ME2_BUFFER_ADR,
        SCALER_MCDI_ME2_BUFFER_GAP_CHK,
        SCALER_MCDI_ME2_BUFFER_LEN,
        SCALER_MCDI_ME2_BUFFER_MEMORY_TYPE },

    { SC_OD_AVAILABLE,
        SC_OD_ADR,
        SC_OD_GAP_CHK,
        SC_OD_LEN,
        SC_OD_MEMORY_TYPE },

    { SC_BUF_AVAILABLE,
        SC_BUF_ADR,
        SC_BUF_GAP_CHK,
        SC_BUF_LEN,
        SC_BUF_MEMORY_TYPE },

    { SCALER_MLOAD_BUFFER_AVAILABLE,
        SCALER_MLOAD_BUFFER_ADR,
        SCALER_MLOAD_BUFFER_GAP_CHK,
        SCALER_MLOAD_BUFFER_LEN,
        SCALER_MLOAD_BUFFER_MEMORY_TYPE },

    { SCALER_DYNAMIC_XC_BUFFER_AVAILABLE,
        SCALER_DYNAMIC_XC_BUFFER_ADR,
        SCALER_DYNAMIC_XC_BUFFER_GAP_CHK,
        SCALER_DYNAMIC_XC_BUFFER_LEN,
        SCALER_DYNAMIC_XC_BUFFER_MEMORY_TYPE },

    { TWOTO3D_DEPTH_DETECT_BUF_AVAILABLE,
        TWOTO3D_DEPTH_DETECT_BUF_ADR,
        TWOTO3D_DEPTH_DETECT_BUF_GAP_CHK,
        TWOTO3D_DEPTH_DETECT_BUF_LEN,
        TWOTO3D_DEPTH_DETECT_BUF_MEMORY_TYPE },

    { TWOTO3D_DEPTH_RENDER_BUF_AVAILABLE,
        TWOTO3D_DEPTH_RENDER_BUF_ADR,
        TWOTO3D_DEPTH_RENDER_BUF_GAP_CHK,
        TWOTO3D_DEPTH_RENDER_BUF_LEN,
        TWOTO3D_DEPTH_RENDER_BUF_MEMORY_TYPE },

    {
        T3D_DMA_CLIENT9_BUF_AVAILABLE,
        T3D_DMA_CLIENT9_BUF_ADR,
        T3D_DMA_CLIENT9_BUF_GAP_CHK,
        T3D_DMA_CLIENT9_BUF_LEN,
        T3D_DMA_CLIENT9_BUF_MEMORY_TYPE },

    {
        SC2_MAIN_FB_AVAILABLE,
        SC2_MAIN_FB_ADR,
        SC2_MAIN_FB_GAP_CHK,
        SC2_MAIN_FB_LEN,
        SC2_MAIN_FB_MEMORY_TYPE},

    {
        VDEC_DUMMY_AVAILABLE,
        VDEC_DUMMY_ADR,
        VDEC_DUMMY_GAP_CHK,
        VDEC_DUMMY_LEN,
        VDEC_DUMMY_MEMORY_TYPE},

    {
        JPD_FRAMEBUFFER_AVAILABLE,
        JPD_FRAMEBUFFER_ADR,
        JPD_FRAMEBUFFER_GAP_CHK,
        JPD_FRAMEBUFFER_LEN,
        JPD_FRAMEBUFFER_MEMORY_TYPE},

    {
        VDEC_SW_DETILE_BUF_AVAILABLE,
        VDEC_SW_DETILE_BUF_ADR,
        VDEC_SW_DETILE_BUF_GAP_CHK,
        VDEC_SW_DETILE_BUF_LEN,
        VDEC_SW_DETILE_BUF_MEMORY_TYPE},
     {
        JPD_OUTPUT_BUF_AVAILABLE,
        JPD_OUTPUT_BUF_ADR,
        JPD_OUTPUT_BUF_GAP_CHK,
        JPD_OUTPUT_BUF_LEN,
        JPD_OUTPUT_BUF_MEMORY_TYPE},

	 {
        HWI2C_DMA_AVAILABLE,
        HWI2C_DMA_ADR,
        HWI2C_DMA_GAP_CHK,
        HWI2C_DMA_LEN,
        HWI2C_DMA_MEMORY_TYPE},
};

#ifdef CONFIG_MSTAR_CMA
CMA_MMapInfo_t __cma_mmap_1024mb_0mb[CMA_MMAP_ID_MAX] =
{
    // refer to mmap_hwopt_xx.h for symbol names
    {   "XC_BT_SOUND",
        XC_DMA_ADR,
        XC_DMA_LEN,
        XC_DMA_MEMORY_TYPE,
        XC_DMA_CMA_HID }//,
};
#endif /* CONFIG_MSTAR_CMA */
//#endif
